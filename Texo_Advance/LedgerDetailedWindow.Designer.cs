﻿namespace Texo_Advance
{
    partial class LedgerDetailedWindow
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.ledgerDataGridView = new System.Windows.Forms.DataGridView();
            this.dateTextBoxGrid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.chqBillTextBoxGrid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.referenceAccountTextBoxGrid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.debitTextBoxGrid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.creditTextBoxGrid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.balanceTextBoxGrid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.voucherTextBoxGrid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.remarkTextBoxGrid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.chqDateTextBoxGrid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.chqVoucherNoTextBoxGrid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.transactionTextBoxGrid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.referenceTextBoxGrid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.openingDebitTextBox = new System.Windows.Forms.TextBox();
            this.openingCreditTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tranTotalDebitTextBox = new System.Windows.Forms.TextBox();
            this.tranTotalCreditTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.balTotalDebitTextBox = new System.Windows.Forms.TextBox();
            this.balTotalCreditTextBox = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.ledgerDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // ledgerDataGridView
            // 
            this.ledgerDataGridView.AllowUserToAddRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ledgerDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.ledgerDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ledgerDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dateTextBoxGrid,
            this.chqBillTextBoxGrid,
            this.referenceAccountTextBoxGrid,
            this.debitTextBoxGrid,
            this.creditTextBoxGrid,
            this.balanceTextBoxGrid,
            this.voucherTextBoxGrid,
            this.remarkTextBoxGrid,
            this.chqDateTextBoxGrid,
            this.chqVoucherNoTextBoxGrid,
            this.transactionTextBoxGrid,
            this.referenceTextBoxGrid});
            this.ledgerDataGridView.Location = new System.Drawing.Point(22, 75);
            this.ledgerDataGridView.Name = "ledgerDataGridView";
            this.ledgerDataGridView.ReadOnly = true;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ledgerDataGridView.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.ledgerDataGridView.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.ledgerDataGridView.Size = new System.Drawing.Size(1068, 432);
            this.ledgerDataGridView.TabIndex = 0;
            this.ledgerDataGridView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ledgerDataGridView_KeyDown);
            // 
            // dateTextBoxGrid
            // 
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTextBoxGrid.DefaultCellStyle = dataGridViewCellStyle2;
            this.dateTextBoxGrid.HeaderText = "DATE";
            this.dateTextBoxGrid.Name = "dateTextBoxGrid";
            this.dateTextBoxGrid.ReadOnly = true;
            // 
            // chqBillTextBoxGrid
            // 
            this.chqBillTextBoxGrid.HeaderText = "CHQ/BILL";
            this.chqBillTextBoxGrid.Name = "chqBillTextBoxGrid";
            this.chqBillTextBoxGrid.ReadOnly = true;
            // 
            // referenceAccountTextBoxGrid
            // 
            this.referenceAccountTextBoxGrid.HeaderText = "REFERENCE ACCOUNT";
            this.referenceAccountTextBoxGrid.Name = "referenceAccountTextBoxGrid";
            this.referenceAccountTextBoxGrid.ReadOnly = true;
            this.referenceAccountTextBoxGrid.Width = 250;
            // 
            // debitTextBoxGrid
            // 
            this.debitTextBoxGrid.HeaderText = "DEBIT";
            this.debitTextBoxGrid.Name = "debitTextBoxGrid";
            this.debitTextBoxGrid.ReadOnly = true;
            this.debitTextBoxGrid.Width = 150;
            // 
            // creditTextBoxGrid
            // 
            this.creditTextBoxGrid.HeaderText = "CREDIT";
            this.creditTextBoxGrid.Name = "creditTextBoxGrid";
            this.creditTextBoxGrid.ReadOnly = true;
            this.creditTextBoxGrid.Width = 150;
            // 
            // balanceTextBoxGrid
            // 
            this.balanceTextBoxGrid.HeaderText = "BALANCE";
            this.balanceTextBoxGrid.Name = "balanceTextBoxGrid";
            this.balanceTextBoxGrid.ReadOnly = true;
            this.balanceTextBoxGrid.Width = 200;
            // 
            // voucherTextBoxGrid
            // 
            this.voucherTextBoxGrid.HeaderText = "VNO.";
            this.voucherTextBoxGrid.Name = "voucherTextBoxGrid";
            this.voucherTextBoxGrid.ReadOnly = true;
            this.voucherTextBoxGrid.Width = 50;
            // 
            // remarkTextBoxGrid
            // 
            this.remarkTextBoxGrid.HeaderText = "REMARK";
            this.remarkTextBoxGrid.Name = "remarkTextBoxGrid";
            this.remarkTextBoxGrid.ReadOnly = true;
            this.remarkTextBoxGrid.Width = 400;
            // 
            // chqDateTextBoxGrid
            // 
            this.chqDateTextBoxGrid.HeaderText = "CHQ.DATE";
            this.chqDateTextBoxGrid.Name = "chqDateTextBoxGrid";
            this.chqDateTextBoxGrid.ReadOnly = true;
            // 
            // chqVoucherNoTextBoxGrid
            // 
            this.chqVoucherNoTextBoxGrid.HeaderText = "V.NO.";
            this.chqVoucherNoTextBoxGrid.Name = "chqVoucherNoTextBoxGrid";
            this.chqVoucherNoTextBoxGrid.ReadOnly = true;
            // 
            // transactionTextBoxGrid
            // 
            this.transactionTextBoxGrid.HeaderText = "TRANSACTION";
            this.transactionTextBoxGrid.Name = "transactionTextBoxGrid";
            this.transactionTextBoxGrid.ReadOnly = true;
            this.transactionTextBoxGrid.Width = 200;
            // 
            // referenceTextBoxGrid
            // 
            this.referenceTextBoxGrid.HeaderText = "REFERENCE";
            this.referenceTextBoxGrid.Name = "referenceTextBoxGrid";
            this.referenceTextBoxGrid.ReadOnly = true;
            this.referenceTextBoxGrid.Width = 200;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(274, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(237, 16);
            this.label1.TabIndex = 1;
            this.label1.Text = "BALANCE BROUGHT FORWARD";
            // 
            // openingDebitTextBox
            // 
            this.openingDebitTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.openingDebitTextBox.Location = new System.Drawing.Point(518, 43);
            this.openingDebitTextBox.Name = "openingDebitTextBox";
            this.openingDebitTextBox.ReadOnly = true;
            this.openingDebitTextBox.Size = new System.Drawing.Size(144, 21);
            this.openingDebitTextBox.TabIndex = 2;
            this.openingDebitTextBox.Text = "0.00";
            this.openingDebitTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // openingCreditTextBox
            // 
            this.openingCreditTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.openingCreditTextBox.Location = new System.Drawing.Point(668, 43);
            this.openingCreditTextBox.Name = "openingCreditTextBox";
            this.openingCreditTextBox.ReadOnly = true;
            this.openingCreditTextBox.Size = new System.Drawing.Size(144, 21);
            this.openingCreditTextBox.TabIndex = 2;
            this.openingCreditTextBox.Text = "0.00";
            this.openingCreditTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(343, 530);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(169, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "TRANSACTION TOTAL";
            // 
            // tranTotalDebitTextBox
            // 
            this.tranTotalDebitTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tranTotalDebitTextBox.Location = new System.Drawing.Point(518, 528);
            this.tranTotalDebitTextBox.Name = "tranTotalDebitTextBox";
            this.tranTotalDebitTextBox.ReadOnly = true;
            this.tranTotalDebitTextBox.Size = new System.Drawing.Size(144, 21);
            this.tranTotalDebitTextBox.TabIndex = 2;
            this.tranTotalDebitTextBox.Text = "0.00";
            this.tranTotalDebitTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tranTotalCreditTextBox
            // 
            this.tranTotalCreditTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tranTotalCreditTextBox.Location = new System.Drawing.Point(668, 528);
            this.tranTotalCreditTextBox.Name = "tranTotalCreditTextBox";
            this.tranTotalCreditTextBox.ReadOnly = true;
            this.tranTotalCreditTextBox.Size = new System.Drawing.Size(144, 21);
            this.tranTotalCreditTextBox.TabIndex = 2;
            this.tranTotalCreditTextBox.Text = "0.00";
            this.tranTotalCreditTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(436, 557);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 16);
            this.label3.TabIndex = 1;
            this.label3.Text = "BALANCE";
            // 
            // balTotalDebitTextBox
            // 
            this.balTotalDebitTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.balTotalDebitTextBox.Location = new System.Drawing.Point(518, 555);
            this.balTotalDebitTextBox.Name = "balTotalDebitTextBox";
            this.balTotalDebitTextBox.ReadOnly = true;
            this.balTotalDebitTextBox.Size = new System.Drawing.Size(144, 21);
            this.balTotalDebitTextBox.TabIndex = 2;
            this.balTotalDebitTextBox.Text = "0.00";
            this.balTotalDebitTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // balTotalCreditTextBox
            // 
            this.balTotalCreditTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.balTotalCreditTextBox.Location = new System.Drawing.Point(668, 555);
            this.balTotalCreditTextBox.Name = "balTotalCreditTextBox";
            this.balTotalCreditTextBox.ReadOnly = true;
            this.balTotalCreditTextBox.Size = new System.Drawing.Size(144, 21);
            this.balTotalCreditTextBox.TabIndex = 2;
            this.balTotalCreditTextBox.Text = "0.00";
            this.balTotalCreditTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // LedgerDetailedWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.balTotalCreditTextBox);
            this.Controls.Add(this.balTotalDebitTextBox);
            this.Controls.Add(this.tranTotalCreditTextBox);
            this.Controls.Add(this.tranTotalDebitTextBox);
            this.Controls.Add(this.openingCreditTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.openingDebitTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ledgerDataGridView);
            this.Name = "LedgerDetailedWindow";
            this.Size = new System.Drawing.Size(1140, 584);
            ((System.ComponentModel.ISupportInitialize)(this.ledgerDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView ledgerDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateTextBoxGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn chqBillTextBoxGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn referenceAccountTextBoxGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn debitTextBoxGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn creditTextBoxGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn balanceTextBoxGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn voucherTextBoxGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn remarkTextBoxGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn chqDateTextBoxGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn chqVoucherNoTextBoxGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn transactionTextBoxGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn referenceTextBoxGrid;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox openingDebitTextBox;
        private System.Windows.Forms.TextBox openingCreditTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tranTotalDebitTextBox;
        private System.Windows.Forms.TextBox tranTotalCreditTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox balTotalDebitTextBox;
        private System.Windows.Forms.TextBox balTotalCreditTextBox;
    }
}
