﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Texo_Advance
{
    public partial class TakaDetailsWindow : Form
    {
        double dTotalTaka = 0;
        double dTotalMetres = 0;
        long lVoucherNo = 0;
        
        public List<greytaka_details> takaDetails = new List<greytaka_details>();
        public TakaDetailsWindow()
        {
            InitializeComponent();
            //dTotalTaka = dTaka;
            //dTotalMetres = dMtrs;
        }
        public void setDefaultData(long lSrNo,double dTaka,double dMtrs)
        {
            lVoucherNo = lSrNo;
            dTotalTaka = dTaka;
            dTotalMetres = dMtrs;
            setTakaDetailsData();
        }

        private void TakaDetailsWindow_Load(object sender, EventArgs e)
        {
            if(takaListView.Items.Count == 0)
                totalMtrsTextBox.Text = "0.0";
            if (takaListView.Items.Count == 0)
                totalTakaTextBox.Text = "0";
            talaMtrsTextBox.Text = "0";
            takaWeightTextBox.Text = "0.0";
            talaMtrsTextBox.Focus();
            talaMtrsTextBox.SelectAll();
            takaWeightTextBox.SelectAll();
        }

        private void talaMtrsTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            
            if (e.KeyCode == Keys.Enter)
            {
                /*decimal dMtrs =0;
                bool bFlag = decimal.TryParse(talaMtrsTextBox.Text,out dMtrs);
                if(bFlag)
                {
                    if (!checkTotalTakaData(dMtrs))
                        return;
                    string[] arr = new string[4];
                    ListViewItem itm;
                    //Add first item
                    
                    arr[0] = (takaListView.Items.Count+1).ToString();
                    arr[1] = dMtrs.ToString();
                    arr[2] = "0.0";
                    itm = new ListViewItem(arr);
                    takaListView.Items.Add(itm);
                    talaMtrsTextBox.SelectAll();
                    setTotalTakaData();
                }
                 */
                decimal dMtrs = 0;
                var greyTakaMtrsAfterSplit = talaMtrsTextBox.Text.Split('+');
                for(int i = 0;i < greyTakaMtrsAfterSplit.Count();i++)
                {
                    decimal dTempMtrs = 0;
                    decimal.TryParse(greyTakaMtrsAfterSplit[i], out dTempMtrs);
                    dMtrs = dMtrs + dTempMtrs;
                }
                if (!checkTotalTakaData(dMtrs))
                    return;
                string[] arr = new string[4];
                ListViewItem itm;
                //Add first item

                arr[0] = (takaListView.Items.Count + 1).ToString();
                arr[1] = talaMtrsTextBox.Text;
                arr[2] = "0.0";
                itm = new ListViewItem(arr);
                takaListView.Items.Add(itm);
                talaMtrsTextBox.SelectAll();
                setTotalTakaData();
            }
        
            
        }

        private void takaWeightTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                decimal dMtrs = 0;
                decimal dWt = 0;
                bool bFlag = decimal.TryParse(talaMtrsTextBox.Text, out dMtrs);
                bool bFlag1 = decimal.TryParse(takaWeightTextBox.Text, out dWt);
                if (bFlag && bFlag1)
                {
                    if (!checkTotalTakaData(dMtrs))
                        return;
                    string[] arr = new string[4];
                    ListViewItem itm;
                    //Add first item

                    arr[0] = (takaListView.Items.Count + 1).ToString();
                    arr[1] = dMtrs.ToString();
                    arr[2] = dWt.ToString();
                    itm = new ListViewItem(arr);
                    takaListView.Items.Add(itm);
                    takaWeightTextBox.Text = "0.0";
                    talaMtrsTextBox.Text = "0.0";
                    talaMtrsTextBox.Focus();
                    talaMtrsTextBox.SelectAll();
                    setTotalTakaData();
                }

            }
        }

        private void setTotalTakaData()
        {
            string strTotalTaka = Convert.ToString(takaListView.Items.Count);
            if(takaListView.Items.Count > 0)
            {
                double dTotalMtrs = 0;
                for (int i = 0; i < takaListView.Items.Count; i++)
                {
                    double dMtrs = 0;
                    var greyTakaMtrsAfterSplit = takaListView.Items[i].SubItems[1].Text.Split('+');
                    for(int j = 0;j < greyTakaMtrsAfterSplit.Count();j++)
                    {
                        double dTempMtrs = 0;
                        double.TryParse(greyTakaMtrsAfterSplit[j], out dTempMtrs);
                        dMtrs = dMtrs + dTempMtrs;
                    }
                    //double dMtrs = Convert.ToDouble(takaListView.Items[i].SubItems[1].Text);
                    dTotalMtrs = dTotalMtrs + dMtrs;
                }
                totalMtrsTextBox.Text = (Convert.ToString(dTotalMtrs));
                totalTakaTextBox.Text = Convert.ToString(takaListView.Items.Count);
                if ((dTotalMtrs > dTotalMetres))
                    MessageBox.Show("Taka Mtrs exceeding...", "Error", MessageBoxButtons.OK);
                else if((takaListView.Items.Count > dTotalTaka))
                    MessageBox.Show("No. Of Takas exceeding...", "Error", MessageBoxButtons.OK);
            }
            
        }
        private bool checkTotalTakaData(decimal dMtrs)
        {
            //decimal dTotalTaka = Convert.ToDecimal(totalTakaTextBox.Text);
            //decimal dTotalMtrs = Convert.ToDecimal(totalMtrsTextBox.Text);
            decimal dTotalMtrs = 0;
            if (takaListView.Items.Count > 0)
            {
                
                for (int i = 0; i < takaListView.Items.Count; i++)
                {
                    decimal dMtrs1 = Convert.ToDecimal(takaListView.Items[i].SubItems[1].Text);
                    dTotalMtrs = dTotalMtrs + dMtrs1;
                }
            }

                decimal dNewTotalMtrs = 0;
            dNewTotalMtrs = dTotalMtrs + dMtrs;
            if ((dNewTotalMtrs > Convert.ToDecimal(dTotalMetres)))
            {
                MessageBox.Show("Taka Mtrs exceeding...", "Error", MessageBoxButtons.OK);
                return false;
            }
                
            if ((takaListView.Items.Count +1)> dTotalTaka)
            {
                MessageBox.Show("No. Of Takas exceeding...", "Error", MessageBoxButtons.OK);
                return false;
            }

            return true;
            

        }

        private void deleteTakaButton_Click(object sender, EventArgs e)
        {
            for (int i = 0;i< takaListView.Items.Count;i++)
            {
                if (takaListView.Items[i].Selected)
                {
                    takaListView.Items.Remove(takaListView.Items[i]);
                }
            }
            for(int j = 0;j < takaListView.Items.Count;j++)
            {
                takaListView.Items[j].SubItems[0].Text = Convert.ToString(j + 1);
            }
            talaMtrsTextBox.Focus();
            talaMtrsTextBox.SelectAll();
            setTotalTakaData();
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (takaDetails.Count > 0)
                    takaDetails.Clear();
                for (int i = 0; i < takaListView.Items.Count; i++)
                {
                    greytaka_details takaData = new greytaka_details();
                    //takaData.greypurchase_id = lVoucherNo;
                    takaData.takaMtrs = takaListView.Items[i].SubItems[1].Text;
                    takaData.takaNo = Convert.ToInt64(takaListView.Items[i].SubItems[0].Text);
                    takaData.takaWeight = Convert.ToDouble(takaListView.Items[i].SubItems[2].Text);
                    takaDetails.Add(takaData);
                }
                MessageBox.Show("Taka Details Saved", "Taka Details", MessageBoxButtons.OK);
            }
            catch(Exception ex)
            {
                MessageBox.Show("Taka Details not Saved", "Error", MessageBoxButtons.OK);
                return;
            }

            
        }

        private void setTakaDetailsData()
        {

            takaListView.Items.Clear();
            for (int i = 0;i < takaDetails.Count;i++)
            {
                string[] arr = new string[4];
                ListViewItem itm;
                //Add first item

                arr[0] = takaDetails[i].takaNo.ToString();
                arr[1] = takaDetails[i].takaMtrs.ToString();
                arr[2] = takaDetails[i].takaWeight.ToString();
                itm = new ListViewItem(arr);
                takaListView.Items.Add(itm);
                
            
            }
            setTotalTakaData();
            //takaDetails.Clear();
            
        }

        public void clearTakaDetails()
        {
            if (takaDetails.Count > 0)
            {
                takaDetails.Clear();
                totalMtrsTextBox.Text = "0.0";
                totalTakaTextBox.Text = "0";
            }
                
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
