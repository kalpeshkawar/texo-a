﻿namespace Texo_Advance
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.sideBar = new System.Windows.Forms.Panel();
            this.homeButton = new System.Windows.Forms.Button();
            this.logOutButton = new System.Windows.Forms.Button();
            this.gstReturnsButton = new System.Windows.Forms.Button();
            this.masterButton = new System.Windows.Forms.Button();
            this.passbookButton = new System.Windows.Forms.Button();
            this.reportButton = new System.Windows.Forms.Button();
            this.jobWorkButton = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.millTransactionsButton = new System.Windows.Forms.Button();
            this.greyPurchaseButton = new System.Windows.Forms.Button();
            this.purchaseButon = new System.Windows.Forms.Button();
            this.salesButton = new System.Windows.Forms.Button();
            this.topBar = new System.Windows.Forms.Panel();
            this.labelName = new System.Windows.Forms.Label();
            this.labelYear = new System.Windows.Forms.Label();
            this.labelStatus = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.bILLENTRYToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salesBillEntryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.purchaseBillEntryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.greyPurchaseBillToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.packingMaterialToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.millReceiptBillToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.jobWorkToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salesReturnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.purchaseReturnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.greySalesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.greyReturnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.debitCreditNotesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.generalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openingBalancesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openingStockToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.journalEntriesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.printerSettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.homePanel = new System.Windows.Forms.Panel();
            this.ledgerButton = new System.Windows.Forms.Button();
            this.sideBar.SuspendLayout();
            this.topBar.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // sideBar
            // 
            this.sideBar.BackColor = System.Drawing.Color.Pink;
            this.sideBar.Controls.Add(this.homeButton);
            this.sideBar.Controls.Add(this.logOutButton);
            this.sideBar.Controls.Add(this.gstReturnsButton);
            this.sideBar.Controls.Add(this.masterButton);
            this.sideBar.Controls.Add(this.ledgerButton);
            this.sideBar.Controls.Add(this.passbookButton);
            this.sideBar.Controls.Add(this.reportButton);
            this.sideBar.Controls.Add(this.jobWorkButton);
            this.sideBar.Controls.Add(this.button1);
            this.sideBar.Controls.Add(this.millTransactionsButton);
            this.sideBar.Controls.Add(this.greyPurchaseButton);
            this.sideBar.Controls.Add(this.purchaseButon);
            this.sideBar.Controls.Add(this.salesButton);
            this.sideBar.Location = new System.Drawing.Point(1, 1);
            this.sideBar.Name = "sideBar";
            this.sideBar.Size = new System.Drawing.Size(118, 716);
            this.sideBar.TabIndex = 0;
            // 
            // homeButton
            // 
            this.homeButton.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.homeButton.ForeColor = System.Drawing.Color.Indigo;
            this.homeButton.Location = new System.Drawing.Point(11, 3);
            this.homeButton.Name = "homeButton";
            this.homeButton.Size = new System.Drawing.Size(100, 33);
            this.homeButton.TabIndex = 1;
            this.homeButton.TabStop = false;
            this.homeButton.Text = "&Home";
            this.homeButton.UseVisualStyleBackColor = true;
            this.homeButton.Click += new System.EventHandler(this.homeButton_Click);
            // 
            // logOutButton
            // 
            this.logOutButton.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.logOutButton.ForeColor = System.Drawing.Color.Indigo;
            this.logOutButton.Location = new System.Drawing.Point(11, 655);
            this.logOutButton.Name = "logOutButton";
            this.logOutButton.Size = new System.Drawing.Size(100, 33);
            this.logOutButton.TabIndex = 0;
            this.logOutButton.TabStop = false;
            this.logOutButton.Text = "&Log Out";
            this.logOutButton.UseVisualStyleBackColor = true;
            this.logOutButton.Click += new System.EventHandler(this.logOutButton_Click);
            // 
            // gstReturnsButton
            // 
            this.gstReturnsButton.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gstReturnsButton.ForeColor = System.Drawing.Color.Indigo;
            this.gstReturnsButton.Location = new System.Drawing.Point(11, 554);
            this.gstReturnsButton.Name = "gstReturnsButton";
            this.gstReturnsButton.Size = new System.Drawing.Size(100, 33);
            this.gstReturnsButton.TabIndex = 0;
            this.gstReturnsButton.TabStop = false;
            this.gstReturnsButton.Text = "GS&T Reports";
            this.gstReturnsButton.UseVisualStyleBackColor = true;
            // 
            // masterButton
            // 
            this.masterButton.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.masterButton.ForeColor = System.Drawing.Color.Indigo;
            this.masterButton.Location = new System.Drawing.Point(11, 605);
            this.masterButton.Name = "masterButton";
            this.masterButton.Size = new System.Drawing.Size(100, 33);
            this.masterButton.TabIndex = 0;
            this.masterButton.TabStop = false;
            this.masterButton.Text = "M&ASTER";
            this.masterButton.UseVisualStyleBackColor = true;
            this.masterButton.Click += new System.EventHandler(this.masterButton_Click);
            // 
            // passbookButton
            // 
            this.passbookButton.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.passbookButton.ForeColor = System.Drawing.Color.Indigo;
            this.passbookButton.Location = new System.Drawing.Point(11, 398);
            this.passbookButton.Name = "passbookButton";
            this.passbookButton.Size = new System.Drawing.Size(100, 33);
            this.passbookButton.TabIndex = 0;
            this.passbookButton.TabStop = false;
            this.passbookButton.Text = "Pass&Book";
            this.passbookButton.UseVisualStyleBackColor = true;
            this.passbookButton.Click += new System.EventHandler(this.passbookButton_Click);
            // 
            // reportButton
            // 
            this.reportButton.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.reportButton.ForeColor = System.Drawing.Color.Indigo;
            this.reportButton.Location = new System.Drawing.Point(11, 507);
            this.reportButton.Name = "reportButton";
            this.reportButton.Size = new System.Drawing.Size(100, 33);
            this.reportButton.TabIndex = 0;
            this.reportButton.TabStop = false;
            this.reportButton.Text = "&Reports";
            this.reportButton.UseVisualStyleBackColor = true;
            this.reportButton.Click += new System.EventHandler(this.reportButton_Click);
            // 
            // jobWorkButton
            // 
            this.jobWorkButton.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.jobWorkButton.ForeColor = System.Drawing.Color.Indigo;
            this.jobWorkButton.Location = new System.Drawing.Point(11, 342);
            this.jobWorkButton.Name = "jobWorkButton";
            this.jobWorkButton.Size = new System.Drawing.Size(100, 33);
            this.jobWorkButton.TabIndex = 0;
            this.jobWorkButton.TabStop = false;
            this.jobWorkButton.Text = "&Job Work";
            this.jobWorkButton.UseVisualStyleBackColor = true;
            this.jobWorkButton.Click += new System.EventHandler(this.jobWorkButton_Click);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.Indigo;
            this.button1.Location = new System.Drawing.Point(11, 289);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 33);
            this.button1.TabIndex = 0;
            this.button1.TabStop = false;
            this.button1.Text = "&Mill Receipt";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // millTransactionsButton
            // 
            this.millTransactionsButton.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.millTransactionsButton.ForeColor = System.Drawing.Color.Indigo;
            this.millTransactionsButton.Location = new System.Drawing.Point(11, 230);
            this.millTransactionsButton.Name = "millTransactionsButton";
            this.millTransactionsButton.Size = new System.Drawing.Size(100, 35);
            this.millTransactionsButton.TabIndex = 0;
            this.millTransactionsButton.TabStop = false;
            this.millTransactionsButton.Text = "Mill &Dispatch ";
            this.millTransactionsButton.UseVisualStyleBackColor = true;
            this.millTransactionsButton.Click += new System.EventHandler(this.millTransactionsButton_Click);
            // 
            // greyPurchaseButton
            // 
            this.greyPurchaseButton.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.greyPurchaseButton.ForeColor = System.Drawing.Color.Indigo;
            this.greyPurchaseButton.Location = new System.Drawing.Point(11, 168);
            this.greyPurchaseButton.Name = "greyPurchaseButton";
            this.greyPurchaseButton.Size = new System.Drawing.Size(100, 38);
            this.greyPurchaseButton.TabIndex = 0;
            this.greyPurchaseButton.TabStop = false;
            this.greyPurchaseButton.Text = "&Grey Purchase";
            this.greyPurchaseButton.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.greyPurchaseButton.UseVisualStyleBackColor = true;
            this.greyPurchaseButton.Click += new System.EventHandler(this.greyPurchaseButton_Click);
            // 
            // purchaseButon
            // 
            this.purchaseButon.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.purchaseButon.ForeColor = System.Drawing.Color.Indigo;
            this.purchaseButon.Location = new System.Drawing.Point(11, 113);
            this.purchaseButon.Name = "purchaseButon";
            this.purchaseButon.Size = new System.Drawing.Size(100, 33);
            this.purchaseButon.TabIndex = 0;
            this.purchaseButon.TabStop = false;
            this.purchaseButon.Text = "&Purchase";
            this.purchaseButon.UseVisualStyleBackColor = true;
            this.purchaseButon.Click += new System.EventHandler(this.purchaseButon_Click);
            // 
            // salesButton
            // 
            this.salesButton.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.salesButton.ForeColor = System.Drawing.Color.Indigo;
            this.salesButton.Location = new System.Drawing.Point(11, 58);
            this.salesButton.Name = "salesButton";
            this.salesButton.Size = new System.Drawing.Size(100, 33);
            this.salesButton.TabIndex = 0;
            this.salesButton.TabStop = false;
            this.salesButton.Text = "&Sales";
            this.salesButton.UseVisualStyleBackColor = true;
            this.salesButton.Click += new System.EventHandler(this.salesButton_Click);
            // 
            // topBar
            // 
            this.topBar.BackColor = System.Drawing.Color.Pink;
            this.topBar.Controls.Add(this.labelName);
            this.topBar.Controls.Add(this.labelYear);
            this.topBar.Controls.Add(this.labelStatus);
            this.topBar.Controls.Add(this.menuStrip1);
            this.topBar.Location = new System.Drawing.Point(118, 1);
            this.topBar.Name = "topBar";
            this.topBar.Size = new System.Drawing.Size(1320, 24);
            this.topBar.TabIndex = 1;
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelName.ForeColor = System.Drawing.Color.Blue;
            this.labelName.Location = new System.Drawing.Point(7, 24);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(170, 30);
            this.labelName.TabIndex = 1;
            this.labelName.Text = "Company Name";
            this.labelName.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labelYear
            // 
            this.labelYear.AutoSize = true;
            this.labelYear.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelYear.ForeColor = System.Drawing.Color.Blue;
            this.labelYear.Location = new System.Drawing.Point(1092, 24);
            this.labelYear.Name = "labelYear";
            this.labelYear.Size = new System.Drawing.Size(149, 30);
            this.labelYear.TabIndex = 1;
            this.labelYear.Text = "Financial Year";
            this.labelYear.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labelStatus
            // 
            this.labelStatus.AutoSize = true;
            this.labelStatus.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelStatus.ForeColor = System.Drawing.Color.Blue;
            this.labelStatus.Location = new System.Drawing.Point(532, 24);
            this.labelStatus.Name = "labelStatus";
            this.labelStatus.Size = new System.Drawing.Size(76, 30);
            this.labelStatus.TabIndex = 1;
            this.labelStatus.Text = "HOME";
            this.labelStatus.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.Bisque;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bILLENTRYToolStripMenuItem,
            this.generalToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1320, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // bILLENTRYToolStripMenuItem
            // 
            this.bILLENTRYToolStripMenuItem.BackColor = System.Drawing.Color.Bisque;
            this.bILLENTRYToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.salesBillEntryToolStripMenuItem,
            this.purchaseBillEntryToolStripMenuItem,
            this.greyPurchaseBillToolStripMenuItem,
            this.packingMaterialToolStripMenuItem,
            this.millReceiptBillToolStripMenuItem,
            this.jobWorkToolStripMenuItem,
            this.salesReturnToolStripMenuItem,
            this.purchaseReturnToolStripMenuItem,
            this.greySalesToolStripMenuItem,
            this.greyReturnToolStripMenuItem,
            this.debitCreditNotesToolStripMenuItem});
            this.bILLENTRYToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bILLENTRYToolStripMenuItem.ForeColor = System.Drawing.Color.Indigo;
            this.bILLENTRYToolStripMenuItem.Name = "bILLENTRYToolStripMenuItem";
            this.bILLENTRYToolStripMenuItem.Size = new System.Drawing.Size(45, 20);
            this.bILLENTRYToolStripMenuItem.Text = "Bills";
            // 
            // salesBillEntryToolStripMenuItem
            // 
            this.salesBillEntryToolStripMenuItem.ForeColor = System.Drawing.Color.Indigo;
            this.salesBillEntryToolStripMenuItem.Name = "salesBillEntryToolStripMenuItem";
            this.salesBillEntryToolStripMenuItem.Size = new System.Drawing.Size(243, 22);
            this.salesBillEntryToolStripMenuItem.Text = "&Sales Bill";
            this.salesBillEntryToolStripMenuItem.Click += new System.EventHandler(this.salesBillEntryToolStripMenuItem_Click);
            // 
            // purchaseBillEntryToolStripMenuItem
            // 
            this.purchaseBillEntryToolStripMenuItem.ForeColor = System.Drawing.Color.Indigo;
            this.purchaseBillEntryToolStripMenuItem.Name = "purchaseBillEntryToolStripMenuItem";
            this.purchaseBillEntryToolStripMenuItem.Size = new System.Drawing.Size(243, 22);
            this.purchaseBillEntryToolStripMenuItem.Text = "&Purchase Bill";
            this.purchaseBillEntryToolStripMenuItem.Click += new System.EventHandler(this.purchaseBillEntryToolStripMenuItem_Click);
            // 
            // greyPurchaseBillToolStripMenuItem
            // 
            this.greyPurchaseBillToolStripMenuItem.ForeColor = System.Drawing.Color.Indigo;
            this.greyPurchaseBillToolStripMenuItem.Name = "greyPurchaseBillToolStripMenuItem";
            this.greyPurchaseBillToolStripMenuItem.Size = new System.Drawing.Size(243, 22);
            this.greyPurchaseBillToolStripMenuItem.Text = "&Grey Purchase";
            this.greyPurchaseBillToolStripMenuItem.Click += new System.EventHandler(this.greyPurchaseBillToolStripMenuItem_Click);
            // 
            // packingMaterialToolStripMenuItem
            // 
            this.packingMaterialToolStripMenuItem.ForeColor = System.Drawing.Color.Indigo;
            this.packingMaterialToolStripMenuItem.Name = "packingMaterialToolStripMenuItem";
            this.packingMaterialToolStripMenuItem.Size = new System.Drawing.Size(243, 22);
            this.packingMaterialToolStripMenuItem.Text = "Pac&king Material";
            this.packingMaterialToolStripMenuItem.Click += new System.EventHandler(this.packingMaterialToolStripMenuItem_Click);
            // 
            // millReceiptBillToolStripMenuItem
            // 
            this.millReceiptBillToolStripMenuItem.ForeColor = System.Drawing.Color.Indigo;
            this.millReceiptBillToolStripMenuItem.Name = "millReceiptBillToolStripMenuItem";
            this.millReceiptBillToolStripMenuItem.Size = new System.Drawing.Size(243, 22);
            this.millReceiptBillToolStripMenuItem.Text = "&Mill Receipt Bill";
            this.millReceiptBillToolStripMenuItem.Click += new System.EventHandler(this.millReceiptBillToolStripMenuItem_Click);
            // 
            // jobWorkToolStripMenuItem
            // 
            this.jobWorkToolStripMenuItem.ForeColor = System.Drawing.Color.Indigo;
            this.jobWorkToolStripMenuItem.Name = "jobWorkToolStripMenuItem";
            this.jobWorkToolStripMenuItem.Size = new System.Drawing.Size(243, 22);
            this.jobWorkToolStripMenuItem.Text = "&Job Work / Value Additions";
            this.jobWorkToolStripMenuItem.Click += new System.EventHandler(this.jobWorkToolStripMenuItem_Click);
            // 
            // salesReturnToolStripMenuItem
            // 
            this.salesReturnToolStripMenuItem.ForeColor = System.Drawing.Color.Indigo;
            this.salesReturnToolStripMenuItem.Name = "salesReturnToolStripMenuItem";
            this.salesReturnToolStripMenuItem.Size = new System.Drawing.Size(243, 22);
            this.salesReturnToolStripMenuItem.Text = "Sales Return";
            this.salesReturnToolStripMenuItem.Click += new System.EventHandler(this.salesReturnToolStripMenuItem_Click);
            // 
            // purchaseReturnToolStripMenuItem
            // 
            this.purchaseReturnToolStripMenuItem.ForeColor = System.Drawing.Color.Indigo;
            this.purchaseReturnToolStripMenuItem.Name = "purchaseReturnToolStripMenuItem";
            this.purchaseReturnToolStripMenuItem.Size = new System.Drawing.Size(243, 22);
            this.purchaseReturnToolStripMenuItem.Text = "Purchase Return";
            this.purchaseReturnToolStripMenuItem.Click += new System.EventHandler(this.purchaseReturnToolStripMenuItem_Click);
            // 
            // greySalesToolStripMenuItem
            // 
            this.greySalesToolStripMenuItem.ForeColor = System.Drawing.Color.Indigo;
            this.greySalesToolStripMenuItem.Name = "greySalesToolStripMenuItem";
            this.greySalesToolStripMenuItem.Size = new System.Drawing.Size(243, 22);
            this.greySalesToolStripMenuItem.Text = "Grey Sales";
            this.greySalesToolStripMenuItem.Click += new System.EventHandler(this.greySalesToolStripMenuItem_Click);
            // 
            // greyReturnToolStripMenuItem
            // 
            this.greyReturnToolStripMenuItem.ForeColor = System.Drawing.Color.Indigo;
            this.greyReturnToolStripMenuItem.Name = "greyReturnToolStripMenuItem";
            this.greyReturnToolStripMenuItem.Size = new System.Drawing.Size(243, 22);
            this.greyReturnToolStripMenuItem.Text = "Grey Purchase Return";
            this.greyReturnToolStripMenuItem.Click += new System.EventHandler(this.greyReturnToolStripMenuItem_Click);
            // 
            // debitCreditNotesToolStripMenuItem
            // 
            this.debitCreditNotesToolStripMenuItem.ForeColor = System.Drawing.Color.Indigo;
            this.debitCreditNotesToolStripMenuItem.Name = "debitCreditNotesToolStripMenuItem";
            this.debitCreditNotesToolStripMenuItem.Size = new System.Drawing.Size(243, 22);
            this.debitCreditNotesToolStripMenuItem.Text = "Debit / Credit Notes";
            this.debitCreditNotesToolStripMenuItem.Click += new System.EventHandler(this.debitCreditNotesToolStripMenuItem_Click);
            // 
            // generalToolStripMenuItem
            // 
            this.generalToolStripMenuItem.BackColor = System.Drawing.Color.Bisque;
            this.generalToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openingBalancesToolStripMenuItem,
            this.openingStockToolStripMenuItem,
            this.journalEntriesToolStripMenuItem,
            this.printerSettingsToolStripMenuItem});
            this.generalToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.generalToolStripMenuItem.ForeColor = System.Drawing.Color.Indigo;
            this.generalToolStripMenuItem.Name = "generalToolStripMenuItem";
            this.generalToolStripMenuItem.Size = new System.Drawing.Size(66, 20);
            this.generalToolStripMenuItem.Text = "General";
            // 
            // openingBalancesToolStripMenuItem
            // 
            this.openingBalancesToolStripMenuItem.ForeColor = System.Drawing.Color.Indigo;
            this.openingBalancesToolStripMenuItem.Name = "openingBalancesToolStripMenuItem";
            this.openingBalancesToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.openingBalancesToolStripMenuItem.Text = "&Opening Balances";
            this.openingBalancesToolStripMenuItem.Click += new System.EventHandler(this.openingBalancesToolStripMenuItem_Click);
            // 
            // openingStockToolStripMenuItem
            // 
            this.openingStockToolStripMenuItem.ForeColor = System.Drawing.Color.Indigo;
            this.openingStockToolStripMenuItem.Name = "openingStockToolStripMenuItem";
            this.openingStockToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.openingStockToolStripMenuItem.Text = "Opening Stock";
            this.openingStockToolStripMenuItem.Click += new System.EventHandler(this.openingStockToolStripMenuItem_Click);
            // 
            // journalEntriesToolStripMenuItem
            // 
            this.journalEntriesToolStripMenuItem.ForeColor = System.Drawing.Color.Indigo;
            this.journalEntriesToolStripMenuItem.Name = "journalEntriesToolStripMenuItem";
            this.journalEntriesToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.journalEntriesToolStripMenuItem.Text = "Journal Entries";
            this.journalEntriesToolStripMenuItem.Click += new System.EventHandler(this.journalEntriesToolStripMenuItem_Click);
            // 
            // printerSettingsToolStripMenuItem
            // 
            this.printerSettingsToolStripMenuItem.ForeColor = System.Drawing.Color.Indigo;
            this.printerSettingsToolStripMenuItem.Name = "printerSettingsToolStripMenuItem";
            this.printerSettingsToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.printerSettingsToolStripMenuItem.Text = "Printer Settings";
            this.printerSettingsToolStripMenuItem.Click += new System.EventHandler(this.printerSettingsToolStripMenuItem_Click);
            // 
            // homePanel
            // 
            this.homePanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.homePanel.AutoSize = true;
            this.homePanel.Location = new System.Drawing.Point(118, 25);
            this.homePanel.Name = "homePanel";
            this.homePanel.Size = new System.Drawing.Size(1339, 693);
            this.homePanel.TabIndex = 2;
            // 
            // ledgerButton
            // 
            this.ledgerButton.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ledgerButton.ForeColor = System.Drawing.Color.Indigo;
            this.ledgerButton.Location = new System.Drawing.Point(11, 451);
            this.ledgerButton.Name = "ledgerButton";
            this.ledgerButton.Size = new System.Drawing.Size(100, 33);
            this.ledgerButton.TabIndex = 0;
            this.ledgerButton.TabStop = false;
            this.ledgerButton.Text = "&Ledger";
            this.ledgerButton.UseVisualStyleBackColor = true;
            this.ledgerButton.Click += new System.EventHandler(this.button2_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.MintCream;
            this.ClientSize = new System.Drawing.Size(1362, 691);
            this.Controls.Add(this.topBar);
            this.Controls.Add(this.homePanel);
            this.Controls.Add(this.sideBar);
            this.KeyPreview = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Texo Advance";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Form1_KeyPress);
            this.sideBar.ResumeLayout(false);
            this.topBar.ResumeLayout(false);
            this.topBar.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel sideBar;
        private System.Windows.Forms.Panel topBar;
        private System.Windows.Forms.Button gstReturnsButton;
        private System.Windows.Forms.Button masterButton;
        private System.Windows.Forms.Button reportButton;
        private System.Windows.Forms.Button jobWorkButton;
        private System.Windows.Forms.Button purchaseButon;
        private System.Windows.Forms.Button salesButton;
        private System.Windows.Forms.Panel homePanel;
        private System.Windows.Forms.Button logOutButton;
        private System.Windows.Forms.Button homeButton;
        private System.Windows.Forms.Button millTransactionsButton;
        private System.Windows.Forms.Button greyPurchaseButton;
        private System.Windows.Forms.Label labelStatus;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.Label labelYear;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button passbookButton;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem bILLENTRYToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salesBillEntryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem purchaseBillEntryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem greyPurchaseBillToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem packingMaterialToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem millReceiptBillToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem jobWorkToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem generalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openingBalancesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openingStockToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem journalEntriesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salesReturnToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem purchaseReturnToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem greySalesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem greyReturnToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem debitCreditNotesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem printerSettingsToolStripMenuItem;
        private System.Windows.Forms.Button ledgerButton;
    }
}

