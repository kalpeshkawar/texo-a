﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Texo_Advance.DBItems;
using System.Globalization;
using System.Data.Entity;

namespace Texo_Advance
{
    public partial class MillDispatchWindow : UserControl
    {
        milldispatch_detail millDispatchDetail;
        //List<milldispatchchallan_detail> millDispatchChallanDetail;
        List<List<millchallandispatchtaka_detail>> millChallanDispatchTakaDetail = new List<List<millchallandispatchtaka_detail>>();
        List<milldispatchchallan_detail> millDispatchChallanDetail = new List<milldispatchchallan_detail>();
        List<millchallandispatchtaka_detail> receivedChallanTakaList;
        MasterDBData DB = new MasterDBData();
        PartyDBData partyDB = new PartyDBData();
        private string strCompanyName = "";
        private string companyGstn = "";
        long lQualityID = 0;
        public MillDispatchWindow(string strCompName,string strCompGstn)
        {
            InitializeComponent();
            strCompanyName = strCompName;
            companyGstn = strCompGstn;

        }

        private void createButton_Click(object sender, EventArgs e)
        {

            long lAvailableTaka = 0;
            bool bAvailableTaka = long.TryParse(avlTakaTextBox.Text,out lAvailableTaka);
            if(!bAvailableTaka)
            {
                MessageBox.Show("Please ckeck the available takas first", "MILL DISPATCH", MessageBoxButtons.OK);
                refBillNoTextBox.Focus();
                return;
            }
            double dAvailableMtrs = 0;
            bool bAvailableMtrs = double.TryParse(avlMtrsTextBox.Text, out dAvailableMtrs);
            if (!bAvailableMtrs)
            {
                MessageBox.Show("Please ckeck the available takas first", "MILL DISPATCH", MessageBoxButtons.OK);
                refBillNoTextBox.Focus();
                return;
            }
            if(lAvailableTaka == 0 || dAvailableMtrs == 0)
            {
                MessageBox.Show("No Grey Taka in stock for the selected Bill", "MILL DISPATCH", MessageBoxButtons.OK);
                return;
            }
            long lChallanNo = getVirtualChallanNo();
            if(lChallanNo == -1)
            {
                MessageBox.Show("Unable to fetch Challan No.","Error",MessageBoxButtons.OK);
                return;
            }
            MillChallanWindow mcw = new MillChallanWindow(greyQualityTextBox.Text,lAvailableTaka,dAvailableMtrs,lChallanNo,Convert.ToInt64(jobTypeComboBox.SelectedValue));
            
            mcw.ShowDialog();
            
            if(mcw.takaList.Count > 0)
            {
                milldispatchchallan_detail challanDetail = new milldispatchchallan_detail();
                challanDetail.challanNo = mcw.getChallanNo();
                challanDetail.master = mcw.getMasterName();
                challanDetail.mill_id = mcw.getMillID();
                
                challanDetail.voucherNo = Convert.ToInt64(voucherNoTextBox.Text);
                challanDetail.totalTakas = mcw.takaList.Count;
                double dTotalMtrs = 0;
                for(int i = 0;i < mcw.takaList.Count;i++)
                {
                    double dTakaMtrs = 0;
                    string greyTakaMtrs = mcw.takaList[i];
                    var greyTakaMtrsAfterSplit = greyTakaMtrs.Split('+');
                    for (int j = 0; j < greyTakaMtrsAfterSplit.Count(); j++)
                    {
                        double dTempGreyMtrs = 0;
                        //double.TryParse(millReceivedTakaDataGridView.Rows[i].Cells[finMtrsGridTextBox.Index].EditedFormattedValue.ToString(),out dTempFinMtrs);
                        double.TryParse(greyTakaMtrsAfterSplit[j], out dTempGreyMtrs);
                        dTakaMtrs = dTakaMtrs + dTempGreyMtrs;
                    }
                    dTotalMtrs = dTotalMtrs + dTakaMtrs;
                }
                challanDetail.totalMtrs = dTotalMtrs;
                challanDetail.cardNo = mcw.getChallanNo();
                challanDetail.rate = mcw.getJobRate();
                challanDetail.remark = mcw.getRemarkData();
                challanDetail.date = DateTime.ParseExact(mcw.getDate(), "dd-MM-yyyy", CultureInfo.InvariantCulture);

                if ((long)(jobTypeComboBox.SelectedValue) == 1)
                    challanDetail.reference_billNo = refBillNoTextBox.Text;
                else
                {
                    challanDetail.reference_billNo = partyDB.getRefBillNoForReprocessChallan(refBillNoTextBox.Text);
                    challanDetail.reference_challanNo = refBillNoTextBox.Text;
                }
                challanDetail.quality_id = lQualityID;
                List<millchallandispatchtaka_detail> tempTakaDetails = challanDetail.millchallandispatchtaka_detail.ToList();
                for (int i = 0;i < mcw.takaList.Count;i++)
                {
                    millchallandispatchtaka_detail takaList = new millchallandispatchtaka_detail();
                    takaList.greyMtrs = mcw.takaList[i];
                    takaList.isReceived = false;

                    tempTakaDetails.Add(takaList);
                     
                    //challanDetail.millchallandispatchtaka_detail.Add(takaList);
                }
                int iIndex = millChallanDispatchTakaDetail.Count;
                millChallanDispatchTakaDetail.Add(tempTakaDetails);
                
                millDispatchChallanDetail.Add(challanDetail);
                millDispatchChallanDetail[iIndex].millchallandispatchtaka_detail = millChallanDispatchTakaDetail[iIndex];
                updateChallanDataInList(millDispatchChallanDetail);
                avlMtrsTextBox.Text = Convert.ToString(Convert.ToDouble(avlMtrsTextBox.Text) - dTotalMtrs);
                avlTakaTextBox.Text = Convert.ToString(Convert.ToInt64(avlTakaTextBox.Text) - challanDetail.totalTakas);

                //viewButton.Enabled = true;
                //printButton.Enabled = true;
                //deleteButton.Enabled = true;
            }

        }

        private void MillDispatchWindow_Load(object sender, EventArgs e)
        {
            companyNameTextBox.Text = strCompanyName;
            dateTextBox.Text = DateTime.Today.ToString("dd-MM-yyyy");
            fillProcessTypeList();
            setDefaultData();
        }

        private void setDefaultData()
        {
            long lSrNo = partyDB.getMillDispatchVoucherNo();
            if (lSrNo == -1)
            {
                MessageBox.Show("Unable to retrieve Grey Purchase Data", "Error", MessageBoxButtons.OK);
                return;
            }
            else if (lSrNo == 0)
            {
                voucherNoTextBox.Text = "1";
            }
            else
                voucherNoTextBox.Text = Convert.ToString(lSrNo + 1);
            //viewButton.Enabled = false;
            //printButton.Enabled = false;
            //deleteButton.Enabled = false;
            updateButton.Enabled = false;
            viewVoucherButton.Enabled = true;
            deleteVoucherButton.Enabled = false;
            //createButton.Enabled = true;
            saveButton.Enabled = true;

        }

        private void fillProcessTypeList()
        {
            var millProcessList = DB.getMillProcessType();
            if (millProcessList == null)
                return;
            jobTypeComboBox.DisplayMember = "type";
            jobTypeComboBox.ValueMember = "millProcess_id";
            jobTypeComboBox.DataSource = millProcessList;

        }

        private void dateTextBox_Validating(object sender, CancelEventArgs e)
        {
            DateTime dt;
            bool isValid = DateTime.TryParseExact(dateTextBox.Text, "dd-MM-yyyy", null, DateTimeStyles.None, out dt);
            if (!isValid)
            {
                MessageBox.Show("Please enter the valid date", "DATE", MessageBoxButtons.OK);
                dateTextBox.Text = DateTime.Today.ToString("dd-MM-yyyy");
                dateTextBox.Focus();
            }
        }

        private void refBillNoTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                long lRefBillNo = 0;
                bool bFlag = long.TryParse(refBillNoTextBox.Text, out lRefBillNo);
                if (!bFlag)
                {
                    MessageBox.Show("Please enter a valid Bill No.", "Error", MessageBoxButtons.OK);
                    return;
                }
                if((long)jobTypeComboBox.SelectedValue == 1)
                {
                    greypurchase_details greyData = partyDB.getGreyPurchaseBillDetail(lRefBillNo);
                    if (greyData == null)
                    {
                        MessageBox.Show("Voucher no. of Ref Bill do not exist", "Error", MessageBoxButtons.OK);
                        return;
                    }
                    setRefBillDataInControl(greyData);
                }
                else
                {
                    long lChallanID = partyDB.getMillDispatchChallanIDFromChallanNo(refBillNoTextBox.Text);
                    if(lChallanID == -1)
                    {
                        MessageBox.Show("Please enter a valid Challan No.", "Error", MessageBoxButtons.OK);
                        return;
                    }
                    receivedChallanTakaList = partyDB.getMillReceivedaTakaDetailForReprocess(lChallanID);
                    setRefChallanTakaDetailsInControl();
                }
                
            }
            
        }

        private void setRefChallanTakaDetailsInControl()
        {
            if(receivedChallanTakaList != null && receivedChallanTakaList.Count>0)
            {
                double dFinMts = 0;
                long lTakas = 0;               
                for (int i = 0;i < receivedChallanTakaList.Count;i++)
                {
                    //dFinMts = dFinMts + Convert.ToDouble(receivedChallanTakaList[i].finMtrs);
                    string takaMtrs = receivedChallanTakaList[i].finMtrs;
                    var takaMtrsAfterSplit = takaMtrs.Split('+');
                    for (int j = 0; j < takaMtrsAfterSplit.Count(); j++)
                    {
                        double dTempFinMtrs = 0;
                        //double.TryParse(millReceivedTakaDataGridView.Rows[i].Cells[finMtrsGridTextBox.Index].EditedFormattedValue.ToString(),out dTempFinMtrs);
                        double.TryParse(takaMtrsAfterSplit[j], out dTempFinMtrs);
                        dFinMts = dFinMts + dTempFinMtrs;
                        //if (j > 0 && takaMtrsAfterSplit.Count() > 1)
                          //  iNoOfTP++;
                    }
                }

                lTakas = receivedChallanTakaList.Count;
                recdTakaTextBox.Text = Convert.ToString(lTakas);
                recdMtrsTextBox.Text = Convert.ToString(dFinMts);
                List<milldispatchchallan_detail> refernceChallanList = partyDB.getReferenceChallanDetails(refBillNoTextBox.Text);
                List<milldispatchchallan_detail> refernceChallanOtherVoucherList = partyDB.getReferenceChallanForOtherVoucherDetails(refBillNoTextBox.Text,Convert.ToInt64(voucherNoTextBox.Text)); 
                long lReprocessedTaka = 0;
                double dReprocessedMtrs = 0;
                if (refernceChallanList != null && refernceChallanList.Count>0)
                {
                    if(jobListView.Items.Count>0)
                    {
                        bool bTakaUsedInNewChallan = false;
                        for (int i = 0; i < refernceChallanOtherVoucherList.Count; i++)
                        {
                            lReprocessedTaka = lReprocessedTaka + (long)refernceChallanOtherVoucherList[i].totalTakas;
                            dReprocessedMtrs = dReprocessedMtrs + (double)refernceChallanOtherVoucherList[i].totalMtrs;

                        }
                        for (int i = 0; i < jobListView.Items.Count; i++)
                        {
                            if (Convert.ToInt64(refBillNoTextBox.Text) == Convert.ToInt64(jobListView.Items[i].SubItems[0].Text))
                            {
                                bTakaUsedInNewChallan = true;
                                lTakas = lTakas - Convert.ToInt64(jobListView.Items[i].SubItems[3].Text);
                                dFinMts = dFinMts - Convert.ToDouble(jobListView.Items[i].SubItems[4].Text);
                            }

                        }
                        
                        if(bTakaUsedInNewChallan)
                        {
                            lTakas = lTakas - lReprocessedTaka;
                            dFinMts = dFinMts - dReprocessedMtrs;
                        }
                        
                    }
                    else
                    {
                        for (int i = 0; i < refernceChallanList.Count; i++)
                        {
                            lReprocessedTaka = lReprocessedTaka + (long)refernceChallanList[i].totalTakas;
                            dReprocessedMtrs = dReprocessedMtrs + (double)refernceChallanList[i].totalMtrs;

                        }
                        lTakas = lTakas - lReprocessedTaka;
                        dFinMts = dFinMts - dReprocessedMtrs;
                    }
                    
                }

                
                avlTakaTextBox.Text = Convert.ToString(lTakas);
                avlMtrsTextBox.Text = Convert.ToString(dFinMts);
                greypurchase_details greyDetails = partyDB.getGreyPartyIDFromRefBillNo(Convert.ToInt64(receivedChallanTakaList[0].milldispatchchallan_detail.reference_billNo));
                if(greyDetails !=null)
                {
                    string strGreyPartyName = DB.getGreyPartyNameInMillDispatch(Convert.ToInt64(greyDetails.partyid));
                    if (strGreyPartyName == "")
                    {
                        MessageBox.Show("Unable to fetch grey party name", "Error", MessageBoxButtons.OK);
                    }
                    weaverTextBox.Text = strGreyPartyName;
                    string strQualityName = DB.getGreyQualityNameInMillDispatch((long)receivedChallanTakaList[0].milldispatchchallan_detail.quality_id);
                    if (strQualityName == "")
                    {
                        MessageBox.Show("Unable to fetch grey quality name", "Error", MessageBoxButtons.OK);
                    }
                    greyQualityTextBox.Text = strQualityName;
                    wtTextBox.Text = Convert.ToString(greyDetails.weight);
                    lQualityID = (long)receivedChallanTakaList[0].milldispatchchallan_detail.quality_id;
                }
                
            }
            else
            {
                MessageBox.Show("No Taka pending/received against this challan No.", "Error", MessageBoxButtons.OK);
                return;
                
            }
        }

        private void voucherNoTextBox_Validating(object sender, CancelEventArgs e)
        {
            long lVoucherNo = 0;
            bool bFlag = long.TryParse(voucherNoTextBox.Text, out lVoucherNo);
            if(!bFlag)
            {
                MessageBox.Show("Please enter a valid Voucher No.", "Error", MessageBoxButtons.OK);
                return;
            }

           /* if(millDispatchDetail != null)
            {
                if(lVoucherNo != millDispatchDetail.voucherNo)
                {
                    DialogResult result = MessageBox.Show("Do you want to add new voucher","Confirm",MessageBoxButtons.YesNo);
                    if (result == DialogResult.Yes)
                    {
                        resetAllData();
                        setDefaultData();
                        millChallanDispatchTakaDetail.Clear();
                        millDispatchChallanDetail.Clear();
                    }
                    else
                        voucherNoTextBox.Focus();
                    
                }
            }
            else
            {
                DialogResult result = MessageBox.Show("Do you want to add new voucher", "Confirm", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    resetAllData();
                    setDefaultData();
                    millChallanDispatchTakaDetail.Clear();
                    millDispatchChallanDetail.Clear();
                }
                else
                    voucherNoTextBox.Focus();

            }
           */
        }

        private void setRefBillDataInControl(greypurchase_details billData)
        {
            recdTakaTextBox.Text = Convert.ToString(billData.taka);
            recdMtrsTextBox.Text = Convert.ToString(billData.mtrs);
            string strGreyPartyName = DB.getGreyPartyNameInMillDispatch((long)billData.partyid);
            if(strGreyPartyName == "")
            {
                MessageBox.Show("Unable to fetch grey party name", "Error", MessageBoxButtons.OK);
            }
            weaverTextBox.Text = strGreyPartyName;
            string strQualityName = DB.getGreyQualityNameInMillDispatch((long)billData.quality);
            if (strQualityName == "")
            {
                MessageBox.Show("Unable to fetch grey quality name", "Error", MessageBoxButtons.OK);
            }
            greyQualityTextBox.Text = strQualityName;
            wtTextBox.Text = Convert.ToString(billData.weight);
            List<double> usedTakaDetails = partyDB.getTakaDetailsForReferenceBillNo(refBillNoTextBox.Text);
            List<milldispatchchallan_detail> challanDetails = partyDB.getChallanDetailsForReferenceBillNo(refBillNoTextBox.Text, Convert.ToInt64(voucherNoTextBox.Text));
            
            if(usedTakaDetails != null)
            {
                long lTempTkas = 0, lOtherVoucherTaka = 0;
                double dTempMtrs = 0, dOtherVoucherMtrs = 0;
                long lAvailableTaka = 0;
                double dAvailableMtrs = 0.0;
                if(jobListView.Items.Count > 0)
                {
                    
                    bool bTakaUsedInNewChallan = false;

                    if (challanDetails.Count > 0)
                    {
                        for(int j =0;j<challanDetails.Count;j++)
                        {
                            lOtherVoucherTaka = (long)(lOtherVoucherTaka + challanDetails[j].totalTakas);
                            dOtherVoucherMtrs = (double)(dOtherVoucherMtrs + challanDetails[j].totalMtrs);
                        }
                
                    }
                    for (int i = 0; i < jobListView.Items.Count; i++)
                    {
                        if (Convert.ToInt64(refBillNoTextBox.Text) == Convert.ToInt64(jobListView.Items[i].SubItems[0].Text))
                        {
                            bTakaUsedInNewChallan = true;
                            lTempTkas = lTempTkas + Convert.ToInt64(jobListView.Items[i].SubItems[3].Text);
                            dTempMtrs = dTempMtrs + Convert.ToDouble(jobListView.Items[i].SubItems[4].Text);
                        }

                    }

                    if (bTakaUsedInNewChallan)
                    {
                        lAvailableTaka = (long)(billData.taka - lOtherVoucherTaka - lTempTkas);
                        dAvailableMtrs = (double)(billData.mtrs - dOtherVoucherMtrs - dTempMtrs);
                    }
                    else
                    {
                        lAvailableTaka = (long)billData.taka - Convert.ToInt64(usedTakaDetails[0]);
                        dAvailableMtrs = (double)billData.mtrs - usedTakaDetails[1];
                    }
                }
                else
                {
                    if (challanDetails.Count > 0)
                    {
                        for (int j = 0; j < challanDetails.Count; j++)
                        {
                            lOtherVoucherTaka = (long)(lOtherVoucherTaka + challanDetails[j].totalTakas);
                            dOtherVoucherMtrs = (double)(dOtherVoucherMtrs + challanDetails[j].totalMtrs);
                        }

                    }
                    lAvailableTaka = (long)billData.taka - lOtherVoucherTaka;
                    dAvailableMtrs = (double)billData.mtrs - dOtherVoucherMtrs;

                    //lAvailableTaka = (long)billData.taka - Convert.ToInt64(usedTakaDetails[0]);
                    //dAvailableMtrs = (double)billData.mtrs - usedTakaDetails[1];
                }
                
                avlTakaTextBox.Text = Convert.ToString(lAvailableTaka);
                avlMtrsTextBox.Text = Convert.ToString(dAvailableMtrs);
            }
            else
            {
                avlTakaTextBox.Text = Convert.ToString(billData.taka);
                avlMtrsTextBox.Text = Convert.ToString(billData.mtrs);
            }

            lQualityID = (long)billData.quality;


        }

        private void updateChallanDataInList(List<milldispatchchallan_detail> challanDataList)
        {
            jobListView.Items.Clear();
            for (int i = 0;i < challanDataList.Count;i++)
            {
                if(!challanDataList[i].isDeleted)
                {
                    var millDetail = DB.getMillDetails((long)challanDataList[i].mill_id);
                    if (millDetail == null)
                    {
                        MessageBox.Show("Unable to fetch Mill Data", "MILL", MessageBoxButtons.OK);
                        return;

                    }
                    challanDataList[i].mill_gstn = millDetail.gst;
                    challanDataList[i].processType = Convert.ToInt64(jobTypeComboBox.SelectedValue);
                    challanDataList[i].station_id = Convert.ToInt64(millDetail.city_id);


                    string[] arr = new string[10];
                    if((long)jobTypeComboBox.SelectedValue == 1)
                        arr[0] = Convert.ToString(challanDataList[i].reference_billNo);
                    else
                        arr[0] = Convert.ToString(challanDataList[i].reference_challanNo);
                    arr[1] = Convert.ToString(challanDataList[i].challanNo);
                    arr[2] = millDetail.party_name;
                    arr[3] = Convert.ToString(challanDataList[i].totalTakas);
                    arr[4] = Convert.ToString(challanDataList[i].totalMtrs);
                    arr[5] = Convert.ToString(challanDataList[i].challanNo);
                    arr[6] = dateTextBox.Text;
                    arr[7] = Convert.ToString(challanDataList[i].rate);
                    arr[8] = jobTypeComboBox.Text;
                    arr[9] = challanDataList[i].remark;

                    ListViewItem itm = new ListViewItem(arr);
                    jobListView.Items.Add(itm);

                }
            }
            double dVoucherMtrs = 0;
            long lVoucherTaka = 0;
            for (int i = 0;i < jobListView.Items.Count;i++)
            {
                lVoucherTaka = lVoucherTaka + Convert.ToInt64(jobListView.Items[i].SubItems[3].Text);
                dVoucherMtrs = dVoucherMtrs + Convert.ToDouble(jobListView.Items[i].SubItems[4].Text);
            }
            totalMtrsTextBox.Text = Convert.ToString(dVoucherMtrs);
            totalTakaTextBox.Text = Convert.ToString(lVoucherTaka);
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            if(jobListView.Items.Count <= 0)
            {
                MessageBox.Show("No Challans created...", "ERROR", MessageBoxButtons.OK);
                return;

            }
            milldispatch_detail dispatchChallan = new milldispatch_detail();
            dispatchChallan.date = DateTime.ParseExact(dateTextBox.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            
            dispatchChallan.totalMtrs = Convert.ToDouble(totalMtrsTextBox.Text);
            dispatchChallan.totalTaka = Convert.ToInt64(totalTakaTextBox.Text);
            dispatchChallan.voucherNo = Convert.ToInt64(voucherNoTextBox.Text);
            dispatchChallan.milldispatchchallan_detail = millDispatchChallanDetail;
            bool bMillChallanAdded = partyDB.addMillDispatchChallan(dispatchChallan);
            if(bMillChallanAdded)
            {
                MessageBox.Show("Mill Voucher Added","Confirmation",MessageBoxButtons.OK);
                resetAllData();
                setDefaultData();
                millDispatchChallanDetail.Clear();
                millChallanDispatchTakaDetail.Clear();
            }
            else
            {
                MessageBox.Show("Mill Voucher not Added", "ERROR", MessageBoxButtons.OK);
            }
            
        }

        private long getVirtualChallanNo()
        {
            long lSrNo = Convert.ToInt64(partyDB.getMillDispatchChallanNo());
            if (lSrNo == -1)
            {
                return -1;
            }
            else if (lSrNo == 0)
            {
                return (long)(jobListView.Items.Count + 1);
            }
            else
            {
                return (findMaxChallanNoFromJobList((long)(lSrNo + 1)));
            }
                
        }

        private long findMaxChallanNoFromJobList(long lVirtualChallanNo)
        {
            if(jobListView.Items.Count > 0)
            {
                long lMaxChallanNoInLIst = 0;
                for (int i = 0;i < jobListView.Items.Count;i++)
                {
                    long lChallanNo = 0;
                    long.TryParse(jobListView.Items[i].SubItems[1].Text,out lChallanNo);
                    if (lChallanNo > lMaxChallanNoInLIst)
                        lMaxChallanNoInLIst = lChallanNo;
                }
                if (lMaxChallanNoInLIst >= lVirtualChallanNo)
                    return (lMaxChallanNoInLIst + 1);
                else
                    return lVirtualChallanNo;
            }
            else
                return lVirtualChallanNo;
        }

        private void resetAllData()
        {
            refBillNoTextBox.Text = "";
            recdTakaTextBox.Text = "";
            recdMtrsTextBox.Text = "";
            weaverTextBox.Text = "";
            greyQualityTextBox.Text = "";
            wtTextBox.Text = "";
            avlMtrsTextBox.Text = "0.0";
            avlTakaTextBox.Text = "0";
            totalMtrsTextBox.Text = "0.0";
            totalTakaTextBox.Text = "0";
            jobListView.Items.Clear();

        }

        private void viewButton_Click(object sender, EventArgs e)
        {
            if(jobListView.Items.Count > 0)
            {
                if(jobListView.SelectedItems.Count == 1)
                {
                    int iSelectedIndex = -1;
                    int iListSelectedIndex = -1;
                    for(int i = 0;i < jobListView.Items.Count;i++)
                    {
                        if (jobListView.Items[i].Selected)
                            iListSelectedIndex = i;
                    }
                    if(iListSelectedIndex == -1)
                    {
                        MessageBox.Show("Please Select a challan from list", "SELECT", MessageBoxButtons.OK);
                        return;
                    }

                    string challanNo = jobListView.Items[iListSelectedIndex].SubItems[1].Text;
                    for(int i= 0;i < millDispatchChallanDetail.Count;i++)
                    {
                        if (millDispatchChallanDetail[i].challanNo == challanNo)
                            iSelectedIndex = i;
                    }
                    if (iSelectedIndex == -1)
                    {
                        MessageBox.Show("Unable to fetch data of challan", "ERROR", MessageBoxButtons.OK);
                        return;
                    }

                    MillChallanWindow mcw = new MillChallanWindow();
                    mcw.setChallanDataInControl(millDispatchChallanDetail[iSelectedIndex], millChallanDispatchTakaDetail[iSelectedIndex]);
                    mcw.ShowDialog();


                    if (mcw.takaList.Count > 0)
                    {
                        //milldispatchchallan_detail challanDetail = new milldispatchchallan_detail();
                        millDispatchChallanDetail[iSelectedIndex].challanNo = mcw.getChallanNo();
                        millDispatchChallanDetail[iSelectedIndex].master = mcw.getMasterName();
                        millDispatchChallanDetail[iSelectedIndex].mill_id = mcw.getMillID();

                        millDispatchChallanDetail[iSelectedIndex].voucherNo = Convert.ToInt64(voucherNoTextBox.Text);
                        millDispatchChallanDetail[iSelectedIndex].totalTakas = mcw.takaList.Count;
                        double dTotalMtrs = 0;
                        for (int i = 0; i < mcw.takaList.Count; i++)
                        {
                            double dTakaMtrs = 0;
                            string greyTakaMtrs = mcw.takaList[i];
                            var greyTakaMtrsAfterSplit = greyTakaMtrs.Split('+');
                            for (int j = 0; j < greyTakaMtrsAfterSplit.Count(); j++)
                            {
                                double dTempGreyMtrs = 0;
                                //double.TryParse(millReceivedTakaDataGridView.Rows[i].Cells[finMtrsGridTextBox.Index].EditedFormattedValue.ToString(),out dTempFinMtrs);
                                double.TryParse(greyTakaMtrsAfterSplit[j], out dTempGreyMtrs);
                                dTakaMtrs = dTakaMtrs + dTempGreyMtrs;
                            }
                            dTotalMtrs = dTotalMtrs + dTakaMtrs;
                        }
                        millDispatchChallanDetail[iSelectedIndex].totalMtrs = dTotalMtrs;
                        millDispatchChallanDetail[iSelectedIndex].cardNo = mcw.getChallanNo();
                        millDispatchChallanDetail[iSelectedIndex].rate = mcw.getJobRate();
                        millDispatchChallanDetail[iSelectedIndex].remark = mcw.getRemarkData();
                        millDispatchChallanDetail[iSelectedIndex].date = DateTime.ParseExact(mcw.getDate(), "dd-MM-yyyy", CultureInfo.InvariantCulture);

                        /*if ((long)(jobTypeComboBox.SelectedValue) == 1)
                            millDispatchChallanDetail[iSelectedIndex].reference_billNo = Convert.ToInt64(refBillNoTextBox.Text);
                        else
                        {
                            millDispatchChallanDetail[iSelectedIndex].reference_challanNo = Convert.ToInt64(refBillNoTextBox.Text);
                        }*/
                        //millDispatchChallanDetail[iSelectedIndex].quality_id = lQualityID;
                        for (int i = 0; i < mcw.takaList.Count; i++)
                        {
                            List<millchallandispatchtaka_detail> takaList = millChallanDispatchTakaDetail[iSelectedIndex];
                            takaList[i].greyMtrs = mcw.takaList[i];
                            takaList[i].isReceived = false;
                            //challanDetail.millchallandispatchtaka_detail.Add(takaList);
                        }
                        //challanDetailList.Add(challanDetail);
                        updateChallanDataInList(millDispatchChallanDetail);
                        //avlMtrsTextBox.Text = Convert.ToString(Convert.ToDouble(avlMtrsTextBox.Text) - dTotalMtrs);
                        //avlTakaTextBox.Text = Convert.ToString(Convert.ToInt64(avlTakaTextBox.Text) - millDispatchChallanDetail[iSelectedIndex].totalTakas);
                    }

                }
                else
                {
                    MessageBox.Show("Please Select a challan from list", "SELECT", MessageBoxButtons.OK);
                    return;
                }
                //viewButton.Enabled = true;
                //printButton.Enabled = true;
                //deleteButton.Enabled = true;
                //updateButton.Enabled = true;
                //viewVoucherButton.Enabled = true;
                //deleteVoucherButton.Enabled = true;
                //createButton.Enabled = false;
                //saveButton.Enabled = false;
            }
            else
            {
                MessageBox.Show("No Challan in list to display", "NO DATA", MessageBoxButtons.OK);
                return;
            }
            

        }

        private void viewVoucherButton_Click(object sender, EventArgs e)
        {
            long lVoucherNo = 0;
            bool bValidVoucherNo = long.TryParse(voucherNoTextBox.Text, out lVoucherNo);
            if (!bValidVoucherNo)
            {
                MessageBox.Show("Invalid VoucherNo.", "Error", MessageBoxButtons.OK);
                voucherNoTextBox.Focus();
                return;
            }
            try
            {
                var db = DBHelper2.GetDbContext(Properties.Settings.Default.FinancialYearDbName);

                millDispatchDetail = db.milldispatch_detail.Where(x => x.voucherNo == lVoucherNo).SingleOrDefault();

                if (millDispatchDetail == null)
                {
                    MessageBox.Show("Voucher does not exist", "Error", MessageBoxButtons.OK);
                    voucherNoTextBox.Focus();
                    return;
                }
                millDispatchChallanDetail.Clear();
                millChallanDispatchTakaDetail.Clear();
                DateTime dt, dt1;
                bool bFlag = DateTime.TryParse(millDispatchDetail.date.ToString(), out dt);
                if (bFlag)
                {
                    dt1 = dt.Date;
                    //DateTime.Parse(dt1.ToString()).ToString("dd-MM-yyyy");
                    dateTextBox.Text = dt1.ToString("dd-MM-yyyy");
                }
                
                millDispatchChallanDetail = millDispatchDetail.milldispatchchallan_detail.ToList();
                jobTypeComboBox.SelectedValue = millDispatchChallanDetail[0].processType;
                updateChallanDataInList(millDispatchChallanDetail);
                for(int i = 0;i < millDispatchChallanDetail.Count;i++)
                {
                    List<millchallandispatchtaka_detail> tempTakaList = new List<millchallandispatchtaka_detail>();
                    tempTakaList = millDispatchChallanDetail[i].millchallandispatchtaka_detail.ToList();
                    millChallanDispatchTakaDetail.Add(tempTakaList);
                    
                }
                db.Dispose();
                viewButton.Enabled = true;
                //printButton.Enabled = true;
                //deleteButton.Enabled = true;
                //updateButton.Enabled = true;
                viewVoucherButton.Enabled = true;
                deleteVoucherButton.Enabled = true;
                //createButton.Enabled = false;
                saveButton.Enabled = false;
                refBillNoTextBox.Text = "";
                recdTakaTextBox.Text = "";
                recdMtrsTextBox.Text = "";
                weaverTextBox.Text = "";
                greyQualityTextBox.Text = "";
                wtTextBox.Text = "";
                avlMtrsTextBox.Text = "0.0";
                avlTakaTextBox.Text = "0";
            }
            catch
            {
                return;
            }
            
        }

        private void updateButton_Click(object sender, EventArgs e)
        {
            if (jobListView.Items.Count <= 0)
            {
                MessageBox.Show("No Challans created...", "ERROR", MessageBoxButtons.OK);
                return;

            }
            //milldispatch_detail dispatchChallan = new milldispatch_detail();
            millDispatchDetail.date = DateTime.ParseExact(dateTextBox.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);

            millDispatchDetail.totalMtrs = Convert.ToDouble(totalMtrsTextBox.Text);
            millDispatchDetail.totalTaka = Convert.ToInt64(totalTakaTextBox.Text);
            millDispatchDetail.voucherNo = Convert.ToInt64(voucherNoTextBox.Text);
            millDispatchDetail.milldispatchchallan_detail = millDispatchChallanDetail;
            
            long lVoucherNo = Convert.ToInt64(voucherNoTextBox.Text);
            /*try
            {
                
                var db = DBHelper2.GetDbContext(Properties.Settings.Default.FinancialYearDbName);
                List<milldispatchchallan_detail> oldChallanList = db.milldispatchchallan_detail.Where(x => x.voucherNo == lVoucherNo).ToList();
                if(oldChallanList == null)
                {
                    MessageBox.Show("Unable to update Voucher","Error",MessageBoxButtons.OK);
                    return;
                }
                if(oldChallanList.Count > millDispatchChallanDetail.Count)
                {
                    
                    for(int i = 0;i < oldChallanList.Count;i++)
                    {
                        bool bChallanFound = false;
                        for (int j= 0;j < millDispatchChallanDetail.Count;j++)
                        {
                            if(millDispatchChallanDetail[i].challanNo == oldChallanList[i].challanNo)
                            {
                                bChallanFound = true;
                            }
                        }
                        if(!bChallanFound)
                        {
                            long lDeleteChallanNo = oldChallanList[i].challanNo;
                            milldispatchchallan_detail deleteChallanDetail =  db.milldispatchchallan_detail.Where(x => x.challanNo == lDeleteChallanNo).SingleOrDefault();
                            db.milldispatchchallan_detail.Remove(deleteChallanDetail);
                        }
                    }
                }

                db.milldispatch_detail.Add(millDispatchDetail);
                
                db.Entry(millDispatchDetail).State = EntityState.Modified;
                foreach (milldispatchchallan_detail sd in millDispatchDetail.milldispatchchallan_detail)
                {
                    db.Entry(sd).State = sd.challan_id == 0 ? EntityState.Added : EntityState.Modified;

                    foreach (millchallandispatchtaka_detail td in sd.millchallandispatchtaka_detail)
                    {
                        db.Entry(td).State = td.millTaka_id == 0 ? EntityState.Added : EntityState.Modified;
                    }

                }

                db.SaveChanges();
                db.Dispose();
            //bool bMillChallanAdded = partyDB.updateMillDispatchVoucher(millDispatchDetail);

            MessageBox.Show("Mill Voucher Updated", "Confirmation", MessageBoxButtons.OK);
                resetAllData();
                setDefaultData();
            
        }
        catch(Exception ex)
        {
            MessageBox.Show("Mill Voucher not updated", "ERROR", MessageBoxButtons.OK);
        }
            */

            bool bMillChallanAdded = partyDB.updateMillDispatchVoucher(millDispatchDetail);
            if(bMillChallanAdded)
            {
                MessageBox.Show("Mill Voucher Updated", "Confirmation", MessageBoxButtons.OK);
                resetAllData();
                setDefaultData();
            }
            else
                MessageBox.Show("Mill Voucher not updated", "ERROR", MessageBoxButtons.OK);
        }

        private void voucherNoTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                long lVoucherNo = 0;
                bool bValidVoucherNo = long.TryParse(voucherNoTextBox.Text, out lVoucherNo);
                if (!bValidVoucherNo)
                {
                    MessageBox.Show("Invalid VoucherNo.", "Error", MessageBoxButtons.OK);
                    voucherNoTextBox.Focus();
                    return;
                }
                try
                {
                    var db = DBHelper2.GetDbContext(Properties.Settings.Default.FinancialYearDbName);

                    millDispatchDetail = db.milldispatch_detail.Where(x => x.voucherNo == lVoucherNo).SingleOrDefault();

                    if (millDispatchDetail == null)
                    {
                        MessageBox.Show("Voucher does not exist", "Error", MessageBoxButtons.OK);

                        DialogResult result = MessageBox.Show("Do you want to add new voucher", "Confirm", MessageBoxButtons.YesNo);
                        if (result == DialogResult.Yes)
                        {
                            resetAllData();
                            setDefaultData();
                            millChallanDispatchTakaDetail.Clear();
                            millDispatchChallanDetail.Clear();
                        }
                        else
                            voucherNoTextBox.Focus();
                        //voucherNoTextBox.Focus();
                        return;
                    }
                    millDispatchChallanDetail.Clear();
                    millChallanDispatchTakaDetail.Clear();

                    DateTime dt, dt1;
                    bool bFlag = DateTime.TryParse(millDispatchDetail.date.ToString(), out dt);
                    if (bFlag)
                    {
                        dt1 = dt.Date;
                        //DateTime.Parse(dt1.ToString()).ToString("dd-MM-yyyy");
                        dateTextBox.Text = dt1.ToString("dd-MM-yyyy");
                    }
                    millDispatchChallanDetail = millDispatchDetail.milldispatchchallan_detail.ToList();
                    jobTypeComboBox.SelectedValue = millDispatchChallanDetail[0].processType;
                    updateChallanDataInList(millDispatchChallanDetail);
                    for (int i = 0; i < millDispatchChallanDetail.Count; i++)
                    {
                        List<millchallandispatchtaka_detail> tempTakaList = new List<millchallandispatchtaka_detail>();
                        tempTakaList = millDispatchChallanDetail[i].millchallandispatchtaka_detail.ToList();
                        millChallanDispatchTakaDetail.Add(tempTakaList);

                    }
                    db.Dispose();
                    //viewButton.Enabled = true;
                    //printButton.Enabled = true;
                    //deleteButton.Enabled = true;
                    updateButton.Enabled = true;
                    viewVoucherButton.Enabled = true;
                    deleteVoucherButton.Enabled = true;
                    //createButton.Enabled = false;
                    saveButton.Enabled = false;
                    refBillNoTextBox.Text = "";
                    recdTakaTextBox.Text = "";
                    recdMtrsTextBox.Text = "";
                    weaverTextBox.Text = "";
                    greyQualityTextBox.Text = "";
                    wtTextBox.Text = "";
                    avlMtrsTextBox.Text = "0.0";
                    avlTakaTextBox.Text = "0";
                }
                catch
                {
                    return;
                }
            }
            
        }

        private void deleteVoucherButton_Click(object sender, EventArgs e)
        {
            long lVoucherNo = 0;
            bool bValidVoucherNo = long.TryParse(voucherNoTextBox.Text, out lVoucherNo);
            if (!bValidVoucherNo)
            {
                MessageBox.Show("Invalid VoucherNo.", "Error", MessageBoxButtons.OK);
                voucherNoTextBox.Focus();
                return;
            }
            DialogResult result = MessageBox.Show("Do you want to delete voucher?", "DELETE", MessageBoxButtons.YesNo);
            if (result == DialogResult.No)
                return;

            
            //first check whethet the challan taka is received or not
            bool bChallanTakaReceived = false;
            for (int i = 0; i < millDispatchChallanDetail.Count; i++)
            {
                List<millchallandispatchtaka_detail> tempTakaDetail = millChallanDispatchTakaDetail[i];
                for (int j = 0; j < tempTakaDetail.Count; j++)
                {
                    if (tempTakaDetail[j].isReceived == true)
                    {
                        bChallanTakaReceived = true;
                        break;
                    }
                }
                if (bChallanTakaReceived)
                {
                    MessageBox.Show("Challan cannot be deleted some or few taka of challan already received", "ERROR", MessageBoxButtons.OK);
                    return;
                }
                
            }

            bValidVoucherNo = partyDB.deleteMillDispatchChallan(lVoucherNo);
            if (bValidVoucherNo)
            {
                MessageBox.Show("Voucher deleted", "Voucher", MessageBoxButtons.OK);
                millDispatchChallanDetail.Clear();
                millChallanDispatchTakaDetail.Clear();
                voucherNoTextBox.Focus();
                resetAllData();
                setDefaultData();
            }
            else
            {
                MessageBox.Show("Voucher not deleted", "Voucher", MessageBoxButtons.OK);
                voucherNoTextBox.Focus();
                return;
            }
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            if (jobListView.Items.Count > 0)
            {
                if (jobListView.SelectedItems.Count == 1)
                {
                    int iSelectedIndex = -1;
                    int iListSelectedIndex = -1;
                    for (int i = 0; i < jobListView.Items.Count; i++)
                    {
                        if (jobListView.Items[i].Selected)
                            iListSelectedIndex = i;
                    }
                    
                    if (iListSelectedIndex == -1)
                    {
                        MessageBox.Show("Please Select a challan from list", "SELECT", MessageBoxButtons.OK);
                        return;
                    }

                    
                    DialogResult result = MessageBox.Show("Do you want to delete challan?", "DELETE", MessageBoxButtons.YesNo);
                    if (result == DialogResult.No)
                        return;

                    string challanNo = jobListView.Items[iListSelectedIndex].SubItems[1].Text;
                    //first check whethet the challan taka is received or not
                    bool bChallanTakaReceived = false;
                    for (int i = 0; i < millDispatchChallanDetail.Count; i++)
                    {
                        if (millDispatchChallanDetail[i].challanNo == challanNo)
                        {
                            List<millchallandispatchtaka_detail> tempTakaDetail = millChallanDispatchTakaDetail[i];
                            for (int j = 0; j < tempTakaDetail.Count; j++)
                            {
                                if(tempTakaDetail[j].isReceived == true)
                                {
                                    bChallanTakaReceived = true;
                                    break;
                                }
                            }
                            if(bChallanTakaReceived)
                            {
                                MessageBox.Show("Challan cannot be deleted some or few taka of challan already received", "ERROR", MessageBoxButtons.OK);
                                return;
                            }
                        }

                    }
                    //if no taka is received in challan go ahead with the delete challan
                    for (int i = 0; i < millDispatchChallanDetail.Count; i++)
                    {
                        if (millDispatchChallanDetail[i].challanNo == challanNo)
                        {
                            iSelectedIndex = i;
                            millDispatchChallanDetail[i].isDeleted = true;
                            List<millchallandispatchtaka_detail> tempTakaDetail = millChallanDispatchTakaDetail[i];
                            for (int j = 0; j < tempTakaDetail.Count; j++)
                                tempTakaDetail[j].isDeleted = true;
                        }

                    }
                    if (iSelectedIndex == -1)
                    {
                        MessageBox.Show("Unable to fetch data", "ERROR", MessageBoxButtons.OK);
                        return;
                    }
                    //millDispatchChallanDetail.RemoveAt(iSelectedIndex);
                    //millChallanDispatchTakaDetail.RemoveAt(iSelectedIndex);

                    updateChallanDataInList(millDispatchChallanDetail);
                    //avlMtrsTextBox.Text = Convert.ToString(Convert.ToDouble(avlMtrsTextBox.Text) - dTotalMtrs);
                    //avlTakaTextBox.Text = Convert.ToString(Convert.ToInt64(avlTakaTextBox.Text) - millDispatchChallanDetail[iSelectedIndex].totalTakas);
                    

                }
                else
                {
                    MessageBox.Show("Please Select a challan from list", "SELECT", MessageBoxButtons.OK);
                    return;
                }
                refBillNoTextBox.Text = "";
                recdTakaTextBox.Text = "";
                recdMtrsTextBox.Text = "";
                weaverTextBox.Text = "";
                greyQualityTextBox.Text = "";
                wtTextBox.Text = "";
                avlMtrsTextBox.Text = "0.0";
                avlTakaTextBox.Text = "0";

                //viewButton.Enabled = true;
                //printButton.Enabled = true;
                //deleteButton.Enabled = true;
                //updateButton.Enabled = true;
                //viewVoucherButton.Enabled = true;
                //deleteVoucherButton.Enabled = true;
                //createButton.Enabled = false;
                //saveButton.Enabled = false;
            }
            else
            {
                MessageBox.Show("No Challan in list to display", "NO DATA", MessageBoxButtons.OK);
                return;
            }

        }

        private void jobTypeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if((long)jobTypeComboBox.SelectedValue == 2 || (long)jobTypeComboBox.SelectedValue == 3)
            {
                label5.Text = "REF.CH.NO.:";
            }
            else
                label5.Text = "REF BILL NO.:";
        }
    }
}
