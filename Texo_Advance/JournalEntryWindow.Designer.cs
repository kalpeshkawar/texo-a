﻿namespace Texo_Advance
{
    partial class JournalEntryWindow
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(JournalEntryWindow));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.companyNameTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.voucherNoTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.dateTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.creditAccCurBalTextBox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.debitAccCurBalTextBox = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.amountTextBox = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.chqRefNoTextBox = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.creditRemarksTextBox = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.debitRemarksTextBox = new System.Windows.Forms.TextBox();
            this.addNewButton = new System.Windows.Forms.Button();
            this.updateButton = new System.Windows.Forms.Button();
            this.deleteButton = new System.Windows.Forms.Button();
            this.closeButton = new System.Windows.Forms.Button();
            this.viewButton = new System.Windows.Forms.Button();
            this.creditAccountComboBox = new JTG.ColumnComboBox();
            this.debitAccountComboBox = new JTG.ColumnComboBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label1.Location = new System.Drawing.Point(365, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(175, 22);
            this.label1.TabIndex = 0;
            this.label1.Text = "JOURNAL ENTRIES";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Blue;
            this.label2.Location = new System.Drawing.Point(38, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "COMPANY:";
            // 
            // companyNameTextBox
            // 
            this.companyNameTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.companyNameTextBox.Location = new System.Drawing.Point(156, 62);
            this.companyNameTextBox.Name = "companyNameTextBox";
            this.companyNameTextBox.ReadOnly = true;
            this.companyNameTextBox.Size = new System.Drawing.Size(246, 22);
            this.companyNameTextBox.TabIndex = 2;
            this.companyNameTextBox.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Blue;
            this.label3.Location = new System.Drawing.Point(408, 64);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(116, 16);
            this.label3.TabIndex = 1;
            this.label3.Text = "VOUCHER NO.:";
            // 
            // voucherNoTextBox
            // 
            this.voucherNoTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.voucherNoTextBox.Location = new System.Drawing.Point(534, 61);
            this.voucherNoTextBox.Name = "voucherNoTextBox";
            this.voucherNoTextBox.Size = new System.Drawing.Size(80, 22);
            this.voucherNoTextBox.TabIndex = 0;
            this.voucherNoTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.voucherNoTextBox_KeyDown);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Blue;
            this.label4.Location = new System.Drawing.Point(619, 62);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 16);
            this.label4.TabIndex = 1;
            this.label4.Text = "DATE:";
            // 
            // dateTextBox
            // 
            this.dateTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTextBox.Location = new System.Drawing.Point(674, 59);
            this.dateTextBox.Name = "dateTextBox";
            this.dateTextBox.Size = new System.Drawing.Size(98, 22);
            this.dateTextBox.TabIndex = 1;
            this.dateTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.dateTextBox_Validating);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Blue;
            this.label5.Location = new System.Drawing.Point(410, 100);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(86, 16);
            this.label5.TabIndex = 1;
            this.label5.Text = "DEBIT A/C:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Blue;
            this.label6.Location = new System.Drawing.Point(39, 100);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(97, 16);
            this.label6.TabIndex = 1;
            this.label6.Text = "CREDIT A/C:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Blue;
            this.label7.Location = new System.Drawing.Point(38, 137);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(121, 16);
            this.label7.TabIndex = 1;
            this.label7.Text = "CUR. BALANCE:";
            // 
            // creditAccCurBalTextBox
            // 
            this.creditAccCurBalTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.creditAccCurBalTextBox.Location = new System.Drawing.Point(156, 136);
            this.creditAccCurBalTextBox.Name = "creditAccCurBalTextBox";
            this.creditAccCurBalTextBox.ReadOnly = true;
            this.creditAccCurBalTextBox.Size = new System.Drawing.Size(180, 22);
            this.creditAccCurBalTextBox.TabIndex = 4;
            this.creditAccCurBalTextBox.TabStop = false;
            this.creditAccCurBalTextBox.Text = "0.0";
            this.creditAccCurBalTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Blue;
            this.label8.Location = new System.Drawing.Point(410, 138);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(121, 16);
            this.label8.TabIndex = 1;
            this.label8.Text = "CUR. BALANCE:";
            // 
            // debitAccCurBalTextBox
            // 
            this.debitAccCurBalTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.debitAccCurBalTextBox.Location = new System.Drawing.Point(536, 137);
            this.debitAccCurBalTextBox.Name = "debitAccCurBalTextBox";
            this.debitAccCurBalTextBox.ReadOnly = true;
            this.debitAccCurBalTextBox.Size = new System.Drawing.Size(180, 22);
            this.debitAccCurBalTextBox.TabIndex = 5;
            this.debitAccCurBalTextBox.TabStop = false;
            this.debitAccCurBalTextBox.Text = "0.0";
            this.debitAccCurBalTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Blue;
            this.label9.Location = new System.Drawing.Point(38, 175);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(77, 16);
            this.label9.TabIndex = 1;
            this.label9.Text = "AMOUNT:";
            // 
            // amountTextBox
            // 
            this.amountTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.amountTextBox.Location = new System.Drawing.Point(156, 174);
            this.amountTextBox.Name = "amountTextBox";
            this.amountTextBox.Size = new System.Drawing.Size(180, 22);
            this.amountTextBox.TabIndex = 6;
            this.amountTextBox.Text = "0";
            this.amountTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Blue;
            this.label10.Location = new System.Drawing.Point(411, 176);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(109, 16);
            this.label10.TabIndex = 1;
            this.label10.Text = "CHQ/REF.NO.:";
            // 
            // chqRefNoTextBox
            // 
            this.chqRefNoTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chqRefNoTextBox.Location = new System.Drawing.Point(535, 175);
            this.chqRefNoTextBox.Name = "chqRefNoTextBox";
            this.chqRefNoTextBox.Size = new System.Drawing.Size(180, 22);
            this.chqRefNoTextBox.TabIndex = 7;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Blue;
            this.label11.Location = new System.Drawing.Point(38, 213);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(83, 16);
            this.label11.TabIndex = 1;
            this.label11.Text = "CR. RMKS:";
            // 
            // creditRemarksTextBox
            // 
            this.creditRemarksTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.creditRemarksTextBox.Location = new System.Drawing.Point(158, 212);
            this.creditRemarksTextBox.Multiline = true;
            this.creditRemarksTextBox.Name = "creditRemarksTextBox";
            this.creditRemarksTextBox.Size = new System.Drawing.Size(244, 65);
            this.creditRemarksTextBox.TabIndex = 8;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Blue;
            this.label12.Location = new System.Drawing.Point(412, 214);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(84, 16);
            this.label12.TabIndex = 1;
            this.label12.Text = "DR. RMKS:";
            // 
            // debitRemarksTextBox
            // 
            this.debitRemarksTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.debitRemarksTextBox.Location = new System.Drawing.Point(537, 213);
            this.debitRemarksTextBox.Multiline = true;
            this.debitRemarksTextBox.Name = "debitRemarksTextBox";
            this.debitRemarksTextBox.Size = new System.Drawing.Size(239, 65);
            this.debitRemarksTextBox.TabIndex = 9;
            // 
            // addNewButton
            // 
            this.addNewButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addNewButton.ForeColor = System.Drawing.Color.Red;
            this.addNewButton.Location = new System.Drawing.Point(132, 335);
            this.addNewButton.Name = "addNewButton";
            this.addNewButton.Size = new System.Drawing.Size(97, 36);
            this.addNewButton.TabIndex = 10;
            this.addNewButton.Text = "ADD NEW";
            this.addNewButton.UseVisualStyleBackColor = true;
            this.addNewButton.Click += new System.EventHandler(this.addNewButton_Click);
            // 
            // updateButton
            // 
            this.updateButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.updateButton.ForeColor = System.Drawing.Color.Red;
            this.updateButton.Location = new System.Drawing.Point(376, 335);
            this.updateButton.Name = "updateButton";
            this.updateButton.Size = new System.Drawing.Size(97, 36);
            this.updateButton.TabIndex = 12;
            this.updateButton.Text = "UPDATE";
            this.updateButton.UseVisualStyleBackColor = true;
            this.updateButton.Click += new System.EventHandler(this.updateButton_Click);
            // 
            // deleteButton
            // 
            this.deleteButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deleteButton.ForeColor = System.Drawing.Color.Red;
            this.deleteButton.Location = new System.Drawing.Point(494, 335);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(97, 36);
            this.deleteButton.TabIndex = 13;
            this.deleteButton.Text = "DELETE";
            this.deleteButton.UseVisualStyleBackColor = true;
            this.deleteButton.Click += new System.EventHandler(this.deleteButton_Click);
            // 
            // closeButton
            // 
            this.closeButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.closeButton.ForeColor = System.Drawing.Color.Red;
            this.closeButton.Location = new System.Drawing.Point(610, 335);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(97, 36);
            this.closeButton.TabIndex = 14;
            this.closeButton.Text = "CLOSE";
            this.closeButton.UseVisualStyleBackColor = true;
            this.closeButton.Click += new System.EventHandler(this.closeButton_Click);
            // 
            // viewButton
            // 
            this.viewButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.viewButton.ForeColor = System.Drawing.Color.Red;
            this.viewButton.Location = new System.Drawing.Point(257, 335);
            this.viewButton.Name = "viewButton";
            this.viewButton.Size = new System.Drawing.Size(97, 36);
            this.viewButton.TabIndex = 11;
            this.viewButton.Text = "VIEW";
            this.viewButton.UseVisualStyleBackColor = true;
            this.viewButton.Click += new System.EventHandler(this.viewButton_Click);
            // 
            // creditAccountComboBox
            // 
            this.creditAccountComboBox.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.creditAccountComboBox.DropDownHeight = 125;
            this.creditAccountComboBox.DropDownWidth = 17;
            this.creditAccountComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.creditAccountComboBox.FormattingEnabled = true;
            this.creditAccountComboBox.IntegralHeight = false;
            this.creditAccountComboBox.Location = new System.Drawing.Point(156, 98);
            this.creditAccountComboBox.MaxDropDownItems = 100;
            this.creditAccountComboBox.Name = "creditAccountComboBox";
            this.creditAccountComboBox.Size = new System.Drawing.Size(246, 22);
            this.creditAccountComboBox.TabIndex = 2;
            this.creditAccountComboBox.ViewColumn = 0;
            this.creditAccountComboBox.SelectedIndexChanged += new System.EventHandler(this.creditAccountComboBox_SelectedIndexChanged);
            this.creditAccountComboBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.creditAccountComboBox_KeyDown);
            // 
            // debitAccountComboBox
            // 
            this.debitAccountComboBox.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.debitAccountComboBox.DropDownHeight = 125;
            this.debitAccountComboBox.DropDownWidth = 17;
            this.debitAccountComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.debitAccountComboBox.FormattingEnabled = true;
            this.debitAccountComboBox.IntegralHeight = false;
            this.debitAccountComboBox.Location = new System.Drawing.Point(535, 98);
            this.debitAccountComboBox.MaxDropDownItems = 100;
            this.debitAccountComboBox.Name = "debitAccountComboBox";
            this.debitAccountComboBox.Size = new System.Drawing.Size(242, 22);
            this.debitAccountComboBox.TabIndex = 3;
            this.debitAccountComboBox.ViewColumn = 0;
            this.debitAccountComboBox.SelectedIndexChanged += new System.EventHandler(this.debitAccountComboBox_SelectedIndexChanged);
            this.debitAccountComboBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.debitAccountComboBox_KeyDown);
            // 
            // JournalEntryWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.closeButton);
            this.Controls.Add(this.deleteButton);
            this.Controls.Add(this.updateButton);
            this.Controls.Add(this.viewButton);
            this.Controls.Add(this.addNewButton);
            this.Controls.Add(this.creditAccountComboBox);
            this.Controls.Add(this.debitAccountComboBox);
            this.Controls.Add(this.dateTextBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.voucherNoTextBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.debitAccCurBalTextBox);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.chqRefNoTextBox);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.debitRemarksTextBox);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.creditRemarksTextBox);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.amountTextBox);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.creditAccCurBalTextBox);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.companyNameTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.ForeColor = System.Drawing.Color.Blue;
            this.Name = "JournalEntryWindow";
            this.Size = new System.Drawing.Size(1053, 447);
            this.Load += new System.EventHandler(this.JournalEntryWindow_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox companyNameTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox voucherNoTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox dateTextBox;
        private System.Windows.Forms.Label label5;
        private JTG.ColumnComboBox debitAccountComboBox;
        private System.Windows.Forms.Label label6;
        private JTG.ColumnComboBox creditAccountComboBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox creditAccCurBalTextBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox debitAccCurBalTextBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox amountTextBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox chqRefNoTextBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox creditRemarksTextBox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox debitRemarksTextBox;
        private System.Windows.Forms.Button addNewButton;
        private System.Windows.Forms.Button updateButton;
        private System.Windows.Forms.Button deleteButton;
        private System.Windows.Forms.Button closeButton;
        private System.Windows.Forms.Button viewButton;
    }
}
