﻿namespace Texo_Advance
{
    partial class CuttingReportWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.companyNameComboBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cuttcardNoTextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.dateTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.millNameTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.lotNoTextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.screenNameComboBox = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.weaverNameTextBox = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.qualityTextBox = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.wtTextBox = new System.Windows.Forms.TextBox();
            this.takaDeailsListView = new System.Windows.Forms.ListView();
            this.greyMtrs = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.finishMtrs = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.shortage = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.no = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label11 = new System.Windows.Forms.Label();
            this.totalSareesTextBox = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.secondTextBox = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label13 = new System.Windows.Forms.Label();
            this.secRateTextBox = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.outcutTextBox = new System.Windows.Forms.TextBox();
            this.outRateTextBox = new System.Windows.Forms.TextBox();
            this.freshSareesTextBox = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.fentTextBox = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.fentRateTextBox = new System.Windows.Forms.TextBox();
            this.chindiTextBox = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.chindiRateTextBox = new System.Windows.Forms.TextBox();
            this.greyRateTextBox = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.greyMtrsTextBox = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.greyTakaTextBox = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.greyDiscTextBox = new System.Windows.Forms.TextBox();
            this.jobRateTextBox = new System.Windows.Forms.TextBox();
            this.finMtrsTextBox = new System.Windows.Forms.TextBox();
            this.finTakaTextBox = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.jobPerTextBox = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label29 = new System.Windows.Forms.Label();
            this.basicCostingTextBox = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.avgShortageTextBox = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.greyAndMillDiscCheckBox = new System.Windows.Forms.CheckBox();
            this.wastageRateCheckBox = new System.Windows.Forms.CheckBox();
            this.actualCostingTextBox = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.avgWastageTextBox = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // companyNameComboBox
            // 
            this.companyNameComboBox.FormattingEnabled = true;
            this.companyNameComboBox.Location = new System.Drawing.Point(155, 38);
            this.companyNameComboBox.Name = "companyNameComboBox";
            this.companyNameComboBox.Size = new System.Drawing.Size(246, 21);
            this.companyNameComboBox.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(8, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(129, 16);
            this.label2.TabIndex = 3;
            this.label2.Text = "COMPANY NAME";
            // 
            // cuttcardNoTextBox
            // 
            this.cuttcardNoTextBox.Location = new System.Drawing.Point(581, 36);
            this.cuttcardNoTextBox.Name = "cuttcardNoTextBox";
            this.cuttcardNoTextBox.Size = new System.Drawing.Size(61, 20);
            this.cuttcardNoTextBox.TabIndex = 9;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(406, 37);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(155, 16);
            this.label7.TabIndex = 8;
            this.label7.Text = "CUTTING CARD NO.:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(648, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(130, 16);
            this.label1.TabIndex = 8;
            this.label1.Text = "RECD CARD NO.:";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(800, 36);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(62, 20);
            this.textBox1.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(867, 35);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 16);
            this.label4.TabIndex = 8;
            this.label4.Text = "DATE:";
            // 
            // dateTextBox
            // 
            this.dateTextBox.Location = new System.Drawing.Point(929, 36);
            this.dateTextBox.Name = "dateTextBox";
            this.dateTextBox.Size = new System.Drawing.Size(62, 20);
            this.dateTextBox.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(8, 76);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 16);
            this.label3.TabIndex = 3;
            this.label3.Text = "MILL NAME";
            // 
            // millNameTextBox
            // 
            this.millNameTextBox.Location = new System.Drawing.Point(155, 76);
            this.millNameTextBox.Name = "millNameTextBox";
            this.millNameTextBox.Size = new System.Drawing.Size(246, 20);
            this.millNameTextBox.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(407, 78);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 16);
            this.label5.TabIndex = 8;
            this.label5.Text = "LOT NO:";
            // 
            // lotNoTextBox
            // 
            this.lotNoTextBox.Location = new System.Drawing.Point(488, 77);
            this.lotNoTextBox.Name = "lotNoTextBox";
            this.lotNoTextBox.Size = new System.Drawing.Size(90, 20);
            this.lotNoTextBox.TabIndex = 9;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(596, 76);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(121, 16);
            this.label6.TabIndex = 8;
            this.label6.Text = "SCREEN NAME:";
            // 
            // screenNameComboBox
            // 
            this.screenNameComboBox.FormattingEnabled = true;
            this.screenNameComboBox.Location = new System.Drawing.Point(736, 75);
            this.screenNameComboBox.Name = "screenNameComboBox";
            this.screenNameComboBox.Size = new System.Drawing.Size(141, 21);
            this.screenNameComboBox.TabIndex = 4;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(8, 114);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(120, 16);
            this.label8.TabIndex = 3;
            this.label8.Text = "WEAVER NAME";
            // 
            // weaverNameTextBox
            // 
            this.weaverNameTextBox.Location = new System.Drawing.Point(155, 114);
            this.weaverNameTextBox.Name = "weaverNameTextBox";
            this.weaverNameTextBox.Size = new System.Drawing.Size(213, 20);
            this.weaverNameTextBox.TabIndex = 9;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(385, 112);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(72, 16);
            this.label9.TabIndex = 3;
            this.label9.Text = "QUALITY";
            // 
            // qualityTextBox
            // 
            this.qualityTextBox.Location = new System.Drawing.Point(469, 112);
            this.qualityTextBox.Name = "qualityTextBox";
            this.qualityTextBox.Size = new System.Drawing.Size(213, 20);
            this.qualityTextBox.TabIndex = 9;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(702, 111);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(36, 16);
            this.label10.TabIndex = 3;
            this.label10.Text = "WT.";
            // 
            // wtTextBox
            // 
            this.wtTextBox.Location = new System.Drawing.Point(755, 109);
            this.wtTextBox.Name = "wtTextBox";
            this.wtTextBox.Size = new System.Drawing.Size(65, 20);
            this.wtTextBox.TabIndex = 9;
            // 
            // takaDeailsListView
            // 
            this.takaDeailsListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.no,
            this.greyMtrs,
            this.finishMtrs,
            this.shortage});
            this.takaDeailsListView.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.takaDeailsListView.FullRowSelect = true;
            this.takaDeailsListView.HideSelection = false;
            this.takaDeailsListView.Location = new System.Drawing.Point(11, 152);
            this.takaDeailsListView.Name = "takaDeailsListView";
            this.takaDeailsListView.Size = new System.Drawing.Size(337, 312);
            this.takaDeailsListView.TabIndex = 10;
            this.takaDeailsListView.UseCompatibleStateImageBehavior = false;
            this.takaDeailsListView.View = System.Windows.Forms.View.Details;
            this.takaDeailsListView.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
            // 
            // greyMtrs
            // 
            this.greyMtrs.Text = "GREY MTRS";
            this.greyMtrs.Width = 100;
            // 
            // finishMtrs
            // 
            this.finishMtrs.Text = "FIN MTRS";
            this.finishMtrs.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.finishMtrs.Width = 100;
            // 
            // shortage
            // 
            this.shortage.Text = "SHORTAGE";
            this.shortage.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.shortage.Width = 100;
            // 
            // no
            // 
            this.no.Text = "NO";
            this.no.Width = 30;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(2, 30);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(73, 16);
            this.label11.TabIndex = 8;
            this.label11.Text = "SAREES:";
            // 
            // totalSareesTextBox
            // 
            this.totalSareesTextBox.Location = new System.Drawing.Point(83, 29);
            this.totalSareesTextBox.Name = "totalSareesTextBox";
            this.totalSareesTextBox.Size = new System.Drawing.Size(55, 22);
            this.totalSareesTextBox.TabIndex = 9;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(136, 29);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(75, 16);
            this.label12.TabIndex = 8;
            this.label12.Text = "SECOND:";
            // 
            // secondTextBox
            // 
            this.secondTextBox.Location = new System.Drawing.Point(218, 28);
            this.secondTextBox.Name = "secondTextBox";
            this.secondTextBox.Size = new System.Drawing.Size(56, 22);
            this.secondTextBox.TabIndex = 9;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.outRateTextBox);
            this.groupBox1.Controls.Add(this.outcutTextBox);
            this.groupBox1.Controls.Add(this.chindiRateTextBox);
            this.groupBox1.Controls.Add(this.fentRateTextBox);
            this.groupBox1.Controls.Add(this.secRateTextBox);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.secondTextBox);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.chindiTextBox);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.fentTextBox);
            this.groupBox1.Controls.Add(this.freshSareesTextBox);
            this.groupBox1.Controls.Add(this.totalSareesTextBox);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.groupBox1.Location = new System.Drawing.Point(354, 152);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(637, 102);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "INPUT PARAMETERS";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(272, 27);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(53, 16);
            this.label13.TabIndex = 8;
            this.label13.Text = "RATE:";
            // 
            // secRateTextBox
            // 
            this.secRateTextBox.Location = new System.Drawing.Point(325, 26);
            this.secRateTextBox.Name = "secRateTextBox";
            this.secRateTextBox.Size = new System.Drawing.Size(60, 22);
            this.secRateTextBox.TabIndex = 9;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(386, 26);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(75, 16);
            this.label14.TabIndex = 8;
            this.label14.Text = "OUTCUT:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(515, 24);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(53, 16);
            this.label15.TabIndex = 8;
            this.label15.Text = "RATE:";
            // 
            // outcutTextBox
            // 
            this.outcutTextBox.Location = new System.Drawing.Point(466, 25);
            this.outcutTextBox.Name = "outcutTextBox";
            this.outcutTextBox.Size = new System.Drawing.Size(49, 22);
            this.outcutTextBox.TabIndex = 9;
            // 
            // outRateTextBox
            // 
            this.outRateTextBox.Location = new System.Drawing.Point(573, 23);
            this.outRateTextBox.Name = "outRateTextBox";
            this.outRateTextBox.Size = new System.Drawing.Size(60, 22);
            this.outRateTextBox.TabIndex = 9;
            // 
            // freshSareesTextBox
            // 
            this.freshSareesTextBox.Location = new System.Drawing.Point(83, 57);
            this.freshSareesTextBox.Name = "freshSareesTextBox";
            this.freshSareesTextBox.Size = new System.Drawing.Size(55, 22);
            this.freshSareesTextBox.TabIndex = 9;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(2, 58);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(63, 16);
            this.label16.TabIndex = 8;
            this.label16.Text = "FRESH:";
            // 
            // fentTextBox
            // 
            this.fentTextBox.Location = new System.Drawing.Point(195, 58);
            this.fentTextBox.Name = "fentTextBox";
            this.fentTextBox.Size = new System.Drawing.Size(55, 22);
            this.fentTextBox.TabIndex = 9;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(138, 59);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(52, 16);
            this.label17.TabIndex = 8;
            this.label17.Text = "FENT:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(254, 57);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(53, 16);
            this.label18.TabIndex = 8;
            this.label18.Text = "RATE:";
            // 
            // fentRateTextBox
            // 
            this.fentRateTextBox.Location = new System.Drawing.Point(312, 56);
            this.fentRateTextBox.Name = "fentRateTextBox";
            this.fentRateTextBox.Size = new System.Drawing.Size(60, 22);
            this.fentRateTextBox.TabIndex = 9;
            // 
            // chindiTextBox
            // 
            this.chindiTextBox.Location = new System.Drawing.Point(441, 57);
            this.chindiTextBox.Name = "chindiTextBox";
            this.chindiTextBox.Size = new System.Drawing.Size(55, 22);
            this.chindiTextBox.TabIndex = 9;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(375, 58);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(63, 16);
            this.label19.TabIndex = 8;
            this.label19.Text = "CHINDI:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(500, 56);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(53, 16);
            this.label20.TabIndex = 8;
            this.label20.Text = "RATE:";
            // 
            // chindiRateTextBox
            // 
            this.chindiRateTextBox.Location = new System.Drawing.Point(558, 55);
            this.chindiRateTextBox.Name = "chindiRateTextBox";
            this.chindiRateTextBox.Size = new System.Drawing.Size(60, 22);
            this.chindiRateTextBox.TabIndex = 9;
            // 
            // greyRateTextBox
            // 
            this.greyRateTextBox.Location = new System.Drawing.Point(456, 17);
            this.greyRateTextBox.Name = "greyRateTextBox";
            this.greyRateTextBox.Size = new System.Drawing.Size(55, 22);
            this.greyRateTextBox.TabIndex = 9;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(348, 18);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(99, 16);
            this.label21.TabIndex = 8;
            this.label21.Text = "GREY RATE:";
            // 
            // greyMtrsTextBox
            // 
            this.greyMtrsTextBox.Location = new System.Drawing.Point(289, 19);
            this.greyMtrsTextBox.Name = "greyMtrsTextBox";
            this.greyMtrsTextBox.Size = new System.Drawing.Size(55, 22);
            this.greyMtrsTextBox.TabIndex = 9;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(176, 21);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(101, 16);
            this.label22.TabIndex = 8;
            this.label22.Text = "GREY MTRS:";
            // 
            // greyTakaTextBox
            // 
            this.greyTakaTextBox.Location = new System.Drawing.Point(118, 20);
            this.greyTakaTextBox.Name = "greyTakaTextBox";
            this.greyTakaTextBox.Size = new System.Drawing.Size(55, 22);
            this.greyTakaTextBox.TabIndex = 9;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(10, 21);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(97, 16);
            this.label23.TabIndex = 8;
            this.label23.Text = "GREY TAKA:";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(518, 16);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(60, 16);
            this.label24.TabIndex = 8;
            this.label24.Text = "DISC%:";
            // 
            // greyDiscTextBox
            // 
            this.greyDiscTextBox.Location = new System.Drawing.Point(588, 16);
            this.greyDiscTextBox.Name = "greyDiscTextBox";
            this.greyDiscTextBox.Size = new System.Drawing.Size(57, 22);
            this.greyDiscTextBox.TabIndex = 9;
            // 
            // jobRateTextBox
            // 
            this.jobRateTextBox.Location = new System.Drawing.Point(456, 50);
            this.jobRateTextBox.Name = "jobRateTextBox";
            this.jobRateTextBox.Size = new System.Drawing.Size(55, 22);
            this.jobRateTextBox.TabIndex = 9;
            // 
            // finMtrsTextBox
            // 
            this.finMtrsTextBox.Location = new System.Drawing.Point(289, 52);
            this.finMtrsTextBox.Name = "finMtrsTextBox";
            this.finMtrsTextBox.Size = new System.Drawing.Size(55, 22);
            this.finMtrsTextBox.TabIndex = 9;
            // 
            // finTakaTextBox
            // 
            this.finTakaTextBox.Location = new System.Drawing.Point(118, 53);
            this.finTakaTextBox.Name = "finTakaTextBox";
            this.finTakaTextBox.Size = new System.Drawing.Size(55, 22);
            this.finTakaTextBox.TabIndex = 9;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(348, 51);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(86, 16);
            this.label25.TabIndex = 8;
            this.label25.Text = "JOB RATE:";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(176, 54);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(108, 16);
            this.label26.TabIndex = 8;
            this.label26.Text = "FINISH MTRS:";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Black;
            this.label27.Location = new System.Drawing.Point(10, 54);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(104, 16);
            this.label27.TabIndex = 8;
            this.label27.Text = "FINISH TAKA:";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(518, 49);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(60, 16);
            this.label28.TabIndex = 8;
            this.label28.Text = "DISC%:";
            // 
            // jobPerTextBox
            // 
            this.jobPerTextBox.Location = new System.Drawing.Point(588, 49);
            this.jobPerTextBox.Name = "jobPerTextBox";
            this.jobPerTextBox.Size = new System.Drawing.Size(57, 22);
            this.jobPerTextBox.TabIndex = 9;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label21);
            this.groupBox2.Controls.Add(this.greyRateTextBox);
            this.groupBox2.Controls.Add(this.jobRateTextBox);
            this.groupBox2.Controls.Add(this.greyMtrsTextBox);
            this.groupBox2.Controls.Add(this.finMtrsTextBox);
            this.groupBox2.Controls.Add(this.jobPerTextBox);
            this.groupBox2.Controls.Add(this.greyTakaTextBox);
            this.groupBox2.Controls.Add(this.greyDiscTextBox);
            this.groupBox2.Controls.Add(this.finTakaTextBox);
            this.groupBox2.Controls.Add(this.label22);
            this.groupBox2.Controls.Add(this.label25);
            this.groupBox2.Controls.Add(this.label23);
            this.groupBox2.Controls.Add(this.label26);
            this.groupBox2.Controls.Add(this.label27);
            this.groupBox2.Controls.Add(this.label28);
            this.groupBox2.Controls.Add(this.label24);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.groupBox2.Location = new System.Drawing.Point(354, 260);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(648, 95);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "GREY AND MILL DATA";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Blue;
            this.label29.Location = new System.Drawing.Point(5, 17);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(141, 18);
            this.label29.TabIndex = 8;
            this.label29.Text = "BASIC COSTING:";
            // 
            // basicCostingTextBox
            // 
            this.basicCostingTextBox.Location = new System.Drawing.Point(145, 16);
            this.basicCostingTextBox.Name = "basicCostingTextBox";
            this.basicCostingTextBox.Size = new System.Drawing.Size(65, 22);
            this.basicCostingTextBox.TabIndex = 9;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Red;
            this.label30.Location = new System.Drawing.Point(206, 16);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(162, 18);
            this.label30.TabIndex = 8;
            this.label30.Text = "AVG. SHORTAGE%:";
            // 
            // avgShortageTextBox
            // 
            this.avgShortageTextBox.Location = new System.Drawing.Point(367, 15);
            this.avgShortageTextBox.Name = "avgShortageTextBox";
            this.avgShortageTextBox.Size = new System.Drawing.Size(55, 22);
            this.avgShortageTextBox.TabIndex = 9;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.wastageRateCheckBox);
            this.groupBox3.Controls.Add(this.greyAndMillDiscCheckBox);
            this.groupBox3.Controls.Add(this.label32);
            this.groupBox3.Controls.Add(this.label30);
            this.groupBox3.Controls.Add(this.label31);
            this.groupBox3.Controls.Add(this.label29);
            this.groupBox3.Controls.Add(this.actualCostingTextBox);
            this.groupBox3.Controls.Add(this.avgWastageTextBox);
            this.groupBox3.Controls.Add(this.basicCostingTextBox);
            this.groupBox3.Controls.Add(this.avgShortageTextBox);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.ForeColor = System.Drawing.Color.Maroon;
            this.groupBox3.Location = new System.Drawing.Point(354, 362);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(645, 100);
            this.groupBox3.TabIndex = 13;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "RESULTS";
            // 
            // greyAndMillDiscCheckBox
            // 
            this.greyAndMillDiscCheckBox.AutoSize = true;
            this.greyAndMillDiscCheckBox.Location = new System.Drawing.Point(16, 49);
            this.greyAndMillDiscCheckBox.Name = "greyAndMillDiscCheckBox";
            this.greyAndMillDiscCheckBox.Size = new System.Drawing.Size(302, 20);
            this.greyAndMillDiscCheckBox.TabIndex = 10;
            this.greyAndMillDiscCheckBox.Text = "INCLUDE GREY AND MILL DISCOUNTS";
            this.greyAndMillDiscCheckBox.UseVisualStyleBackColor = true;
            // 
            // wastageRateCheckBox
            // 
            this.wastageRateCheckBox.AutoSize = true;
            this.wastageRateCheckBox.Location = new System.Drawing.Point(16, 74);
            this.wastageRateCheckBox.Name = "wastageRateCheckBox";
            this.wastageRateCheckBox.Size = new System.Drawing.Size(349, 20);
            this.wastageRateCheckBox.TabIndex = 10;
            this.wastageRateCheckBox.Text = "INCLUDE SECOND,FENT AND CHINDI RATES";
            this.wastageRateCheckBox.UseVisualStyleBackColor = true;
            // 
            // actualCostingTextBox
            // 
            this.actualCostingTextBox.Location = new System.Drawing.Point(441, 69);
            this.actualCostingTextBox.Name = "actualCostingTextBox";
            this.actualCostingTextBox.Size = new System.Drawing.Size(74, 22);
            this.actualCostingTextBox.TabIndex = 9;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Purple;
            this.label31.Location = new System.Drawing.Point(392, 44);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(162, 20);
            this.label31.TabIndex = 8;
            this.label31.Text = "ACTUAL COSTING";
            // 
            // avgWastageTextBox
            // 
            this.avgWastageTextBox.Location = new System.Drawing.Point(579, 14);
            this.avgWastageTextBox.Name = "avgWastageTextBox";
            this.avgWastageTextBox.Size = new System.Drawing.Size(55, 22);
            this.avgWastageTextBox.TabIndex = 9;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Red;
            this.label32.Location = new System.Drawing.Point(428, 15);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(151, 18);
            this.label32.TabIndex = 8;
            this.label32.Text = "AVG. WASTAGE%:";
            // 
            // CuttingReportWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1006, 481);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.takaDeailsListView);
            this.Controls.Add(this.lotNoTextBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.dateTextBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.wtTextBox);
            this.Controls.Add(this.qualityTextBox);
            this.Controls.Add(this.weaverNameTextBox);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.millNameTextBox);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.cuttcardNoTextBox);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.screenNameComboBox);
            this.Controls.Add(this.companyNameComboBox);
            this.Controls.Add(this.label2);
            this.Name = "CuttingReportWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CuttingReportWindow";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox companyNameComboBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox cuttcardNoTextBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox dateTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox millNameTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox lotNoTextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox screenNameComboBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox weaverNameTextBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox qualityTextBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox wtTextBox;
        private System.Windows.Forms.ListView takaDeailsListView;
        private System.Windows.Forms.ColumnHeader no;
        private System.Windows.Forms.ColumnHeader greyMtrs;
        private System.Windows.Forms.ColumnHeader finishMtrs;
        private System.Windows.Forms.ColumnHeader shortage;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox totalSareesTextBox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox secondTextBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox outRateTextBox;
        private System.Windows.Forms.TextBox outcutTextBox;
        private System.Windows.Forms.TextBox chindiRateTextBox;
        private System.Windows.Forms.TextBox fentRateTextBox;
        private System.Windows.Forms.TextBox secRateTextBox;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox chindiTextBox;
        private System.Windows.Forms.TextBox fentTextBox;
        private System.Windows.Forms.TextBox freshSareesTextBox;
        private System.Windows.Forms.TextBox greyRateTextBox;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox greyMtrsTextBox;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox greyTakaTextBox;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox greyDiscTextBox;
        private System.Windows.Forms.TextBox jobRateTextBox;
        private System.Windows.Forms.TextBox finMtrsTextBox;
        private System.Windows.Forms.TextBox finTakaTextBox;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox jobPerTextBox;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox basicCostingTextBox;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox avgShortageTextBox;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox wastageRateCheckBox;
        private System.Windows.Forms.CheckBox greyAndMillDiscCheckBox;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox actualCostingTextBox;
        private System.Windows.Forms.TextBox avgWastageTextBox;
    }
}