﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Texo_Advance.DBItems;
using System.Globalization;

namespace Texo_Advance
{
    public partial class MillReceiptWindow : UserControl
    {
        string strCompanyName = "";
        string strCompanyGstn = "";
        bool bVoucherViewMode = false;
        MasterDBData DB = new MasterDBData();
        PartyDBData partyDB = new PartyDBData();
        millreceipt_detail millReceiptVoucherList;
        List<List<millchallandispatchtaka_detail>> millReceiptChallanTakaDetails = new List<List<millchallandispatchtaka_detail>>();
        public MillReceiptWindow(string companyName,string gstNo)
        {
            InitializeComponent();
            strCompanyName = companyName;
            strCompanyGstn = gstNo;
            
        }

        private void MillReceiptWindow_Load(object sender, EventArgs e)
        {
            companyNameTextBox.Text = strCompanyName;
            dateTextBox.Text = DateTime.Today.ToString("dd-MM-yyyy");
            updateMillList();
            updateQualityList();
            setDefaultData();
            createButton.Enabled = true;
            viewButton.Enabled = true;
            updateButton.Enabled = false;
            deleteButton.Enabled = false;
        }

        private void companyNameComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void updateMillList()
        {
            var partyList = DB.getPartyDetails(10);
            if (partyList != null)
            {
                partyNameComboBox.DisplayMember = "party_name";
                partyNameComboBox.ValueMember = "partyid";
                partyNameComboBox.DataSource = partyList;
            }
        }

        private void resetAllData()
        {
            billNoTextBox.Clear();
            remarkTextBox.Clear();
            millReceiptDataGridView.Rows.Clear();
            totalGreyMtrsTextBox.Clear();
            totalRecMtrsTextBox.Clear();
            totalRecTakaTextBox.Clear();
            hsnCodeTextBox.Clear();
            updateMillList();
            discPerTextBox.Text = "0";
            discPerAmtFinalTextBox.Text = "0";
            reMtrTextBox.Text = "0";
            rdAmtFinalTextBox.Text = "0";
            tdsPerTextBox.Text = "0";
            tdsAmtTextBox2.Text = "0";
            cgstAmtTextBox.Text = "0";
            sgstAmtTextBox.Text = "0";
            igstAmtTextBox.Text = "0";
            grossAmountTextBox.Text = "0";
            addLessTextBox.Text = "0";
            taxValTextBox.Text = "0";
            invValueTextBox.Text = "0";
            netAmtAftTdsTextBox.Text = "0";
            createButton.Enabled = true;
            viewButton.Enabled = true;
            updateButton.Enabled = false;
            deleteButton.Enabled = false;
        }
        private void setDefaultData()
        {
            long lSrNo = partyDB.getMillReceiptVoucherNo();
            if (lSrNo == -1)
            {
                MessageBox.Show("Unable to retrieve Grey Purchase Data", "Error", MessageBoxButtons.OK);
                return;
            }
            else if (lSrNo == 0)
            {
                voucherNoTextBox.Text = "1";
            }
            else
                voucherNoTextBox.Text = Convert.ToString(lSrNo + 1);
        }

        private void millReceiptDataGridView_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            if(e.ColumnIndex == recMtrsTextBox.Index)
            {
                if (millReceiptDataGridView.Rows[e.RowIndex].Cells[challanNoComboBox.Index].Value == null)
                    return;
                DialogResult result = MessageBox.Show("Do you want to enter takawise details","Confirm",MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    long lChallanId = (long)millReceiptDataGridView.Rows[e.RowIndex].Cells[challanNoComboBox.Index].Value;
                    
                    
                    List<millchallandispatchtaka_detail> challanDetail = new List<millchallandispatchtaka_detail>();
                    challanDetail = partyDB.getMillChallanTakaDetails(lChallanId);
                    if (challanDetail == null)
                    {
                        MessageBox.Show("Unable to fetch Challan Data", "ERROR", MessageBoxButtons.OK);
                        return;
                    }
                    bool bViewModeOnly = false;
                    if(millReceiptChallanTakaDetails.Count > 0)
                    {
                        if((millReceiptDataGridView.Rows.Count - 1) > millReceiptChallanTakaDetails.Count)
                        {
                            for (int i = 0; i < millReceiptChallanTakaDetails.Count; i++)
                            {
                                List<millchallandispatchtaka_detail> tempTakaList = millReceiptChallanTakaDetails[i];
                                for (int j = 0; j < challanDetail.Count; j++)
                                {
                                    for (int k = 0; k < tempTakaList.Count; k++)
                                    {
                                        if (challanDetail[j].millTaka_id == tempTakaList[k].millTaka_id && tempTakaList[k].isReceived == true)
                                            challanDetail[j].isReceived = true;
                                    }

                                }

                            }
                        }
                        else
                        {
                            bViewModeOnly = true;
                            List<millchallandispatchtaka_detail> tempChallanDetail = millReceiptChallanTakaDetails[e.RowIndex];
                            for(int i = 0;i < challanDetail.Count;i++)
                            {
                                if((bool)tempChallanDetail[i].isReceived && ((bool)!challanDetail[i].isReceived))
                                {
                                    challanDetail[i].finMtrs = tempChallanDetail[i].finMtrs;
                                }
                            }
                        }
                        
                    }

                    MillReceiptTakaChallan mrtcw = new MillReceiptTakaChallan();
                    mrtcw.setDefaultData(challanDetail, bViewModeOnly);
                    mrtcw.ShowDialog();
                    challanDetail = mrtcw.getMillReceiptTakaDetails();
                    //if(challanDetail.Count > 0)
                    {
                        if(bViewModeOnly)
                        {
                            bViewModeOnly = false;
                            
                            for (int i = 0; i < challanDetail.Count; i++)
                            {
                                double dFinMtrs = 0;
                                string takaMtrs = challanDetail[i].finMtrs;
                                var takaMtrsAfterSplit = takaMtrs.Split('+');
                                for (int j = 0; j < takaMtrsAfterSplit.Count(); j++)
                                {
                                    double dTempFinMtrs = 0;
                                    //double.TryParse(millReceivedTakaDataGridView.Rows[i].Cells[finMtrsGridTextBox.Index].EditedFormattedValue.ToString(),out dTempFinMtrs);
                                    double.TryParse(takaMtrsAfterSplit[j], out dTempFinMtrs);
                                    dFinMtrs = dFinMtrs + dTempFinMtrs;
                                }
                                if (dFinMtrs > 0 && ((bool)!challanDetail[i].isReceived))
                                {
                                    challanDetail[i].isReceived = true;
                                }
                            }

                            millReceiptChallanTakaDetails[e.RowIndex] = challanDetail;
                            millReceiptDataGridView.Rows[e.RowIndex].Cells[recMtrsTextBox.Index].Value = mrtcw.getTotalReceivedMtrs();
                            millReceiptDataGridView.Rows[e.RowIndex].Cells[takaTextBox.Index].Value = Convert.ToInt32(mrtcw.getTotalReceivedTaka()) + Convert.ToInt32(mrtcw.getNoOfTPs());
                            millReceiptDataGridView.Rows[e.RowIndex].Cells[greyMtrsTextBox.Index].Value = mrtcw.getGreyMtrs();
                        }
                        else if(mrtcw.getTakaReceivedStatus())
                        {
                            millReceiptChallanTakaDetails.Add(challanDetail);
                            millReceiptDataGridView.Rows[e.RowIndex].Cells[recMtrsTextBox.Index].Value = mrtcw.getTotalReceivedMtrs();
                            millReceiptDataGridView.Rows[e.RowIndex].Cells[takaTextBox.Index].Value = Convert.ToInt32(mrtcw.getTotalReceivedTaka()) + Convert.ToInt32(mrtcw.getNoOfTPs());
                            millReceiptDataGridView.Rows[e.RowIndex].Cells[greyMtrsTextBox.Index].Value = mrtcw.getGreyMtrs();
                        }
                        
                    }
                    double dTotalFinMtrs = 0, dGreyMtrs = 0;
                    long lTotalTakas = 0;
                    for(int i = 0;i < millReceiptDataGridView.Rows.Count-1;i++)
                    {
                        dTotalFinMtrs = dTotalFinMtrs + Convert.ToDouble(millReceiptDataGridView.Rows[i].Cells[recMtrsTextBox.Index].EditedFormattedValue.ToString());
                        dGreyMtrs = dGreyMtrs + Convert.ToDouble(millReceiptDataGridView.Rows[i].Cells[greyMtrsTextBox.Index].EditedFormattedValue.ToString());
                        lTotalTakas = lTotalTakas + Convert.ToInt64(millReceiptDataGridView.Rows[i].Cells[takaTextBox.Index].EditedFormattedValue.ToString());
                    }
                    totalGreyMtrsTextBox.Text = Convert.ToString(dGreyMtrs);
                    totalRecMtrsTextBox.Text = Convert.ToString(dTotalFinMtrs);
                    totalRecTakaTextBox.Text = Convert.ToString(lTotalTakas);
                    //millReceiptDataGridView.CurrentCell = millReceiptDataGridView.Rows[e.RowIndex].Cells[greyMtrsTextBox.Index];
                    

                }
                else
                    return;
            }
        }

        private void dateTextBox_Validating(object sender, CancelEventArgs e)
        {
            DateTime dt;
            bool isValid = DateTime.TryParseExact(dateTextBox.Text, "dd-MM-yyyy", null, DateTimeStyles.None, out dt);
            if (!isValid)
            {
                MessageBox.Show("Please enter the valid date", "DATE", MessageBoxButtons.OK);
                dateTextBox.Text = DateTime.Today.ToString("dd-MM-yyyy");
                dateTextBox.Focus();
            }
        }

        private void partyNameComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            gstnTextBox.Text = DB.getPartyGstn((long)partyNameComboBox.SelectedValue);
            List<milldispatchchallan_detail> challanList = partyDB.getOustandingChallanList((long)partyNameComboBox.SelectedValue);
            if(challanList== null)
            {
                MessageBox.Show("Unable to fetch data", "ERROR", MessageBoxButtons.OK);
                return;
            }
            if(challanList.Count <= 0)
            {
                MessageBox.Show("No Pending Challan in Mill", "ERROR", MessageBoxButtons.OK);
                return;
            }
            updateMillChallanList(challanList);
            //gstnTextBox.Text = DB.getPartyGstn((long)partyNameComboBox.SelectedValue);
            string companyStateCode = strCompanyGstn.Substring(0, 2);
            string partyStateCode = gstnTextBox.Text.Substring(0, 2);
            
            if (partyStateCode == companyStateCode)
            {
                igstAmtTextBox.Enabled = false;
                igstRateTextBox.Enabled = false;
                cgstAmtTextBox.Enabled = true;
                sgstAmtTextBox.Enabled = true;
                cgstRateTextBox.Enabled = true;
                sgstRateTextBox.Enabled = true;

                cgstRateTextBox.Text = "2.5";
                sgstRateTextBox.Text = "2.5";
                igstRateTextBox.Text = "0";
            }
            else
            {
                igstAmtTextBox.Enabled = true;
                igstRateTextBox.Enabled = true;
                cgstAmtTextBox.Enabled = false;
                sgstAmtTextBox.Enabled = false;
                cgstRateTextBox.Enabled = false;
                sgstRateTextBox.Enabled = false;

                cgstRateTextBox.Text = "0";
                sgstRateTextBox.Text = "0";
                igstRateTextBox.Text = "5";
            }
                
        }

        private void updateMillChallanList(List<milldispatchchallan_detail> challanList)
        {
            DataGridViewComboBoxCell cell = (DataGridViewComboBoxCell)millReceiptDataGridView.Rows[0].Cells[challanNoComboBox.Index];
            cell.ValueMember = "challan_id";
            cell.DisplayMember = "challanNo";
            cell.DataSource = challanList;
        }
        private void updateLatestMillChallanList(long lMillId,int iRow)
        {
            List<milldispatchchallan_detail> challanList = partyDB.getOustandingChallanList((long)partyNameComboBox.SelectedValue);
            if (challanList == null)
            {
                MessageBox.Show("Unable to fetch data", "ERROR", MessageBoxButtons.OK);
                return;
            }
            if (challanList.Count <= 0)
            {
                MessageBox.Show("No Pending Challan in Mill", "ERROR", MessageBoxButtons.OK);
                return;
            }
            for(int i = 0;i < iRow;i++)
            {
                long lChallanId = (long)millReceiptDataGridView.Rows[i].Cells[challanNoComboBox.Index].Value;
                milldispatchchallan_detail deleteChallan = challanList.Where(x => x.challan_id == lChallanId).SingleOrDefault();
                challanList.Remove(deleteChallan);
            }
            DataGridViewComboBoxCell cell = (DataGridViewComboBoxCell)millReceiptDataGridView.Rows[iRow].Cells[challanNoComboBox.Index];
            cell.ValueMember = "challan_id";
            cell.DisplayMember = "challanNo";
            cell.DataSource = challanList;
        }


        private void millReceiptDataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void millReceiptDataGridView_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (millReceiptDataGridView.CurrentCell.ColumnIndex == 0 && e.Control is ComboBox)
            {
                ComboBox comboBox = e.Control as ComboBox;
                comboBox.SelectedIndex = -1;
                comboBox.SelectedIndexChanged -= LastColumnComboSelectionChanged;
                comboBox.SelectedIndexChanged += LastColumnComboSelectionChanged;
                
            }
        }
        private void LastColumnComboSelectionChanged(object sender, EventArgs e)
        {
            
            string challanNo = millReceiptDataGridView.Rows[millReceiptDataGridView.CurrentRow.Index].Cells[challanNoComboBox.Index].EditedFormattedValue.ToString();
            milldispatchchallan_detail challanDetails = partyDB.getMillChallanDetails(challanNo);
            if(challanDetails == null)
            {
                MessageBox.Show("Unable to fetch challan data", "ERROR", MessageBoxButtons.OK);
                return;
            }
            millReceiptDataGridView.Rows[millReceiptDataGridView.CurrentRow.Index].Cells[recdCardNoTextBox.Index].Value = Convert.ToString(challanDetails.cardNo);
            millReceiptDataGridView.Rows[millReceiptDataGridView.CurrentRow.Index].Cells[greyQualityComboBox.Index].Value = challanDetails.quality_id;
            millReceiptDataGridView.Rows[millReceiptDataGridView.CurrentRow.Index].Cells[jobRateTextBox.Index].Value = Convert.ToString(challanDetails.rate);
            long lTotalTaka = 0;
            double dTotalGreyMtrs = 0;
            List<millchallandispatchtaka_detail> millChallanTakaDetails = partyDB.getMillChallanTakaDetails((long)challanDetails.challan_id);
            for(int i = 0;i < millChallanTakaDetails.Count;i++)
            {
                if(!((bool)millChallanTakaDetails[i].isReceived))
                {
                    lTotalTaka = lTotalTaka + 1;

                    double dGreyMtrs = 0;

                    string greyTakaMtrs = millChallanTakaDetails[i].greyMtrs;
                    var greyTakaMtrsAfterSplit = greyTakaMtrs.Split('+');
                    for (int j = 0; j < greyTakaMtrsAfterSplit.Count(); j++)
                    {
                        double dTempGreyMtrs = 0;
                        //double.TryParse(millReceivedTakaDataGridView.Rows[i].Cells[finMtrsGridTextBox.Index].EditedFormattedValue.ToString(),out dTempFinMtrs);
                        double.TryParse(greyTakaMtrsAfterSplit[j], out dTempGreyMtrs);
                        dGreyMtrs = dGreyMtrs + dTempGreyMtrs;
                    }

                    dTotalGreyMtrs = dTotalGreyMtrs + dGreyMtrs;
                }
            }
            millReceiptDataGridView.Rows[millReceiptDataGridView.CurrentRow.Index].Cells[takaTextBox.Index].Value = Convert.ToString(lTotalTaka);
            millReceiptDataGridView.Rows[millReceiptDataGridView.CurrentRow.Index].Cells[greyMtrsTextBox.Index].Value = Convert.ToString(dTotalGreyMtrs);

        }

        private void updateQualityList()
        {
            List<greyquality_details> greyQualityList = DB.getGreyQualityList();
            greyQualityComboBox.DisplayMember = "quality_name";
            greyQualityComboBox.ValueMember = "greyquality_id";
            greyQualityComboBox.DataSource = greyQualityList;
        }

        private void millReceiptDataGridView_CellValidated(object sender, DataGridViewCellEventArgs e)
        {
            if(e.ColumnIndex == jobAmountTextBox.Index)
            {
                if(e.RowIndex == (millReceiptDataGridView.Rows.Count - 2))
                {
                    DialogResult result = MessageBox.Show("Do You want to add new challan?", "MILL RECEIPT", MessageBoxButtons.YesNo);
                    if (result == DialogResult.Yes)
                    {
                        millReceiptDataGridView.CausesValidation = false;
                        millReceiptDataGridView.Rows[e.RowIndex + 1].ReadOnly = false;
                        DataGridViewComboBoxCell cell = (DataGridViewComboBoxCell)millReceiptDataGridView.Rows[e.RowIndex + 1].Cells[challanNoComboBox.Index];
                        updateLatestMillChallanList((long)partyNameComboBox.SelectedValue, e.RowIndex + 1);
                        return;
                    }
                    else
                    {
                        millReceiptDataGridView.CausesValidation = false;
                        discPerTextBox.Focus();
                    }
                }
                
            }
        }

        private void millReceiptDataGridView_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if(millReceiptDataGridView.Rows.Count > 0 && (e.RowIndex < millReceiptDataGridView.Rows.Count -1))
            {
                if (e.ColumnIndex == challanNoComboBox.Index)
                {
                    if ((int)millReceiptDataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].EditedFormattedValue.ToString().Length <= 0)
                    {
                        if(challanNoComboBox.Items.Count!=0)
                        {
                            MessageBox.Show("Please select the correct item", "INPUT ERROR", MessageBoxButtons.OK);
                            //saleDataGridView.CurrentCell = saleDataGridView[e.RowIndex, e.ColumnIndex];
                            millReceiptDataGridView.CurrentCell.Selected = true;
                            e.Cancel = true;
                        }
                        
                    }
                    if(millReceiptDataGridView.Rows.Count > e.RowIndex + 1)
                        millReceiptDataGridView.Rows[e.RowIndex + 1].ReadOnly = true;
                }
                else if (e.ColumnIndex == recMtrsTextBox.Index)
                {
                    double dRecdMtrs = 0;
                    bool bFlag = double.TryParse(millReceiptDataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].EditedFormattedValue.ToString(), out dRecdMtrs);
                    if (!bFlag || dRecdMtrs <= 0)
                    {
                        MessageBox.Show("Invalid input in Recd. Mtrs", "INPUT ERROR", MessageBoxButtons.OK);
                        //saleDataGridView.CurrentCell = saleDataGridView[e.RowIndex, e.ColumnIndex];
                        millReceiptDataGridView.CurrentCell.Selected = true;
                        e.Cancel = true;
                    }
                    else
                    {
                        dRecdMtrs = 0;
                        double.TryParse(millReceiptDataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].EditedFormattedValue.ToString(), out dRecdMtrs);
                        double dJobRate = 0;
                        double.TryParse(millReceiptDataGridView.Rows[e.RowIndex].Cells[jobRateTextBox.Index].EditedFormattedValue.ToString(), out dJobRate);
                        double dJobAmount = Math.Round(dRecdMtrs * dJobRate,2);
                        millReceiptDataGridView.Rows[e.RowIndex].Cells[jobAmountTextBox.Index].Value = Convert.ToString(dJobAmount);
                        double dTotalRecdMtrs = 0, dTotalRecdTakas = 0, dTotalGreyMtrs = 0, dTotalJobAmount = 0;
                        for (int i = 0; i < millReceiptDataGridView.Rows.Count - 1; i++)
                        {
                            double dTakas = 0;
                            double.TryParse(millReceiptDataGridView.Rows[i].Cells[takaTextBox.Index].EditedFormattedValue.ToString(), out dTakas);
                            dTotalRecdTakas = dTotalRecdTakas + dTakas;
                            double dReceivedMtrs = 0;
                            double.TryParse(millReceiptDataGridView.Rows[i].Cells[recMtrsTextBox.Index].EditedFormattedValue.ToString(), out dReceivedMtrs);
                            dTotalRecdMtrs = dTotalRecdMtrs + dReceivedMtrs;
                            double dGreyMtrs = 0;
                            double.TryParse(millReceiptDataGridView.Rows[i].Cells[greyMtrsTextBox.Index].EditedFormattedValue.ToString(), out dGreyMtrs);
                            dTotalGreyMtrs = dTotalGreyMtrs + dGreyMtrs;
                            double dJobAmt = 0;
                            double.TryParse(millReceiptDataGridView.Rows[i].Cells[jobAmountTextBox.Index].EditedFormattedValue.ToString(), out dJobAmt);
                            dTotalJobAmount = dTotalJobAmount + dJobAmt;

                        }
                        totalGreyMtrsTextBox.Text = Convert.ToString(dTotalGreyMtrs);
                        totalRecMtrsTextBox.Text = Convert.ToString(dTotalRecdMtrs);
                        totalRecTakaTextBox.Text = Convert.ToString(dTotalRecdTakas);
                        grossAmountTextBox.Text = Convert.ToString(dTotalJobAmount);
                    }
                }
                else if (e.ColumnIndex == jobRateTextBox.Index)
                {
                    double dRate = 0;
                    bool bFlag = double.TryParse(millReceiptDataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].EditedFormattedValue.ToString(), out dRate);
                    if (!bFlag || dRate < 0)
                    {
                        MessageBox.Show("Invalid rate", "INPUT ERROR", MessageBoxButtons.OK);
                        //saleDataGridView.CurrentCell = saleDataGridView[e.RowIndex, e.ColumnIndex];
                        millReceiptDataGridView.CurrentCell.Selected = true;
                        e.Cancel = true;
                    }
                    else
                    {
                        double dRecdMtrs = 0;
                        double.TryParse(millReceiptDataGridView.Rows[e.RowIndex].Cells[recMtrsTextBox.Index].EditedFormattedValue.ToString(), out dRecdMtrs);
                        double dJobRate = 0;
                        double.TryParse(millReceiptDataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].EditedFormattedValue.ToString(), out dJobRate);
                        double dJobAmount = Math.Round(dRecdMtrs * dJobRate, 2);
                        millReceiptDataGridView.Rows[e.RowIndex].Cells[jobAmountTextBox.Index].Value = Convert.ToString(dJobAmount);
                        double dTotalRecdMtrs = 0,dTotalRecdTakas = 0,dTotalGreyMtrs = 0,dTotalJobAmount = 0;
                        for (int i = 0;i < millReceiptDataGridView.Rows.Count - 1;i++)
                        {
                            double dTakas = 0;
                            double.TryParse(millReceiptDataGridView.Rows[i].Cells[takaTextBox.Index].EditedFormattedValue.ToString(), out dTakas);
                            dTotalRecdTakas = dTotalRecdTakas + dTakas;
                            double dReceivedMtrs = 0;
                            double.TryParse(millReceiptDataGridView.Rows[i].Cells[recMtrsTextBox.Index].EditedFormattedValue.ToString(), out dReceivedMtrs);
                            dTotalRecdMtrs = dTotalRecdMtrs + dReceivedMtrs;
                            double dGreyMtrs = 0;
                            double.TryParse(millReceiptDataGridView.Rows[i].Cells[greyMtrsTextBox.Index].EditedFormattedValue.ToString(), out dGreyMtrs);
                            dTotalGreyMtrs = dTotalGreyMtrs + dGreyMtrs;
                            double dJobAmt = 0;
                            double.TryParse(millReceiptDataGridView.Rows[i].Cells[jobAmountTextBox.Index].EditedFormattedValue.ToString(), out dJobAmt);
                            dTotalJobAmount = dTotalJobAmount + dJobAmt;
                            
                        }
                        totalGreyMtrsTextBox.Text = Convert.ToString(dTotalGreyMtrs);
                        totalRecMtrsTextBox.Text = Convert.ToString(dTotalRecdMtrs);
                        totalRecTakaTextBox.Text = Convert.ToString(dTotalRecdTakas);
                        grossAmountTextBox.Text = Convert.ToString(dTotalJobAmount);
                    }
                }
            }
            


        }

        private void discPerTextBox_TextChanged(object sender, EventArgs e)
        {
            double dDiscountPercntage = 0;
            bool bFlag = double.TryParse(discPerTextBox.Text,out dDiscountPercntage);
            if(!bFlag)
            {
                discPerTextBox.Text = "0";
                discPerTextBox.SelectAll();
                return;
            }

            double dGrossAmount = 0;
            double.TryParse(grossAmountTextBox.Text,out dGrossAmount);
            double dDiscountAmount = Math.Round((dDiscountPercntage / 100) * dGrossAmount, 2);
            discPerAmtFinalTextBox.Text = Convert.ToString(dDiscountAmount);

            double dRDAmount = 0;
            double.TryParse(rdAmtFinalTextBox.Text,out dRDAmount);
            double dTdsAmount = 0;
            double.TryParse(tdsAmtTextBox2.Text,out dTdsAmount);

            double dOtherLessAny = 0;
            double.TryParse(addLessTextBox.Text,out dOtherLessAny);
            
            double dTaxableValue = dGrossAmount - dDiscountAmount - dRDAmount - dOtherLessAny;

            double dCgstRate = 0;
            double.TryParse(cgstRateTextBox.Text,out dCgstRate);
            double dCgstAmount = Math.Round((dCgstRate / 100)*dTaxableValue,2);
            cgstAmtTextBox.Text = Convert.ToString(dCgstAmount);
            double dSgstRate = 0;
            double.TryParse(sgstRateTextBox.Text,out dSgstRate);
            double dSgstAmount = Math.Round((dSgstRate / 100) * dTaxableValue, 2);
            sgstAmtTextBox.Text = Convert.ToString(dSgstAmount);
            double dIgstRate = 0;
            double.TryParse(igstRateTextBox.Text,out dIgstRate);
            double dIgstAmount = Math.Round((dIgstRate / 100) * dTaxableValue, 2);
            igstAmtTextBox.Text = Convert.ToString(dIgstAmount);
            string companyStateCode = strCompanyGstn.Substring(0, 2);
            string partyStateCode = gstnTextBox.Text.Substring(0, 2);
            bool bLocalTaxInvoice = false;
            if (partyStateCode == companyStateCode)
                bLocalTaxInvoice = true;
            double dInvoiceValue = 0;
            if (bLocalTaxInvoice)
                dInvoiceValue = dTaxableValue + dCgstAmount + dCgstAmount;
            else
                dInvoiceValue = dTaxableValue + dIgstAmount;

            double dNetAmount = Math.Round(dInvoiceValue - dTdsAmount);
            invValueTextBox.Text = Convert.ToString(dInvoiceValue);
            taxValTextBox.Text = Convert.ToString(dTaxableValue);
            netAmtAftTdsTextBox.Text = Convert.ToString(dNetAmount);

        }

        private void discPerAmtFinalTextBox_TextChanged(object sender, EventArgs e)
        {
            double dDiscountAmount = 0;
            bool bFlag = double.TryParse(discPerAmtFinalTextBox.Text, out dDiscountAmount);
            if (!bFlag)
            {
                discPerAmtFinalTextBox.Text = "0";
                discPerAmtFinalTextBox.SelectAll();
                return;
            }
            dDiscountAmount = Convert.ToDouble(discPerAmtFinalTextBox.Text);//Math.Round((dDiscountPercntage / 100) * Convert.ToDouble(grossAmountTextBox.Text), 2);
            discPerAmtFinalTextBox.Text = Convert.ToString(dDiscountAmount);

            double dRDAmount = Convert.ToDouble(rdAmtFinalTextBox.Text);
            double dTdsAmount = Convert.ToDouble(tdsAmtTextBox2.Text);
            
            double dOtherLessAny = Convert.ToDouble(addLessTextBox.Text);
            double dGrossAmount = Convert.ToDouble(grossAmountTextBox.Text);

            double dTaxableValue = dGrossAmount - dDiscountAmount - dRDAmount - dOtherLessAny;
            double dCgstAmount = Math.Round((Convert.ToDouble(cgstRateTextBox.Text) / 100) * dTaxableValue, 2);
            cgstAmtTextBox.Text = Convert.ToString(dCgstAmount);
            double dSgstAmount = Math.Round((Convert.ToDouble(sgstRateTextBox.Text) / 100) * dTaxableValue, 2);
            sgstAmtTextBox.Text = Convert.ToString(dSgstAmount);
            double dIgstAmount = Math.Round((Convert.ToDouble(igstRateTextBox.Text) / 100) * dTaxableValue, 2);
            igstAmtTextBox.Text = Convert.ToString(dIgstAmount);
            string companyStateCode = strCompanyGstn.Substring(0, 2);
            string partyStateCode = gstnTextBox.Text.Substring(0, 2);
            bool bLocalTaxInvoice = false;
            if (partyStateCode == companyStateCode)
                bLocalTaxInvoice = true;
            double dInvoiceValue = 0;
            if (bLocalTaxInvoice)
                dInvoiceValue = dTaxableValue + dCgstAmount + dCgstAmount;
            else
                dInvoiceValue = dTaxableValue + dIgstAmount;

            double dNetAmount = Math.Round(dInvoiceValue - dTdsAmount);
            invValueTextBox.Text = Convert.ToString(dInvoiceValue);
            taxValTextBox.Text = Convert.ToString(dTaxableValue);
            netAmtAftTdsTextBox.Text = Convert.ToString(dNetAmount);
        }

        private void reMtrTextBox_TextChanged(object sender, EventArgs e)
        {
            double dRDPerMtr = 0;
            bool bFlag = double.TryParse(reMtrTextBox.Text, out dRDPerMtr);
            if (!bFlag)
            {
                reMtrTextBox.Text = "0";
                reMtrTextBox.SelectAll();
                return;
            }
            double dDiscountAmount = Convert.ToDouble(discPerAmtFinalTextBox.Text);//Math.Round((dDiscountPercntage / 100) * Convert.ToDouble(grossAmountTextBox.Text), 2);
            //discPerAmtFinalTextBox.Text = Convert.ToString(dDiscountAmount);
            double dReceivedMtrs = 0;
            double.TryParse(totalRecMtrsTextBox.Text,out dReceivedMtrs);
            double dRDAmount = Math.Round(dReceivedMtrs * dRDPerMtr,2);
            rdAmtFinalTextBox.Text = Convert.ToString(dRDAmount);
            double dTdsAmount = Convert.ToDouble(tdsAmtTextBox2.Text);
            
            double dOtherLessAny = Convert.ToDouble(addLessTextBox.Text);
            double dGrossAmount = Convert.ToDouble(grossAmountTextBox.Text);

            double dTaxableValue = dGrossAmount - dDiscountAmount - dRDAmount - dOtherLessAny;
            double dCgstAmount = Math.Round((Convert.ToDouble(cgstRateTextBox.Text) / 100) * dTaxableValue, 2);
            cgstAmtTextBox.Text = Convert.ToString(dCgstAmount);
            double dSgstAmount = Math.Round((Convert.ToDouble(sgstRateTextBox.Text) / 100) * dTaxableValue, 2);
            sgstAmtTextBox.Text = Convert.ToString(dSgstAmount);
            double dIgstAmount = Math.Round((Convert.ToDouble(igstRateTextBox.Text) / 100) * dTaxableValue, 2);
            igstAmtTextBox.Text = Convert.ToString(dIgstAmount);
            string companyStateCode = strCompanyGstn.Substring(0, 2);
            string partyStateCode = gstnTextBox.Text.Substring(0, 2);
            bool bLocalTaxInvoice = false;
            if (partyStateCode == companyStateCode)
                bLocalTaxInvoice = true;
            double dInvoiceValue = 0;
            if (bLocalTaxInvoice)
                dInvoiceValue = dTaxableValue + dCgstAmount + dCgstAmount;
            else
                dInvoiceValue = dTaxableValue + dIgstAmount;

            double dNetAmount = Math.Round(dInvoiceValue - dTdsAmount);
            invValueTextBox.Text = Convert.ToString(dInvoiceValue);
            taxValTextBox.Text = Convert.ToString(dTaxableValue);
            netAmtAftTdsTextBox.Text = Convert.ToString(dNetAmount);
        }

        private void rdAmtFinalTextBox_TextChanged(object sender, EventArgs e)
        {
            double dRDAmount = 0;
            bool bFlag = double.TryParse(rdAmtFinalTextBox.Text, out dRDAmount);
            if (!bFlag)
            {
                rdAmtFinalTextBox.Text = "0";
                rdAmtFinalTextBox.SelectAll();
                return;
            }
            double dDiscountAmount = Convert.ToDouble(discPerAmtFinalTextBox.Text);//Math.Round((dDiscountPercntage / 100) * Convert.ToDouble(grossAmountTextBox.Text), 2);
            //discPerAmtFinalTextBox.Text = Convert.ToString(dDiscountAmount);

            //dRDAmount = Convert.ToDouble(rdAmtFinalTextBox.Text);
            double dTdsAmount = Convert.ToDouble(tdsAmtTextBox2.Text);
            
            double dOtherLessAny = Convert.ToDouble(addLessTextBox.Text);
            double dGrossAmount = Convert.ToDouble(grossAmountTextBox.Text);

            double dTaxableValue = dGrossAmount - dDiscountAmount - dRDAmount - dOtherLessAny;
            double dCgstAmount = Math.Round((Convert.ToDouble(cgstRateTextBox.Text) / 100) * dTaxableValue, 2);
            cgstAmtTextBox.Text = Convert.ToString(dCgstAmount);
            double dSgstAmount = Math.Round((Convert.ToDouble(sgstRateTextBox.Text) / 100) * dTaxableValue, 2);
            sgstAmtTextBox.Text = Convert.ToString(dSgstAmount);
            double dIgstAmount = Math.Round((Convert.ToDouble(igstRateTextBox.Text) / 100) * dTaxableValue, 2);
            igstAmtTextBox.Text = Convert.ToString(dIgstAmount);
            string companyStateCode = strCompanyGstn.Substring(0, 2);
            string partyStateCode = gstnTextBox.Text.Substring(0, 2);
            bool bLocalTaxInvoice = false;
            if (partyStateCode == companyStateCode)
                bLocalTaxInvoice = true;
            double dInvoiceValue = 0;
            if (bLocalTaxInvoice)
                dInvoiceValue = dTaxableValue + dCgstAmount + dCgstAmount;
            else
                dInvoiceValue = dTaxableValue + dIgstAmount;

            double dNetAmount = Math.Round(dInvoiceValue - dTdsAmount);
            invValueTextBox.Text = Convert.ToString(dInvoiceValue);
            taxValTextBox.Text = Convert.ToString(dTaxableValue);
            netAmtAftTdsTextBox.Text = Convert.ToString(dNetAmount);
        }

        private void tdsPerTextBox_TextChanged(object sender, EventArgs e)
        {
            double dTdsPercentage = 0;
            bool bFlag = double.TryParse(tdsPerTextBox.Text, out dTdsPercentage);
            if (!bFlag)
            {
                tdsPerTextBox.Text = "0";
                tdsPerTextBox.SelectAll();
                return;
            }
            double dDiscountAmount = Convert.ToDouble(discPerAmtFinalTextBox.Text);//Math.Round((dDiscountPercntage / 100) * Convert.ToDouble(grossAmountTextBox.Text), 2);
            //discPerAmtFinalTextBox.Text = Convert.ToString(dDiscountAmount);

            double dRDAmount = Convert.ToDouble(rdAmtFinalTextBox.Text);
            
            
            double dOtherLessAny = Convert.ToDouble(addLessTextBox.Text);
            double dGrossAmount = Convert.ToDouble(grossAmountTextBox.Text);

            double dTaxableValue = dGrossAmount - dDiscountAmount - dRDAmount - dOtherLessAny;
            double dCgstAmount = Math.Round((Convert.ToDouble(cgstRateTextBox.Text) / 100) * dTaxableValue, 2);
            cgstAmtTextBox.Text = Convert.ToString(dCgstAmount);
            double dSgstAmount = Math.Round((Convert.ToDouble(sgstRateTextBox.Text) / 100) * dTaxableValue, 2);
            sgstAmtTextBox.Text = Convert.ToString(dSgstAmount);
            double dIgstAmount = Math.Round((Convert.ToDouble(igstRateTextBox.Text) / 100) * dTaxableValue, 2);
            igstAmtTextBox.Text = Convert.ToString(dIgstAmount);
            double dTdsAmount = Math.Round(dTaxableValue * (dTdsPercentage /100),2);
            tdsAmtTextBox2.Text = Convert.ToString(dTdsAmount);
            string companyStateCode = strCompanyGstn.Substring(0, 2);
            string partyStateCode = gstnTextBox.Text.Substring(0, 2);
            bool bLocalTaxInvoice = false;
            if (partyStateCode == companyStateCode)
                bLocalTaxInvoice = true;
            double dInvoiceValue = 0;
            if (bLocalTaxInvoice)
                dInvoiceValue = dTaxableValue + dCgstAmount + dCgstAmount;
            else
                dInvoiceValue = dTaxableValue + dIgstAmount;

            double dNetAmount = Math.Round(dInvoiceValue - dTdsAmount);
            invValueTextBox.Text = Convert.ToString(dInvoiceValue);
            taxValTextBox.Text = Convert.ToString(dTaxableValue);
            netAmtAftTdsTextBox.Text = Convert.ToString(dNetAmount);
        }

        private void tdsAmtTextBox2_TextChanged(object sender, EventArgs e)
        {
            double dTdsAmount = 0;
            bool bFlag = double.TryParse(tdsAmtTextBox2.Text, out dTdsAmount);
            if (!bFlag)
            {
                tdsAmtTextBox2.Text = "0";
                tdsAmtTextBox2.SelectAll();
                return;
            }
            double dDiscountAmount = Convert.ToDouble(discPerAmtFinalTextBox.Text);//Math.Round((dDiscountPercntage / 100) * Convert.ToDouble(grossAmountTextBox.Text), 2);
            //discPerAmtFinalTextBox.Text = Convert.ToString(dDiscountAmount);

            double dRDAmount = Convert.ToDouble(rdAmtFinalTextBox.Text);

            
            double dOtherLessAny = Convert.ToDouble(addLessTextBox.Text);
            double dGrossAmount = Convert.ToDouble(grossAmountTextBox.Text);

            double dTaxableValue = dGrossAmount - dDiscountAmount - dRDAmount - dOtherLessAny;
            double dCgstAmount = Math.Round((Convert.ToDouble(cgstRateTextBox.Text) / 100) * dTaxableValue, 2);
            cgstAmtTextBox.Text = Convert.ToString(dCgstAmount);
            double dSgstAmount = Math.Round((Convert.ToDouble(sgstRateTextBox.Text) / 100) * dTaxableValue, 2);
            sgstAmtTextBox.Text = Convert.ToString(dSgstAmount);
            double dIgstAmount = Math.Round((Convert.ToDouble(igstRateTextBox.Text) / 100) * dTaxableValue, 2);
            igstAmtTextBox.Text = Convert.ToString(dIgstAmount);
            //double dTdsAmount = Math.Round(dTaxableValue * (dTdsPercentage / 100), 2);
            string companyStateCode = strCompanyGstn.Substring(0, 2);
            string partyStateCode = gstnTextBox.Text.Substring(0, 2);
            bool bLocalTaxInvoice = false;
            if (partyStateCode == companyStateCode)
                bLocalTaxInvoice = true;
            double dInvoiceValue = 0;
            if (bLocalTaxInvoice)
                dInvoiceValue = dTaxableValue + dCgstAmount + dCgstAmount;
            else
                dInvoiceValue = dTaxableValue + dIgstAmount;

            double dNetAmount = Math.Round(dInvoiceValue - dTdsAmount);
            invValueTextBox.Text = Convert.ToString(dInvoiceValue);
            taxValTextBox.Text = Convert.ToString(dTaxableValue);
            netAmtAftTdsTextBox.Text = Convert.ToString(dNetAmount);
        }

        private void cgstRateTextBox_TextChanged(object sender, EventArgs e)
        {
            double dCgstPercentage = 0;
            bool bFlag = double.TryParse(cgstRateTextBox.Text, out dCgstPercentage);
            if (!bFlag)
            {
                cgstRateTextBox.Text = "0";
                cgstRateTextBox.SelectAll();
                return;
            }
            double dDiscountAmount = Convert.ToDouble(discPerAmtFinalTextBox.Text);//Math.Round((dDiscountPercntage / 100) * Convert.ToDouble(grossAmountTextBox.Text), 2);
            //discPerAmtFinalTextBox.Text = Convert.ToString(dDiscountAmount);

            double dRDAmount = Convert.ToDouble(rdAmtFinalTextBox.Text);

            
            double dIgstAmount = Convert.ToDouble(igstAmtTextBox.Text);
            double dOtherLessAny = Convert.ToDouble(addLessTextBox.Text);
            double dGrossAmount = Convert.ToDouble(grossAmountTextBox.Text);

            double dTaxableValue = dGrossAmount - dDiscountAmount - dRDAmount - dOtherLessAny;
            double dCgstAmount = Math.Round((dCgstPercentage / 100) * dTaxableValue,2);
            double dSgstAmount = Convert.ToDouble(dCgstAmount);
            sgstRateTextBox.Text = Convert.ToString(dCgstPercentage);
            sgstAmtTextBox.Text = Convert.ToString(dSgstAmount);
            double dTdsAmount = Convert.ToDouble(tdsAmtTextBox2.Text);
            string companyStateCode = strCompanyGstn.Substring(0, 2);
            string partyStateCode = gstnTextBox.Text.Substring(0, 2);
            bool bLocalTaxInvoice = false;
            if (partyStateCode == companyStateCode)
                bLocalTaxInvoice = true;
            double dInvoiceValue = 0;
            if (bLocalTaxInvoice)
                dInvoiceValue = dTaxableValue + dCgstAmount + dCgstAmount;
            else
                dInvoiceValue = dTaxableValue + dIgstAmount;

            double dNetAmount = Math.Round(dInvoiceValue - dTdsAmount);
            invValueTextBox.Text = Convert.ToString(dInvoiceValue);
            taxValTextBox.Text = Convert.ToString(dTaxableValue);
            netAmtAftTdsTextBox.Text = Convert.ToString(dNetAmount);
        }

        private void sgstRateTextBox_TextChanged(object sender, EventArgs e)
        {
            double dSgstPercentage = 0;
            bool bFlag = double.TryParse(sgstRateTextBox.Text, out dSgstPercentage);
            if (!bFlag)
            {
                sgstRateTextBox.Text = "0";
                sgstRateTextBox.SelectAll();
                return;
            }
            double dDiscountAmount = Convert.ToDouble(discPerAmtFinalTextBox.Text);//Math.Round((dDiscountPercntage / 100) * Convert.ToDouble(grossAmountTextBox.Text), 2);
            //discPerAmtFinalTextBox.Text = Convert.ToString(dDiscountAmount);

            double dRDAmount = Convert.ToDouble(rdAmtFinalTextBox.Text);


            double dIgstAmount = Convert.ToDouble(igstAmtTextBox.Text);
            double dOtherLessAny = Convert.ToDouble(addLessTextBox.Text);
            double dGrossAmount = Convert.ToDouble(grossAmountTextBox.Text);

            double dTaxableValue = dGrossAmount - dDiscountAmount - dRDAmount - dOtherLessAny;
            double dSgstAmount = Math.Round((dSgstPercentage / 100) * dTaxableValue,2);
            double dCgstAmount = Convert.ToDouble(dSgstAmount);
            cgstRateTextBox.Text = Convert.ToString(dSgstPercentage);
            cgstAmtTextBox.Text = Convert.ToString(dCgstAmount);
            double dTdsAmount = Convert.ToDouble(tdsAmtTextBox2.Text);
            string companyStateCode = strCompanyGstn.Substring(0, 2);
            string partyStateCode = gstnTextBox.Text.Substring(0, 2);
            bool bLocalTaxInvoice = false;
            if (partyStateCode == companyStateCode)
                bLocalTaxInvoice = true;
            double dInvoiceValue = 0;
            if (bLocalTaxInvoice)
                dInvoiceValue = dTaxableValue + dCgstAmount + dCgstAmount;
            else
                dInvoiceValue = dTaxableValue + dIgstAmount;

            double dNetAmount = Math.Round(dInvoiceValue - dTdsAmount);
            invValueTextBox.Text = Convert.ToString(dInvoiceValue);
            taxValTextBox.Text = Convert.ToString(dTaxableValue);
            netAmtAftTdsTextBox.Text = Convert.ToString(dNetAmount);
        }

        private void igstRateTextBox_TextChanged(object sender, EventArgs e)
        {
            double dIgstPercentage = 0;
            bool bFlag = double.TryParse(igstRateTextBox.Text, out dIgstPercentage);
            if (!bFlag)
            {
                igstRateTextBox.Text = "0";
                igstRateTextBox.SelectAll();
                return;
            }
            double dDiscountAmount = Convert.ToDouble(discPerAmtFinalTextBox.Text);//Math.Round((dDiscountPercntage / 100) * Convert.ToDouble(grossAmountTextBox.Text), 2);
            //discPerAmtFinalTextBox.Text = Convert.ToString(dDiscountAmount);

            double dRDAmount = Convert.ToDouble(rdAmtFinalTextBox.Text);

            
            double dOtherLessAny = Convert.ToDouble(addLessTextBox.Text);
            double dGrossAmount = Convert.ToDouble(grossAmountTextBox.Text);

            double dTaxableValue = dGrossAmount - dDiscountAmount - dRDAmount - dOtherLessAny;

            double dIgstAmount = Math.Round(Convert.ToDouble(dTaxableValue *(dIgstPercentage/100)),2);
            double dSgstAmount = Convert.ToDouble(sgstAmtTextBox.Text);
            double dCgstAmount = Convert.ToDouble(cgstAmtTextBox.Text);
            
            
            double dTdsAmount = Convert.ToDouble(tdsAmtTextBox2.Text);
            string companyStateCode = strCompanyGstn.Substring(0, 2);
            string partyStateCode = gstnTextBox.Text.Substring(0, 2);
            bool bLocalTaxInvoice = false;
            if (partyStateCode == companyStateCode)
                bLocalTaxInvoice = true;
            double dInvoiceValue = 0;
            if (bLocalTaxInvoice)
                dInvoiceValue = dTaxableValue + dCgstAmount + dCgstAmount;
            else
                dInvoiceValue = dTaxableValue + dIgstAmount;

            double dNetAmount = Math.Round(dInvoiceValue - dTdsAmount);
            invValueTextBox.Text = Convert.ToString(dInvoiceValue);
            taxValTextBox.Text = Convert.ToString(dTaxableValue);
            netAmtAftTdsTextBox.Text = Convert.ToString(dNetAmount);
        }

        private void igstAmtTextBox_TextChanged(object sender, EventArgs e)
        {
            double dIgstAmount = 0;
            bool bFlag = double.TryParse(igstAmtTextBox.Text, out dIgstAmount);
            if (!bFlag)
            {
                igstAmtTextBox.Text = "0";
                igstAmtTextBox.SelectAll();
                return;
            }
            double dDiscountAmount = Convert.ToDouble(discPerAmtFinalTextBox.Text);//Math.Round((dDiscountPercntage / 100) * Convert.ToDouble(grossAmountTextBox.Text), 2);
            //discPerAmtFinalTextBox.Text = Convert.ToString(dDiscountAmount);

            double dRDAmount = Convert.ToDouble(rdAmtFinalTextBox.Text);


            double dOtherLessAny = Convert.ToDouble(addLessTextBox.Text);
            double dGrossAmount = Convert.ToDouble(grossAmountTextBox.Text);

            double dTaxableValue = dGrossAmount - dDiscountAmount - dRDAmount - dOtherLessAny;

            //double dIgstAmount = Math.Round(Convert.ToDouble(dTaxableValue * (dIgstPercentage / 100)), 2);
            double dSgstAmount = Convert.ToDouble(sgstAmtTextBox.Text);
            double dCgstAmount = Convert.ToDouble(cgstAmtTextBox.Text);


            double dTdsAmount = Convert.ToDouble(tdsAmtTextBox2.Text);
            string companyStateCode = strCompanyGstn.Substring(0, 2);
            string partyStateCode = gstnTextBox.Text.Substring(0, 2);
            bool bLocalTaxInvoice = false;
            if (partyStateCode == companyStateCode)
                bLocalTaxInvoice = true;
            double dInvoiceValue = 0;
            if (bLocalTaxInvoice)
                dInvoiceValue = dTaxableValue + dCgstAmount + dCgstAmount;
            else
                dInvoiceValue = dTaxableValue + dIgstAmount;

            double dNetAmount = Math.Round(dInvoiceValue - dTdsAmount);
            invValueTextBox.Text = Convert.ToString(dInvoiceValue);
            taxValTextBox.Text = Convert.ToString(dTaxableValue);
            netAmtAftTdsTextBox.Text = Convert.ToString(dNetAmount);
        }

        private void sgstAmtTextBox_TextChanged(object sender, EventArgs e)
        {
            double dSgstAmount = 0;
            bool bFlag = double.TryParse(sgstAmtTextBox.Text, out dSgstAmount);
            if (!bFlag)
            {
                igstAmtTextBox.Text = "0";
                igstAmtTextBox.SelectAll();
                return;
            }
            double dDiscountAmount = Convert.ToDouble(discPerAmtFinalTextBox.Text);//Math.Round((dDiscountPercntage / 100) * Convert.ToDouble(grossAmountTextBox.Text), 2);
            //discPerAmtFinalTextBox.Text = Convert.ToString(dDiscountAmount);

            double dRDAmount = Convert.ToDouble(rdAmtFinalTextBox.Text);


            double dOtherLessAny = Convert.ToDouble(addLessTextBox.Text);
            double dGrossAmount = Convert.ToDouble(grossAmountTextBox.Text);

            double dTaxableValue = dGrossAmount - dDiscountAmount - dRDAmount - dOtherLessAny;

            double dIgstAmount = Convert.ToDouble(igstAmtTextBox.Text);
            //double dSgstAmount = Convert.ToDouble(sgstAmtTextBox);
            double dCgstAmount = dSgstAmount;//Convert.ToDouble(cgstAmtTextBox);
            cgstAmtTextBox.Text = Convert.ToString(dCgstAmount);

            double dTdsAmount = Convert.ToDouble(tdsAmtTextBox2.Text);
            string companyStateCode = strCompanyGstn.Substring(0, 2);
            string partyStateCode = gstnTextBox.Text.Substring(0, 2);
            bool bLocalTaxInvoice = false;
            if (partyStateCode == companyStateCode)
                bLocalTaxInvoice = true;
            double dInvoiceValue = 0;
            if (bLocalTaxInvoice)
                dInvoiceValue = dTaxableValue + dCgstAmount + dCgstAmount;
            else
                dInvoiceValue = dTaxableValue + dIgstAmount;

            double dNetAmount = Math.Round(dInvoiceValue - dTdsAmount);
            invValueTextBox.Text = Convert.ToString(dInvoiceValue);
            taxValTextBox.Text = Convert.ToString(dTaxableValue);
            netAmtAftTdsTextBox.Text = Convert.ToString(dNetAmount);
        }

        private void cgstAmtTextBox_TextChanged(object sender, EventArgs e)
        {
            double dCgstAmount = 0;
            bool bFlag = double.TryParse(cgstAmtTextBox.Text, out dCgstAmount);
            if (!bFlag)
            {
                cgstAmtTextBox.Text = "0";
                cgstAmtTextBox.SelectAll();
                return;
            }
            double dDiscountAmount = Convert.ToDouble(discPerAmtFinalTextBox.Text);//Math.Round((dDiscountPercntage / 100) * Convert.ToDouble(grossAmountTextBox.Text), 2);
            //discPerAmtFinalTextBox.Text = Convert.ToString(dDiscountAmount);

            double dRDAmount = Convert.ToDouble(rdAmtFinalTextBox.Text);


            double dOtherLessAny = Convert.ToDouble(addLessTextBox.Text);
            double dGrossAmount = Convert.ToDouble(grossAmountTextBox.Text);

            double dTaxableValue = dGrossAmount - dDiscountAmount - dRDAmount - dOtherLessAny;

            double dIgstAmount = Convert.ToDouble(igstAmtTextBox.Text);
            double dSgstAmount = dCgstAmount;// Convert.ToDouble(sgstAmtTextBox);
            sgstAmtTextBox.Text = Convert.ToString(dSgstAmount);
            //double dCgstAmount = Convert.ToDouble(cgstAmtTextBox);


            double dTdsAmount = Convert.ToDouble(tdsAmtTextBox2.Text);
            string companyStateCode = strCompanyGstn.Substring(0, 2);
            string partyStateCode = gstnTextBox.Text.Substring(0, 2);
            bool bLocalTaxInvoice = false;
            if (partyStateCode == companyStateCode)
                bLocalTaxInvoice = true;
            double dInvoiceValue = 0;
            if (bLocalTaxInvoice)
                dInvoiceValue = dTaxableValue + dCgstAmount + dCgstAmount;
            else
                dInvoiceValue = dTaxableValue + dIgstAmount;

            double dNetAmount = Math.Round(dInvoiceValue - dTdsAmount);
            invValueTextBox.Text = Convert.ToString(dInvoiceValue);
            taxValTextBox.Text = Convert.ToString(dTaxableValue);
            netAmtAftTdsTextBox.Text = Convert.ToString(dNetAmount);
        }

        private void addLessTextBox_TextChanged(object sender, EventArgs e)
        {
            double dOtherLessAny = 0;
            bool bFlag = double.TryParse(addLessTextBox.Text, out dOtherLessAny);
            if (!bFlag)
            {
                addLessTextBox.Text = "0";
                addLessTextBox.SelectAll();
                return;
            }
            double dDiscountAmount = Convert.ToDouble(discPerAmtFinalTextBox.Text);//Math.Round((dDiscountPercntage / 100) * Convert.ToDouble(grossAmountTextBox.Text), 2);
            //discPerAmtFinalTextBox.Text = Convert.ToString(dDiscountAmount);

            double dRDAmount = Convert.ToDouble(rdAmtFinalTextBox.Text);


            //double dOtherLessAny = Convert.ToDouble(addLessTextBox.Text);
            double dGrossAmount = Convert.ToDouble(grossAmountTextBox.Text);

            double dTaxableValue = dGrossAmount - dDiscountAmount - dRDAmount - dOtherLessAny;

            double dCgstAmount = Math.Round((Convert.ToDouble(cgstRateTextBox.Text) / 100) * dTaxableValue, 2);
            cgstAmtTextBox.Text = Convert.ToString(dCgstAmount);
            double dSgstAmount = Math.Round((Convert.ToDouble(sgstRateTextBox.Text) / 100) * dTaxableValue, 2);
            sgstAmtTextBox.Text = Convert.ToString(dSgstAmount);
            double dIgstAmount = Math.Round((Convert.ToDouble(igstRateTextBox.Text) / 100) * dTaxableValue, 2);
            igstAmtTextBox.Text = Convert.ToString(dIgstAmount);


            double dTdsAmount = Convert.ToDouble(tdsAmtTextBox2.Text);
            string companyStateCode = strCompanyGstn.Substring(0, 2);
            string partyStateCode = gstnTextBox.Text.Substring(0, 2);
            bool bLocalTaxInvoice = false;
            if (partyStateCode == companyStateCode)
                bLocalTaxInvoice = true;
            double dInvoiceValue = 0;
            if (bLocalTaxInvoice)
                dInvoiceValue = dTaxableValue + dCgstAmount + dCgstAmount;
            else
                dInvoiceValue = dTaxableValue + dIgstAmount;

            double dNetAmount = Math.Round(dInvoiceValue - dTdsAmount);
            invValueTextBox.Text = Convert.ToString(dInvoiceValue);
            taxValTextBox.Text = Convert.ToString(dTaxableValue);
            netAmtAftTdsTextBox.Text = Convert.ToString(dNetAmount);
        }

        private void grossAmountTextBox_TextChanged(object sender, EventArgs e)
        {
            
            double dDiscountAmount = Convert.ToDouble(discPerAmtFinalTextBox.Text);//Math.Round((dDiscountPercntage / 100) * Convert.ToDouble(grossAmountTextBox.Text), 2);
            //discPerAmtFinalTextBox.Text = Convert.ToString(dDiscountAmount);

            double dRDAmount = Convert.ToDouble(rdAmtFinalTextBox.Text);


            double dOtherLessAny = Convert.ToDouble(addLessTextBox.Text);
            double dGrossAmount = Convert.ToDouble(grossAmountTextBox.Text);

            double dTaxableValue = dGrossAmount - dDiscountAmount - dRDAmount - dOtherLessAny;

            double dIgstAmount = Convert.ToDouble(igstAmtTextBox.Text);
            double dSgstAmount = Convert.ToDouble(sgstAmtTextBox.Text);
            //sgstAmtTextBox.Text = Convert.ToString(dSgstAmount);
            double dCgstAmount = Convert.ToDouble(cgstAmtTextBox.Text);


            double dTdsAmount = Convert.ToDouble(tdsAmtTextBox2.Text);
            string companyStateCode = strCompanyGstn.Substring(0, 2);
            string partyStateCode = gstnTextBox.Text.Substring(0, 2);
            bool bLocalTaxInvoice = false;
            if (partyStateCode == companyStateCode)
                bLocalTaxInvoice = true;
            double dInvoiceValue = 0;
            if (bLocalTaxInvoice)
                dInvoiceValue = dTaxableValue + dCgstAmount + dCgstAmount;
            else
                dInvoiceValue = dTaxableValue + dIgstAmount;

            double dNetAmount = Math.Round(dInvoiceValue - dTdsAmount);
            invValueTextBox.Text = Convert.ToString(dInvoiceValue);
            taxValTextBox.Text = Convert.ToString(dTaxableValue);
            netAmtAftTdsTextBox.Text = Convert.ToString(dNetAmount);
        }

        private void billNoTextBox_Validating_1(object sender, CancelEventArgs e)
        {
            if (billNoTextBox.Text == "")
            {
                MessageBox.Show("Please enter Bill No.", "BILL NO", MessageBoxButtons.OK);
                billNoTextBox.Focus();
                return;
            }
        }

        private void createButton_Click(object sender, EventArgs e)
        {

            millreceipt_detail millReceiptDetails = new millreceipt_detail();
            millReceiptDetails.receiptVoucherNo = Convert.ToInt64(voucherNoTextBox.Text);
            millReceiptDetails.addLessAny = Convert.ToDouble(addLessTextBox.Text);
            millReceiptDetails.billNo = (string)billNoTextBox.Text;
            millReceiptDetails.cgstAmount = Convert.ToDouble(cgstAmtTextBox.Text);
            millReceiptDetails.cgstPercentage = Convert.ToDouble(cgstRateTextBox.Text);
            millReceiptDetails.discountAmount = Convert.ToDouble(discPerAmtFinalTextBox.Text);
            millReceiptDetails.disountPercentage = Convert.ToDouble(discPerTextBox.Text);
            millReceiptDetails.greyMtrs = Convert.ToDouble(totalGreyMtrsTextBox.Text);
            millReceiptDetails.grossAmount = Convert.ToDouble(grossAmountTextBox.Text);
            millReceiptDetails.hsnCode = hsnCodeTextBox.Text;
            millReceiptDetails.igstAmount = Convert.ToDouble(igstAmtTextBox.Text);
            millReceiptDetails.igstPercentage = Convert.ToDouble(igstRateTextBox.Text);
            millReceiptDetails.invoiceValue = Convert.ToDouble(invValueTextBox.Text);
            millReceiptDetails.mill_id = (long)partyNameComboBox.SelectedValue;
            millReceiptDetails.netAmountAfterTds = Convert.ToDouble(netAmtAftTdsTextBox.Text);
            millReceiptDetails.rdAmount = Convert.ToDouble(rdAmtFinalTextBox.Text);
            millReceiptDetails.rdPerMtr = Convert.ToDouble(reMtrTextBox.Text);
            millReceiptDetails.recdMtrs = Convert.ToDouble(totalRecMtrsTextBox.Text);
            millReceiptDetails.recdTaka = Convert.ToInt64(totalRecTakaTextBox.Text);
            millReceiptDetails.receiptVoucherNo = Convert.ToInt64(voucherNoTextBox.Text);
            millReceiptDetails.remark = remarkTextBox.Text;
            millReceiptDetails.sgstAmount = Convert.ToDouble(sgstAmtTextBox.Text);
            millReceiptDetails.sgstPercentage = Convert.ToDouble(sgstRateTextBox.Text);
            millReceiptDetails.taxableValue = Convert.ToDouble(taxValTextBox.Text);
            millReceiptDetails.tdsAmount = Convert.ToDouble(tdsAmtTextBox2.Text);
            millReceiptDetails.tdsPercentage = Convert.ToDouble(tdsPerTextBox.Text);
            millReceiptDetails.date = DateTime.ParseExact(dateTextBox.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            List<millchallandispatchtaka_detail> updatedTakaList = new List<millchallandispatchtaka_detail>();
            for(int i = 0;i < millReceiptChallanTakaDetails.Count;i++)
            {
                List<millchallandispatchtaka_detail> tempList = millReceiptChallanTakaDetails[i].ToList();
                for(int j = 0;j < tempList.Count;j++)
                {
                    tempList[j].lotNo = millReceiptDataGridView.Rows[i].Cells[lotNoTextBox.Index].EditedFormattedValue.ToString();
                    tempList[j].marka = millReceiptDataGridView.Rows[i].Cells[markaTextBox.Index].EditedFormattedValue.ToString();
                    tempList[j].rate = Convert.ToDouble(millReceiptDataGridView.Rows[i].Cells[jobRateTextBox.Index].EditedFormattedValue.ToString());
                    tempList[j].jobAmount = Convert.ToDouble(millReceiptDataGridView.Rows[i].Cells[jobAmountTextBox.Index].EditedFormattedValue.ToString());
                    DataGridViewCheckBoxCell chkBoxCell = (DataGridViewCheckBoxCell)millReceiptDataGridView.Rows[i].Cells[reprocessTakaCheckBox.Index];
                    
                    if ((bool)chkBoxCell.Value == true && (bool)tempList[j].isReceived)
                        tempList[j].reProcessTaka = 1;
                    else
                        tempList[j].reProcessTaka = 0;
                    //tempList[j].receiptVoucherNo = Convert.ToInt64(voucherNoTextBox.Text);
                    updatedTakaList.Add(tempList[j]);
                    //millReceiptDetails.millchallandispatchtaka_detail.Add(tempList);
                }
            }
            millReceiptDetails.millchallandispatchtaka_detail = updatedTakaList;
            bool bVoucherAdded = partyDB.AddMillReceiptVoucher(millReceiptDetails);
            if(bVoucherAdded)
            {
                MessageBox.Show("Voucher Added", "SUCCESS", MessageBoxButtons.OK);
                resetAllData();
                setDefaultData();
            }
            else
            {
                MessageBox.Show("Voucher not Added", "SUCCESS", MessageBoxButtons.OK);
            }
        }

        private void voucherNoTextBox_Validating(object sender, CancelEventArgs e)
        {

        }

        private void voucherNoTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                bVoucherViewMode = true;
                millReceiptVoucherList = partyDB.getMillReceiptVoucherDetail(Convert.ToInt64(voucherNoTextBox.Text));
                if (millReceiptVoucherList == null)
                {
                    MessageBox.Show("Voucher does not exist", "ERROR", MessageBoxButtons.OK);
                    setDefaultData();
                    resetAllData();
                    return;
                }
                    
                millReceiptChallanTakaDetails.Clear();
                millReceiptDataGridView.Rows.Clear();
                billNoTextBox.Text = millReceiptVoucherList.billNo;
                DateTime dt, dt1;
                bool bFlag = DateTime.TryParse(millReceiptVoucherList.date.ToString(), out dt);
                if (bFlag)
                {
                    dt1 = dt.Date;
                    //DateTime.Parse(dt1.ToString()).ToString("dd-MM-yyyy");
                    dateTextBox.Text = dt1.ToString("dd-MM-yyyy");
                }
                partyNameComboBox.SelectedValue = millReceiptVoucherList.mill_id;
                gstnTextBox.Text = DB.getPartyGstn((long)partyNameComboBox.SelectedValue);
                hsnCodeTextBox.Text = millReceiptVoucherList.hsnCode;
                remarkTextBox.Text = millReceiptVoucherList.remark;
                totalGreyMtrsTextBox.Text = Convert.ToString(millReceiptVoucherList.greyMtrs);
                totalRecMtrsTextBox.Text = Convert.ToString(millReceiptVoucherList.recdMtrs);
                totalRecTakaTextBox.Text = Convert.ToString(millReceiptVoucherList.recdTaka);
                grossAmountTextBox.Text = Convert.ToString(millReceiptVoucherList.grossAmount);
                discPerTextBox.Text = Convert.ToString(millReceiptVoucherList.disountPercentage);
                discPerAmtFinalTextBox.Text = Convert.ToString(millReceiptVoucherList.discountAmount);
                reMtrTextBox.Text = Convert.ToString(millReceiptVoucherList.rdPerMtr);
                rdAmtFinalTextBox.Text = Convert.ToString(millReceiptVoucherList.rdAmount);
                tdsPerTextBox.Text = Convert.ToString(millReceiptVoucherList.tdsPercentage);
                tdsAmtTextBox2.Text = Convert.ToString(millReceiptVoucherList.tdsAmount);
                cgstRateTextBox.Text = Convert.ToString(millReceiptVoucherList.cgstPercentage);
                cgstAmtTextBox.Text = Convert.ToString(millReceiptVoucherList.cgstAmount);
                sgstRateTextBox.Text = Convert.ToString(millReceiptVoucherList.sgstPercentage);
                sgstAmtTextBox.Text = Convert.ToString(millReceiptVoucherList.sgstAmount);
                igstRateTextBox.Text = Convert.ToString(millReceiptVoucherList.igstPercentage);
                igstAmtTextBox.Text = Convert.ToString(millReceiptVoucherList.igstAmount);
                addLessTextBox.Text = Convert.ToString(millReceiptVoucherList.addLessAny);
                taxValTextBox.Text = Convert.ToString(millReceiptVoucherList.taxableValue);
                invValueTextBox.Text = Convert.ToString(millReceiptVoucherList.invoiceValue);
                netAmtAftTdsTextBox.Text = Convert.ToString(millReceiptVoucherList.netAmountAfterTds);

                List<millchallandispatchtaka_detail> tempList = new List<millchallandispatchtaka_detail>();
                tempList = millReceiptVoucherList.millchallandispatchtaka_detail.ToList();
                List<millchallandispatchtaka_detail> rowWiseList = new List<millchallandispatchtaka_detail>();
                //rowWiseList.Add(tempList[0]);
                var distintObject = tempList.Select(x => x.challan_id).Distinct();
                var lChallanIdList = distintObject.ToList();
                for(int i = 0;i < lChallanIdList.Count;i++)
                {
                    long lChallnId = (long)lChallanIdList[i];
                    List<millchallandispatchtaka_detail> takaChallanList = new List<millchallandispatchtaka_detail>();
                    takaChallanList = tempList.Where(x => x.challan_id == lChallnId).ToList();
                    millReceiptChallanTakaDetails.Add(takaChallanList);
                }

                //millReceiptDataGridView.Rows.Clear();
                for (int i =0;i < millReceiptChallanTakaDetails.Count;i++)
                {
                    List<millchallandispatchtaka_detail> takachallanDetails = millReceiptChallanTakaDetails[0].ToList();
                    if(takachallanDetails != null)
                    {
                        millReceiptDataGridView.Rows.Add();
                        long lChallanId = (long)takachallanDetails[0].challan_id;
                        List<milldispatchchallan_detail> challanData = partyDB.getChallanDetail(lChallanId);
                        if (challanData == null)
                        {
                            MessageBox.Show("Unable to fetch data", "ERROR", MessageBoxButtons.OK);
                            return;
                        }

                        DataGridViewComboBoxCell cell = (DataGridViewComboBoxCell)millReceiptDataGridView.Rows[i].Cells[challanNoComboBox.Index];
                        cell.ValueMember = "challan_id";
                        cell.DisplayMember = "challanNo";
                        cell.DataSource = challanData;
                        cell.ReadOnly = true;

                        millReceiptDataGridView.Rows[i].Cells[challanNoComboBox.Index].Value = takachallanDetails[0].challan_id;
                        millReceiptDataGridView.Rows[i].Cells[lotNoTextBox.Index].Value = takachallanDetails[0].lotNo;
                        millReceiptDataGridView.Rows[i].Cells[greyQualityComboBox.Index].Value = challanData[0].quality_id;
                        millReceiptDataGridView.Rows[i].Cells[recdCardNoTextBox.Index].Value = challanData[0].cardNo;
                        //millReceiptDataGridView.Rows[i].Cells[challanNoComboBox.Index].Value = takachallanDetails[0].challan_id;
                        long lNoOfTakas = 0;
                        long lNoOfTP = 0;
                        double dTotalRecdMtrs = 0, dTotalGreyMtrs = 0;
                        bool bReprocessTaka = false;
                        for (int j = 0; j < takachallanDetails.Count; j++)
                        {
                            if ((bool)takachallanDetails[j].isReceived)
                            {
                                double dFinMtrs = 0;

                                string finishTakaMtrs = takachallanDetails[j].finMtrs;
                                var finishTakaMtrsAfterSplit = finishTakaMtrs.Split('+');
                                for (int k = 0; k < finishTakaMtrsAfterSplit.Count(); k++)
                                {
                                    double dTempFinishMtrs = 0;
                                    //double.TryParse(millReceivedTakaDataGridView.Rows[i].Cells[finMtrsGridTextBox.Index].EditedFormattedValue.ToString(),out dTempFinMtrs);
                                    double.TryParse(finishTakaMtrsAfterSplit[k], out dTempFinishMtrs);
                                    dFinMtrs = dFinMtrs + dTempFinishMtrs;
                                    if (k > 0 && finishTakaMtrsAfterSplit.Count() > 1)
                                        lNoOfTP++;
                                }

                                double dGreyMtrs = 0;

                                string greyTakaMtrs = takachallanDetails[j].greyMtrs;
                                var greyTakaMtrsAfterSplit = greyTakaMtrs.Split('+');
                                for (int k = 0; k < greyTakaMtrsAfterSplit.Count(); k++)
                                {
                                    double dTempGreyMtrs = 0;
                                    //double.TryParse(millReceivedTakaDataGridView.Rows[i].Cells[finMtrsGridTextBox.Index].EditedFormattedValue.ToString(),out dTempFinMtrs);
                                    double.TryParse(greyTakaMtrsAfterSplit[k], out dTempGreyMtrs);
                                    dGreyMtrs = dGreyMtrs + dTempGreyMtrs;
                                }

                                if (!bReprocessTaka && takachallanDetails[j].reProcessTaka == 1)
                                    bReprocessTaka = true;
                                dTotalGreyMtrs = dTotalGreyMtrs + dGreyMtrs;
                                dTotalRecdMtrs = dTotalRecdMtrs + dFinMtrs;
                                lNoOfTakas++;
                            }
                        }

                         DataGridViewCheckBoxCell chkBoxCell = (DataGridViewCheckBoxCell)millReceiptDataGridView.Rows[i].Cells[reprocessTakaCheckBox.Index];
                    if (bReprocessTaka)
                        chkBoxCell.Value = 1;
                    else
                        chkBoxCell.Value = 0;
                        millReceiptDataGridView.Rows[i].Cells[takaTextBox.Index].Value = Convert.ToString(lNoOfTakas + lNoOfTP);
                        millReceiptDataGridView.Rows[i].Cells[recMtrsTextBox.Index].Value = Convert.ToString(dTotalRecdMtrs);
                        millReceiptDataGridView.Rows[i].Cells[greyMtrsTextBox.Index].Value = Convert.ToString(dTotalGreyMtrs);
                        millReceiptDataGridView.Rows[i].Cells[jobRateTextBox.Index].Value = takachallanDetails[0].rate;
                        millReceiptDataGridView.Rows[i].Cells[jobAmountTextBox.Index].Value = takachallanDetails[0].jobAmount;
                    }
                    
                }
                millReceiptDataGridView.Rows[millReceiptChallanTakaDetails.Count].ReadOnly = true;
                createButton.Enabled = false;
                viewButton.Enabled = true;
                updateButton.Enabled = true;
                deleteButton.Enabled = true;
            }





        }

        private void updateButton_Click(object sender, EventArgs e)
        {
            //millreceipt_detail millReceiptDetails = new millreceipt_detail();
            millreceipt_detail millReceiptDetails = millReceiptVoucherList;
            millReceiptDetails.receiptVoucherNo = Convert.ToInt64(voucherNoTextBox.Text);
            millReceiptDetails.addLessAny = Convert.ToDouble(addLessTextBox.Text);
            millReceiptDetails.billNo = (string)billNoTextBox.Text;
            millReceiptDetails.cgstAmount = Convert.ToDouble(cgstAmtTextBox.Text);
            millReceiptDetails.cgstPercentage = Convert.ToDouble(cgstRateTextBox.Text);
            millReceiptDetails.discountAmount = Convert.ToDouble(discPerAmtFinalTextBox.Text);
            millReceiptDetails.disountPercentage = Convert.ToDouble(discPerTextBox.Text);
            millReceiptDetails.greyMtrs = Convert.ToDouble(totalGreyMtrsTextBox.Text);
            millReceiptDetails.grossAmount = Convert.ToDouble(grossAmountTextBox.Text);
            millReceiptDetails.hsnCode = hsnCodeTextBox.Text;
            millReceiptDetails.igstAmount = Convert.ToDouble(igstAmtTextBox.Text);
            millReceiptDetails.igstPercentage = Convert.ToDouble(igstRateTextBox.Text);
            millReceiptDetails.invoiceValue = Convert.ToDouble(invValueTextBox.Text);
            millReceiptDetails.mill_id = (long)partyNameComboBox.SelectedValue;
            millReceiptDetails.netAmountAfterTds = Convert.ToDouble(netAmtAftTdsTextBox.Text);
            millReceiptDetails.rdAmount = Convert.ToDouble(rdAmtFinalTextBox.Text);
            millReceiptDetails.rdPerMtr = Convert.ToDouble(reMtrTextBox.Text);
            millReceiptDetails.recdMtrs = Convert.ToDouble(totalRecMtrsTextBox.Text);
            millReceiptDetails.recdTaka = Convert.ToInt64(totalRecTakaTextBox.Text);
            millReceiptDetails.receiptVoucherNo = Convert.ToInt64(voucherNoTextBox.Text);
            millReceiptDetails.remark = remarkTextBox.Text;
            millReceiptDetails.sgstAmount = Convert.ToDouble(sgstAmtTextBox.Text);
            millReceiptDetails.sgstPercentage = Convert.ToDouble(sgstRateTextBox.Text);
            millReceiptDetails.taxableValue = Convert.ToDouble(taxValTextBox.Text);
            millReceiptDetails.tdsAmount = Convert.ToDouble(tdsAmtTextBox2.Text);
            millReceiptDetails.tdsPercentage = Convert.ToDouble(tdsPerTextBox.Text);
            millReceiptDetails.date = DateTime.ParseExact(dateTextBox.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            List<millchallandispatchtaka_detail> updatedTakaList = new List<millchallandispatchtaka_detail>();
            for (int i = 0; i < millReceiptChallanTakaDetails.Count; i++)
            {
                List<millchallandispatchtaka_detail> tempList = millReceiptChallanTakaDetails[i].ToList();
                for (int j = 0; j < tempList.Count; j++)
                {
                    tempList[j].lotNo = millReceiptDataGridView.Rows[i].Cells[lotNoTextBox.Index].EditedFormattedValue.ToString();
                    tempList[j].marka = millReceiptDataGridView.Rows[i].Cells[markaTextBox.Index].EditedFormattedValue.ToString();
                    tempList[j].rate = Convert.ToDouble(millReceiptDataGridView.Rows[i].Cells[jobRateTextBox.Index].EditedFormattedValue.ToString());
                    tempList[j].jobAmount = Convert.ToDouble(millReceiptDataGridView.Rows[i].Cells[jobAmountTextBox.Index].EditedFormattedValue.ToString());
                    //tempList[j].receiptVoucherNo = Convert.ToInt64(voucherNoTextBox.Text);
                    DataGridViewCheckBoxCell chkBoxCell = (DataGridViewCheckBoxCell)millReceiptDataGridView.Rows[i].Cells[reprocessTakaCheckBox.Index];
                    double dFinMtrs = 0;
                    double.TryParse(tempList[j].finMtrs, out dFinMtrs);
                    if ((bool)chkBoxCell.Value == true && (bool)tempList[j].isReceived)
                        tempList[j].reProcessTaka = 1;
                    else
                        tempList[j].reProcessTaka = 0;
                    //List<milldispatchchallan_detail> tempChallan = partyDB.getChallanDetail((long)tempList[j].challan_id);
                    //millReceiptDetails.millchallandispatchtaka_detail.Add(tempList);
                    tempList[j].milldispatchchallan_detail = null;
                    updatedTakaList.Add(tempList[j]);
                }
            }
            millReceiptDetails.millchallandispatchtaka_detail = updatedTakaList;
            bool bVoucherAdded = partyDB.UpdateMillReceiptVoucher(millReceiptDetails);
            if (bVoucherAdded)
            {
                MessageBox.Show("Voucher Updated", "SUCCESS", MessageBoxButtons.OK);
                setDefaultData();
                resetAllData();
            }
            else
            {
                MessageBox.Show("Voucher not Updated", "ERROR", MessageBoxButtons.OK);
            }
        }

        private void viewButton_Click(object sender, EventArgs e)
        {
            bVoucherViewMode = true;
            millReceiptVoucherList = partyDB.getMillReceiptVoucherDetail(Convert.ToInt64(voucherNoTextBox.Text));
            if (millReceiptVoucherList == null)
            {
                MessageBox.Show("Voucher does not exist", "ERROR", MessageBoxButtons.OK);
                setDefaultData();
                resetAllData();
                return;
            }
            millReceiptChallanTakaDetails.Clear();
            billNoTextBox.Text = millReceiptVoucherList.billNo;
            DateTime dt, dt1;
            bool bFlag = DateTime.TryParse(millReceiptVoucherList.date.ToString(), out dt);
            if (bFlag)
            {
                dt1 = dt.Date;
                //DateTime.Parse(dt1.ToString()).ToString("dd-MM-yyyy");
                dateTextBox.Text = dt1.ToString("dd-MM-yyyy");
            }
            partyNameComboBox.SelectedValue = millReceiptVoucherList.mill_id;
            gstnTextBox.Text = DB.getPartyGstn((long)partyNameComboBox.SelectedValue);
            hsnCodeTextBox.Text = millReceiptVoucherList.hsnCode;
            remarkTextBox.Text = millReceiptVoucherList.remark;
            totalGreyMtrsTextBox.Text = Convert.ToString(millReceiptVoucherList.greyMtrs);
            totalRecMtrsTextBox.Text = Convert.ToString(millReceiptVoucherList.recdMtrs);
            totalRecTakaTextBox.Text = Convert.ToString(millReceiptVoucherList.recdTaka);
            grossAmountTextBox.Text = Convert.ToString(millReceiptVoucherList.grossAmount);
            discPerTextBox.Text = Convert.ToString(millReceiptVoucherList.disountPercentage);
            discPerAmtFinalTextBox.Text = Convert.ToString(millReceiptVoucherList.discountAmount);
            reMtrTextBox.Text = Convert.ToString(millReceiptVoucherList.rdPerMtr);
            rdAmtFinalTextBox.Text = Convert.ToString(millReceiptVoucherList.rdAmount);
            tdsPerTextBox.Text = Convert.ToString(millReceiptVoucherList.tdsPercentage);
            tdsAmtTextBox2.Text = Convert.ToString(millReceiptVoucherList.tdsAmount);
            cgstRateTextBox.Text = Convert.ToString(millReceiptVoucherList.cgstPercentage);
            cgstAmtTextBox.Text = Convert.ToString(millReceiptVoucherList.cgstAmount);
            sgstRateTextBox.Text = Convert.ToString(millReceiptVoucherList.sgstPercentage);
            sgstAmtTextBox.Text = Convert.ToString(millReceiptVoucherList.sgstAmount);
            igstRateTextBox.Text = Convert.ToString(millReceiptVoucherList.igstPercentage);
            igstAmtTextBox.Text = Convert.ToString(millReceiptVoucherList.igstAmount);
            addLessTextBox.Text = Convert.ToString(millReceiptVoucherList.addLessAny);
            taxValTextBox.Text = Convert.ToString(millReceiptVoucherList.taxableValue);
            invValueTextBox.Text = Convert.ToString(millReceiptVoucherList.invoiceValue);
            netAmtAftTdsTextBox.Text = Convert.ToString(millReceiptVoucherList.netAmountAfterTds);

            List<millchallandispatchtaka_detail> tempList = new List<millchallandispatchtaka_detail>();
            tempList = millReceiptVoucherList.millchallandispatchtaka_detail.ToList();
            List<millchallandispatchtaka_detail> rowWiseList = new List<millchallandispatchtaka_detail>();
            //rowWiseList.Add(tempList[0]);
            var distintObject = tempList.Select(x => x.challan_id).Distinct();
            var lChallanIdList = distintObject.ToList();
            for (int i = 0; i < lChallanIdList.Count; i++)
            {
                long lChallnId = (long)lChallanIdList[i];
                List<millchallandispatchtaka_detail> takaChallanList = new List<millchallandispatchtaka_detail>();
                takaChallanList = tempList.Where(x => x.challan_id == lChallnId).ToList();
                millReceiptChallanTakaDetails.Add(takaChallanList);
            }



            millReceiptDataGridView.Rows.Clear();
            for (int i = 0; i < millReceiptChallanTakaDetails.Count; i++)
            {
                List<millchallandispatchtaka_detail> takachallanDetails = millReceiptChallanTakaDetails[i].ToList();
                if(takachallanDetails != null)
                {
                    millReceiptDataGridView.Rows.Add();
                    long lChallanId = (long)takachallanDetails[0].challan_id;
                    List<milldispatchchallan_detail> challanData = partyDB.getChallanDetail(lChallanId);
                    if (challanData == null)
                    {
                        MessageBox.Show("Unable to fetch data", "ERROR", MessageBoxButtons.OK);
                        return;
                    }

                    DataGridViewComboBoxCell cell = (DataGridViewComboBoxCell)millReceiptDataGridView.Rows[i].Cells[challanNoComboBox.Index];
                    cell.ValueMember = "challan_id";
                    cell.DisplayMember = "challanNo";
                    cell.DataSource = challanData;
                    cell.ReadOnly = true;

                    millReceiptDataGridView.Rows[i].Cells[challanNoComboBox.Index].Value = takachallanDetails[0].challan_id;
                    millReceiptDataGridView.Rows[i].Cells[lotNoTextBox.Index].Value = takachallanDetails[0].lotNo;
                    millReceiptDataGridView.Rows[i].Cells[greyQualityComboBox.Index].Value = challanData[0].quality_id;
                    millReceiptDataGridView.Rows[i].Cells[recdCardNoTextBox.Index].Value = challanData[0].cardNo;
                    millReceiptDataGridView.Rows[i].Cells[challanNoComboBox.Index].Value = takachallanDetails[0].challan_id;
                    
                    long lNoOfTakas = 0;
                    long lNoOfTP = 0;
                    double dTotalRecdMtrs = 0, dTotalGreyMtrs = 0;
                    bool bReprocessTaka = false;
                    for (int j = 0; j < takachallanDetails.Count; j++)
                    {
                        if ((bool)takachallanDetails[j].isReceived)
                        {

                            double dFinMtrs = 0;

                            string finishTakaMtrs = takachallanDetails[j].finMtrs;
                            var finishTakaMtrsAfterSplit = finishTakaMtrs.Split('+');
                            for (int k = 0; k < finishTakaMtrsAfterSplit.Count(); k++)
                            {
                                double dTempFinishMtrs = 0;
                                //double.TryParse(millReceivedTakaDataGridView.Rows[i].Cells[finMtrsGridTextBox.Index].EditedFormattedValue.ToString(),out dTempFinMtrs);
                                double.TryParse(finishTakaMtrsAfterSplit[k], out dTempFinishMtrs);
                                dFinMtrs = dFinMtrs + dTempFinishMtrs;
                                if (k > 0 && finishTakaMtrsAfterSplit.Count() > 1)
                                    lNoOfTP++;
                            }

                            double dGreyMtrs = 0;

                            string greyTakaMtrs = takachallanDetails[j].greyMtrs;
                            var greyTakaMtrsAfterSplit = greyTakaMtrs.Split('+');
                            for (int k = 0; k < greyTakaMtrsAfterSplit.Count(); k++)
                            {
                                double dTempGreyMtrs = 0;
                                //double.TryParse(millReceivedTakaDataGridView.Rows[i].Cells[finMtrsGridTextBox.Index].EditedFormattedValue.ToString(),out dTempFinMtrs);
                                double.TryParse(greyTakaMtrsAfterSplit[k], out dTempGreyMtrs);
                                dGreyMtrs = dGreyMtrs + dTempGreyMtrs;
                            }
                            if (!bReprocessTaka && takachallanDetails[j].reProcessTaka == 1)
                                bReprocessTaka = true;

                            dTotalGreyMtrs = dTotalGreyMtrs + dGreyMtrs;
                            dTotalRecdMtrs = dTotalRecdMtrs + dFinMtrs;
                            lNoOfTakas++;
                        }
                    }
                    DataGridViewCheckBoxCell chkBoxCell = (DataGridViewCheckBoxCell)millReceiptDataGridView.Rows[i].Cells[reprocessTakaCheckBox.Index];
                    if (bReprocessTaka)
                        chkBoxCell.Value = 1;
                    else
                        chkBoxCell.Value = 0;
                    millReceiptDataGridView.Rows[i].Cells[takaTextBox.Index].Value = Convert.ToString(lNoOfTakas + lNoOfTP);
                    millReceiptDataGridView.Rows[i].Cells[recMtrsTextBox.Index].Value = Convert.ToString(dTotalRecdMtrs);
                    millReceiptDataGridView.Rows[i].Cells[greyMtrsTextBox.Index].Value = Convert.ToString(dTotalGreyMtrs);
                    millReceiptDataGridView.Rows[i].Cells[jobRateTextBox.Index].Value = takachallanDetails[0].rate;
                    millReceiptDataGridView.Rows[i].Cells[jobAmountTextBox.Index].Value = takachallanDetails[0].jobAmount;
                }
                
            }
            millReceiptDataGridView.Rows[millReceiptChallanTakaDetails.Count].ReadOnly = true;
            createButton.Enabled = false;
            viewButton.Enabled = true;
            updateButton.Enabled = true;
            deleteButton.Enabled = true;
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            //millreceipt_detail millReceiptDetails = new millreceipt_detail();
            millreceipt_detail millReceiptDetails = millReceiptVoucherList;
            //millReceiptDetails.receiptVoucherNo = null;
            List<millchallandispatchtaka_detail> updatedTakaList = new List<millchallandispatchtaka_detail>();
            for (int i = 0; i < millReceiptChallanTakaDetails.Count; i++)
            {
                List<millchallandispatchtaka_detail> tempList = millReceiptChallanTakaDetails[i].ToList();
                for (int j = 0; j < tempList.Count; j++)
                {
                    tempList[j].lotNo = null;
                    tempList[j].marka = "";
                    tempList[j].rate = 0;
                    tempList[j].jobAmount = 0;
                    tempList[j].finMtrs = "0.0";
                    tempList[j].isReceived = false;
                    tempList[j].millvoucher_id = null;

                    //tempList[j].receiptVoucherNo = Convert.ToInt64(voucherNoTextBox.Text);
                    updatedTakaList.Add(tempList[j]);
                    //millReceiptDetails.millchallandispatchtaka_detail.Add(tempList);
                }
            }
            millReceiptDetails.millchallandispatchtaka_detail = updatedTakaList;
            millReceiptDetails.isDeleted = true;
            millReceiptDetails.receiptVoucherNo = null;
            bool bVoucherDeleted = partyDB.UpdateMillReceiptVoucher(millReceiptDetails);
            if (bVoucherDeleted)
            {
                MessageBox.Show("Voucher Deleted", "SUCCESS", MessageBoxButtons.OK);
                resetAllData();
                setDefaultData();
            }
            else
            {
                MessageBox.Show("Voucher not Deleted", "SUCCESS", MessageBoxButtons.OK);
            }
        }
    }
}
