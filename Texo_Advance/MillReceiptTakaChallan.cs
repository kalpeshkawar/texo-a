﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Texo_Advance
{
    public partial class MillReceiptTakaChallan : Form
    {
        List<millchallandispatchtaka_detail> millReceiptChallanTakaDetails;
        int NoOfTakas = 0;
        bool bTakaReceived = false;
        double dGreyMtrs = 0;
        int iNoOfTP = 0;
        public MillReceiptTakaChallan()
        {
            InitializeComponent();
        }

        private void MillReceiptTakaChallan_Load(object sender, EventArgs e)
        {

        }

        public void setDefaultData(List<millchallandispatchtaka_detail> challanDetail,bool bViewModeOnly)
        {
            millReceivedTakaDataGridView.Rows.Clear();
            int rowIndex = 0,iTakaCount = 0;
            iNoOfTP = 0;
            double dTotalGreyMtrs = 0, dTotalFinishMtrs = 0;
            for (int i = 0;i < challanDetail.Count;i++)
            {
                if(bViewModeOnly)
                {
                    millReceivedTakaDataGridView.Rows.Add();
                    millReceivedTakaDataGridView.Rows[rowIndex].Cells[taka_id.Index].Value = Convert.ToString(challanDetail[i].millTaka_id);
                    millReceivedTakaDataGridView.Rows[rowIndex].Cells[greyMtrsGridTextBox.Index].Value = Convert.ToString(challanDetail[i].greyMtrs);
                    if(challanDetail[i].finMtrs != null)
                    {
                        millReceivedTakaDataGridView.Rows[rowIndex].Cells[finMtrsGridTextBox.Index].Value = Convert.ToString(challanDetail[i].finMtrs);

                        double dFinMtrs = 0;
                        string takaMtrs = challanDetail[i].finMtrs;
                        var takaMtrsAfterSplit = takaMtrs.Split('+');
                        for (int j = 0; j < takaMtrsAfterSplit.Count(); j++)
                        {
                            double dTempFinMtrs = 0;
                            //double.TryParse(millReceivedTakaDataGridView.Rows[i].Cells[finMtrsGridTextBox.Index].EditedFormattedValue.ToString(),out dTempFinMtrs);
                            double.TryParse(takaMtrsAfterSplit[j], out dTempFinMtrs);
                            dFinMtrs = dFinMtrs + dTempFinMtrs;
                            if (j > 0 && takaMtrsAfterSplit.Count() > 1)
                                iNoOfTP++;
                        }

                        double dGreyMtrs = 0;
                        string greyTakaMtrs = challanDetail[i].greyMtrs;
                        var greyTakaMtrsAfterSplit = takaMtrs.Split('+');
                        for (int j = 0; j < greyTakaMtrsAfterSplit.Count(); j++)
                        {
                            double dTempGreyMtrs = 0;
                            //double.TryParse(millReceivedTakaDataGridView.Rows[i].Cells[finMtrsGridTextBox.Index].EditedFormattedValue.ToString(),out dTempFinMtrs);
                            double.TryParse(greyTakaMtrsAfterSplit[j], out dTempGreyMtrs);
                            dGreyMtrs = dGreyMtrs + dTempGreyMtrs;
                            
                        }
                        double dShortage = (double)((dGreyMtrs - dFinMtrs) / dGreyMtrs) * 100;
                        dTotalGreyMtrs = dTotalGreyMtrs + dGreyMtrs;
                        dTotalFinishMtrs = dTotalFinishMtrs + dFinMtrs;
                        iTakaCount++;
                        millReceivedTakaDataGridView.Rows[rowIndex].Cells[shortageGridTextBox.Index].Value = Convert.ToString(Math.Round(dShortage, 2));
                    }
                    rowIndex++;
                }
                else
                {
                    if ((bool)(!challanDetail[i].isReceived))
                    {
                        millReceivedTakaDataGridView.Rows.Add();
                        millReceivedTakaDataGridView.Rows[rowIndex].Cells[taka_id.Index].Value = Convert.ToString(challanDetail[i].millTaka_id);
                        millReceivedTakaDataGridView.Rows[rowIndex].Cells[greyMtrsGridTextBox.Index].Value = Convert.ToString(challanDetail[i].greyMtrs);
                        rowIndex++;
                    }
                    
                }
            }
            millReceiptChallanTakaDetails = challanDetail;
            NoOfTakas = challanDetail.Count;
            millReceivedTakaDataGridView.AllowUserToAddRows = false;
            if(bViewModeOnly)
            {
                recdMtrsTextBox.Text = Convert.ToString(Math.Round(dTotalFinishMtrs, 2));
                recdTakaTextBox.Text = Convert.ToString(iTakaCount);
                avgShortageTextBox.Text = Convert.ToString(Math.Round((dTotalGreyMtrs - dTotalFinishMtrs) / dTotalGreyMtrs * 100, 2));
                dGreyMtrs = dTotalGreyMtrs;
            }
        }

        private void millReceivedTakaDataGridView_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            double dFinMtrs = 0;
            double dTempMtrs = 0;
            int iNoOfTakas = 0;
            iNoOfTP = 0;
            double dTotalGreyMtrs = 0;
            if(millReceivedTakaDataGridView.Rows.Count-1 <= NoOfTakas )
            {
                for (int i = 0; i < millReceivedTakaDataGridView.Rows.Count; i++)
                {
                    dFinMtrs = 0;
                    string takaMtrs = millReceivedTakaDataGridView.Rows[i].Cells[finMtrsGridTextBox.Index].EditedFormattedValue.ToString();
                    var takaMtrsAfterSplit = takaMtrs.Split('+');
                    for(int j = 0;j < takaMtrsAfterSplit.Count();j++)
                    {
                        double dTempFinMtrs = 0;
                        //double.TryParse(millReceivedTakaDataGridView.Rows[i].Cells[finMtrsGridTextBox.Index].EditedFormattedValue.ToString(),out dTempFinMtrs);
                        double.TryParse(takaMtrsAfterSplit[j], out dTempFinMtrs);
                        dFinMtrs = dFinMtrs + dTempFinMtrs;
                        if (j > 0 && takaMtrsAfterSplit.Count() > 1)
                            iNoOfTP++;
                    }
                    //double.TryParse(millReceivedTakaDataGridView.Rows[i].Cells[finMtrsGridTextBox.Index].EditedFormattedValue.ToString(), out dFinMtrs);
                    if (dFinMtrs > 0)
                    {
                        double dGreyMtrs = Convert.ToDouble(millReceivedTakaDataGridView.Rows[i].Cells[greyMtrsGridTextBox.Index].EditedFormattedValue.ToString());
                        double dShortage = ((dGreyMtrs - dFinMtrs) / dGreyMtrs) * 100;
                        dTotalGreyMtrs = dTotalGreyMtrs + dGreyMtrs;
                        
                        dTempMtrs = dTempMtrs + dFinMtrs;
                        millReceivedTakaDataGridView.Rows[i].Cells[shortageGridTextBox.Index].Value = Convert.ToString(Math.Round(dShortage, 2));
                        iNoOfTakas++;
                    }

                }
            }
            else
            {
                for (int i = 0; i < NoOfTakas; i++)
                {
                    double.TryParse(millReceivedTakaDataGridView.Rows[i].Cells[finMtrsGridTextBox.Index].EditedFormattedValue.ToString(), out dFinMtrs);
                    if (dFinMtrs > 0)
                    {
                        double dGreyMtrs = Convert.ToDouble(millReceivedTakaDataGridView.Rows[i].Cells[greyMtrsGridTextBox.Index].EditedFormattedValue.ToString());
                        double dShortage = ((dGreyMtrs - dFinMtrs) / dGreyMtrs) * 100;
                        dTotalGreyMtrs = dTotalGreyMtrs + dGreyMtrs;
                        dTempMtrs = dTempMtrs + dFinMtrs;
                        millReceivedTakaDataGridView.Rows[i].Cells[shortageGridTextBox.Index].Value = Convert.ToString(Math.Round(dShortage, 2));
                        iNoOfTakas++;
                    }

                }
            }
            
            recdMtrsTextBox.Text = Convert.ToString(Math.Round(dTempMtrs,2));
            recdTakaTextBox.Text = Convert.ToString(iNoOfTakas);
            avgShortageTextBox.Text = Convert.ToString(Math.Round((dTotalGreyMtrs - dTempMtrs)/dTotalGreyMtrs *100,2));
            dGreyMtrs = dTotalGreyMtrs;
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < millReceiptChallanTakaDetails.Count; i++)
            {
                //if ((bool)(!millReceiptChallanTakaDetails[i].isReceived))
                {
                    for(int j = 0;j < millReceivedTakaDataGridView.RowCount;j++)
                    {
                        double dFinMtrs = 0;

                        string finishTakaMtrs = millReceivedTakaDataGridView.Rows[j].Cells[finMtrsGridTextBox.Index].EditedFormattedValue.ToString();
                        var greyTakaMtrsAfterSplit = finishTakaMtrs.Split('+');
                        for (int k = 0; k < greyTakaMtrsAfterSplit.Count(); k++)
                        {
                            double dTempFinishMtrs = 0;
                            //double.TryParse(millReceivedTakaDataGridView.Rows[i].Cells[finMtrsGridTextBox.Index].EditedFormattedValue.ToString(),out dTempFinMtrs);
                            double.TryParse(greyTakaMtrsAfterSplit[k], out dTempFinishMtrs);
                            dFinMtrs = dFinMtrs + dTempFinishMtrs;
                        }

                        //double.TryParse(millReceivedTakaDataGridView.Rows[j].Cells[finMtrsGridTextBox.Index].EditedFormattedValue.ToString(), out dFinMtrs);
                        long lTakaId = 0;
                        long.TryParse(millReceivedTakaDataGridView.Rows[j].Cells[taka_id.Index].EditedFormattedValue.ToString(), out lTakaId);
                        if (dFinMtrs > 0 && (lTakaId == millReceiptChallanTakaDetails[i].millTaka_id))
                        {
                            millReceiptChallanTakaDetails[i].finMtrs = finishTakaMtrs;
                            millReceiptChallanTakaDetails[i].isReceived = true;
                        }
                        else if (dFinMtrs == 0 && (lTakaId == millReceiptChallanTakaDetails[i].millTaka_id))
                        {
                            millReceiptChallanTakaDetails[i].finMtrs = finishTakaMtrs;
                            millReceiptChallanTakaDetails[i].isReceived = false;
                        }
                    }
                    
                }
            }
            bTakaReceived = true;
            MessageBox.Show("Taka Details Saved", "Taka Details", MessageBoxButtons.OK);
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public List<millchallandispatchtaka_detail> getMillReceiptTakaDetails()
        {
            return millReceiptChallanTakaDetails;
        }
        public string getTotalReceivedTaka()
        {
            return recdTakaTextBox.Text;
        }
        public string getTotalReceivedMtrs()
        {
            return recdMtrsTextBox.Text;
        }
        public bool getTakaReceivedStatus()
        {
            return bTakaReceived;
        }
        public double getGreyMtrs()
        {
            return dGreyMtrs;
        }

        public int getNoOfTPs()
        {
            return iNoOfTP;
        }
    }
}
