﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Cryptography;

namespace Texo_Advance.Reports
{
    public partial class Reports : UserControl
    {
        ReportType reportType;
        public Reports()
        {
            InitializeComponent();
        }
        private ReportMain mainForm = null;
        public Reports(ReportMain callingForm)
        {
            mainForm = callingForm as ReportMain;
            InitializeComponent();
        }

        private void visualButton1_Click(object sender, EventArgs e)
        {

        }

        private void visualTile1_Click(object sender, EventArgs e)
        {

        }

        private void visualTile3_Click(object sender, EventArgs e)
        {

        }

        private void Reports_MouseHover(object sender, EventArgs e)
        {
            childPanel.Visible = false;

        }

        private void SalesReport1_MouseHover(object sender, EventArgs e)
        {
            Point p = new Point(440, 24);
            childPanel.Location = p;
            childPanel.Visible = true;
            reportType = ReportType.SALES;
            
        }

        private void Reports_Load(object sender, EventArgs e)
        {
            
            childPanel.Visible = false;
        }

        private void SalesReport1_MouseLeave(object sender, EventArgs e)
        {
            //childPanel.Visible = false;
        }

        private void Reports_MouseLeave(object sender, EventArgs e)
        {
            childPanel.Visible = false;
        }

        private void PurchaseReport_MouseHover(object sender, EventArgs e)
        {
            reportType = ReportType.PURCHASE;
            Point p = new Point(440, 62);
            childPanel.Location = p;
            childPanel.Visible = true;
            
        }

        private void MillDispatchReport_MouseHover(object sender, EventArgs e)
        {
            reportType = ReportType.MILLDISPATCH;
            Point p = new Point(440, 100);
            childPanel.Location = p;
            childPanel.Visible = true;
        }

        private void MillReceietReport_MouseHover(object sender, EventArgs e)
        {
            reportType = ReportType.MILLDISPATCH;
            Point p = new Point(440, 142);
            childPanel.Location = p;
            childPanel.Visible = true;
        }

        private void JobWorkReport_MouseHover(object sender, EventArgs e)
        {
            reportType = ReportType.JOBWQRK;
            Point p = new Point(440, 178);
            childPanel.Location = p;
            childPanel.Visible = true;
        }

        private void YearWiseReport_MouseHover(object sender, EventArgs e)
        {
            reportType = ReportType.COMPARE;
            childPanel.Visible = false;
        }

        private void MonthlyReport_Click(object sender, EventArgs e)
        {
            TabPage myPage = new TabPage();
            Report1 rep = new Report1(reportType);
            rep.Dock = DockStyle.Fill;
            myPage.Controls.Add(rep);
            if (ReportType.SALES == reportType) 
            myPage.Text = "Monthly Sales Report";
            else if (ReportType.PURCHASE == reportType)
                myPage.Text = "Monthly Purchase Report";
            else if (ReportType.MILLDISPATCH == reportType)
                myPage.Text = "Monthly Mill Report";
            else if (ReportType.JOBWQRK == reportType)
                myPage.Text = "Monthly Job Report";            
            //ControlCollection ctrls = ReportMain.ControlCollection.;
            TabControl tb=(TabControl) mainForm.Controls[0];
            tb.TabPages.Add(myPage);
            tb.SelectedIndex = tb.TabPages.IndexOf(myPage);
           
        }
    }
}
