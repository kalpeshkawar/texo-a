﻿namespace Texo_Advance.Reports
{
    partial class Reports
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Reports));
            this.SalesReport = new System.Windows.Forms.PictureBox();
            this.SalesReport1 = new VisualPlus.Toolkit.Controls.Layout.VisualTile();
            this.panel1 = new System.Windows.Forms.Panel();
            this.JobWorkReport = new VisualPlus.Toolkit.Controls.Layout.VisualTile();
            this.YearWiseReport = new VisualPlus.Toolkit.Controls.Layout.VisualTile();
            this.MillReceietReport = new VisualPlus.Toolkit.Controls.Layout.VisualTile();
            this.MillDispatchReport = new VisualPlus.Toolkit.Controls.Layout.VisualTile();
            this.PurchaseReport = new VisualPlus.Toolkit.Controls.Layout.VisualTile();
            this.childPanel = new System.Windows.Forms.Panel();
            this.DateWise = new VisualPlus.Toolkit.Controls.Layout.VisualTile();
            this.YearWise = new VisualPlus.Toolkit.Controls.Layout.VisualTile();
            this.QuarterlyReport = new VisualPlus.Toolkit.Controls.Layout.VisualTile();
            this.MonthlyReport = new VisualPlus.Toolkit.Controls.Layout.VisualTile();
            ((System.ComponentModel.ISupportInitialize)(this.SalesReport)).BeginInit();
            this.panel1.SuspendLayout();
            this.childPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // SalesReport
            // 
            this.SalesReport.Image = ((System.Drawing.Image)(resources.GetObject("SalesReport.Image")));
            this.SalesReport.Location = new System.Drawing.Point(574, 336);
            this.SalesReport.Name = "SalesReport";
            this.SalesReport.Size = new System.Drawing.Size(639, 278);
            this.SalesReport.TabIndex = 1;
            this.SalesReport.TabStop = false;
            // 
            // SalesReport1
            // 
            this.SalesReport1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.SalesReport1.BackColorState.Disabled = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.SalesReport1.BackColorState.Enabled = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.SalesReport1.BackColorState.Hover = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(183)))), ((int)(((byte)(230)))));
            this.SalesReport1.BackColorState.Pressed = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.SalesReport1.BackgroundImageLayout = VisualPlus.Enumerators.BackgroundLayout.Stretch;
            this.SalesReport1.Font = new System.Drawing.Font("Arial", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SalesReport1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.SalesReport1.Image = ((System.Drawing.Image)(resources.GetObject("SalesReport1.Image")));
            this.SalesReport1.Location = new System.Drawing.Point(0, 0);
            this.SalesReport1.MouseState = VisualPlus.Enumerators.MouseStates.Normal;
            this.SalesReport1.Name = "SalesReport1";
            this.SalesReport1.Offset = new System.Drawing.Point(0, 0);
            this.SalesReport1.Size = new System.Drawing.Size(387, 42);
            this.SalesReport1.TabIndex = 2;
            this.SalesReport1.Text = "Sales Report";
            this.SalesReport1.TextStyle.Disabled = System.Drawing.Color.FromArgb(((int)(((byte)(131)))), ((int)(((byte)(129)))), ((int)(((byte)(129)))));
            this.SalesReport1.TextStyle.Enabled = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.SalesReport1.TextStyle.Hover = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.SalesReport1.TextStyle.Pressed = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.SalesReport1.TextStyle.TextAlignment = System.Drawing.StringAlignment.Center;
            this.SalesReport1.TextStyle.TextLineAlignment = System.Drawing.StringAlignment.Center;
            this.SalesReport1.TextStyle.TextRenderingHint = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;
            this.SalesReport1.Type = VisualPlus.Toolkit.Controls.Layout.VisualTile.TileType.Text;
            this.SalesReport1.Click += new System.EventHandler(this.visualTile1_Click);
            this.SalesReport1.MouseLeave += new System.EventHandler(this.SalesReport1_MouseLeave);
            this.SalesReport1.MouseHover += new System.EventHandler(this.SalesReport1_MouseHover);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.JobWorkReport);
            this.panel1.Controls.Add(this.YearWiseReport);
            this.panel1.Controls.Add(this.MillReceietReport);
            this.panel1.Controls.Add(this.MillDispatchReport);
            this.panel1.Controls.Add(this.PurchaseReport);
            this.panel1.Controls.Add(this.SalesReport1);
            this.panel1.Location = new System.Drawing.Point(52, 24);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(387, 238);
            this.panel1.TabIndex = 3;
            // 
            // JobWorkReport
            // 
            this.JobWorkReport.BackColor = System.Drawing.Color.Linen;
            this.JobWorkReport.BackColorState.Disabled = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.JobWorkReport.BackColorState.Enabled = System.Drawing.Color.Linen;
            this.JobWorkReport.BackColorState.Hover = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(183)))), ((int)(((byte)(230)))));
            this.JobWorkReport.BackColorState.Pressed = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.JobWorkReport.BackgroundImageLayout = VisualPlus.Enumerators.BackgroundLayout.Stretch;
            this.JobWorkReport.Font = new System.Drawing.Font("Arial", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JobWorkReport.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.JobWorkReport.Image = ((System.Drawing.Image)(resources.GetObject("JobWorkReport.Image")));
            this.JobWorkReport.Location = new System.Drawing.Point(0, 154);
            this.JobWorkReport.MouseState = VisualPlus.Enumerators.MouseStates.Normal;
            this.JobWorkReport.Name = "JobWorkReport";
            this.JobWorkReport.Offset = new System.Drawing.Point(0, 0);
            this.JobWorkReport.Size = new System.Drawing.Size(387, 42);
            this.JobWorkReport.TabIndex = 8;
            this.JobWorkReport.Text = "Job Work Report";
            this.JobWorkReport.TextStyle.Disabled = System.Drawing.Color.FromArgb(((int)(((byte)(131)))), ((int)(((byte)(129)))), ((int)(((byte)(129)))));
            this.JobWorkReport.TextStyle.Enabled = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.JobWorkReport.TextStyle.Hover = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.JobWorkReport.TextStyle.Pressed = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.JobWorkReport.TextStyle.TextAlignment = System.Drawing.StringAlignment.Center;
            this.JobWorkReport.TextStyle.TextLineAlignment = System.Drawing.StringAlignment.Center;
            this.JobWorkReport.TextStyle.TextRenderingHint = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;
            this.JobWorkReport.Type = VisualPlus.Toolkit.Controls.Layout.VisualTile.TileType.Text;
            this.JobWorkReport.MouseHover += new System.EventHandler(this.JobWorkReport_MouseHover);
            // 
            // YearWiseReport
            // 
            this.YearWiseReport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.YearWiseReport.BackColorState.Disabled = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.YearWiseReport.BackColorState.Enabled = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.YearWiseReport.BackColorState.Hover = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(183)))), ((int)(((byte)(230)))));
            this.YearWiseReport.BackColorState.Pressed = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.YearWiseReport.BackgroundImageLayout = VisualPlus.Enumerators.BackgroundLayout.Stretch;
            this.YearWiseReport.Font = new System.Drawing.Font("Arial", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.YearWiseReport.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.YearWiseReport.Image = ((System.Drawing.Image)(resources.GetObject("YearWiseReport.Image")));
            this.YearWiseReport.Location = new System.Drawing.Point(0, 194);
            this.YearWiseReport.MouseState = VisualPlus.Enumerators.MouseStates.Normal;
            this.YearWiseReport.Name = "YearWiseReport";
            this.YearWiseReport.Offset = new System.Drawing.Point(0, 0);
            this.YearWiseReport.Size = new System.Drawing.Size(387, 42);
            this.YearWiseReport.TabIndex = 6;
            this.YearWiseReport.Text = "Compare Report";
            this.YearWiseReport.TextStyle.Disabled = System.Drawing.Color.FromArgb(((int)(((byte)(131)))), ((int)(((byte)(129)))), ((int)(((byte)(129)))));
            this.YearWiseReport.TextStyle.Enabled = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.YearWiseReport.TextStyle.Hover = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.YearWiseReport.TextStyle.Pressed = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.YearWiseReport.TextStyle.TextAlignment = System.Drawing.StringAlignment.Center;
            this.YearWiseReport.TextStyle.TextLineAlignment = System.Drawing.StringAlignment.Center;
            this.YearWiseReport.TextStyle.TextRenderingHint = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;
            this.YearWiseReport.Type = VisualPlus.Toolkit.Controls.Layout.VisualTile.TileType.Text;
            this.YearWiseReport.Click += new System.EventHandler(this.visualTile3_Click);
            this.YearWiseReport.MouseHover += new System.EventHandler(this.YearWiseReport_MouseHover);
            // 
            // MillReceietReport
            // 
            this.MillReceietReport.BackColor = System.Drawing.SystemColors.ControlLight;
            this.MillReceietReport.BackColorState.Disabled = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.MillReceietReport.BackColorState.Enabled = System.Drawing.SystemColors.ControlLight;
            this.MillReceietReport.BackColorState.Hover = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(183)))), ((int)(((byte)(230)))));
            this.MillReceietReport.BackColorState.Pressed = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.MillReceietReport.BackgroundImageLayout = VisualPlus.Enumerators.BackgroundLayout.Stretch;
            this.MillReceietReport.Font = new System.Drawing.Font("Arial", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MillReceietReport.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.MillReceietReport.Image = ((System.Drawing.Image)(resources.GetObject("MillReceietReport.Image")));
            this.MillReceietReport.Location = new System.Drawing.Point(0, 118);
            this.MillReceietReport.MouseState = VisualPlus.Enumerators.MouseStates.Normal;
            this.MillReceietReport.Name = "MillReceietReport";
            this.MillReceietReport.Offset = new System.Drawing.Point(0, 0);
            this.MillReceietReport.Size = new System.Drawing.Size(387, 42);
            this.MillReceietReport.TabIndex = 5;
            this.MillReceietReport.Text = "Mill Receipt Report";
            this.MillReceietReport.TextStyle.Disabled = System.Drawing.Color.FromArgb(((int)(((byte)(131)))), ((int)(((byte)(129)))), ((int)(((byte)(129)))));
            this.MillReceietReport.TextStyle.Enabled = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.MillReceietReport.TextStyle.Hover = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.MillReceietReport.TextStyle.Pressed = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.MillReceietReport.TextStyle.TextAlignment = System.Drawing.StringAlignment.Center;
            this.MillReceietReport.TextStyle.TextLineAlignment = System.Drawing.StringAlignment.Center;
            this.MillReceietReport.TextStyle.TextRenderingHint = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;
            this.MillReceietReport.Type = VisualPlus.Toolkit.Controls.Layout.VisualTile.TileType.Text;
            this.MillReceietReport.MouseHover += new System.EventHandler(this.MillReceietReport_MouseHover);
            // 
            // MillDispatchReport
            // 
            this.MillDispatchReport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.MillDispatchReport.BackColorState.Disabled = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.MillDispatchReport.BackColorState.Enabled = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.MillDispatchReport.BackColorState.Hover = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(183)))), ((int)(((byte)(230)))));
            this.MillDispatchReport.BackColorState.Pressed = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.MillDispatchReport.BackgroundImageLayout = VisualPlus.Enumerators.BackgroundLayout.Stretch;
            this.MillDispatchReport.Font = new System.Drawing.Font("Arial", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MillDispatchReport.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.MillDispatchReport.Image = ((System.Drawing.Image)(resources.GetObject("MillDispatchReport.Image")));
            this.MillDispatchReport.Location = new System.Drawing.Point(0, 76);
            this.MillDispatchReport.MouseState = VisualPlus.Enumerators.MouseStates.Normal;
            this.MillDispatchReport.Name = "MillDispatchReport";
            this.MillDispatchReport.Offset = new System.Drawing.Point(0, 0);
            this.MillDispatchReport.Size = new System.Drawing.Size(387, 42);
            this.MillDispatchReport.TabIndex = 4;
            this.MillDispatchReport.Text = "Mill Dispatch Report";
            this.MillDispatchReport.TextStyle.Disabled = System.Drawing.Color.FromArgb(((int)(((byte)(131)))), ((int)(((byte)(129)))), ((int)(((byte)(129)))));
            this.MillDispatchReport.TextStyle.Enabled = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.MillDispatchReport.TextStyle.Hover = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.MillDispatchReport.TextStyle.Pressed = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.MillDispatchReport.TextStyle.TextAlignment = System.Drawing.StringAlignment.Center;
            this.MillDispatchReport.TextStyle.TextLineAlignment = System.Drawing.StringAlignment.Center;
            this.MillDispatchReport.TextStyle.TextRenderingHint = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;
            this.MillDispatchReport.Type = VisualPlus.Toolkit.Controls.Layout.VisualTile.TileType.Text;
            this.MillDispatchReport.MouseHover += new System.EventHandler(this.MillDispatchReport_MouseHover);
            // 
            // PurchaseReport
            // 
            this.PurchaseReport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.PurchaseReport.BackColorState.Disabled = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.PurchaseReport.BackColorState.Enabled = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.PurchaseReport.BackColorState.Hover = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(183)))), ((int)(((byte)(230)))));
            this.PurchaseReport.BackColorState.Pressed = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.PurchaseReport.BackgroundImageLayout = VisualPlus.Enumerators.BackgroundLayout.Stretch;
            this.PurchaseReport.Font = new System.Drawing.Font("Arial", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PurchaseReport.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.PurchaseReport.Image = ((System.Drawing.Image)(resources.GetObject("PurchaseReport.Image")));
            this.PurchaseReport.Location = new System.Drawing.Point(0, 38);
            this.PurchaseReport.MouseState = VisualPlus.Enumerators.MouseStates.Normal;
            this.PurchaseReport.Name = "PurchaseReport";
            this.PurchaseReport.Offset = new System.Drawing.Point(0, 0);
            this.PurchaseReport.Size = new System.Drawing.Size(387, 42);
            this.PurchaseReport.TabIndex = 3;
            this.PurchaseReport.Text = "Purchase Report";
            this.PurchaseReport.TextStyle.Disabled = System.Drawing.Color.FromArgb(((int)(((byte)(131)))), ((int)(((byte)(129)))), ((int)(((byte)(129)))));
            this.PurchaseReport.TextStyle.Enabled = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.PurchaseReport.TextStyle.Hover = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.PurchaseReport.TextStyle.Pressed = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.PurchaseReport.TextStyle.TextAlignment = System.Drawing.StringAlignment.Center;
            this.PurchaseReport.TextStyle.TextLineAlignment = System.Drawing.StringAlignment.Center;
            this.PurchaseReport.TextStyle.TextRenderingHint = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;
            this.PurchaseReport.Type = VisualPlus.Toolkit.Controls.Layout.VisualTile.TileType.Text;
            this.PurchaseReport.MouseHover += new System.EventHandler(this.PurchaseReport_MouseHover);
            // 
            // childPanel
            // 
            this.childPanel.Controls.Add(this.DateWise);
            this.childPanel.Controls.Add(this.YearWise);
            this.childPanel.Controls.Add(this.QuarterlyReport);
            this.childPanel.Controls.Add(this.MonthlyReport);
            this.childPanel.Location = new System.Drawing.Point(574, 119);
            this.childPanel.Name = "childPanel";
            this.childPanel.Size = new System.Drawing.Size(200, 119);
            this.childPanel.TabIndex = 4;
            // 
            // DateWise
            // 
            this.DateWise.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.DateWise.BackColorState.Disabled = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.DateWise.BackColorState.Enabled = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.DateWise.BackColorState.Hover = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(183)))), ((int)(((byte)(230)))));
            this.DateWise.BackColorState.Pressed = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.DateWise.BackgroundImageLayout = VisualPlus.Enumerators.BackgroundLayout.Stretch;
            this.DateWise.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.DateWise.Image = ((System.Drawing.Image)(resources.GetObject("DateWise.Image")));
            this.DateWise.Location = new System.Drawing.Point(0, 87);
            this.DateWise.MouseState = VisualPlus.Enumerators.MouseStates.Normal;
            this.DateWise.Name = "DateWise";
            this.DateWise.Offset = new System.Drawing.Point(0, 0);
            this.DateWise.Size = new System.Drawing.Size(200, 32);
            this.DateWise.TabIndex = 6;
            this.DateWise.Text = "Date Wise";
            this.DateWise.TextStyle.Disabled = System.Drawing.Color.FromArgb(((int)(((byte)(131)))), ((int)(((byte)(129)))), ((int)(((byte)(129)))));
            this.DateWise.TextStyle.Enabled = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.DateWise.TextStyle.Hover = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.DateWise.TextStyle.Pressed = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.DateWise.TextStyle.TextAlignment = System.Drawing.StringAlignment.Center;
            this.DateWise.TextStyle.TextLineAlignment = System.Drawing.StringAlignment.Center;
            this.DateWise.TextStyle.TextRenderingHint = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;
            this.DateWise.Type = VisualPlus.Toolkit.Controls.Layout.VisualTile.TileType.Text;
            // 
            // YearWise
            // 
            this.YearWise.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.YearWise.BackColorState.Disabled = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.YearWise.BackColorState.Enabled = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.YearWise.BackColorState.Hover = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(183)))), ((int)(((byte)(230)))));
            this.YearWise.BackColorState.Pressed = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.YearWise.BackgroundImageLayout = VisualPlus.Enumerators.BackgroundLayout.Stretch;
            this.YearWise.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.YearWise.Image = ((System.Drawing.Image)(resources.GetObject("YearWise.Image")));
            this.YearWise.Location = new System.Drawing.Point(0, 58);
            this.YearWise.MouseState = VisualPlus.Enumerators.MouseStates.Normal;
            this.YearWise.Name = "YearWise";
            this.YearWise.Offset = new System.Drawing.Point(0, 0);
            this.YearWise.Size = new System.Drawing.Size(200, 29);
            this.YearWise.TabIndex = 5;
            this.YearWise.Text = "Yearly Report";
            this.YearWise.TextStyle.Disabled = System.Drawing.Color.FromArgb(((int)(((byte)(131)))), ((int)(((byte)(129)))), ((int)(((byte)(129)))));
            this.YearWise.TextStyle.Enabled = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.YearWise.TextStyle.Hover = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.YearWise.TextStyle.Pressed = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.YearWise.TextStyle.TextAlignment = System.Drawing.StringAlignment.Center;
            this.YearWise.TextStyle.TextLineAlignment = System.Drawing.StringAlignment.Center;
            this.YearWise.TextStyle.TextRenderingHint = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;
            this.YearWise.Type = VisualPlus.Toolkit.Controls.Layout.VisualTile.TileType.Text;
            // 
            // QuarterlyReport
            // 
            this.QuarterlyReport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.QuarterlyReport.BackColorState.Disabled = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.QuarterlyReport.BackColorState.Enabled = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.QuarterlyReport.BackColorState.Hover = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(183)))), ((int)(((byte)(230)))));
            this.QuarterlyReport.BackColorState.Pressed = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.QuarterlyReport.BackgroundImageLayout = VisualPlus.Enumerators.BackgroundLayout.Stretch;
            this.QuarterlyReport.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.QuarterlyReport.Image = ((System.Drawing.Image)(resources.GetObject("QuarterlyReport.Image")));
            this.QuarterlyReport.Location = new System.Drawing.Point(0, 29);
            this.QuarterlyReport.MouseState = VisualPlus.Enumerators.MouseStates.Normal;
            this.QuarterlyReport.Name = "QuarterlyReport";
            this.QuarterlyReport.Offset = new System.Drawing.Point(0, 0);
            this.QuarterlyReport.Size = new System.Drawing.Size(200, 29);
            this.QuarterlyReport.TabIndex = 4;
            this.QuarterlyReport.Text = "Quarterly Report";
            this.QuarterlyReport.TextStyle.Disabled = System.Drawing.Color.FromArgb(((int)(((byte)(131)))), ((int)(((byte)(129)))), ((int)(((byte)(129)))));
            this.QuarterlyReport.TextStyle.Enabled = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.QuarterlyReport.TextStyle.Hover = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.QuarterlyReport.TextStyle.Pressed = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.QuarterlyReport.TextStyle.TextAlignment = System.Drawing.StringAlignment.Center;
            this.QuarterlyReport.TextStyle.TextLineAlignment = System.Drawing.StringAlignment.Center;
            this.QuarterlyReport.TextStyle.TextRenderingHint = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;
            this.QuarterlyReport.Type = VisualPlus.Toolkit.Controls.Layout.VisualTile.TileType.Text;
            // 
            // MonthlyReport
            // 
            this.MonthlyReport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.MonthlyReport.BackColorState.Disabled = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(220)))), ((int)(((byte)(220)))));
            this.MonthlyReport.BackColorState.Enabled = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.MonthlyReport.BackColorState.Hover = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(183)))), ((int)(((byte)(230)))));
            this.MonthlyReport.BackColorState.Pressed = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.MonthlyReport.BackgroundImageLayout = VisualPlus.Enumerators.BackgroundLayout.Stretch;
            this.MonthlyReport.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.MonthlyReport.Image = ((System.Drawing.Image)(resources.GetObject("MonthlyReport.Image")));
            this.MonthlyReport.Location = new System.Drawing.Point(0, 0);
            this.MonthlyReport.MouseState = VisualPlus.Enumerators.MouseStates.Normal;
            this.MonthlyReport.Name = "MonthlyReport";
            this.MonthlyReport.Offset = new System.Drawing.Point(0, 0);
            this.MonthlyReport.Size = new System.Drawing.Size(200, 29);
            this.MonthlyReport.TabIndex = 3;
            this.MonthlyReport.Text = "Monthly Report";
            this.MonthlyReport.TextStyle.Disabled = System.Drawing.Color.FromArgb(((int)(((byte)(131)))), ((int)(((byte)(129)))), ((int)(((byte)(129)))));
            this.MonthlyReport.TextStyle.Enabled = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.MonthlyReport.TextStyle.Hover = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.MonthlyReport.TextStyle.Pressed = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.MonthlyReport.TextStyle.TextAlignment = System.Drawing.StringAlignment.Center;
            this.MonthlyReport.TextStyle.TextLineAlignment = System.Drawing.StringAlignment.Center;
            this.MonthlyReport.TextStyle.TextRenderingHint = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;
            this.MonthlyReport.Type = VisualPlus.Toolkit.Controls.Layout.VisualTile.TileType.Text;
            this.MonthlyReport.Click += new System.EventHandler(this.MonthlyReport_Click);
            // 
            // Reports
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.Controls.Add(this.childPanel);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.SalesReport);
            this.Name = "Reports";
            this.Size = new System.Drawing.Size(1500, 900);
            this.Load += new System.EventHandler(this.Reports_Load);
            this.MouseLeave += new System.EventHandler(this.Reports_MouseLeave);
            this.MouseHover += new System.EventHandler(this.Reports_MouseHover);
            ((System.ComponentModel.ISupportInitialize)(this.SalesReport)).EndInit();
            this.panel1.ResumeLayout(false);
            this.childPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.PictureBox SalesReport;
        private VisualPlus.Toolkit.Controls.Layout.VisualTile SalesReport1;
        private System.Windows.Forms.Panel panel1;
        private VisualPlus.Toolkit.Controls.Layout.VisualTile PurchaseReport;
        private VisualPlus.Toolkit.Controls.Layout.VisualTile JobWorkReport;
        private VisualPlus.Toolkit.Controls.Layout.VisualTile YearWiseReport;
        private VisualPlus.Toolkit.Controls.Layout.VisualTile MillReceietReport;
        private VisualPlus.Toolkit.Controls.Layout.VisualTile MillDispatchReport;
        private System.Windows.Forms.Panel childPanel;
        private VisualPlus.Toolkit.Controls.Layout.VisualTile DateWise;
        private VisualPlus.Toolkit.Controls.Layout.VisualTile YearWise;
        private VisualPlus.Toolkit.Controls.Layout.VisualTile QuarterlyReport;
        private VisualPlus.Toolkit.Controls.Layout.VisualTile MonthlyReport;
    }
}
