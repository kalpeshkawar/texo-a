﻿namespace Texo_Advance.Reports
{
    partial class Report1
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            //System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            //System.Windows.Forms.DataVisualization.Charting.Legend legend3 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            //System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            //System.Windows.Forms.DataVisualization.Charting.Series series6 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Report1));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.partyCB = new JTG.ColumnComboBox();
            this.salesTypeCB = new VisualPlus.Toolkit.Controls.Interactivity.VisualComboBox();
            this.OrderingCombo = new VisualPlus.Toolkit.Controls.Interactivity.VisualComboBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.generateReport = new System.Windows.Forms.Button();
            this.generatePdfBtn = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Month = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pcs = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.taxableAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cgst = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sgst = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.igst = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Discount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.totalamount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            //this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.brokerCB = new JTG.ColumnComboBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            // ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Black;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Orange;
            this.label1.Location = new System.Drawing.Point(17, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 16);
            this.label1.TabIndex = 1;
            this.label1.Text = "Report Type";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Black;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(19, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 16);
            this.label2.TabIndex = 3;
            this.label2.Text = "Party";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Black;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(351, 69);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 16);
            this.label3.TabIndex = 6;
            this.label3.Text = "Broker";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panel1.Controls.Add(this.brokerCB);
            this.panel1.Controls.Add(this.partyCB);
            this.panel1.Controls.Add(this.checkBox3);
            this.panel1.Controls.Add(this.salesTypeCB);
            this.panel1.Controls.Add(this.checkBox2);
            this.panel1.Controls.Add(this.OrderingCombo);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Location = new System.Drawing.Point(29, 22);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(626, 124);
            this.panel1.TabIndex = 0;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // partyCB
            // 
            this.partyCB.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.partyCB.DropDownWidth = 17;
            this.partyCB.FormattingEnabled = true;
            this.partyCB.Location = new System.Drawing.Point(119, 64);
            this.partyCB.Name = "partyCB";
            this.partyCB.Size = new System.Drawing.Size(181, 21);
            this.partyCB.TabIndex = 15;
            this.partyCB.ViewColumn = 0;
            // 
            // salesTypeCB
            // 
            this.salesTypeCB.BackColor = System.Drawing.Color.Transparent;
            this.salesTypeCB.BackColorState.Disabled = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(243)))), ((int)(((byte)(243)))));
            this.salesTypeCB.BackColorState.Enabled = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.salesTypeCB.Border.Color = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.salesTypeCB.Border.HoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(183)))), ((int)(((byte)(230)))));
            this.salesTypeCB.Border.HoverVisible = true;
            this.salesTypeCB.Border.Rounding = 6;
            this.salesTypeCB.Border.Thickness = 1;
            this.salesTypeCB.Border.Type = VisualPlus.Enumerators.ShapeTypes.Rounded;
            this.salesTypeCB.Border.Visible = true;
            this.salesTypeCB.ButtonColor = System.Drawing.Color.FromArgb(((int)(((byte)(119)))), ((int)(((byte)(119)))), ((int)(((byte)(118)))));
            this.salesTypeCB.ButtonImage = null;
            this.salesTypeCB.ButtonStyle = VisualPlus.Toolkit.Controls.Interactivity.VisualComboBox.ButtonStyles.Arrow;
            this.salesTypeCB.ButtonWidth = 30;
            this.salesTypeCB.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.salesTypeCB.DropDownHeight = 100;
            this.salesTypeCB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.salesTypeCB.DropDownWidth = 108;
            this.salesTypeCB.FormattingEnabled = true;
            this.salesTypeCB.ImageList = null;
            this.salesTypeCB.ImageVisible = false;
            this.salesTypeCB.Index = 0;
            this.salesTypeCB.IntegralHeight = false;
            this.salesTypeCB.ItemHeight = 24;
            this.salesTypeCB.ItemImageVisible = true;
            this.salesTypeCB.Items.AddRange(new object[] {
            "Sales Report",
            "Purchase Report"});
            this.salesTypeCB.Location = new System.Drawing.Point(119, 12);
            this.salesTypeCB.MenuItemHover = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(183)))), ((int)(((byte)(230)))));
            this.salesTypeCB.MenuItemNormal = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(244)))), ((int)(((byte)(249)))));
            this.salesTypeCB.MenuTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.salesTypeCB.MouseState = VisualPlus.Enumerators.MouseStates.Normal;
            this.salesTypeCB.Name = "salesTypeCB";
            this.salesTypeCB.SeparatorColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.salesTypeCB.Size = new System.Drawing.Size(181, 30);
            this.salesTypeCB.State = VisualPlus.Enumerators.MouseStates.Normal;
            this.salesTypeCB.TabIndex = 13;
            this.salesTypeCB.TextAlignment = System.Drawing.StringAlignment.Center;
            this.salesTypeCB.TextDisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(131)))), ((int)(((byte)(129)))), ((int)(((byte)(129)))));
            this.salesTypeCB.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.salesTypeCB.TextLineAlignment = System.Drawing.StringAlignment.Center;
            this.salesTypeCB.TextRendering = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;
            this.salesTypeCB.TextStyle.Disabled = System.Drawing.Color.FromArgb(((int)(((byte)(131)))), ((int)(((byte)(129)))), ((int)(((byte)(129)))));
            this.salesTypeCB.TextStyle.Enabled = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.salesTypeCB.TextStyle.Hover = System.Drawing.Color.Empty;
            this.salesTypeCB.TextStyle.Pressed = System.Drawing.Color.Empty;
            this.salesTypeCB.TextStyle.TextAlignment = System.Drawing.StringAlignment.Center;
            this.salesTypeCB.TextStyle.TextLineAlignment = System.Drawing.StringAlignment.Center;
            this.salesTypeCB.TextStyle.TextRenderingHint = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;
            this.salesTypeCB.Watermark.Active = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.salesTypeCB.Watermark.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.salesTypeCB.Watermark.Inactive = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(211)))), ((int)(((byte)(211)))));
            this.salesTypeCB.Watermark.Text = "Watermark text";
            this.salesTypeCB.Watermark.Visible = false;
            this.salesTypeCB.SelectedIndexChanged += new System.EventHandler(this.visualComboBox1_SelectedIndexChanged);
            // 
            // OrderingCombo
            // 
            this.OrderingCombo.BackColor = System.Drawing.Color.Transparent;
            this.OrderingCombo.BackColorState.Disabled = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(243)))), ((int)(((byte)(243)))));
            this.OrderingCombo.BackColorState.Enabled = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.OrderingCombo.Border.Color = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.OrderingCombo.Border.HoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(183)))), ((int)(((byte)(230)))));
            this.OrderingCombo.Border.HoverVisible = true;
            this.OrderingCombo.Border.Rounding = 6;
            this.OrderingCombo.Border.Thickness = 1;
            this.OrderingCombo.Border.Type = VisualPlus.Enumerators.ShapeTypes.Rounded;
            this.OrderingCombo.Border.Visible = true;
            this.OrderingCombo.ButtonColor = System.Drawing.Color.FromArgb(((int)(((byte)(119)))), ((int)(((byte)(119)))), ((int)(((byte)(118)))));
            this.OrderingCombo.ButtonImage = null;
            this.OrderingCombo.ButtonStyle = VisualPlus.Toolkit.Controls.Interactivity.VisualComboBox.ButtonStyles.Arrow;
            this.OrderingCombo.ButtonWidth = 30;
            this.OrderingCombo.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.OrderingCombo.DropDownHeight = 100;
            this.OrderingCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.OrderingCombo.DropDownWidth = 76;
            this.OrderingCombo.FormattingEnabled = true;
            this.OrderingCombo.ImageList = null;
            this.OrderingCombo.ImageVisible = false;
            this.OrderingCombo.Index = 0;
            this.OrderingCombo.IntegralHeight = false;
            this.OrderingCombo.ItemHeight = 24;
            this.OrderingCombo.ItemImageVisible = true;
            this.OrderingCombo.Items.AddRange(new object[] {
            "Date-Wise"});
            this.OrderingCombo.Location = new System.Drawing.Point(417, 12);
            this.OrderingCombo.MenuItemHover = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(183)))), ((int)(((byte)(230)))));
            this.OrderingCombo.MenuItemNormal = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(244)))), ((int)(((byte)(249)))));
            this.OrderingCombo.MenuTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.OrderingCombo.MouseState = VisualPlus.Enumerators.MouseStates.Normal;
            this.OrderingCombo.Name = "OrderingCombo";
            this.OrderingCombo.SeparatorColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.OrderingCombo.Size = new System.Drawing.Size(159, 30);
            this.OrderingCombo.State = VisualPlus.Enumerators.MouseStates.Normal;
            this.OrderingCombo.TabIndex = 5;
            this.OrderingCombo.TextAlignment = System.Drawing.StringAlignment.Center;
            this.OrderingCombo.TextDisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(131)))), ((int)(((byte)(129)))), ((int)(((byte)(129)))));
            this.OrderingCombo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.OrderingCombo.TextLineAlignment = System.Drawing.StringAlignment.Center;
            this.OrderingCombo.TextRendering = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;
            this.OrderingCombo.TextStyle.Disabled = System.Drawing.Color.FromArgb(((int)(((byte)(131)))), ((int)(((byte)(129)))), ((int)(((byte)(129)))));
            this.OrderingCombo.TextStyle.Enabled = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.OrderingCombo.TextStyle.Hover = System.Drawing.Color.Empty;
            this.OrderingCombo.TextStyle.Pressed = System.Drawing.Color.Empty;
            this.OrderingCombo.TextStyle.TextAlignment = System.Drawing.StringAlignment.Center;
            this.OrderingCombo.TextStyle.TextLineAlignment = System.Drawing.StringAlignment.Center;
            this.OrderingCombo.TextStyle.TextRenderingHint = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;
            this.OrderingCombo.Watermark.Active = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.OrderingCombo.Watermark.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OrderingCombo.Watermark.Inactive = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(211)))), ((int)(((byte)(211)))));
            this.OrderingCombo.Watermark.Text = "Watermark text";
            this.OrderingCombo.Watermark.Visible = false;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Location = new System.Drawing.Point(661, 325);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(8, 8);
            this.flowLayoutPanel1.TabIndex = 18;
            // 
            // generateReport
            // 
            this.generateReport.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.generateReport.Location = new System.Drawing.Point(29, 165);
            this.generateReport.Name = "generateReport";
            this.generateReport.Size = new System.Drawing.Size(149, 30);
            this.generateReport.TabIndex = 19;
            this.generateReport.Text = "Generate Report";
            this.generateReport.UseVisualStyleBackColor = false;
            this.generateReport.Click += new System.EventHandler(this.generateReport_Click);
            // 
            // generatePdfBtn
            // 
            this.generatePdfBtn.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.generatePdfBtn.Location = new System.Drawing.Point(203, 165);
            this.generatePdfBtn.Name = "generatePdfBtn";
            this.generatePdfBtn.Size = new System.Drawing.Size(146, 30);
            this.generatePdfBtn.TabIndex = 20;
            this.generatePdfBtn.Text = "Generate PDF";
            this.generatePdfBtn.UseVisualStyleBackColor = false;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Month,
            this.pcs,
            this.quantity,
            this.taxableAmount,
            this.cgst,
            this.sgst,
            this.igst,
            this.Discount,
            this.totalamount});
            this.dataGridView1.Location = new System.Drawing.Point(29, 211);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(943, 150);
            this.dataGridView1.TabIndex = 21;
            // 
            // Month
            // 
            this.Month.HeaderText = "Month";
            this.Month.Name = "Month";
            this.Month.ReadOnly = true;
            // 
            // pcs
            // 
            this.pcs.HeaderText = "Pcs.";
            this.pcs.Name = "pcs";
            this.pcs.ReadOnly = true;
            // 
            // quantity
            // 
            this.quantity.HeaderText = "Quantity";
            this.quantity.Name = "quantity";
            this.quantity.ReadOnly = true;
            // 
            // taxableAmount
            // 
            this.taxableAmount.HeaderText = "Taxable Amount";
            this.taxableAmount.Name = "taxableAmount";
            this.taxableAmount.ReadOnly = true;
            // 
            // cgst
            // 
            this.cgst.HeaderText = "CGST";
            this.cgst.Name = "cgst";
            this.cgst.ReadOnly = true;
            // 
            // sgst
            // 
            this.sgst.HeaderText = "SGST";
            this.sgst.Name = "sgst";
            this.sgst.ReadOnly = true;
            // 
            // igst
            // 
            this.igst.HeaderText = "IGST";
            this.igst.Name = "igst";
            this.igst.ReadOnly = true;
            // 
            // Discount
            // 
            this.Discount.HeaderText = "Discount";
            this.Discount.Name = "Discount";
            this.Discount.ReadOnly = true;
            // 
            // totalamount
            // 
            this.totalamount.HeaderText = "Total Amount";
            this.totalamount.Name = "totalamount";
            this.totalamount.ReadOnly = true;
            // 
            // chart1
            // 
            //chartArea3.Name = "ChartArea1";
            //this.chart1.ChartAreas.Add(chartArea3);
            //legend3.Name = "Legend1";
            //this.chart1.Legends.Add(legend3);
            //this.chart1.Location = new System.Drawing.Point(661, 3);
            //this.chart1.Name = "chart1";
            //series5.ChartArea = "ChartArea1";
            //series5.Legend = "Legend1";
            //series5.Name = "Sales";
            //series5.YAxisType = System.Windows.Forms.DataVisualization.Charting.AxisType.Secondary;
            //series6.ChartArea = "ChartArea1";
            //series6.Color = System.Drawing.Color.OrangeRed;
            //series6.Legend = "Legend1";
            //series6.Name = "Sales Return";
            //this.chart1.Series.Add(series5);
            //this.chart1.Series.Add(series6);
            //this.chart1.Size = new System.Drawing.Size(587, 202);
            //this.chart1.TabIndex = 22;
            //this.chart1.Text = "chart1";
            // 
            // brokerCB
            // 
            this.brokerCB.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.brokerCB.DropDownWidth = 17;
            this.brokerCB.FormattingEnabled = true;
            this.brokerCB.Location = new System.Drawing.Point(417, 65);
            this.brokerCB.Name = "brokerCB";
            this.brokerCB.Size = new System.Drawing.Size(181, 21);
            this.brokerCB.TabIndex = 16;
            this.brokerCB.ViewColumn = 0;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.BackColor = System.Drawing.Color.Transparent;
            this.checkBox2.Location = new System.Drawing.Point(607, 69);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(15, 14);
            this.checkBox2.TabIndex = 3;
            this.checkBox2.UseVisualStyleBackColor = false;
            this.checkBox2.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.BackColor = System.Drawing.Color.Transparent;
            this.checkBox3.Location = new System.Drawing.Point(309, 68);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(15, 14);
            this.checkBox3.TabIndex = 14;
            this.checkBox3.UseVisualStyleBackColor = false;
            this.checkBox3.CheckedChanged += new System.EventHandler(this.checkBox3_CheckedChanged);
            // 
            // Report1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            //this.Controls.Add(this.chart1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.generatePdfBtn);
            this.Controls.Add(this.generateReport);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.panel1);
            this.Name = "Report1";
            this.Size = new System.Drawing.Size(1500, 800);
            this.Load += new System.EventHandler(this.Report1_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
           // ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button generateReport;
        private System.Windows.Forms.Button generatePdfBtn;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Month;
        private System.Windows.Forms.DataGridViewTextBoxColumn pcs;
        private System.Windows.Forms.DataGridViewTextBoxColumn quantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn taxableAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn cgst;
        private System.Windows.Forms.DataGridViewTextBoxColumn sgst;
        private System.Windows.Forms.DataGridViewTextBoxColumn igst;
        private System.Windows.Forms.DataGridViewTextBoxColumn Discount;
        private System.Windows.Forms.DataGridViewTextBoxColumn totalamount;
        //private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private JTG.ColumnComboBox partyCB;
        private VisualPlus.Toolkit.Controls.Interactivity.VisualComboBox salesTypeCB;
        private VisualPlus.Toolkit.Controls.Interactivity.VisualComboBox OrderingCombo;
        private JTG.ColumnComboBox brokerCB;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.CheckBox checkBox2;
    }
}
