﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Texo_Advance.DBItems;

namespace Texo_Advance.Reports
{
    public partial class Report1 : UserControl
    {
        //public Report1()
        //{
        //    InitializeComponent();
        //}

        ReportType type;
        MasterDBData db = new MasterDBData();
        PartyDBData pdb = new PartyDBData();
        
        public Report1(Object OBJ)
        {
             type = (ReportType)OBJ;

            InitializeComponent();
        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Report1_Load(object sender, EventArgs e)
        {
            if (type == ReportType.SALES)
                salesTypeCB.SelectedIndex = 0;
            else if (type == ReportType.PURCHASE)
                salesTypeCB.SelectedIndex = 1;
            salesTypeCB.Enabled = false;
            OrderingCombo.SelectedIndex = 0;
            fillPartyAccountList();
            partyCB.SelectedIndex = -1;
            partyCB.Enabled = false;
            brokerCB.Enabled = false;
        }

        private void fillPartyAccountList()
        {
            int iColumnCount = 5;
            var partyAccountList = db.getPartyDetails(1);
            /*if (partyAccountList != null)
            {
                accountNameComboBox.DisplayMember = "party_name";
                accountNameComboBox.ValueMember = "partyid";
                accountNameComboBox.DataSource = partyAccountList;
            }*/
            DataTable myDataTable = new DataTable("ParentTable");
            // Declare variables for DataColumn and DataRow objects.
            DataColumn myDataColumn;
            DataRow myDataRow;

            //Add some coulumns
            for (int index = 0; index < iColumnCount; index++)
            {
                myDataColumn = new DataColumn();
                myDataColumn.DataType = typeof(string);
                if (index == 0)
                    myDataColumn.ColumnName = "NAME";
                else if (index == 1)
                    myDataColumn.ColumnName = "ACCOUNT TYPE";
                else if (index == 2)
                    myDataColumn.ColumnName = "ADDRESS";
                else if (index == 3)
                    myDataColumn.ColumnName = "CITY";
                else if (index == 4)
                    myDataColumn.ColumnName = "PARTY_ID";
                //myDataColumn.Caption = "ParentItem";
                myDataColumn.AutoIncrement = false;
                myDataColumn.ReadOnly = false;
                myDataColumn.Unique = false;
                // Add the Column to the DataColumnCollection.
                myDataTable.Columns.Add(myDataColumn);
            }

            //Add a row

            int iCol = 0;
            //Add some more rows
            for (int index = 0; index < partyAccountList.Count; index++)
            {
                myDataRow = myDataTable.NewRow();
                myDataRow[0] = partyAccountList[index].party_name;
                myDataRow[1] = "Sundry Debtors";
                myDataRow[2] = partyAccountList[index].address + partyAccountList[index].address2;
                myDataRow[3] = db.getCityName((long)partyAccountList[index].city_id);
                myDataRow[4] = partyAccountList[index].partyid;
                myDataTable.Rows.Add(myDataRow);
            }


            //Now set the Data of the ColumnComboBox
            partyCB.Data = myDataTable;
            //Set which row will be displayed in the text box
            //If you set this to a column that isn't displayed then the suggesting functionality won't work.
            partyCB.ColumnSpacing = 10;
            partyCB.ViewColumn = 0;
            partyCB.ValueMember = "PARTY_ID";
            partyCB.Columns[0].Width = 150;
            partyCB.Columns[1].Width = 100;
            partyCB.Columns[2].Width = 100;
            partyCB.Columns[3].Width = 50;
            //Set a few columns to not be shown
            partyCB.Columns[4].Display = false;


        }

        private void visualComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void visualPanel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void pbRB_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            
            if (checkBox2.Checked)
                brokerCB.Enabled = true;
            else
                brokerCB.Enabled = false;
        }

        private void generateReport_Click(object sender, EventArgs e)
        {
            if (checkBox3.Enabled)
            {
                if (partyCB.SelectedIndex == -1)
                {
                    MessageBox.Show("Please select the party", "Validation_Error", MessageBoxButtons.OK);
                    return;
                }
                    }
            var sakesReport =pdb.getMonthlySalesReport();

        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {

            if (checkBox3.Checked)
                partyCB.Enabled = true;
            else
                partyCB.Enabled = false;
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
