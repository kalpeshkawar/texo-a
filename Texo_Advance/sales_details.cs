//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Texo_Advance
{
    using System;
    using System.Collections.Generic;
    
    public partial class sales_details
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public sales_details()
        {
            this.salesitem_details = new HashSet<salesitem_details>();
        }
    
        public string caseNoDetail2 { get; set; }
        public Nullable<double> grossAmount { get; set; }
        public Nullable<double> igstRate { get; set; }
        public Nullable<double> sgstRate { get; set; }
        public Nullable<double> cgstRate { get; set; }
        public Nullable<double> addLessDetail2 { get; set; }
        public Nullable<double> addLessDetail1 { get; set; }
        public Nullable<double> discountOnAmount { get; set; }
        public Nullable<double> discountPercentage { get; set; }
        public long partyid { get; set; }
        public long salesTypeid { get; set; }
        public string billno { get; set; }
        public string orderno { get; set; }
        public Nullable<long> statecode { get; set; }
        public Nullable<long> gstTypeid { get; set; }
        public Nullable<long> station { get; set; }
        public string partygst { get; set; }
        public Nullable<long> brokerid { get; set; }
        public Nullable<long> transportid { get; set; }
        public string lrno { get; set; }
        public string remark { get; set; }
        public string discount { get; set; }
        public string additional { get; set; }
        public Nullable<System.DateTime> date { get; set; }
        public string caseno { get; set; }
        public Nullable<double> cgst { get; set; }
        public Nullable<double> sgst { get; set; }
        public Nullable<double> igst { get; set; }
        public double taxablevalue { get; set; }
        public double billamount { get; set; }
        public long salesbillid { get; set; }
        public Nullable<System.DateTime> version { get; set; }
        public Nullable<long> duedays { get; set; }
        public Nullable<long> voucherNo { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<salesitem_details> salesitem_details { get; set; }
    }
}
