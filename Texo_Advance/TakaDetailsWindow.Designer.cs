﻿namespace Texo_Advance
{
    partial class TakaDetailsWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.talaMtrsTextBox = new System.Windows.Forms.TextBox();
            this.takaWeightTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.srno = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.mtrs = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.weight = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.takaListView = new System.Windows.Forms.ListView();
            this.totalTakaTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.totalMtrsTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.deleteTakaButton = new System.Windows.Forms.Button();
            this.saveButton = new System.Windows.Forms.Button();
            this.closeButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // talaMtrsTextBox
            // 
            this.talaMtrsTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.talaMtrsTextBox.Location = new System.Drawing.Point(74, 43);
            this.talaMtrsTextBox.Name = "talaMtrsTextBox";
            this.talaMtrsTextBox.Size = new System.Drawing.Size(100, 22);
            this.talaMtrsTextBox.TabIndex = 0;
            this.talaMtrsTextBox.Text = "0";
            this.talaMtrsTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.talaMtrsTextBox_KeyDown);
            // 
            // takaWeightTextBox
            // 
            this.takaWeightTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.takaWeightTextBox.Location = new System.Drawing.Point(229, 43);
            this.takaWeightTextBox.Name = "takaWeightTextBox";
            this.takaWeightTextBox.Size = new System.Drawing.Size(100, 22);
            this.takaWeightTextBox.TabIndex = 1;
            this.takaWeightTextBox.Text = "0.0";
            this.takaWeightTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.takaWeightTextBox_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(76, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 16);
            this.label1.TabIndex = 2;
            this.label1.Text = "TAKA MTRS.";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(226, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(115, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "TAKA WEIGHT.";
            // 
            // srno
            // 
            this.srno.Text = "SR.NO.";
            this.srno.Width = 70;
            // 
            // mtrs
            // 
            this.mtrs.Text = "TAKA MTRS";
            this.mtrs.Width = 120;
            // 
            // weight
            // 
            this.weight.Text = "WEIGHT";
            this.weight.Width = 100;
            // 
            // takaListView
            // 
            this.takaListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.srno,
            this.mtrs,
            this.weight});
            this.takaListView.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.takaListView.FullRowSelect = true;
            this.takaListView.GridLines = true;
            this.takaListView.HideSelection = false;
            this.takaListView.LabelEdit = true;
            this.takaListView.Location = new System.Drawing.Point(22, 87);
            this.takaListView.Name = "takaListView";
            this.takaListView.Size = new System.Drawing.Size(294, 344);
            this.takaListView.TabIndex = 2;
            this.takaListView.UseCompatibleStateImageBehavior = false;
            this.takaListView.View = System.Windows.Forms.View.Details;
            // 
            // totalTakaTextBox
            // 
            this.totalTakaTextBox.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.totalTakaTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalTakaTextBox.Location = new System.Drawing.Point(340, 113);
            this.totalTakaTextBox.Name = "totalTakaTextBox";
            this.totalTakaTextBox.ReadOnly = true;
            this.totalTakaTextBox.Size = new System.Drawing.Size(100, 22);
            this.totalTakaTextBox.TabIndex = 4;
            this.totalTakaTextBox.Text = "0";
            this.totalTakaTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.talaMtrsTextBox_KeyDown);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(342, 87);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 16);
            this.label3.TabIndex = 2;
            this.label3.Text = "TOTAL TAKA";
            // 
            // totalMtrsTextBox
            // 
            this.totalMtrsTextBox.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.totalMtrsTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalMtrsTextBox.Location = new System.Drawing.Point(340, 186);
            this.totalMtrsTextBox.Name = "totalMtrsTextBox";
            this.totalMtrsTextBox.ReadOnly = true;
            this.totalMtrsTextBox.Size = new System.Drawing.Size(100, 22);
            this.totalMtrsTextBox.TabIndex = 5;
            this.totalMtrsTextBox.Text = "0.0";
            this.totalMtrsTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.talaMtrsTextBox_KeyDown);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(342, 160);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(108, 16);
            this.label4.TabIndex = 2;
            this.label4.Text = "TOTAL MTRS.";
            // 
            // deleteTakaButton
            // 
            this.deleteTakaButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deleteTakaButton.Location = new System.Drawing.Point(340, 240);
            this.deleteTakaButton.Name = "deleteTakaButton";
            this.deleteTakaButton.Size = new System.Drawing.Size(102, 44);
            this.deleteTakaButton.TabIndex = 6;
            this.deleteTakaButton.Text = "DELETE TAKA DETAIL";
            this.deleteTakaButton.UseVisualStyleBackColor = true;
            this.deleteTakaButton.Click += new System.EventHandler(this.deleteTakaButton_Click);
            // 
            // saveButton
            // 
            this.saveButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveButton.Location = new System.Drawing.Point(338, 306);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(102, 44);
            this.saveButton.TabIndex = 7;
            this.saveButton.Text = "SAVE";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // closeButton
            // 
            this.closeButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.closeButton.Location = new System.Drawing.Point(338, 375);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(102, 44);
            this.closeButton.TabIndex = 7;
            this.closeButton.Text = "CLOSE";
            this.closeButton.UseVisualStyleBackColor = true;
            this.closeButton.Click += new System.EventHandler(this.closeButton_Click);
            // 
            // TakaDetailsWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Aquamarine;
            this.ClientSize = new System.Drawing.Size(461, 450);
            this.Controls.Add(this.closeButton);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.deleteTakaButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.totalMtrsTextBox);
            this.Controls.Add(this.totalTakaTextBox);
            this.Controls.Add(this.takaWeightTextBox);
            this.Controls.Add(this.talaMtrsTextBox);
            this.Controls.Add(this.takaListView);
            this.Name = "TakaDetailsWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TakaDetailsWindow";
            this.Load += new System.EventHandler(this.TakaDetailsWindow_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox talaMtrsTextBox;
        private System.Windows.Forms.TextBox takaWeightTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ColumnHeader srno;
        private System.Windows.Forms.ColumnHeader mtrs;
        private System.Windows.Forms.ColumnHeader weight;
        private System.Windows.Forms.ListView takaListView;
        private System.Windows.Forms.TextBox totalTakaTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox totalMtrsTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button deleteTakaButton;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Button closeButton;
    }
}