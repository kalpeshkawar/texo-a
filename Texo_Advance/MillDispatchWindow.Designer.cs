﻿namespace Texo_Advance
{
    partial class MillDispatchWindow
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>  
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.jobTypeComboBox = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.dateTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.wtTextBox = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.recdMtrsTextBox = new System.Windows.Forms.TextBox();
            this.recdTakaTextBox = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.jobListView = new System.Windows.Forms.ListView();
            this.referenceNumber = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.challanNo = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.mill = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.taka = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.mtrs = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cardNo = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.dispatchDate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.rate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.process = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.remark = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label6 = new System.Windows.Forms.Label();
            this.createButton = new System.Windows.Forms.Button();
            this.viewButton = new System.Windows.Forms.Button();
            this.printButton = new System.Windows.Forms.Button();
            this.deleteButton = new System.Windows.Forms.Button();
            this.saveButton = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.voucherNoTextBox = new System.Windows.Forms.TextBox();
            this.refBillNoTextBox = new System.Windows.Forms.TextBox();
            this.companyNameTextBox = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.weaverTextBox = new System.Windows.Forms.TextBox();
            this.greyQualityTextBox = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.avlTakaTextBox = new System.Windows.Forms.TextBox();
            this.avlMtrsTextBox = new System.Windows.Forms.TextBox();
            this.totalTakaTextBox = new System.Windows.Forms.TextBox();
            this.totalMtrsTextBox = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.viewVoucherButton = new System.Windows.Forms.Button();
            this.deleteVoucherButton = new System.Windows.Forms.Button();
            this.updateButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(409, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(267, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "MILL DISPATCH ENTRY";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(20, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(142, 18);
            this.label2.TabIndex = 1;
            this.label2.Text = "COMPANY NAME";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(21, 91);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 18);
            this.label3.TabIndex = 1;
            this.label3.Text = "TYPE:";
            // 
            // jobTypeComboBox
            // 
            this.jobTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.jobTypeComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.jobTypeComboBox.FormattingEnabled = true;
            this.jobTypeComboBox.Location = new System.Drawing.Point(74, 90);
            this.jobTypeComboBox.Name = "jobTypeComboBox";
            this.jobTypeComboBox.Size = new System.Drawing.Size(219, 21);
            this.jobTypeComboBox.TabIndex = 3;
            this.jobTypeComboBox.SelectedIndexChanged += new System.EventHandler(this.jobTypeComboBox_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(685, 50);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 18);
            this.label4.TabIndex = 1;
            this.label4.Text = "DATE";
            // 
            // dateTextBox
            // 
            this.dateTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTextBox.Location = new System.Drawing.Point(747, 48);
            this.dateTextBox.Name = "dateTextBox";
            this.dateTextBox.Size = new System.Drawing.Size(117, 24);
            this.dateTextBox.TabIndex = 2;
            this.dateTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.dateTextBox_Validating);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(298, 92);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(119, 18);
            this.label5.TabIndex = 1;
            this.label5.Text = "REF BILL NO.:";
            // 
            // wtTextBox
            // 
            this.wtTextBox.BackColor = System.Drawing.SystemColors.Info;
            this.wtTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.wtTextBox.Location = new System.Drawing.Point(811, 132);
            this.wtTextBox.Name = "wtTextBox";
            this.wtTextBox.ReadOnly = true;
            this.wtTextBox.Size = new System.Drawing.Size(83, 24);
            this.wtTextBox.TabIndex = 9;
            this.wtTextBox.TabStop = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(736, 133);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(73, 18);
            this.label9.TabIndex = 4;
            this.label9.Text = "WT KG.:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(449, 133);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(132, 18);
            this.label8.TabIndex = 5;
            this.label8.Text = "GREY QUALITY:";
            // 
            // recdMtrsTextBox
            // 
            this.recdMtrsTextBox.BackColor = System.Drawing.SystemColors.Info;
            this.recdMtrsTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.recdMtrsTextBox.Location = new System.Drawing.Point(800, 87);
            this.recdMtrsTextBox.Name = "recdMtrsTextBox";
            this.recdMtrsTextBox.ReadOnly = true;
            this.recdMtrsTextBox.Size = new System.Drawing.Size(89, 24);
            this.recdMtrsTextBox.TabIndex = 6;
            this.recdMtrsTextBox.TabStop = false;
            // 
            // recdTakaTextBox
            // 
            this.recdTakaTextBox.BackColor = System.Drawing.SystemColors.Info;
            this.recdTakaTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.recdTakaTextBox.Location = new System.Drawing.Point(600, 91);
            this.recdTakaTextBox.Name = "recdTakaTextBox";
            this.recdTakaTextBox.ReadOnly = true;
            this.recdTakaTextBox.Size = new System.Drawing.Size(88, 24);
            this.recdTakaTextBox.TabIndex = 5;
            this.recdTakaTextBox.TabStop = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(694, 91);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(110, 18);
            this.label13.TabIndex = 8;
            this.label13.Text = "REC. MTRS.:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(505, 92);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(99, 18);
            this.label12.TabIndex = 9;
            this.label12.Text = "REC. TAKA:";
            // 
            // jobListView
            // 
            this.jobListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.referenceNumber,
            this.challanNo,
            this.mill,
            this.taka,
            this.mtrs,
            this.cardNo,
            this.dispatchDate,
            this.rate,
            this.process,
            this.remark});
            this.jobListView.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.jobListView.ForeColor = System.Drawing.Color.Blue;
            this.jobListView.FullRowSelect = true;
            this.jobListView.GridLines = true;
            this.jobListView.HideSelection = false;
            this.jobListView.Location = new System.Drawing.Point(20, 252);
            this.jobListView.MultiSelect = false;
            this.jobListView.Name = "jobListView";
            this.jobListView.Size = new System.Drawing.Size(935, 177);
            this.jobListView.TabIndex = 12;
            this.jobListView.UseCompatibleStateImageBehavior = false;
            this.jobListView.View = System.Windows.Forms.View.Details;
            // 
            // referenceNumber
            // 
            this.referenceNumber.Text = "REF.NO.";
            this.referenceNumber.Width = 65;
            // 
            // challanNo
            // 
            this.challanNo.Text = "CH. NO.";
            // 
            // mill
            // 
            this.mill.Text = "MILL";
            this.mill.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.mill.Width = 298;
            // 
            // taka
            // 
            this.taka.Text = "TAKA";
            this.taka.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // mtrs
            // 
            this.mtrs.Text = "MTRS";
            this.mtrs.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.mtrs.Width = 80;
            // 
            // cardNo
            // 
            this.cardNo.Text = "CARD NO";
            this.cardNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // dispatchDate
            // 
            this.dispatchDate.Text = "DISP.DATE";
            this.dispatchDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.dispatchDate.Width = 100;
            // 
            // rate
            // 
            this.rate.Text = "RATE";
            this.rate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // process
            // 
            this.process.Text = "PROCESS";
            this.process.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.process.Width = 82;
            // 
            // remark
            // 
            this.remark.Text = "REMARK";
            this.remark.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.remark.Width = 100;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(355, 219);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(260, 20);
            this.label6.TabIndex = 9;
            this.label6.Text = "JOB WISE CHALLAN DETAILS";
            // 
            // createButton
            // 
            this.createButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.createButton.ForeColor = System.Drawing.Color.Red;
            this.createButton.Location = new System.Drawing.Point(980, 214);
            this.createButton.Name = "createButton";
            this.createButton.Size = new System.Drawing.Size(92, 47);
            this.createButton.TabIndex = 13;
            this.createButton.Text = "CREATE CHALLAN";
            this.createButton.UseVisualStyleBackColor = true;
            this.createButton.Click += new System.EventHandler(this.createButton_Click);
            // 
            // viewButton
            // 
            this.viewButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.viewButton.ForeColor = System.Drawing.Color.Red;
            this.viewButton.Location = new System.Drawing.Point(980, 279);
            this.viewButton.Name = "viewButton";
            this.viewButton.Size = new System.Drawing.Size(92, 47);
            this.viewButton.TabIndex = 14;
            this.viewButton.Text = "VIEW CHALLAN";
            this.viewButton.UseVisualStyleBackColor = true;
            this.viewButton.Click += new System.EventHandler(this.viewButton_Click);
            // 
            // printButton
            // 
            this.printButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.printButton.ForeColor = System.Drawing.Color.Red;
            this.printButton.Location = new System.Drawing.Point(980, 344);
            this.printButton.Name = "printButton";
            this.printButton.Size = new System.Drawing.Size(92, 47);
            this.printButton.TabIndex = 16;
            this.printButton.Text = "PRINT CHALLAN";
            this.printButton.UseVisualStyleBackColor = true;
            // 
            // deleteButton
            // 
            this.deleteButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deleteButton.ForeColor = System.Drawing.Color.Red;
            this.deleteButton.Location = new System.Drawing.Point(980, 409);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(92, 47);
            this.deleteButton.TabIndex = 17;
            this.deleteButton.Text = "DELETE CHALLAN";
            this.deleteButton.UseVisualStyleBackColor = true;
            this.deleteButton.Click += new System.EventHandler(this.deleteButton_Click);
            // 
            // saveButton
            // 
            this.saveButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.saveButton.Location = new System.Drawing.Point(223, 504);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(92, 47);
            this.saveButton.TabIndex = 15;
            this.saveButton.Text = "CREATE VOUCHER";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(446, 51);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(130, 18);
            this.label7.TabIndex = 4;
            this.label7.Text = "VOUCHER NO.:";
            // 
            // voucherNoTextBox
            // 
            this.voucherNoTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.voucherNoTextBox.Location = new System.Drawing.Point(594, 50);
            this.voucherNoTextBox.Name = "voucherNoTextBox";
            this.voucherNoTextBox.Size = new System.Drawing.Size(83, 24);
            this.voucherNoTextBox.TabIndex = 1;
            this.voucherNoTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.voucherNoTextBox_KeyDown);
            this.voucherNoTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.voucherNoTextBox_Validating);
            // 
            // refBillNoTextBox
            // 
            this.refBillNoTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.refBillNoTextBox.Location = new System.Drawing.Point(424, 90);
            this.refBillNoTextBox.Name = "refBillNoTextBox";
            this.refBillNoTextBox.Size = new System.Drawing.Size(77, 24);
            this.refBillNoTextBox.TabIndex = 4;
            this.refBillNoTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.refBillNoTextBox_KeyDown);
            // 
            // companyNameTextBox
            // 
            this.companyNameTextBox.BackColor = System.Drawing.SystemColors.Info;
            this.companyNameTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.companyNameTextBox.Location = new System.Drawing.Point(162, 49);
            this.companyNameTextBox.Name = "companyNameTextBox";
            this.companyNameTextBox.ReadOnly = true;
            this.companyNameTextBox.Size = new System.Drawing.Size(278, 24);
            this.companyNameTextBox.TabIndex = 0;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(20, 132);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(140, 18);
            this.label10.TabIndex = 1;
            this.label10.Text = "WEAVER /AADAT";
            // 
            // weaverTextBox
            // 
            this.weaverTextBox.BackColor = System.Drawing.SystemColors.Info;
            this.weaverTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.weaverTextBox.Location = new System.Drawing.Point(162, 131);
            this.weaverTextBox.Name = "weaverTextBox";
            this.weaverTextBox.ReadOnly = true;
            this.weaverTextBox.Size = new System.Drawing.Size(278, 24);
            this.weaverTextBox.TabIndex = 7;
            this.weaverTextBox.TabStop = false;
            // 
            // greyQualityTextBox
            // 
            this.greyQualityTextBox.BackColor = System.Drawing.SystemColors.Info;
            this.greyQualityTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.greyQualityTextBox.Location = new System.Drawing.Point(587, 132);
            this.greyQualityTextBox.Name = "greyQualityTextBox";
            this.greyQualityTextBox.ReadOnly = true;
            this.greyQualityTextBox.Size = new System.Drawing.Size(146, 24);
            this.greyQualityTextBox.TabIndex = 8;
            this.greyQualityTextBox.TabStop = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.label11.Location = new System.Drawing.Point(23, 178);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(93, 18);
            this.label11.TabIndex = 9;
            this.label11.Text = "AVL. TAKA:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.label14.Location = new System.Drawing.Point(212, 177);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(104, 18);
            this.label14.TabIndex = 8;
            this.label14.Text = "AVL. MTRS.:";
            // 
            // avlTakaTextBox
            // 
            this.avlTakaTextBox.BackColor = System.Drawing.SystemColors.Info;
            this.avlTakaTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.avlTakaTextBox.ForeColor = System.Drawing.Color.Red;
            this.avlTakaTextBox.Location = new System.Drawing.Point(118, 177);
            this.avlTakaTextBox.Name = "avlTakaTextBox";
            this.avlTakaTextBox.ReadOnly = true;
            this.avlTakaTextBox.Size = new System.Drawing.Size(88, 24);
            this.avlTakaTextBox.TabIndex = 50;
            this.avlTakaTextBox.TabStop = false;
            this.avlTakaTextBox.Text = "0";
            // 
            // avlMtrsTextBox
            // 
            this.avlMtrsTextBox.BackColor = System.Drawing.SystemColors.Info;
            this.avlMtrsTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.avlMtrsTextBox.ForeColor = System.Drawing.Color.Red;
            this.avlMtrsTextBox.Location = new System.Drawing.Point(318, 175);
            this.avlMtrsTextBox.Name = "avlMtrsTextBox";
            this.avlMtrsTextBox.ReadOnly = true;
            this.avlMtrsTextBox.Size = new System.Drawing.Size(89, 24);
            this.avlMtrsTextBox.TabIndex = 50;
            this.avlMtrsTextBox.TabStop = false;
            this.avlMtrsTextBox.Text = "0.0";
            // 
            // totalTakaTextBox
            // 
            this.totalTakaTextBox.BackColor = System.Drawing.SystemColors.Info;
            this.totalTakaTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalTakaTextBox.Location = new System.Drawing.Point(391, 457);
            this.totalTakaTextBox.Name = "totalTakaTextBox";
            this.totalTakaTextBox.ReadOnly = true;
            this.totalTakaTextBox.Size = new System.Drawing.Size(67, 22);
            this.totalTakaTextBox.TabIndex = 51;
            this.totalTakaTextBox.Text = "0";
            // 
            // totalMtrsTextBox
            // 
            this.totalMtrsTextBox.BackColor = System.Drawing.SystemColors.Info;
            this.totalMtrsTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalMtrsTextBox.Location = new System.Drawing.Point(474, 457);
            this.totalMtrsTextBox.Name = "totalMtrsTextBox";
            this.totalMtrsTextBox.ReadOnly = true;
            this.totalMtrsTextBox.Size = new System.Drawing.Size(67, 22);
            this.totalMtrsTextBox.TabIndex = 51;
            this.totalMtrsTextBox.Text = "0";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(399, 432);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(47, 16);
            this.label15.TabIndex = 52;
            this.label15.Text = "TAKA";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(483, 432);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(55, 16);
            this.label16.TabIndex = 52;
            this.label16.Text = "MTRS.";
            // 
            // viewVoucherButton
            // 
            this.viewVoucherButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.viewVoucherButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.viewVoucherButton.Location = new System.Drawing.Point(483, 504);
            this.viewVoucherButton.Name = "viewVoucherButton";
            this.viewVoucherButton.Size = new System.Drawing.Size(92, 47);
            this.viewVoucherButton.TabIndex = 15;
            this.viewVoucherButton.Text = "VIEW VOUCHER";
            this.viewVoucherButton.UseVisualStyleBackColor = true;
            this.viewVoucherButton.Click += new System.EventHandler(this.viewVoucherButton_Click);
            // 
            // deleteVoucherButton
            // 
            this.deleteVoucherButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deleteVoucherButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.deleteVoucherButton.Location = new System.Drawing.Point(613, 504);
            this.deleteVoucherButton.Name = "deleteVoucherButton";
            this.deleteVoucherButton.Size = new System.Drawing.Size(92, 47);
            this.deleteVoucherButton.TabIndex = 15;
            this.deleteVoucherButton.Text = "DELETE VOUCHER";
            this.deleteVoucherButton.UseVisualStyleBackColor = true;
            this.deleteVoucherButton.Click += new System.EventHandler(this.deleteVoucherButton_Click);
            // 
            // updateButton
            // 
            this.updateButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.updateButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.updateButton.Location = new System.Drawing.Point(353, 504);
            this.updateButton.Name = "updateButton";
            this.updateButton.Size = new System.Drawing.Size(92, 47);
            this.updateButton.TabIndex = 15;
            this.updateButton.Text = "UPDATE VOUCHER";
            this.updateButton.UseVisualStyleBackColor = true;
            this.updateButton.Click += new System.EventHandler(this.updateButton_Click);
            // 
            // MillDispatchWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.totalMtrsTextBox);
            this.Controls.Add(this.totalTakaTextBox);
            this.Controls.Add(this.weaverTextBox);
            this.Controls.Add(this.companyNameTextBox);
            this.Controls.Add(this.greyQualityTextBox);
            this.Controls.Add(this.refBillNoTextBox);
            this.Controls.Add(this.deleteButton);
            this.Controls.Add(this.printButton);
            this.Controls.Add(this.deleteVoucherButton);
            this.Controls.Add(this.viewVoucherButton);
            this.Controls.Add(this.updateButton);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.viewButton);
            this.Controls.Add(this.createButton);
            this.Controls.Add(this.jobListView);
            this.Controls.Add(this.avlMtrsTextBox);
            this.Controls.Add(this.avlTakaTextBox);
            this.Controls.Add(this.recdMtrsTextBox);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.recdTakaTextBox);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.voucherNoTextBox);
            this.Controls.Add(this.wtTextBox);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.dateTextBox);
            this.Controls.Add(this.jobTypeComboBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "MillDispatchWindow";
            this.Size = new System.Drawing.Size(1111, 571);
            this.Load += new System.EventHandler(this.MillDispatchWindow_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox jobTypeComboBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox dateTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox wtTextBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox recdMtrsTextBox;
        private System.Windows.Forms.TextBox recdTakaTextBox;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ListView jobListView;
        private System.Windows.Forms.ColumnHeader challanNo;
        private System.Windows.Forms.ColumnHeader mill;
        private System.Windows.Forms.ColumnHeader cardNo;
        private System.Windows.Forms.ColumnHeader dispatchDate;
        private System.Windows.Forms.ColumnHeader taka;
        private System.Windows.Forms.ColumnHeader mtrs;
        private System.Windows.Forms.ColumnHeader rate;
        private System.Windows.Forms.ColumnHeader process;
        private System.Windows.Forms.ColumnHeader remark;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button createButton;
        private System.Windows.Forms.Button viewButton;
        private System.Windows.Forms.Button printButton;
        private System.Windows.Forms.Button deleteButton;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox voucherNoTextBox;
        private System.Windows.Forms.TextBox refBillNoTextBox;
        private System.Windows.Forms.TextBox companyNameTextBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox weaverTextBox;
        private System.Windows.Forms.TextBox greyQualityTextBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox avlTakaTextBox;
        private System.Windows.Forms.TextBox avlMtrsTextBox;
        private System.Windows.Forms.TextBox totalTakaTextBox;
        private System.Windows.Forms.TextBox totalMtrsTextBox;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ColumnHeader referenceNumber;
        private System.Windows.Forms.Button viewVoucherButton;
        private System.Windows.Forms.Button deleteVoucherButton;
        private System.Windows.Forms.Button updateButton;
    }
}
