﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Texo_Advance.CustomControl
{
    class PassBookBillDataForMultiColumnDisplay
    {
        public string billNo { get; set; }

        public string date { get; set; }

        public long taxableValue { get; set; }

        public double netAmount { get; set; }

        public double pendingAmount { get; set; }

        public long billType { get; set; }
    }
}
