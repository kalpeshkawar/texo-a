﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Texo_Advance.CustomControl
{
    class JobWorkReceiptItemForMultiColumnDisplay
    {
        public string challanNo { get; set; }

        public string strItemName { get; set; }

        public long available_pcs { get; set; }

        public double available_mtrsQty { get; set; }

        public long jobProcessItemDetails_id { get; set; }
    }
}
