﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Management.Instrumentation;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Texo_Advance.DBItems;

namespace Texo_Advance.MasterItems
{
    public partial class CreateYear : Form
    {
        MasterDBData db = new MasterDBData();
        public CreateYear()
        {
            InitializeComponent();
        }

        private void toDate_ValueChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void create_btn_Click(object sender, EventArgs e)
        {
            string startDate = toDate.Value.ToString("yyyy");
            string endDate = end_datePicker.Value.ToString("yy");
            string CompanyName = Properties.Settings.Default.CompanyName;
            long companyId = Properties.Settings.Default.CompanyId;
            string financialYear = startDate + "-" + endDate;
            String dbName = CompanyName + "_" + financialYear +"_"+ System.DateTime.Now.ToString("MMddyyyy");
            financial_year year = new financial_year();
            year.company_id = companyId;
            year.db_name = dbName;
            year.financial_year1 = financialYear;
            if (db.setFinancialYear(year))
                MessageBox.Show("Succesfully Created " + financialYear + " year", "Created!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            else
                MessageBox.Show("Failed Creation of year "+financialYear, "Failed!!", MessageBoxButtons.OK, MessageBoxIcon.Error);







        }
    }
}
