﻿namespace Texo_Advance.MasterItems
{
    partial class FinancialYear
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.create = new System.Windows.Forms.Button();
            this.switch_btn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // create
            // 
            this.create.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.create.Location = new System.Drawing.Point(99, 57);
            this.create.Name = "create";
            this.create.Size = new System.Drawing.Size(132, 60);
            this.create.TabIndex = 0;
            this.create.Text = "Create";
            this.create.UseVisualStyleBackColor = false;
            this.create.Click += new System.EventHandler(this.create_Click);
            // 
            // switch_btn
            // 
            this.switch_btn.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.switch_btn.Location = new System.Drawing.Point(99, 142);
            this.switch_btn.Name = "switch_btn";
            this.switch_btn.Size = new System.Drawing.Size(132, 59);
            this.switch_btn.TabIndex = 1;
            this.switch_btn.Text = "Switch Year";
            this.switch_btn.UseVisualStyleBackColor = false;
            this.switch_btn.Click += new System.EventHandler(this.switch_btn_Click);
            // 
            // FinancialYear
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(332, 256);
            this.Controls.Add(this.switch_btn);
            this.Controls.Add(this.create);
            this.Name = "FinancialYear";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Financial_Year";
            this.Load += new System.EventHandler(this.Financial_Year_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button create;
        private System.Windows.Forms.Button switch_btn;
    }
}