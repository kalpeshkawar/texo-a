﻿namespace Texo_Advance.MasterItems
{
    partial class CreateYear
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.toDate = new System.Windows.Forms.DateTimePicker();
            this.end_datePicker = new System.Windows.Forms.DateTimePicker();
            this.create_btn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // toDate
            // 
            this.toDate.CalendarMonthBackground = System.Drawing.SystemColors.ActiveCaption;
            this.toDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.toDate.Location = new System.Drawing.Point(81, 17);
            this.toDate.Name = "toDate";
            this.toDate.Size = new System.Drawing.Size(125, 20);
            this.toDate.TabIndex = 0;
            this.toDate.Value = new System.DateTime(2020, 8, 10, 0, 0, 0, 0);
            this.toDate.ValueChanged += new System.EventHandler(this.toDate_ValueChanged);
            // 
            // end_datePicker
            // 
            this.end_datePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.end_datePicker.Location = new System.Drawing.Point(340, 17);
            this.end_datePicker.Name = "end_datePicker";
            this.end_datePicker.Size = new System.Drawing.Size(114, 20);
            this.end_datePicker.TabIndex = 1;
            this.end_datePicker.Value = new System.DateTime(2020, 8, 10, 0, 0, 0, 0);
            // 
            // create_btn
            // 
            this.create_btn.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.create_btn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.create_btn.Location = new System.Drawing.Point(179, 63);
            this.create_btn.Name = "create_btn";
            this.create_btn.Size = new System.Drawing.Size(124, 34);
            this.create_btn.TabIndex = 2;
            this.create_btn.Text = "Create";
            this.create_btn.UseVisualStyleBackColor = false;
            this.create_btn.Click += new System.EventHandler(this.create_btn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Start Date";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(274, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "End Date";
            // 
            // CreateYear
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(476, 134);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.create_btn);
            this.Controls.Add(this.end_datePicker);
            this.Controls.Add(this.toDate);
            this.Name = "CreateYear";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CreateYear";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker toDate;
        private System.Windows.Forms.DateTimePicker end_datePicker;
        private System.Windows.Forms.Button create_btn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}