﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;
using Texo_Advance.DBItems;
using System.Data.Entity;
using Texo_Advance.CustomControl;

namespace Texo_Advance
{
    public partial class GreyPurchaseWindow : UserControl
    {
        private string strCompanyName = "";
        private string companyGstn = "";
        List<greypurchase_type> greyPurchaseTypeList;
        MasterDBData DB = new MasterDBData();
        PartyDBData partyDB = new PartyDBData();
        greypurchase_details greyPurchaseDetailForFetch;
        greypurchase_details greyPurchaseDetail = new greypurchase_details();
        TakaDetailsWindow tdw = new TakaDetailsWindow();
        public GreyPurchaseWindow(string str,string gstn)
        {
            InitializeComponent();
            strCompanyName = str;
            companyGstn = gstn;
            fillPurchaseTypeList();
        }

        private void GreyPurchaseWindow_Load(object sender, EventArgs e)
        {
            companyNameTextBox.Text = strCompanyName;
            dateTextBox.Text = DateTime.Today.ToString("dd-MM-yyyy");
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double dTaka = 0;
            double dMtrs = 0;
            if(recdTakaTextBox.Text != "" || recdMtrsTextBox.Text != "")
            {
                dTaka = Convert.ToDouble(recdTakaTextBox.Text);
                dMtrs = Convert.ToDouble(recdMtrsTextBox.Text);
            }
            if (dTaka > 0 && dMtrs > 0)
            {

                tdw.setDefaultData(Convert.ToInt64(srnoTextBox.Text),dTaka, dMtrs);
                tdw.ShowDialog();
                
            }
            else
                MessageBox.Show("Please Enter correct Recd. Taka and Taka Mtrs.", "Error", MessageBoxButtons.OK);
            
        }

        public void setDafaultData()
        {
            fillBrokerDetails();
            fillTransportDetails();
            fillGstTypeList();
            fillCityList();
            fillGreyQualityList();
            fillCheckerList();
            fillPartyNames();
            long lSrNo = partyDB.getGreyPurchaseBillNo(Convert.ToInt64(greyPurchaseTypeComboBox.SelectedValue));
            if(lSrNo==-1)
            {
                MessageBox.Show("Unable to retrieve Grey Purchase Data", "Error", MessageBoxButtons.OK);
                return;
            }
            else if(lSrNo == 0)
            {
                srnoTextBox.Text = Convert.ToString("1");
            }
            else
                srnoTextBox.Text = Convert.ToString(lSrNo+1);
            billNoTextBox.Focus();
            tdw.clearTakaDetails();
            addButton.Enabled = true;
            updateButton.Enabled = false;
            deleteButton.Enabled = false;
        }

        private void fillPartyNames()
        {
            var partyList = DB.getPartyDetails(2);
            /*if (partyList != null)
            {
                partyNameComboBox.DisplayMember = "party_name";
                partyNameComboBox.ValueMember = "partyid";
                partyNameComboBox.DataSource = partyList;
            }*/
            int iColumnCount = 5;


            DataTable myDataTable = new DataTable("ParentTable");
            // Declare variables for DataColumn and DataRow objects.
            DataColumn myDataColumn;
            DataRow myDataRow;

            //Add some coulumns
            for (int index = 0; index < iColumnCount; index++)
            {
                myDataColumn = new DataColumn();
                myDataColumn.DataType = typeof(string);
                if (index == 0)
                    myDataColumn.ColumnName = "NAME";
                else if (index == 1)
                    myDataColumn.ColumnName = "BROKER";
                else if (index == 2)
                    myDataColumn.ColumnName = "ADDRESS";
                else if (index == 3)
                    myDataColumn.ColumnName = "CITY";
                else if (index == 4)
                    myDataColumn.ColumnName = "PARTY_ID";
                //myDataColumn.Caption = "ParentItem";
                myDataColumn.AutoIncrement = false;
                myDataColumn.ReadOnly = false;
                myDataColumn.Unique = false;
                // Add the Column to the DataColumnCollection.
                myDataTable.Columns.Add(myDataColumn);
            }


            //Add some more rows
            for (int index = 0; index < partyList.Count; index++)
            {
                myDataRow = myDataTable.NewRow();
                myDataRow[0] = partyList[index].party_name;
                myDataRow[1] = DB.getBrokerNameFromID(Convert.ToInt64(partyList[index].brokerid));
                myDataRow[2] = partyList[index].address + "  " + partyList[index].address2;
                myDataRow[3] = DB.getCityName((long)partyList[index].city_id);
                myDataRow[4] = partyList[index].partyid;
                myDataTable.Rows.Add(myDataRow);
            }


            //Now set the Data of the ColumnComboBox
            partyNameComboBox.DisplayMember = "NAME";
            partyNameComboBox.ValueMember = "PARTY_ID";
            partyNameComboBox.Data = myDataTable;
            //Set which row will be displayed in the text box
            //If you set this to a column that isn't displayed then the suggesting functionality won't work.
            partyNameComboBox.ColumnSpacing = 50;
            partyNameComboBox.ViewColumn = 0;

            partyNameComboBox.Columns[0].Width = 150;
            partyNameComboBox.Columns[1].Width = 150;
            partyNameComboBox.Columns[2].Width = 250;
            partyNameComboBox.Columns[3].Width = 50;
            //Set a few columns to not be shown
            partyNameComboBox.Columns[4].Display = false;
            partyNameComboBox.SelectedIndex = -1;
            partyNameComboBox.SelectedIndex = 0;

        }

        private void fillBrokerDetails()
        {

            var brokerList = DB.getBrokerDetails();
            if (brokerList != null)
            {
                brokerComboBox.DisplayMember = "party_name";
                brokerComboBox.ValueMember = "brokerid";
                brokerComboBox.DataSource = brokerList;

            }
        }
        private void fillTransportDetails()
        {

            var transportList = DB.getTransportDetails();
            if (transportList != null)
            {
                transportComboBox.DisplayMember = "name";
                transportComboBox.ValueMember = "transportid";
                transportComboBox.DataSource = transportList;
            }
        }

        private void fillPurchaseTypeList()
        {
            greyPurchaseTypeList = DB.getGreyPurchaseTypeList();
            if(greyPurchaseTypeList != null)
            {
                greyPurchaseTypeComboBox.DisplayMember = "type";
                greyPurchaseTypeComboBox.ValueMember = "greyPurchaseType_id";
                greyPurchaseTypeComboBox.DataSource = greyPurchaseTypeList;
            }
        }

        private void fillCityList()
        {

            var stateCodeList = DB.getCityList();
            if (stateCodeList != null)
            {
                cityComboBox.DisplayMember = "name";
                cityComboBox.ValueMember = "city_id";
                cityComboBox.DataSource = stateCodeList;
            }
        }

        private void fillGstTypeList()
        {

            gstTypeComboBox.SelectedIndex = -1;
            var gstTypeList = DB.getGstTypeList();
            if (gstTypeList != null)
            {
                gstTypeComboBox.DisplayMember = "type";
                gstTypeComboBox.ValueMember = "gsttype_id";
                gstTypeComboBox.DataSource = gstTypeList;
            }
        }

        private void fillGreyQualityList()
        {

            var greyQualityList = DB.getGreyQualityList();
            if (greyQualityList != null)
            {
                qualityComboBox.DisplayMember = "quality_name";
                qualityComboBox.ValueMember = "greyquality_id";
                qualityComboBox.DataSource = greyQualityList;
            }
            
        }

        private void fillCheckerList()
        {            
            var checkerList = DB.getCheckerList();
            if (checkerList != null)
            {
                checkerComboBox.DisplayMember = "checker_name";
                checkerComboBox.ValueMember = "checkertype_id";
                checkerComboBox.DataSource = checkerList;
            }
            
        }
        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            AccountManagerForm acc = new AccountManagerForm();
            acc.setDefaultAccountType(2);
            acc.ShowDialog();
            fillPartyNames();
        }

        private void partyNameComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            fetchPartyDataFromPartyName();
            if(partyNameComboBox.SelectedIndex != -1)
                if((long)greyPurchaseTypeComboBox.SelectedValue == 2)
                    fillRefBillData(Convert.ToInt64(partyNameComboBox["PARTY_ID"].ToString()));
        }

        private void fetchPartyDataFromPartyName()
        {
            if (partyNameComboBox.SelectedIndex != -1)
            {
                fillBrokerDetails();
                fillTransportDetails();
                fillGstTypeList();
                fillCityList();
                fillGreyQualityList();
                fillCheckerList();
                using (var db = new Entities())
                {
                    //string value = partyComboBox.di;
                    if (partyNameComboBox.Items.Count == 0)
                        return;
                    var partyDetail1 = partyNameComboBox["PARTY_ID"].ToString();
                    
                    var partyDetail = DB.getPartyDetailFromID(2, Convert.ToInt64(partyDetail1));
                    if (partyDetail == null)
                        return;
                    //var partyDetail = party_Detail.Find(x=> x.party_name == strPartyName);

                    transportComboBox.SelectedValue = partyDetail.transportid;
                    brokerComboBox.SelectedValue = partyDetail.brokerid;
                    cityComboBox.SelectedValue = partyDetail.city_id;
                    
                    gstnTextBox.Text = partyDetail.gst;
                    string gstn = gstnTextBox.Text;
                    if (gstn.Length > 0)
                    {
                        string strPartyStateCode = gstn.Substring(0, 2);
                        string strCompanyStateCode = companyGstn.Substring(0,2);
                        if (strCompanyStateCode == strPartyStateCode)
                            gstTypeComboBox.SelectedIndex = 1;
                        else
                            gstTypeComboBox.SelectedIndex = 0;
                    }


                }
            }
        }

        private void cityComboBox_KeyDown(object sender, KeyEventArgs e)
        {
            int iSelectIndx = -1;
            if (e.KeyCode == Keys.Enter)
            {
                iSelectIndx = cityComboBox.FindString(cityComboBox.Text);
                if (iSelectIndx == -1)
                {
                    if (MessageBox.Show("Do You want to add new City?", "ADD CITY", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        DB.addCity(cityComboBox.Text);
                        
                        fillCityList();
                    }
                }
                else
                {
                    cityComboBox.SelectedIndex = iSelectIndx;
                }

            }
        }

        private void qualityComboBox_KeyDown(object sender, KeyEventArgs e)
        {
            int iSelectIndx = -1;
            if (e.KeyCode == Keys.Enter)
            {
                iSelectIndx = qualityComboBox.FindString(qualityComboBox.Text);
                if (iSelectIndx == -1)
                {
                    if (MessageBox.Show("Do You want to add new Grey Quality?", "ADD Quality", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        DB.addGreyQuality(qualityComboBox.Text);
                        
                        fillGreyQualityList();
                    }
                }
                else
                {
                    qualityComboBox.SelectedIndex = iSelectIndx;
                }
            }

        }

        private void recdTakaTextBox_Validating(object sender, CancelEventArgs e)
        {
            long lRecdTaka = 0;
            bool bValidTakas = long.TryParse(recdTakaTextBox.Text,out lRecdTaka);
            if(!bValidTakas)
            {
                MessageBox.Show("Please Enter Valid No. of Recd. Takas", "ALERT", MessageBoxButtons.OK);
                recdTakaTextBox.Text = "0";
                recdTakaTextBox.Focus();
                return;
            }
            //setGrossAmount();
        }

        private void recdMtrsTextBox_Validating(object sender, CancelEventArgs e)
        {
            double dRecdMtrs = 0;
            bool bValidRecdMtrs = double.TryParse(recdMtrsTextBox.Text, out dRecdMtrs);
            if (!bValidRecdMtrs)
            {
                MessageBox.Show("Please Enter Valid No. of Recd. Mtrs.", "ALERT", MessageBoxButtons.OK);
                recdMtrsTextBox.Text = "0.0";
                recdMtrsTextBox.Focus();
                return;
            }
            setGrossAmount();
        }

        private void purRateTextBox_Validating(object sender, CancelEventArgs e)
        {
            double dPurRate = 0;
            bool bValidPurRate = double.TryParse(purRateTextBox.Text, out dPurRate);
            if (!bValidPurRate)
            {
                MessageBox.Show("Please Enter Valid Purchase Rate", "ALERT", MessageBoxButtons.OK);
                purRateTextBox.Text = "0.0";
                purRateTextBox.Focus();
                return;
            }
            setGrossAmount();
        }

        private void disPerTextBox_TextChanged(object sender, EventArgs e)
        {
            double dDiscountPercentage = 0;
            bool bFlag = double.TryParse(disPerTextBox.Text, out dDiscountPercentage);
            if (bFlag)
                dDiscountPercentage = Convert.ToDouble(disPerTextBox.Text);
            else
            {
                disPerTextBox.Text = "0";
                disPerTextBox.SelectAll();
                dDiscountPercentage = 0;
            }
                

            double dGrossAmount = Convert.ToDouble(grossAmtTextBox.Text);
            double dDiscountAmount = (dDiscountPercentage * dGrossAmount) / 100;
            discAmtTextBox.Text = Convert.ToString(Math.Round(dDiscountAmount));
        }

        private void sgstPerTextBox_TextChanged(object sender, EventArgs e)
        {
            double dSgstPercentage = 0;
            bool bFlag = double.TryParse(sgstPerTextBox.Text, out dSgstPercentage);
            if (bFlag)
                dSgstPercentage = Convert.ToDouble(sgstPerTextBox.Text);
            else
            {
                sgstPerTextBox.Text = "0";
                sgstPerTextBox.SelectAll();
                dSgstPercentage = 0;
            }
                
            double dTaxableValue = Convert.ToDouble(taxableValueTextBox.Text);
            double dSgstAmount = (dSgstPercentage * dTaxableValue) / 100;
            sgstAmtTextBox.Text = Convert.ToString(dSgstAmount);
        }

        private void cgstPerTextBox_TextChanged(object sender, EventArgs e)
        {
            double dcgstPercentage = 0;
            bool bFlag = double.TryParse(cgstPerTextBox.Text, out dcgstPercentage);
            if (bFlag)
                dcgstPercentage = Convert.ToDouble(cgstPerTextBox.Text);
            else
            {
                cgstPerTextBox.Text = "0";
                cgstPerTextBox.SelectAll();
                dcgstPercentage = 0;
            }
                
            double dTaxableValue = Convert.ToDouble(taxableValueTextBox.Text);
            double dcgstAmount = (dcgstPercentage * dTaxableValue) / 100;
            cgstAmtTextBox.Text = Convert.ToString(dcgstAmount);
        }

        private void igstPerTextBox_TextChanged(object sender, EventArgs e)
        {
            double digstPercentage = 0;
            bool bFlag = double.TryParse(igstPerTextBox.Text, out digstPercentage);
            if (bFlag)
                digstPercentage = Convert.ToDouble(igstPerTextBox.Text);
            else
            {
                igstPerTextBox.Text = "0";
                igstPerTextBox.SelectAll();
                digstPercentage = 0;
            }
                
            double dTaxableValue = Convert.ToDouble(taxableValueTextBox.Text);
            double digstAmount = (digstPercentage * dTaxableValue) / 100;
            igstAmtTextBox.Text = Convert.ToString(digstAmount);
        }

        private void foldLessTextBox_TextChanged(object sender, EventArgs e)
        {
            updateTaxableValueAndNetAmount();
        }

        private void addAnyTextBox_TextChanged(object sender, EventArgs e)
        {
            updateTaxableValueAndNetAmount();
        }

        private void lessAnyTextBox_TextChanged(object sender, EventArgs e)
        {
            updateTaxableValueAndNetAmount();
        }

        private void igstAmtTextBox_TextChanged(object sender, EventArgs e)
        {
            updateTaxableValueAndNetAmount();
        }

        private void cgstAmtTextBox_TextChanged(object sender, EventArgs e)
        {
            updateTaxableValueAndNetAmount();
        }

        private void sgstAmtTextBox_TextChanged(object sender, EventArgs e)
        {
            updateTaxableValueAndNetAmount();
        }

        private void discAmtTextBox_TextChanged(object sender, EventArgs e)
        {
            updateTaxableValueAndNetAmount();
        }

        private void checkerComboBox_KeyDown(object sender, KeyEventArgs e)
        {
            int iSelectIndx = -1;
            if (e.KeyCode == Keys.Enter)
            {
                iSelectIndx = checkerComboBox.FindString(checkerComboBox.Text);
                if (iSelectIndx == -1)
                {
                    if (MessageBox.Show("Do You want to add new Checker?", "ADD Checker", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        DB.addChecker(checkerComboBox.Text);
                        fillCheckerList();
                    }
                }
                else
                {
                    checkerComboBox.SelectedIndex = iSelectIndx;
                }

            }
        }

        private void transportComboBox_KeyDown(object sender, KeyEventArgs e)
        {
            int iSelectIndx = -1;
            if (e.KeyCode == Keys.Enter)
            {
                iSelectIndx = transportComboBox.FindString(transportComboBox.Text);
                if (iSelectIndx == -1)
                {
                    if (MessageBox.Show("Do You want to add new Checker?", "ADD Checker", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        //cityComboBox.Items.Add(cityComboBox.Text);
                        DB.addTransport(transportComboBox.Text);
                        fillTransportDetails();
                    }
                }
                else
                {
                    transportComboBox.SelectedIndex = iSelectIndx;
                }

            }
        }

        private void setGrossAmount()
        {
            //double dTaka = Convert.ToDouble(recdTakaTextBox.Text);
            double dMtrs = Convert.ToDouble(recdMtrsTextBox.Text);
            double dPurchaseRate = Convert.ToDouble(purRateTextBox.Text);

            double dGrossAmount = dMtrs * dPurchaseRate;
            grossAmtTextBox.Text = Convert.ToString(Math.Round(dGrossAmount));
        }

        private void grossAmtTextBox_TextChanged(object sender, EventArgs e)
        {
            updateTaxableValueAndNetAmount();

        }

        private void updateTaxableValueAndNetAmount()
        {
            double dDiscount = 0;
            bool bFlag = double.TryParse(discAmtTextBox.Text, out dDiscount);
            if (!bFlag)
            {
                dDiscount = Convert.ToDouble(disPerTextBox.Text) * Convert.ToDouble(grossAmtTextBox.Text) / 100;
                discAmtTextBox.Text = Convert.ToString(dDiscount);
                discAmtTextBox.SelectAll();
            }
                

            double dFoldLess = 0;
            bFlag = double.TryParse(foldLessTextBox.Text, out dFoldLess);
            if (!bFlag)
            {
                foldLessTextBox.Text = "0";
                foldLessTextBox.SelectAll();
                dFoldLess = 0;
            }
                

            double dAddAny = 0;
            bFlag = double.TryParse(addAnyTextBox.Text, out dAddAny);
            if (!bFlag)
            {
                addAnyTextBox.Text = "0";
                addAnyTextBox.SelectAll();
                dAddAny = 0;
            }
                

            double dLessAny = 0;
            bFlag = double.TryParse(lessAnyTextBox.Text, out dLessAny);
            if (!bFlag)
            {
                dLessAny = 0;
                lessAnyTextBox.Text = "0";
                lessAnyTextBox.SelectAll();
            }
                

            double dGrossAmount = 0;
            bFlag = double.TryParse(grossAmtTextBox.Text, out dGrossAmount);
            if (!bFlag)
                dGrossAmount = 0;


            double dTaxableValue = dGrossAmount - dDiscount - dFoldLess + dAddAny - dLessAny;
            taxableValueTextBox.Text = Convert.ToString(Math.Round(dTaxableValue));

            double dSgstAmount = 0;
            bFlag = double.TryParse(sgstAmtTextBox.Text, out dGrossAmount);
            if (!bFlag)
            {
                dSgstAmount = Convert.ToDouble(sgstPerTextBox.Text) * dTaxableValue / 100;
                sgstAmtTextBox.Text = Convert.ToString(dSgstAmount);
                sgstAmtTextBox.SelectAll();
            }
                

            double dCgstAmount = 0;
            bFlag = double.TryParse(cgstAmtTextBox.Text, out dCgstAmount);
            if (!bFlag)
            {
                dCgstAmount = Convert.ToDouble(cgstPerTextBox.Text) * dTaxableValue / 100;
                cgstAmtTextBox.Text = Convert.ToString(dCgstAmount);
                cgstAmtTextBox.SelectAll();
            }
                 

            double dIgstAmount = 0;
            bFlag = double.TryParse(igstAmtTextBox.Text, out dIgstAmount);
            if (!bFlag)
            {
                dIgstAmount = Convert.ToDouble(igstPerTextBox.Text) * dTaxableValue / 100;
                igstAmtTextBox.Text = Convert.ToString(dIgstAmount);
                igstAmtTextBox.SelectAll();
            }
                
            
            if (gstTypeComboBox.SelectedIndex == 0)
            {
                double dNetAmount = dTaxableValue + dIgstAmount;
                netAmtTextBox.Text = Convert.ToString(Math.Round(dNetAmount));
            }
            else
            {
                double dNetAmount = dTaxableValue + dCgstAmount + dSgstAmount;
                netAmtTextBox.Text = Convert.ToString(Math.Round(dNetAmount));
            }
        }

        private void gstTypeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(gstTypeComboBox.SelectedIndex == 0)
            {
                sgstAmtTextBox.Enabled = false;
                cgstAmtTextBox.Enabled = false;
                sgstPerTextBox.Text = "0";
                cgstPerTextBox.Text = "0";
                sgstPerTextBox.Enabled = false;
                cgstPerTextBox.Enabled = false;
                igstAmtTextBox.Enabled = true;
                igstPerTextBox.Text = "5";
                igstPerTextBox.Enabled = true;
            }
            else if (gstTypeComboBox.SelectedIndex == 1)
            {
                sgstAmtTextBox.Enabled = true;
                cgstAmtTextBox.Enabled = true;
                sgstPerTextBox.Text = "2.5";
                cgstPerTextBox.Text = "2.5";
                sgstPerTextBox.Enabled = true;
                cgstPerTextBox.Enabled = true;
                igstAmtTextBox.Enabled = false;
                igstPerTextBox.Text = "5";
                igstPerTextBox.Enabled = false;
            }
        }

        private void taxableValueTextBox_TextChanged(object sender, EventArgs e)
        {
            double dcgstPercentage = Convert.ToDouble(cgstPerTextBox.Text);
            double dTaxableValue = Convert.ToDouble(taxableValueTextBox.Text);
            double dcgstAmount = (dcgstPercentage * dTaxableValue) / 100;
            double dSgstPercentage = Convert.ToDouble(sgstPerTextBox.Text);
            //double dTaxableValue = Convert.ToDouble(taxableValueTextBox.Text);
            double dSgstAmount = (dSgstPercentage * dTaxableValue) / 100;
            double digstPercentage = Convert.ToDouble(igstPerTextBox.Text);
            //double dTaxableValue = Convert.ToDouble(taxableValueTextBox.Text);
            double digstAmount = (digstPercentage * dTaxableValue) / 100;
            if(gstTypeComboBox.SelectedIndex == 0)
                igstAmtTextBox.Text = Convert.ToString(digstAmount);
            else
            {
                sgstAmtTextBox.Text = Convert.ToString(dSgstAmount);
                cgstAmtTextBox.Text = Convert.ToString(dcgstAmount);
            }
            
        }

        private void dateTextBox_Validating(object sender, CancelEventArgs e)
        {
            DateTime dt;
            bool isValid = DateTime.TryParseExact(dateTextBox.Text, "dd-MM-yyyy", null, DateTimeStyles.None, out dt);
            if (!isValid)
            {
                MessageBox.Show("Please enter the valid date", "DATE", MessageBoxButtons.OK);
                dateTextBox.Text = DateTime.Today.ToString("dd-MM-yyyy");
                dateTextBox.Focus();
            }
        }

        private void srnoTextBox_Validating(object sender, CancelEventArgs e)
        {
            long lSrNo = 0;
            bool bValidSrNo = long.TryParse(srnoTextBox.Text, out lSrNo);
            if(!bValidSrNo)
            {
                MessageBox.Show("Please enter a valid Sr.No", "ALERT", MessageBoxButtons.OK);
                srnoTextBox.Focus();
                return;
            }
        }

        private void billNoTextBox_Validating(object sender, CancelEventArgs e)
        {
            string strBillNo = billNoTextBox.Text;
            if(!strBillNo.All(char.IsLetterOrDigit))
            {
                MessageBox.Show("Please enter a valid Bill No", "ALERT", MessageBoxButtons.OK);
                billNoTextBox.Focus();
                return;
            }

        }

        private void dueDaysTextBox_Validating(object sender, CancelEventArgs e)
        {
            long lDueDays = 0;
            bool bValidDueDays = long.TryParse(dueDaysTextBox.Text, out lDueDays);
            if (!bValidDueDays)
            {
                MessageBox.Show("Please enter a valid Due Date", "ALERT", MessageBoxButtons.OK);
                dueDaysTextBox.Text = "0";
                dueDaysTextBox.Focus();
                return;
            }
        }

        private void interestTextBox_Validating(object sender, CancelEventArgs e)
        {
            double dInterest = 0;
            bool bValidInerest = double.TryParse(interestTextBox.Text, out dInterest);
            if (!bValidInerest)
            {
                MessageBox.Show("Please enter a valid Due Interest Percentage", "ALERT", MessageBoxButtons.OK);
                interestTextBox.Text = "0.0";
                interestTextBox.Focus();
                return;
            }
        }

        private void wtTextBox_Validating(object sender, CancelEventArgs e)
        {
            double dWeight = 0;
            bool bValidWeight = double.TryParse(wtTextBox.Text, out dWeight);
            if (!bValidWeight)
            {
                MessageBox.Show("Please enter a valid Weight", "ALERT", MessageBoxButtons.OK);
                wtTextBox.Text = "0.0";
                wtTextBox.Focus();
                return;
            }
        }

        private void hsncTextBox_Validating(object sender, CancelEventArgs e)
        {
            long lHsnCode = 0;
            bool bValidHsnCode = long.TryParse(hsncTextBox.Text, out lHsnCode);
            if(hsncTextBox.Text != "")
            {
                if (!bValidHsnCode)
                {
                    MessageBox.Show("Please enter a valid HSN  Code", "ALERT", MessageBoxButtons.OK);
                    hsncTextBox.Text = "";
                    hsncTextBox.Focus();
                    return;
                }
            }
            
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            bool bBillExists = partyDB.checkGreyPurchaseBillExists(Convert.ToInt64(srnoTextBox.Text),billNoTextBox.Text,Convert.ToInt64(partyNameComboBox["PARTY_ID"].ToString()));
            if (bBillExists)
            {
                MessageBox.Show("Bill of party with same bill no. already exists", "ALERT", MessageBoxButtons.OK);
                return;
            }
            double dNetAmount = 0;
            bool bFlag = double.TryParse(netAmtTextBox.Text,out dNetAmount);
            if(!bFlag || dNetAmount <= 0)
            {
                MessageBox.Show("Invalid Bill", "ALERT", MessageBoxButtons.OK);
                return;
            }
                
            greyPurchaseDetail = getGreyPurchaseData(greyPurchaseDetail);
            bFlag = partyDB.addGreyPurchaseBill(greyPurchaseDetail);
            if (bFlag)
            {
                MessageBox.Show("Bill Added Successfully", "Grey Bill", MessageBoxButtons.OK);
                resetAllData();
                setDafaultData();
            }
            else
                MessageBox.Show("Bill not added Successfully", "Grey Bill", MessageBoxButtons.OK);
            srnoTextBox.Focus();
        }

        private greypurchase_details getGreyPurchaseData(greypurchase_details greyPurchaseBill)
        {
            greyPurchaseBill.addAny = Convert.ToDouble(addAnyTextBox.Text);
            greyPurchaseBill.greyPurchaseType_id = Convert.ToInt64(greyPurchaseTypeComboBox.SelectedValue);
            if(greyPurchaseBill.greyPurchaseType_id == 1)
            {
                greyPurchaseBill.billNo = billNoTextBox.Text;
                greyPurchaseBill.orderNo = orderTextBox.Text;
            }
            else
            {
                //greyPurchaseBill.billNo = "0";
                greyPurchaseBill.billNo = billNoTextBox.Text;
                greyPurchaseBill.orderNo = refBillNoComboBox.Text;
            }
               
            greyPurchaseBill.broker_id = Convert.ToInt64(brokerComboBox.SelectedValue);
            greyPurchaseBill.cgstAmount = Convert.ToDouble(cgstAmtTextBox.Text);
            greyPurchaseBill.cgstRate = Convert.ToDouble(cgstPerTextBox.Text);
            greyPurchaseBill.checker = Convert.ToInt64(checkerComboBox.SelectedValue);
            greyPurchaseBill.city = Convert.ToInt64(cityComboBox.SelectedValue);
            greyPurchaseBill.date = DateTime.ParseExact(dateTextBox.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            greyPurchaseBill.discountAmount = Convert.ToDouble(discAmtTextBox.Text);
            greyPurchaseBill.discountPercentage = Convert.ToDouble(disPerTextBox.Text);
            greyPurchaseBill.dueDays = Convert.ToInt64(dueDaysTextBox.Text);
            greyPurchaseBill.foldLess = Convert.ToDouble(foldLessTextBox.Text);
            greyPurchaseBill.greypurchase_id = Convert.ToInt64(srnoTextBox.Text);
            greyPurchaseBill.greytaka_details = tdw.takaDetails;
            greyPurchaseBill.grossAmount = Convert.ToDouble(grossAmtTextBox.Text);
            greyPurchaseBill.gstn = gstnTextBox.Text;
            greyPurchaseBill.gst_type = Convert.ToInt64(gstTypeComboBox.SelectedValue);
            greyPurchaseBill.hsnCode = Convert.ToInt64(hsncTextBox.Text);
            greyPurchaseBill.igstAmount = Convert.ToDouble(igstAmtTextBox.Text);
            greyPurchaseBill.igstRate = Convert.ToDouble(igstPerTextBox.Text);
            greyPurchaseBill.interest = Convert.ToDouble(interestTextBox.Text);
            greyPurchaseBill.lessAny = Convert.ToDouble(lessAnyTextBox.Text);
            greyPurchaseBill.mtrs = Convert.ToDouble(recdMtrsTextBox.Text);
            greyPurchaseBill.netAmount = Convert.ToDouble(netAmtTextBox.Text);
            
            greyPurchaseBill.partyid = Convert.ToInt64(partyNameComboBox["PARTY_ID"].ToString());//Convert.ToInt64(partyNameComboBox.SelectedValue);
            greyPurchaseBill.quality = Convert.ToInt64(qualityComboBox.SelectedValue);
            greyPurchaseBill.rate = Convert.ToDouble(purRateTextBox.Text);
            greyPurchaseBill.remarks = remarksTextBox.Text;
            greyPurchaseBill.sgstAmount = Convert.ToDouble(sgstAmtTextBox.Text);
            greyPurchaseBill.sgstRate = Convert.ToDouble(sgstPerTextBox.Text);
            greyPurchaseBill.taka = Convert.ToInt64(recdTakaTextBox.Text);
            greyPurchaseBill.taxableValue = Convert.ToDouble(taxableValueTextBox.Text);
            greyPurchaseBill.transport_id = Convert.ToInt64(transportComboBox.SelectedValue);
            greyPurchaseBill.weight = Convert.ToDouble(wtTextBox.Text);
            
            return greyPurchaseBill;
        }
        private void setGreyPurchaseData(greypurchase_details greyPurchaseBill)
        {
            addAnyTextBox.Text = Convert.ToString(greyPurchaseBill.addAny);
            billNoTextBox.Text = greyPurchaseBill.billNo;
            greyPurchaseTypeComboBox.SelectedValue = Convert.ToInt64(greyPurchaseBill.greyPurchaseType_id);
            if (Convert.ToInt64(greyPurchaseBill.greyPurchaseType_id) == 2)
                refBillNoComboBox.Text = greyPurchaseBill.orderNo;
            brokerComboBox.SelectedValue = Convert.ToInt64(greyPurchaseBill.broker_id);
            cgstAmtTextBox.Text = Convert.ToString(greyPurchaseBill.cgstAmount);
            cgstPerTextBox.Text = Convert.ToString(greyPurchaseBill.cgstRate);
            checkerComboBox.SelectedValue = Convert.ToInt64(greyPurchaseBill.checker);
            cityComboBox.SelectedValue = Convert.ToInt64(greyPurchaseBill.city);
            DateTime dt, dt1;
            bool bFlag = DateTime.TryParse(greyPurchaseBill.date.ToString(), out dt);
            if (bFlag)
            {
                dt1 = dt.Date;
                //DateTime.Parse(dt1.ToString()).ToString("dd-MM-yyyy");
                dateTextBox.Text = dt1.ToString("dd-MM-yyyy");
            }

            discAmtTextBox.Text = Convert.ToString(greyPurchaseBill.discountAmount);
            disPerTextBox.Text = Convert.ToString(greyPurchaseBill.discountPercentage);
            dueDaysTextBox.Text = Convert.ToString(greyPurchaseBill.dueDays);
            foldLessTextBox.Text = Convert.ToString(greyPurchaseBill.foldLess);
            //greyPurchaseBill.greypurchase_id = Convert.ToInt64(srnoTextBox.Text);
            //greyPurchaseBill.greytaka_details = tdw.takaDetails;
            grossAmtTextBox.Text = Convert.ToString(greyPurchaseBill.grossAmount);
            gstnTextBox.Text = greyPurchaseBill.gstn;
            gstTypeComboBox.SelectedValue = Convert.ToInt64(greyPurchaseBill.gst_type);
            hsncTextBox.Text = Convert.ToString(greyPurchaseBill.hsnCode);
            igstAmtTextBox.Text = Convert.ToString(greyPurchaseBill.igstAmount);
            igstPerTextBox.Text = Convert.ToString(greyPurchaseBill.igstRate);
            interestTextBox.Text = Convert.ToString(greyPurchaseBill.interest);
            lessAnyTextBox.Text = Convert.ToString(greyPurchaseBill.lessAny);
            recdMtrsTextBox.Text = Convert.ToString(greyPurchaseBill.mtrs);
            netAmtTextBox.Text = Convert.ToString(greyPurchaseBill.netAmount);
            orderTextBox.Text = Convert.ToString(greyPurchaseBill.orderNo);
            setPartyComboBoxSelectedValue(Convert.ToInt64(greyPurchaseBill.partyid));
            //partyNameComboBox.SelectedValue = Convert.ToInt64(greyPurchaseBill.partyid);
            qualityComboBox.SelectedValue = Convert.ToInt64(greyPurchaseBill.quality);
            purRateTextBox.Text = Convert.ToString(greyPurchaseBill.rate);
            remarksTextBox.Text = greyPurchaseBill.remarks;
            sgstAmtTextBox.Text = Convert.ToString(greyPurchaseBill.sgstAmount);
            sgstPerTextBox.Text = Convert.ToString(greyPurchaseBill.sgstRate);
            recdTakaTextBox.Text = Convert.ToString(greyPurchaseBill.taka);
            taxableValueTextBox.Text = Convert.ToString(greyPurchaseBill.taxableValue);
            transportComboBox.SelectedValue = Convert.ToInt64(greyPurchaseBill.transport_id);
            wtTextBox.Text = Convert.ToString(greyPurchaseBill.weight);

        }

        private void resetAllData()
        {
            billNoTextBox.Text = "";
            orderTextBox.Text = "0";
            wtTextBox.Text = "0.0";
            hsncTextBox.Text = "";
            lrnoTextBox.Text = "";
            recdMtrsTextBox.Text = "0.0";
            recdTakaTextBox.Text = "0";
            purRateTextBox.Text = "0.0";
            grossAmtTextBox.Text = "0";
            discAmtTextBox.Text = "0";
            disPerTextBox.Text = "0";
            foldLessTextBox.Text = "0";
            addAnyTextBox.Text = "0";
            lessAnyTextBox.Text = "0";
            remarksTextBox.Text = "";
            dueDaysTextBox.Text = "0";
            interestTextBox.Text = "0.0";
            refBillNoComboBox.Text = "";
        }

        private void srnoTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                long lSrNo = 0;
                bool bValid = long.TryParse(srnoTextBox.Text,out lSrNo);
                if(!bValid || lSrNo <= 0)
                {
                    MessageBox.Show("Invalid  SrNo.","Invalid Input",MessageBoxButtons.OK);
                    srnoTextBox.Focus();
                    return;
                }
                bValid = partyDB.checkGreyPurchaseSrnoExists(lSrNo,Convert.ToInt64(greyPurchaseTypeComboBox.SelectedValue));
                if(!bValid)
                {
                    DialogResult result = MessageBox.Show("Srno. does not exist.Do you want to add new voucher?", "Invalid Voucher", MessageBoxButtons.YesNo);
                    if(result == DialogResult.Yes)
                    {
                        resetAllData();
                        setDafaultData();
                        srnoTextBox.Focus();
                        return;
                    }
                    else
                    {
                        srnoTextBox.Focus();
                        return;
                    }
                }
                try
                {
                    var db = DBHelper2.GetDbContext(Properties.Settings.Default.FinancialYearDbName);
                    long lGreyPurchaseType = Convert.ToInt64(greyPurchaseTypeComboBox.SelectedValue);
                    greyPurchaseDetailForFetch = db.greypurchase_details.Where(x => x.greypurchase_id == lSrNo).Where(x=>x.greyPurchaseType_id == lGreyPurchaseType).SingleOrDefault();
                    if(greyPurchaseDetailForFetch == null)
                    {
                        MessageBox.Show("Unable to fetch Grey Purchase Data", "Invalid Input", MessageBoxButtons.OK);
                        return;
                    }
                    tdw.takaDetails = greyPurchaseDetailForFetch.greytaka_details.ToList();
                    
                    db.Dispose();
                }
                catch(Exception ex)
                {
                    return;
                }
                setGreyPurchaseData(greyPurchaseDetailForFetch);
                billNoTextBox.Focus();
                billNoTextBox.SelectAll();
                addButton.Enabled = false;
                updateButton.Enabled = true;
                deleteButton.Enabled = true;

            }
        }

        private void viewButton_Click(object sender, EventArgs e)
        {
            long lSrNo = 0;
            bool bValid = long.TryParse(srnoTextBox.Text, out lSrNo);
            if (!bValid || lSrNo <= 0)
            {
                MessageBox.Show("Invalid  SrNo.", "Invalid Input", MessageBoxButtons.OK);
                srnoTextBox.Focus();
                return;
            }
            bValid = partyDB.checkGreyPurchaseSrnoExists(lSrNo, Convert.ToInt64(greyPurchaseTypeComboBox.SelectedValue));
            if (!bValid)
            {
                DialogResult result = MessageBox.Show("Srno. does not exist.Do you want to add new voucher?", "Invalid Voucher", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    resetAllData();
                    setDafaultData();
                    srnoTextBox.Focus();
                    return;
                }
                else
                {
                    srnoTextBox.Focus();
                    return;
                }
            }
            try
            {
                var db = DBHelper2.GetDbContext(Properties.Settings.Default.FinancialYearDbName);
                long lGreyPurchaseType = Convert.ToInt64(greyPurchaseTypeComboBox.SelectedValue);
                greyPurchaseDetailForFetch = db.greypurchase_details.Where(x => x.greypurchase_id == lSrNo).Where(x=>x.greyPurchaseType_id == lGreyPurchaseType).SingleOrDefault();
                if (greyPurchaseDetailForFetch == null)
                {
                    MessageBox.Show("Unable to fetch Grey Purchase Data", "Invalid Input", MessageBoxButtons.OK);
                    return;
                }
                tdw.takaDetails = greyPurchaseDetailForFetch.greytaka_details.ToList();

                db.Dispose();
            }
            catch (Exception ex)
            {
                return;
            }
            setGreyPurchaseData(greyPurchaseDetailForFetch);
            billNoTextBox.Focus();
            billNoTextBox.SelectAll();
            addButton.Enabled = false;
            updateButton.Enabled = true;
            deleteButton.Enabled = true;
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            long lSrNo = 0;
            bool bFlag = long.TryParse(srnoTextBox.Text, out lSrNo);
            if(!bFlag)
            {
                MessageBox.Show("Please enter correct Voucher No", "DELETE VOUCHER", MessageBoxButtons.OK);
                srnoTextBox.Focus();
                return;
            }
            bFlag = partyDB.checkGreyPurchaseSrnoExists(lSrNo,Convert.ToInt64(greyPurchaseTypeComboBox.SelectedValue));
            if (!bFlag)
            {
                MessageBox.Show("Voucher No does not exist", "DELETE VOUCHER", MessageBoxButtons.OK);
                srnoTextBox.Focus();
                return;
            }
            bFlag = partyDB.deleteGreyPurchaseBill(lSrNo, Convert.ToInt64(greyPurchaseTypeComboBox.SelectedValue));
            if(bFlag)
            {
                MessageBox.Show("Voucher deleted successfully", "DELETE", MessageBoxButtons.OK);
                srnoTextBox.Focus();
                resetAllData();
                setDafaultData();
            }

        }

        private void cityComboBox_Validating(object sender, CancelEventArgs e)
        {
            if (cityComboBox.SelectedIndex == -1)
            {
                MessageBox.Show("Please select city name", "QUALITY", MessageBoxButtons.OK);
                cityComboBox.Focus();
                return;
            }
        }

        private void qualityComboBox_Validating(object sender, CancelEventArgs e)
        {
            if(qualityComboBox.SelectedIndex == -1)
            {
                MessageBox.Show("Please select quality name", "QUALITY", MessageBoxButtons.OK);
                qualityComboBox.Focus();
                return;
            }
        }

        private void brokerComboBox_Validating(object sender, CancelEventArgs e)
        {
            if (brokerComboBox.SelectedIndex == -1)
            {
                MessageBox.Show("Please select broker name", "QUALITY", MessageBoxButtons.OK);
                brokerComboBox.Focus();
                return;
            }
        }

        private void updateButton_Click(object sender, EventArgs e)
        {
            bool bBillExists = partyDB.checkGreyPurchaseBillExists(Convert.ToInt64(srnoTextBox.Text), billNoTextBox.Text, Convert.ToInt64(partyNameComboBox["PARTY_ID"].ToString()));
            if (bBillExists)
            {
                MessageBox.Show("Bill of party with same bill no. already exists", "ALERT", MessageBoxButtons.OK);
                return;
            }
            if(greyPurchaseDetailForFetch.greypurchase_id != Convert.ToInt64(srnoTextBox.Text))
            {
                MessageBox.Show("Changging voucher no. is not allowed", "ALERT", MessageBoxButtons.OK);
                return;
            }
            double dNetAmount = 0;
            bool bFlag = double.TryParse(netAmtTextBox.Text, out dNetAmount);
            if (!bFlag || dNetAmount <= 0)
            {
                MessageBox.Show("Invalid Bill", "ALERT", MessageBoxButtons.OK);
                return;
            }

            greyPurchaseDetailForFetch = getGreyPurchaseDataForUpdate(greyPurchaseDetailForFetch);
            greyPurchaseDetailForFetch.greypurchase_id = Convert.ToInt64(srnoTextBox.Text);
            bFlag = setGreyTakaDetailsForUpdate(greyPurchaseDetailForFetch.greytaka_details.ToList());
            
            //bFlag = partyDB.updateGreyPurchaseBill(greyPurchaseDetailForFetch);
            if (bFlag)
            {
                MessageBox.Show("Bill updated Successfully", "Grey Bill", MessageBoxButtons.OK);
                resetAllData();
                setDafaultData();
            }
            else
                MessageBox.Show("Bill not updated Successfully", "Grey Bill", MessageBoxButtons.OK);
            srnoTextBox.Focus();
        }

        private greypurchase_details getGreyPurchaseDataForUpdate(greypurchase_details greyPurchaseBill)
        {
            greyPurchaseBill.addAny = Convert.ToDouble(addAnyTextBox.Text);
            greyPurchaseBill.billNo = billNoTextBox.Text;
            greyPurchaseBill.broker_id = Convert.ToInt64(brokerComboBox.SelectedValue);
            greyPurchaseBill.greyPurchaseType_id = Convert.ToInt64(greyPurchaseTypeComboBox.SelectedValue);
            greyPurchaseBill.cgstAmount = Convert.ToDouble(cgstAmtTextBox.Text);
            greyPurchaseBill.cgstRate = Convert.ToDouble(cgstPerTextBox.Text);
            greyPurchaseBill.checker = Convert.ToInt64(checkerComboBox.SelectedValue);
            greyPurchaseBill.city = Convert.ToInt64(cityComboBox.SelectedValue);
            greyPurchaseBill.date = DateTime.ParseExact(dateTextBox.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            greyPurchaseBill.discountAmount = Convert.ToDouble(discAmtTextBox.Text);
            greyPurchaseBill.discountPercentage = Convert.ToDouble(disPerTextBox.Text);
            greyPurchaseBill.dueDays = Convert.ToInt64(dueDaysTextBox.Text);
            greyPurchaseBill.foldLess = Convert.ToDouble(foldLessTextBox.Text);
            greyPurchaseBill.greypurchase_id = Convert.ToInt64(srnoTextBox.Text);
            //greyPurchaseBill.greytaka_details = tdw.takaDetails;
            greyPurchaseBill.grossAmount = Convert.ToDouble(grossAmtTextBox.Text);
            greyPurchaseBill.gstn = gstnTextBox.Text;
            greyPurchaseBill.gst_type = Convert.ToInt64(gstTypeComboBox.SelectedValue);
            greyPurchaseBill.hsnCode = Convert.ToInt64(hsncTextBox.Text);
            greyPurchaseBill.igstAmount = Convert.ToDouble(igstAmtTextBox.Text);
            greyPurchaseBill.igstRate = Convert.ToDouble(igstPerTextBox.Text);
            greyPurchaseBill.interest = Convert.ToDouble(interestTextBox.Text);
            greyPurchaseBill.lessAny = Convert.ToDouble(lessAnyTextBox.Text);
            greyPurchaseBill.mtrs = Convert.ToDouble(recdMtrsTextBox.Text);
            greyPurchaseBill.netAmount = Convert.ToDouble(netAmtTextBox.Text);
            greyPurchaseBill.orderNo = orderTextBox.Text;
            greyPurchaseBill.partyid = Convert.ToInt64(partyNameComboBox["PARTY_ID"].ToString());
            greyPurchaseBill.quality = Convert.ToInt64(qualityComboBox.SelectedValue);
            greyPurchaseBill.rate = Convert.ToDouble(purRateTextBox.Text);
            greyPurchaseBill.remarks = remarksTextBox.Text;
            greyPurchaseBill.sgstAmount = Convert.ToDouble(sgstAmtTextBox.Text);
            greyPurchaseBill.sgstRate = Convert.ToDouble(sgstPerTextBox.Text);
            greyPurchaseBill.taka = Convert.ToInt64(recdTakaTextBox.Text);
            greyPurchaseBill.taxableValue = Convert.ToDouble(taxableValueTextBox.Text);
            greyPurchaseBill.transport_id = Convert.ToInt64(transportComboBox.SelectedValue);
            greyPurchaseBill.weight = Convert.ToDouble(wtTextBox.Text);

            return greyPurchaseBill;
        }

        private bool setGreyTakaDetailsForUpdate(List<greytaka_details> greyTakaDetails)
        {
            /*
            List<greytaka_details> tempTakaDetails = new List<greytaka_details>();
            for(int i = 0;i < greyTakaDetails.Count;i++)
            {
                greytaka_details newTakaDetail = new greytaka_details();
                newTakaDetail.takadetails_id = greyTakaDetails[i].takadetails_id;
                newTakaDetail.greypurchase_id = greyTakaDetails[i].greypurchase_id;
                newTakaDetail.takaMtrs = greyTakaDetails[i].takaMtrs;
                newTakaDetail.takaNo = greyTakaDetails[i].takaNo;
                newTakaDetail.takaWeight = greyTakaDetails[i].takaWeight;
                tempTakaDetails.Add(newTakaDetail);
            }
            greyTakaDetails.Clear();
            for(int i = 0;i < tempTakaDetails.Count;i++)
            {
                greytaka_details newTakaDetail = new greytaka_details();
                newTakaDetail.takadetails_id = tempTakaDetails[i].takadetails_id;
                newTakaDetail.greypurchase_id = tempTakaDetails[i].greypurchase_id;
                newTakaDetail.takaMtrs = tempTakaDetails[i].takaMtrs;
                newTakaDetail.takaNo = tempTakaDetails[i].takaNo;
                newTakaDetail.takaWeight = tempTakaDetails[i].takaWeight;
                greyTakaDetails.Add(newTakaDetail);
            }
            for (int i = 0;i<tdw.takaDetails.Count;i++)
            {
                //if(i>= greyTakaDetails.Count)
                {
                    greytaka_details newTakaDetail = new greytaka_details();
                    newTakaDetail.greypurchase_id = tdw.takaDetails[i].greypurchase_id;
                    newTakaDetail.takaMtrs = tdw.takaDetails[i].takaMtrs;
                    newTakaDetail.takaNo = tdw.takaDetails[i].takaNo;
                    newTakaDetail.takaWeight = tdw.takaDetails[i].takaWeight;
                    greyTakaDetails.Add(newTakaDetail);

                }*/
            /*else
            {

                greyTakaDetails[i].greypurchase_id = tdw.takaDetails[i].greypurchase_id;
                greyTakaDetails[i].takaMtrs = tdw.takaDetails[i].takaMtrs;
                greyTakaDetails[i].takaNo = tdw.takaDetails[i].takaNo;
                greyTakaDetails[i].takaWeight = tdw.takaDetails[i].takaWeight;
            }*/


            try
            {
                var db = DBHelper2.GetDbContext(Properties.Settings.Default.FinancialYearDbName);
                if (tdw.takaDetails.Count <= greyTakaDetails.Count)
                {
                    for (int i = 0; i < (greyTakaDetails.Count - tdw.takaDetails.Count); i++)
                    {
                        long lTakaId = greyTakaDetails[i].takadetails_id;
                        greytaka_details deleteTakaDetail = db.greytaka_details.Where(x => x.takadetails_id == lTakaId).SingleOrDefault();
                        if(deleteTakaDetail == null)
                        {
                            MessageBox.Show("Unable to data Update", "Database Error", MessageBoxButtons.OK);
                            return false;
                        }
                        db.greytaka_details.Remove(deleteTakaDetail);
                        //bool bFlag = partyDB.deleteGreyPurchaseTakaDetail(greyTakaDetails[i].takadetails_id);
                        greyTakaDetails.RemoveAt(i);
                    }
                }

                for (int i = 0; i < tdw.takaDetails.Count; i++)
                {
                    if (i >= greyTakaDetails.Count)
                    {
                        greytaka_details newTakaDetail = new greytaka_details();
                        //newTakaDetail.greypurchase_id = tdw.takaDetails[i].greypurchase_id;
                        newTakaDetail.takaMtrs = tdw.takaDetails[i].takaMtrs;
                        newTakaDetail.takaNo = tdw.takaDetails[i].takaNo;
                        newTakaDetail.takaWeight = tdw.takaDetails[i].takaWeight;
                        greyTakaDetails.Add(newTakaDetail);

                    }
                    else
                    {

                        //greyTakaDetails[i].greypurchase_id = tdw.takaDetails[i].greypurchase_id;
                        greyTakaDetails[i].takaMtrs = tdw.takaDetails[i].takaMtrs;
                        greyTakaDetails[i].takaNo = tdw.takaDetails[i].takaNo;
                        greyTakaDetails[i].takaWeight = tdw.takaDetails[i].takaWeight;
                    }

                }
                greyPurchaseDetailForFetch.greytaka_details = greyTakaDetails;
                db.greypurchase_details.Add(greyPurchaseDetailForFetch);
                //db.greypurchase_details.Attach(purchaseBill);

                db.Entry(greyPurchaseDetailForFetch).State = EntityState.Modified;
                foreach (greytaka_details sd in greyPurchaseDetailForFetch.greytaka_details)
                {
                    db.Entry(sd).State = sd.takadetails_id == 0 ? EntityState.Added : EntityState.Modified;
                    //db.Entry(sd).State = EntityState.Modified;
                }
                db.SaveChanges();
                db.Dispose();
            }  
            catch(Exception ex)
            {
                return false;
            }
                
            return true;
        }

        private void setPartyComboBoxSelectedValue(long lPartyID)
        {
            try
            {
                //partyComboBox.SelectedIndex = -1;
                if (lPartyID >= 0)
                {
                    for (int index = 0; index < partyNameComboBox.Items.Count; index++)
                    {
                        DataTable partyTable = partyNameComboBox.Data;
                        DataRow Row = partyTable.Rows[index];
                        if (Row != null)
                        {
                            if (Convert.ToInt64(Row["PARTY_ID"].ToString()) == lPartyID)
                            {
                                partyNameComboBox.SelectedIndex = -1;
                                partyNameComboBox.SelectedIndex = index;
                                break;
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                return;
            }
        }

        private void greyPurchaseTypeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if((long)greyPurchaseTypeComboBox.SelectedValue == 2)
            {
                label36.Text = "Ref Bill No.";
                dueDaysTextBox.Visible = false;
                interestTextBox.Visible = false;
                label37.Visible = false;
                label6.Visible = false;
                orderTextBox.Visible = false;
                refBillNoComboBox.Visible = true;
                resetAllData();
                setDafaultData();
                fillRefBillData(Convert.ToInt64(partyNameComboBox["PARTY_ID"].ToString()));
            }
            else if ((long)greyPurchaseTypeComboBox.SelectedValue == 1)
            {
                label36.Text = "DUE DAYS:";
                dueDaysTextBox.Visible = true;
                interestTextBox.Visible = true;
                label37.Visible = true;
                label6.Visible = true;
                orderTextBox.Visible = true;
                refBillNoComboBox.Visible = false;
                resetAllData();
                setDafaultData();
            }
        }

        private void fillRefBillData(long lPartyID)
        {
            List<greypurchase_details> purchaseBillList = partyDB.getGreyPurchaseBillDetailFromPartyID(lPartyID);
            
            List<PassBookBillDataForMultiColumnDisplay> passBookMultiColunDisplayDataList = new List<PassBookBillDataForMultiColumnDisplay>();
            if (purchaseBillList == null)
                return;
            
            for (int i = 0; i < purchaseBillList.Count; i++)
            {
                PassBookBillDataForMultiColumnDisplay multiColumnDisplayBillData = new PassBookBillDataForMultiColumnDisplay();
                multiColumnDisplayBillData.billNo = purchaseBillList[i].billNo;
                //multiColumnDisplayBillData.billType = Convert.ToInt64(saleBillList[i].salesTypeid);
                DateTime dt, dt1;
                string billDate = "";
                bool bFlag = DateTime.TryParse(purchaseBillList[i].date.ToString(), out dt);
                if (bFlag)
                {
                    dt1 = dt.Date;
                    //DateTime.Parse(dt1.ToString()).ToString("dd-MM-yyyy");
                    billDate = dt1.ToString("dd-MM-yyyy");
                }
                multiColumnDisplayBillData.date = billDate;
                multiColumnDisplayBillData.netAmount = Convert.ToDouble(purchaseBillList[i].netAmount);
                multiColumnDisplayBillData.taxableValue = Convert.ToInt64(purchaseBillList[i].taxableValue);
                List<passbook_itemdetails> billReceivedList = partyDB.getReceivedBillAmount(lPartyID, 2, Convert.ToString(purchaseBillList[i].billNo));
                if (billReceivedList == null || billReceivedList.Count == 0)
                    multiColumnDisplayBillData.pendingAmount = multiColumnDisplayBillData.netAmount;
                else
                {
                    double dReceivedAmount = 0;
                    for (int j = 0; j < billReceivedList.Count; j++)
                    {
                        dReceivedAmount = dReceivedAmount + Convert.ToDouble(billReceivedList[j].discountAmount + billReceivedList[j].others + billReceivedList[j].paymentReceivedAmount + billReceivedList[j].rdAmount + billReceivedList[j].rgAmount + billReceivedList[j].tdsAmount + billReceivedList[j].addLess);
                    }
                    multiColumnDisplayBillData.pendingAmount = multiColumnDisplayBillData.netAmount - dReceivedAmount;
                }
                List<greypurchase_details> rgReceivedList = partyDB.getGreyPurchasePurchaseRGAmount(Convert.ToString(purchaseBillList[i].billNo), lPartyID);
                if (rgReceivedList != null)
                {
                    for (int j = 0; j < rgReceivedList.Count; j++)
                    {
                        multiColumnDisplayBillData.pendingAmount = multiColumnDisplayBillData.pendingAmount - Convert.ToDouble(rgReceivedList[j].netAmount);
                    }
                }

                if (multiColumnDisplayBillData.pendingAmount > 0)
                    passBookMultiColunDisplayDataList.Add(multiColumnDisplayBillData);
            }

            int iColumnCount = 4;


            DataTable myDataTable = new DataTable("ParentTable");
            // Declare variables for DataColumn and DataRow objects.
            DataColumn myDataColumn;
            DataRow myDataRow;

            //Add some coulumns
            for (int index = 0; index < iColumnCount; index++)
            {
                myDataColumn = new DataColumn();
                myDataColumn.DataType = typeof(string);
                if (index == 0)
                    myDataColumn.ColumnName = "DATE";
                else if (index == 1)
                    myDataColumn.ColumnName = "BILLNO";
                else if (index == 2)
                    myDataColumn.ColumnName = "NET AMOUNT";
                else if (index == 3)
                    myDataColumn.ColumnName = "PENDING AMOUNT";

                //myDataColumn.Caption = "ParentItem";
                myDataColumn.AutoIncrement = false;
                myDataColumn.ReadOnly = false;
                myDataColumn.Unique = false;
                // Add the Column to the DataColumnCollection.
                myDataTable.Columns.Add(myDataColumn);
            }


            //Add some more rows
            for (int index = 0; index < passBookMultiColunDisplayDataList.Count; index++)
            {
                myDataRow = myDataTable.NewRow();
                myDataRow[0] = passBookMultiColunDisplayDataList[index].date;
                myDataRow[1] = passBookMultiColunDisplayDataList[index].billNo;
                myDataRow[2] = passBookMultiColunDisplayDataList[index].netAmount;
                myDataRow[3] = passBookMultiColunDisplayDataList[index].pendingAmount;

                myDataTable.Rows.Add(myDataRow);
            }


            //Now set the Data of the ColumnComboBox
            refBillNoComboBox.DisplayMember = "BILLNO";
            refBillNoComboBox.ValueMember = "BILLNO";
            refBillNoComboBox.Data = myDataTable;
            //Set which row will be displayed in the text box
            //If you set this to a column that isn't displayed then the suggesting functionality won't work.
            refBillNoComboBox.ColumnSpacing = 50;
            refBillNoComboBox.ViewColumn = 1;

            refBillNoComboBox.Columns[0].Width = 50;
            refBillNoComboBox.Columns[1].Width = 50;
            refBillNoComboBox.Columns[2].Width = 80;
            refBillNoComboBox.Columns[3].Width = 80;
            //Set a few columns to not be shown
            refBillNoComboBox.SelectedIndex = -1;

        }
    }
}
