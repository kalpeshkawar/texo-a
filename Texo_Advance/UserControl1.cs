﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Entity;
using System.Globalization;
using Texo_Advance.DBItems;
using Texo_Advance.CustomControl;

namespace Texo_Advance
{
    public partial class PurchaseWindow : UserControl
    {
        private string companyName = "";
        private string companyGstn = "";
        MasterDBData DB = new MasterDBData();
        PartyDBData partyDB = new PartyDBData();
        purchase_details purchaseBillDetails;
        public PurchaseWindow(string name, string gstn)
        {
            companyName = name;
            companyGstn = gstn;
            //companyGstn = "08AKPPO7958A1Z5";
            InitializeComponent();
            //partyComboBox.DataSource = 
            //purchaseDataGridView.CurrentCellDirtyStateChanged += purchaseDataGridView_CurrentCellDirtyStateChanged;
        }

        private void fillPurchaseTypeList()
        {
            var typeOfPurchaseList = DB.getPurchaseTypeList();

            purchaseTypeComboBox.DisplayMember = "type";
            purchaseTypeComboBox.ValueMember = "purchasetype_id";
            purchaseTypeComboBox.DataSource = typeOfPurchaseList;
        }
        public void setDafaultData()
        {
            
            long maxVoucherNo = partyDB.getLastPurchaseBillNo(Convert.ToInt64(purchaseTypeComboBox.SelectedValue));
            if(maxVoucherNo == -1)
            {
                MessageBox.Show("Not able to retrieve new Bill No.","Error",MessageBoxButtons.OK);
                return;
            }
            if (maxVoucherNo != 0)
            {
                vouvherNoTextBox.Text = Convert.ToString(maxVoucherNo + 1);
                    
            }
            else
            {
                vouvherNoTextBox.Text = "1";
                    
            }
            dateTextBox.Text = DateTime.Today.ToString("dd-MM-yyyy");
           
            billNoTextBox.Text = "";
            
            button1.Enabled = false;
            deleteButton.Enabled = false;
            addNewButton.Enabled = true;
        }

        private void updateItemList()
        {
            var itemList = DB.getItemList();
            if (itemList != null && itemList.Count > 0)
            {
                itemNameGridCombo.DisplayMember = "itemname";
                itemNameGridCombo.ValueMember = "itemid";
                itemNameGridCombo.DataSource = itemList;
            }
        }

        private void PurchaseWindow_Load(object sender, EventArgs e)
        {
            comNameTextBox.Text = companyName;
            fillPurchaseTypeList();
            updateItemList();
            UpdatePartyDetails();
        }

        private void UpdatePartyDetails()
        {
            
            var partyList = DB.getPartyDetails(2);
            /*if (partyList != null)
            {
                partyComboBox.DisplayMember = "party_name";
                partyComboBox.ValueMember = "partyid";
                partyComboBox.DataSource = partyList;
            }*/
            int iColumnCount = 5;


            DataTable myDataTable = new DataTable("ParentTable");
            // Declare variables for DataColumn and DataRow objects.
            DataColumn myDataColumn;
            DataRow myDataRow;

            //Add some coulumns
            for (int index = 0; index < iColumnCount; index++)
            {
                myDataColumn = new DataColumn();
                myDataColumn.DataType = typeof(string);
                if (index == 0)
                    myDataColumn.ColumnName = "NAME";
                else if (index == 1)
                    myDataColumn.ColumnName = "BROKER";
                else if (index == 2)
                    myDataColumn.ColumnName = "ADDRESS";
                else if (index == 3)
                    myDataColumn.ColumnName = "CITY";
                else if (index == 4)
                    myDataColumn.ColumnName = "PARTY_ID";
                //myDataColumn.Caption = "ParentItem";
                myDataColumn.AutoIncrement = false;
                myDataColumn.ReadOnly = false;
                myDataColumn.Unique = false;
                // Add the Column to the DataColumnCollection.
                myDataTable.Columns.Add(myDataColumn);
            }


            //Add some more rows
            for (int index = 0; index < partyList.Count; index++)
            {
                myDataRow = myDataTable.NewRow();
                myDataRow[0] = partyList[index].party_name;
                myDataRow[1] = DB.getBrokerNameFromID(Convert.ToInt64(partyList[index].brokerid));
                myDataRow[2] = partyList[index].address + " " + partyList[index].address2;
                myDataRow[3] = DB.getCityName((long)partyList[index].city_id);
                myDataRow[4] = partyList[index].partyid;
                myDataTable.Rows.Add(myDataRow);
            }


            //Now set the Data of the ColumnComboBox
            partyComboBox.DisplayMember = "NAME";
            partyComboBox.ValueMember = "PARTY_ID";
            partyComboBox.Data = myDataTable;
            //Set which row will be displayed in the text box
            //If you set this to a column that isn't displayed then the suggesting functionality won't work.
            partyComboBox.ColumnSpacing = 50;
            partyComboBox.ViewColumn = 0;

            partyComboBox.Columns[0].Width = 150;
            partyComboBox.Columns[1].Width = 150;
            partyComboBox.Columns[2].Width = 250;
            partyComboBox.Columns[3].Width = 50;
            //Set a few columns to not be shown
            partyComboBox.Columns[4].Display = false;
            partyComboBox.SelectedIndex = -1;
            partyComboBox.SelectedIndex = 0;
        }

        private void fillBrokerDetails()
        {
            var brokerList = DB.getBrokerDetails();
            if (brokerList != null)
            {
                brokerComboBox.DisplayMember = "party_name";
                brokerComboBox.ValueMember = "brokerid";
                brokerComboBox.DataSource = brokerList;

            }
        }
        private void fillTransportDetails()
        {
            var transportList = DB.getTransportDetails();
            if (transportList != null)
            {
                transportComboBox.DisplayMember = "name";
                transportComboBox.ValueMember = "transportid";
                transportComboBox.DataSource = transportList;
            }
        }
        private void fillStateCodeList()
        {
            var stateCodeList = DB.getStateCodeList();
            if (stateCodeList != null)
            {
                stateComboBox.DisplayMember = "code";
                stateComboBox.ValueMember = "statecode_id";
                stateComboBox.DataSource = stateCodeList;
            }
        }
        private void fillGstTypeList()
        {
            gstTypeComboBox.SelectedIndex = -1;
            var gstTypeList = DB.getGstTypeList();
            if (gstTypeList != null)
            {
                gstTypeComboBox.DisplayMember = "type";
                gstTypeComboBox.ValueMember = "gsttype_id";
                gstTypeComboBox.DataSource = gstTypeList;
            }
        }
        private void fillCityList()
        {
            var cityList = DB.getCityList();
            if (cityList != null)
            {
                stationComboBox.DisplayMember = "name";
                stationComboBox.ValueMember = "city_id";
                stationComboBox.DataSource = cityList;
            }
        }

        private void partyComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            fetchPartyDataFromPartyName();
            if ((long)purchaseTypeComboBox.SelectedValue == 6 || (long)purchaseTypeComboBox.SelectedValue == 7 || (long)purchaseTypeComboBox.SelectedValue == 8 || (long)purchaseTypeComboBox.SelectedValue == 9 || (long)purchaseTypeComboBox.SelectedValue == 10)
            {
                if(partyComboBox.SelectedIndex != -1)
                {
                    fillReferenceBillDetailsForReturn(Convert.ToInt64(partyComboBox["PARTY_ID"].ToString()));
                }
                    
            }
                
                    //UpdatePartyDetails();
        }

        private void fetchPartyDataFromPartyName()
        {
            if (partyComboBox.SelectedIndex != -1)
            {
                fillBrokerDetails();
                fillTransportDetails();
                fillStateCodeList();
                fillGstTypeList();
                fillCityList();
                using (var db = new Entities())
                {
                    //string value = partyComboBox.di;
                    //party_details partyDetail2 = ((party_details)partyComboBox.SelectedItem);
                    if (partyComboBox.Items.Count == 0)
                        return;
                    string partyID = partyComboBox["PARTY_ID"].ToString();
                    
                    var partyDetail = DB.getPartyDetailFromID(2,Convert.ToInt64(partyID));
                    if (partyDetail == null)
                        return;
                    //var partyDetail = party_Detail.Find(x=> x.party_name == strPartyName);
                    
                    transportComboBox.SelectedValue = partyDetail.transportid;
                    brokerComboBox.SelectedValue = partyDetail.brokerid;
                    stationComboBox.SelectedValue = partyDetail.city_id;
                    gstnTextBox.Text = partyDetail.gst;
                    string gstn = gstnTextBox.Text;
                    if (gstn.Length > 0)
                    {
                        string stateCode = gstn.Substring(0, 2);
                        int indexForStateCode = stateComboBox.FindString(stateCode);
                        if (indexForStateCode != -1)
                            stateComboBox.SelectedIndex = indexForStateCode;

                    }


                }
            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            AccountManagerForm acc = new AccountManagerForm();
            acc.setDefaultAccountType(2);
            acc.ShowDialog();
            UpdatePartyDetails();
            
        }

        private void purchaseDataGridView_CellValidated(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == amountTextBoxGrid.Index && e.RowIndex == (purchaseDataGridView.RowCount - 2) && ((long)purchaseTypeComboBox.SelectedValue == 1 || (long)purchaseTypeComboBox.SelectedValue == 6))
            {
                DialogResult result = MessageBox.Show("Do You want to add new item?", "Purchase", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    purchaseDataGridView.CausesValidation = false;
                    //bFlagNewRow = true;
                    return;
                }
                else
                {
                    purchaseDataGridView.CausesValidation = false;
                    //ClearSelection();
                    remarkTextBox.Focus();
                    //return;
                }

                //   saleDataGridView.Rows.Add();
            }
            else if(e.ColumnIndex == totalAmountTextBoxColumn.Index && e.RowIndex == (purchaseDataGridView.RowCount - 2) && (long)purchaseTypeComboBox.SelectedValue != 1 && (long)purchaseTypeComboBox.SelectedValue != 6)
            {
                DialogResult result = MessageBox.Show("Do You want to add new item?", "Purchase", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    purchaseDataGridView.CausesValidation = false;
                    //bFlagNewRow = true;
                    return;
                }
                else
                {
                    purchaseDataGridView.CausesValidation = false;
                    //ClearSelection();
                    remarkTextBox.Focus();
                    //return;
                }
            }
        }

        private void itemLinkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            QualityInformationManagerForm qmf = new QualityInformationManagerForm();
            qmf.setMainScreenAddMode(true);
            qmf.ShowDialog();
            updateItemList();
        }

        private void purchaseDataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == amountTextBoxGrid.Index && ((long)purchaseTypeComboBox.SelectedIndex == 0 || (long)purchaseTypeComboBox.SelectedIndex == 5))
            {
                if (purchaseDataGridView.RowCount > 0)
                {
                    double dTotalAmount = 0;
                    for (int i = 0; i < (purchaseDataGridView.RowCount - 1); i++)
                    {
                        string strAmount = purchaseDataGridView.Rows[i].Cells[amountTextBoxGrid.Index].Value.ToString();
                        if (strAmount != "")
                            dTotalAmount = dTotalAmount + Convert.ToDouble(strAmount);
                        else
                            dTotalAmount = dTotalAmount + 0;
                    }
                    totalAmountTextBox.Text = Convert.ToString(dTotalAmount);
                }
            }
            else if (e.ColumnIndex == pcsTextBoxGrid.Index)
            {
                if (purchaseDataGridView.RowCount > 0)
                {
                    double dTotalPcs = 0;
                    for (int i = 0; i < (purchaseDataGridView.RowCount - 1); i++)
                    {
                        string strPcs = purchaseDataGridView.Rows[i].Cells[pcsTextBoxGrid.Index].Value.ToString();
                        if (strPcs != "")
                            dTotalPcs = dTotalPcs + Convert.ToDouble(strPcs);
                        else
                            dTotalPcs = dTotalPcs + 0;
                    }
                    totalPcsTextBox.Text = Convert.ToString(dTotalPcs);
                }
            }
            else if (e.ColumnIndex == qtyEditBoxGid.Index)
            {
                if (purchaseDataGridView.RowCount > 0)
                {
                    double dTotalMtrs = 0;
                    for (int i = 0; i < (purchaseDataGridView.RowCount - 1); i++)
                    {
                        string strMtrs = purchaseDataGridView.Rows[i].Cells[qtyEditBoxGid.Index].Value.ToString();
                        if (strMtrs != "")
                            dTotalMtrs = dTotalMtrs + Convert.ToDouble(strMtrs);
                        else
                            dTotalMtrs = dTotalMtrs + 0;
                    }
                    totalQtyTextBox.Text = Convert.ToString(dTotalMtrs);
                }
            }
            else if (e.ColumnIndex == totalAmountTextBoxColumn.Index && (long)purchaseTypeComboBox.SelectedIndex != 0 && (long)purchaseTypeComboBox.SelectedIndex != 5)
            {
                if (purchaseDataGridView.RowCount > 0)
                {
                    double dTotalAmount = 0;
                    for (int i = 0; i < (purchaseDataGridView.RowCount - 1); i++)
                    {
                        string strAmount = purchaseDataGridView.Rows[i].Cells[totalAmountTextBoxColumn.Index].Value.ToString();
                        if (strAmount != "")
                            dTotalAmount = dTotalAmount + Convert.ToDouble(strAmount);
                        else
                            dTotalAmount = dTotalAmount + 0;
                    }
                    billAmtTextBox.Text = Convert.ToString(dTotalAmount);
                }
            }

        }

        private void purchaseDataGridView_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (e.RowIndex != (purchaseDataGridView.RowCount - 1) || e.RowIndex == 0)
            {
                if (e.ColumnIndex == itemNameGridCombo.Index)
                {
                    if(itemNameGridCombo.Items.Count>0)
                    {
                        if (e.FormattedValue.ToString().Length == 0)
                        {
                            MessageBox.Show("Please select the correct item", "Item Name", MessageBoxButtons.OK);
                            //saleDataGridView.CurrentCell = saleDataGridView[e.RowIndex, e.ColumnIndex];
                            purchaseDataGridView.CurrentCell.Selected = true;
                            e.Cancel = true;
                        }
                    }
                    

                }
                else if (e.ColumnIndex == hsncEditBoxGrid.Index)
                {
                    if (e.FormattedValue.ToString().Length == 0)
                    {
                        DialogResult res = MessageBox.Show("Please enter the HSNCode. Leaving it may affect GST Reports.Do You want to continue?", "HSN CODE", MessageBoxButtons.YesNo);
                        if (res == DialogResult.No)
                        {
                            //saleDataGridView.CurrentCell = saleDataGridView[e.RowIndex, e.ColumnIndex];
                            purchaseDataGridView.CurrentCell.Selected = true;
                            e.Cancel = true;
                        }
                    }
                }
                else if (e.ColumnIndex == pcsTextBoxGrid.Index)
                {
                    double dPcs = Convert.ToDouble(e.FormattedValue.ToString());
                    if (dPcs <= 0)
                    {
                        MessageBox.Show("Please enter correct PCS.", "PCS", MessageBoxButtons.OK);
                        //saleDataGridView.CurrentCell = saleDataGridView[e.RowIndex, e.ColumnIndex];
                        purchaseDataGridView.CurrentCell.Selected = true;
                        e.Cancel = true;
                    }
                    else
                    {
                        string str = purchaseDataGridView.Rows[e.RowIndex].Cells[unitComboGrid.Index].FormattedValue.ToString();
                        if (str == "PCS")
                        {
                            string strRate = purchaseDataGridView.Rows[e.RowIndex].Cells[rateTextBoxGrid.Index].FormattedValue.ToString();
                            if (strRate != "")
                            {
                                double dRate = Convert.ToDouble(strRate);
                                string strAmount = Convert.ToString(dPcs * dRate);
                                purchaseDataGridView.Rows[e.RowIndex].Cells[amountTextBoxGrid.Index].Value = strAmount;
                            }
                            if((long)purchaseTypeComboBox.SelectedValue != 1 && (long)purchaseTypeComboBox.SelectedValue != 6)
                            {
                                double dRDAmount = 0;
                                double.TryParse(purchaseDataGridView.Rows[e.RowIndex].Cells[rdAmountTextBoxColumn.Index].EditedFormattedValue.ToString(),out dRDAmount);
                                double dDiscountAmount = 0;
                                double.TryParse(purchaseDataGridView.Rows[e.RowIndex].Cells[discountAmountTextBoxColumn.Index].EditedFormattedValue.ToString(), out dDiscountAmount);
                                double dAddLess = 0;
                                double.TryParse(purchaseDataGridView.Rows[e.RowIndex].Cells[addLessTextBoxColumn.Index].EditedFormattedValue.ToString(), out dAddLess);
                                double dCgstPercentage = 0;
                                double.TryParse(purchaseDataGridView.Rows[e.RowIndex].Cells[cgstPerTextBoxColumn.Index].EditedFormattedValue.ToString(), out dCgstPercentage);
                                double dCgstAmount = 0;
                                //double.TryParse(purchaseDataGridView.Rows[e.RowIndex].Cells[cgstAmountTextBoxColumn.Index].EditedFormattedValue.ToString(), out dCgstAmount);
                                double dSgstPercentage = 0;
                                double.TryParse(purchaseDataGridView.Rows[e.RowIndex].Cells[sgstPerTextBoxColumn.Index].EditedFormattedValue.ToString(), out dSgstPercentage);
                                double dSgstAmount = 0;
                                //double.TryParse(purchaseDataGridView.Rows[e.RowIndex].Cells[sgstAmountTextBoxColumn.Index].EditedFormattedValue.ToString(), out dSgstAmount);
                                double dAmount = 0;
                                double.TryParse(purchaseDataGridView.Rows[e.RowIndex].Cells[amountTextBoxGrid.Index].EditedFormattedValue.ToString(), out dAmount);
                                double dTaxableValue = dAmount - dRDAmount - dDiscountAmount - dAddLess;
                                if((long)gstTypeComboBox.SelectedValue == 2)
                                {
                                    dCgstAmount = (dCgstPercentage / 100) * dTaxableValue;
                                    dSgstAmount = (dSgstPercentage / 100) * dTaxableValue;
                                    purchaseDataGridView.Rows[e.RowIndex].Cells[sgstAmountTextBoxColumn.Index].Value = Convert.ToString(dSgstAmount);
                                    purchaseDataGridView.Rows[e.RowIndex].Cells[cgstAmountTextBoxColumn.Index].Value = Convert.ToString(dCgstAmount);
                                    purchaseDataGridView.Rows[e.RowIndex].Cells[totalAmountTextBoxColumn.Index].Value = Convert.ToString(dTaxableValue + dSgstAmount + dCgstAmount);
                                }
                                else if ((long)gstTypeComboBox.SelectedValue == 1)
                                {
                                    double dIgstPercentage = 0;
                                    double.TryParse(purchaseDataGridView.Rows[e.RowIndex].Cells[igstPerTextBoxColumn.Index].EditedFormattedValue.ToString(), out dIgstPercentage);
                                    double dIgstAmount = (dIgstPercentage / 100) * dTaxableValue;
                                    purchaseDataGridView.Rows[e.RowIndex].Cells[igstAmountTextBoxColumn.Index].Value = Convert.ToString(dIgstAmount);
                                    purchaseDataGridView.Rows[e.RowIndex].Cells[totalAmountTextBoxColumn.Index].Value = Convert.ToString(dTaxableValue + dIgstAmount);
                                }

                            }
                        }
                    }
                }
                else if (e.ColumnIndex == cutEditBoxGrid.Index)
                {
                    double dPcs = Convert.ToDouble(purchaseDataGridView.Rows[e.RowIndex].Cells[pcsTextBoxGrid.Index].FormattedValue.ToString());
                    if (dPcs <= 0)
                    {
                        MessageBox.Show("Please enter correct Cut", "CUT", MessageBoxButtons.OK);
                        //saleDataGridView.CurrentCell = saleDataGridView[e.RowIndex, e.ColumnIndex];
                        purchaseDataGridView.CurrentCell.Selected = true;
                        e.Cancel = true;
                    }
                    else
                    {
                        string str = purchaseDataGridView.Rows[e.RowIndex].Cells[unitComboGrid.Index].FormattedValue.ToString();
                        string strCut = e.FormattedValue.ToString();
                        double dCut;
                        if (strCut != "")
                            dCut = Convert.ToDouble(strCut);
                        else
                            dCut = 0;
                        double dMtrs = Convert.ToInt32(dPcs * dCut);
                        double dRate;
                        string strRate = purchaseDataGridView.Rows[e.RowIndex].Cells[rateTextBoxGrid.Index].FormattedValue.ToString();

                        if (strRate != "")
                            dRate = Convert.ToDouble(strRate);
                        else
                            dRate = 0;

                        purchaseDataGridView.Rows[e.RowIndex].Cells[qtyEditBoxGid.Index].Value = Convert.ToString(dMtrs);
                        if (str == "MTRS")
                        {

                            //string strMtrs = saleDataGridView.Rows[e.RowIndex].Cells[qtyEditBoxGid.Index].FormattedValue.ToString();
                            if (strRate != "")
                            {
                                string strAmount = Convert.ToString(dMtrs * dRate);
                                purchaseDataGridView.Rows[e.RowIndex].Cells[amountTextBoxGrid.Index].Value = strAmount;
                            }
                            else
                                purchaseDataGridView.Rows[e.RowIndex].Cells[amountTextBoxGrid.Index].Value = "0";
                        }
                        else
                            purchaseDataGridView.Rows[e.RowIndex].Cells[amountTextBoxGrid.Index].Value = Convert.ToString(dPcs * dRate);

                        if ((long)purchaseTypeComboBox.SelectedValue != 1 && (long)purchaseTypeComboBox.SelectedValue != 6)
                        {
                            double dRDAmount = 0;
                            double.TryParse(purchaseDataGridView.Rows[e.RowIndex].Cells[rdAmountTextBoxColumn.Index].EditedFormattedValue.ToString(), out dRDAmount);
                            double dDiscountAmount = 0;
                            double.TryParse(purchaseDataGridView.Rows[e.RowIndex].Cells[discountAmountTextBoxColumn.Index].EditedFormattedValue.ToString(), out dDiscountAmount);
                            double dAddLess = 0;
                            double.TryParse(purchaseDataGridView.Rows[e.RowIndex].Cells[addLessTextBoxColumn.Index].EditedFormattedValue.ToString(), out dAddLess);
                            double dCgstPercentage = 0;
                            double.TryParse(purchaseDataGridView.Rows[e.RowIndex].Cells[cgstPerTextBoxColumn.Index].EditedFormattedValue.ToString(), out dCgstPercentage);
                            double dCgstAmount = 0;
                            //double.TryParse(purchaseDataGridView.Rows[e.RowIndex].Cells[cgstAmountTextBoxColumn.Index].EditedFormattedValue.ToString(), out dCgstAmount);
                            double dSgstPercentage = 0;
                            double.TryParse(purchaseDataGridView.Rows[e.RowIndex].Cells[sgstPerTextBoxColumn.Index].EditedFormattedValue.ToString(), out dSgstPercentage);
                            double dSgstAmount = 0;
                            //double.TryParse(purchaseDataGridView.Rows[e.RowIndex].Cells[sgstAmountTextBoxColumn.Index].EditedFormattedValue.ToString(), out dSgstAmount);
                            double dAmount = 0;
                            double.TryParse(purchaseDataGridView.Rows[e.RowIndex].Cells[amountTextBoxGrid.Index].EditedFormattedValue.ToString(), out dAmount);
                            double dTaxableValue = dAmount - dRDAmount - dDiscountAmount - dAddLess;
                            if ((long)gstTypeComboBox.SelectedValue == 2)
                            {
                                dCgstAmount = (dCgstPercentage / 100) * dTaxableValue;
                                dSgstAmount = (dSgstPercentage / 100) * dTaxableValue;
                                purchaseDataGridView.Rows[e.RowIndex].Cells[sgstAmountTextBoxColumn.Index].Value = Convert.ToString(dSgstAmount);
                                purchaseDataGridView.Rows[e.RowIndex].Cells[cgstAmountTextBoxColumn.Index].Value = Convert.ToString(dCgstAmount);
                                purchaseDataGridView.Rows[e.RowIndex].Cells[totalAmountTextBoxColumn.Index].Value = Convert.ToString(dTaxableValue + dSgstAmount + dCgstAmount);
                            }
                            else if ((long)gstTypeComboBox.SelectedValue == 1)
                            {
                                double dIgstPercentage = 0;
                                double.TryParse(purchaseDataGridView.Rows[e.RowIndex].Cells[igstPerTextBoxColumn.Index].EditedFormattedValue.ToString(), out dIgstPercentage);
                                double dIgstAmount = (dIgstPercentage / 100) * dTaxableValue;
                                purchaseDataGridView.Rows[e.RowIndex].Cells[igstAmountTextBoxColumn.Index].Value = Convert.ToString(dIgstAmount);
                                purchaseDataGridView.Rows[e.RowIndex].Cells[totalAmountTextBoxColumn.Index].Value = Convert.ToString(dTaxableValue + dIgstAmount);
                            }

                        }
                    }
                }
                else if (e.ColumnIndex == rateTextBoxGrid.Index)
                {
                    double dRate = Convert.ToDouble(e.FormattedValue.ToString());
                    if (dRate <= 0)
                    {
                        MessageBox.Show("Please enter correct RATE", "RATE", MessageBoxButtons.OK);
                        //saleDataGridView.CurrentCell = saleDataGridView[e.RowIndex, e.ColumnIndex];
                        purchaseDataGridView.CurrentCell.Selected = true;
                        e.Cancel = true;
                    }
                    else
                    {
                        double dPcs = 0;
                        double.TryParse(purchaseDataGridView.Rows[e.RowIndex].Cells[pcsTextBoxGrid.Index].FormattedValue.ToString(),out dPcs);
                        string str = purchaseDataGridView.Rows[e.RowIndex].Cells[unitComboGrid.Index].FormattedValue.ToString();
                        
                        double dCut = 0;
                        double.TryParse(purchaseDataGridView.Rows[e.RowIndex].Cells[cutEditBoxGrid.Index].FormattedValue.ToString(),out dCut);
                        
                        double dMtrs;
                        string strMtrs = purchaseDataGridView.Rows[e.RowIndex].Cells[qtyEditBoxGid.Index].FormattedValue.ToString();
                        if (strMtrs != "")
                            dMtrs = Convert.ToDouble(strMtrs);
                        else
                            dMtrs = Convert.ToDouble(dPcs * dCut);

                        if (str == "MTRS")
                        {
                            //string strMtrs = saleDataGridView.Rows[e.RowIndex].Cells[qtyEditBoxGid.Index].FormattedValue.ToString();
                            string strAmount = Convert.ToString(dMtrs * dRate);
                            purchaseDataGridView.Rows[e.RowIndex].Cells[amountTextBoxGrid.Index].Value = strAmount;

                        }
                        else
                            purchaseDataGridView.Rows[e.RowIndex].Cells[amountTextBoxGrid.Index].Value = Convert.ToString(dPcs * dRate);

                        if ((long)purchaseTypeComboBox.SelectedValue != 1 && (long)purchaseTypeComboBox.SelectedValue != 6)
                        {
                            double dRDAmount = 0;
                            double.TryParse(purchaseDataGridView.Rows[e.RowIndex].Cells[rdAmountTextBoxColumn.Index].EditedFormattedValue.ToString(), out dRDAmount);
                            double dDiscountAmount = 0;
                            double.TryParse(purchaseDataGridView.Rows[e.RowIndex].Cells[discountAmountTextBoxColumn.Index].EditedFormattedValue.ToString(), out dDiscountAmount);
                            double dAddLess = 0;
                            double.TryParse(purchaseDataGridView.Rows[e.RowIndex].Cells[addLessTextBoxColumn.Index].EditedFormattedValue.ToString(), out dAddLess);
                            double dCgstPercentage = 0;
                            double.TryParse(purchaseDataGridView.Rows[e.RowIndex].Cells[cgstPerTextBoxColumn.Index].EditedFormattedValue.ToString(), out dCgstPercentage);
                            double dCgstAmount = 0;
                            //double.TryParse(purchaseDataGridView.Rows[e.RowIndex].Cells[cgstAmountTextBoxColumn.Index].EditedFormattedValue.ToString(), out dCgstAmount);
                            double dSgstPercentage = 0;
                            double.TryParse(purchaseDataGridView.Rows[e.RowIndex].Cells[sgstPerTextBoxColumn.Index].EditedFormattedValue.ToString(), out dSgstPercentage);
                            double dSgstAmount = 0;
                            //double.TryParse(purchaseDataGridView.Rows[e.RowIndex].Cells[sgstAmountTextBoxColumn.Index].EditedFormattedValue.ToString(), out dSgstAmount);
                            double dAmount = 0;
                            double.TryParse(purchaseDataGridView.Rows[e.RowIndex].Cells[amountTextBoxGrid.Index].EditedFormattedValue.ToString(), out dAmount);
                            double dTaxableValue = dAmount - dRDAmount - dDiscountAmount - dAddLess;
                            if ((long)gstTypeComboBox.SelectedValue == 2)
                            {
                                dCgstAmount = (dCgstPercentage / 100) * dTaxableValue;
                                dSgstAmount = (dSgstPercentage / 100) * dTaxableValue;
                                purchaseDataGridView.Rows[e.RowIndex].Cells[sgstAmountTextBoxColumn.Index].Value = Convert.ToString(dSgstAmount);
                                purchaseDataGridView.Rows[e.RowIndex].Cells[cgstAmountTextBoxColumn.Index].Value = Convert.ToString(dCgstAmount);
                                purchaseDataGridView.Rows[e.RowIndex].Cells[totalAmountTextBoxColumn.Index].Value = Convert.ToString(dTaxableValue + dSgstAmount + dCgstAmount);
                            }
                            else if ((long)gstTypeComboBox.SelectedValue == 1)
                            {
                                double dIgstPercentage = 0;
                                double.TryParse(purchaseDataGridView.Rows[e.RowIndex].Cells[igstPerTextBoxColumn.Index].EditedFormattedValue.ToString(), out dIgstPercentage);
                                double dIgstAmount = (dIgstPercentage / 100) * dTaxableValue;
                                purchaseDataGridView.Rows[e.RowIndex].Cells[igstAmountTextBoxColumn.Index].Value = Convert.ToString(dIgstAmount);
                                purchaseDataGridView.Rows[e.RowIndex].Cells[totalAmountTextBoxColumn.Index].Value = Convert.ToString(dTaxableValue + dIgstAmount);
                            }

                        }
                    }
                }
                else if (e.ColumnIndex == unitComboGrid.Index)
                {
                    double dPcs = 0;
                    double.TryParse(purchaseDataGridView.Rows[e.RowIndex].Cells[pcsTextBoxGrid.Index].FormattedValue.ToString(), out dPcs);
                    string str = purchaseDataGridView.Rows[e.RowIndex].Cells[unitComboGrid.Index].FormattedValue.ToString();

                    double dCut = 0;
                    double.TryParse(purchaseDataGridView.Rows[e.RowIndex].Cells[cutEditBoxGrid.Index].FormattedValue.ToString(), out dCut);

                    double dMtrs = Convert.ToInt32(dPcs * dCut);
                    double dRate;
                    string strRate = purchaseDataGridView.Rows[e.RowIndex].Cells[rateTextBoxGrid.Index].FormattedValue.ToString();

                    if (strRate != "")
                        dRate = Convert.ToDouble(strRate);
                    else
                        dRate = 0;

                    purchaseDataGridView.Rows[e.RowIndex].Cells[qtyEditBoxGid.Index].Value = Convert.ToString(dMtrs);
                    if (str == "MTRS")
                    {

                        //string strMtrs = saleDataGridView.Rows[e.RowIndex].Cells[qtyEditBoxGid.Index].FormattedValue.ToString();
                        if (strRate != "")
                        {
                            string strAmount = Convert.ToString(dMtrs * dRate);
                            purchaseDataGridView.Rows[e.RowIndex].Cells[amountTextBoxGrid.Index].Value = strAmount;
                        }
                        else
                            purchaseDataGridView.Rows[e.RowIndex].Cells[amountTextBoxGrid.Index].Value = "0";
                    }
                    else
                        purchaseDataGridView.Rows[e.RowIndex].Cells[amountTextBoxGrid.Index].Value = Convert.ToString(dPcs * dRate);

                    if ((long)purchaseTypeComboBox.SelectedValue != 1 && (long)purchaseTypeComboBox.SelectedValue != 6)
                    {
                        double dRDAmount = 0;
                        double.TryParse(purchaseDataGridView.Rows[e.RowIndex].Cells[rdAmountTextBoxColumn.Index].EditedFormattedValue.ToString(), out dRDAmount);
                        double dDiscountAmount = 0;
                        double.TryParse(purchaseDataGridView.Rows[e.RowIndex].Cells[discountAmountTextBoxColumn.Index].EditedFormattedValue.ToString(), out dDiscountAmount);
                        double dAddLess = 0;
                        double.TryParse(purchaseDataGridView.Rows[e.RowIndex].Cells[addLessTextBoxColumn.Index].EditedFormattedValue.ToString(), out dAddLess);
                        double dCgstPercentage = 0;
                        double.TryParse(purchaseDataGridView.Rows[e.RowIndex].Cells[cgstPerTextBoxColumn.Index].EditedFormattedValue.ToString(), out dCgstPercentage);
                        double dCgstAmount = 0;
                        //double.TryParse(purchaseDataGridView.Rows[e.RowIndex].Cells[cgstAmountTextBoxColumn.Index].EditedFormattedValue.ToString(), out dCgstAmount);
                        double dSgstPercentage = 0;
                        double.TryParse(purchaseDataGridView.Rows[e.RowIndex].Cells[sgstPerTextBoxColumn.Index].EditedFormattedValue.ToString(), out dSgstPercentage);
                        double dSgstAmount = 0;
                        //double.TryParse(purchaseDataGridView.Rows[e.RowIndex].Cells[sgstAmountTextBoxColumn.Index].EditedFormattedValue.ToString(), out dSgstAmount);
                        double dAmount = 0;
                        double.TryParse(purchaseDataGridView.Rows[e.RowIndex].Cells[amountTextBoxGrid.Index].EditedFormattedValue.ToString(), out dAmount);
                        double dTaxableValue = dAmount - dRDAmount - dDiscountAmount - dAddLess;
                        if ((long)gstTypeComboBox.SelectedValue == 2)
                        {
                            dCgstAmount = (dCgstPercentage / 100) * dTaxableValue;
                            dSgstAmount = (dSgstPercentage / 100) * dTaxableValue;
                            purchaseDataGridView.Rows[e.RowIndex].Cells[sgstAmountTextBoxColumn.Index].Value = Convert.ToString(dSgstAmount);
                            purchaseDataGridView.Rows[e.RowIndex].Cells[cgstAmountTextBoxColumn.Index].Value = Convert.ToString(dCgstAmount);
                            purchaseDataGridView.Rows[e.RowIndex].Cells[totalAmountTextBoxColumn.Index].Value = Convert.ToString(dTaxableValue + dSgstAmount + dCgstAmount);
                        }
                        else if ((long)gstTypeComboBox.SelectedValue == 1)
                        {
                            double dIgstPercentage = 0;
                            double.TryParse(purchaseDataGridView.Rows[e.RowIndex].Cells[igstPerTextBoxColumn.Index].EditedFormattedValue.ToString(), out dIgstPercentage);
                            double dIgstAmount = (dIgstPercentage / 100) * dTaxableValue;
                            purchaseDataGridView.Rows[e.RowIndex].Cells[igstAmountTextBoxColumn.Index].Value = Convert.ToString(dIgstAmount);
                            purchaseDataGridView.Rows[e.RowIndex].Cells[totalAmountTextBoxColumn.Index].Value = Convert.ToString(dTaxableValue + dIgstAmount);
                        }

                    }
                }
                else if(e.ColumnIndex == rdAmountTextBoxColumn.Index)
                {
                    if ((long)purchaseTypeComboBox.SelectedValue != 1 && (long)purchaseTypeComboBox.SelectedValue != 6)
                    {
                        double dRDAmount = 0;
                        double.TryParse(purchaseDataGridView.Rows[e.RowIndex].Cells[rdAmountTextBoxColumn.Index].EditedFormattedValue.ToString(), out dRDAmount);
                        double dDiscountAmount = 0;
                        double.TryParse(purchaseDataGridView.Rows[e.RowIndex].Cells[discountAmountTextBoxColumn.Index].EditedFormattedValue.ToString(), out dDiscountAmount);
                        double dAddLess = 0;
                        double.TryParse(purchaseDataGridView.Rows[e.RowIndex].Cells[addLessTextBoxColumn.Index].EditedFormattedValue.ToString(), out dAddLess);
                        double dCgstPercentage = 0;
                        double.TryParse(purchaseDataGridView.Rows[e.RowIndex].Cells[cgstPerTextBoxColumn.Index].EditedFormattedValue.ToString(), out dCgstPercentage);
                        double dCgstAmount = 0;
                        //double.TryParse(purchaseDataGridView.Rows[e.RowIndex].Cells[cgstAmountTextBoxColumn.Index].EditedFormattedValue.ToString(), out dCgstAmount);
                        double dSgstPercentage = 0;
                        double.TryParse(purchaseDataGridView.Rows[e.RowIndex].Cells[sgstPerTextBoxColumn.Index].EditedFormattedValue.ToString(), out dSgstPercentage);
                        double dSgstAmount = 0;
                        //double.TryParse(purchaseDataGridView.Rows[e.RowIndex].Cells[sgstAmountTextBoxColumn.Index].EditedFormattedValue.ToString(), out dSgstAmount);
                        double dAmount = 0;
                        double.TryParse(purchaseDataGridView.Rows[e.RowIndex].Cells[amountTextBoxGrid.Index].EditedFormattedValue.ToString(), out dAmount);
                        double dTaxableValue = dAmount - dRDAmount - dDiscountAmount - dAddLess;
                        if ((long)gstTypeComboBox.SelectedValue == 2)
                        {
                            dCgstAmount = (dCgstPercentage / 100) * dTaxableValue;
                            dSgstAmount = (dSgstPercentage / 100) * dTaxableValue;
                            purchaseDataGridView.Rows[e.RowIndex].Cells[sgstAmountTextBoxColumn.Index].Value = Convert.ToString(dSgstAmount);
                            purchaseDataGridView.Rows[e.RowIndex].Cells[cgstAmountTextBoxColumn.Index].Value = Convert.ToString(dCgstAmount);
                            purchaseDataGridView.Rows[e.RowIndex].Cells[totalAmountTextBoxColumn.Index].Value = Convert.ToString(dTaxableValue + dSgstAmount + dCgstAmount);
                        }
                        else if ((long)gstTypeComboBox.SelectedValue == 1)
                        {
                            double dIgstPercentage = 0;
                            double.TryParse(purchaseDataGridView.Rows[e.RowIndex].Cells[igstPerTextBoxColumn.Index].EditedFormattedValue.ToString(), out dIgstPercentage);
                            double dIgstAmount = (dIgstPercentage / 100) * dTaxableValue;
                            purchaseDataGridView.Rows[e.RowIndex].Cells[igstAmountTextBoxColumn.Index].Value = Convert.ToString(dIgstAmount);
                            purchaseDataGridView.Rows[e.RowIndex].Cells[totalAmountTextBoxColumn.Index].Value = Convert.ToString(dTaxableValue + dIgstAmount);
                        }

                    }
                }
                else if(e.ColumnIndex == discountAmountTextBoxColumn.Index)
                {
                    if ((long)purchaseTypeComboBox.SelectedValue != 1 && (long)purchaseTypeComboBox.SelectedValue != 6)
                    {
                        double dRDAmount = 0;
                        double.TryParse(purchaseDataGridView.Rows[e.RowIndex].Cells[rdAmountTextBoxColumn.Index].EditedFormattedValue.ToString(), out dRDAmount);
                        double dDiscountAmount = 0;
                        double.TryParse(purchaseDataGridView.Rows[e.RowIndex].Cells[discountAmountTextBoxColumn.Index].EditedFormattedValue.ToString(), out dDiscountAmount);
                        double dAddLess = 0;
                        double.TryParse(purchaseDataGridView.Rows[e.RowIndex].Cells[addLessTextBoxColumn.Index].EditedFormattedValue.ToString(), out dAddLess);
                        double dCgstPercentage = 0;
                        double.TryParse(purchaseDataGridView.Rows[e.RowIndex].Cells[cgstPerTextBoxColumn.Index].EditedFormattedValue.ToString(), out dCgstPercentage);
                        double dCgstAmount = 0;
                        //double.TryParse(purchaseDataGridView.Rows[e.RowIndex].Cells[cgstAmountTextBoxColumn.Index].EditedFormattedValue.ToString(), out dCgstAmount);
                        double dSgstPercentage = 0;
                        double.TryParse(purchaseDataGridView.Rows[e.RowIndex].Cells[sgstPerTextBoxColumn.Index].EditedFormattedValue.ToString(), out dSgstPercentage);
                        double dSgstAmount = 0;
                        //double.TryParse(purchaseDataGridView.Rows[e.RowIndex].Cells[sgstAmountTextBoxColumn.Index].EditedFormattedValue.ToString(), out dSgstAmount);
                        double dAmount = 0;
                        double.TryParse(purchaseDataGridView.Rows[e.RowIndex].Cells[amountTextBoxGrid.Index].EditedFormattedValue.ToString(), out dAmount);
                        double dTaxableValue = dAmount - dRDAmount - dDiscountAmount - dAddLess;
                        if ((long)gstTypeComboBox.SelectedValue == 2)
                        {
                            dCgstAmount = (dCgstPercentage / 100) * dTaxableValue;
                            dSgstAmount = (dSgstPercentage / 100) * dTaxableValue;
                            purchaseDataGridView.Rows[e.RowIndex].Cells[sgstAmountTextBoxColumn.Index].Value = Convert.ToString(dSgstAmount);
                            purchaseDataGridView.Rows[e.RowIndex].Cells[cgstAmountTextBoxColumn.Index].Value = Convert.ToString(dCgstAmount);
                            purchaseDataGridView.Rows[e.RowIndex].Cells[totalAmountTextBoxColumn.Index].Value = Convert.ToString(dTaxableValue + dSgstAmount + dCgstAmount);
                        }
                        else if ((long)gstTypeComboBox.SelectedValue == 1)
                        {
                            double dIgstPercentage = 0;
                            double.TryParse(purchaseDataGridView.Rows[e.RowIndex].Cells[igstPerTextBoxColumn.Index].EditedFormattedValue.ToString(), out dIgstPercentage);
                            double dIgstAmount = (dIgstPercentage / 100) * dTaxableValue;
                            purchaseDataGridView.Rows[e.RowIndex].Cells[igstAmountTextBoxColumn.Index].Value = Convert.ToString(dIgstAmount);
                            purchaseDataGridView.Rows[e.RowIndex].Cells[totalAmountTextBoxColumn.Index].Value = Convert.ToString(dTaxableValue + dIgstAmount);
                        }

                    }
                }
                else if(e.ColumnIndex == addLessTextBoxColumn.Index || e.ColumnIndex == cgstPerTextBoxColumn.Index || e.ColumnIndex == cgstAmountTextBoxColumn.Index || e.ColumnIndex == sgstPerTextBoxColumn.Index || e.ColumnIndex == sgstAmountTextBoxColumn.Index || e.ColumnIndex == igstPerTextBoxColumn.Index || e.ColumnIndex == igstAmountTextBoxColumn.Index)
                {
                    if ((long)purchaseTypeComboBox.SelectedValue != 1 && (long)purchaseTypeComboBox.SelectedValue != 6)
                    {
                        double dRDAmount = 0;
                        double.TryParse(purchaseDataGridView.Rows[e.RowIndex].Cells[rdAmountTextBoxColumn.Index].EditedFormattedValue.ToString(), out dRDAmount);
                        double dDiscountAmount = 0;
                        double.TryParse(purchaseDataGridView.Rows[e.RowIndex].Cells[discountAmountTextBoxColumn.Index].EditedFormattedValue.ToString(), out dDiscountAmount);
                        double dAddLess = 0;
                        double.TryParse(purchaseDataGridView.Rows[e.RowIndex].Cells[addLessTextBoxColumn.Index].EditedFormattedValue.ToString(), out dAddLess);
                        double dCgstPercentage = 0;
                        double.TryParse(purchaseDataGridView.Rows[e.RowIndex].Cells[cgstPerTextBoxColumn.Index].EditedFormattedValue.ToString(), out dCgstPercentage);
                        double dCgstAmount = 0;
                        //double.TryParse(purchaseDataGridView.Rows[e.RowIndex].Cells[cgstAmountTextBoxColumn.Index].EditedFormattedValue.ToString(), out dCgstAmount);
                        double dSgstPercentage = 0;
                        double.TryParse(purchaseDataGridView.Rows[e.RowIndex].Cells[sgstPerTextBoxColumn.Index].EditedFormattedValue.ToString(), out dSgstPercentage);
                        double dSgstAmount = 0;
                        //double.TryParse(purchaseDataGridView.Rows[e.RowIndex].Cells[sgstAmountTextBoxColumn.Index].EditedFormattedValue.ToString(), out dSgstAmount);
                        double dAmount = 0;
                        double.TryParse(purchaseDataGridView.Rows[e.RowIndex].Cells[amountTextBoxGrid.Index].EditedFormattedValue.ToString(), out dAmount);
                        double dTaxableValue = dAmount - dRDAmount - dDiscountAmount - dAddLess;
                        if ((long)gstTypeComboBox.SelectedValue == 2)
                        {
                            dCgstAmount = (dCgstPercentage / 100) * dTaxableValue;
                            dSgstAmount = (dSgstPercentage / 100) * dTaxableValue;
                            purchaseDataGridView.Rows[e.RowIndex].Cells[sgstAmountTextBoxColumn.Index].Value = Convert.ToString(dSgstAmount);
                            purchaseDataGridView.Rows[e.RowIndex].Cells[cgstAmountTextBoxColumn.Index].Value = Convert.ToString(dCgstAmount);
                            purchaseDataGridView.Rows[e.RowIndex].Cells[totalAmountTextBoxColumn.Index].Value = Convert.ToString(dTaxableValue + dSgstAmount + dCgstAmount);
                        }
                        else if ((long)gstTypeComboBox.SelectedValue == 1)
                        {
                            double dIgstPercentage = 0;
                            double.TryParse(purchaseDataGridView.Rows[e.RowIndex].Cells[igstPerTextBoxColumn.Index].EditedFormattedValue.ToString(), out dIgstPercentage);
                            double dIgstAmount = (dIgstPercentage / 100) * dTaxableValue;
                            purchaseDataGridView.Rows[e.RowIndex].Cells[igstAmountTextBoxColumn.Index].Value = Convert.ToString(dIgstAmount);
                            purchaseDataGridView.Rows[e.RowIndex].Cells[totalAmountTextBoxColumn.Index].Value = Convert.ToString(dTaxableValue + dIgstAmount);
                        }

                    }
                }
                else if (e.ColumnIndex == bundlesEditBoxGrid.Index)
                {
                    string bundlesString = purchaseDataGridView.Rows[e.RowIndex].Cells[bundlesEditBoxGrid.Index].EditedFormattedValue.ToString();
                    if (bundlesString != "")
                    {
                        DataTable dt = new DataTable();
                        try
                        {
                            int answer = (int)dt.Compute(bundlesString, "");
                            purchaseDataGridView.Rows[e.RowIndex].Cells[pcsTextBoxGrid.Index].Value = answer;
                        }
                        catch (Exception ex)
                        {
                            purchaseDataGridView.Rows[e.RowIndex].Cells[pcsTextBoxGrid.Index].Value = 0;
                        }
                    }
                    else
                        purchaseDataGridView.Rows[e.RowIndex].Cells[pcsTextBoxGrid.Index].Value = 0;

                }
                if ((long)purchaseTypeComboBox.SelectedValue != 1 && (long)purchaseTypeComboBox.SelectedValue != 6)
                {
                    double dBillAmount = 0;
                    for(int i = 0;i<purchaseDataGridView.RowCount-1;i++)
                    {
                        double dItemTotalAmount = 0;
                        double.TryParse(purchaseDataGridView.Rows[e.RowIndex].Cells[totalAmountTextBoxColumn.Index].EditedFormattedValue.ToString(),out dItemTotalAmount);
                        dBillAmount = dBillAmount + dItemTotalAmount;
                    }
                    billAmtTextBox.Text = Convert.ToString(dBillAmount);
                }
            }
        }

        private void purchaseTypeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if ((long)purchaseTypeComboBox.SelectedValue == 1 || (long)purchaseTypeComboBox.SelectedValue == 6)
                {
                    rdAmountTextBoxColumn.Visible = false;
                    discountAmountTextBoxColumn.Visible = false;
                    cgstAmountTextBoxColumn.Visible = false;
                    cgstPerTextBoxColumn.Visible = false;
                    sgstPerTextBoxColumn.Visible = false;
                    sgstAmountTextBoxColumn.Visible = false;
                    igstAmountTextBoxColumn.Visible = false;
                    igstPerTextBoxColumn.Visible = false;
                    addLessTextBoxColumn.Visible = false;
                    totalAmountTextBox.Visible = false;
                    totalAmountTextBoxColumn.Visible = false;
                    groupBox1.Show();
                    label15.Show();
                    totalAmountTextBox.Show();
                    totalPcsTextBox.Show();
                    totalQtyTextBox.Show();
                    label31.Show();
                    taxValTextBox.Show();
                }
                else
                {
                    rdAmountTextBoxColumn.Visible = true;
                    discountAmountTextBoxColumn.Visible = true;
                    if (gstTypeComboBox.SelectedIndex == 1)
                    {
                        cgstAmountTextBoxColumn.Visible = true;
                        cgstPerTextBoxColumn.Visible = true;
                        sgstPerTextBoxColumn.Visible = true;
                        sgstAmountTextBoxColumn.Visible = true;
                        igstAmountTextBoxColumn.Visible = false;
                        igstPerTextBoxColumn.Visible = false;
                    }
                    else if (gstTypeComboBox.SelectedIndex == 0)
                    {
                        igstAmountTextBoxColumn.Visible = true;
                        igstPerTextBoxColumn.Visible = true;
                        cgstAmountTextBoxColumn.Visible = false;
                        cgstPerTextBoxColumn.Visible = false;
                        sgstPerTextBoxColumn.Visible = false;
                        sgstAmountTextBoxColumn.Visible = false;
                    }
                    totalAmountTextBox.Visible = true;
                    addLessTextBoxColumn.Visible = true;
                    totalAmountTextBoxColumn.Visible = true;
                    groupBox1.Hide();
                    label15.Hide();
                    totalAmountTextBox.Hide();
                    totalPcsTextBox.Hide();
                    totalQtyTextBox.Hide();
                    label31.Hide();
                    taxValTextBox.Hide();
                }
                if ((long)purchaseTypeComboBox.SelectedValue == 1 || (long)purchaseTypeComboBox.SelectedValue == 2 || (long)purchaseTypeComboBox.SelectedValue == 3 || (long)purchaseTypeComboBox.SelectedValue == 4 || (long)purchaseTypeComboBox.SelectedValue == 5)
                {
                    label1.Enabled = false;
                    refBillNoComboBox.Enabled = false;

                }
                else
                {
                    label1.Enabled = true;
                    refBillNoComboBox.Enabled = true;

                    fillReferenceBillDetailsForReturn(Convert.ToInt64(partyComboBox["PARTY_ID"].ToString()));
                }
                purchaseDataGridView.Rows.Clear();
                resetAllData();
                setDafaultData();
            }
            catch(Exception ex)
            {
                return;
            }
        }
        private void UpdateCgstSgstData()
        {
            double dSgstRate = 0;
            double.TryParse(sgstRateTextBox.Text,out dSgstRate);
            double dTaxableValue = 0;
            double.TryParse(taxValTextBox.Text,out dTaxableValue);
            double dSgstAmount = (dTaxableValue * dSgstRate) / 100;
            sgstAmtTextBox.Text = Convert.ToString(dSgstAmount);
            cgstRateTextBox.Text = sgstRateTextBox.Text;
            cgstAmtTextBox.Text = sgstAmtTextBox.Text;

            double dCgstAmount = Convert.ToDouble(cgstAmtTextBox.Text);

            if ((long)purchaseTypeComboBox.SelectedValue == 1 || (long)purchaseTypeComboBox.SelectedValue == 6)
                billAmtTextBox.Text = Convert.ToString(Math.Round(dCgstAmount + dSgstAmount + dTaxableValue));
            if((long)purchaseTypeComboBox.SelectedIndex != 0 && (long)purchaseTypeComboBox.SelectedIndex != 5)
            {
                cgstPerTextBoxColumn.Visible = true;
                cgstAmountTextBoxColumn.Visible = true;
                sgstPerTextBoxColumn.Visible = true;
                sgstAmountTextBoxColumn.Visible = true;
                igstPerTextBoxColumn.Visible = false;
                igstAmountTextBoxColumn.Visible = false;
            }
            
        }
        private void UpdateIgstData()
        {
            string strIgstRate = igstRateTextBox.Text;
            double dIgstRate = 0;
            double.TryParse(igstRateTextBox.Text,out dIgstRate);
            double dTaxableValue = 0;
            double.TryParse(taxValTextBox.Text,out dTaxableValue);
            double dIgstAmount = (dTaxableValue * dIgstRate) / 100;
            

            igstAmtTextBox.Text = Convert.ToString(dIgstAmount);
            if((long)purchaseTypeComboBox.SelectedValue == 1 || (long)purchaseTypeComboBox.SelectedValue == 6)
                billAmtTextBox.Text = Convert.ToString(Math.Round(dIgstAmount + dTaxableValue));
            if ((long)purchaseTypeComboBox.SelectedIndex != 0 && (long)purchaseTypeComboBox.SelectedIndex != 5)
            {
                cgstPerTextBoxColumn.Visible = false;
                cgstAmountTextBoxColumn.Visible = false;
                sgstPerTextBoxColumn.Visible = false;
                sgstAmountTextBoxColumn.Visible = false;
                igstPerTextBoxColumn.Visible = true;
                igstAmountTextBoxColumn.Visible = true;
            }
        }

        private void gstTypeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (gstTypeComboBox.SelectedIndex == 0)
            {
                cgstRateTextBox.Text = "0";
                cgstAmtTextBox.Enabled = false;
                sgstRateTextBox.Text = "0";
                sgstAmtTextBox.Enabled = false;
                cgstRateTextBox.Enabled = false;
                sgstRateTextBox.Enabled = false;
                igstRateTextBox.Text = "5";
                igstRateTextBox.Enabled = true;
                igstAmtTextBox.Enabled = true;

                UpdateIgstData();
            }
            else if (gstTypeComboBox.SelectedIndex == 1)
            {
                cgstRateTextBox.Text = "2.5";
                cgstAmtTextBox.Enabled = true;
                sgstRateTextBox.Text = "2.5";
                sgstAmtTextBox.Enabled = true;
                cgstRateTextBox.Enabled = true;
                sgstRateTextBox.Enabled = true;
                igstRateTextBox.Text = "0";
                igstRateTextBox.Enabled = false;
                igstAmtTextBox.Enabled = false;

                UpdateCgstSgstData();
            }
        }

        private void cgstRateTextBox_TextChanged(object sender, EventArgs e)
        {
            string strCgstRate = cgstRateTextBox.Text;
            double dCgstRate = 0;
            bool bFlag = double.TryParse(strCgstRate, out dCgstRate);
            if (bFlag)
                dCgstRate = Convert.ToDouble(strCgstRate);
            double dCgstAmount = 0;
            bFlag = double.TryParse(taxValTextBox.Text, out dCgstAmount);
            if (bFlag)
                dCgstAmount = (Convert.ToDouble(taxValTextBox.Text) * dCgstRate) / 100;
            cgstAmtTextBox.Text = Convert.ToString(dCgstAmount);
            sgstRateTextBox.Text = cgstRateTextBox.Text;
            sgstAmtTextBox.Text = cgstAmtTextBox.Text;

            double dSgstAmount = Convert.ToDouble(sgstAmtTextBox.Text);
            double dTaxableValue = Convert.ToDouble(taxValTextBox.Text);
            if ((long)purchaseTypeComboBox.SelectedValue == 1 || (long)purchaseTypeComboBox.SelectedValue == 6)
                billAmtTextBox.Text = Convert.ToString(Math.Round(dCgstAmount + dSgstAmount + dTaxableValue));
        }

        private void sgstRateTextBox_TextChanged(object sender, EventArgs e)
        {
            string strSgstRate = sgstRateTextBox.Text;
            double dSgstRate = 0;
            bool bFlag = double.TryParse(strSgstRate, out dSgstRate);
            if (bFlag)
                dSgstRate = Convert.ToDouble(strSgstRate);
            double dSgstAmount = 0;
            bFlag = double.TryParse(taxValTextBox.Text, out dSgstAmount);
            if (bFlag)
                dSgstAmount = (Convert.ToDouble(taxValTextBox.Text) * dSgstRate) / 100;
            sgstAmtTextBox.Text = Convert.ToString(dSgstAmount);
            cgstRateTextBox.Text = sgstRateTextBox.Text;
            cgstAmtTextBox.Text = sgstAmtTextBox.Text;

            double dCgstAmount = Convert.ToDouble(cgstAmtTextBox.Text);
            double dTaxableValue = Convert.ToDouble(taxValTextBox.Text);
            if ((long)purchaseTypeComboBox.SelectedValue == 1 || (long)purchaseTypeComboBox.SelectedValue == 6)
                billAmtTextBox.Text = Convert.ToString(Math.Round(dCgstAmount + dSgstAmount + dTaxableValue));
        }

        private void igstRateTextBox_TextChanged(object sender, EventArgs e)
        {
            double dIgstRate = 0;
            double dIgstAmount = 0;
            bool bFlag = double.TryParse(igstRateTextBox.Text, out dIgstRate);
            if (bFlag)
                dIgstAmount = (dIgstRate * Convert.ToDouble(taxValTextBox.Text)) / 100;

            igstAmtTextBox.Text = Convert.ToString(dIgstAmount);
            if ((long)purchaseTypeComboBox.SelectedValue == 1 || (long)purchaseTypeComboBox.SelectedValue == 6)
                billAmtTextBox.Text = Convert.ToString(Math.Round(dIgstAmount + Convert.ToDouble(taxValTextBox.Text)));
        }

        private void totalAmountTextBox_TextChanged(object sender, EventArgs e)
        {
            double dTotalAmount = 0;
            double.TryParse(totalAmountTextBox.Text,out dTotalAmount);
            double dDiscountAmount = 0;
            double.TryParse(discPerAmtFinalTextBox.Text, out dDiscountAmount);
            double dAddLessAmount = 0;
            double.TryParse(addLessAmtFinalTextBox.Text,out dAddLessAmount);
            double dTaxableValue = dTotalAmount - dDiscountAmount + dAddLessAmount;
            taxValTextBox.Text = Convert.ToString(dTaxableValue);
        }

        private void taxValTextBox_TextChanged(object sender, EventArgs e)
        {
            if (igstRateTextBox.Enabled)
            {
                UpdateIgstData();
            }
            else
            {
                UpdateCgstSgstData();
            }
        }

        private void stateComboBox_SelectedValueChanged(object sender, EventArgs e)
        {
            string strCompanyStationCode = companyGstn.Substring(0, 2);
            if (stateComboBox.Text == strCompanyStationCode)
                gstTypeComboBox.SelectedValue = Convert.ToInt64(2);
            else
                gstTypeComboBox.SelectedValue = Convert.ToInt64(1);
        }

        private void discPerTextBox_TextChanged(object sender, EventArgs e)
        {

            double dDiscountPercentage = 0;
            double dDiscountPercentageOnAmount = 0;
            double dDiscountAmount = 0;
            bool bFlag = double.TryParse(discPerTextBox.Text, out dDiscountPercentage);
            if (bFlag)
                dDiscountPercentage = Convert.ToDouble(discPerTextBox.Text);
            bool bFlag1 = double.TryParse(discPerAmtTextBox.Text, out dDiscountPercentageOnAmount);
            if (bFlag1)
                dDiscountPercentageOnAmount = Convert.ToDouble(discPerAmtTextBox.Text);
            dDiscountAmount = (dDiscountPercentage * dDiscountPercentageOnAmount) / 100;
            discPerAmtFinalTextBox.Text = Convert.ToString(dDiscountAmount);
        }

        private void discPerAmtTextBox_TextChanged(object sender, EventArgs e)
        {
            double dDiscountPercentage = 0;
            double dDiscountPercentageOnAmount = 0;
            double dDiscountAmount = 0;
            bool bFlag = double.TryParse(discPerTextBox.Text, out dDiscountPercentage);
            if (bFlag)
                dDiscountPercentage = Convert.ToDouble(discPerTextBox.Text);
            bool bFlag1 = double.TryParse(discPerAmtTextBox.Text, out dDiscountPercentageOnAmount);
            if (bFlag1)
                dDiscountPercentageOnAmount = Convert.ToDouble(discPerAmtTextBox.Text);
            dDiscountAmount = (dDiscountPercentage * dDiscountPercentageOnAmount) / 100;
            discPerAmtFinalTextBox.Text = Convert.ToString(dDiscountAmount);
        }

        private void addLessTextBox_TextChanged(object sender, EventArgs e)
        {

            double dAddLess = 0;
            double dAddLessAmount = 0;
            bool bFlag = double.TryParse(addLessAmtTextBox.Text, out dAddLessAmount);
            bool bFlag1 = double.TryParse(addLessTextBox.Text, out dAddLess);

            if (bFlag && bFlag1)
                dAddLessAmount = Convert.ToDouble(addLessTextBox.Text) * Convert.ToDouble(addLessAmtTextBox.Text);
            addLessAmtFinalTextBox.Text = Convert.ToString(dAddLessAmount);
        }

        private void addLessAmtTextBox_TextChanged(object sender, EventArgs e)
        {
            double dAddLess = 0;
            double dAddLessAmount = 0;
            bool bFlag = double.TryParse(addLessAmtTextBox.Text, out dAddLessAmount);
            bool bFlag1 = double.TryParse(addLessTextBox.Text, out dAddLess);

            if (bFlag && bFlag1)
                dAddLessAmount = Convert.ToDouble(addLessTextBox.Text) * Convert.ToDouble(addLessAmtTextBox.Text);
            addLessAmtFinalTextBox.Text = Convert.ToString(dAddLessAmount);
        }

        private void discPerAmtFinalTextBox_TextChanged(object sender, EventArgs e)
        {
            double dDiscPerAmountFinal = 0;
            double dTaxableValue = 0;
            bool bFlag = double.TryParse(discPerAmtFinalTextBox.Text, out dDiscPerAmountFinal);
            double dTotalAmount = 0;
            double.TryParse(totalAmountTextBox.Text,out dTotalAmount);
            double dAddLessAmount = 0;
            double.TryParse(addLessAmtFinalTextBox.Text,out dAddLessAmount);
            
            dTaxableValue = dTotalAmount - dDiscPerAmountFinal + dAddLessAmount;
            
            taxValTextBox.Text = Convert.ToString(dTaxableValue);
        }

        private void addLessAmtFinalTextBox_TextChanged(object sender, EventArgs e)
        {
            double dTaxableValue = 0;
            double dDiscPerAmountFinal = 0;
            
            double.TryParse(discPerAmtFinalTextBox.Text, out dDiscPerAmountFinal);
            double dTotalAmount = 0;
            double.TryParse(totalAmountTextBox.Text, out dTotalAmount);
            double dAddLessFinalAmount = 0;
            double.TryParse(addLessAmtFinalTextBox.Text, out dAddLessFinalAmount);
            dTaxableValue = Convert.ToDouble(totalAmountTextBox.Text) - dDiscPerAmountFinal + dAddLessFinalAmount;
            
            taxValTextBox.Text = Convert.ToString(dTaxableValue);
        }

        private void dateTextBox_Validating(object sender, CancelEventArgs e)
        {
            DateTime dt;
            bool isValid = DateTime.TryParseExact(dateTextBox.Text, "dd-MM-yyyy", null, DateTimeStyles.None, out dt);
            if (!isValid)
            {
                MessageBox.Show("Please enter the valid date", "DATE", MessageBoxButtons.OK);
                dateTextBox.Focus();
            }
        }

        private void addNewButton_Click(object sender, EventArgs e)
        {
            long lVoucherNo = 0;
            bool bVoucherNoValid = long.TryParse(vouvherNoTextBox.Text, out lVoucherNo);
            if(!bVoucherNoValid)
            {
                MessageBox.Show("Please enter a valid Voucher No.", "Voucher Error", MessageBoxButtons.OK);
                vouvherNoTextBox.Focus();
                vouvherNoTextBox.SelectAll();
                return;
            }
            if (billNoTextBox.Text.Length <= 0)
            {
                MessageBox.Show("Please enter a valid Bill No.", "Bill Error", MessageBoxButtons.OK);
                billNoTextBox.Focus();
                billNoTextBox.SelectAll();
                return;
            }

            bool bFlag = partyDB.checkPurchaseBillExists(lVoucherNo, billNoTextBox.Text, Convert.ToInt64(partyComboBox["PARTY_ID"].ToString()));
            if (!bFlag)
            {
                MessageBox.Show("Voucher already Exists", "Voucher Error", MessageBoxButtons.OK);
                billNoTextBox.Focus();
                billNoTextBox.SelectAll();
                return;
            }
            purchase_details purchaseDetails = new purchase_details();
            if((long)purchaseTypeComboBox.SelectedValue == 1 || (long)purchaseTypeComboBox.SelectedValue == 6)
            {
                purchaseDetails = getPurchaseDataFromControl(purchaseDetails);
                purchaseDetails.purchaseitem_details = getPurchaseItemDetails();

            }
            else
            {
                purchaseDetails = getAllTypesPurchaseDataFromControl(purchaseDetails);
                purchaseDetails.purchaseitem_details = getPurchaseItemDataForAllPurchases();
            }
            
            bool bSalesBillAdded = partyDB.addPurchaseBill(purchaseDetails);
            if (bSalesBillAdded)
            {
                MessageBox.Show("Purchase Bill Added Successfully", "SALES", MessageBoxButtons.OK);
                resetAllData();
                setDafaultData();
                vouvherNoTextBox.Focus();
                vouvherNoTextBox.SelectAll();
            }
            else
                MessageBox.Show("Purchase Bill NOT Added Successfully", "ERROR", MessageBoxButtons.OK);
            
        }

        private purchase_details getPurchaseDataFromControl(purchase_details purchaseDetails)
        {
            purchaseDetails.billno = billNoTextBox.Text;
            purchaseDetails.brokerid = Convert.ToInt64(brokerComboBox.SelectedValue);
            purchaseDetails.caseno = caseNoTextBox.Text;
            purchaseDetails.cgst = Convert.ToDouble(cgstAmtTextBox.Text);
            purchaseDetails.date = DateTime.ParseExact(dateTextBox.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            //salesDetails.date = Convert.ToDateTime(dateTextBox.Text);
            purchaseDetails.discount = discPerAmtFinalTextBox.Text;
            purchaseDetails.purchase_gsttypeid = Convert.ToInt64(gstTypeComboBox.SelectedValue);
            purchaseDetails.igst = Convert.ToDouble(igstAmtTextBox.Text);
            purchaseDetails.lrno = lrnoTextBox.Text;
            //purchaseDetails.orderno = orderNoTextBox.Text;
            purchaseDetails.partygst = gstnTextBox.Text;
            purchaseDetails.partyid = Convert.ToInt64(Convert.ToInt64(partyComboBox["PARTY_ID"].ToString()));
            purchaseDetails.remark = remarkTextBox.Text;
            purchaseDetails.voucherNo = Convert.ToInt64(vouvherNoTextBox.Text);
            purchaseDetails.purchase_typeid = Convert.ToInt64(purchaseTypeComboBox.SelectedValue);
            if (purchaseDetails.purchase_typeid == 6)
                purchaseDetails.refBillNo = refBillNoComboBox["BILLNO"].ToString();
            purchaseDetails.sgst = Convert.ToDouble(sgstAmtTextBox.Text);
            purchaseDetails.stationcode = Convert.ToInt64(stateComboBox.SelectedValue);
            purchaseDetails.station = Convert.ToInt64(stationComboBox.SelectedValue);
            purchaseDetails.taxablevalue = Convert.ToDouble(taxValTextBox.Text);
            purchaseDetails.transportid = Convert.ToInt64(transportComboBox.SelectedValue);
            purchaseDetails.additional = addLessAmtFinalTextBox.Text;
            purchaseDetails.billamount = Convert.ToDouble(billAmtTextBox.Text);
            purchaseDetails.addLessDetail1 = Convert.ToDouble(addLessTextBox.Text);
            purchaseDetails.addLessDetail2 = Convert.ToDouble(addLessAmtTextBox.Text);
            purchaseDetails.caseNoDetail2 = caseNoTextBox2.Text;
            purchaseDetails.cgstRate = Convert.ToDouble(cgstRateTextBox.Text);
            purchaseDetails.discountOnAmount = Convert.ToDouble(discPerAmtTextBox.Text);
            purchaseDetails.discountPercentage = Convert.ToDouble(discPerTextBox.Text);
            purchaseDetails.grossAmount = Convert.ToDouble(totalAmountTextBox.Text);
            purchaseDetails.igstRate = Convert.ToDouble(igstRateTextBox.Text);
            purchaseDetails.sgstRate = Convert.ToDouble(sgstRateTextBox.Text);
            purchaseDetails.duedays = Convert.ToInt64(dueDaysTextBox.Text);
            return purchaseDetails;

        }

        private purchase_details getAllTypesPurchaseDataFromControl(purchase_details purchaseDetails)
        {
            purchaseDetails.billno = billNoTextBox.Text;
            purchaseDetails.brokerid = Convert.ToInt64(brokerComboBox.SelectedValue);
            //purchaseDetails.caseno = caseNoTextBox.Text;
            //purchaseDetails.cgst = Convert.ToDouble(cgstAmtTextBox.Text);
            purchaseDetails.date = DateTime.ParseExact(dateTextBox.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            //salesDetails.date = Convert.ToDateTime(dateTextBox.Text);
            //purchaseDetails.discount = discPerAmtFinalTextBox.Text;
            purchaseDetails.purchase_gsttypeid = Convert.ToInt64(gstTypeComboBox.SelectedValue);
            
            purchaseDetails.lrno = lrnoTextBox.Text;
            //purchaseDetails.orderno = orderNoTextBox.Text;
            purchaseDetails.partygst = gstnTextBox.Text;
            purchaseDetails.partyid = Convert.ToInt64(Convert.ToInt64(partyComboBox["PARTY_ID"].ToString()));
            purchaseDetails.remark = remarkTextBox.Text;
            purchaseDetails.voucherNo = Convert.ToInt64(vouvherNoTextBox.Text);
            purchaseDetails.purchase_typeid = Convert.ToInt64(purchaseTypeComboBox.SelectedValue);
            if (purchaseDetails.purchase_typeid == 7)
                purchaseDetails.refBillNo = refBillNoComboBox["BILLNO"].ToString();
            purchaseDetails.stationcode = Convert.ToInt64(stateComboBox.SelectedValue);
            purchaseDetails.station = Convert.ToInt64(stationComboBox.SelectedValue);
            
            purchaseDetails.transportid = Convert.ToInt64(transportComboBox.SelectedValue);
            //purchaseDetails.additional = addLessAmtFinalTextBox.Text;
            purchaseDetails.billamount = Convert.ToDouble(billAmtTextBox.Text);
            //purchaseDetails.addLessDetail1 = Convert.ToDouble(addLessTextBox.Text);
            //purchaseDetails.addLessDetail2 = Convert.ToDouble(addLessAmtTextBox.Text);
            //purchaseDetails.caseNoDetail2 = caseNoTextBox2.Text;
            //purchaseDetails.cgstRate = Convert.ToDouble(cgstRateTextBox.Text);
            //purchaseDetails.discountOnAmount = Convert.ToDouble(discPerAmtTextBox.Text);
            //purchaseDetails.discountPercentage = Convert.ToDouble(discPerTextBox.Text);
            purchaseDetails.grossAmount = Convert.ToDouble(totalAmountTextBox.Text);
            //purchaseDetails.igstRate = Convert.ToDouble(igstRateTextBox.Text);
            //purchaseDetails.sgstRate = Convert.ToDouble(sgstRateTextBox.Text);
            purchaseDetails.duedays = Convert.ToInt64(dueDaysTextBox.Text);
            purchaseDetails.taxablevalue = purchaseDetails.billamount;
            if (purchaseDetails.cgst == null)
                purchaseDetails.cgst = 0;
            if (purchaseDetails.sgst == null)
                purchaseDetails.sgst = 0;
            if (purchaseDetails.igst == null)
                purchaseDetails.igst = 0;
            double dPurchaseDiscount = 0;
            double.TryParse(purchaseDetails.discount,out dPurchaseDiscount);

            for (int i=0;i < purchaseDataGridView.RowCount-1;i++)
            {
                double dCgstAmount = 0;
                double.TryParse(purchaseDataGridView.Rows[i].Cells[cgstAmountTextBoxColumn.Index].EditedFormattedValue.ToString(),out dCgstAmount);
                purchaseDetails.cgst = purchaseDetails.cgst + dCgstAmount;
                double dSgstAmount = 0;
                double.TryParse(purchaseDataGridView.Rows[i].Cells[sgstAmountTextBoxColumn.Index].EditedFormattedValue.ToString(), out dSgstAmount);
                purchaseDetails.sgst = purchaseDetails.sgst + dSgstAmount;
                double dIgstAmount = 0;
                double.TryParse(purchaseDataGridView.Rows[i].Cells[igstAmountTextBoxColumn.Index].EditedFormattedValue.ToString(), out dIgstAmount);
                purchaseDetails.igst = purchaseDetails.igst + dIgstAmount;
                
                double dDiscountAmount = 0;
                double.TryParse(purchaseDataGridView.Rows[i].Cells[discountAmountTextBoxColumn.Index].EditedFormattedValue.ToString(), out dDiscountAmount);
                dPurchaseDiscount = dPurchaseDiscount + dDiscountAmount;
                
            }
            purchaseDetails.taxablevalue = purchaseDetails.taxablevalue - purchaseDetails.cgst - purchaseDetails.sgst - purchaseDetails.igst;
            purchaseDetails.discount = Convert.ToString(dPurchaseDiscount);
            return purchaseDetails;

        }

        private void resetAllData()
        {
            purchaseDataGridView.Rows.Clear();
            remarkTextBox.Text = "";
            totalPcsTextBox.Text = "0";
            totalQtyTextBox.Text = "0";
            totalAmountTextBox.Text = "0";
            discPerTextBox.Text = "0";
            discPerAmtTextBox.Text = "0";
            addLessTextBox.Text = "0";
            addLessAmtTextBox.Text = "0";
            caseNoTextBox.Text = "";
            caseNoTextBox2.Text = "1";
            cgstRateTextBox.Text = "2.5";
            sgstRateTextBox.Text = "2.5";
            igstRateTextBox.Text = "5";
            lrnoTextBox.Text = "";
            billAmtTextBox.Text = "0";
            billNoTextBox.Focus();
        }

        private List<purchaseitem_details> getPurchaseItemDetails()
        {

            if (purchaseDataGridView.RowCount > 0)
            {
                List<purchaseitem_details> itemDetails = new List<purchaseitem_details>();
                for (int i = 0; i < purchaseDataGridView.RowCount - 1; i++)
                {
                    purchaseitem_details purchaseItem = new purchaseitem_details();
                    purchaseItem.purchaseitem_id = (long)(purchaseDataGridView.Rows[i].Cells[itemNameGridCombo.Index].Value);
                    if (purchaseDataGridView.Rows[i].Cells[hsncEditBoxGrid.Index].Value == null)
                        purchaseItem.hsncode = "";
                    else
                        purchaseItem.hsncode = purchaseDataGridView.Rows[i].Cells[hsncEditBoxGrid.Index].Value.ToString();
                    if (purchaseDataGridView.Rows[i].Cells[bundlesEditBoxGrid.Index].Value == null)
                        purchaseItem.bundles = "";
                    else
                        purchaseItem.bundles = purchaseDataGridView.Rows[i].Cells[bundlesEditBoxGrid.Index].Value.ToString();
                    purchaseItem.amount = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[amountTextBoxGrid.Index].Value.ToString());
                    purchaseItem.cut = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[cutEditBoxGrid.Index].Value.ToString());
                    purchaseItem.packing = purchaseDataGridView.Rows[i].Cells[packingComboGrid.Index].EditedFormattedValue.ToString();
                    purchaseItem.pcs = Convert.ToInt64(purchaseDataGridView.Rows[i].Cells[pcsTextBoxGrid.Index].Value.ToString());
                    purchaseItem.quantity = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[qtyEditBoxGid.Index].Value.ToString());
                    purchaseItem.rate = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[rateTextBoxGrid.Index].Value.ToString());
                    //purchaseItem.purchasebillid = Convert.ToInt64(vouvherNoTextBox.Text);
                    purchaseItem.unit = purchaseDataGridView.Rows[i].Cells[unitComboGrid.Index].EditedFormattedValue.ToString();
                    itemDetails.Add(purchaseItem);
                    //itemDetails[i].
                }
                return itemDetails;
            }
            else
                return null;
        }

        private List<purchaseitem_details> getPurchaseItemDataForAllPurchases()
        {
            if (purchaseDataGridView.RowCount > 0)
            {
                List<purchaseitem_details> itemDetails = new List<purchaseitem_details>();
                for (int i = 0; i < purchaseDataGridView.RowCount - 1; i++)
                {
                    purchaseitem_details purchaseItem = new purchaseitem_details();
                    purchaseItem.purchaseitem_id = (long)(purchaseDataGridView.Rows[i].Cells[itemNameGridCombo.Index].Value);
                    if (purchaseDataGridView.Rows[i].Cells[hsncEditBoxGrid.Index].Value == null)
                        purchaseItem.hsncode = "";
                    else
                        purchaseItem.hsncode = purchaseDataGridView.Rows[i].Cells[hsncEditBoxGrid.Index].Value.ToString();
                    if (purchaseDataGridView.Rows[i].Cells[bundlesEditBoxGrid.Index].Value == null)
                        purchaseItem.bundles = "";
                    else
                        purchaseItem.bundles = purchaseDataGridView.Rows[i].Cells[bundlesEditBoxGrid.Index].Value.ToString();
                    purchaseItem.amount = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[amountTextBoxGrid.Index].Value.ToString());
                    purchaseItem.cut = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[cutEditBoxGrid.Index].Value.ToString());
                    purchaseItem.packing = purchaseDataGridView.Rows[i].Cells[packingComboGrid.Index].EditedFormattedValue.ToString();
                    purchaseItem.pcs = Convert.ToInt64(purchaseDataGridView.Rows[i].Cells[pcsTextBoxGrid.Index].Value.ToString());
                    purchaseItem.quantity = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[qtyEditBoxGid.Index].Value.ToString());
                    purchaseItem.rate = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[rateTextBoxGrid.Index].Value.ToString());
                    //purchaseItem.purchasebillid = Convert.ToInt64(vouvherNoTextBox.Text);
                    purchaseItem.unit = purchaseDataGridView.Rows[i].Cells[unitComboGrid.Index].EditedFormattedValue.ToString();
                    purchaseItem.rdAmount = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[rdAmountTextBoxColumn.Index].EditedFormattedValue.ToString());
                    purchaseItem.discount = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[discountAmountTextBoxColumn.Index].EditedFormattedValue.ToString());
                    purchaseItem.cgstRate = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[cgstPerTextBoxColumn.Index].EditedFormattedValue.ToString());
                    purchaseItem.cgstAmount = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[cgstAmountTextBoxColumn.Index].EditedFormattedValue.ToString());
                    purchaseItem.sgstRate = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[sgstPerTextBoxColumn.Index].EditedFormattedValue.ToString());
                    purchaseItem.sgstAmount = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[sgstAmountTextBoxColumn.Index].EditedFormattedValue.ToString());
                    purchaseItem.igstRate = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[igstPerTextBoxColumn.Index].EditedFormattedValue.ToString());
                    purchaseItem.igstAmount = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[igstAmountTextBoxColumn.Index].EditedFormattedValue.ToString());
                    purchaseItem.addLess = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[addLessTextBoxColumn.Index].EditedFormattedValue.ToString());
                    purchaseItem.totalAmount = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[totalAmountTextBoxColumn.Index].EditedFormattedValue.ToString());
                    purchaseItem.taxableValue = purchaseItem.totalAmount - purchaseItem.cgstAmount - purchaseItem.sgstAmount - purchaseItem.igstAmount;
                    itemDetails.Add(purchaseItem);
                    //itemDetails[i].
                }
                return itemDetails;
            }
            else
                return null;
        }

        private void openBillButton_Click(object sender, EventArgs e)
        {
            long lVoucherNo = 0;
            bool bValidVoucherNo = long.TryParse(vouvherNoTextBox.Text,out lVoucherNo);
            if (!bValidVoucherNo)
            {
                MessageBox.Show("Please enter a valid voucher No.", "Voucher Error", MessageBoxButtons.OK);
                vouvherNoTextBox.Focus();
                vouvherNoTextBox.SelectAll();
                return;
            }
            long lPurchaseTypeID = Convert.ToInt64(purchaseTypeComboBox.SelectedValue);
            List<purchaseitem_details> purchaseItemDetails;
            try
            {
                var db = DBHelper2.GetDbContext(Properties.Settings.Default.FinancialYearDbName);
                purchaseBillDetails = db.purchase_details.Where(x => x.voucherNo == lVoucherNo).Where(x=>x.purchase_typeid == lPurchaseTypeID).SingleOrDefault();
                //salesDetails = getSalesDataFromControl(salesDetails);
                if (purchaseBillDetails == null)
                {
                    DialogResult result = MessageBox.Show("Voucher No. does not Exists.Do you want to enter a new voucher?", "Bill Error", MessageBoxButtons.YesNo);
                    if (result == DialogResult.Yes)
                    {
                        resetAllData();
                        setDafaultData();
                        billNoTextBox.SelectAll();
                        return;
                    }
                    billNoTextBox.Focus();
                    billNoTextBox.SelectAll();
                    return;
                }
                purchaseItemDetails = purchaseBillDetails.purchaseitem_details.ToList();

                db.Dispose();

            }
            catch (Exception ex)
            {
                return;
            }
            //saleBillDetail = partyDB.getSalesBillDetail(Convert.ToInt64(billNoTextBox.Text));

            vouvherNoTextBox.Text = Convert.ToString(purchaseBillDetails.voucherNo);
            //orderNoTextBox.Text = saleBillDetail.orderno;
            DateTime dt, dt1;
            bool bFlag = DateTime.TryParse(purchaseBillDetails.date.ToString(), out dt);
            if (bFlag)
            {
                dt1 = dt.Date;
                //DateTime.Parse(dt1.ToString()).ToString("dd-MM-yyyy");
                dateTextBox.Text = dt1.ToString("dd-MM-yyyy");
            }


            //dateTextBox.Text = saleBillDetail.date.ToString();
            billNoTextBox.Text = Convert.ToString(purchaseBillDetails.billno);
            setPartyComboBoxSelectedValue(Convert.ToInt64(purchaseBillDetails.partyid));
            if ((long)purchaseTypeComboBox.SelectedValue == 6 || (long)purchaseTypeComboBox.SelectedValue == 7)
                refBillNoComboBox.Text = purchaseBillDetails.refBillNo;
            //partyComboBox.SelectedValue = purchaseBillDetails.partyid;
            stateComboBox.SelectedValue = purchaseBillDetails.stationcode;
            gstTypeComboBox.SelectedValue = purchaseBillDetails.purchase_gsttypeid;
            stationComboBox.SelectedValue = purchaseBillDetails.station;
            brokerComboBox.SelectedValue = purchaseBillDetails.brokerid;
            transportComboBox.SelectedValue = purchaseBillDetails.transportid;
            lrnoTextBox.Text = purchaseBillDetails.lrno;
            remarkTextBox.Text = purchaseBillDetails.remark;
            discPerTextBox.Text = Convert.ToString(purchaseBillDetails.discountPercentage);
            discPerAmtTextBox.Text = Convert.ToString(purchaseBillDetails.discountOnAmount);
            discPerAmtFinalTextBox.Text = Convert.ToString(purchaseBillDetails.discount);
            addLessTextBox.Text = Convert.ToString(purchaseBillDetails.addLessDetail1);
            addLessAmtTextBox.Text = Convert.ToString(purchaseBillDetails.addLessDetail2);
            addLessAmtFinalTextBox.Text = Convert.ToString(purchaseBillDetails.additional);
            cgstRateTextBox.Text = Convert.ToString(purchaseBillDetails.cgstRate);
            cgstAmtTextBox.Text = Convert.ToString(purchaseBillDetails.cgst);
            sgstRateTextBox.Text = Convert.ToString(purchaseBillDetails.sgstRate);
            sgstAmtTextBox.Text = Convert.ToString(purchaseBillDetails.sgst);
            igstRateTextBox.Text = Convert.ToString(purchaseBillDetails.igstRate);
            igstAmtTextBox.Text = Convert.ToString(purchaseBillDetails.igst);
            caseNoTextBox.Text = purchaseBillDetails.caseno;
            caseNoTextBox2.Text = purchaseBillDetails.caseNoDetail2;
            taxValTextBox.Text = Convert.ToString(purchaseBillDetails.taxablevalue);
            billAmtTextBox.Text = Convert.ToString(purchaseBillDetails.billamount);
            totalAmountTextBox.Text = Convert.ToString(purchaseBillDetails.grossAmount);
            dueDaysTextBox.Text = Convert.ToString(purchaseBillDetails.duedays);

            //List<salesitem_details> saleItemDetails = partyDB.getItemDetailsOfSaleBill(Convert.ToInt64(billNoTextBox.Text));
            if (purchaseBillDetails.purchaseitem_details == null)
                return;
            else
                purchaseDataGridView.Rows.Clear();
            double dPcs = 0,dQuantity = 0;
            if((long)purchaseTypeComboBox.SelectedValue == 1 || (long)purchaseTypeComboBox.SelectedValue == 6)
            {
                for (int i = 0; i < purchaseItemDetails.Count; i++)
                {
                    purchaseDataGridView.Rows.Add();
                    purchaseDataGridView.Rows[i].Cells[itemNameGridCombo.Index].Value = purchaseItemDetails[i].purchaseitem_id;
                    purchaseDataGridView.Rows[i].Cells[bundlesEditBoxGrid.Index].Value = purchaseItemDetails[i].bundles;
                    purchaseDataGridView.Rows[i].Cells[hsncEditBoxGrid.Index].Value = purchaseItemDetails[i].hsncode;
                    purchaseDataGridView.Rows[i].Cells[packingComboGrid.Index].Value = purchaseItemDetails[i].packing;
                    //saleDataGridView.Rows[i].Cells[packingComboGrid.Index].Value = saleItemDetails[i].packing;
                    purchaseDataGridView.Rows[i].Cells[unitComboGrid.Index].Value = purchaseItemDetails[i].unit;
                    purchaseDataGridView.Rows[i].Cells[pcsTextBoxGrid.Index].Value = Convert.ToString(purchaseItemDetails[i].pcs);
                    dPcs = dPcs + Convert.ToDouble(purchaseItemDetails[i].pcs);
                    dQuantity = dQuantity + Convert.ToDouble(purchaseItemDetails[i].quantity);
                    purchaseDataGridView.Rows[i].Cells[cutEditBoxGrid.Index].Value = Convert.ToString(purchaseItemDetails[i].cut);
                    purchaseDataGridView.Rows[i].Cells[qtyEditBoxGid.Index].Value = Convert.ToString(purchaseItemDetails[i].quantity);
                    purchaseDataGridView.Rows[i].Cells[rateTextBoxGrid.Index].Value = Convert.ToString(purchaseItemDetails[i].rate);
                    purchaseDataGridView.Rows[i].Cells[amountTextBoxGrid.Index].Value = Convert.ToString(purchaseItemDetails[i].amount);

                }
            }
            else
            {
                for (int i = 0; i < purchaseItemDetails.Count; i++)
                {
                    purchaseDataGridView.Rows.Add();
                    purchaseDataGridView.Rows[i].Cells[itemNameGridCombo.Index].Value = purchaseItemDetails[i].purchaseitem_id;
                    purchaseDataGridView.Rows[i].Cells[bundlesEditBoxGrid.Index].Value = purchaseItemDetails[i].bundles;
                    purchaseDataGridView.Rows[i].Cells[hsncEditBoxGrid.Index].Value = purchaseItemDetails[i].hsncode;
                    purchaseDataGridView.Rows[i].Cells[packingComboGrid.Index].Value = purchaseItemDetails[i].packing;
                    //saleDataGridView.Rows[i].Cells[packingComboGrid.Index].Value = saleItemDetails[i].packing;
                    purchaseDataGridView.Rows[i].Cells[unitComboGrid.Index].Value = purchaseItemDetails[i].unit;
                    purchaseDataGridView.Rows[i].Cells[pcsTextBoxGrid.Index].Value = Convert.ToString(purchaseItemDetails[i].pcs);

                    purchaseDataGridView.Rows[i].Cells[cutEditBoxGrid.Index].Value = Convert.ToString(purchaseItemDetails[i].cut);
                    purchaseDataGridView.Rows[i].Cells[qtyEditBoxGid.Index].Value = Convert.ToString(purchaseItemDetails[i].quantity);
                    purchaseDataGridView.Rows[i].Cells[rateTextBoxGrid.Index].Value = Convert.ToString(purchaseItemDetails[i].rate);
                    purchaseDataGridView.Rows[i].Cells[amountTextBoxGrid.Index].Value = Convert.ToString(purchaseItemDetails[i].amount);
                    purchaseDataGridView.Rows[i].Cells[rdAmountTextBoxColumn.Index].Value = Convert.ToString(purchaseItemDetails[i].rdAmount);
                    purchaseDataGridView.Rows[i].Cells[discountAmountTextBoxColumn.Index].Value = Convert.ToString(purchaseItemDetails[i].discount);
                    purchaseDataGridView.Rows[i].Cells[cgstAmountTextBoxColumn.Index].Value = Convert.ToString(purchaseItemDetails[i].cgstAmount);
                    purchaseDataGridView.Rows[i].Cells[cgstPerTextBoxColumn.Index].Value = Convert.ToString(purchaseItemDetails[i].cgstRate);
                    purchaseDataGridView.Rows[i].Cells[sgstAmountTextBoxColumn.Index].Value = Convert.ToString(purchaseItemDetails[i].sgstAmount);
                    purchaseDataGridView.Rows[i].Cells[sgstPerTextBoxColumn.Index].Value = Convert.ToString(purchaseItemDetails[i].sgstRate);
                    purchaseDataGridView.Rows[i].Cells[igstAmountTextBoxColumn.Index].Value = Convert.ToString(purchaseItemDetails[i].igstAmount);
                    purchaseDataGridView.Rows[i].Cells[igstPerTextBoxColumn.Index].Value = Convert.ToString(purchaseItemDetails[i].igstRate);
                    purchaseDataGridView.Rows[i].Cells[addLessTextBoxColumn.Index].Value = Convert.ToString(purchaseItemDetails[i].addLess);
                    purchaseDataGridView.Rows[i].Cells[totalAmountTextBoxColumn.Index].Value = Convert.ToString(purchaseItemDetails[i].totalAmount);
                }
            }
            
            totalPcsTextBox.Text = Convert.ToString(dPcs);
            totalQtyTextBox.Text = Convert.ToString(dQuantity);
            button1.Enabled = true;
            deleteButton.Enabled = true;
            addNewButton.Enabled = false;
            vouvherNoTextBox.Focus();
            vouvherNoTextBox.SelectAll();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            long lVoucherNo = 0;
            bool bValidVoucherNo = long.TryParse(vouvherNoTextBox.Text, out lVoucherNo);
            if (!bValidVoucherNo)
            {
                MessageBox.Show("Please enter a valid Voucher No.", "Voucher Error", MessageBoxButtons.OK);
                vouvherNoTextBox.Focus();
                return;
            }
            if (billNoTextBox.Text.Length <= 0)
            {
                MessageBox.Show("Please enter a valid Bill No.", "Bill Error", MessageBoxButtons.OK);
                billNoTextBox.Focus();
                return;
            }

            /*bool bFlag = partyDB.checkPurchaseBillExists(lVoucherNo,Convert.ToInt64(billNoTextBox.Text), Convert.ToInt64(partyComboBox["PARTY_ID"].ToString()));
            if (!bFlag)
            {
                DialogResult result = MessageBox.Show("Bill No. of same party already Exists!", "Bill Error", MessageBoxButtons.OK);
                
                billNoTextBox.Focus();
                billNoTextBox.SelectAll();
                return;
            }*/
            if(purchaseBillDetails.voucherNo != lVoucherNo)
            {
                MessageBox.Show("You are not allowed to change the Voucher No.", "ALERT", MessageBoxButtons.OK);
                vouvherNoTextBox.Text = Convert.ToString(purchaseBillDetails.voucherNo);
                vouvherNoTextBox.Focus();
                return;
            }
            bool bPurchaseBillAdded = false;
            long lBillNo = Convert.ToInt64(billNoTextBox.Text);

            if((long)purchaseTypeComboBox.SelectedValue == 1 || (long)purchaseTypeComboBox.SelectedValue == 6)
            {
                try
                {
                    //var db = DBHelper2.GetDbContext(Properties.Settings.Default.FinancialYearDbName);
                    //sales_details salesDetails = db.sales_details.Where(x => x.billno == lBillNo).SingleOrDefault();
                    purchaseBillDetails = getPurchaseDataFromControl(purchaseBillDetails);

                    purchaseBillDetails.purchaseitem_details = getPurchaseItemDetailsForPurchaseBillUpdate(purchaseBillDetails.purchaseitem_details.ToList());

                    //db.Dispose();
                    bPurchaseBillAdded = partyDB.updatePurchaseBill(purchaseBillDetails);
                }
                catch (Exception ex)
                {
                    return;
                }
            }
            else
            {
                try
                {
                    //var db = DBHelper2.GetDbContext(Properties.Settings.Default.FinancialYearDbName);
                    //sales_details salesDetails = db.sales_details.Where(x => x.billno == lBillNo).SingleOrDefault();
                    purchaseBillDetails = getAllTypesPurchaseDataFromControl(purchaseBillDetails);

                    purchaseBillDetails.purchaseitem_details = getPurchaseItemDetailsForAllTypesPurchaseBillUpdate(purchaseBillDetails.purchaseitem_details.ToList());

                    //db.Dispose();
                    bPurchaseBillAdded = partyDB.updatePurchaseBill(purchaseBillDetails);
                }
                catch (Exception ex)
                {
                    return;
                }
            }
            /*sales_details salesDetails = partyDB.getSalesBillDetail(Convert.ToInt64(vNoTextBox.Text));
            salesDetails = getSalesDataFromControl(salesDetails);
            salesDetails.salesitem_details = getSalesItemDetailsForSaleBillUpdate(salesDetails.salesitem_details.ToList());
            sales_details salesDetails = new sales_details();
            salesDetails = getSalesDataFromControl(salesDetails);
            salesDetails.salesbillid = Convert.ToInt64(vNoTextBox.Text);
            salesDetails.salesitem_details = getSalesItemDetails();
            bool bSalesBillAdded = partyDB.updateSalesBill(salesDetails);*/
            if (bPurchaseBillAdded)
            {
                MessageBox.Show("Purchase Bill updated Successfully", "SALES", MessageBoxButtons.OK);
                resetAllData();
                setDafaultData();
                billNoTextBox.SelectAll();

            }
            else
                MessageBox.Show("Purchase Bill NOT updated", "ERROR", MessageBoxButtons.OK);
        }

        private List<purchaseitem_details> getPurchaseItemDetailsForPurchaseBillUpdate(List<purchaseitem_details> itemDetails)
        {
            if (purchaseDataGridView.RowCount > 0)
            {
                //List<salesitem_details> itemDetails = new List<salesitem_details>();


                for (int i = 0; i < purchaseDataGridView.RowCount - 1; i++)
                {
                    if (i >= itemDetails.Count)
                    {
                        purchaseitem_details purchaseItem = new purchaseitem_details();
                        purchaseItem.purchaseitem_id = (long)(purchaseDataGridView.Rows[i].Cells[itemNameGridCombo.Index].Value);
                        if (purchaseDataGridView.Rows[i].Cells[hsncEditBoxGrid.Index].Value == null)
                            purchaseItem.hsncode = "";
                        else
                            purchaseItem.hsncode = purchaseDataGridView.Rows[i].Cells[hsncEditBoxGrid.Index].Value.ToString();
                        if (purchaseDataGridView.Rows[i].Cells[bundlesEditBoxGrid.Index].Value == null)
                            purchaseItem.bundles = "";
                        else
                            purchaseItem.bundles = purchaseDataGridView.Rows[i].Cells[bundlesEditBoxGrid.Index].Value.ToString();
                        purchaseItem.amount = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[amountTextBoxGrid.Index].Value.ToString());
                        purchaseItem.cut = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[cutEditBoxGrid.Index].Value.ToString());
                        purchaseItem.packing = purchaseDataGridView.Rows[i].Cells[packingComboGrid.Index].EditedFormattedValue.ToString();
                        purchaseItem.pcs = Convert.ToInt64(purchaseDataGridView.Rows[i].Cells[pcsTextBoxGrid.Index].Value.ToString());
                        purchaseItem.quantity = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[qtyEditBoxGid.Index].Value.ToString());
                        purchaseItem.rate = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[rateTextBoxGrid.Index].Value.ToString());
                        //purchaseItem.purchasebillid = Convert.ToInt64(vouvherNoTextBox.Text);
                        purchaseItem.unit = purchaseDataGridView.Rows[i].Cells[unitComboGrid.Index].EditedFormattedValue.ToString();
                        //if (itemDetails[i].item_id == 0)
                        itemDetails.Add(purchaseItem);
                    }
                    else
                    {
                        if (itemDetails[i].item_id == 0)
                        {
                            purchaseitem_details purchaseItem = new purchaseitem_details();
                            purchaseItem.purchaseitem_id = (long)(purchaseDataGridView.Rows[i].Cells[itemNameGridCombo.Index].Value);
                            if (purchaseDataGridView.Rows[i].Cells[hsncEditBoxGrid.Index].Value == null)
                                purchaseItem.hsncode = "";
                            else
                                purchaseItem.hsncode = purchaseDataGridView.Rows[i].Cells[hsncEditBoxGrid.Index].Value.ToString();
                            if (purchaseDataGridView.Rows[i].Cells[bundlesEditBoxGrid.Index].Value == null)
                                purchaseItem.bundles = "";
                            else
                                purchaseItem.bundles = purchaseDataGridView.Rows[i].Cells[bundlesEditBoxGrid.Index].Value.ToString();
                            purchaseItem.amount = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[amountTextBoxGrid.Index].Value.ToString());
                            purchaseItem.cut = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[cutEditBoxGrid.Index].Value.ToString());
                            purchaseItem.packing = purchaseDataGridView.Rows[i].Cells[packingComboGrid.Index].EditedFormattedValue.ToString();
                            purchaseItem.pcs = Convert.ToInt64(purchaseDataGridView.Rows[i].Cells[pcsTextBoxGrid.Index].Value.ToString());
                            purchaseItem.quantity = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[qtyEditBoxGid.Index].Value.ToString());
                            purchaseItem.rate = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[rateTextBoxGrid.Index].Value.ToString());
                            //purchaseItem.purchasebillid = Convert.ToInt64(vouvherNoTextBox.Text);
                            purchaseItem.unit = purchaseDataGridView.Rows[i].Cells[unitComboGrid.Index].EditedFormattedValue.ToString();
                            if (itemDetails[i].item_id == 0)
                                itemDetails.Add(purchaseItem);
                        }
                        else
                        {

                            itemDetails[i].purchaseitem_id = (long)(purchaseDataGridView.Rows[i].Cells[itemNameGridCombo.Index].Value);
                            if (purchaseDataGridView.Rows[i].Cells[hsncEditBoxGrid.Index].Value == null)
                                itemDetails[i].hsncode = "";
                            else
                                itemDetails[i].hsncode = purchaseDataGridView.Rows[i].Cells[hsncEditBoxGrid.Index].Value.ToString();
                            if (purchaseDataGridView.Rows[i].Cells[bundlesEditBoxGrid.Index].Value == null)
                                itemDetails[i].bundles = "";
                            else
                                itemDetails[i].bundles = purchaseDataGridView.Rows[i].Cells[bundlesEditBoxGrid.Index].Value.ToString();
                            itemDetails[i].amount = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[amountTextBoxGrid.Index].Value.ToString());
                            itemDetails[i].cut = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[cutEditBoxGrid.Index].Value.ToString());
                            itemDetails[i].packing = purchaseDataGridView.Rows[i].Cells[packingComboGrid.Index].EditedFormattedValue.ToString();
                            itemDetails[i].pcs = Convert.ToInt64(purchaseDataGridView.Rows[i].Cells[pcsTextBoxGrid.Index].Value.ToString());
                            itemDetails[i].quantity = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[qtyEditBoxGid.Index].Value.ToString());
                            itemDetails[i].rate = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[rateTextBoxGrid.Index].Value.ToString());
                            //itemDetails[i].purchasebillid = Convert.ToInt64(vouvherNoTextBox.Text);
                            itemDetails[i].unit = purchaseDataGridView.Rows[i].Cells[unitComboGrid.Index].EditedFormattedValue.ToString();

                        }
                    }


                    //itemDetails[i].
                }
                return itemDetails;
            }
            else
                return null;
        }

        private List<purchaseitem_details> getPurchaseItemDetailsForAllTypesPurchaseBillUpdate(List<purchaseitem_details> itemDetails)
        {
            if (purchaseDataGridView.RowCount > 0)
            {
                //List<salesitem_details> itemDetails = new List<salesitem_details>();


                for (int i = 0; i < purchaseDataGridView.RowCount - 1; i++)
                {
                    if (i >= itemDetails.Count)
                    {
                        purchaseitem_details purchaseItem = new purchaseitem_details();
                        purchaseItem.purchaseitem_id = (long)(purchaseDataGridView.Rows[i].Cells[itemNameGridCombo.Index].Value);
                        if (purchaseDataGridView.Rows[i].Cells[hsncEditBoxGrid.Index].Value == null)
                            purchaseItem.hsncode = "";
                        else
                            purchaseItem.hsncode = purchaseDataGridView.Rows[i].Cells[hsncEditBoxGrid.Index].Value.ToString();
                        if (purchaseDataGridView.Rows[i].Cells[bundlesEditBoxGrid.Index].Value == null)
                            purchaseItem.bundles = "";
                        else
                            purchaseItem.bundles = purchaseDataGridView.Rows[i].Cells[bundlesEditBoxGrid.Index].Value.ToString();
                        purchaseItem.amount = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[amountTextBoxGrid.Index].Value.ToString());
                        purchaseItem.cut = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[cutEditBoxGrid.Index].Value.ToString());
                        purchaseItem.packing = purchaseDataGridView.Rows[i].Cells[packingComboGrid.Index].EditedFormattedValue.ToString();
                        purchaseItem.pcs = Convert.ToInt64(purchaseDataGridView.Rows[i].Cells[pcsTextBoxGrid.Index].Value.ToString());
                        purchaseItem.quantity = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[qtyEditBoxGid.Index].Value.ToString());
                        purchaseItem.rate = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[rateTextBoxGrid.Index].Value.ToString());
                        //purchaseItem.purchasebillid = Convert.ToInt64(vouvherNoTextBox.Text);
                        purchaseItem.unit = purchaseDataGridView.Rows[i].Cells[unitComboGrid.Index].EditedFormattedValue.ToString();

                        purchaseItem.rdAmount = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[rdAmountTextBoxColumn.Index].EditedFormattedValue.ToString());
                        purchaseItem.discount = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[discountAmountTextBoxColumn.Index].EditedFormattedValue.ToString());
                        purchaseItem.cgstRate = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[cgstPerTextBoxColumn.Index].EditedFormattedValue.ToString());
                        purchaseItem.cgstAmount = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[cgstAmountTextBoxColumn.Index].EditedFormattedValue.ToString());
                        purchaseItem.sgstRate = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[sgstPerTextBoxColumn.Index].EditedFormattedValue.ToString());
                        purchaseItem.sgstAmount = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[sgstAmountTextBoxColumn.Index].EditedFormattedValue.ToString());
                        purchaseItem.igstRate = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[igstPerTextBoxColumn.Index].EditedFormattedValue.ToString());
                        purchaseItem.igstAmount = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[igstAmountTextBoxColumn.Index].EditedFormattedValue.ToString());
                        purchaseItem.addLess = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[addLessTextBoxColumn.Index].EditedFormattedValue.ToString());
                        purchaseItem.totalAmount = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[totalAmountTextBoxColumn.Index].EditedFormattedValue.ToString());
                        purchaseItem.taxableValue = purchaseItem.totalAmount - purchaseItem.sgstAmount - purchaseItem.cgstAmount - purchaseItem.igstAmount;
                        //if (itemDetails[i].item_id == 0)
                        itemDetails.Add(purchaseItem);
                    }
                    else
                    {
                        if (itemDetails[i].item_id == 0)
                        {
                            purchaseitem_details purchaseItem = new purchaseitem_details();
                            purchaseItem.purchaseitem_id = (long)(purchaseDataGridView.Rows[i].Cells[itemNameGridCombo.Index].Value);
                            if (purchaseDataGridView.Rows[i].Cells[hsncEditBoxGrid.Index].Value == null)
                                purchaseItem.hsncode = "";
                            else
                                purchaseItem.hsncode = purchaseDataGridView.Rows[i].Cells[hsncEditBoxGrid.Index].Value.ToString();
                            if (purchaseDataGridView.Rows[i].Cells[bundlesEditBoxGrid.Index].Value == null)
                                purchaseItem.bundles = "";
                            else
                                purchaseItem.bundles = purchaseDataGridView.Rows[i].Cells[bundlesEditBoxGrid.Index].Value.ToString();
                            purchaseItem.amount = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[amountTextBoxGrid.Index].Value.ToString());
                            purchaseItem.cut = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[cutEditBoxGrid.Index].Value.ToString());
                            purchaseItem.packing = purchaseDataGridView.Rows[i].Cells[packingComboGrid.Index].EditedFormattedValue.ToString();
                            purchaseItem.pcs = Convert.ToInt64(purchaseDataGridView.Rows[i].Cells[pcsTextBoxGrid.Index].Value.ToString());
                            purchaseItem.quantity = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[qtyEditBoxGid.Index].Value.ToString());
                            purchaseItem.rate = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[rateTextBoxGrid.Index].Value.ToString());
                            //purchaseItem.purchasebillid = Convert.ToInt64(vouvherNoTextBox.Text);
                            purchaseItem.unit = purchaseDataGridView.Rows[i].Cells[unitComboGrid.Index].EditedFormattedValue.ToString();

                            purchaseItem.rdAmount = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[rdAmountTextBoxColumn.Index].EditedFormattedValue.ToString());
                            purchaseItem.discount = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[discountAmountTextBoxColumn.Index].EditedFormattedValue.ToString());
                            purchaseItem.cgstRate = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[cgstPerTextBoxColumn.Index].EditedFormattedValue.ToString());
                            purchaseItem.cgstAmount = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[cgstAmountTextBoxColumn.Index].EditedFormattedValue.ToString());
                            purchaseItem.sgstRate = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[sgstPerTextBoxColumn.Index].EditedFormattedValue.ToString());
                            purchaseItem.sgstAmount = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[sgstAmountTextBoxColumn.Index].EditedFormattedValue.ToString());
                            purchaseItem.igstRate = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[igstPerTextBoxColumn.Index].EditedFormattedValue.ToString());
                            purchaseItem.igstAmount = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[igstAmountTextBoxColumn.Index].EditedFormattedValue.ToString());
                            purchaseItem.addLess = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[addLessTextBoxColumn.Index].EditedFormattedValue.ToString());
                            purchaseItem.totalAmount = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[totalAmountTextBoxColumn.Index].EditedFormattedValue.ToString());
                            purchaseItem.taxableValue = purchaseItem.totalAmount - purchaseItem.sgstAmount - purchaseItem.cgstAmount - purchaseItem.igstAmount;
                            if (itemDetails[i].item_id == 0)
                                itemDetails.Add(purchaseItem);
                        }
                        else
                        {

                            itemDetails[i].purchaseitem_id = (long)(purchaseDataGridView.Rows[i].Cells[itemNameGridCombo.Index].Value);
                            if (purchaseDataGridView.Rows[i].Cells[hsncEditBoxGrid.Index].Value == null)
                                itemDetails[i].hsncode = "";
                            else
                                itemDetails[i].hsncode = purchaseDataGridView.Rows[i].Cells[hsncEditBoxGrid.Index].Value.ToString();
                            if (purchaseDataGridView.Rows[i].Cells[bundlesEditBoxGrid.Index].Value == null)
                                itemDetails[i].bundles = "";
                            else
                                itemDetails[i].bundles = purchaseDataGridView.Rows[i].Cells[bundlesEditBoxGrid.Index].Value.ToString();
                            itemDetails[i].amount = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[amountTextBoxGrid.Index].Value.ToString());
                            itemDetails[i].cut = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[cutEditBoxGrid.Index].Value.ToString());
                            itemDetails[i].packing = purchaseDataGridView.Rows[i].Cells[packingComboGrid.Index].EditedFormattedValue.ToString();
                            itemDetails[i].pcs = Convert.ToInt64(purchaseDataGridView.Rows[i].Cells[pcsTextBoxGrid.Index].Value.ToString());
                            itemDetails[i].quantity = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[qtyEditBoxGid.Index].Value.ToString());
                            itemDetails[i].rate = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[rateTextBoxGrid.Index].Value.ToString());
                            //itemDetails[i].purchasebillid = Convert.ToInt64(vouvherNoTextBox.Text);
                            itemDetails[i].unit = purchaseDataGridView.Rows[i].Cells[unitComboGrid.Index].EditedFormattedValue.ToString();

                            itemDetails[i].rdAmount = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[rdAmountTextBoxColumn.Index].EditedFormattedValue.ToString());
                            itemDetails[i].discount = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[discountAmountTextBoxColumn.Index].EditedFormattedValue.ToString());
                            itemDetails[i].cgstRate = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[cgstPerTextBoxColumn.Index].EditedFormattedValue.ToString());
                            itemDetails[i].cgstAmount = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[cgstAmountTextBoxColumn.Index].EditedFormattedValue.ToString());
                            itemDetails[i].sgstRate = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[sgstPerTextBoxColumn.Index].EditedFormattedValue.ToString());
                            itemDetails[i].sgstAmount = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[sgstAmountTextBoxColumn.Index].EditedFormattedValue.ToString());
                            itemDetails[i].igstRate = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[igstPerTextBoxColumn.Index].EditedFormattedValue.ToString());
                            itemDetails[i].igstAmount = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[igstAmountTextBoxColumn.Index].EditedFormattedValue.ToString());
                            itemDetails[i].addLess = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[addLessTextBoxColumn.Index].EditedFormattedValue.ToString());
                            itemDetails[i].totalAmount = Convert.ToDouble(purchaseDataGridView.Rows[i].Cells[totalAmountTextBoxColumn.Index].EditedFormattedValue.ToString());
                            itemDetails[i].taxableValue = itemDetails[i].totalAmount - itemDetails[i].sgstAmount - itemDetails[i].cgstAmount - itemDetails[i].igstAmount;

                        }
                    }


                    //itemDetails[i].
                }
                return itemDetails;
            }
            else
                return null;
        }

        private void dueDaysTextBox_TextChanged(object sender, EventArgs e)
        {
            long lDueDays = 0;
            bool bFlag = long.TryParse(dueDaysTextBox.Text, out lDueDays);
            if (!bFlag)
                dueDaysTextBox.Text = "0";
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            if (vouvherNoTextBox.Text.Length <= 0)
            {
                MessageBox.Show("Please enter a valid voucher No.", "Bill Error", MessageBoxButtons.OK);
                vouvherNoTextBox.Focus();
                return;
            }
            
            DialogResult result = MessageBox.Show("Are you sure you want to delete the purchase bill", "DELETE BILL", MessageBoxButtons.YesNo);
            if (result == DialogResult.No)
                return;

            bool bFlag = partyDB.deletePurchaseBill(Convert.ToInt64(vouvherNoTextBox.Text), Convert.ToInt64(purchaseTypeComboBox.SelectedValue));
            if (bFlag)
            {
                MessageBox.Show("Voucher No. " + vouvherNoTextBox.Text + " deleted successfully.", "Delete Bill", MessageBoxButtons.OK);
                resetAllData();
                setDafaultData();
            }
            else
                MessageBox.Show("Bill No. " + vouvherNoTextBox.Text + " not deleted successfully.", "Delete Bill", MessageBoxButtons.OK);

        }

        private void vouvherNoTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                long lVoucherNo = 0;
                bool bFlag = long.TryParse(vouvherNoTextBox.Text, out lVoucherNo);
                if(!bFlag)
                {
                    MessageBox.Show("Please enter a valid voucher No.", "Voucher Error", MessageBoxButtons.OK);
                    vouvherNoTextBox.Focus();
                    vouvherNoTextBox.SelectAll();
                    return;
                }
                List<purchaseitem_details> purchaseItemDetails;
                long lPurchaseTypeID = Convert.ToInt64(purchaseTypeComboBox.SelectedValue);
                try
                {
                    var db = DBHelper2.GetDbContext(Properties.Settings.Default.FinancialYearDbName);
                    purchaseBillDetails = db.purchase_details.Where(x => x.voucherNo == lVoucherNo).Where(x=>x.purchase_typeid == lPurchaseTypeID).SingleOrDefault();
                    //salesDetails = getSalesDataFromControl(salesDetails);
                    if (purchaseBillDetails == null)
                    {
                        DialogResult result = MessageBox.Show("Voucher No. does not Exists.Do you want to enter a new voucher?", "Voucher Error", MessageBoxButtons.YesNo);
                        if (result == DialogResult.Yes)
                        {
                            resetAllData();
                            setDafaultData();
                            vouvherNoTextBox.SelectAll();
                            return;
                        }
                        vouvherNoTextBox.Focus();
                        vouvherNoTextBox.SelectAll();
                        return;
                    }
                    purchaseItemDetails = purchaseBillDetails.purchaseitem_details.ToList();

                    db.Dispose();

                }
                catch (Exception ex)
                {
                    return;
                }

                vouvherNoTextBox.Text = Convert.ToString(purchaseBillDetails.voucherNo);
                //orderNoTextBox.Text = saleBillDetail.orderno;
                DateTime dt, dt1;
                bFlag = DateTime.TryParse(purchaseBillDetails.date.ToString(), out dt);
                if (bFlag)
                {
                    dt1 = dt.Date;
                    //DateTime.Parse(dt1.ToString()).ToString("dd-MM-yyyy");
                    dateTextBox.Text = dt1.ToString("dd-MM-yyyy");
                }


                //dateTextBox.Text = saleBillDetail.date.ToString();
                try
                {
                    billNoTextBox.Text = Convert.ToString(purchaseBillDetails.billno);
                    setPartyComboBoxSelectedValue(Convert.ToInt64(purchaseBillDetails.partyid));
                    if ((long)purchaseTypeComboBox.SelectedValue == 6 || (long)purchaseTypeComboBox.SelectedValue == 7)
                        refBillNoComboBox.Text = purchaseBillDetails.refBillNo;
                    //partyComboBox.SelectedValue = purchaseBillDetails.partyid;
                    stationComboBox.SelectedValue = purchaseBillDetails.station;
                    gstTypeComboBox.SelectedValue = purchaseBillDetails.purchase_gsttypeid;
                    stateComboBox.SelectedValue = purchaseBillDetails.stationcode;
                    brokerComboBox.SelectedValue = purchaseBillDetails.brokerid;
                    transportComboBox.SelectedValue = purchaseBillDetails.transportid;
                    lrnoTextBox.Text = purchaseBillDetails.lrno;
                    remarkTextBox.Text = purchaseBillDetails.remark;
                    discPerTextBox.Text = Convert.ToString(purchaseBillDetails.discountPercentage);
                    discPerAmtTextBox.Text = Convert.ToString(purchaseBillDetails.discountOnAmount);
                    discPerAmtFinalTextBox.Text = Convert.ToString(purchaseBillDetails.discount);
                    addLessTextBox.Text = Convert.ToString(purchaseBillDetails.addLessDetail1);
                    addLessAmtTextBox.Text = Convert.ToString(purchaseBillDetails.addLessDetail2);
                    addLessAmtFinalTextBox.Text = Convert.ToString(purchaseBillDetails.additional);
                    cgstRateTextBox.Text = Convert.ToString(purchaseBillDetails.cgstRate);
                    cgstAmtTextBox.Text = Convert.ToString(purchaseBillDetails.cgst);
                    sgstRateTextBox.Text = Convert.ToString(purchaseBillDetails.sgstRate);
                    sgstAmtTextBox.Text = Convert.ToString(purchaseBillDetails.sgst);
                    igstRateTextBox.Text = Convert.ToString(purchaseBillDetails.igstRate);
                    igstAmtTextBox.Text = Convert.ToString(purchaseBillDetails.igst);
                    caseNoTextBox.Text = purchaseBillDetails.caseno;
                    caseNoTextBox2.Text = purchaseBillDetails.caseNoDetail2;
                    dueDaysTextBox.Text = Convert.ToString(purchaseBillDetails.duedays);
                    //taxValTextBox.Text = Convert.ToString(saleBillDetail.taxablevalue);
                    billAmtTextBox.Text = Convert.ToString(purchaseBillDetails.billamount);
                    totalAmountTextBox.Text = Convert.ToString(purchaseBillDetails.grossAmount);
                }
                catch (Exception ex)
                {
                    return;
                }

                //List<salesitem_details> saleItemDetails = partyDB.getItemDetailsOfSaleBill(Convert.ToInt64(billNoTextBox.Text));
                //saleItemDetails = saleBillDetail.salesitem_details.ToList();
                if (purchaseItemDetails == null)
                    return;
                else
                    purchaseDataGridView.Rows.Clear();
                double dPcs = 0, dQuantity = 0;
                if((long)purchaseTypeComboBox.SelectedValue == 1 || (long)purchaseTypeComboBox.SelectedValue == 6)
                {
                    for (int i = 0; i < purchaseItemDetails.Count; i++)
                    {
                        purchaseDataGridView.Rows.Add();
                        purchaseDataGridView.Rows[i].Cells[itemNameGridCombo.Index].Value = purchaseItemDetails[i].purchaseitem_id;
                        purchaseDataGridView.Rows[i].Cells[bundlesEditBoxGrid.Index].Value = purchaseItemDetails[i].bundles;
                        purchaseDataGridView.Rows[i].Cells[hsncEditBoxGrid.Index].Value = purchaseItemDetails[i].hsncode;
                        purchaseDataGridView.Rows[i].Cells[packingComboGrid.Index].Value = purchaseItemDetails[i].packing;
                        //saleDataGridView.Rows[i].Cells[packingComboGrid.Index].Value = saleItemDetails[i].packing;
                        purchaseDataGridView.Rows[i].Cells[unitComboGrid.Index].Value = purchaseItemDetails[i].unit;
                        purchaseDataGridView.Rows[i].Cells[pcsTextBoxGrid.Index].Value = Convert.ToString(purchaseItemDetails[i].pcs);
                        dPcs = dPcs + Convert.ToDouble(purchaseItemDetails[i].pcs);
                        dQuantity = dQuantity + Convert.ToDouble(purchaseItemDetails[i].quantity);
                        purchaseDataGridView.Rows[i].Cells[cutEditBoxGrid.Index].Value = Convert.ToString(purchaseItemDetails[i].cut);
                        purchaseDataGridView.Rows[i].Cells[qtyEditBoxGid.Index].Value = Convert.ToString(purchaseItemDetails[i].quantity);
                        purchaseDataGridView.Rows[i].Cells[rateTextBoxGrid.Index].Value = Convert.ToString(purchaseItemDetails[i].rate);
                        purchaseDataGridView.Rows[i].Cells[amountTextBoxGrid.Index].Value = Convert.ToString(purchaseItemDetails[i].amount);

                    }
                }
                else
                {
                    for (int i = 0; i < purchaseItemDetails.Count; i++)
                    {
                        purchaseDataGridView.Rows.Add();
                        purchaseDataGridView.Rows[i].Cells[itemNameGridCombo.Index].Value = purchaseItemDetails[i].purchaseitem_id;
                        purchaseDataGridView.Rows[i].Cells[bundlesEditBoxGrid.Index].Value = purchaseItemDetails[i].bundles;
                        purchaseDataGridView.Rows[i].Cells[hsncEditBoxGrid.Index].Value = purchaseItemDetails[i].hsncode;
                        purchaseDataGridView.Rows[i].Cells[packingComboGrid.Index].Value = purchaseItemDetails[i].packing;
                        //saleDataGridView.Rows[i].Cells[packingComboGrid.Index].Value = saleItemDetails[i].packing;
                        purchaseDataGridView.Rows[i].Cells[unitComboGrid.Index].Value = purchaseItemDetails[i].unit;
                        purchaseDataGridView.Rows[i].Cells[pcsTextBoxGrid.Index].Value = Convert.ToString(purchaseItemDetails[i].pcs);
                        
                        purchaseDataGridView.Rows[i].Cells[cutEditBoxGrid.Index].Value = Convert.ToString(purchaseItemDetails[i].cut);
                        purchaseDataGridView.Rows[i].Cells[qtyEditBoxGid.Index].Value = Convert.ToString(purchaseItemDetails[i].quantity);
                        purchaseDataGridView.Rows[i].Cells[rateTextBoxGrid.Index].Value = Convert.ToString(purchaseItemDetails[i].rate);
                        purchaseDataGridView.Rows[i].Cells[amountTextBoxGrid.Index].Value = Convert.ToString(purchaseItemDetails[i].amount);
                        purchaseDataGridView.Rows[i].Cells[rdAmountTextBoxColumn.Index].Value = Convert.ToString(purchaseItemDetails[i].rdAmount);
                        purchaseDataGridView.Rows[i].Cells[discountAmountTextBoxColumn.Index].Value = Convert.ToString(purchaseItemDetails[i].discount);
                        purchaseDataGridView.Rows[i].Cells[cgstAmountTextBoxColumn.Index].Value = Convert.ToString(purchaseItemDetails[i].cgstAmount);
                        purchaseDataGridView.Rows[i].Cells[cgstPerTextBoxColumn.Index].Value = Convert.ToString(purchaseItemDetails[i].cgstRate);
                        purchaseDataGridView.Rows[i].Cells[sgstAmountTextBoxColumn.Index].Value = Convert.ToString(purchaseItemDetails[i].sgstAmount);
                        purchaseDataGridView.Rows[i].Cells[sgstPerTextBoxColumn.Index].Value = Convert.ToString(purchaseItemDetails[i].sgstRate);
                        purchaseDataGridView.Rows[i].Cells[igstAmountTextBoxColumn.Index].Value = Convert.ToString(purchaseItemDetails[i].igstAmount);
                        purchaseDataGridView.Rows[i].Cells[igstPerTextBoxColumn.Index].Value = Convert.ToString(purchaseItemDetails[i].igstRate);
                        purchaseDataGridView.Rows[i].Cells[addLessTextBoxColumn.Index].Value = Convert.ToString(purchaseItemDetails[i].addLess);
                        purchaseDataGridView.Rows[i].Cells[totalAmountTextBoxColumn.Index].Value = Convert.ToString(purchaseItemDetails[i].totalAmount);
                    }
                }
                totalPcsTextBox.Text = Convert.ToString(dPcs);
                totalQtyTextBox.Text = Convert.ToString(dQuantity);

                button1.Enabled = true;
                deleteButton.Enabled = true;
                addNewButton.Enabled = false;
                vouvherNoTextBox.Focus();
                vouvherNoTextBox.SelectAll();
            }
        }

        private void igstAmtTextBox_TextChanged(object sender, EventArgs e)
        {
            double dBillAmout = 0;
            double dIgstAmount = 0;
            bool bFlag = double.TryParse(igstAmtTextBox.Text, out dIgstAmount);
            if (bFlag)
                dBillAmout = dIgstAmount + Convert.ToDouble(taxValTextBox.Text);
            else
                dBillAmout = Convert.ToDouble(taxValTextBox.Text);
            if ((long)purchaseTypeComboBox.SelectedValue == 1 || (long)purchaseTypeComboBox.SelectedValue == 6)
                billAmtTextBox.Text = Convert.ToString(Math.Round(dBillAmout));
        }

        private void sgstAmtTextBox_TextChanged(object sender, EventArgs e)
        {
            double dSgstAmount = 0;
            bool bFlag = double.TryParse(sgstAmtTextBox.Text, out dSgstAmount);
            if (bFlag)
                dSgstAmount = (Convert.ToDouble(sgstAmtTextBox.Text));
            sgstAmtTextBox.Text = Convert.ToString(dSgstAmount);
            
            cgstAmtTextBox.Text = sgstAmtTextBox.Text;

            double dCgstAmount = Convert.ToDouble(cgstAmtTextBox.Text);
            double dTaxableValue = Convert.ToDouble(taxValTextBox.Text);
            if ((long)purchaseTypeComboBox.SelectedValue == 1 || (long)purchaseTypeComboBox.SelectedValue == 6)
                billAmtTextBox.Text = Convert.ToString(Math.Round(dCgstAmount + dSgstAmount + dTaxableValue));
        }

        private void cgstAmtTextBox_TextChanged(object sender, EventArgs e)
        {
            double dCgstAmount = 0;
            bool bFlag = double.TryParse(cgstAmtTextBox.Text, out dCgstAmount);
            if (bFlag)
                dCgstAmount = (Convert.ToDouble(cgstAmtTextBox.Text));
            cgstAmtTextBox.Text = Convert.ToString(dCgstAmount);

            sgstAmtTextBox.Text = cgstAmtTextBox.Text;

            double dSgstAmount = Convert.ToDouble(sgstAmtTextBox.Text);
            double dTaxableValue = Convert.ToDouble(taxValTextBox.Text);
            if ((long)purchaseTypeComboBox.SelectedValue == 1 || (long)purchaseTypeComboBox.SelectedValue == 6)
                billAmtTextBox.Text = Convert.ToString(Math.Round(dCgstAmount + dSgstAmount + dTaxableValue));
        }

        private void setPartyComboBoxSelectedValue(long lPartyID)
        {
            try
            {
                //partyComboBox.SelectedIndex = -1;
                if (lPartyID >= 0)
                {
                    for (int index = 0; index < partyComboBox.Items.Count; index++)
                    {
                        DataTable partyTable = partyComboBox.Data;
                        DataRow Row = partyTable.Rows[index];
                        if (Row != null)
                        {
                            if (Convert.ToInt64(Row["PARTY_ID"].ToString()) == lPartyID)
                            {
                                partyComboBox.SelectedIndex = -1;
                                partyComboBox.SelectedIndex = index;
                                break;
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                return;
            }
        }

        private void fillReferenceBillDetailsForReturn(long lPartyID)
        {
            List<purchase_details> purchaseBillList = partyDB.getPurchaseBillDetailFromPartyID(lPartyID);
            List<PassBookBillDataForMultiColumnDisplay> passBookMultiColunDisplayDataList = new List<PassBookBillDataForMultiColumnDisplay>();
            if (purchaseBillList == null)
                return;

            for (int i = 0; i < purchaseBillList.Count; i++)
            {
                PassBookBillDataForMultiColumnDisplay multiColumnDisplayBillData = new PassBookBillDataForMultiColumnDisplay();
                multiColumnDisplayBillData.billNo = purchaseBillList[i].billno;
                //multiColumnDisplayBillData.billType = Convert.ToInt64(saleBillList[i].salesTypeid);
                DateTime dt, dt1;
                string billDate = "";
                bool bFlag = DateTime.TryParse(purchaseBillList[i].date.ToString(), out dt);
                if (bFlag)
                {
                    dt1 = dt.Date;
                    //DateTime.Parse(dt1.ToString()).ToString("dd-MM-yyyy");
                    billDate = dt1.ToString("dd-MM-yyyy");
                }
                multiColumnDisplayBillData.date = billDate;
                multiColumnDisplayBillData.netAmount = Convert.ToDouble(purchaseBillList[i].billamount);
                multiColumnDisplayBillData.taxableValue = Convert.ToInt64(purchaseBillList[i].taxablevalue);
                List<passbook_itemdetails> billReceivedList = partyDB.getReceivedBillAmount(lPartyID, 2, Convert.ToString(purchaseBillList[i].billno));
                if (billReceivedList == null || billReceivedList.Count == 0)
                    multiColumnDisplayBillData.pendingAmount = multiColumnDisplayBillData.netAmount;
                else
                {
                    double dReceivedAmount = 0;
                    for (int j = 0; j < billReceivedList.Count; j++)
                    {
                        dReceivedAmount = dReceivedAmount + Convert.ToDouble(billReceivedList[j].discountAmount + billReceivedList[j].others + billReceivedList[j].paymentReceivedAmount + billReceivedList[j].rdAmount + billReceivedList[j].rgAmount + billReceivedList[j].tdsAmount + billReceivedList[j].addLess);
                    }
                    multiColumnDisplayBillData.pendingAmount = multiColumnDisplayBillData.netAmount - dReceivedAmount;
                }
                List<purchase_details> rgReceivedList = partyDB.getPurchaseRGAmount(Convert.ToString(purchaseBillList[i].billno), lPartyID);
                if (rgReceivedList != null)
                {
                    for (int j = 0; j < rgReceivedList.Count; j++)
                    {
                        multiColumnDisplayBillData.pendingAmount = multiColumnDisplayBillData.pendingAmount - Convert.ToDouble(rgReceivedList[j].billamount);
                    }
                }

                if (multiColumnDisplayBillData.pendingAmount > 0 && (purchaseBillList[i].purchase_typeid == 1 || purchaseBillList[i].purchase_typeid == 2 || purchaseBillList[i].purchase_typeid == 3 || purchaseBillList[i].purchase_typeid == 4 || purchaseBillList[i].purchase_typeid == 5))
                    passBookMultiColunDisplayDataList.Add(multiColumnDisplayBillData);
            }

            int iColumnCount = 4;


            DataTable myDataTable = new DataTable("ParentTable");
            // Declare variables for DataColumn and DataRow objects.
            DataColumn myDataColumn;
            DataRow myDataRow;

            //Add some coulumns
            for (int index = 0; index < iColumnCount; index++)
            {
                myDataColumn = new DataColumn();
                myDataColumn.DataType = typeof(string);
                if (index == 0)
                    myDataColumn.ColumnName = "DATE";
                else if (index == 1)
                    myDataColumn.ColumnName = "BILLNO";
                else if (index == 2)
                    myDataColumn.ColumnName = "NET AMOUNT";
                else if (index == 3)
                    myDataColumn.ColumnName = "PENDING AMOUNT";

                //myDataColumn.Caption = "ParentItem";
                myDataColumn.AutoIncrement = false;
                myDataColumn.ReadOnly = false;
                myDataColumn.Unique = false;
                // Add the Column to the DataColumnCollection.
                myDataTable.Columns.Add(myDataColumn);
            }


            //Add some more rows
            for (int index = 0; index < passBookMultiColunDisplayDataList.Count; index++)
            {
                myDataRow = myDataTable.NewRow();
                myDataRow[0] = passBookMultiColunDisplayDataList[index].date;
                myDataRow[1] = passBookMultiColunDisplayDataList[index].billNo;
                myDataRow[2] = passBookMultiColunDisplayDataList[index].netAmount;
                myDataRow[3] = passBookMultiColunDisplayDataList[index].pendingAmount;

                myDataTable.Rows.Add(myDataRow);
            }


            //Now set the Data of the ColumnComboBox
            refBillNoComboBox.DisplayMember = "BILLNO";
            refBillNoComboBox.ValueMember = "BILLNO";
            refBillNoComboBox.Data = myDataTable;
            //Set which row will be displayed in the text box
            //If you set this to a column that isn't displayed then the suggesting functionality won't work.
            refBillNoComboBox.ColumnSpacing = 50;
            refBillNoComboBox.ViewColumn = 1;

            refBillNoComboBox.Columns[0].Width = 50;
            refBillNoComboBox.Columns[1].Width = 50;
            refBillNoComboBox.Columns[2].Width = 80;
            refBillNoComboBox.Columns[3].Width = 80;
            //Set a few columns to not be shown
            refBillNoComboBox.SelectedIndex = -1;


        }

        public void setPurchaseTypeMode(long lPurchaseTypeID)
        {
            purchaseTypeComboBox.SelectedValue = lPurchaseTypeID;
        }
    }

}