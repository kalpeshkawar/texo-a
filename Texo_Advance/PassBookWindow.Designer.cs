﻿namespace Texo_Advance
{
    partial class PassBookWindow
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PassBookWindow));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle40 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle41 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle42 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle35 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle36 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle37 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle38 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle39 = new System.Windows.Forms.DataGridViewCellStyle();
            this.typeComboBox = new System.Windows.Forms.ComboBox();
            this.companyNameTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.voucherNoTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dateTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.bankCashComboBox = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.billNoTextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.bankCashCurBalTextBox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.accountCurBalTextBox = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.chqNoTextBox = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.remarkTextBox = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.recAmountTextBox = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.draweeBankComboBox = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.addNewButton = new System.Windows.Forms.Button();
            this.viewButton = new System.Windows.Forms.Button();
            this.updateButton = new System.Windows.Forms.Button();
            this.deleteButton = new System.Windows.Forms.Button();
            this.chqDateTextBox = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.onlineRadioButton = new System.Windows.Forms.RadioButton();
            this.chqRadioButton = new System.Windows.Forms.RadioButton();
            this.paymentModeGroupBox = new System.Windows.Forms.GroupBox();
            this.accountNameComboBox = new JTG.ColumnComboBox();
            this.passbookDataGridView = new System.Windows.Forms.DataGridView();
            this.billNoGridTextBox = new Texo_Advance.CustomControl.DataGridViewMultiColumnComboColumn();
            this.dateGridTextBox = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.taxableValueGridTextBox = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.netAmountGridTextBox = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.typeGridTextBox = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tdsAmountGridTextBox = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.daysGridTextBox = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rdAmountGridTextBox = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.discountPerGridTextBox = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.discAmountGridTextBox = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.comissionPerGridTextBox = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.commAmtGridTexBox = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.returnGoodsGridTextBox = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.addLessGridTextBox = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.othersGridTextBox = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.paymentRecdGridTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pendingAmtGridTextBox = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.paymentModeGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.passbookDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // typeComboBox
            // 
            this.typeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.typeComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.typeComboBox.FormattingEnabled = true;
            this.typeComboBox.Location = new System.Drawing.Point(427, 38);
            this.typeComboBox.Name = "typeComboBox";
            this.typeComboBox.Size = new System.Drawing.Size(194, 24);
            this.typeComboBox.TabIndex = 1;
            this.typeComboBox.SelectedIndexChanged += new System.EventHandler(this.typeComboBox_SelectedIndexChanged);
            // 
            // companyNameTextBox
            // 
            this.companyNameTextBox.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.companyNameTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.companyNameTextBox.Location = new System.Drawing.Point(160, 39);
            this.companyNameTextBox.Name = "companyNameTextBox";
            this.companyNameTextBox.ReadOnly = true;
            this.companyNameTextBox.Size = new System.Drawing.Size(247, 22);
            this.companyNameTextBox.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label1.Location = new System.Drawing.Point(21, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(133, 16);
            this.label1.TabIndex = 2;
            this.label1.Text = "COMPANY NAME:";
            // 
            // voucherNoTextBox
            // 
            this.voucherNoTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.voucherNoTextBox.Location = new System.Drawing.Point(687, 39);
            this.voucherNoTextBox.Name = "voucherNoTextBox";
            this.voucherNoTextBox.Size = new System.Drawing.Size(71, 22);
            this.voucherNoTextBox.TabIndex = 2;
            this.voucherNoTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.voucherNoTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.voucherNoTextBox_KeyDown);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label2.Location = new System.Drawing.Point(631, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "V. NO.:";
            // 
            // dateTextBox
            // 
            this.dateTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTextBox.Location = new System.Drawing.Point(825, 37);
            this.dateTextBox.Name = "dateTextBox";
            this.dateTextBox.Size = new System.Drawing.Size(114, 22);
            this.dateTextBox.TabIndex = 3;
            this.dateTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.dateTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.dateTextBox_Validating);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label3.Location = new System.Drawing.Point(766, 40);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 16);
            this.label3.TabIndex = 2;
            this.label3.Text = "DATE:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(448, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(198, 22);
            this.label4.TabIndex = 2;
            this.label4.Text = "COMPANY PASSBOOK";
            // 
            // bankCashComboBox
            // 
            this.bankCashComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.bankCashComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bankCashComboBox.FormattingEnabled = true;
            this.bankCashComboBox.Location = new System.Drawing.Point(160, 76);
            this.bankCashComboBox.Name = "bankCashComboBox";
            this.bankCashComboBox.Size = new System.Drawing.Size(247, 24);
            this.bankCashComboBox.TabIndex = 4;
            this.bankCashComboBox.SelectedIndexChanged += new System.EventHandler(this.bankCashComboBox_SelectedIndexChanged);
            this.bankCashComboBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.bankCashComboBox_KeyDown);
            this.bankCashComboBox.Validating += new System.ComponentModel.CancelEventHandler(this.bankCashComboBox_Validating);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label5.Location = new System.Drawing.Point(21, 81);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 16);
            this.label5.TabIndex = 2;
            this.label5.Text = "BANK :";
            // 
            // billNoTextBox
            // 
            this.billNoTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.billNoTextBox.Location = new System.Drawing.Point(508, 76);
            this.billNoTextBox.Name = "billNoTextBox";
            this.billNoTextBox.Size = new System.Drawing.Size(78, 22);
            this.billNoTextBox.TabIndex = 5;
            this.billNoTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.billNoTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.billNoTextBox_KeyDown);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label6.Location = new System.Drawing.Point(437, 79);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(72, 16);
            this.label6.TabIndex = 2;
            this.label6.Text = "BILL NO :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label7.Location = new System.Drawing.Point(592, 80);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(88, 16);
            this.label7.TabIndex = 2;
            this.label7.Text = "A/C NAME :";
            // 
            // bankCashCurBalTextBox
            // 
            this.bankCashCurBalTextBox.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.bankCashCurBalTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bankCashCurBalTextBox.Location = new System.Drawing.Point(160, 116);
            this.bankCashCurBalTextBox.Name = "bankCashCurBalTextBox";
            this.bankCashCurBalTextBox.ReadOnly = true;
            this.bankCashCurBalTextBox.Size = new System.Drawing.Size(185, 21);
            this.bankCashCurBalTextBox.TabIndex = 7;
            this.bankCashCurBalTextBox.TabStop = false;
            this.bankCashCurBalTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label8.Location = new System.Drawing.Point(21, 118);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(121, 16);
            this.label8.TabIndex = 2;
            this.label8.Text = "CUR. BALANCE:";
            // 
            // accountCurBalTextBox
            // 
            this.accountCurBalTextBox.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.accountCurBalTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.accountCurBalTextBox.Location = new System.Drawing.Point(686, 116);
            this.accountCurBalTextBox.Name = "accountCurBalTextBox";
            this.accountCurBalTextBox.ReadOnly = true;
            this.accountCurBalTextBox.Size = new System.Drawing.Size(185, 22);
            this.accountCurBalTextBox.TabIndex = 8;
            this.accountCurBalTextBox.TabStop = false;
            this.accountCurBalTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label9.Location = new System.Drawing.Point(559, 118);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(121, 16);
            this.label9.TabIndex = 2;
            this.label9.Text = "CUR. BALANCE:";
            // 
            // chqNoTextBox
            // 
            this.chqNoTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chqNoTextBox.Location = new System.Drawing.Point(160, 153);
            this.chqNoTextBox.Name = "chqNoTextBox";
            this.chqNoTextBox.Size = new System.Drawing.Size(130, 22);
            this.chqNoTextBox.TabIndex = 9;
            this.chqNoTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label10.Location = new System.Drawing.Point(21, 155);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(74, 16);
            this.label10.TabIndex = 2;
            this.label10.Text = "CHQ NO.:";
            // 
            // remarkTextBox
            // 
            this.remarkTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.remarkTextBox.Location = new System.Drawing.Point(161, 220);
            this.remarkTextBox.Name = "remarkTextBox";
            this.remarkTextBox.Size = new System.Drawing.Size(519, 22);
            this.remarkTextBox.TabIndex = 13;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label11.Location = new System.Drawing.Point(313, 188);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(122, 16);
            this.label11.TabIndex = 2;
            this.label11.Text = "DRAWEE BANK:";
            // 
            // recAmountTextBox
            // 
            this.recAmountTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.recAmountTextBox.Location = new System.Drawing.Point(687, 153);
            this.recAmountTextBox.Name = "recAmountTextBox";
            this.recAmountTextBox.Size = new System.Drawing.Size(130, 22);
            this.recAmountTextBox.TabIndex = 10;
            this.recAmountTextBox.Text = "0.0";
            this.recAmountTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label12.Location = new System.Drawing.Point(598, 155);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(81, 16);
            this.label12.TabIndex = 2;
            this.label12.Text = "AMOUNT.:";
            // 
            // draweeBankComboBox
            // 
            this.draweeBankComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.draweeBankComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.draweeBankComboBox.FormattingEnabled = true;
            this.draweeBankComboBox.ItemHeight = 16;
            this.draweeBankComboBox.Location = new System.Drawing.Point(452, 182);
            this.draweeBankComboBox.MaxDropDownItems = 100;
            this.draweeBankComboBox.Name = "draweeBankComboBox";
            this.draweeBankComboBox.Size = new System.Drawing.Size(169, 24);
            this.draweeBankComboBox.TabIndex = 12;
            this.draweeBankComboBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.draweeBankComboBox_KeyDown);
            this.draweeBankComboBox.Validating += new System.ComponentModel.CancelEventHandler(this.draweeBankComboBox_Validating);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label13.Location = new System.Drawing.Point(21, 223);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(79, 16);
            this.label13.TabIndex = 2;
            this.label13.Text = "REMARK :";
            // 
            // addNewButton
            // 
            this.addNewButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addNewButton.ForeColor = System.Drawing.Color.MediumBlue;
            this.addNewButton.Location = new System.Drawing.Point(180, 503);
            this.addNewButton.Name = "addNewButton";
            this.addNewButton.Size = new System.Drawing.Size(100, 40);
            this.addNewButton.TabIndex = 15;
            this.addNewButton.Text = "ADD NEW";
            this.addNewButton.UseVisualStyleBackColor = true;
            this.addNewButton.Click += new System.EventHandler(this.addNewButton_Click);
            // 
            // viewButton
            // 
            this.viewButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.viewButton.ForeColor = System.Drawing.Color.MediumBlue;
            this.viewButton.Location = new System.Drawing.Point(310, 503);
            this.viewButton.Name = "viewButton";
            this.viewButton.Size = new System.Drawing.Size(100, 40);
            this.viewButton.TabIndex = 16;
            this.viewButton.Text = "VIEW";
            this.viewButton.UseVisualStyleBackColor = true;
            this.viewButton.Click += new System.EventHandler(this.viewButton_Click);
            // 
            // updateButton
            // 
            this.updateButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.updateButton.ForeColor = System.Drawing.Color.MediumBlue;
            this.updateButton.Location = new System.Drawing.Point(440, 503);
            this.updateButton.Name = "updateButton";
            this.updateButton.Size = new System.Drawing.Size(100, 40);
            this.updateButton.TabIndex = 17;
            this.updateButton.Text = "UPDATE";
            this.updateButton.UseVisualStyleBackColor = true;
            this.updateButton.Click += new System.EventHandler(this.updateButton_Click);
            // 
            // deleteButton
            // 
            this.deleteButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deleteButton.ForeColor = System.Drawing.Color.MediumBlue;
            this.deleteButton.Location = new System.Drawing.Point(570, 503);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(100, 40);
            this.deleteButton.TabIndex = 18;
            this.deleteButton.Text = "DELETE";
            this.deleteButton.UseVisualStyleBackColor = true;
            this.deleteButton.Click += new System.EventHandler(this.deleteButton_Click);
            // 
            // chqDateTextBox
            // 
            this.chqDateTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chqDateTextBox.Location = new System.Drawing.Point(162, 187);
            this.chqDateTextBox.Name = "chqDateTextBox";
            this.chqDateTextBox.Size = new System.Drawing.Size(130, 22);
            this.chqDateTextBox.TabIndex = 11;
            this.chqDateTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.chqDateTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.chqDateTextBox_Validating);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label14.Location = new System.Drawing.Point(21, 190);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(89, 16);
            this.label14.TabIndex = 2;
            this.label14.Text = "CHQ DATE:";
            // 
            // onlineRadioButton
            // 
            this.onlineRadioButton.AutoSize = true;
            this.onlineRadioButton.Location = new System.Drawing.Point(11, 20);
            this.onlineRadioButton.Name = "onlineRadioButton";
            this.onlineRadioButton.Size = new System.Drawing.Size(81, 20);
            this.onlineRadioButton.TabIndex = 7;
            this.onlineRadioButton.Text = "ONLINE";
            this.onlineRadioButton.UseVisualStyleBackColor = true;
            this.onlineRadioButton.CheckedChanged += new System.EventHandler(this.onlineRadioButton_CheckedChanged);
            // 
            // chqRadioButton
            // 
            this.chqRadioButton.AutoSize = true;
            this.chqRadioButton.Location = new System.Drawing.Point(100, 19);
            this.chqRadioButton.Name = "chqRadioButton";
            this.chqRadioButton.Size = new System.Drawing.Size(89, 20);
            this.chqRadioButton.TabIndex = 8;
            this.chqRadioButton.Text = "CHEQUE";
            this.chqRadioButton.UseVisualStyleBackColor = true;
            this.chqRadioButton.CheckedChanged += new System.EventHandler(this.chqRadioButton_CheckedChanged);
            // 
            // paymentModeGroupBox
            // 
            this.paymentModeGroupBox.Controls.Add(this.chqRadioButton);
            this.paymentModeGroupBox.Controls.Add(this.onlineRadioButton);
            this.paymentModeGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.paymentModeGroupBox.Location = new System.Drawing.Point(354, 106);
            this.paymentModeGroupBox.Name = "paymentModeGroupBox";
            this.paymentModeGroupBox.Size = new System.Drawing.Size(199, 52);
            this.paymentModeGroupBox.TabIndex = 7;
            this.paymentModeGroupBox.TabStop = false;
            this.paymentModeGroupBox.Text = "PAYMENT MODE";
            // 
            // accountNameComboBox
            // 
            this.accountNameComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.accountNameComboBox.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.accountNameComboBox.DropDownWidth = 17;
            this.accountNameComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.accountNameComboBox.Location = new System.Drawing.Point(687, 76);
            this.accountNameComboBox.MaxDropDownItems = 100;
            this.accountNameComboBox.Name = "accountNameComboBox";
            this.accountNameComboBox.Size = new System.Drawing.Size(182, 22);
            this.accountNameComboBox.TabIndex = 6;
            this.accountNameComboBox.ViewColumn = 0;
            this.accountNameComboBox.SelectedIndexChanged += new System.EventHandler(this.accountNameComboBox_SelectedIndexChanged);
            this.accountNameComboBox.Validating += new System.ComponentModel.CancelEventHandler(this.accountNameComboBox_Validating);
            // 
            // passbookDataGridView
            // 
            this.passbookDataGridView.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle22.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle22.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle22.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle22.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle22.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle22.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.passbookDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle22;
            this.passbookDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.passbookDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.billNoGridTextBox,
            this.dateGridTextBox,
            this.taxableValueGridTextBox,
            this.netAmountGridTextBox,
            this.typeGridTextBox,
            this.tdsAmountGridTextBox,
            this.daysGridTextBox,
            this.rdAmountGridTextBox,
            this.discountPerGridTextBox,
            this.discAmountGridTextBox,
            this.comissionPerGridTextBox,
            this.commAmtGridTexBox,
            this.returnGoodsGridTextBox,
            this.addLessGridTextBox,
            this.othersGridTextBox,
            this.paymentRecdGridTextBoxColumn,
            this.pendingAmtGridTextBox});
            dataGridViewCellStyle40.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle40.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle40.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle40.ForeColor = System.Drawing.Color.Red;
            dataGridViewCellStyle40.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle40.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle40.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.passbookDataGridView.DefaultCellStyle = dataGridViewCellStyle40;
            this.passbookDataGridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.passbookDataGridView.Location = new System.Drawing.Point(24, 265);
            this.passbookDataGridView.Name = "passbookDataGridView";
            dataGridViewCellStyle41.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle41.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle41.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle41.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle41.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle41.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle41.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.passbookDataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle41;
            dataGridViewCellStyle42.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.passbookDataGridView.RowsDefaultCellStyle = dataGridViewCellStyle42;
            this.passbookDataGridView.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.passbookDataGridView.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black;
            this.passbookDataGridView.Size = new System.Drawing.Size(922, 199);
            this.passbookDataGridView.TabIndex = 14;
            this.passbookDataGridView.CellValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.passbookDataGridView_CellValidated);
            this.passbookDataGridView.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.passbookDataGridView_CellValidating);
            this.passbookDataGridView.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.passbookDataGridView_EditingControlShowing);
            this.passbookDataGridView.Enter += new System.EventHandler(this.passbookDataGridView_Enter);
            // 
            // billNoGridTextBox
            // 
            dataGridViewCellStyle23.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle23.ForeColor = System.Drawing.Color.Black;
            this.billNoGridTextBox.DefaultCellStyle = dataGridViewCellStyle23;
            this.billNoGridTextBox.HeaderText = "BILL NO.";
            this.billNoGridTextBox.MaxDropDownItems = 100;
            this.billNoGridTextBox.Name = "billNoGridTextBox";
            this.billNoGridTextBox.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.billNoGridTextBox.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // dateGridTextBox
            // 
            dataGridViewCellStyle24.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle24.ForeColor = System.Drawing.Color.Black;
            this.dateGridTextBox.DefaultCellStyle = dataGridViewCellStyle24;
            this.dateGridTextBox.HeaderText = "DATE";
            this.dateGridTextBox.Name = "dateGridTextBox";
            this.dateGridTextBox.ReadOnly = true;
            // 
            // taxableValueGridTextBox
            // 
            dataGridViewCellStyle25.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle25.ForeColor = System.Drawing.Color.Black;
            this.taxableValueGridTextBox.DefaultCellStyle = dataGridViewCellStyle25;
            this.taxableValueGridTextBox.HeaderText = "TAXABLE VALUE";
            this.taxableValueGridTextBox.Name = "taxableValueGridTextBox";
            this.taxableValueGridTextBox.ReadOnly = true;
            // 
            // netAmountGridTextBox
            // 
            dataGridViewCellStyle26.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle26.ForeColor = System.Drawing.Color.Black;
            this.netAmountGridTextBox.DefaultCellStyle = dataGridViewCellStyle26;
            this.netAmountGridTextBox.HeaderText = "NET AMOUNT";
            this.netAmountGridTextBox.Name = "netAmountGridTextBox";
            this.netAmountGridTextBox.ReadOnly = true;
            // 
            // typeGridTextBox
            // 
            dataGridViewCellStyle27.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle27.ForeColor = System.Drawing.Color.Black;
            this.typeGridTextBox.DefaultCellStyle = dataGridViewCellStyle27;
            this.typeGridTextBox.HeaderText = "TYPE";
            this.typeGridTextBox.Name = "typeGridTextBox";
            this.typeGridTextBox.ReadOnly = true;
            // 
            // tdsAmountGridTextBox
            // 
            dataGridViewCellStyle28.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle28.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle28.NullValue = "0";
            this.tdsAmountGridTextBox.DefaultCellStyle = dataGridViewCellStyle28;
            this.tdsAmountGridTextBox.HeaderText = "TDS AMOUNT";
            this.tdsAmountGridTextBox.Name = "tdsAmountGridTextBox";
            // 
            // daysGridTextBox
            // 
            dataGridViewCellStyle29.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle29.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle29.NullValue = "0";
            this.daysGridTextBox.DefaultCellStyle = dataGridViewCellStyle29;
            this.daysGridTextBox.HeaderText = "DAYS";
            this.daysGridTextBox.Name = "daysGridTextBox";
            this.daysGridTextBox.ReadOnly = true;
            // 
            // rdAmountGridTextBox
            // 
            dataGridViewCellStyle30.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle30.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle30.NullValue = "0";
            this.rdAmountGridTextBox.DefaultCellStyle = dataGridViewCellStyle30;
            this.rdAmountGridTextBox.HeaderText = "RD AMT.";
            this.rdAmountGridTextBox.Name = "rdAmountGridTextBox";
            // 
            // discountPerGridTextBox
            // 
            dataGridViewCellStyle31.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle31.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle31.NullValue = "0";
            this.discountPerGridTextBox.DefaultCellStyle = dataGridViewCellStyle31;
            this.discountPerGridTextBox.HeaderText = "DISC%";
            this.discountPerGridTextBox.Name = "discountPerGridTextBox";
            // 
            // discAmountGridTextBox
            // 
            dataGridViewCellStyle32.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle32.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle32.NullValue = "0";
            this.discAmountGridTextBox.DefaultCellStyle = dataGridViewCellStyle32;
            this.discAmountGridTextBox.HeaderText = "DISC.AMT.";
            this.discAmountGridTextBox.Name = "discAmountGridTextBox";
            // 
            // comissionPerGridTextBox
            // 
            dataGridViewCellStyle33.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle33.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle33.NullValue = "0";
            this.comissionPerGridTextBox.DefaultCellStyle = dataGridViewCellStyle33;
            this.comissionPerGridTextBox.HeaderText = "COMM%";
            this.comissionPerGridTextBox.Name = "comissionPerGridTextBox";
            // 
            // commAmtGridTexBox
            // 
            dataGridViewCellStyle34.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle34.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle34.NullValue = "0";
            this.commAmtGridTexBox.DefaultCellStyle = dataGridViewCellStyle34;
            this.commAmtGridTexBox.HeaderText = "COMM.AMT.";
            this.commAmtGridTexBox.Name = "commAmtGridTexBox";
            // 
            // returnGoodsGridTextBox
            // 
            dataGridViewCellStyle35.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle35.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle35.NullValue = "0";
            this.returnGoodsGridTextBox.DefaultCellStyle = dataGridViewCellStyle35;
            this.returnGoodsGridTextBox.HeaderText = "RG";
            this.returnGoodsGridTextBox.Name = "returnGoodsGridTextBox";
            // 
            // addLessGridTextBox
            // 
            dataGridViewCellStyle36.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle36.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle36.NullValue = "0";
            this.addLessGridTextBox.DefaultCellStyle = dataGridViewCellStyle36;
            this.addLessGridTextBox.HeaderText = "ADD/LESS";
            this.addLessGridTextBox.Name = "addLessGridTextBox";
            // 
            // othersGridTextBox
            // 
            dataGridViewCellStyle37.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle37.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle37.NullValue = "0";
            this.othersGridTextBox.DefaultCellStyle = dataGridViewCellStyle37;
            this.othersGridTextBox.HeaderText = "OTHERS";
            this.othersGridTextBox.Name = "othersGridTextBox";
            // 
            // paymentRecdGridTextBoxColumn
            // 
            dataGridViewCellStyle38.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle38.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle38.NullValue = "0";
            this.paymentRecdGridTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle38;
            this.paymentRecdGridTextBoxColumn.HeaderText = "PAYMENT RECD.";
            this.paymentRecdGridTextBoxColumn.Name = "paymentRecdGridTextBoxColumn";
            // 
            // pendingAmtGridTextBox
            // 
            dataGridViewCellStyle39.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle39.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle39.NullValue = "0";
            this.pendingAmtGridTextBox.DefaultCellStyle = dataGridViewCellStyle39;
            this.pendingAmtGridTextBox.HeaderText = "PENDIING AMT.";
            this.pendingAmtGridTextBox.Name = "pendingAmtGridTextBox";
            this.pendingAmtGridTextBox.ReadOnly = true;
            // 
            // PassBookWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.MintCream;
            this.Controls.Add(this.paymentModeGroupBox);
            this.Controls.Add(this.accountNameComboBox);
            this.Controls.Add(this.deleteButton);
            this.Controls.Add(this.updateButton);
            this.Controls.Add(this.viewButton);
            this.Controls.Add(this.addNewButton);
            this.Controls.Add(this.passbookDataGridView);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dateTextBox);
            this.Controls.Add(this.voucherNoTextBox);
            this.Controls.Add(this.billNoTextBox);
            this.Controls.Add(this.accountCurBalTextBox);
            this.Controls.Add(this.remarkTextBox);
            this.Controls.Add(this.recAmountTextBox);
            this.Controls.Add(this.chqDateTextBox);
            this.Controls.Add(this.chqNoTextBox);
            this.Controls.Add(this.bankCashCurBalTextBox);
            this.Controls.Add(this.companyNameTextBox);
            this.Controls.Add(this.draweeBankComboBox);
            this.Controls.Add(this.bankCashComboBox);
            this.Controls.Add(this.typeComboBox);
            this.ForeColor = System.Drawing.Color.Red;
            this.Name = "PassBookWindow";
            this.Size = new System.Drawing.Size(1173, 551);
            this.Load += new System.EventHandler(this.PassBookWindow_Load);
            this.paymentModeGroupBox.ResumeLayout(false);
            this.paymentModeGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.passbookDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox typeComboBox;
        private System.Windows.Forms.TextBox companyNameTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox voucherNoTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox dateTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox bankCashComboBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox billNoTextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox bankCashCurBalTextBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox accountCurBalTextBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox chqNoTextBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox remarkTextBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox recAmountTextBox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox draweeBankComboBox;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.DataGridView passbookDataGridView;
        private System.Windows.Forms.Button addNewButton;
        private System.Windows.Forms.Button viewButton;
        private System.Windows.Forms.Button updateButton;
        private System.Windows.Forms.Button deleteButton;
        private System.Windows.Forms.TextBox chqDateTextBox;
        private System.Windows.Forms.Label label14;
        private JTG.ColumnComboBox accountNameComboBox;
        private System.Windows.Forms.RadioButton onlineRadioButton;
        private System.Windows.Forms.RadioButton chqRadioButton;
        private System.Windows.Forms.GroupBox paymentModeGroupBox;
        private CustomControl.DataGridViewMultiColumnComboColumn billNoGridTextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateGridTextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn taxableValueGridTextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn netAmountGridTextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn typeGridTextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn tdsAmountGridTextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn daysGridTextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn rdAmountGridTextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn discountPerGridTextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn discAmountGridTextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn comissionPerGridTextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn commAmtGridTexBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn returnGoodsGridTextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn addLessGridTextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn othersGridTextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn paymentRecdGridTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn pendingAmtGridTextBox;
    }
}
