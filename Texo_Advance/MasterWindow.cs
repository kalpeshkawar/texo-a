﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Texo_Advance.MasterItems;

namespace Texo_Advance
{
    public partial class MasterWindow : UserControl
    {
        public MasterWindow()
        {
            InitializeComponent();
        }

        private void createCompanyButton_Click(object sender, EventArgs e)
        {
            CreateCompanyWindow ccw = new CreateCompanyWindow();
            ccw.ShowDialog();
        }

        private void cuttingReportButton_Click(object sender, EventArgs e)
        {
            CuttingReportWindow crw = new CuttingReportWindow();
            crw.ShowDialog();

        }

        private void fin_year_Click(object sender, EventArgs e)
        {
            FinancialYear fy = new FinancialYear();
            fy.ShowDialog();
        }
    }
}
