﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using System.Globalization;
using Texo_Advance.DBItems;

namespace Texo_Advance
{
    public partial class JournalEntryWindow : UserControl
    {
        MasterDBData DB = new MasterDBData();
        PartyDBData partyDB = new PartyDBData();
        journalentry_detail jornalEntryDetail = new journalentry_detail();
        public JournalEntryWindow(string companyName)
        {
            InitializeComponent();
            companyNameTextBox.Text = companyName;
        }

        private void JournalEntryWindow_Load(object sender, EventArgs e)
        {
            fillCreditAccountPartyList();
            fillDebitAccountPartyList();
            setDefaultData();
            dateTextBox.Text = DateTime.Today.ToString("dd-MM-yyyy");
            addNewButton.Enabled = true;
            updateButton.Enabled = false;
            deleteButton.Enabled = false;
            viewButton.Enabled = true;
        }

        private void fillCreditAccountPartyList()
        {
            int iColumnCount = 5;
            var partyAccountList = DB.getPartyAccountListForPassBook();
            
            DataTable myDataTable = new DataTable("ParentTable");
            // Declare variables for DataColumn and DataRow objects.
            DataColumn myDataColumn;
            DataRow myDataRow;

            //Add some coulumns
            for (int index = 0; index < iColumnCount; index++)
            {
                myDataColumn = new DataColumn();
                myDataColumn.DataType = typeof(string);
                if (index == 0)
                    myDataColumn.ColumnName = "NAME";
                else if (index == 1)
                    myDataColumn.ColumnName = "ACCOUNT_TYPE";
                else if (index == 2)
                    myDataColumn.ColumnName = "ADDRESS";
                else if (index == 3)
                    myDataColumn.ColumnName = "CITY";
                else if (index == 4)
                    myDataColumn.ColumnName = "PARTY_ID";
                //myDataColumn.Caption = "ParentItem";
                myDataColumn.AutoIncrement = false;
                myDataColumn.ReadOnly = false;
                myDataColumn.Unique = false;
                // Add the Column to the DataColumnCollection.
                myDataTable.Columns.Add(myDataColumn);
            }

            //Add some more rows
            for (int index = 0; index < partyAccountList.Count; index++)
            {
                myDataRow = myDataTable.NewRow();
                myDataRow[0] = partyAccountList[index].party_name;
                myDataRow[1] = DB.getAccountTypeName(partyAccountList[index].account_typeid);
                myDataRow[2] = partyAccountList[index].address + partyAccountList[index].address2;
                myDataRow[3] = DB.getCityName((long)partyAccountList[index].city_id);
                myDataRow[4] = partyAccountList[index].partyid;
                myDataTable.Rows.Add(myDataRow);
            }


            //Now set the Data of the ColumnComboBox
            creditAccountComboBox.ValueMember = "PARTY_ID";
            creditAccountComboBox.Data = myDataTable;
            //Set which row will be displayed in the text box
            //If you set this to a column that isn't displayed then the suggesting functionality won't work.
            creditAccountComboBox.ColumnSpacing = 30;
            creditAccountComboBox.ViewColumn = 0;

            creditAccountComboBox.Columns[0].Width = 175;
            creditAccountComboBox.Columns[1].Width = 125;
            creditAccountComboBox.Columns[2].Width = 125;
            creditAccountComboBox.Columns[3].Width = 75;
            //Set a few columns to not be shown
            creditAccountComboBox.Columns[4].Display = false;


        }

        private void fillDebitAccountPartyList()
        {
            int iColumnCount = 5;
            var partyAccountList = DB.getPartyAccountListForPassBook();

            DataTable myDataTable = new DataTable("ParentTable");
            // Declare variables for DataColumn and DataRow objects.
            DataColumn myDataColumn;
            DataRow myDataRow;

            //Add some coulumns
            for (int index = 0; index < iColumnCount; index++)
            {
                myDataColumn = new DataColumn();
                myDataColumn.DataType = typeof(string);
                if (index == 0)
                    myDataColumn.ColumnName = "NAME";
                else if (index == 1)
                    myDataColumn.ColumnName = "ACCOUNT_TYPE";
                else if (index == 2)
                    myDataColumn.ColumnName = "ADDRESS";
                else if (index == 3)
                    myDataColumn.ColumnName = "CITY";
                else if (index == 4)
                    myDataColumn.ColumnName = "PARTY_ID";
                //myDataColumn.Caption = "ParentItem";
                myDataColumn.AutoIncrement = false;
                myDataColumn.ReadOnly = false;
                myDataColumn.Unique = false;
                // Add the Column to the DataColumnCollection.
                myDataTable.Columns.Add(myDataColumn);
            }

            //Add some more rows
            for (int index = 0; index < partyAccountList.Count; index++)
            {
                myDataRow = myDataTable.NewRow();
                myDataRow[0] = partyAccountList[index].party_name;
                myDataRow[1] = DB.getAccountTypeName(partyAccountList[index].account_typeid);
                myDataRow[2] = partyAccountList[index].address + partyAccountList[index].address2;
                myDataRow[3] = DB.getCityName((long)partyAccountList[index].city_id);
                myDataRow[4] = partyAccountList[index].partyid;
                myDataTable.Rows.Add(myDataRow);
            }


            //Now set the Data of the ColumnComboBox
            debitAccountComboBox.ValueMember = "PARTY_ID";
            debitAccountComboBox.Data = myDataTable;
            //Set which row will be displayed in the text box
            //If you set this to a column that isn't displayed then the suggesting functionality won't work.
            debitAccountComboBox.ColumnSpacing = 30;
            debitAccountComboBox.ViewColumn = 0;

            debitAccountComboBox.Columns[0].Width = 175;
            debitAccountComboBox.Columns[1].Width = 125;
            debitAccountComboBox.Columns[2].Width = 125;
            debitAccountComboBox.Columns[3].Width = 75;
            //Set a few columns to not be shown
            debitAccountComboBox.Columns[4].Display = false;


        }

        private void setDefaultData()
        {
            long lMaxVoucherNo = partyDB.getJournalEntryVoucherNo();
            if (lMaxVoucherNo != 0)
            {
                voucherNoTextBox.Text = Convert.ToString(lMaxVoucherNo + 1);
            }
            else
            {
                voucherNoTextBox.Text = "1";
            }
        }

        private void dateTextBox_Validating(object sender, CancelEventArgs e)
        {
            DateTime dt;
            bool isValid = DateTime.TryParseExact(dateTextBox.Text, "dd-MM-yyyy", null, DateTimeStyles.None, out dt);
            if (!isValid)
            {
                MessageBox.Show("Please enter the valid date", "DATE", MessageBoxButtons.OK);
                dateTextBox.Focus();
            }
        }

        private void resetAllData()
        {
            dateTextBox.Text = DateTime.Today.ToString("dd-MM-yyyy");
            creditAccountComboBox.SelectedIndex = -1;
            creditAccountComboBox.Text = "";
            debitAccountComboBox.SelectedIndex = -1;
            debitAccountComboBox.Text = "";
            creditAccCurBalTextBox.Text = "0.0";
            debitAccCurBalTextBox.Text = "0.0";
            amountTextBox.Text = "0";
            chqRefNoTextBox.Text = "";
            creditRemarksTextBox.Clear();
            debitRemarksTextBox.Clear();
            addNewButton.Enabled = true;
            updateButton.Enabled = false;
            deleteButton.Enabled = false;
        }

        private void addNewButton_Click(object sender, EventArgs e)
        {
            if(creditAccountComboBox.SelectedIndex == -1)
            {
                MessageBox.Show("Please select credit party A/C","ALERT",MessageBoxButtons.OK);
                creditAccountComboBox.Focus();
                return;
            }
            else if (debitAccountComboBox.SelectedIndex == -1)
            {
                MessageBox.Show("Please select debit party A/C", "ALERT", MessageBoxButtons.OK);
                debitAccountComboBox.Focus();
                return;
            }
            double dAmount = 0;
            double.TryParse(amountTextBox.Text, out dAmount);
            if(dAmount <= 0)
            {
                MessageBox.Show("Please enter a valid amount", "ALERT", MessageBoxButtons.OK);
                amountTextBox.Focus();
                return;
            }

            journalentry_detail journalEntry = new journalentry_detail();
            journalEntry.amount = dAmount;
            journalEntry.chqRefNo = chqRefNoTextBox.Text;
            journalEntry.creditAccount_id = Convert.ToInt64(creditAccountComboBox["PARTY_ID"].ToString());
            journalEntry.creditRemarks = creditRemarksTextBox.Text;
            journalEntry.date = DateTime.ParseExact(dateTextBox.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            journalEntry.debitAccount_id = Convert.ToInt64(debitAccountComboBox["PARTY_ID"].ToString());
            journalEntry.debitRemarks = debitRemarksTextBox.Text;
            journalEntry.voucherNo = Convert.ToInt64(voucherNoTextBox.Text);

            bool bFlag = partyDB.addJournalEntryVoucher(journalEntry);
            if(bFlag)
            {
                MessageBox.Show("Journal Entry Added", "Journal Entry", MessageBoxButtons.OK);
                resetAllData();
                setDefaultData();

            }
            else
            {
                MessageBox.Show("Journal Entry Not Added", "ERROR", MessageBoxButtons.OK);
            }
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            this.Parent.Controls.Remove(this);
        }

        private void creditAccountComboBox_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter && creditAccountComboBox.SelectedIndex == -1)
            {
                AccountManagerForm acc = new AccountManagerForm();
                acc.setDefaultAccountType(1);
                acc.setDefaultName(creditAccountComboBox.Text);
                acc.ShowDialog();
                fillCreditAccountPartyList();
            }
        }

        private void debitAccountComboBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && debitAccountComboBox.SelectedIndex == -1)
            {
                AccountManagerForm acc = new AccountManagerForm();
                acc.setDefaultAccountType(1);
                acc.setDefaultName(debitAccountComboBox.Text);
                acc.ShowDialog();
                fillDebitAccountPartyList();
            }
        }

        private void viewButton_Click(object sender, EventArgs e)
        {
            long lVoucherNo = 0;
            long.TryParse(voucherNoTextBox.Text,out lVoucherNo);
            jornalEntryDetail = partyDB.getJournalEntryDetail(lVoucherNo);
            if(jornalEntryDetail == null)
            {
                MessageBox.Show("Journal Entry Does Not Exist", "VOUCHER", MessageBoxButtons.OK);
                voucherNoTextBox.Focus();
                resetAllData();
                setDefaultData();
                return;
            }
            DateTime dt, dt1;
            bool bFlag = DateTime.TryParse(jornalEntryDetail.date.ToString(), out dt);
            if (bFlag)
            {
                dt1 = dt.Date;
                //DateTime.Parse(dt1.ToString()).ToString("dd-MM-yyyy");
                dateTextBox.Text = dt1.ToString("dd-MM-yyyy");
            }
            
            setCreditAccountComboBoxSelectedValue(Convert.ToInt64(jornalEntryDetail.creditAccount_id));
            
            setDebitAccountComboBoxSelectedValue(Convert.ToInt64(jornalEntryDetail.debitAccount_id));
            amountTextBox.Text = Convert.ToString(jornalEntryDetail.amount);
            chqRefNoTextBox.Text = jornalEntryDetail.chqRefNo;
            creditRemarksTextBox.Text = jornalEntryDetail.creditRemarks;
            debitRemarksTextBox.Text = jornalEntryDetail.debitRemarks;

            addNewButton.Enabled = false;
            updateButton.Enabled = true;
            deleteButton.Enabled = true;
        }

        private void updateButton_Click(object sender, EventArgs e)
        {
            if (creditAccountComboBox.SelectedIndex == -1)
            {
                MessageBox.Show("Please select credit party A/C", "ALERT", MessageBoxButtons.OK);
                creditAccountComboBox.Focus();
                return;
            }
            else if (debitAccountComboBox.SelectedIndex == -1)
            {
                MessageBox.Show("Please select debit party A/C", "ALERT", MessageBoxButtons.OK);
                debitAccountComboBox.Focus();
                return;
            }
            double dAmount = 0;
            double.TryParse(amountTextBox.Text, out dAmount);
            if (dAmount <= 0)
            {
                MessageBox.Show("Please enter a valid amount", "ALERT", MessageBoxButtons.OK);
                amountTextBox.Focus();
                return;
            }

            jornalEntryDetail.amount = dAmount;
            jornalEntryDetail.chqRefNo = chqRefNoTextBox.Text;
            jornalEntryDetail.creditAccount_id = Convert.ToInt64(creditAccountComboBox["PARTY_ID"].ToString());
            jornalEntryDetail.creditRemarks = creditRemarksTextBox.Text;
            jornalEntryDetail.date = DateTime.ParseExact(dateTextBox.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            jornalEntryDetail.debitAccount_id = Convert.ToInt64(debitAccountComboBox["PARTY_ID"].ToString());
            jornalEntryDetail.debitRemarks = debitRemarksTextBox.Text;
            jornalEntryDetail.voucherNo = Convert.ToInt64(voucherNoTextBox.Text);

            bool bFlag = partyDB.addJournalEntryVoucher(jornalEntryDetail);
            if (bFlag)
            {
                MessageBox.Show("Journal Entry Updated", "Journal Entry", MessageBoxButtons.OK);
                resetAllData();
                setDefaultData();

            }
            else
            {
                MessageBox.Show("Journal Entry Not Updated", "ERROR", MessageBoxButtons.OK);
            }
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            long lVoucherNo = 0;
            long.TryParse(voucherNoTextBox.Text, out lVoucherNo);
            if(lVoucherNo == 0)
            {
                MessageBox.Show("Please Enter Valid Voucher No", "VOUCHER", MessageBoxButtons.OK);
                voucherNoTextBox.Focus();
                return;
            }
            bool bVoucherDeleted = partyDB.deleteJournalEntryVoucher(lVoucherNo);
            if(bVoucherDeleted)
            {
                MessageBox.Show("Journal Entry Voucher Deleted", "Journal Entry", MessageBoxButtons.OK);
                resetAllData();
                setDefaultData();
            }
            else
                MessageBox.Show("Journal Entry Voucher Not Deleted", "Journal Entry", MessageBoxButtons.OK);
        }

        private void setCreditAccountComboBoxSelectedValue(long lPartyID)
        {
            try
            {
                //partyComboBox.SelectedIndex = -1;
                if (lPartyID >= 0)
                {
                    for (int index = 0; index < creditAccountComboBox.Items.Count; index++)
                    {
                        DataTable partyTable = creditAccountComboBox.Data;
                        DataRow Row = partyTable.Rows[index];
                        if (Row != null)
                        {
                            if (Convert.ToInt64(Row["PARTY_ID"].ToString()) == lPartyID)
                            {
                                creditAccountComboBox.SelectedIndex = -1;
                                creditAccountComboBox.SelectedIndex = index;
                                break;
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                return;
            }
        }

        private void setDebitAccountComboBoxSelectedValue(long lPartyID)
        {
            try
            {
                //partyComboBox.SelectedIndex = -1;
                if (lPartyID >= 0)
                {
                    for (int index = 0; index < debitAccountComboBox.Items.Count; index++)
                    {
                        DataTable partyTable = debitAccountComboBox.Data;
                        DataRow Row = partyTable.Rows[index];
                        if (Row != null)
                        {
                            if (Convert.ToInt64(Row["PARTY_ID"].ToString()) == lPartyID)
                            {
                                debitAccountComboBox.SelectedIndex = -1;
                                debitAccountComboBox.SelectedIndex = index;
                                break;
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                return;
            }
        }

        private void voucherNoTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                long lVoucherNo = 0;
                long.TryParse(voucherNoTextBox.Text, out lVoucherNo);
                jornalEntryDetail = partyDB.getJournalEntryDetail(lVoucherNo);
                if (jornalEntryDetail == null)
                {
                    MessageBox.Show("Journal Entry Does Not Exist", "VOUCHER", MessageBoxButtons.OK);
                    voucherNoTextBox.Focus();
                    resetAllData();
                    setDefaultData();
                    return;
                }
                DateTime dt, dt1;
                bool bFlag = DateTime.TryParse(jornalEntryDetail.date.ToString(), out dt);
                if (bFlag)
                {
                    dt1 = dt.Date;
                    //DateTime.Parse(dt1.ToString()).ToString("dd-MM-yyyy");
                    dateTextBox.Text = dt1.ToString("dd-MM-yyyy");
                }

                setCreditAccountComboBoxSelectedValue(Convert.ToInt64(jornalEntryDetail.creditAccount_id));

                setDebitAccountComboBoxSelectedValue(Convert.ToInt64(jornalEntryDetail.debitAccount_id));
                amountTextBox.Text = Convert.ToString(jornalEntryDetail.amount);
                chqRefNoTextBox.Text = jornalEntryDetail.chqRefNo;
                creditRemarksTextBox.Text = jornalEntryDetail.creditRemarks;
                debitRemarksTextBox.Text = jornalEntryDetail.debitRemarks;

                addNewButton.Enabled = false;
                updateButton.Enabled = true;
                deleteButton.Enabled = true;
            }
        }

        private void creditAccountComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(creditAccountComboBox.SelectedIndex != -1)
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-IN");
                string str = CultureInfo.CurrentCulture.NumberFormat.CurrencySymbol;
                string partyID = creditAccountComboBox["PARTY_ID"].ToString();
                string accountTypeId = creditAccountComboBox["ACCOUNT_TYPE"].ToString();
                long lAccountTypeId = DB.getAccountTypeID(accountTypeId);
                double dOutstandingOpeningBalance = DB.getOutstandingOpeningBalance(Convert.ToInt64(partyID));
                double dBillOutstandingAmount = partyDB.getOutstandingAmount(Convert.ToInt64(partyID), lAccountTypeId);
                double dJournalEntryOutstandingAmount = partyDB.getOutstandingFromJournalEntryData(Convert.ToInt64(partyID));
                double dOutstandingAmount = dOutstandingOpeningBalance + dBillOutstandingAmount + dJournalEntryOutstandingAmount;
                if (dOutstandingAmount > 0)
                    creditAccCurBalTextBox.Text = str + string.Format(System.Globalization.CultureInfo.CreateSpecificCulture("hi-IN"), "{0:#,0.00}", dOutstandingAmount) + " CR";
                else if (dOutstandingAmount < 0)
                    creditAccCurBalTextBox.Text = str + string.Format(System.Globalization.CultureInfo.CreateSpecificCulture("hi-IN"), "{0:#,0.00}", -dOutstandingAmount) + " DR";
                else
                    creditAccCurBalTextBox.Text = str + string.Format(System.Globalization.CultureInfo.CreateSpecificCulture("hi-IN"), "{0:#,0.00}", dOutstandingAmount);

            }
        }

        private void debitAccountComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (debitAccountComboBox.SelectedIndex != -1)
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-IN");
                string str = CultureInfo.CurrentCulture.NumberFormat.CurrencySymbol;
                string partyID = debitAccountComboBox["PARTY_ID"].ToString();
                string accountTypeId = debitAccountComboBox["ACCOUNT_TYPE"].ToString();
                long lAccountTypeId = DB.getAccountTypeID(accountTypeId);
                double dOutstandingOpeningBalance = DB.getOutstandingOpeningBalance(Convert.ToInt64(partyID));
                double dBillOutstandingAmount = partyDB.getOutstandingAmount(Convert.ToInt64(partyID), lAccountTypeId);
                double dJournalEntryOutstandingAmount = partyDB.getOutstandingFromJournalEntryData(Convert.ToInt64(partyID));
                double dOutstandingAmount = dOutstandingOpeningBalance + dBillOutstandingAmount + dJournalEntryOutstandingAmount;
                if (dOutstandingAmount > 0)
                    debitAccCurBalTextBox.Text = str + string.Format(System.Globalization.CultureInfo.CreateSpecificCulture("hi-IN"), "{0:#,0.00}", dOutstandingAmount) + " CR";
                else if (dOutstandingAmount < 0)
                    debitAccCurBalTextBox.Text = str + string.Format(System.Globalization.CultureInfo.CreateSpecificCulture("hi-IN"), "{0:#,0.00}", -dOutstandingAmount) + " DR";
                else
                    debitAccCurBalTextBox.Text = str + string.Format(System.Globalization.CultureInfo.CreateSpecificCulture("hi-IN"), "{0:#,0.00}", dOutstandingAmount);

            }
        }
    }
}
