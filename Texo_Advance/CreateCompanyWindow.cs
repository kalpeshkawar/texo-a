﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Texo_Advance.DBItems;

namespace Texo_Advance
{
    public partial class CreateCompanyWindow : Form
    {

        MasterDBData masterDB = new MasterDBData();
        public CreateCompanyWindow()
        {
            InitializeComponent();
        }



        private void CreateCompanyWindow_Load(object sender, EventArgs e)
        {
          
            companyTypeComboBox.DataSource = masterDB.getLegderType();
            companyTypeComboBox.DisplayMember = "type";
            companyTypeComboBox.ValueMember = "ledger_typeid";
            companyTypeComboBox.SelectedIndex = 1;

        }

        private void createButton_Click(object sender, EventArgs e)
        {
            company_details cd = new company_details();
            cd.companyname = companyNameTextBox.Text;
            cd.bankdetails = bankNameTextBox.Text;
            cd.bankno = bankDetailsTextBox.Text;
            cd.description = bankDescrTextBox.Text;
            cd.email = emailIdTextBox.Text;
            cd.panno = panNoTextBox.Text;
            cd.pincode = Convert.ToInt32(pincodeTextBox.Text);
            cd.gstno = gstnTextBox.Text;
            cd.properitor = properitorTextBox.Text;
            cd.mobile = Convert.ToInt64(monileNoTextBox.Text);
            cd.city = cityTextBox.Text;
            cd.address = addressTextBox.Text + ";" + address1TextBox.Text;
            if (masterDB.setCompany(cd))
            {
                MessageBox.Show("Company Added SuccessFully", "Create", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                MessageBox.Show("Comapny Not Added,Please try Again later", "Failed!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }
    }
}
