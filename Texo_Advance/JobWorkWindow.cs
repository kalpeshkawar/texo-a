﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Texo_Advance.DBItems;
using System.Globalization;
using System.Data.Entity.Core.Common.CommandTrees.ExpressionBuilder;
using Texo_Advance.CustomControl;
using System.Reflection;

namespace Texo_Advance
{
    public partial class JobWorkWindow : UserControl
    {
        jobprocess_details jobProcessData = new jobprocess_details();
        MasterDBData DB = new MasterDBData();
        PartyDBData partyDB = new PartyDBData();
        List<JobWorkReceiptItemForMultiColumnDisplay> multiColumnItemDisplayDataList = new List<JobWorkReceiptItemForMultiColumnDisplay>();
        string strCompanyName = "";
        string strCompanyGstn = "";
        int iJobWorkMode = 1;//1-dispatch 2- receive challam 3- receive job bill
        List<long> lSelectedObjects = new List<long>();
        List<int> iSelectedObjectsIndex = new List<int>();
        public JobWorkWindow(string companyName, string gstNumber)
        {
            InitializeComponent();
            strCompanyName = companyName;
            strCompanyGstn = gstNumber;
            
        }

        private void JobWorkWindow_Load(object sender, EventArgs e)
        {
            jobWorkTypeComboBox.Items.Add("WORK DISPATCH CHALLAN");
            jobWorkTypeComboBox.Items.Add("WORK RECEIVE GOODS CHALLAN");
            jobWorkTypeComboBox.Items.Add("WORK RECEIVE BILL CHALLAN");
            jobWorkTypeComboBox.SelectedIndex = 0;
            companyNameTextBox.Text = strCompanyName;
            UpdatePartyDetails();
            UpdateWorkTypeDetails();
            dateTextBox.Text = DateTime.Today.ToString("dd-MM-yyyy");
            UpdateWorkTypeGstDetails();
            updateItemList();
            addNewButton.Enabled = true;
            updateBill.Enabled = false;
            printButton.Enabled = false;
            deleteButton.Enabled = false;
            viewBillButton.Enabled = true;
            jobReceiptDataGridView.AutoGenerateColumns = false;
        }

        private void jobWorkTypeComboBox_SelectedValueChanged(object sender, EventArgs e)
        {
            if (jobWorkTypeComboBox.SelectedItem.ToString() == "WORK DISPATCH CHALLAN")
            {
                iJobWorkMode = 1;
                UpdateControls();
            }
            else if (jobWorkTypeComboBox.SelectedItem.ToString() == "WORK RECEIVE GOODS CHALLAN")
            {
                iJobWorkMode = 2;
                UpdateControls();
            }
            else
            {
                iJobWorkMode = 3;
                UpdateControls();
            }


        }
        private void UpdateControls()//true = WorkDispatchType false = WorkReceiptType
        {
            if(iJobWorkMode == 1)
            {
                resetAllData();
                jobDispatchDataGridView.Rows.Clear();
                billTypeLabel.Text = "CHALLAN NO.:";
                jobReceiptDataGridView.Hide();
                jobDispatchDataGridView.Show();
                jobChallanReceiveGridView.Hide();
                lSelectedObjects.Clear();
                iSelectedObjectsIndex.Clear();
                detailsGroupBox.Hide();
                label7.Hide();
                billNoTextBox.Hide();
                label31.Hide();
                taxValTextBox.Hide();
                setDefaultChallanNoNo();
                billNoTextBox.Text = "0";
                totalAmountTextBox.Text = "0";
                totalPcsTextBox.Text = "0";
                totalQtyTextBox.Text = "0";
                additionalJobChallanCheckBox.Show();
                updateChallanListForAdditionalJobWork(0);
                addNewButton.Enabled = true;
                updateBill.Enabled = false;
                printButton.Enabled = false;
                deleteButton.Enabled = false;
                viewBillButton.Enabled = true;
                remarkTextBox.Clear();
            }
            else if (iJobWorkMode == 3)
            {
                resetAllData();
                multiColumnItemDisplayDataList.Clear();
                lSelectedObjects.Clear();
                iSelectedObjectsIndex.Clear();
                jobReceiptDataGridView.Rows.Clear();
                //refReceiptComboBox.DataSource = null;
                //refReceiptComboBox.Items.Clear();
                additionalJobChallanCheckBox.Hide();
                billTypeLabel.Text = "VOUCHER NO.:";
                jobReceiptDataGridView.Show();
                jobChallanReceiveGridView.Hide();
                jobDispatchDataGridView.Hide();
                detailsGroupBox.Show();
                label7.Show();
                label7.Text = "BILL NO.";
                billNoTextBox.Show();
                
                label31.Show();
                taxValTextBox.Show();
                setDefaultVoucherNo();
                if (partyNameComboBox.Items.Count > 0)
                    updateReceivedChallanPendingListForChallanReceive((long)partyNameComboBox.SelectedValue, 0);
                billNoTextBox.Text = "0";
                totalAmountTextBox.Text = "0";
                totalPcsTextBox.Text = "0";
                totalQtyTextBox.Text = "0";
                remarkTextBox.Clear();
                addNewButton.Enabled = true;
                updateBill.Enabled = false;
                printButton.Enabled = false;
                deleteButton.Enabled = false;
                viewBillButton.Enabled = true;
            }
            else if (iJobWorkMode == 2)
            {
                resetAllData();
                multiColumnItemDisplayDataList.Clear();
                lSelectedObjects.Clear();
                iSelectedObjectsIndex.Clear();
                jobChallanReceiveGridView.Rows.Clear();
                //refReceiptComboBox.DataSource = null;
                //refReceiptComboBox.Items.Clear();
                billTypeLabel.Text = "VOUCHER NO.:";
                jobReceiptDataGridView.Hide();
                jobChallanReceiveGridView.Show();
                jobDispatchDataGridView.Hide();
                detailsGroupBox.Hide();
                label7.Visible = false;
                //label7.Text = "CHALLAN NO.";
                billNoTextBox.Hide();
                additionalJobChallanCheckBox.Hide();
                label31.Hide();
                taxValTextBox.Hide();
                setDefaultMillReceiptVoucherNo();
                updateReceivedChallanPendingList(0);
                voucherNoTextBox.Focus();
                billNoTextBox.Text = "0";
                totalAmountTextBox.Text = "0";
                totalPcsTextBox.Text = "0";
                totalQtyTextBox.Text = "0";
                remarkTextBox.Clear();
                addNewButton.Enabled = true;
                updateBill.Enabled = false;
                printButton.Enabled = false;
                deleteButton.Enabled = false;
                viewBillButton.Enabled = true;
            }

        }

        private void setDefaultVoucherNo()
        {
            long lSrNo = partyDB.getJobWorkDefaultVoucherNo();
            if (lSrNo == -1)
            {
                MessageBox.Show("Unable to retrieve Job work Voucher Data", "Error", MessageBoxButtons.OK);
                return;
            }
            else if (lSrNo == 0)
            {
                voucherNoTextBox.Text = "1";
            }
            else
                voucherNoTextBox.Text = Convert.ToString(lSrNo + 1);
        }

        private void setDefaultMillReceiptVoucherNo()
        {
            long lSrNo = partyDB.getJobWorkReceiptDefaultVoucherNo();
            if (lSrNo == -1)
            {
                MessageBox.Show("Unable to retrieve Job work Voucher Data", "Error", MessageBoxButtons.OK);
                return;
            }
            else if (lSrNo == 0)
            {
                voucherNoTextBox.Text = "1";
            }
            else
                voucherNoTextBox.Text = Convert.ToString(lSrNo + 1);
        }

        private void setDefaultChallanNoNo()
        {
            long lSrNo = Convert.ToInt64(partyDB.getJobWorkDefaultChallanNo());
            if (lSrNo == -1)
            {
                MessageBox.Show("Unable to retrieve Job work Challan Data", "Error", MessageBoxButtons.OK);
                return;
            }
            else if (lSrNo == 0)
            {
                voucherNoTextBox.Text = "1";
            }
            else
                voucherNoTextBox.Text = Convert.ToString(lSrNo + 1);
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            AccountManagerForm acc = new AccountManagerForm();
            acc.setDefaultAccountType(11);
            acc.ShowDialog();
            UpdatePartyDetails();
        }

        private void UpdatePartyDetails()
        {
            var partyList = DB.getPartyDetails(11);
            if (partyList != null)
            {
                partyNameComboBox.DisplayMember = "party_name";
                partyNameComboBox.ValueMember = "partyid";
                partyNameComboBox.DataSource = partyList;
            }

        }

        private void UpdateWorkTypeGstDetails()
        {
            var partyList = DB.getWorkTypeGstDetails();
            if (partyList != null)
            {
                gstTypeComboBox.DisplayMember = "gstType";
                gstTypeComboBox.ValueMember = "worktypegst_id";
                gstTypeComboBox.DataSource = partyList;
            }

        }

        private void UpdateWorkTypeDetails()
        {
            var workTpeList = DB.getWorkTypeDetails();
            if (workTpeList != null)
            {
                workTypeComboBox.DisplayMember = "worktype";
                workTypeComboBox.ValueMember = "worktype_id";
                workTypeComboBox.DataSource = workTpeList;
            }

        }

        private void dateTextBox_Validating(object sender, CancelEventArgs e)
        {
            DateTime dt;
            bool isValid = DateTime.TryParseExact(dateTextBox.Text, "dd-MM-yyyy", null, DateTimeStyles.None, out dt);
            if (!isValid)
            {
                MessageBox.Show("Please enter the valid date", "DATE", MessageBoxButtons.OK);
                dateTextBox.Focus();
            }
        }

        
        private void fetchPartyDataFromPartyName()
        {
            if (partyNameComboBox.SelectedIndex != -1)
            {
                fillBrokerDetails();
                fillTransportDetails();
                //fillStateCodeList();
                //fillGstTypeList();
                //fillCityList();
                using (var db = new Entities())
                {
                    party_details partyDetail2 = (party_details)partyNameComboBox.SelectedItem;
                    //var partyDetail = db.party_details.Single(x => x.party_name == partyDetail2.party_name);
                    var partyDetail = DB.getPartyDetailFromID(11, partyDetail2.partyid);
                    if (partyDetail == null)
                        return;
                    transportComboBox.SelectedValue = partyDetail.transportid;
                    brokerComboBox.SelectedValue = partyDetail.brokerid;
                    //stationComboBox.SelectedValue = partyDetail.city_id;
                    partyGstnTextBox.Text = partyDetail.gst;
                    string gstn = partyGstnTextBox.Text;
                    if (gstn.Length > 0)
                    {
                        //string stateCode = gstn.Substring(0, 2);
                        //int indexForStateCode = stateComboBox.FindString(stateCode);
                        if (gstn.Substring(0, 2) == strCompanyGstn.Substring(0,2))
                            gstTypeComboBox.SelectedValue = (long)1;
                        else
                            gstTypeComboBox.SelectedValue = (long)2;

                    }


                }
            }
        }

        private void fillBrokerDetails()
        {

            var brokerList = DB.getBrokerDetails();
            if (brokerList != null)
            {
                brokerComboBox.DisplayMember = "party_name";
                brokerComboBox.ValueMember = "brokerid";
                brokerComboBox.DataSource = brokerList;

            }

        }
        private void fillTransportDetails()
        {

            var transportList = DB.getTransportDetails();
            if (transportList != null)
            {
                transportComboBox.DisplayMember = "name";
                transportComboBox.ValueMember = "transportid";
                transportComboBox.DataSource = transportList;
            }

        }

        private void partyNameComboBox_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            if (partyNameComboBox.Items.Count == 0)
                return;
            fetchPartyDataFromPartyName();
            if (iJobWorkMode == 2)
                updateReceivedChallanPendingList(0);
            if (iJobWorkMode == 3)
                updateReceivedChallanPendingListForChallanReceive((long)partyNameComboBox.SelectedValue, 0);
        }

        private void updateReceivedChallanPendingList(int iRowIndex)
        {
            if (partyNameComboBox.Items.Count == 0)
                return;
            var challanList = partyDB.getJobWorkPendingChallanList((long)partyNameComboBox.SelectedValue);
            if (challanList != null)
            {
                multiColumnItemDisplayDataList.Clear();
                for (int i = 0; i < challanList.Count; i++)
                {
                    JobWorkReceiptItemForMultiColumnDisplay itemDisplayData = new JobWorkReceiptItemForMultiColumnDisplay();

                    itemDisplayData.challanNo = challanList[i].challanNo;

                    List<jobprocessreceipt_itemdetails> receiptChallanList = challanList[i].jobprocessreceipt_itemdetails.ToList();

                    itemDisplayData.available_pcs = Convert.ToInt64(challanList[i].dispatch_pcs);
                    itemDisplayData.available_mtrsQty = (double)challanList[i].dispatch_mtrsQty;
                    itemDisplayData.strItemName = DB.getItemNameFromId((long)challanList[i].item_id);
                    itemDisplayData.jobProcessItemDetails_id = (long)challanList[i].jobProcessItemDetails_id;
                    
                    for (int j = 0; j < receiptChallanList.Count; j++)
                    {
                        if (challanList[i].jobProcessItemDetails_id == receiptChallanList[j].jobProcessItemDetails_id)
                        {
                            itemDisplayData.available_pcs = (long)(itemDisplayData.available_pcs - (double)receiptChallanList[j].pcs);
                            itemDisplayData.available_mtrsQty = itemDisplayData.available_mtrsQty - (double)receiptChallanList[j].mtrs;
                        }

                    }
                    if (itemDisplayData.available_pcs > 0 && itemDisplayData.available_mtrsQty > 0)
                        multiColumnItemDisplayDataList.Add(itemDisplayData);
                }

                for(int j = 0;j < iSelectedObjectsIndex.Count; j++)
                {
                    multiColumnItemDisplayDataList[iSelectedObjectsIndex[j]].available_pcs = multiColumnItemDisplayDataList[iSelectedObjectsIndex[j]].available_pcs - Convert.ToInt64(jobChallanReceiveGridView.Rows[j].Cells[pcsReceiveTextBox.Index].Value);
                    multiColumnItemDisplayDataList[iSelectedObjectsIndex[j]].available_mtrsQty = multiColumnItemDisplayDataList[iSelectedObjectsIndex[j]].available_mtrsQty - Convert.ToDouble(jobChallanReceiveGridView.Rows[j].Cells[qunatityReceiveTextBox.Index].Value);
                    if (multiColumnItemDisplayDataList[iSelectedObjectsIndex[j]].available_pcs <= 0 || multiColumnItemDisplayDataList[iSelectedObjectsIndex[j]].available_mtrsQty <= 0)
                        multiColumnItemDisplayDataList.RemoveAt(iSelectedObjectsIndex[j]);
                }
                
                /*
                for (int i = 0;i < challanList.Count;i++)
                {
                    JobWorkReceiptItemForMultiColumnDisplay itemDisplayData = new JobWorkReceiptItemForMultiColumnDisplay();

                    itemDisplayData.challanNo = (long)challanList[i].challanNo;

                    List<jobprocessreceipt_itemdetails> receiptChallanList = challanList[i].jobprocessreceipt_itemdetails.ToList();

                    double dPcs = 0;
                    double dMtrs = 0;
                    for(int j = 0;j < receiptChallanList.Count;j++)
                    {
                        if (challanList[i].jobProcessItemDetails_id == receiptChallanList[j].jobProcessItemDetails_id)
                        {
                            dPcs = dPcs + (double)receiptChallanList[j].pcs;
                            dMtrs = dMtrs + (double)receiptChallanList[j].mtrs;
                         }



                    }
                    double dUsedPcs = 0;
                    double dUsedMtrs = 0;
                    for (int k = 0; k < jobChallanReceiveGridView.RowCount - 1; k++)
                    {
                        //long lJobProcessItemDetailsId = Convert.ToInt64(jobReceiptDataGridView.Rows[k].Cells[refReceiptComboBox.Index].Value);
                        long lJobProcessItemDetailsId = lSelectedObjects[k];
                        if (challanList[i].jobProcessItemDetails_id == lJobProcessItemDetailsId && jobChallanReceiveGridView.Rows[k].Cells[refNoChallanReceiveComboBox.Index].ReadOnly == false)
                        {
                            dUsedPcs = dUsedPcs + Convert.ToDouble(jobChallanReceiveGridView.Rows[k].Cells[pcsReceiveTextBox.Index].Value);
                            dUsedMtrs = dUsedMtrs + Convert.ToDouble(jobChallanReceiveGridView.Rows[k].Cells[qunatityReceiveTextBox.Index].Value);
                        }
                    }


                    itemDisplayData.available_pcs = Convert.ToInt64(challanList[i].dispatch_pcs - dPcs - dUsedPcs);
                    itemDisplayData.available_mtrsQty = (double)challanList[i].dispatch_mtrsQty - dMtrs - dUsedMtrs;
                    itemDisplayData.strItemName = DB.getItemNameFromId((long)challanList[i].item_id);
                    itemDisplayData.jobProcessItemDetails_id = (long)challanList[i].jobProcessItemDetails_id;
                    if(itemDisplayData.available_pcs >0 && itemDisplayData.available_mtrsQty > 0)
                        multiColumnItemDisplayDataList.Add(itemDisplayData);    
                }*/

                DataTable db = ToDataTable(multiColumnItemDisplayDataList);
                /*if(iRowIndex == 0)
                {
                    refNoChallanReceiveComboBox.CellTemplate = new DataGridViewMultiColumnComboCell();
                    refNoChallanReceiveComboBox.DataSource = db;

                    refNoChallanReceiveComboBox.HeaderText = "RefNo";

                    refNoChallanReceiveComboBox.DataPropertyName = "challanNo";
                    refNoChallanReceiveComboBox.DisplayMember = "challanNo";
                    refNoChallanReceiveComboBox.ValueMember = "jobProcessItemDetails_id";
                    //refReceiptComboBox.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
                    refNoChallanReceiveComboBox.Width = 100;
                    refNoChallanReceiveComboBox.DropDownWidth = 300;

                }
                else
                {
                    DataGridViewComboBoxCell cell = new DataGridViewMultiColumnComboCell();
                    cell = (DataGridViewComboBoxCell)jobChallanReceiveGridView.Rows[iRowIndex].Cells[refNoChallanReceiveComboBox.Index];
                    cell.DataSource = db;

                    //refReceiptComboBox.HeaderText = "RefNo";

                    //cell.DataPropertyName = "challanNo";
                    cell.DisplayMember = "challanNo";
                    cell.ValueMember = "jobProcessItemDetails_id";
                    //cell.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
                    //cell.Width = 100;
                    cell.DropDownWidth = 300;

                }
                */
                DataGridViewComboBoxCell cell = new DataGridViewMultiColumnComboCell();
                cell = (DataGridViewComboBoxCell)jobChallanReceiveGridView.Rows[iRowIndex].Cells[refNoChallanReceiveComboBox.Index];
                cell.DataSource = db;

                //refReceiptComboBox.HeaderText = "RefNo";

                //cell.DataPropertyName = "challanNo";
                cell.DisplayMember = "challanNo";
                cell.ValueMember = "jobProcessItemDetails_id";
                //cell.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
                //cell.Width = 100;
                cell.DropDownWidth = 300;

            }   

        }

        private void updateReceivedChallanPendingListForChallanReceive(long lPartyId,int iRowIndex)
        {
            var challanList = partyDB.getReceiveChallanItemList(lPartyId);
            if (challanList != null)
            {
                multiColumnItemDisplayDataList.Clear();
                List<jobprocessreceipt_itemdetails> receiptBillItemDetails = partyDB.getReceiptBillItemList(lPartyId);
                for (int i = 0; i < challanList.Count; i++)
                {
                    bool bItemReceived = false;
                    for(int j = 0;j < receiptBillItemDetails.Count;j++)
                    {
                        if((receiptBillItemDetails[j].jobProcessItemDetails_id == challanList[i].jobProcessItemDetails_id) && (receiptBillItemDetails[j].receiveVoucherNo == challanList[i].receiveVoucherNo))
                        {
                            bItemReceived = true;
                            break;
                        }
                    }
                    if(!bItemReceived)
                    {
                        JobWorkReceiptItemForMultiColumnDisplay itemDisplayData = new JobWorkReceiptItemForMultiColumnDisplay();

                        itemDisplayData.challanNo = challanList[i].challanNo;

                        itemDisplayData.available_pcs = Convert.ToInt64(challanList[i].freshItem);
                        itemDisplayData.available_mtrsQty = (double)challanList[i].mtrs;
                        itemDisplayData.strItemName = DB.getItemNameFromId((long)challanList[i].item_id);
                        //itemDisplayData.jobProcessItemDetails_id = (long)challanList[i].jobProcessItemDetails_id;
                        itemDisplayData.jobProcessItemDetails_id = (long)challanList[i].jobWorkReceiptId;
                        if (itemDisplayData.available_pcs > 0 && itemDisplayData.available_mtrsQty > 0)
                            multiColumnItemDisplayDataList.Add(itemDisplayData);
                    }
                    
                }


                for (int j = 0; j < iSelectedObjectsIndex.Count; j++)
                {
                    multiColumnItemDisplayDataList[iSelectedObjectsIndex[j]].available_pcs = multiColumnItemDisplayDataList[iSelectedObjectsIndex[j]].available_pcs - Convert.ToInt64(jobReceiptDataGridView.Rows[j].Cells[pcsReceiptTextBox.Index].Value);
                    multiColumnItemDisplayDataList[iSelectedObjectsIndex[j]].available_mtrsQty = multiColumnItemDisplayDataList[iSelectedObjectsIndex[j]].available_mtrsQty - Convert.ToDouble(jobReceiptDataGridView.Rows[j].Cells[qtyReceiptTextBox.Index].Value);
                    if (multiColumnItemDisplayDataList[iSelectedObjectsIndex[j]].available_pcs <= 0 || multiColumnItemDisplayDataList[iSelectedObjectsIndex[j]].available_mtrsQty <= 0)
                        multiColumnItemDisplayDataList.RemoveAt(iSelectedObjectsIndex[j]);
                }

                DataTable db = ToDataTable(multiColumnItemDisplayDataList);
                
                DataGridViewComboBoxCell cell = new DataGridViewMultiColumnComboCell();
                cell = (DataGridViewComboBoxCell)jobReceiptDataGridView.Rows[iRowIndex].Cells[refReceiptComboBox.Index];
                cell.DataSource = db;

                //refReceiptComboBox.HeaderText = "RefNo";

                //cell.DataPropertyName = "challanNo";
                cell.DisplayMember = "challanNo";
                cell.ValueMember = "jobProcessItemDetails_id";
                //cell.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
                //cell.Width = 100;
                cell.DropDownWidth = 300;

            }

        }

        private void updateChallanListForAdditionalJobWork(int iRowIndex)
        {
            if(additionalJobChallanCheckBox.Checked)
            {
                var challanList = partyDB.getReceiveChallanItemListForAdditionalJobWork();
                if (challanList != null)
                {
                    multiColumnItemDisplayDataList.Clear();
                    for (int i = 0; i < challanList.Count; i++)
                    {
                        JobWorkReceiptItemForMultiColumnDisplay itemDisplayData = new JobWorkReceiptItemForMultiColumnDisplay();

                        itemDisplayData.challanNo = challanList[i].challanNo;
                        /*double dUsedPcs = 0;
                        double dUsedMtrs = 0;
                        for (int k = 0; k < jobReceiptDataGridView.RowCount - 1; k++)
                        {
                            //long lJobProcessItemDetailsId = Convert.ToInt64(jobReceiptDataGridView.Rows[k].Cells[refReceiptComboBox.Index].Value);
                            long lJobProcessItemDetailsId = lSelectedObjects[k];
                            if (challanList[i].jobProcessItemDetails_id == lJobProcessItemDetailsId)
                            {
                                dUsedPcs = dUsedPcs + Convert.ToDouble(jobReceiptDataGridView.Rows[k].Cells[pcsReceiptTextBox.Index].Value);
                                dUsedMtrs = dUsedMtrs + Convert.ToDouble(jobReceiptDataGridView.Rows[k].Cells[qtyReceiptTextBox.Index].Value);
                            }
                        }*/

                        itemDisplayData.available_pcs = Convert.ToInt64(challanList[i].freshItem);
                        itemDisplayData.available_mtrsQty = (double)challanList[i].mtrs;
                        itemDisplayData.strItemName = DB.getItemNameFromId((long)challanList[i].item_id);
                        itemDisplayData.jobProcessItemDetails_id = (long)challanList[i].jobWorkReceiptId;
                        if (itemDisplayData.available_pcs > 0 && itemDisplayData.available_mtrsQty > 0)
                            multiColumnItemDisplayDataList.Add(itemDisplayData);


                    }
                    for (int j = 0; j < iSelectedObjectsIndex.Count; j++)
                    {
                        multiColumnItemDisplayDataList[iSelectedObjectsIndex[j]].available_pcs = multiColumnItemDisplayDataList[iSelectedObjectsIndex[j]].available_pcs - Convert.ToInt64(jobDispatchDataGridView.Rows[j].Cells[pcsTextBox.Index].Value);
                        multiColumnItemDisplayDataList[iSelectedObjectsIndex[j]].available_mtrsQty = multiColumnItemDisplayDataList[iSelectedObjectsIndex[j]].available_mtrsQty - Convert.ToDouble(jobDispatchDataGridView.Rows[j].Cells[qtyTextBox.Index].Value);
                        if (multiColumnItemDisplayDataList[iSelectedObjectsIndex[j]].available_pcs <= 0 || multiColumnItemDisplayDataList[iSelectedObjectsIndex[j]].available_mtrsQty <= 0)
                            multiColumnItemDisplayDataList.RemoveAt(iSelectedObjectsIndex[j]);
                    }

                    DataTable db = ToDataTable(multiColumnItemDisplayDataList);

                    DataGridViewComboBoxCell cell = new DataGridViewMultiColumnComboCell();
                    cell = (DataGridViewComboBoxCell)jobDispatchDataGridView.Rows[iRowIndex].Cells[refNoComboBox.Index];
                    cell.DataSource = db;

                    //refReceiptComboBox.HeaderText = "RefNo";

                    //cell.DataPropertyName = "challanNo";
                    cell.DisplayMember = "challanNo";
                    cell.ValueMember = "jobProcessItemDetails_id";
                    //cell.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
                    //cell.Width = 100;
                    cell.DropDownWidth = 300;

                }
            }
            else
            {
                var finishPurchseBills = partyDB.getFinishPurchaseDetail();
                if(finishPurchseBills != null)
                {
                    multiColumnItemDisplayDataList.Clear();
                    for (int i = 0;i < finishPurchseBills.Count;i++)
                    {
                        List<purchaseitem_details> purchaseItemDetailsList = finishPurchseBills[i].purchaseitem_details.ToList();
                        for (int j = 0; j < purchaseItemDetailsList.Count; j++)
                        {
                            List<jobprocess_itemdetails> receiveDispatchList = partyDB.ckeckIfFinishPurchaseReferenceInJobWorkExists((long)purchaseItemDetailsList[j].item_id);
                            if (receiveDispatchList == null || receiveDispatchList.Count == 0)
                            {
                                //List<purchaseitem_details> purchaseItemDetailsList = finishPurchseBills[i].purchaseitem_details.ToList();
                                //for (int j = 0; j < purchaseItemDetailsList.Count; j++)
                                {
                                    JobWorkReceiptItemForMultiColumnDisplay itemDisplayData = new JobWorkReceiptItemForMultiColumnDisplay();
                                    itemDisplayData.challanNo = finishPurchseBills[i].billno;
                                    itemDisplayData.available_mtrsQty = Convert.ToDouble(purchaseItemDetailsList[j].quantity);
                                    itemDisplayData.available_pcs = Convert.ToInt64(purchaseItemDetailsList[j].pcs);
                                    itemDisplayData.strItemName = DB.getItemNameFromId((long)purchaseItemDetailsList[j].purchaseitem_id);
                                    itemDisplayData.jobProcessItemDetails_id = Convert.ToInt64(purchaseItemDetailsList[j].item_id);
                                    multiColumnItemDisplayDataList.Add(itemDisplayData);
                                }
                            }
                            else
                            {
                                //List<purchaseitem_details> purchaseItemDetailsList = finishPurchseBills[i].purchaseitem_details.ToList();
                                //for (int j = 0; j < purchaseItemDetailsList.Count; j++)
                                {
                                    JobWorkReceiptItemForMultiColumnDisplay itemDisplayData = new JobWorkReceiptItemForMultiColumnDisplay();
                                    itemDisplayData.challanNo = finishPurchseBills[i].billno;
                                    itemDisplayData.available_mtrsQty = Convert.ToDouble(purchaseItemDetailsList[j].quantity);
                                    itemDisplayData.available_pcs = Convert.ToInt64(purchaseItemDetailsList[j].pcs);
                                    itemDisplayData.strItemName = DB.getItemNameFromId((long)purchaseItemDetailsList[j].purchaseitem_id);
                                    itemDisplayData.jobProcessItemDetails_id = Convert.ToInt64(purchaseItemDetailsList[j].item_id);
                                    for (int k = 0; k < receiveDispatchList.Count; k++)
                                    {
                                        if (purchaseItemDetailsList[j].item_id == receiveDispatchList[k].dispatchReference_id)
                                        {
                                            itemDisplayData.available_pcs = Convert.ToInt64(itemDisplayData.available_pcs - receiveDispatchList[k].dispatch_pcs);
                                            itemDisplayData.available_mtrsQty = Convert.ToDouble(itemDisplayData.available_mtrsQty - receiveDispatchList[k].dispatch_mtrsQty);
                                        }
                                        if ((k + 1) >= receiveDispatchList.Count)
                                        {
                                            if (itemDisplayData.available_pcs > 0 && itemDisplayData.available_mtrsQty > 0)
                                                multiColumnItemDisplayDataList.Add(itemDisplayData);
                                        }
                                    }
                                }
                            }
                        }
                        
                    }

                    for (int j = 0; j < iSelectedObjectsIndex.Count; j++)
                    {
                        multiColumnItemDisplayDataList[iSelectedObjectsIndex[j]].available_pcs = multiColumnItemDisplayDataList[iSelectedObjectsIndex[j]].available_pcs - Convert.ToInt64(jobDispatchDataGridView.Rows[j].Cells[pcsTextBox.Index].Value);
                        multiColumnItemDisplayDataList[iSelectedObjectsIndex[j]].available_mtrsQty = multiColumnItemDisplayDataList[iSelectedObjectsIndex[j]].available_mtrsQty - Convert.ToDouble(jobDispatchDataGridView.Rows[j].Cells[qtyTextBox.Index].Value);
                        if (multiColumnItemDisplayDataList[iSelectedObjectsIndex[j]].available_pcs <= 0 || multiColumnItemDisplayDataList[iSelectedObjectsIndex[j]].available_mtrsQty <= 0)
                            multiColumnItemDisplayDataList.RemoveAt(iSelectedObjectsIndex[j]);
                    }

                    DataTable db = ToDataTable(multiColumnItemDisplayDataList);

                    DataGridViewComboBoxCell cell = new DataGridViewMultiColumnComboCell();
                    cell = (DataGridViewComboBoxCell)jobDispatchDataGridView.Rows[iRowIndex].Cells[refNoComboBox.Index];
                    cell.DataSource = db;
                    
                    //refReceiptComboBox.HeaderText = "RefNo";

                    //cell.DataPropertyName = "challanNo";
                    cell.DisplayMember = "challanNo";
                    cell.ValueMember = "jobProcessItemDetails_id";
                    //cell.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
                    //cell.Width = 100;
                    cell.DropDownWidth = 400;
                }
            }
            

        }


        public DataTable ToDataTable<T>(List<T> items)

        {

            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties

            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);

            foreach (PropertyInfo prop in Props)

            {

                //Setting column names as Property names

                dataTable.Columns.Add(prop.Name);
               
            }

            foreach (T item in items)

            {

                var values = new object[Props.Length];

                for (int i = 0; i < Props.Length; i++)

                {
                    
                    //inserting property values to datatable rows

                    values[i] = Props[i].GetValue(item, null);

                }

                dataTable.Rows.Add(values);

            }

            //put a breakpoint here and check datatable

            return dataTable;

        }
        private void updateItemList()
        {
            var itemList = DB.getItemList();
            if (itemList != null)
            {
                itemNameComboBox.DisplayMember = "itemname";
                itemNameComboBox.ValueMember = "itemid";
                itemNameComboBox.DataSource = itemList;
            }

        }

        private void jobDispatchDataGridView_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (jobDispatchDataGridView.CurrentCell.ColumnIndex == itemNameComboBox.Index && e.Control is ComboBox)
            {
                ComboBox comboBox = e.Control as ComboBox;
                //comboBox.SelectedIndex = -1;
                comboBox.SelectedIndexChanged -= LastColumnComboSelectionChanged;
                comboBox.SelectedIndexChanged += LastColumnComboSelectionChanged;

            }
            else if (jobDispatchDataGridView.CurrentCell.ColumnIndex == refNoComboBox.Index && e.Control is ComboBox)
            {
                ComboBox comboBox = e.Control as ComboBox;
                //comboBox.SelectedIndex = -1;
                comboBox.SelectedIndexChanged -= refNoDispatchComboSelectionChanged;
                comboBox.SelectedIndexChanged += refNoDispatchComboSelectionChanged;

            }

        }

        private void refNoDispatchComboSelectionChanged(object sender, EventArgs e)
        {
            long lJobProcssDispatchItemId = 0;
            int iJobProcssDispatchItemSelectedIndex = -1;
            try
            {
                lJobProcssDispatchItemId = Convert.ToInt64(((ComboBox)sender).SelectedValue);
                iJobProcssDispatchItemSelectedIndex = Convert.ToInt32(((ComboBox)sender).SelectedIndex);
            }
            catch (Exception ex)
            {
                return;
            }
            if (lSelectedObjects.Count == (jobDispatchDataGridView.CurrentRow.Index + 1))
            {
                lSelectedObjects[jobDispatchDataGridView.CurrentRow.Index] = lJobProcssDispatchItemId;
                iSelectedObjectsIndex[jobDispatchDataGridView.CurrentRow.Index] = iJobProcssDispatchItemSelectedIndex;
            }
            else
            {
                lSelectedObjects.Add(lJobProcssDispatchItemId);
                iSelectedObjectsIndex.Add(iJobProcssDispatchItemSelectedIndex);
            }
            if (additionalJobChallanCheckBox.Checked)
            {
                jobprocessreceipt_itemdetails jobWorkItemData = partyDB.getReceiveChallanItemDataForAdditionalJobWork(Convert.ToInt64(lJobProcssDispatchItemId));
                if (jobWorkItemData == null)
                {
                    //MessageBox.Show("No Pending Item in Challan", "ERROR", MessageBoxButtons.OK);
                    return;
                }

                var itemList = DB.getItemList();
                List<item_details> SelectedChallanItemList = new List<item_details>(); ;


                if (itemList != null)
                {
                    item_details itemDataList = itemList.Where(x => x.itemid == jobWorkItemData.item_id).SingleOrDefault();
                    if (!SelectedChallanItemList.Contains(itemDataList))
                        SelectedChallanItemList.Add(itemDataList);
                }

                    DataGridViewComboBoxCell cell = (DataGridViewComboBoxCell)jobDispatchDataGridView.Rows[jobDispatchDataGridView.CurrentRow.Index].Cells[itemNameComboBox.Index];
                    cell.DataSource = SelectedChallanItemList;
                    cell.DisplayMember = "itemname";
                    cell.ValueMember = "itemid";

                

                //lSelectedObjects.Add(lJobProcssDispatchItemId);
                //iSelectedObjectsIndex.Add(iJobProcssDispatchItemSelectedIndex);    
                jobprocessreceipt_itemdetails itemDetails = partyDB.getReceiveChallanItemDataForAdditionalJobWork(lJobProcssDispatchItemId);
                if (itemDetails != null)
                {

                    jobDispatchDataGridView.Rows[jobDispatchDataGridView.CurrentRow.Index].Cells[itemNameComboBox.Index].Value = itemDetails.item_id;
                    jobDispatchDataGridView.Rows[jobDispatchDataGridView.CurrentRow.Index].Cells[hsncTextBox.Index].Value = itemDetails.hsnCode;

                    jobDispatchDataGridView.Rows[jobDispatchDataGridView.CurrentRow.Index].Cells[unitComboBox.Index].Value = itemDetails.unit;

                    jobDispatchDataGridView.Rows[jobDispatchDataGridView.CurrentRow.Index].Cells[cutTextBox.Index].Value = itemDetails.cut;

                    if (multiColumnItemDisplayDataList != null)
                    {
                        if (multiColumnItemDisplayDataList.Count > 0)
                        {
                            jobDispatchDataGridView.Rows[jobDispatchDataGridView.CurrentRow.Index].Cells[pcsTextBox.Index].Value = multiColumnItemDisplayDataList[iJobProcssDispatchItemSelectedIndex].available_pcs;

                            jobDispatchDataGridView.Rows[jobDispatchDataGridView.CurrentRow.Index].Cells[qtyTextBox.Index].Value = multiColumnItemDisplayDataList[iJobProcssDispatchItemSelectedIndex].available_mtrsQty;

                        }
                    }
                    
                }
            }
            else
            {
                List<purchaseitem_details> jobWorkItemList = partyDB.getPurchaseBillItemDetails(Convert.ToInt64(lJobProcssDispatchItemId));
                if (jobWorkItemList == null)
                {
                    //MessageBox.Show("No Pending Item in Challan", "ERROR", MessageBoxButtons.OK);
                    return;
                }

                var itemList = DB.getItemList();
                List<item_details> SelectedChallanItemList = new List<item_details>(); ;


                if (itemList != null)
                {
                    for (int i = 0; i < jobWorkItemList.Count; i++)
                    {
                        item_details itemDataList = itemList.Where(x => x.itemid == jobWorkItemList[i].purchaseitem_id).SingleOrDefault();
                        if (!SelectedChallanItemList.Contains(itemDataList))
                            SelectedChallanItemList.Add(itemDataList);
                    }


                    DataGridViewComboBoxCell cell = (DataGridViewComboBoxCell)jobDispatchDataGridView.Rows[jobDispatchDataGridView.CurrentRow.Index].Cells[itemNameComboBox.Index];
                    cell.DataSource = SelectedChallanItemList;
                    cell.DisplayMember = "itemname";
                    cell.ValueMember = "itemid";

                }

                //lSelectedObjects.Add(lJobProcssDispatchItemId);
                //iSelectedObjectsIndex.Add(iJobProcssDispatchItemSelectedIndex);    
                purchaseitem_details itemDetails = partyDB.getPurchaseBillItemData(lJobProcssDispatchItemId);
                if (itemDetails != null)
                {

                    jobDispatchDataGridView.Rows[jobDispatchDataGridView.CurrentRow.Index].Cells[itemNameComboBox.Index].Value = itemDetails.purchaseitem_id;
                    jobDispatchDataGridView.Rows[jobDispatchDataGridView.CurrentRow.Index].Cells[hsncTextBox.Index].Value = itemDetails.hsncode;

                    jobDispatchDataGridView.Rows[jobDispatchDataGridView.CurrentRow.Index].Cells[unitComboBox.Index].Value = itemDetails.unit;

                    jobDispatchDataGridView.Rows[jobDispatchDataGridView.CurrentRow.Index].Cells[cutTextBox.Index].Value = itemDetails.cut;

                    if (multiColumnItemDisplayDataList != null)
                    {
                        if (multiColumnItemDisplayDataList.Count > 0)
                        {
                            jobDispatchDataGridView.Rows[jobDispatchDataGridView.CurrentRow.Index].Cells[pcsTextBox.Index].Value = multiColumnItemDisplayDataList[iJobProcssDispatchItemSelectedIndex].available_pcs;

                            jobDispatchDataGridView.Rows[jobDispatchDataGridView.CurrentRow.Index].Cells[qtyTextBox.Index].Value = multiColumnItemDisplayDataList[iJobProcssDispatchItemSelectedIndex].available_mtrsQty;

                        }
                    }
                    /*if (itemDetails.unit == "PCS")
                    {
                        jobDispatchDataGridView.Rows[jobDispatchDataGridView.CurrentRow.Index].Cells[amountTextBox.Index].Value = Convert.ToInt64(jobDispatchDataGridView.Rows[jobDispatchDataGridView.CurrentRow.Index].Cells[pcsTextBox.Index].Value) * itemDetails.rate;

                        //jobDispatchDataGridView.Rows[jobReceiptDataGridView.CurrentRow.Index].Cells[freshTextBox.Index].Value = jobDispatchDataGridView.Rows[jobDispatchDataGridView.CurrentRow.Index].Cells[pcsTextBox.Index].Value;

                    }
                    else
                    {
                        jobDispatchDataGridView.Rows[jobDispatchDataGridView.CurrentRow.Index].Cells[amountTextBox.Index].Value = Convert.ToInt64(jobDispatchDataGridView.Rows[jobDispatchDataGridView.CurrentRow.Index].Cells[qtyTextBox.Index].Value) * itemDetails.rate;
                        //jobReceiptDataGridView.Rows[jobReceiptDataGridView.CurrentRow.Index].Cells[freshTextBox.Index].Value = jobReceiptDataGridView.Rows[jobReceiptDataGridView.CurrentRow.Index].Cells[qtyReceiptTextBox.Index].Value;
                    }*/

                }
            }
        }
        private void RefNoColumnComboSelectionChanged(object sender, EventArgs e)
        {
            string strChallanNo = (string)jobReceiptDataGridView.Rows[jobReceiptDataGridView.CurrentRow.Index].Cells[refReceiptComboBox.Index].EditedFormattedValue.ToString();

            
            if (refReceiptComboBox.Items != null)
            {
                if (strChallanNo == "")
                {
                    //MessageBox.Show("Please Select Challan No", "ERROR", MessageBoxButtons.OK);
                    return;
                }
                //jobprocess_details selectedChallan = (jobprocess_details)jobReceiptDataGridView.Rows[jobReceiptDataGridView.CurrentRow.Index].Cells[refReceiptComboBox.Index].Value;
                //List<jobprocess_itemdetails> jobWorkItemList = selectedChallan.jobprocess_itemdetails.ToList();//partyDB.getJobWorkPendingChallanItemList(Convert.ToInt64(strChallanNo));
                List<jobprocess_itemdetails> jobWorkItemList = partyDB.getJobWorkPendingChallanItemList(strChallanNo);
                if (jobWorkItemList == null)
                {
                    MessageBox.Show("No Pending Item in Challan", "ERROR", MessageBoxButtons.OK);
                    return;
                }

                var itemList = DB.getItemList();
                List<item_details> SelectedChallanItemList = new List<item_details>(); ;

                
                if (itemList != null)
                {
                    for(int i = 0;i < jobWorkItemList.Count;i++)
                    {
                        item_details itemDetail = itemList.Where(x=>x.itemid == jobWorkItemList[i].item_id).SingleOrDefault();
                        if (!SelectedChallanItemList.Contains(itemDetail))
                            SelectedChallanItemList.Add(itemDetail);
                    }
                    
                    
                    DataGridViewComboBoxCell cell = (DataGridViewComboBoxCell)jobReceiptDataGridView.Rows[jobReceiptDataGridView.CurrentRow.Index].Cells[itemNameReceiptComboBox.Index];
                    cell.DataSource = SelectedChallanItemList;
                    cell.DisplayMember = "itemname";
                    cell.ValueMember = "itemid";
                    
                }
                long lJobProcssDispatchItemId = 0;
                int iJobProcssDispatchItemSelectedIndex = -1;
                try
                {
                    lJobProcssDispatchItemId = Convert.ToInt64(((ComboBox)sender).SelectedValue);
                    iJobProcssDispatchItemSelectedIndex = Convert.ToInt32(((ComboBox)sender).SelectedIndex);
                }
                catch(Exception ex)
                {
                    return;
                }
                if (lSelectedObjects.Count == (jobReceiptDataGridView.CurrentRow.Index + 1))
                {
                    lSelectedObjects[jobReceiptDataGridView.CurrentRow.Index] = lJobProcssDispatchItemId;
                    iSelectedObjectsIndex[jobReceiptDataGridView.CurrentRow.Index] = iJobProcssDispatchItemSelectedIndex;
                }
                else
                {
                    lSelectedObjects.Add(lJobProcssDispatchItemId);
                    iSelectedObjectsIndex.Add(iJobProcssDispatchItemSelectedIndex);
                }
                //lSelectedObjects.Add(lJobProcssDispatchItemId);
                //iSelectedObjectsIndex.Add(iJobProcssDispatchItemSelectedIndex);    
                jobprocessreceipt_itemdetails itemDetails = partyDB.getJobWorkReceiptChallanItemForBill(lJobProcssDispatchItemId);
                if(itemDetails != null)
                {
                    jobReceiptDataGridView.Rows[jobReceiptDataGridView.CurrentRow.Index].Cells[jobTypeReceiptTextBox.Index].Value =  itemDetails.jobTypeDetails;
                    jobReceiptDataGridView.Rows[jobReceiptDataGridView.CurrentRow.Index].Cells[itemNameReceiptComboBox.Index].Value = itemDetails.item_id;
                    jobReceiptDataGridView.Rows[jobReceiptDataGridView.CurrentRow.Index].Cells[hsncodeReceiptTextBox.Index].Value = itemDetails.hsnCode;
                    jobReceiptDataGridView.Rows[jobReceiptDataGridView.CurrentRow.Index].Cells[hsncodeReceiptTextBox.Index].ReadOnly = true;
                    jobReceiptDataGridView.Rows[jobReceiptDataGridView.CurrentRow.Index].Cells[unitReceiptComboBox.Index].Value = itemDetails.unit;
                    jobReceiptDataGridView.Rows[jobReceiptDataGridView.CurrentRow.Index].Cells[unitReceiptComboBox.Index].ReadOnly = true;
                    jobReceiptDataGridView.Rows[jobReceiptDataGridView.CurrentRow.Index].Cells[cutReceiptTextBox.Index].Value = itemDetails.cut;
                    jobReceiptDataGridView.Rows[jobReceiptDataGridView.CurrentRow.Index].Cells[cutReceiptTextBox.Index].ReadOnly = true;
                    jobReceiptDataGridView.Rows[jobReceiptDataGridView.CurrentRow.Index].Cells[rateReceiptTextBox.Index].Value = itemDetails.rate;
                    jobReceiptDataGridView.Rows[jobReceiptDataGridView.CurrentRow.Index].Cells[rateReceiptTextBox.Index].ReadOnly = true;
                    jobReceiptDataGridView.Rows[jobReceiptDataGridView.CurrentRow.Index].Cells[plainTextBox.Index].Value = itemDetails.plainItem;
                    jobReceiptDataGridView.Rows[jobReceiptDataGridView.CurrentRow.Index].Cells[plainTextBox.Index].ReadOnly = true;
                    jobReceiptDataGridView.Rows[jobReceiptDataGridView.CurrentRow.Index].Cells[secTextBox.Index].Value = itemDetails.secondItem;
                    jobReceiptDataGridView.Rows[jobReceiptDataGridView.CurrentRow.Index].Cells[secTextBox.Index].ReadOnly = true;
                    jobReceiptDataGridView.Rows[jobReceiptDataGridView.CurrentRow.Index].Cells[shortTextBox.Index].Value = itemDetails.shortItem;
                    jobReceiptDataGridView.Rows[jobReceiptDataGridView.CurrentRow.Index].Cells[shortTextBox.Index].ReadOnly = true;


                    if (multiColumnItemDisplayDataList!= null)
                    {
                        if (multiColumnItemDisplayDataList.Count > 0)
                        {
                            jobReceiptDataGridView.Rows[jobReceiptDataGridView.CurrentRow.Index].Cells[pcsReceiptTextBox.Index].Value = itemDetails.pcs;
                            jobReceiptDataGridView.Rows[jobReceiptDataGridView.CurrentRow.Index].Cells[pcsReceiptTextBox.Index].ReadOnly = true;
                            jobReceiptDataGridView.Rows[jobReceiptDataGridView.CurrentRow.Index].Cells[qtyReceiptTextBox.Index].Value = itemDetails.mtrs;
                            jobReceiptDataGridView.Rows[jobReceiptDataGridView.CurrentRow.Index].Cells[qtyReceiptTextBox.Index].ReadOnly = true;
                        }
                        /*List<JobWorkReceiptItemForMultiColumnDisplay> avialableChallanData = multiColumnItemDisplayDataList.Where(x => x.jobProcessItemDetails_id == lJobProcssDispatchItemId).ToList();
                        if(avialableChallanData != null || avialableChallanData.Count > 0)
                        {
                            if(avialableChallanData.Count > 1)
                            {
                                jobReceiptDataGridView.Rows[jobReceiptDataGridView.CurrentRow.Index].Cells[pcsReceiptTextBox.Index].Value = avialableChallanData[iJobProcssDispatchItemSelectedIndex].available_pcs;
                                jobReceiptDataGridView.Rows[jobReceiptDataGridView.CurrentRow.Index].Cells[pcsReceiptTextBox.Index].ReadOnly = true;
                                jobReceiptDataGridView.Rows[jobReceiptDataGridView.CurrentRow.Index].Cells[qtyReceiptTextBox.Index].Value = avialableChallanData[iJobProcssDispatchItemSelectedIndex].available_mtrsQty;
                                jobReceiptDataGridView.Rows[jobReceiptDataGridView.CurrentRow.Index].Cells[qtyReceiptTextBox.Index].ReadOnly = true;
                            }
                            else
                            {
                                jobReceiptDataGridView.Rows[jobReceiptDataGridView.CurrentRow.Index].Cells[pcsReceiptTextBox.Index].Value = avialableChallanData[0].available_pcs;
                                jobReceiptDataGridView.Rows[jobReceiptDataGridView.CurrentRow.Index].Cells[pcsReceiptTextBox.Index].ReadOnly = true;
                                jobReceiptDataGridView.Rows[jobReceiptDataGridView.CurrentRow.Index].Cells[qtyReceiptTextBox.Index].Value = avialableChallanData[0].available_mtrsQty;
                                jobReceiptDataGridView.Rows[jobReceiptDataGridView.CurrentRow.Index].Cells[qtyReceiptTextBox.Index].ReadOnly = true;
                            }
                            
                        }
                        */

                    }
                    if (itemDetails.unit == "PCS")
                    {
                        jobReceiptDataGridView.Rows[jobReceiptDataGridView.CurrentRow.Index].Cells[freshTextBox.Index].Value = multiColumnItemDisplayDataList[iJobProcssDispatchItemSelectedIndex].available_pcs;
                        jobReceiptDataGridView.Rows[jobReceiptDataGridView.CurrentRow.Index].Cells[amountReceiptTextBox.Index].Value = Convert.ToInt64(jobReceiptDataGridView.Rows[jobReceiptDataGridView.CurrentRow.Index].Cells[pcsReceiptTextBox.Index].Value) * itemDetails.rate;
                        
                    }
                    else
                    {
                        jobReceiptDataGridView.Rows[jobReceiptDataGridView.CurrentRow.Index].Cells[amountReceiptTextBox.Index].Value = multiColumnItemDisplayDataList[iJobProcssDispatchItemSelectedIndex].available_mtrsQty * itemDetails.rate;
                        jobReceiptDataGridView.Rows[jobReceiptDataGridView.CurrentRow.Index].Cells[freshTextBox.Index].Value = multiColumnItemDisplayDataList[iJobProcssDispatchItemSelectedIndex].available_pcs;//multiColumnItemDisplayDataList[iJobProcssDispatchItemSelectedIndex].available_mtrsQty;
                    }
                    jobReceiptDataGridView.Rows[jobReceiptDataGridView.CurrentRow.Index].Cells[amountReceiptTextBox.Index].ReadOnly = true;
                    jobReceiptDataGridView.Rows[jobReceiptDataGridView.CurrentRow.Index].Cells[freshTextBox.Index].ReadOnly = true;
                }
            }

        }
        private void LastColumnComboSelectionChanged(object sender, EventArgs e)
        {

            string strItemName = (string)jobDispatchDataGridView.Rows[jobDispatchDataGridView.CurrentRow.Index].Cells[itemNameComboBox.Index].EditedFormattedValue.ToString();
            if(strItemName == "")
            {
                MessageBox.Show("Please Select a valid Item", "ERROR", MessageBoxButtons.OK);
                return;
            }

            item_details itemDetail = DB.getItemDetails(strItemName);
            if(itemDetail == null)
            {
                MessageBox.Show("Item Details not fetched", "ERROR", MessageBoxButtons.OK);
                return;
            }
            jobDispatchDataGridView.Rows[jobDispatchDataGridView.CurrentRow.Index].Cells[hsncTextBox.Index].Value = itemDetail.hsncode;
            //jobDispatchDataGridView.Rows[jobDispatchDataGridView.CurrentRow.Index].Cells[unitComboBox.Index].Value = itemDetail.unit_id;
        }

        private void jobDispatchDataGridView_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (jobDispatchDataGridView.Rows.Count > 0 && (e.RowIndex <= jobDispatchDataGridView.Rows.Count - 1) && jobDispatchDataGridView.Rows[e.RowIndex].ReadOnly == false)
            {
                if (e.ColumnIndex == refNoComboBox.Index)
                {
                    /*if ((int)jobDispatchDataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].EditedFormattedValue.ToString().Length <= 0)
                    {
                        if (refNoComboBox.Items.Count != 0)
                        {
                            MessageBox.Show("Please select the correct item", "INPUT ERROR", MessageBoxButtons.OK);
                            //saleDataGridView.CurrentCell = saleDataGridView[e.RowIndex, e.ColumnIndex];
                            jobDispatchDataGridView.CurrentCell.Selected = true;
                            e.Cancel = true;
                        }

                    }*/
                    if (jobDispatchDataGridView.Rows.Count > e.RowIndex + 1)
                        jobDispatchDataGridView.Rows[e.RowIndex + 1].ReadOnly = true;
                }
                else if (e.ColumnIndex == itemNameComboBox.Index)
                {
                    if ((int)jobDispatchDataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].EditedFormattedValue.ToString().Length <= 0)
                        if (itemNameComboBox.Items.Count != 0)
                        {
                            MessageBox.Show("Please select the correct item", "INPUT ERROR", MessageBoxButtons.OK);
                            //saleDataGridView.CurrentCell = saleDataGridView[e.RowIndex, e.ColumnIndex];
                            jobDispatchDataGridView.CurrentCell.Selected = true;
                            e.Cancel = true;
                        }
                    if (jobDispatchDataGridView.Rows.Count > e.RowIndex + 1)
                        jobDispatchDataGridView.Rows[e.RowIndex + 1].ReadOnly = true;
                }
                else if (e.ColumnIndex == pcsTextBox.Index)
                {
                    if ((int)jobDispatchDataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].EditedFormattedValue.ToString().Length <= 0)
                    {
                        MessageBox.Show("Please enter the correct PCS", "INPUT ERROR", MessageBoxButtons.OK);

                        jobDispatchDataGridView.CurrentCell.Selected = true;
                        e.Cancel = true;
                    }
                    else
                    {
                        double dPcs = 0;
                        bool bFlag = double.TryParse(jobDispatchDataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].EditedFormattedValue.ToString(), out dPcs);
                        double dCut = 0;
                        bFlag = double.TryParse(jobDispatchDataGridView.Rows[e.RowIndex].Cells[cutTextBox.Index].EditedFormattedValue.ToString(), out dCut);
                        double dMtrsQuantity = dPcs * dCut;
                        jobDispatchDataGridView.Rows[e.RowIndex].Cells[qtyTextBox.Index].Value = Convert.ToString(dMtrsQuantity);
                        double dTotalPcs = 0;
                        for (int i = 0; i < jobDispatchDataGridView.Rows.Count - 1; i++)
                        {
                            double dTempPcs = 0;
                            double.TryParse(jobDispatchDataGridView.Rows[e.RowIndex].Cells[pcsTextBox.Index].EditedFormattedValue.ToString(), out dTempPcs);
                            dTotalPcs = dTotalPcs + dTempPcs;
                        }
                        totalPcsTextBox.Text = Convert.ToString(dTotalPcs);

                    }
                }
                else if (e.ColumnIndex == cutTextBox.Index)
                {
                    if ((int)jobDispatchDataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].EditedFormattedValue.ToString().Length <= 0)
                    {
                        MessageBox.Show("Please enter the correct cut", "INPUT ERROR", MessageBoxButtons.OK);

                        jobDispatchDataGridView.CurrentCell.Selected = true;
                        e.Cancel = true;
                    }
                    else
                    {
                        double dPcs = 0;
                        bool bFlag = double.TryParse(jobDispatchDataGridView.Rows[e.RowIndex].Cells[pcsTextBox.Index].EditedFormattedValue.ToString(), out dPcs);
                        double dCut = 0;
                        double.TryParse(jobDispatchDataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].EditedFormattedValue.ToString(), out dCut);
                        double dMtrsQuantity = dPcs * dCut;
                        jobDispatchDataGridView.Rows[e.RowIndex].Cells[qtyTextBox.Index].Value = Convert.ToString(dMtrsQuantity);
                        double dTotalMtrsQuantity = 0;
                        for (int i = 0; i < jobDispatchDataGridView.Rows.Count - 1; i++)
                        {
                            double dTempMtrsQuantity = 0;
                            double.TryParse(jobDispatchDataGridView.Rows[e.RowIndex].Cells[qtyTextBox.Index].EditedFormattedValue.ToString(), out dTempMtrsQuantity);
                            dTotalMtrsQuantity = dTotalMtrsQuantity + dTempMtrsQuantity;
                        }

                        totalQtyTextBox.Text = Convert.ToString(dTotalMtrsQuantity);

                    }
                }
                else if (e.ColumnIndex == rateTextBox.Index)
                {
                    if ((int)jobDispatchDataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].EditedFormattedValue.ToString().Length <= 0)
                    {
                        MessageBox.Show("Please enter the correct RATE", "INPUT ERROR", MessageBoxButtons.OK);
                        jobDispatchDataGridView.CurrentCell.Selected = true;
                        e.Cancel = true;
                    }
                    else
                    {
                        if (jobDispatchDataGridView.Rows[e.RowIndex].Cells[unitComboBox.Index].EditedFormattedValue.ToString() == "PCS")
                        {
                            double dPcs = 0;
                            bool bFlag = double.TryParse(jobDispatchDataGridView.Rows[e.RowIndex].Cells[pcsTextBox.Index].EditedFormattedValue.ToString(), out dPcs);
                            double dRate = 0;
                            double.TryParse(jobDispatchDataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].EditedFormattedValue.ToString(), out dRate);
                            jobDispatchDataGridView.Rows[e.RowIndex].Cells[amountTextBox.Index].Value = Convert.ToString(dPcs * dRate);
                            double dAmount = 0;
                            for (int i = 0; i < jobDispatchDataGridView.Rows.Count - 1; i++)
                            {
                                double dTempAmount = 0;
                                double.TryParse(jobDispatchDataGridView.Rows[e.RowIndex].Cells[amountTextBox.Index].EditedFormattedValue.ToString(), out dTempAmount);
                                dAmount = dAmount + dTempAmount;
                            }
                            totalAmountTextBox.Text = Convert.ToString(dAmount);
                            billAmtTextBox.Text = Convert.ToString(Math.Round(dAmount));
                        }
                        else if (jobDispatchDataGridView.Rows[e.RowIndex].Cells[unitComboBox.Index].EditedFormattedValue.ToString() == "MTRS")
                        {
                            double dMtrs = 0;
                            double.TryParse(jobDispatchDataGridView.Rows[e.RowIndex].Cells[qtyTextBox.Index].EditedFormattedValue.ToString(), out dMtrs);
                            double dRate = 0;
                            double.TryParse(jobDispatchDataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].EditedFormattedValue.ToString(), out dRate);
                            jobDispatchDataGridView.Rows[e.RowIndex].Cells[amountTextBox.Index].Value = Convert.ToString(dMtrs * dRate);
                            double dAmount = 0;
                            for (int i = 0; i < jobDispatchDataGridView.Rows.Count - 1; i++)
                            {
                                double dTempAmount = 0;
                                double.TryParse(jobDispatchDataGridView.Rows[e.RowIndex].Cells[amountTextBox.Index].EditedFormattedValue.ToString(), out dTempAmount);
                                dAmount = dAmount + dTempAmount;
                            }
                            totalAmountTextBox.Text = Convert.ToString(dAmount);
                            billAmtTextBox.Text = Convert.ToString(Math.Round(dAmount));
                        }
                    }
                }
                else if (e.ColumnIndex == bundlesTextBox.Index)
                {
                    string bundlesString = jobDispatchDataGridView.Rows[e.RowIndex].Cells[bundlesTextBox.Index].EditedFormattedValue.ToString();
                    if (bundlesString != "")
                    {
                        DataTable dt = new DataTable();
                        try
                        {
                            int answer = (int)dt.Compute(bundlesString, "");
                            jobDispatchDataGridView.Rows[e.RowIndex].Cells[pcsTextBox.Index].Value = answer;
                        }
                        catch (Exception ex)
                        {
                            jobDispatchDataGridView.Rows[e.RowIndex].Cells[pcsTextBox.Index].Value = 0;
                        }
                    }
                    else
                        jobDispatchDataGridView.Rows[e.RowIndex].Cells[pcsTextBox.Index].Value = 0;

                }

            }
            

            }

        private void jobDispatchDataGridView_CellValidated(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == amountTextBox.Index)
            {
                if (e.RowIndex == (jobDispatchDataGridView.Rows.Count - 2))
                {
                    DialogResult result = MessageBox.Show("Do You want to add new Item?", "Job Work Dispatch", MessageBoxButtons.YesNo);
                    if (result == DialogResult.Yes)
                    {
                        jobDispatchDataGridView.CausesValidation = false;
                        jobDispatchDataGridView.Rows[e.RowIndex + 1].ReadOnly = false;
                        updateChallanListForAdditionalJobWork(e.RowIndex + 1);
                        //DataGridViewComboBoxCell cell = (DataGridViewComboBoxCell)jobDispatchDataGridView.Rows[e.RowIndex + 1].Cells[refNoComboBox.Index];
                        //updateLatestMillChallanList((long)partyNameComboBox.SelectedValue, e.RowIndex + 1);
                        return;
                    }
                    else
                    {
                        jobDispatchDataGridView.CausesValidation = false;
                        remarkTextBox.Focus();
                    }
                }
                else if (e.RowIndex < (jobDispatchDataGridView.Rows.Count - 2))
                    jobDispatchDataGridView.Rows[e.RowIndex + 1].ReadOnly = false;
            }
            

        }

        private void addNewButton_Click(object sender, EventArgs e)
        {

            if(iJobWorkMode == 1)
            {
                jobprocess_details jobProcessDetails = new jobprocess_details();
                //List<jobprocess_itemdetails> jobProcessItemDetails = new List<jobprocess_itemdetails>();
                jobProcessDetails.broker_id = (long)brokerComboBox.SelectedValue;
                jobProcessDetails.gstType_id = (long)gstTypeComboBox.SelectedValue;
                jobProcessDetails.party_id = (long)partyNameComboBox.SelectedValue;
                jobProcessDetails.remark = remarkTextBox.Text;
                jobProcessDetails.transport_id = Convert.ToInt64(transportComboBox.SelectedValue);
                //jobProcessDetails.voucherNo = Convert.ToInt64(voucherNoTextBox.Text);
                jobProcessDetails.challanNo = voucherNoTextBox.Text;
                jobProcessDetails.workType_id = (long)workTypeComboBox.SelectedValue;
                jobProcessDetails.amountOnDispatch = Convert.ToDouble(billAmtTextBox.Text);
                jobProcessDetails.totalMtrs = Convert.ToDouble(totalQtyTextBox.Text);
                jobProcessDetails.totalPcs = Convert.ToInt64(totalPcsTextBox.Text);
                jobProcessDetails.lrno = lrNoTextBox.Text;
                jobProcessDetails.dispatchDate = DateTime.ParseExact(dateTextBox.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                for (int i = 0;i < jobDispatchDataGridView.Rows.Count - 1;i++)
                {
                    DataGridViewRow Row = jobDispatchDataGridView.Rows[i];
                    jobprocess_itemdetails jobProcessItemDetail = new jobprocess_itemdetails();
                    jobProcessItemDetail.party_id = jobProcessDetails.party_id;
                    jobProcessItemDetail.challanNo = voucherNoTextBox.Text;
                    jobProcessItemDetail.cut = Convert.ToDouble(Row.Cells[cutTextBox.Index].EditedFormattedValue.ToString());
                    
                    jobProcessItemDetail.dipatchBundles = Row.Cells[bundlesTextBox.Index].EditedFormattedValue.ToString();
                    jobProcessItemDetail.dispatchItemAmount = Convert.ToInt64(Row.Cells[amountTextBox.Index].EditedFormattedValue.ToString());
                    if (additionalJobChallanCheckBox.Checked)
                    {
                        if (Row.Cells[refNoComboBox.Index].Value == null)
                            jobProcessItemDetail.receiptReference_id = null;
                        else
                            jobProcessItemDetail.receiptReference_id = lSelectedObjects[i];
                    }
                    else
                    {
                        if (Row.Cells[refNoComboBox.Index].Value == null)
                            jobProcessItemDetail.dispatchReference_id = null;
                        else
                            jobProcessItemDetail.dispatchReference_id = lSelectedObjects[i];// Convert.ToInt64(Row.Cells[refNoComboBox.Index].EditedFormattedValue.ToString());
                    }
                    
                    jobProcessItemDetail.dispatch_mtrsQty = Convert.ToDouble(Row.Cells[qtyTextBox.Index].EditedFormattedValue.ToString());
                    jobProcessItemDetail.dispatch_pcs = Convert.ToInt64(Row.Cells[pcsTextBox.Index].EditedFormattedValue.ToString());
                    jobProcessItemDetail.hsnCode = Row.Cells[hsncTextBox.Index].EditedFormattedValue.ToString();
                    jobProcessItemDetail.item_id = (long)(Row.Cells[itemNameComboBox.Index].Value);
                    jobProcessItemDetail.jobTypeDetails = Row.Cells[jobTypeTextBox.Index].EditedFormattedValue.ToString();
                    jobProcessItemDetail.rate = Convert.ToDouble(Row.Cells[rateTextBox.Index].EditedFormattedValue.ToString());
                    jobProcessItemDetail.unit = Row.Cells[unitComboBox.Index].EditedFormattedValue.ToString();
                    jobProcessDetails.jobprocess_itemdetails.Add(jobProcessItemDetail);
                }
                
                bool bChalladAdded = partyDB.addJobWorkDispatchChallan(jobProcessDetails);
                if(bChalladAdded)
                {
                    MessageBox.Show("Job Work Challan Added", "JOB WORK", MessageBoxButtons.OK);
                    setDefaultChallanNoNo();
                    resetAllData();
                    updateChallanListForAdditionalJobWork(0);
                }
                else
                {
                    MessageBox.Show("Job Work Challan not Added", "ERROR", MessageBoxButtons.OK);
                }
            }
            else if(iJobWorkMode == 2)
            {
                if(jobChallanReceiveGridView.Rows.Count - 1 > 0)
                {
                    List<jobprocess_itemdetails> itemReceiveList = new List<jobprocess_itemdetails>();
                    jobprocess_itemdetails jobItemDetails = null;
                    for (int i = 0; i < jobChallanReceiveGridView.Rows.Count - 1; i++)
                    {
                        long lJobWorkRocessId = Convert.ToInt64(jobChallanReceiveGridView.Rows[i].Cells[refNoChallanReceiveComboBox.Index].Value);
                        jobItemDetails = partyDB.getJobWorkDispatchChallanItemList(lSelectedObjects[i]);
                        if(jobItemDetails == null)
                        {
                            MessageBox.Show("Challan not received", "ERROR", MessageBoxButtons.OK);
                            return;
                        }
                        List<jobprocessreceipt_itemdetails> challanReceiveItemList = jobItemDetails.jobprocessreceipt_itemdetails.ToList();
                        jobprocessreceipt_itemdetails newReceiveItemData = new jobprocessreceipt_itemdetails();
                        newReceiveItemData.item_id = Convert.ToInt64(jobChallanReceiveGridView.Rows[i].Cells[itemNameReceivveComboBox.Index].Value);
                        newReceiveItemData.challanNo = jobChallanReceiveGridView.Rows[i].Cells[refNoChallanReceiveComboBox.Index].EditedFormattedValue.ToString();
                        newReceiveItemData.cut = Convert.ToDouble(jobChallanReceiveGridView.Rows[i].Cells[cutReceiveTextBox.Index].EditedFormattedValue.ToString());
                        newReceiveItemData.freshItem = Convert.ToDouble(jobChallanReceiveGridView.Rows[i].Cells[freshReceiveTextBox.Index].EditedFormattedValue.ToString());
                        newReceiveItemData.hsnCode = jobChallanReceiveGridView.Rows[i].Cells[hsnCodeReceiveTextBox.Index].EditedFormattedValue.ToString();
                        newReceiveItemData.itemAmount = Convert.ToDouble(jobChallanReceiveGridView.Rows[i].Cells[amountReceiveTextBox.Index].EditedFormattedValue.ToString());
                        newReceiveItemData.jobProcessItemDetails_id = lSelectedObjects[i];//jobItemDetails.jobProcessItemDetails_id;
                        newReceiveItemData.party_id = jobItemDetails.party_id;
                        newReceiveItemData.isDeleted = false;
                        newReceiveItemData.multipleJobsOnItem = Convert.ToBoolean(jobChallanReceiveGridView.Rows[i].Cells[multipleJobsOnItem.Index].Value);
                        newReceiveItemData.jobprocess_id = jobItemDetails.jobprocess_id;
                        newReceiveItemData.jobTypeDetails = jobChallanReceiveGridView.Rows[i].Cells[jobTypeReceiveTextBox.Index].EditedFormattedValue.ToString();
                        newReceiveItemData.lrno = lrNoTextBox.Text;
                        newReceiveItemData.mtrs = Convert.ToDouble(jobChallanReceiveGridView.Rows[i].Cells[qunatityReceiveTextBox.Index].EditedFormattedValue.ToString());
                        newReceiveItemData.pcs = Convert.ToDouble(jobChallanReceiveGridView.Rows[i].Cells[pcsReceiveTextBox.Index].EditedFormattedValue.ToString());
                        newReceiveItemData.plainItem = Convert.ToDouble(jobChallanReceiveGridView.Rows[i].Cells[plainReceiveTextBox.Index].EditedFormattedValue.ToString());
                        newReceiveItemData.rate = Convert.ToDouble(jobChallanReceiveGridView.Rows[i].Cells[rateReceiveTextBox.Index].EditedFormattedValue.ToString());
                        newReceiveItemData.recdDate = DateTime.ParseExact(dateTextBox.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        newReceiveItemData.receiptBundles = jobChallanReceiveGridView.Rows[i].Cells[bundlesReceiveTextBox.Index].EditedFormattedValue.ToString();
                        newReceiveItemData.receiveVoucherNo = Convert.ToInt64(voucherNoTextBox.Text);
                        newReceiveItemData.remark = remarkTextBox.Text;
                        newReceiveItemData.secondItem = Convert.ToDouble(jobChallanReceiveGridView.Rows[i].Cells[secondReceiveTextBox.Index].EditedFormattedValue.ToString());
                        newReceiveItemData.shortItem = Convert.ToDouble(jobChallanReceiveGridView.Rows[i].Cells[shortReceiveTextBox.Index].EditedFormattedValue.ToString());
                        newReceiveItemData.transport_id = Convert.ToInt64(transportComboBox.SelectedValue);
                        newReceiveItemData.unit = jobChallanReceiveGridView.Rows[i].Cells[unitReceiveComboBox.Index].EditedFormattedValue.ToString();
                        challanReceiveItemList.Add(newReceiveItemData);
                        jobItemDetails.jobprocessreceipt_itemdetails = challanReceiveItemList;
                        itemReceiveList.Add(jobItemDetails);
                    }
                    if(jobItemDetails != null)
                    {
                        bool bChalladAdded = partyDB.addJobWorkReceiveChallanItem(itemReceiveList);
                        if (bChalladAdded)
                        {
                            MessageBox.Show("Job Work Challan Received", "JOB WORK", MessageBoxButtons.OK);
                            setDefaultMillReceiptVoucherNo();
                            resetAllData();
                            updateReceivedChallanPendingList(0);
                        }
                        else
                        {
                            MessageBox.Show("Job Work Challan not Received", "ERROR", MessageBoxButtons.OK);
                        }
                    }
                    

                }
                

                /*bool bChalladAdded = partyDB.addJobWorkDispatchChallan(jobProcessDetails);
                if (bChalladAdded)
                {
                    MessageBox.Show("Job Work Challan Added", "JOB WORK", MessageBoxButtons.OK);
                    setDefaultChallanNoNo();
                    resetAllData();
                }
                else
                {
                    MessageBox.Show("Job Work Challan not Added", "ERROR", MessageBoxButtons.OK);
                }*/
            }
            else if (iJobWorkMode == 3)
            {
                if(billNoTextBox.Text == "" || billNoTextBox.Text == "0")
                {
                    MessageBox.Show("Please enter the Bill No.", "JOB WORK", MessageBoxButtons.OK);
                    billNoTextBox.Focus();
                    return;
                }
                if(jobReceiptDataGridView.Rows.Count-1 > 0)
                {
                    jobreceive_billdetail jobBillDetails = new jobreceive_billdetail();
                    for(int i = 0;i < jobReceiptDataGridView.Rows.Count - 1;i++)
                    {
                        jobprocessreceipt_itemdetails itemReceiveData = new jobprocessreceipt_itemdetails();
                        jobprocessreceipt_itemdetails tempData = partyDB.getJobWorkReceiptChallanItemForBill(lSelectedObjects[i]);
                        //List<jobprocessreceipt_itemdetails> tempDataList = partyDB.getReceiveChallanItemList((long)partyNameComboBox.SelectedValue);
                        //jobprocessreceipt_itemdetails tempData = tempDataList[iSelectedObjectsIndex[i]];
                        itemReceiveData.challanNo = tempData.challanNo;
                        itemReceiveData.cut = tempData.cut;
                        itemReceiveData.hsnCode = tempData.hsnCode;
                        itemReceiveData.isDeleted = tempData.isDeleted;
                        itemReceiveData.item_id = tempData.item_id;
                        itemReceiveData.jobProcessItemDetails_id = tempData.jobProcessItemDetails_id;
                        itemReceiveData.jobprocess_id = tempData.jobprocess_id;
                        itemReceiveData.jobReceiveBillDetail_id = tempData.jobReceiveBillDetail_id;
                        itemReceiveData.jobWorkReceiptId = tempData.jobWorkReceiptId;
                        itemReceiveData.party_id = tempData.party_id;
                        itemReceiveData.pcs = tempData.pcs;
                        itemReceiveData.rate = tempData.rate;
                        itemReceiveData.mtrs = tempData.mtrs;
                        itemReceiveData.plainItem = tempData.plainItem;
                        itemReceiveData.receiptBundles = tempData.receiptBundles;
                        itemReceiveData.receiveVoucherNo = tempData.receiveVoucherNo;
                        itemReceiveData.transport_id = tempData.transport_id;
                        itemReceiveData.recdDate = tempData.recdDate;
                        itemReceiveData.version = tempData.version;


                        DataGridViewRow Row = jobReceiptDataGridView.Rows[i];
                        itemReceiveData.freshItem = Convert.ToDouble(Row.Cells[freshTextBox.Index].Value);
                        itemReceiveData.itemAmount = Convert.ToDouble(Row.Cells[amountReceiptTextBox.Index].Value);
                        itemReceiveData.jobTypeDetails = Convert.ToString(Row.Cells[jobTypeReceiptTextBox.Index].Value);
                        //itemReceiveData.lrno = lrNoTextBox.Text;
                        itemReceiveData.receiptBundles = Convert.ToString(Row.Cells[bundlesReceiptTextBox.Index].Value);
                        itemReceiveData.remark = remarkTextBox.Text;
                        itemReceiveData.secondItem = Convert.ToDouble(Row.Cells[secTextBox.Index].Value);
                        itemReceiveData.shortItem = Convert.ToDouble(Row.Cells[shortTextBox.Index].Value);
                        //itemReceiveData.transport_id = Convert.ToInt64(transportComboBox.SelectedValue);
                        itemReceiveData.unit = Convert.ToString(Row.Cells[unitReceiptComboBox.Index].Value);
                        jobBillDetails.jobprocessreceipt_itemdetails.Add(itemReceiveData);
                    }
                    jobBillDetails.date = DateTime.ParseExact(dateTextBox.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    jobBillDetails.addLessDetail1 = Convert.ToDouble(addLessTextBox.Text);
                    jobBillDetails.addLessDetail2 = Convert.ToDouble(addLessAmtTextBox.Text);
                    jobBillDetails.addLessDetail3 = Convert.ToDouble(addLessAmtFinalTextBox.Text);
                    jobBillDetails.billNo = billNoTextBox.Text;
                    jobBillDetails.billVoucherNo = Convert.ToInt64(voucherNoTextBox.Text);
                    jobBillDetails.broker_id = Convert.ToInt64(brokerComboBox.SelectedValue);
                    jobBillDetails.caseNoDetail1 = caseNoTextBox.Text;
                    jobBillDetails.caseNoDetail2 = caseNoTextBox2.Text;
                    jobBillDetails.cgstAmount = Convert.ToDouble(cgstAmtTextBox.Text);
                    jobBillDetails.cgstPercentage = Convert.ToDouble(cgstRateTextBox.Text);
                    //jobBillDetails.caseNoDetail1 = Convert.ToDouble(caseNoTextBox.Text);
                    //jobBillDetails.caseNoDetail2 = Convert.ToDouble(caseNoTextBox2.Text);
                    jobBillDetails.cgstAmount = Convert.ToDouble(cgstAmtTextBox.Text);
                    jobBillDetails.cgstPercentage = Convert.ToDouble(cgstRateTextBox.Text);
                    jobBillDetails.discountAmount = Convert.ToDouble(discPerAmtFinalTextBox.Text);
                    jobBillDetails.discountOnAmount = Convert.ToDouble(discPerAmtTextBox.Text);
                    jobBillDetails.discountPercentage = Convert.ToDouble(discPerTextBox.Text);
                    jobBillDetails.grossAmount = Convert.ToDouble(totalAmountTextBox.Text);
                    jobBillDetails.gstType_id = Convert.ToInt64(gstTypeComboBox.SelectedValue);
                    jobBillDetails.igstAmount = Convert.ToDouble(igstAmtTextBox.Text);
                    jobBillDetails.igstPercentage = Convert.ToDouble(igstRateTextBox.Text);
                    jobBillDetails.lrno = lrNoTextBox.Text;
                    jobBillDetails.netAmount = Convert.ToDouble(billAmtTextBox.Text);
                    jobBillDetails.party_id = Convert.ToInt64(partyNameComboBox.SelectedValue);
                    jobBillDetails.remark = remarkTextBox.Text;
                    jobBillDetails.sgstAmount = Convert.ToDouble(sgstAmtTextBox.Text);
                    jobBillDetails.sgstPercentage = Convert.ToDouble(sgstRateTextBox.Text);
                    jobBillDetails.taxableValue = Convert.ToDouble(taxValTextBox.Text);
                    jobBillDetails.tdsAmount = Convert.ToDouble(tdsAmountTextBox.Text);
                    jobBillDetails.tdsPercentage = Convert.ToDouble(tdsTextBox.Text);
                    jobBillDetails.totalMtrs = Convert.ToDouble(totalQtyTextBox.Text);
                    jobBillDetails.totalPcs = Convert.ToDouble(totalPcsTextBox.Text);
                    jobBillDetails.transport_id = Convert.ToInt64(transportComboBox.SelectedValue);
                    jobBillDetails.workType_id = Convert.ToInt64(workTypeComboBox.SelectedValue);

                    bool bBillAdded = partyDB.addJobWorkReceiveChallanBill(jobBillDetails);
                    if(bBillAdded)
                    {
                        MessageBox.Show("Job Work Bill Added", "JOB WORK", MessageBoxButtons.OK);
                        setDefaultVoucherNo();
                        resetAllData();
                        updateReceivedChallanPendingListForChallanReceive((long)partyNameComboBox.SelectedValue, 0);
                    }
                    else
                    {
                        MessageBox.Show("Job Work Bill not Added", "JOB WORK", MessageBoxButtons.OK);
                    }
                } 

            }
        }

        private void resetAllData()
        {
            jobDispatchDataGridView.Rows.Clear();
            jobReceiptDataGridView.Rows.Clear();
            jobChallanReceiveGridView.Rows.Clear();
            remarkTextBox.Clear();
            lSelectedObjects.Clear();
            iSelectedObjectsIndex.Clear();
            totalAmountTextBox.Text = "0.0";
            totalPcsTextBox.Text = "0";
            totalQtyTextBox.Text = "0.0";
            billAmtTextBox.Text = "0.0";
            lrNoTextBox.Text = "";
            billNoTextBox.Text = "0";
            discPerAmtFinalTextBox.Text = "0";
            discPerAmtTextBox.Text = "0";
            discPerTextBox.Text = "0";
            addLessAmtFinalTextBox.Text = "0";
            addLessAmtTextBox.Text = "0";
            addLessTextBox.Text = "0";
            caseNoTextBox.Text = "";
            caseNoTextBox2.Text = "1";
            tdsTextBox.Text = "0";
            tdsAmountTextBox.Text = "0";
            cgstAmtTextBox.Text = "0";
            cgstRateTextBox.Text = "2.5";
            sgstAmtTextBox.Text = "0";
            sgstRateTextBox.Text = "2.5";
            igstAmtTextBox.Text = "0";
            igstRateTextBox.Text = "5";
            taxValTextBox.Text = "0";
            billAmtTextBox.Text = "0";
            addNewButton.Enabled = true;
            updateBill.Enabled = false;
            printButton.Enabled = false;
            deleteButton.Enabled = false;
            viewBillButton.Enabled = true;
        }

        private void voucherNoTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                if (iJobWorkMode == 1)
                {
                    //long lChallanNo = 0;
                    //long.TryParse(voucherNoTextBox.Text, out lChallanNo);
                    jobProcessData = partyDB.getJobDispatchData(voucherNoTextBox.Text);
                    if (jobProcessData == null)
                    {
                        MessageBox.Show("Unable to fetch Dispatch Challan Data", "ERROR", MessageBoxButtons.OK);
                        setDefaultChallanNoNo();
                        resetAllData();
                        voucherNoTextBox.Focus();
                        updateChallanListForAdditionalJobWork(0);
                        return;
                    }
                    DateTime dt, dt1;
                    bool bFlag = DateTime.TryParse(jobProcessData.dispatchDate.ToString(), out dt);
                    if (bFlag)
                    {
                        dt1 = dt.Date;
                        //DateTime.Parse(dt1.ToString()).ToString("dd-MM-yyyy");
                        dateTextBox.Text = dt1.ToString("dd-MM-yyyy");
                    }
                    partyNameComboBox.SelectedValue = (long)jobProcessData.party_id;
                    gstTypeComboBox.SelectedValue = (long)jobProcessData.gstType_id;
                    brokerComboBox.SelectedValue = (long)jobProcessData.broker_id;
                    workTypeComboBox.SelectedValue = (long)jobProcessData.workType_id;
                    transportComboBox.SelectedValue = Convert.ToInt64(jobProcessData.transport_id);
                    lrNoTextBox.Text = jobProcessData.lrno;
                    remarkTextBox.Text = jobProcessData.remark;
                    totalAmountTextBox.Text = Convert.ToString(jobProcessData.amountOnDispatch);
                    totalPcsTextBox.Text = Convert.ToString(jobProcessData.totalPcs);
                    totalQtyTextBox.Text = Convert.ToString(jobProcessData.totalMtrs);
                    billAmtTextBox.Text = Convert.ToString(jobProcessData.amountOnDispatch);


                    jobDispatchDataGridView.Rows.Clear();
                    List<jobprocess_itemdetails> itemDataList = jobProcessData.jobprocess_itemdetails.ToList();
                    if (itemDataList == null)
                    {
                        MessageBox.Show("Unable to fetch Dispatch Challan Item Details", "ERROR", MessageBoxButtons.OK);
                        return;
                    }
                    for (int i = 0; i < jobProcessData.jobprocess_itemdetails.Count; i++)
                    {
                        jobDispatchDataGridView.Rows.Add();
                        DataGridViewRow Row = jobDispatchDataGridView.Rows[i];

                        if (itemDataList[i].receiptReference_id == null && itemDataList[i].dispatchReference_id == null)
                            Row.Cells[refNoComboBox.Index].Value = null;
                        else if (itemDataList[i].receiptReference_id != null)
                        {
                            DataGridViewComboBoxCell cellchallan = (DataGridViewComboBoxCell)jobDispatchDataGridView.Rows[i].Cells[refNoComboBox.Index];
                            cellchallan.Items.Add(itemDataList[i].receiptReference_id);
                            cellchallan.ReadOnly = true;
                            cellchallan.Value = itemDataList[i].receiptReference_id;
                            lSelectedObjects.Add((long)itemDataList[i].receiptReference_id);
                        }
                        else if (itemDataList[i].dispatchReference_id != -1)
                        {
                            DataGridViewComboBoxCell cellchallan = (DataGridViewComboBoxCell)jobDispatchDataGridView.Rows[i].Cells[refNoComboBox.Index];
                            cellchallan.Items.Add(itemDataList[i].dispatchReference_id);
                            cellchallan.ReadOnly = true;
                            cellchallan.Value = itemDataList[i].dispatchReference_id;
                            lSelectedObjects.Add((long)itemDataList[i].dispatchReference_id);
                        }

                        var itemList = DB.getItemList();
                        List<item_details> SelectedChallanItemList = new List<item_details>(); ;

                        if (itemList != null)
                        {
                            //List<jobprocess_itemdetails> jobWorkItemList = jobChallanData.jobprocess_itemdetails.ToList();
                            for (int j = 0; j < itemDataList.Count; j++)
                            {
                                item_details itemDetail = itemList.Where(x => x.itemid == itemDataList[j].item_id).SingleOrDefault();
                                if (!SelectedChallanItemList.Contains(itemDetail))
                                    SelectedChallanItemList.Add(itemDetail);
                            }


                            DataGridViewComboBoxCell cell = (DataGridViewComboBoxCell)jobDispatchDataGridView.Rows[i].Cells[itemNameComboBox.Index];
                            cell.DataSource = SelectedChallanItemList;
                            cell.DisplayMember = "itemname";
                            cell.ValueMember = "itemid";
                            cell.ReadOnly = true;

                        }


                        Row.Cells[itemNameComboBox.Index].Value = itemDataList[i].item_id;
                        Row.Cells[bundlesTextBox.Index].Value = itemDataList[i].dipatchBundles;
                        Row.Cells[hsncTextBox.Index].Value = itemDataList[i].hsnCode;
                        Row.Cells[jobTypeTextBox.Index].Value = itemDataList[i].jobTypeDetails;
                        Row.Cells[unitComboBox.Index].Value = itemDataList[i].unit;
                        Row.Cells[pcsTextBox.Index].Value = itemDataList[i].dispatch_pcs;
                        Row.Cells[qtyTextBox.Index].Value = itemDataList[i].dispatch_mtrsQty;
                        Row.Cells[cutTextBox.Index].Value = itemDataList[i].cut;
                        Row.Cells[rateTextBox.Index].Value = itemDataList[i].rate;
                        Row.Cells[amountTextBox.Index].Value = itemDataList[i].dispatchItemAmount;


                    }

                }
                else if(iJobWorkMode == 2)
                {
                    
                    long lChallanNo = 0;
                    long.TryParse(voucherNoTextBox.Text, out lChallanNo);
                    List<jobprocessreceipt_itemdetails> jobProcessReceiveData = partyDB.getJobReceiveData(lChallanNo);
                    jobChallanReceiveGridView.Rows.Clear();
                    if (jobProcessReceiveData == null || jobProcessReceiveData.Count == 0)
                    {
                        MessageBox.Show("Voucher Challan does not exist", "ERROR", MessageBoxButtons.OK);
                        setDefaultMillReceiptVoucherNo();
                        resetAllData();
                        updateReceivedChallanPendingList(0);
                        voucherNoTextBox.Focus();
                        return;
                    }

                    jobprocess_details jobChallanData = partyDB.getJobDispatchData(jobProcessReceiveData[0].challanNo);

                    if(jobChallanData == null)
                    {
                        MessageBox.Show("Unable to fetch Dispatch Challan Receive Data", "ERROR", MessageBoxButtons.OK);
                        return;
                    }
                    DateTime dt, dt1;
                    bool bFlag = DateTime.TryParse(jobProcessReceiveData[0].recdDate.ToString(), out dt);
                    if (bFlag)
                    {
                        dt1 = dt.Date;
                        //DateTime.Parse(dt1.ToString()).ToString("dd-MM-yyyy");
                        dateTextBox.Text = dt1.ToString("dd-MM-yyyy");
                    }
                    partyNameComboBox.SelectedValue = (long)jobChallanData.party_id;
                    gstTypeComboBox.SelectedValue = (long)jobChallanData.gstType_id;
                    brokerComboBox.SelectedValue = (long)jobChallanData.broker_id;
                    workTypeComboBox.SelectedValue = (long)jobChallanData.workType_id;
                    transportComboBox.SelectedValue = (long)jobProcessReceiveData[0].transport_id;
                    lrNoTextBox.Text = jobProcessReceiveData[0].lrno;
                    remarkTextBox.Text = jobProcessReceiveData[0].remark;
                    //totalAmountTextBox.Text = Convert.ToString(jobProcessData.amountOnDispatch);
                    //totalPcsTextBox.Text = Convert.ToString(jobProcessData.totalPcs);
                    //totalQtyTextBox.Text = Convert.ToString(jobProcessData.totalMtrs);
                    //billAmtTextBox.Text = Convert.ToString(jobProcessData.amountOnDispatch);


                    
                        
                    for (int i = 0; i < jobProcessReceiveData.Count; i++)
                    {

                        //DataGridViewRow Row = new DataGridViewRow();
                        jobChallanReceiveGridView.Rows.Add();
                        DataGridViewRow Row = jobChallanReceiveGridView.Rows[i];
                        
                        
                        var itemList = DB.getItemList();
                        List<item_details> SelectedChallanItemList = new List<item_details>(); ;


                        if (itemList != null)
                        {
                            List<jobprocess_itemdetails> jobWorkItemList = jobChallanData.jobprocess_itemdetails.ToList();
                            for (int j = 0; j < jobWorkItemList.Count; j++)
                            {
                                item_details itemDetail = itemList.Where(x => x.itemid == jobWorkItemList[j].item_id).SingleOrDefault();
                                if (!SelectedChallanItemList.Contains(itemDetail))
                                    SelectedChallanItemList.Add(itemDetail);
                            }


                            DataGridViewComboBoxCell cell = (DataGridViewComboBoxCell)jobChallanReceiveGridView.Rows[i].Cells[itemNameReceivveComboBox.Index];
                            cell.DataSource = SelectedChallanItemList;
                            cell.DisplayMember = "itemname";
                            cell.ValueMember = "itemid";
                            cell.ReadOnly = true;   

                        }
                        DataGridViewComboBoxCell cellchallan = (DataGridViewComboBoxCell)jobChallanReceiveGridView.Rows[i].Cells[refNoChallanReceiveComboBox.Index];
                        cellchallan.Items.Add(jobProcessReceiveData[i].challanNo);
                        cellchallan.ReadOnly = true;

                        lSelectedObjects.Add((long)jobProcessReceiveData[i].jobProcessItemDetails_id);
                        Row.Cells[refNoChallanReceiveComboBox.Index].Value = jobProcessReceiveData[i].challanNo;
                        Row.Cells[itemNameReceivveComboBox.Index].Value = jobProcessReceiveData[i].item_id;
                        Row.Cells[bundlesReceiveTextBox.Index].Value = jobProcessReceiveData[i].receiptBundles;
                        Row.Cells[hsnCodeReceiveTextBox.Index].Value = jobProcessReceiveData[i].hsnCode;
                        Row.Cells[jobTypeReceiveTextBox.Index].Value = jobProcessReceiveData[i].jobTypeDetails;
                        Row.Cells[unitReceiveComboBox.Index].Value = jobProcessReceiveData[i].unit;
                        Row.Cells[pcsReceiveTextBox.Index].Value = jobProcessReceiveData[i].pcs;
                        Row.Cells[freshReceiveTextBox.Index].Value = jobProcessReceiveData[i].freshItem;
                        Row.Cells[qunatityReceiveTextBox.Index].Value = jobProcessReceiveData[i].mtrs;
                        Row.Cells[cutReceiveTextBox.Index].Value = jobProcessReceiveData[i].cut;
                        Row.Cells[rateReceiveTextBox.Index].Value = jobProcessReceiveData[i].rate;
                        Row.Cells[multipleJobsOnItem.Index].Value = Convert.ToBoolean(jobProcessReceiveData[i].multipleJobsOnItem);
                        Row.Cells[amountReceiveTextBox.Index].Value = jobProcessReceiveData[i].itemAmount;
                        Row.Cells[plainReceiveTextBox.Index].Value = jobProcessReceiveData[i].plainItem;
                        Row.Cells[secondReceiveTextBox.Index].Value = jobProcessReceiveData[i].secondItem;
                        Row.Cells[shortReceiveTextBox.Index].Value = jobProcessReceiveData[i].shortItem;
                        //jobDispatchDataGridView.Rows.Add(Row);


                    }
                    double dTotalAmount = 0, dTotalMtrs = 0, dTotalPcs = 0;
                    for (int i = 0; i < jobChallanReceiveGridView.Rows.Count - 1; i++)
                    {
                        double dAmount = 0, dMtrs = 0, dPcs = 0;
                        double.TryParse(jobChallanReceiveGridView.Rows[i].Cells[amountReceiveTextBox.Index].EditedFormattedValue.ToString(), out dAmount);
                        dTotalAmount = dTotalAmount + dAmount;
                        double.TryParse(jobChallanReceiveGridView.Rows[i].Cells[qunatityReceiveTextBox.Index].EditedFormattedValue.ToString(), out dMtrs);
                        dTotalMtrs = dTotalMtrs + dMtrs;
                        double.TryParse(jobChallanReceiveGridView.Rows[i].Cells[pcsReceiveTextBox.Index].EditedFormattedValue.ToString(), out dPcs);
                        dTotalPcs = dTotalPcs + dPcs;
                    }
                    totalAmountTextBox.Text = Convert.ToString(dTotalAmount);
                    totalPcsTextBox.Text = Convert.ToString(dTotalPcs);
                    totalQtyTextBox.Text = Convert.ToString(dTotalMtrs);
                    billAmtTextBox.Text = totalAmountTextBox.Text;


                }
                else if (iJobWorkMode == 3)
                {
                    long lVoucherNo = 0;
                    bool bFlag = long.TryParse(voucherNoTextBox.Text,out lVoucherNo);
                    jobreceive_billdetail viewBillData = partyDB.getReceiveJobWorkBillDetail(lVoucherNo);
                    if(viewBillData == null)
                    {
                        MessageBox.Show("Voucher Bill does not exist","BILL VOUCHER",MessageBoxButtons.OK);
                        setDefaultVoucherNo();
                        resetAllData();
                        updateReceivedChallanPendingListForChallanReceive((long)partyNameComboBox.SelectedValue, 0);
                        return;
                    }
                    DateTime dt, dt1;
                    bFlag = DateTime.TryParse(viewBillData.date.ToString(), out dt);
                    if (bFlag)
                    {
                        dt1 = dt.Date;
                        //DateTime.Parse(dt1.ToString()).ToString("dd-MM-yyyy");
                        dateTextBox.Text = dt1.ToString("dd-MM-yyyy");
                    }
                    partyNameComboBox.SelectedValue = viewBillData.party_id;
                    gstTypeComboBox.SelectedValue = viewBillData.gstType_id;
                    billNoTextBox.Text = viewBillData.billNo;
                    brokerComboBox.SelectedValue = viewBillData.broker_id;
                    workTypeComboBox.SelectedValue = viewBillData.workType_id;
                    transportComboBox.SelectedValue = viewBillData.transport_id;
                    lrNoTextBox.Text = viewBillData.lrno;
                    remarkTextBox.Text = viewBillData.remark;
                    discPerTextBox.Text = Convert.ToString(viewBillData.discountPercentage);
                    discPerAmtTextBox.Text = Convert.ToString(viewBillData.discountOnAmount);
                    discPerAmtFinalTextBox.Text = Convert.ToString(viewBillData.discountAmount);
                    addLessTextBox.Text = Convert.ToString(viewBillData.addLessDetail1);
                    addLessAmtTextBox.Text = Convert.ToString(viewBillData.addLessDetail2);
                    addLessAmtFinalTextBox.Text = Convert.ToString(viewBillData.addLessDetail3);
                    caseNoTextBox.Text = viewBillData.caseNoDetail1;
                    caseNoTextBox2.Text = viewBillData.caseNoDetail2;
                    tdsTextBox.Text = Convert.ToString(viewBillData.tdsPercentage);
                    tdsAmountTextBox.Text = Convert.ToString(viewBillData.tdsAmount);
                    cgstRateTextBox.Text = Convert.ToString(viewBillData.cgstPercentage);
                    cgstAmtTextBox.Text = Convert.ToString(viewBillData.cgstAmount);
                    sgstRateTextBox.Text = Convert.ToString(viewBillData.sgstPercentage);
                    sgstAmtTextBox.Text = Convert.ToString(viewBillData.sgstAmount);
                    igstRateTextBox.Text = Convert.ToString(viewBillData.igstPercentage);
                    igstAmtTextBox.Text = Convert.ToString(viewBillData.igstAmount);
                    taxValTextBox.Text = Convert.ToString(viewBillData.taxableValue);
                    billAmtTextBox.Text = Convert.ToString(viewBillData.netAmount);
                    totalAmountTextBox.Text = Convert.ToString(viewBillData.grossAmount);
                    totalQtyTextBox.Text = Convert.ToString(viewBillData.totalMtrs);
                    totalPcsTextBox.Text = Convert.ToString(viewBillData.totalPcs);

                    List<jobprocessreceipt_itemdetails> viewReceiveItemList = viewBillData.jobprocessreceipt_itemdetails.ToList();
                    jobReceiptDataGridView.Rows.Clear();
                    for (int i = 0;i < viewReceiveItemList.Count;i++)
                    {
                        jobReceiptDataGridView.Rows.Add();
                        var itemList = DB.getItemList();
                        
                        if (itemList != null)
                        {
                            
                            DataGridViewComboBoxCell cell = (DataGridViewComboBoxCell)jobReceiptDataGridView.Rows[i].Cells[itemNameReceiptComboBox.Index];
                            cell.DataSource = itemList;
                            cell.DisplayMember = "itemname";
                            cell.ValueMember = "itemid";
                            cell.ReadOnly = true;

                        }
                        DataGridViewComboBoxCell cellchallan = (DataGridViewComboBoxCell)jobReceiptDataGridView.Rows[i].Cells[refNoComboBox.Index];
                        cellchallan.Items.Add(viewReceiveItemList[i].challanNo);
                        cellchallan.ReadOnly = true;
                        DataGridViewRow Row = jobReceiptDataGridView.Rows[i];
                        Row.Cells[itemNameReceiptComboBox.Index].Value = viewReceiveItemList[i].item_id;
                        Row.Cells[refNoComboBox.Index].Value = viewReceiveItemList[i].challanNo;
                        Row.Cells[bundlesReceiptTextBox.Index].Value = viewReceiveItemList[i].receiptBundles;
                        Row.Cells[hsncodeReceiptTextBox.Index].Value = viewReceiveItemList[i].hsnCode;
                        Row.Cells[jobTypeReceiptTextBox.Index].Value = viewReceiveItemList[i].jobTypeDetails;
                        Row.Cells[unitReceiptComboBox.Index].Value = viewReceiveItemList[i].unit;
                        Row.Cells[unitReceiptComboBox.Index].ReadOnly = true;
                        Row.Cells[pcsReceiptTextBox.Index].Value = viewReceiveItemList[i].pcs;
                        Row.Cells[pcsReceiptTextBox.Index].ReadOnly = true;
                        
                        Row.Cells[qtyReceiptTextBox.Index].Value = viewReceiveItemList[i].mtrs;
                        Row.Cells[qtyReceiptTextBox.Index].ReadOnly = true;
                        Row.Cells[freshTextBox.Index].Value = viewReceiveItemList[i].freshItem;
                        Row.Cells[freshTextBox.Index].ReadOnly = true;
                        Row.Cells[shortTextBox.Index].Value = viewReceiveItemList[i].shortItem;
                        Row.Cells[shortTextBox.Index].ReadOnly = true;
                        Row.Cells[secTextBox.Index].Value = viewReceiveItemList[i].secondItem;
                        Row.Cells[secTextBox.Index].ReadOnly = true;
                        Row.Cells[plainTextBox.Index].Value = viewReceiveItemList[i].plainItem;
                        Row.Cells[plainTextBox.Index].ReadOnly = true;
                        Row.Cells[cutReceiptTextBox.Index].Value = viewReceiveItemList[i].cut;
                        Row.Cells[cutReceiptTextBox.Index].ReadOnly = true;
                        Row.Cells[rateReceiptTextBox.Index].Value = viewReceiveItemList[i].rate;
                        
                        Row.Cells[amountReceiptTextBox.Index].Value = viewReceiveItemList[i].itemAmount;
                        Row.Cells[amountReceiptTextBox.Index].ReadOnly = true;
                    }

                }
                addNewButton.Enabled = false;
                updateBill.Enabled = true;
                printButton.Enabled = true;
                deleteButton.Enabled = true;
                viewBillButton.Enabled = true;
            }
            
        }

        private void updateBill_Click(object sender, EventArgs e)
        {
            if (iJobWorkMode == 1)
            {
                //jobprocess_details jobProcessDetails = new jobprocess_details();
                //List<jobprocess_itemdetails> jobProcessItemDetails = new List<jobprocess_itemdetails>();
                jobProcessData.broker_id = (long)brokerComboBox.SelectedValue;
                jobProcessData.gstType_id = (long)gstTypeComboBox.SelectedValue;
                jobProcessData.party_id = (long)partyNameComboBox.SelectedValue;
                jobProcessData.remark = remarkTextBox.Text;
                jobProcessData.transport_id = Convert.ToInt64(transportComboBox.SelectedValue);
                //jobProcessData.voucherNo = Convert.ToInt64(voucherNoTextBox.Text);
                jobProcessData.challanNo = voucherNoTextBox.Text;
                jobProcessData.workType_id = (long)workTypeComboBox.SelectedValue;
                jobProcessData.amountOnDispatch = Convert.ToDouble(billAmtTextBox.Text);
                jobProcessData.totalMtrs = Convert.ToDouble(totalQtyTextBox.Text);
                jobProcessData.totalPcs = Convert.ToInt64(totalPcsTextBox.Text);
                jobProcessData.dispatchDate = DateTime.ParseExact(dateTextBox.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);


                List<jobprocess_itemdetails> jobProcessItemDetailList = jobProcessData.jobprocess_itemdetails.ToList();
                for (int i = 0; i < jobDispatchDataGridView.Rows.Count - 1; i++)
                {
                    if(i >= jobProcessItemDetailList.Count)
                    {
                        DataGridViewRow Row = jobDispatchDataGridView.Rows[i];
                        jobprocess_itemdetails jobProcessItemDetail = new jobprocess_itemdetails();
                        jobProcessItemDetail.challanNo = voucherNoTextBox.Text;
                        jobProcessItemDetail.cut = Convert.ToDouble(Row.Cells[cutTextBox.Index].EditedFormattedValue.ToString());

                        jobProcessItemDetail.dipatchBundles = Row.Cells[bundlesTextBox.Index].EditedFormattedValue.ToString();
                        jobProcessItemDetail.dispatchItemAmount = Convert.ToInt64(Row.Cells[amountTextBox.Index].EditedFormattedValue.ToString());
                        if (Row.Cells[refNoComboBox.Index].Value == null)
                            jobProcessItemDetail.dispatchReference_id = null;
                        else
                            jobProcessItemDetail.dispatchReference_id = lSelectedObjects[i];
                        jobProcessItemDetail.dispatch_mtrsQty = Convert.ToDouble(Row.Cells[qtyTextBox.Index].EditedFormattedValue.ToString());
                        jobProcessItemDetail.dispatch_pcs = Convert.ToInt64(Row.Cells[pcsTextBox.Index].EditedFormattedValue.ToString());
                        jobProcessItemDetail.hsnCode = Row.Cells[hsncTextBox.Index].EditedFormattedValue.ToString();
                        jobProcessItemDetail.item_id = (long)(Row.Cells[itemNameComboBox.Index].Value);
                        jobProcessItemDetail.jobTypeDetails = Row.Cells[qtyTextBox.Index].EditedFormattedValue.ToString();
                        jobProcessItemDetail.rate = Convert.ToDouble(Row.Cells[rateTextBox.Index].EditedFormattedValue.ToString());
                        jobProcessItemDetail.unit = Row.Cells[unitComboBox.Index].EditedFormattedValue.ToString();
                        jobProcessData.jobprocess_itemdetails.Add(jobProcessItemDetail);
                    }
                    else
                    {
                        DataGridViewRow Row = jobDispatchDataGridView.Rows[i];

                        jobProcessItemDetailList[i].challanNo = voucherNoTextBox.Text;
                        jobProcessItemDetailList[i].cut = Convert.ToDouble(Row.Cells[cutTextBox.Index].EditedFormattedValue.ToString());

                        jobProcessItemDetailList[i].dipatchBundles = Row.Cells[bundlesTextBox.Index].EditedFormattedValue.ToString();
                        jobProcessItemDetailList[i].dispatchItemAmount = Convert.ToInt64(Row.Cells[amountTextBox.Index].EditedFormattedValue.ToString());
                        if (Row.Cells[refNoComboBox.Index].Value == null)
                            jobProcessItemDetailList[i].dispatchReference_id = null;
                        else
                            jobProcessItemDetailList[i].dispatchReference_id = (long)(Row.Cells[refNoComboBox.Index].Value);
                        jobProcessItemDetailList[i].dispatch_mtrsQty = Convert.ToDouble(Row.Cells[qtyTextBox.Index].EditedFormattedValue.ToString());
                        jobProcessItemDetailList[i].dispatch_pcs = Convert.ToInt64(Row.Cells[pcsTextBox.Index].EditedFormattedValue.ToString());
                        jobProcessItemDetailList[i].hsnCode = Row.Cells[hsncTextBox.Index].EditedFormattedValue.ToString();
                        jobProcessItemDetailList[i].item_id = (long)(Row.Cells[itemNameComboBox.Index].Value);
                        jobProcessItemDetailList[i].jobTypeDetails = Row.Cells[qtyTextBox.Index].EditedFormattedValue.ToString();
                        jobProcessItemDetailList[i].rate = Convert.ToDouble(Row.Cells[rateTextBox.Index].EditedFormattedValue.ToString());
                        jobProcessItemDetailList[i].unit = Row.Cells[unitComboBox.Index].EditedFormattedValue.ToString();
                        //jobProcessData.jobprocess_itemdetails.Add(jobProcessItemDetail);
                    }
                    
                }


                bool bChalladUpdated = partyDB.updateJobWorkDispatchBill(jobProcessData);
                if (bChalladUpdated)
                {
                    MessageBox.Show("Job Work Challan Updated", "JOB WORK", MessageBoxButtons.OK);
                    setDefaultChallanNoNo();
                    resetAllData();
                    updateChallanListForAdditionalJobWork(0);
                    addNewButton.Enabled = true;
                    updateBill.Enabled = false;
                    printButton.Enabled = false;
                    deleteButton.Enabled = false;
                    viewBillButton.Enabled = true;
                }
                else
                {
                    MessageBox.Show("Job Work Challan not Updated", "ERROR", MessageBoxButtons.OK);
                }
            }
            else if(iJobWorkMode == 2)
            {
                if (jobChallanReceiveGridView.Rows.Count - 1 > 0)
                {
                    List<jobprocessreceipt_itemdetails> receiveChallanItemLidtForUpdate = partyDB.getJobWorkReceiveChallanItemListForUpdate(Convert.ToInt64(voucherNoTextBox.Text));
                    if(receiveChallanItemLidtForUpdate != null)
                    {
                        for (int i = 0; i < jobChallanReceiveGridView.Rows.Count - 1; i++)
                        {
                            if (i >= receiveChallanItemLidtForUpdate.Count)
                            {
                                jobprocessreceipt_itemdetails newReceiveItemData = new jobprocessreceipt_itemdetails();
                                jobprocess_itemdetails jobItemDetails = partyDB.getJobWorkDispatchChallanItemList(lSelectedObjects[i]);
                                if (jobItemDetails == null)
                                {
                                    MessageBox.Show("Challan not received", "ERROR", MessageBoxButtons.OK);
                                    return;
                                }

                                newReceiveItemData.item_id = Convert.ToInt64(jobChallanReceiveGridView.Rows[i].Cells[itemNameReceivveComboBox.Index].Value);
                                newReceiveItemData.challanNo = jobChallanReceiveGridView.Rows[i].Cells[refNoChallanReceiveComboBox.Index].EditedFormattedValue.ToString();
                                newReceiveItemData.cut = Convert.ToDouble(jobChallanReceiveGridView.Rows[i].Cells[cutReceiveTextBox.Index].EditedFormattedValue.ToString());
                                newReceiveItemData.freshItem = Convert.ToDouble(jobChallanReceiveGridView.Rows[i].Cells[freshReceiveTextBox.Index].EditedFormattedValue.ToString());
                                newReceiveItemData.hsnCode = jobChallanReceiveGridView.Rows[i].Cells[hsnCodeReceiveTextBox.Index].EditedFormattedValue.ToString();
                                newReceiveItemData.itemAmount = Convert.ToDouble(jobChallanReceiveGridView.Rows[i].Cells[amountReceiveTextBox.Index].EditedFormattedValue.ToString());
                                newReceiveItemData.jobProcessItemDetails_id = lSelectedObjects[i];//jobItemDetails.jobProcessItemDetails_id;
                                newReceiveItemData.jobprocess_id = jobItemDetails.jobprocess_id;
                                newReceiveItemData.multipleJobsOnItem = Convert.ToBoolean(jobChallanReceiveGridView.Rows[i].Cells[multipleJobsOnItem.Index].Value);
                                newReceiveItemData.jobTypeDetails = jobChallanReceiveGridView.Rows[i].Cells[jobTypeReceiveTextBox.Index].EditedFormattedValue.ToString();
                                newReceiveItemData.lrno = lrNoTextBox.Text;
                                newReceiveItemData.mtrs = Convert.ToDouble(jobChallanReceiveGridView.Rows[i].Cells[qunatityReceiveTextBox.Index].EditedFormattedValue.ToString());
                                newReceiveItemData.pcs = Convert.ToDouble(jobChallanReceiveGridView.Rows[i].Cells[pcsReceiveTextBox.Index].EditedFormattedValue.ToString());
                                newReceiveItemData.plainItem = Convert.ToDouble(jobChallanReceiveGridView.Rows[i].Cells[plainReceiveTextBox.Index].EditedFormattedValue.ToString());
                                newReceiveItemData.rate = Convert.ToDouble(jobChallanReceiveGridView.Rows[i].Cells[rateReceiveTextBox.Index].EditedFormattedValue.ToString());
                                newReceiveItemData.recdDate = DateTime.ParseExact(dateTextBox.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                newReceiveItemData.receiptBundles = jobChallanReceiveGridView.Rows[i].Cells[bundlesReceiveTextBox.Index].EditedFormattedValue.ToString();
                                newReceiveItemData.receiveVoucherNo = Convert.ToInt64(voucherNoTextBox.Text);
                                newReceiveItemData.remark = remarkTextBox.Text;
                                newReceiveItemData.secondItem = Convert.ToDouble(jobChallanReceiveGridView.Rows[i].Cells[secondReceiveTextBox.Index].EditedFormattedValue.ToString());
                                newReceiveItemData.shortItem = Convert.ToDouble(jobChallanReceiveGridView.Rows[i].Cells[shortReceiveTextBox.Index].EditedFormattedValue.ToString());
                                newReceiveItemData.transport_id = Convert.ToInt64(transportComboBox.SelectedValue);
                                newReceiveItemData.unit = jobChallanReceiveGridView.Rows[i].Cells[unitReceiveComboBox.Index].EditedFormattedValue.ToString();
                                receiveChallanItemLidtForUpdate.Add(newReceiveItemData);
                                //jobItemDetails.jobprocessreceipt_itemdetails = challanReceiveItemList;
                            }
                            else
                            {

                                receiveChallanItemLidtForUpdate[i].item_id = Convert.ToInt64(jobChallanReceiveGridView.Rows[i].Cells[itemNameReceivveComboBox.Index].Value);
                                receiveChallanItemLidtForUpdate[i].challanNo = jobChallanReceiveGridView.Rows[i].Cells[refNoChallanReceiveComboBox.Index].EditedFormattedValue.ToString();
                                receiveChallanItemLidtForUpdate[i].cut = Convert.ToDouble(jobChallanReceiveGridView.Rows[i].Cells[cutReceiveTextBox.Index].EditedFormattedValue.ToString());
                                receiveChallanItemLidtForUpdate[i].freshItem = Convert.ToDouble(jobChallanReceiveGridView.Rows[i].Cells[freshReceiveTextBox.Index].EditedFormattedValue.ToString());
                                receiveChallanItemLidtForUpdate[i].hsnCode = jobChallanReceiveGridView.Rows[i].Cells[hsnCodeReceiveTextBox.Index].EditedFormattedValue.ToString();
                                receiveChallanItemLidtForUpdate[i].itemAmount = Convert.ToDouble(jobChallanReceiveGridView.Rows[i].Cells[amountReceiveTextBox.Index].EditedFormattedValue.ToString());
                                receiveChallanItemLidtForUpdate[i].jobProcessItemDetails_id = lSelectedObjects[i];//jobItemDetails.jobProcessItemDetails_id;
                                                                                                                  //receiveChallanItemLidtForUpdate[i].jobprocess_id = jobItemDetails.jobprocess_id;
                                receiveChallanItemLidtForUpdate[i].jobTypeDetails = jobChallanReceiveGridView.Rows[i].Cells[jobTypeReceiveTextBox.Index].EditedFormattedValue.ToString();
                                receiveChallanItemLidtForUpdate[i].lrno = lrNoTextBox.Text;
                                receiveChallanItemLidtForUpdate[i].mtrs = Convert.ToDouble(jobChallanReceiveGridView.Rows[i].Cells[qunatityReceiveTextBox.Index].EditedFormattedValue.ToString());
                                receiveChallanItemLidtForUpdate[i].pcs = Convert.ToDouble(jobChallanReceiveGridView.Rows[i].Cells[pcsReceiveTextBox.Index].EditedFormattedValue.ToString());
                                receiveChallanItemLidtForUpdate[i].plainItem = Convert.ToDouble(jobChallanReceiveGridView.Rows[i].Cells[plainReceiveTextBox.Index].EditedFormattedValue.ToString());
                                receiveChallanItemLidtForUpdate[i].rate = Convert.ToDouble(jobChallanReceiveGridView.Rows[i].Cells[rateReceiveTextBox.Index].EditedFormattedValue.ToString());
                                receiveChallanItemLidtForUpdate[i].recdDate = DateTime.ParseExact(dateTextBox.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                receiveChallanItemLidtForUpdate[i].receiptBundles = jobChallanReceiveGridView.Rows[i].Cells[bundlesReceiveTextBox.Index].EditedFormattedValue.ToString();
                                receiveChallanItemLidtForUpdate[i].receiveVoucherNo = Convert.ToInt64(voucherNoTextBox.Text);
                                receiveChallanItemLidtForUpdate[i].multipleJobsOnItem = Convert.ToBoolean(jobChallanReceiveGridView.Rows[i].Cells[multipleJobsOnItem.Index].Value);
                                receiveChallanItemLidtForUpdate[i].remark = remarkTextBox.Text;
                                receiveChallanItemLidtForUpdate[i].secondItem = Convert.ToDouble(jobChallanReceiveGridView.Rows[i].Cells[secondReceiveTextBox.Index].EditedFormattedValue.ToString());
                                receiveChallanItemLidtForUpdate[i].shortItem = Convert.ToDouble(jobChallanReceiveGridView.Rows[i].Cells[shortReceiveTextBox.Index].EditedFormattedValue.ToString());
                                receiveChallanItemLidtForUpdate[i].transport_id = Convert.ToInt64(transportComboBox.SelectedValue);
                                receiveChallanItemLidtForUpdate[i].unit = jobChallanReceiveGridView.Rows[i].Cells[unitReceiveComboBox.Index].EditedFormattedValue.ToString();
                                //receiveChallanItemLidtForUpdate.Add(newReceiveItemData);
                            }

                        }
                    }
                    
                    
                    bool bChalladUpdated = partyDB.updateJobWorkReceiveChallanItem(receiveChallanItemLidtForUpdate);
                    if (bChalladUpdated)
                    {
                        MessageBox.Show("Job Work Receive Challan Updated", "JOB WORK", MessageBoxButtons.OK);
                        setDefaultMillReceiptVoucherNo();
                        updateReceivedChallanPendingList(0);
                        resetAllData();
                        addNewButton.Enabled = true;
                        updateBill.Enabled = false;
                        printButton.Enabled = false;
                        deleteButton.Enabled = false;
                        viewBillButton.Enabled = true;
                    }
                    else
                    {
                        MessageBox.Show("Job Work Receive Challan not Updated", "ERROR", MessageBoxButtons.OK);
                    }
                    


                }
            }
            else if(iJobWorkMode == 3)
            {
                long lVoucherNo = 0;
                bool bFlag = long.TryParse(voucherNoTextBox.Text, out lVoucherNo);
                jobreceive_billdetail jobBillDetails = partyDB.getReceiveJobWorkBillDetail(lVoucherNo);
                if (jobBillDetails == null)
                {
                    MessageBox.Show("Voucher Bill does not exist", "BILL VOUCHER", MessageBoxButtons.OK);
                    return;
                }
                if (billNoTextBox.Text == "" || billNoTextBox.Text == "0")
                {
                    MessageBox.Show("Please enter the Bill No.", "JOB WORK", MessageBoxButtons.OK);
                    billNoTextBox.Focus();
                    return;
                }
                if (jobReceiptDataGridView.Rows.Count - 1 > 0)
                {
                    List<jobprocessreceipt_itemdetails> receiveChallanItem = jobBillDetails.jobprocessreceipt_itemdetails.ToList();
                    List<jobprocessreceipt_itemdetails> receiveChallanItemUpdated = new List<jobprocessreceipt_itemdetails>();
                    if (receiveChallanItem == null)
                    {
                        MessageBox.Show("Voucher Bill data does not exist", "BILL VOUCHER", MessageBoxButtons.OK);
                        return;
                    }
                    for (int i = 0; i < jobReceiptDataGridView.Rows.Count - 1; i++)
                    {
                        if(i >= receiveChallanItem.Count)
                        {
                            jobprocessreceipt_itemdetails itemReceiveData = new jobprocessreceipt_itemdetails();
                            jobprocessreceipt_itemdetails tempData = partyDB.getJobWorkReceiptChallanItemForBill(lSelectedObjects[i]);
                            //jobprocessreceipt_itemdetails tempData = tempDataList[iSelectedObjectsIndex[i]];
                            itemReceiveData.challanNo = tempData.challanNo;
                            itemReceiveData.cut = tempData.cut;
                            itemReceiveData.hsnCode = tempData.hsnCode;
                            itemReceiveData.isDeleted = tempData.isDeleted;
                            itemReceiveData.item_id = tempData.item_id;
                            itemReceiveData.jobProcessItemDetails_id = tempData.jobProcessItemDetails_id;
                            itemReceiveData.jobprocess_id = tempData.jobprocess_id;
                            itemReceiveData.jobReceiveBillDetail_id = tempData.jobReceiveBillDetail_id;
                            itemReceiveData.jobWorkReceiptId = tempData.jobWorkReceiptId;
                            itemReceiveData.party_id = tempData.party_id;
                            itemReceiveData.pcs = tempData.pcs;
                            //itemReceiveData.rate = tempData.rate;
                            itemReceiveData.plainItem = tempData.plainItem;
                            itemReceiveData.mtrs = tempData.mtrs;
                            itemReceiveData.receiptBundles = tempData.receiptBundles;
                            itemReceiveData.receiveVoucherNo = tempData.receiveVoucherNo;
                            itemReceiveData.transport_id = tempData.transport_id;
                            itemReceiveData.recdDate = tempData.recdDate;
                            itemReceiveData.freshItem = tempData.freshItem;
                            itemReceiveData.secondItem = tempData.secondItem;
                            itemReceiveData.shortItem = tempData.shortItem;
                            itemReceiveData.unit = tempData.unit;
                            //jobprocessreceipt_itemdetails itemReceiveData = partyDB.getJobReceiveChallanDetail(lSelectedObjects[i]);
                            DataGridViewRow Row = jobReceiptDataGridView.Rows[i];
                            itemReceiveData.rate = Convert.ToDouble(Row.Cells[rateReceiptTextBox.Index].Value);
                            itemReceiveData.itemAmount = Convert.ToDouble(Row.Cells[amountReceiptTextBox.Index].Value);
                            itemReceiveData.jobTypeDetails = Convert.ToString(Row.Cells[jobTypeReceiptTextBox.Index].Value);
                            //itemReceiveData.lrno = lrNoTextBox.Text;
                            itemReceiveData.receiptBundles = Convert.ToString(Row.Cells[bundlesReceiptTextBox.Index].Value);
                            itemReceiveData.remark = remarkTextBox.Text;
                            itemReceiveData.version = tempData.version;
                            receiveChallanItemUpdated.Add(itemReceiveData);
                        }
                        else
                        {
                            jobprocessreceipt_itemdetails itemReceiveData = new jobprocessreceipt_itemdetails();
                            //List<jobprocessreceipt_itemdetails> tempDataList = partyDB.getJobReceiveChallanDetail(lSelectedObjects[i]);
                            jobprocessreceipt_itemdetails tempData = receiveChallanItem[i];
                            itemReceiveData.challanNo = tempData.challanNo;
                            itemReceiveData.cut = tempData.cut;
                            itemReceiveData.hsnCode = tempData.hsnCode;
                            itemReceiveData.isDeleted = tempData.isDeleted;
                            itemReceiveData.item_id = tempData.item_id;
                            itemReceiveData.jobProcessItemDetails_id = tempData.jobProcessItemDetails_id;
                            itemReceiveData.jobprocess_id = tempData.jobprocess_id;
                            itemReceiveData.jobReceiveBillDetail_id = tempData.jobReceiveBillDetail_id;
                            itemReceiveData.jobWorkReceiptId = tempData.jobWorkReceiptId;
                            itemReceiveData.party_id = tempData.party_id;
                            itemReceiveData.pcs = tempData.pcs;
                            //itemReceiveData.rate = tempData.rate;
                            itemReceiveData.plainItem = tempData.plainItem;
                            itemReceiveData.mtrs = tempData.mtrs;
                            itemReceiveData.receiptBundles = tempData.receiptBundles;
                            itemReceiveData.receiveVoucherNo = tempData.receiveVoucherNo;
                            itemReceiveData.transport_id = tempData.transport_id;
                            itemReceiveData.recdDate = tempData.recdDate;
                            itemReceiveData.freshItem = tempData.freshItem;
                            itemReceiveData.secondItem = tempData.secondItem;
                            itemReceiveData.shortItem = tempData.shortItem;
                            itemReceiveData.unit = tempData.unit;
                            //jobprocessreceipt_itemdetails itemReceiveData = partyDB.getJobReceiveChallanDetail(lSelectedObjects[i]);
                            DataGridViewRow Row = jobReceiptDataGridView.Rows[i];
                            itemReceiveData.rate = Convert.ToDouble(Row.Cells[rateReceiptTextBox.Index].Value);
                            itemReceiveData.itemAmount = Convert.ToDouble(Row.Cells[amountReceiptTextBox.Index].Value);
                            itemReceiveData.jobTypeDetails = Convert.ToString(Row.Cells[jobTypeReceiptTextBox.Index].Value);
                            //itemReceiveData.lrno = lrNoTextBox.Text;
                            itemReceiveData.receiptBundles = Convert.ToString(Row.Cells[bundlesReceiptTextBox.Index].Value);
                            itemReceiveData.remark = remarkTextBox.Text;
                            itemReceiveData.version = tempData.version;

                            receiveChallanItemUpdated.Add(itemReceiveData);
                        }
                        
                        
                    }
                    jobBillDetails.jobprocessreceipt_itemdetails = receiveChallanItemUpdated;
                    jobBillDetails.date = DateTime.ParseExact(dateTextBox.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    jobBillDetails.addLessDetail1 = Convert.ToDouble(addLessTextBox.Text);
                    jobBillDetails.addLessDetail2 = Convert.ToDouble(addLessAmtTextBox.Text);
                    jobBillDetails.addLessDetail3 = Convert.ToDouble(addLessAmtFinalTextBox.Text);
                    jobBillDetails.billNo = billNoTextBox.Text;
                    jobBillDetails.billVoucherNo = Convert.ToInt64(voucherNoTextBox.Text);
                    jobBillDetails.broker_id = Convert.ToInt64(brokerComboBox.SelectedValue);
                    jobBillDetails.caseNoDetail1 = caseNoTextBox.Text;
                    jobBillDetails.caseNoDetail2 = caseNoTextBox2.Text;
                    jobBillDetails.cgstAmount = Convert.ToDouble(cgstAmtTextBox.Text);
                    jobBillDetails.cgstPercentage = Convert.ToDouble(cgstRateTextBox.Text);
                    //jobBillDetails.caseNoDetail1 = Convert.ToDouble(caseNoTextBox.Text);
                    //jobBillDetails.caseNoDetail2 = Convert.ToDouble(caseNoTextBox2.Text);
                    jobBillDetails.cgstAmount = Convert.ToDouble(cgstAmtTextBox.Text);
                    jobBillDetails.cgstPercentage = Convert.ToDouble(cgstRateTextBox.Text);
                    jobBillDetails.discountAmount = Convert.ToDouble(discPerAmtFinalTextBox.Text);
                    jobBillDetails.discountOnAmount = Convert.ToDouble(discPerAmtTextBox.Text);
                    jobBillDetails.discountPercentage = Convert.ToDouble(discPerTextBox.Text);
                    jobBillDetails.grossAmount = Convert.ToDouble(totalAmountTextBox.Text);
                    jobBillDetails.gstType_id = Convert.ToInt64(gstTypeComboBox.SelectedValue);
                    jobBillDetails.igstAmount = Convert.ToDouble(igstAmtTextBox.Text);
                    jobBillDetails.igstPercentage = Convert.ToDouble(igstRateTextBox.Text);
                    jobBillDetails.lrno = lrNoTextBox.Text;
                    jobBillDetails.netAmount = Convert.ToDouble(billAmtTextBox.Text);
                    jobBillDetails.party_id = Convert.ToInt64(partyNameComboBox.SelectedValue);
                    jobBillDetails.remark = remarkTextBox.Text;
                    jobBillDetails.sgstAmount = Convert.ToDouble(sgstAmtTextBox.Text);
                    jobBillDetails.sgstPercentage = Convert.ToDouble(sgstRateTextBox.Text);
                    jobBillDetails.taxableValue = Convert.ToDouble(taxValTextBox.Text);
                    jobBillDetails.tdsAmount = Convert.ToDouble(tdsAmountTextBox.Text);
                    jobBillDetails.tdsPercentage = Convert.ToDouble(tdsTextBox.Text);
                    jobBillDetails.totalMtrs = Convert.ToDouble(totalQtyTextBox.Text);
                    jobBillDetails.totalPcs = Convert.ToDouble(totalPcsTextBox.Text);
                    jobBillDetails.transport_id = Convert.ToInt64(transportComboBox.SelectedValue);
                    jobBillDetails.workType_id = Convert.ToInt64(workTypeComboBox.SelectedValue);

                    bool bBillUpdated = partyDB.addJobWorkReceiveChallanBill(jobBillDetails);
                    if (bBillUpdated)
                    {
                        MessageBox.Show("Job Work Receive Bill Updated", "JOB WORK", MessageBoxButtons.OK);
                        setDefaultVoucherNo();
                        
                        resetAllData();
                        updateReceivedChallanPendingListForChallanReceive((long)partyNameComboBox.SelectedValue, 0);
                        addNewButton.Enabled = true;
                        updateBill.Enabled = false;
                        printButton.Enabled = false;
                        deleteButton.Enabled = false;
                        viewBillButton.Enabled = true;
                    }
                    else
                    {
                        MessageBox.Show("Job Work Receive Bill not Updated", "JOB WORK", MessageBoxButtons.OK);
                    }
                }


            }
        }

        private void viewBillButton_Click(object sender, EventArgs e)
        {
            if (iJobWorkMode == 1)
            {
                //long lChallanNo = 0;
                //long.TryParse(voucherNoTextBox.Text, out lChallanNo);
                jobProcessData = partyDB.getJobDispatchData(voucherNoTextBox.Text);
                if (jobProcessData == null)
                {
                    MessageBox.Show("Unable to fetch Dispatch Challan Data", "ERROR", MessageBoxButtons.OK);
                    setDefaultChallanNoNo();
                    resetAllData();
                    voucherNoTextBox.Focus();
                    updateChallanListForAdditionalJobWork(0);
                    return;
                }
                DateTime dt, dt1;
                bool bFlag = DateTime.TryParse(jobProcessData.dispatchDate.ToString(), out dt);
                if (bFlag)
                {
                    dt1 = dt.Date;
                    //DateTime.Parse(dt1.ToString()).ToString("dd-MM-yyyy");
                    dateTextBox.Text = dt1.ToString("dd-MM-yyyy");
                }
                partyNameComboBox.SelectedValue = (long)jobProcessData.party_id;
                gstTypeComboBox.SelectedValue = (long)jobProcessData.gstType_id;
                brokerComboBox.SelectedValue = (long)jobProcessData.broker_id;
                workTypeComboBox.SelectedValue = (long)jobProcessData.workType_id;
                transportComboBox.SelectedValue = (long)jobProcessData.transport_id;
                lrNoTextBox.Text = jobProcessData.lrno;
                remarkTextBox.Text = jobProcessData.remark;
                totalAmountTextBox.Text = Convert.ToString(jobProcessData.amountOnDispatch);
                totalPcsTextBox.Text = Convert.ToString(jobProcessData.totalPcs);
                totalQtyTextBox.Text = Convert.ToString(jobProcessData.totalMtrs);
                billAmtTextBox.Text = Convert.ToString(jobProcessData.amountOnDispatch);


                jobDispatchDataGridView.Rows.Clear();
                List<jobprocess_itemdetails> itemDataList = jobProcessData.jobprocess_itemdetails.ToList();
                if (itemDataList == null)
                {
                    MessageBox.Show("Unable to fetch Dispatch Challan Item Details", "ERROR", MessageBoxButtons.OK);
                    return;
                }
                for (int i = 0; i < jobProcessData.jobprocess_itemdetails.Count; i++)
                {
                    jobDispatchDataGridView.Rows.Add();
                    DataGridViewRow Row = jobDispatchDataGridView.Rows[i];

                    if (itemDataList[i].receiptReference_id == null && itemDataList[i].dispatchReference_id == null)
                        Row.Cells[refNoComboBox.Index].Value = null;
                    else if(itemDataList[i].receiptReference_id != null)
                    {
                        DataGridViewComboBoxCell cellchallan = (DataGridViewComboBoxCell)jobDispatchDataGridView.Rows[i].Cells[refNoComboBox.Index];
                        cellchallan.Items.Add(itemDataList[i].receiptReference_id);
                        cellchallan.ReadOnly = true;
                        cellchallan.Value = itemDataList[i].receiptReference_id;
                        lSelectedObjects.Add((long)itemDataList[i].receiptReference_id);
                    }
                    else if(itemDataList[i].dispatchReference_id != -1)
                    {
                        DataGridViewComboBoxCell cellchallan = (DataGridViewComboBoxCell)jobDispatchDataGridView.Rows[i].Cells[refNoComboBox.Index];
                        cellchallan.Items.Add(itemDataList[i].dispatchReference_id);
                        cellchallan.ReadOnly = true;
                        cellchallan.Value = itemDataList[i].dispatchReference_id;
                        lSelectedObjects.Add((long)itemDataList[i].dispatchReference_id);

                    }
                        
                    var itemList = DB.getItemList();
                    List<item_details> SelectedChallanItemList = new List<item_details>(); ;

                    if (itemList != null)
                    {
                        //List<jobprocess_itemdetails> jobWorkItemList = jobChallanData.jobprocess_itemdetails.ToList();
                        for (int j = 0; j < itemDataList.Count; j++)
                        {
                            item_details itemDetail = itemList.Where(x => x.itemid == itemDataList[j].item_id).SingleOrDefault();
                            if (!SelectedChallanItemList.Contains(itemDetail))
                                SelectedChallanItemList.Add(itemDetail);
                        }


                        DataGridViewComboBoxCell cell = (DataGridViewComboBoxCell)jobDispatchDataGridView.Rows[i].Cells[itemNameComboBox.Index];
                        cell.DataSource = SelectedChallanItemList;
                        cell.DisplayMember = "itemname";
                        cell.ValueMember = "itemid";
                        cell.ReadOnly = true;

                    }
                    
                    
                    Row.Cells[itemNameComboBox.Index].Value = itemDataList[i].item_id;
                    Row.Cells[bundlesTextBox.Index].Value = itemDataList[i].dipatchBundles;
                    Row.Cells[hsncTextBox.Index].Value = itemDataList[i].hsnCode;
                    Row.Cells[jobTypeTextBox.Index].Value = itemDataList[i].jobTypeDetails;
                    Row.Cells[unitComboBox.Index].Value = itemDataList[i].unit;
                    Row.Cells[pcsTextBox.Index].Value = itemDataList[i].dispatch_pcs;
                    Row.Cells[qtyTextBox.Index].Value = itemDataList[i].dispatch_mtrsQty;
                    Row.Cells[cutTextBox.Index].Value = itemDataList[i].cut;
                    Row.Cells[rateTextBox.Index].Value = itemDataList[i].rate;
                    Row.Cells[amountTextBox.Index].Value = itemDataList[i].dispatchItemAmount;
                    

                }
                
            }
            else if (iJobWorkMode == 2)
            {

                long lChallanNo = 0;
                long.TryParse(voucherNoTextBox.Text, out lChallanNo);
                List<jobprocessreceipt_itemdetails> jobProcessReceiveData = partyDB.getJobReceiveData(lChallanNo);
                if (jobProcessReceiveData == null || jobProcessReceiveData.Count == 0)
                {
                    MessageBox.Show("Voucher Challan does not exist", "ERROR", MessageBoxButtons.OK);
                    setDefaultMillReceiptVoucherNo();
                    resetAllData();
                    updateReceivedChallanPendingList(0);
                    voucherNoTextBox.Focus();
                    return;
                }

                jobprocess_details jobChallanData = partyDB.getJobDispatchData(jobProcessReceiveData[0].challanNo);

                if (jobChallanData == null)
                {
                    MessageBox.Show("Unable to fetch Dispatch Challan Receive Data", "ERROR", MessageBoxButtons.OK);
                    return;
                }
                DateTime dt, dt1;
                bool bFlag = DateTime.TryParse(jobProcessReceiveData[0].recdDate.ToString(), out dt);
                if (bFlag)
                {
                    dt1 = dt.Date;
                    //DateTime.Parse(dt1.ToString()).ToString("dd-MM-yyyy");
                    dateTextBox.Text = dt1.ToString("dd-MM-yyyy");
                }
                partyNameComboBox.SelectedValue = (long)jobChallanData.party_id;
                gstTypeComboBox.SelectedValue = (long)jobChallanData.gstType_id;
                brokerComboBox.SelectedValue = (long)jobChallanData.broker_id;
                workTypeComboBox.SelectedValue = (long)jobChallanData.workType_id;
                transportComboBox.SelectedValue = (long)jobProcessReceiveData[0].transport_id;
                lrNoTextBox.Text = jobProcessReceiveData[0].lrno;
                remarkTextBox.Text = jobProcessReceiveData[0].remark;
                //totalAmountTextBox.Text = Convert.ToString(jobProcessData.amountOnDispatch);
                //totalPcsTextBox.Text = Convert.ToString(jobProcessData.totalPcs);
                //totalQtyTextBox.Text = Convert.ToString(jobProcessData.totalMtrs);
                //billAmtTextBox.Text = Convert.ToString(jobProcessData.amountOnDispatch);


                jobChallanReceiveGridView.Rows.Clear();

                for (int i = 0; i < jobProcessReceiveData.Count; i++)
                {

                    //DataGridViewRow Row = new DataGridViewRow();
                    jobChallanReceiveGridView.Rows.Add();
                    DataGridViewRow Row = jobChallanReceiveGridView.Rows[i];


                    var itemList = DB.getItemList();
                    List<item_details> SelectedChallanItemList = new List<item_details>(); ;


                    if (itemList != null)
                    {
                        List<jobprocess_itemdetails> jobWorkItemList = jobChallanData.jobprocess_itemdetails.ToList();
                        for (int j = 0; j < jobWorkItemList.Count; j++)
                        {
                            item_details itemDetail = itemList.Where(x => x.itemid == jobWorkItemList[j].item_id).SingleOrDefault();
                            if (!SelectedChallanItemList.Contains(itemDetail))
                                SelectedChallanItemList.Add(itemDetail);
                        }


                        DataGridViewComboBoxCell cell = (DataGridViewComboBoxCell)jobChallanReceiveGridView.Rows[i].Cells[itemNameReceivveComboBox.Index];
                        cell.DataSource = SelectedChallanItemList;
                        cell.DisplayMember = "itemname";
                        cell.ValueMember = "itemid";
                        cell.ReadOnly = true;

                    }
                    DataGridViewComboBoxCell cellchallan = (DataGridViewComboBoxCell)jobChallanReceiveGridView.Rows[i].Cells[refNoChallanReceiveComboBox.Index];
                    cellchallan.Items.Add(jobProcessReceiveData[i].challanNo);
                    cellchallan.ReadOnly = true;

                    lSelectedObjects.Add((long)jobProcessReceiveData[i].jobProcessItemDetails_id);
                    Row.Cells[refNoChallanReceiveComboBox.Index].Value = jobProcessReceiveData[i].challanNo;
                    Row.Cells[itemNameReceivveComboBox.Index].Value = jobProcessReceiveData[i].item_id;
                    Row.Cells[bundlesReceiveTextBox.Index].Value = jobProcessReceiveData[i].receiptBundles;
                    Row.Cells[hsnCodeReceiveTextBox.Index].Value = jobProcessReceiveData[i].hsnCode;
                    Row.Cells[jobTypeReceiveTextBox.Index].Value = jobProcessReceiveData[i].jobTypeDetails;
                    Row.Cells[unitReceiveComboBox.Index].Value = jobProcessReceiveData[i].unit;
                    Row.Cells[pcsReceiveTextBox.Index].Value = jobProcessReceiveData[i].pcs;
                    Row.Cells[freshReceiveTextBox.Index].Value = jobProcessReceiveData[i].freshItem;
                    Row.Cells[qunatityReceiveTextBox.Index].Value = jobProcessReceiveData[i].mtrs;
                    Row.Cells[cutReceiveTextBox.Index].Value = jobProcessReceiveData[i].cut;
                    Row.Cells[rateReceiveTextBox.Index].Value = jobProcessReceiveData[i].rate;
                    Row.Cells[multipleJobsOnItem.Index].Value = Convert.ToBoolean(jobProcessReceiveData[i].multipleJobsOnItem);
                    Row.Cells[amountReceiveTextBox.Index].Value = jobProcessReceiveData[i].itemAmount;
                    Row.Cells[plainReceiveTextBox.Index].Value = jobProcessReceiveData[i].plainItem;
                    Row.Cells[secondReceiveTextBox.Index].Value = jobProcessReceiveData[i].secondItem;
                    Row.Cells[shortReceiveTextBox.Index].Value = jobProcessReceiveData[i].shortItem;

                    //jobDispatchDataGridView.Rows.Add(Row);


                }
                double dTotalAmount = 0, dTotalMtrs = 0, dTotalPcs = 0;
                for (int i = 0; i < jobChallanReceiveGridView.Rows.Count - 1; i++)
                {
                    double dAmount = 0, dMtrs = 0, dPcs = 0;
                    double.TryParse(jobChallanReceiveGridView.Rows[i].Cells[amountReceiveTextBox.Index].EditedFormattedValue.ToString(), out dAmount);
                    dTotalAmount = dTotalAmount + dAmount;
                    double.TryParse(jobChallanReceiveGridView.Rows[i].Cells[qunatityReceiveTextBox.Index].EditedFormattedValue.ToString(), out dMtrs);
                    dTotalMtrs = dTotalMtrs + dMtrs;
                    double.TryParse(jobChallanReceiveGridView.Rows[i].Cells[pcsReceiveTextBox.Index].EditedFormattedValue.ToString(), out dPcs);
                    dTotalPcs = dTotalPcs + dPcs;
                }
                totalAmountTextBox.Text = Convert.ToString(dTotalAmount);
                totalPcsTextBox.Text = Convert.ToString(dTotalPcs);
                totalQtyTextBox.Text = Convert.ToString(dTotalMtrs);
                billAmtTextBox.Text = totalAmountTextBox.Text;
            }
            else if (iJobWorkMode == 3)
            {
                long lVoucherNo = 0;
                bool bFlag = long.TryParse(voucherNoTextBox.Text, out lVoucherNo);
                jobreceive_billdetail viewBillData = partyDB.getReceiveJobWorkBillDetail(lVoucherNo);
                if (viewBillData == null)
                {
                    MessageBox.Show("Voucher Bill does not exist", "BILL VOUCHER", MessageBoxButtons.OK);
                    setDefaultVoucherNo();
                    resetAllData();
                    updateReceivedChallanPendingListForChallanReceive((long)partyNameComboBox.SelectedValue, 0);
                    return;
                }
                DateTime dt, dt1;
                bFlag = DateTime.TryParse(viewBillData.date.ToString(), out dt);
                if (bFlag)
                {
                    dt1 = dt.Date;
                    //DateTime.Parse(dt1.ToString()).ToString("dd-MM-yyyy");
                    dateTextBox.Text = dt1.ToString("dd-MM-yyyy");
                }
                partyNameComboBox.SelectedValue = viewBillData.party_id;
                gstTypeComboBox.SelectedValue = viewBillData.gstType_id;
                billNoTextBox.Text = viewBillData.billNo;
                brokerComboBox.SelectedValue = viewBillData.broker_id;
                workTypeComboBox.SelectedValue = viewBillData.workType_id;
                transportComboBox.SelectedValue = viewBillData.transport_id;
                lrNoTextBox.Text = viewBillData.lrno;
                remarkTextBox.Text = viewBillData.remark;
                discPerTextBox.Text = Convert.ToString(viewBillData.discountPercentage);
                discPerAmtTextBox.Text = Convert.ToString(viewBillData.discountOnAmount);
                discPerAmtFinalTextBox.Text = Convert.ToString(viewBillData.discountAmount);
                addLessTextBox.Text = Convert.ToString(viewBillData.addLessDetail1);
                addLessAmtTextBox.Text = Convert.ToString(viewBillData.addLessDetail2);
                addLessAmtFinalTextBox.Text = Convert.ToString(viewBillData.addLessDetail3);
                caseNoTextBox.Text = viewBillData.caseNoDetail1;
                caseNoTextBox2.Text = viewBillData.caseNoDetail2;
                tdsTextBox.Text = Convert.ToString(viewBillData.tdsPercentage);
                tdsAmountTextBox.Text = Convert.ToString(viewBillData.tdsAmount);
                cgstRateTextBox.Text = Convert.ToString(viewBillData.cgstPercentage);
                cgstAmtTextBox.Text = Convert.ToString(viewBillData.cgstAmount);
                sgstRateTextBox.Text = Convert.ToString(viewBillData.sgstPercentage);
                sgstAmtTextBox.Text = Convert.ToString(viewBillData.sgstAmount);
                igstRateTextBox.Text = Convert.ToString(viewBillData.igstPercentage);
                igstAmtTextBox.Text = Convert.ToString(viewBillData.igstAmount);
                taxValTextBox.Text = Convert.ToString(viewBillData.taxableValue);
                billAmtTextBox.Text = Convert.ToString(viewBillData.netAmount);
                totalAmountTextBox.Text = Convert.ToString(viewBillData.grossAmount);
                totalQtyTextBox.Text = Convert.ToString(viewBillData.totalMtrs);
                totalPcsTextBox.Text = Convert.ToString(viewBillData.totalPcs);

                List<jobprocessreceipt_itemdetails> viewReceiveItemList = viewBillData.jobprocessreceipt_itemdetails.ToList();
                jobReceiptDataGridView.Rows.Clear();
                for (int i = 0; i < viewReceiveItemList.Count; i++)
                {
                    jobReceiptDataGridView.Rows.Add();
                    var itemList = DB.getItemList();

                    if (itemList != null)
                    {

                        DataGridViewComboBoxCell cell = (DataGridViewComboBoxCell)jobReceiptDataGridView.Rows[i].Cells[itemNameReceiptComboBox.Index];
                        cell.DataSource = itemList;
                        cell.DisplayMember = "itemname";
                        cell.ValueMember = "itemid";
                        cell.ReadOnly = true;

                    }
                    DataGridViewComboBoxCell cellchallan = (DataGridViewComboBoxCell)jobReceiptDataGridView.Rows[i].Cells[refNoComboBox.Index];
                    cellchallan.Items.Add(viewReceiveItemList[i].challanNo);
                    cellchallan.ReadOnly = true;
                    DataGridViewRow Row = jobReceiptDataGridView.Rows[i];
                    Row.Cells[itemNameReceiptComboBox.Index].Value = viewReceiveItemList[i].item_id;
                    Row.Cells[refNoComboBox.Index].Value = viewReceiveItemList[i].challanNo;
                    Row.Cells[bundlesReceiptTextBox.Index].Value = viewReceiveItemList[i].receiptBundles;
                    Row.Cells[hsncodeReceiptTextBox.Index].Value = viewReceiveItemList[i].hsnCode;
                    Row.Cells[jobTypeReceiptTextBox.Index].Value = viewReceiveItemList[i].jobTypeDetails;
                    Row.Cells[unitReceiptComboBox.Index].Value = viewReceiveItemList[i].unit;
                    Row.Cells[unitReceiptComboBox.Index].ReadOnly = true;
                    Row.Cells[pcsReceiptTextBox.Index].Value = viewReceiveItemList[i].pcs;
                    Row.Cells[pcsReceiptTextBox.Index].ReadOnly = true;

                    Row.Cells[qtyReceiptTextBox.Index].Value = viewReceiveItemList[i].mtrs;
                    Row.Cells[qtyReceiptTextBox.Index].ReadOnly = true;
                    Row.Cells[freshTextBox.Index].Value = viewReceiveItemList[i].freshItem;
                    Row.Cells[freshTextBox.Index].ReadOnly = true;
                    Row.Cells[shortTextBox.Index].Value = viewReceiveItemList[i].shortItem;
                    Row.Cells[shortTextBox.Index].ReadOnly = true;
                    Row.Cells[secTextBox.Index].Value = viewReceiveItemList[i].secondItem;
                    Row.Cells[secTextBox.Index].ReadOnly = true;
                    Row.Cells[plainTextBox.Index].Value = viewReceiveItemList[i].plainItem;
                    Row.Cells[plainTextBox.Index].ReadOnly = true;
                    Row.Cells[cutReceiptTextBox.Index].Value = viewReceiveItemList[i].cut;
                    Row.Cells[cutReceiptTextBox.Index].ReadOnly = true;
                    Row.Cells[rateReceiptTextBox.Index].Value = viewReceiveItemList[i].rate;

                    Row.Cells[amountReceiptTextBox.Index].Value = viewReceiveItemList[i].itemAmount;
                    Row.Cells[amountReceiptTextBox.Index].ReadOnly = true;
                }

            }
            addNewButton.Enabled = false;
            updateBill.Enabled = true;
            printButton.Enabled = true;
            deleteButton.Enabled = true;
            viewBillButton.Enabled = true;
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            if (iJobWorkMode == 1)
            {
                DialogResult result = MessageBox.Show("DELETE JOB WORK CHALLAN ?","CONFIRM",MessageBoxButtons.YesNo);
                if(result == DialogResult.Yes)
               {
                    //long lChallanNo = 0;
                    //long.TryParse(voucherNoTextBox.Text, out lChallanNo);
                    jobProcessData = partyDB.getJobDispatchData(voucherNoTextBox.Text);
                    if (jobProcessData == null)
                    {
                        MessageBox.Show("Unable to fetch Dispatch Challan Data", "ERROR", MessageBoxButtons.OK);
                        voucherNoTextBox.Focus();
                        return;
                    }
                    bool bChallanUsageBeforeDelete = partyDB.checkJobDispatchChallanUsageBeforeDelete(voucherNoTextBox.Text);
                    if(!bChallanUsageBeforeDelete)
                    {
                        MessageBox.Show("Job Challan cannot be deleted as challan is further processed/used", "ERROR", MessageBoxButtons.OK);
                        return;
                    }
                    bool bJobWorkChallanDeleted = partyDB.deleteJobDispatchBill(voucherNoTextBox.Text);
                    if (bJobWorkChallanDeleted)
                    {
                        MessageBox.Show("Job Challan DELETED", "SUCCESS", MessageBoxButtons.OK);
                        resetAllData();
                        setDefaultChallanNoNo();
                        updateChallanListForAdditionalJobWork(0);
                        addNewButton.Enabled = true;
                        updateBill.Enabled = false;
                        printButton.Enabled = false;
                        deleteButton.Enabled = false;
                        viewBillButton.Enabled = true;
                    }
                    else
                    {
                        MessageBox.Show("Job Challan not DELETED", "ERROR", MessageBoxButtons.OK);
                    }
                }
                

            }
            else if (iJobWorkMode == 2)
            {
                DialogResult result = MessageBox.Show("DELETE JOB WORK RECEIVE VOUCHER ?", "CONFIRM", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    long lChallanNo = 0;
                    long.TryParse(voucherNoTextBox.Text, out lChallanNo);
                    List<jobprocessreceipt_itemdetails> jobReceiveDataList = partyDB.getJobReceiveData(lChallanNo);
                    if (jobReceiveDataList == null)
                    {
                        MessageBox.Show("Unable to fetch Receive Challan Voucher Data", "ERROR", MessageBoxButtons.OK);
                        voucherNoTextBox.Focus();
                        return;
                    }
                    bool bChallanUsageBeforeDelete = partyDB.checkJobReceiveChallanUsageBeforeDelete(lChallanNo);
                    if (!bChallanUsageBeforeDelete)
                    {
                        MessageBox.Show("Job Challan cannot be deleted as challan is further processed/used", "ERROR", MessageBoxButtons.OK);
                        return;
                    }

                    for (int i = 0; i < jobReceiveDataList.Count; i++)
                        jobReceiveDataList[i].isDeleted = true;
                    bool bJobWorkChallanDeleted = partyDB.updateJobWorkReceiveChallanItem(jobReceiveDataList);
                    if (bJobWorkChallanDeleted)
                    {
                        MessageBox.Show("Job Receive Challan Voucher DELETED", "SUCCESS", MessageBoxButtons.OK);
                        resetAllData();
                        setDefaultMillReceiptVoucherNo();
                        updateReceivedChallanPendingList(0);
                        addNewButton.Enabled = true;
                        updateBill.Enabled = false;
                        printButton.Enabled = false;
                        deleteButton.Enabled = false;
                        viewBillButton.Enabled = true;
                    }
                    else
                    {
                        MessageBox.Show("Job Receive Challan voucher not DELETED", "ERROR", MessageBoxButtons.OK);
                    }
                }


            }
            else if (iJobWorkMode == 3)
            {
                DialogResult result = MessageBox.Show("DELETE JOB WORK RECEIVE BILL VOUCHER ?", "CONFIRM", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    long lVoucherNo = 0;
                    long.TryParse(voucherNoTextBox.Text, out lVoucherNo);
                    jobreceive_billdetail jobBillDetails = partyDB.getReceiveJobWorkBillDetail(lVoucherNo);
                    
                    if (jobBillDetails == null)
                    {
                        MessageBox.Show("Unable to fetch Receive Challan Voucher Data", "ERROR", MessageBoxButtons.OK);
                        voucherNoTextBox.Focus();
                        return;
                    }
                    List<jobprocessreceipt_itemdetails> jobReceiveDataList = jobBillDetails.jobprocessreceipt_itemdetails.ToList();
                    if (jobReceiveDataList == null)
                    {
                        MessageBox.Show("Unable to fetch Receive Challan Voucher Data", "ERROR", MessageBoxButtons.OK);
                        voucherNoTextBox.Focus();
                        return;
                    }
                    bool bChallanUsageBeforeDelete = partyDB.checkJobReceiptBillUsageBeforeDelete(lVoucherNo);
                    if (!bChallanUsageBeforeDelete)
                    {
                        MessageBox.Show("Job Receive Bill cannot be deleted as challan is further processed/used", "ERROR", MessageBoxButtons.OK);
                        return;
                    }
                    for (int i = 0; i < jobReceiveDataList.Count; i++)
                    {
                        jobReceiveDataList[i].isDeleted = true;
                        jobReceiveDataList[i].jobReceiveBillDetail_id = null;
                    }
                        
                    bool bJobWorkVoucherBillDeleted = partyDB.deleteJobWorkReceiveChallanBill(jobBillDetails);
                    if (bJobWorkVoucherBillDeleted)
                    {
                        MessageBox.Show("Job Receive Bill Voucher DELETED", "SUCCESS", MessageBoxButtons.OK);
                        resetAllData();
                        setDefaultVoucherNo();
                        updateReceivedChallanPendingListForChallanReceive((long)partyNameComboBox.SelectedValue, 0);
                        addNewButton.Enabled = true;
                        updateBill.Enabled = false;
                        printButton.Enabled = false;
                        deleteButton.Enabled = false;
                        viewBillButton.Enabled = true;
                    }
                    else
                    {
                        MessageBox.Show("Job Receive Bill voucher not DELETED", "ERROR", MessageBoxButtons.OK);
                    }
                }


            }
        }

        private void voucherNoTextBox_Validating(object sender, CancelEventArgs e)
        {
            if(iJobWorkMode == 1)
            {
                long lChallanNo = 0;
                bool bFlag = long.TryParse(voucherNoTextBox.Text,out lChallanNo);
                if(!bFlag || lChallanNo == 0)
                {
                    MessageBox.Show("Enter valid Challan No.","ERROR",MessageBoxButtons.OK);
                    return;
                }
            }
            else if (iJobWorkMode == 2)
            {
                long lChallanNo = 0;
                bool bFlag = long.TryParse(voucherNoTextBox.Text, out lChallanNo);
                if (!bFlag || lChallanNo == 0)
                {
                    MessageBox.Show("Enter valid Challan No.to be received", "ERROR", MessageBoxButtons.OK);
                    return;
                }
            }
        }

        private void jobReceiptDataGridView_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (jobReceiptDataGridView.CurrentCell.ColumnIndex == refReceiptComboBox.Index && e.Control is ComboBox)
            {
                ComboBox comboBox = e.Control as ComboBox;
                comboBox.SelectedIndex = -1;
                comboBox.SelectedIndexChanged -= RefNoColumnComboSelectionChanged;
                comboBox.SelectedIndexChanged += RefNoColumnComboSelectionChanged;

            }
            else if (jobReceiptDataGridView.CurrentCell.ColumnIndex == itemNameReceiptComboBox.Index && e.Control is ComboBox)
            {
                ComboBox comboBox = e.Control as ComboBox;
                //comboBox.SelectedIndex = -1;
                comboBox.SelectedIndexChanged -= itemNameColumnComboSelectionChanged;
                comboBox.SelectedIndexChanged += itemNameColumnComboSelectionChanged;

            }
        }

        private void itemNameColumnComboSelectionChanged(object sender, EventArgs e)
        {
            /*long lItemId = (long)jobReceiptDataGridView.Rows[jobReceiptDataGridView.CurrentRow.Index].Cells[itemNameReceiptComboBox.Index].Value;
            if (lItemId <= 0)
            {
                MessageBox.Show("Please Select a valid Item", "ERROR", MessageBoxButtons.OK);
                return;
            }
            jobprocess_itemdetails itemDetail = partyDB.getJobWorkPendingChallanItemList(lItemId);
            if (itemDetail == null)
            {
                MessageBox.Show("Item Details not fetched", "ERROR", MessageBoxButtons.OK);
                return;
            }*/

        }

        private void jobReceiptDataGridView_CellValidated(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == amountReceiptTextBox.Index)
            {
                if (e.RowIndex == (jobReceiptDataGridView.Rows.Count - 2))
                {
                    DialogResult result = MessageBox.Show("Do You want to add new Item?", "Job Work Dispatch", MessageBoxButtons.YesNo);
                    if (result == DialogResult.Yes)
                    {
                        jobReceiptDataGridView.CausesValidation = false;
                        jobReceiptDataGridView.Rows[e.RowIndex + 1].ReadOnly = false;
                        if (iJobWorkMode == 3)
                            updateReceivedChallanPendingListForChallanReceive((long)partyNameComboBox.SelectedValue,e.RowIndex + 1);
                        
                            //DataGridViewComboBoxCell cell = (DataGridViewComboBoxCell)jobDispatchDataGridView.Rows[e.RowIndex + 1].Cells[refNoComboBox.Index];
                            //updateLatestMillChallanList((long)partyNameComboBox.SelectedValue, e.RowIndex + 1);
                            return;
                    }
                    else
                    {
                        jobReceiptDataGridView.CausesValidation = false;
                        remarkTextBox.Focus();
                    }
                }

            }
            else if(e.ColumnIndex == refReceiptComboBox.Index)
            {
                long lObjectId = Convert.ToInt64(jobReceiptDataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Value);

            }
        }

        private bool checkItemDetails(int iRwIndex)
        {
            bool bValidateItem = false;
            DataGridViewRow Row = jobReceiptDataGridView.Rows[iRwIndex];
            if(Row != null)
            {
                double dPcs = 0;
                double.TryParse(Row.Cells[pcsReceiptTextBox.Index].EditedFormattedValue.ToString(),out dPcs);
                double dMtrsQuantity = 0;
                double.TryParse(Row.Cells[qtyReceiptTextBox.Index].EditedFormattedValue.ToString(), out dMtrsQuantity);
                double dPlain = 0;
                double.TryParse(Row.Cells[plainTextBox.Index].EditedFormattedValue.ToString(), out dPlain);
                double dSecond = 0;
                double.TryParse(Row.Cells[secTextBox.Index].EditedFormattedValue.ToString(), out dSecond);
                double dShort = 0;
                double.TryParse(Row.Cells[shortTextBox.Index].EditedFormattedValue.ToString(), out dShort);
                double dFresh = 0;
                double.TryParse(Row.Cells[freshTextBox.Index].EditedFormattedValue.ToString(), out dFresh);
                if(Row.Cells[unitReceiptComboBox.Index].EditedFormattedValue.ToString() == "MTRS")
                {
                    if(dMtrsQuantity == (dFresh+dShort+dPlain+dSecond))
                    {
                        bValidateItem = true;
                        return bValidateItem;
                    }
                    else
                    {
                        bValidateItem = false;
                        return bValidateItem;
                    }
                }
                else
                {
                    if (dPcs == (dFresh + dShort + dPlain + dSecond))
                    {
                        bValidateItem = true;
                        return bValidateItem;
                    }
                    else
                    {
                        bValidateItem = false;
                        return bValidateItem;
                    }
                }

            }
            else
                return bValidateItem;
            
        }

        private bool checkReceiveItemDetails(int iRwIndex)
        {
            bool bValidateItem = false;
            DataGridViewRow Row = jobChallanReceiveGridView.Rows[iRwIndex];
            if (Row != null)
            {
                double dPcs = 0;
                double.TryParse(Row.Cells[pcsReceiveTextBox.Index].EditedFormattedValue.ToString(), out dPcs);
                double dMtrsQuantity = 0;
                double.TryParse(Row.Cells[qunatityReceiveTextBox.Index].EditedFormattedValue.ToString(), out dMtrsQuantity);
                double dPlain = 0;
                double.TryParse(Row.Cells[plainReceiveTextBox.Index].EditedFormattedValue.ToString(), out dPlain);
                double dSecond = 0;
                double.TryParse(Row.Cells[secondReceiveTextBox.Index].EditedFormattedValue.ToString(), out dSecond);
                double dShort = 0;
                double.TryParse(Row.Cells[shortReceiveTextBox.Index].EditedFormattedValue.ToString(), out dShort);
                double dFresh = 0;
                double.TryParse(Row.Cells[freshReceiveTextBox.Index].EditedFormattedValue.ToString(), out dFresh);
                if (Row.Cells[unitReceiveComboBox.Index].EditedFormattedValue.ToString() == "MTRS")
                {
                    if (dMtrsQuantity == (dFresh + dShort + dPlain + dSecond))
                    {
                        bValidateItem = true;
                        return bValidateItem;
                    }
                    else
                    {
                        bValidateItem = false;
                        return bValidateItem;
                    }
                }
                else
                {
                    if (dPcs == (dFresh + dShort + dPlain + dSecond))
                    {
                        bValidateItem = true;
                        return bValidateItem;
                    }
                    else
                    {
                        bValidateItem = false;
                        return bValidateItem;
                    }
                }

            }
            else
                return bValidateItem;

        }

        private void jobReceiptDataGridView_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (jobReceiptDataGridView.Rows.Count > 0 && (e.RowIndex <= jobReceiptDataGridView.Rows.Count - 1) && jobReceiptDataGridView.Rows[e.RowIndex].ReadOnly == false)
            {
                if (e.ColumnIndex == refReceiptComboBox.Index)
                {
                    
                    if ((int)jobReceiptDataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].EditedFormattedValue.ToString().Length <= 0)
                    {
                        
                        if (refReceiptComboBox.Items.Count != 0)
                        {
                            MessageBox.Show("Please select the correct item", "INPUT ERROR", MessageBoxButtons.OK);
                            //saleDataGridView.CurrentCell = saleDataGridView[e.RowIndex, e.ColumnIndex];
                            jobReceiptDataGridView.CurrentCell.Selected = true;
                            e.Cancel = true;
                        }

                    }
                    if (jobReceiptDataGridView.Rows.Count > e.RowIndex + 1)
                        jobReceiptDataGridView.Rows[e.RowIndex + 1].ReadOnly = true;
                }
                else if(e.ColumnIndex == pcsReceiptTextBox.Index)
                {
                    long lPcs = 0;
                    long.TryParse(jobReceiptDataGridView.Rows[e.RowIndex].Cells[freshTextBox.Index].EditedFormattedValue.ToString(),out lPcs);
                    double dCut = 0;
                    double.TryParse(jobReceiptDataGridView.Rows[e.RowIndex].Cells[cutReceiptTextBox.Index].EditedFormattedValue.ToString(),out dCut);

                    long lActualPcs = 0;
                    long.TryParse(jobReceiptDataGridView.Rows[e.RowIndex].Cells[pcsReceiptTextBox.Index].EditedFormattedValue.ToString(), out lActualPcs);
                    jobReceiptDataGridView.Rows[e.RowIndex].Cells[qtyReceiptTextBox.Index].Value = lActualPcs * dCut;


                    //jobReceiptDataGridView.Rows[e.RowIndex].Cells[qtyReceiptTextBox.Index].Value = dCut * lPcs;
                    double dRate = 0;
                    double.TryParse(jobReceiptDataGridView.Rows[e.RowIndex].Cells[rateReceiptTextBox.Index].EditedFormattedValue.ToString(), out dRate);
                    if (jobReceiptDataGridView.Rows[e.RowIndex].Cells[unitReceiptComboBox.Index].EditedFormattedValue.ToString() == "MTRS")
                        jobReceiptDataGridView.Rows[e.RowIndex].Cells[amountReceiptTextBox.Index].Value = dCut * lPcs * dRate;
                    else
                        jobReceiptDataGridView.Rows[e.RowIndex].Cells[amountReceiptTextBox.Index].Value = lPcs * dRate;
                    double dTotalAmount = 0,dTotalPcs =0,dTotalMtrs = 0;
                    for(int i = 0;i < jobReceiptDataGridView.Rows.Count-1;i++)
                    {
                        double dAmount = 0,dMtrs = 0,dPcs = 0;
                        double.TryParse(jobReceiptDataGridView.Rows[i].Cells[amountReceiptTextBox.Index].EditedFormattedValue.ToString(), out dAmount);
                        dTotalAmount = dTotalAmount + dAmount;
                        double.TryParse(jobReceiptDataGridView.Rows[i].Cells[qtyReceiptTextBox.Index].EditedFormattedValue.ToString(), out dMtrs);
                        dTotalMtrs = dTotalMtrs + dMtrs;
                        double.TryParse(jobReceiptDataGridView.Rows[i].Cells[pcsReceiptTextBox.Index].EditedFormattedValue.ToString(), out dPcs);
                        dTotalPcs = dTotalPcs + dPcs;
                    }
                    discPerAmtTextBox.Text = Convert.ToString(dTotalAmount);
                    totalAmountTextBox.Text = Convert.ToString(dTotalAmount);
                    totalPcsTextBox.Text = Convert.ToString(dTotalPcs);
                    totalQtyTextBox.Text = Convert.ToString(dTotalMtrs);
                    
                }
                else if (e.ColumnIndex == qtyReceiptTextBox.Index)
                {
                    long lPcs = 0;
                    long.TryParse(jobReceiptDataGridView.Rows[e.RowIndex].Cells[freshTextBox.Index].EditedFormattedValue.ToString(), out lPcs);
                    double dCut = 0;
                    double.TryParse(jobReceiptDataGridView.Rows[e.RowIndex].Cells[cutReceiptTextBox.Index].EditedFormattedValue.ToString(), out dCut);
                    double dQuantity = 0;
                    
                    double.TryParse(jobReceiptDataGridView.Rows[e.RowIndex].Cells[qtyReceiptTextBox.Index].EditedFormattedValue.ToString(),out dQuantity);

                    double dRate = 0;
                    double.TryParse(jobReceiptDataGridView.Rows[e.RowIndex].Cells[rateReceiptTextBox.Index].EditedFormattedValue.ToString(), out dRate);
                    if (jobReceiptDataGridView.Rows[e.RowIndex].Cells[unitReceiptComboBox.Index].EditedFormattedValue.ToString() == "MTRS")
                        jobReceiptDataGridView.Rows[e.RowIndex].Cells[amountReceiptTextBox.Index].Value = dQuantity * dRate;
                    else
                        jobReceiptDataGridView.Rows[e.RowIndex].Cells[amountReceiptTextBox.Index].Value = lPcs * dRate;
                    double dTotalAmount = 0, dTotalPcs = 0, dTotalMtrs = 0;
                    for (int i = 0; i < jobReceiptDataGridView.Rows.Count - 1; i++)
                    {
                        double dAmount = 0, dMtrs = 0, dPcs = 0;
                        double.TryParse(jobReceiptDataGridView.Rows[i].Cells[amountReceiptTextBox.Index].EditedFormattedValue.ToString(), out dAmount);
                        dTotalAmount = dTotalAmount + dAmount;
                        double.TryParse(jobReceiptDataGridView.Rows[i].Cells[qtyReceiptTextBox.Index].EditedFormattedValue.ToString(), out dMtrs);
                        dTotalMtrs = dTotalMtrs + dMtrs;
                        double.TryParse(jobReceiptDataGridView.Rows[i].Cells[pcsReceiptTextBox.Index].EditedFormattedValue.ToString(), out dPcs);
                        dTotalPcs = dTotalPcs + dPcs;
                    }
                    discPerAmtTextBox.Text = Convert.ToString(dTotalAmount);
                    totalAmountTextBox.Text = Convert.ToString(dTotalAmount);
                    totalPcsTextBox.Text = Convert.ToString(dTotalPcs);
                    totalQtyTextBox.Text = Convert.ToString(dTotalMtrs);
                    
                }
                else if (e.ColumnIndex == cutReceiptTextBox.Index)
                {
                    long lPcs = 0;
                    long.TryParse(jobReceiptDataGridView.Rows[e.RowIndex].Cells[freshTextBox.Index].EditedFormattedValue.ToString(), out lPcs);
                    double dCut = 0;
                    double.TryParse(jobReceiptDataGridView.Rows[e.RowIndex].Cells[cutReceiptTextBox.Index].EditedFormattedValue.ToString(), out dCut);
                    double dQuantity = 0;
                    double.TryParse(jobReceiptDataGridView.Rows[e.RowIndex].Cells[qtyReceiptTextBox.Index].EditedFormattedValue.ToString(), out dQuantity);

                    double dRate = 0;
                    double.TryParse(jobReceiptDataGridView.Rows[e.RowIndex].Cells[rateReceiptTextBox.Index].EditedFormattedValue.ToString(), out dRate);
                    if (jobReceiptDataGridView.Rows[e.RowIndex].Cells[unitReceiptComboBox.Index].EditedFormattedValue.ToString() == "MTRS")
                        jobReceiptDataGridView.Rows[e.RowIndex].Cells[amountReceiptTextBox.Index].Value = dQuantity * dRate;
                    else
                        jobReceiptDataGridView.Rows[e.RowIndex].Cells[amountReceiptTextBox.Index].Value = lPcs * dRate;
                    double dTotalAmount = 0, dTotalPcs = 0, dTotalMtrs = 0;
                    for (int i = 0; i < jobReceiptDataGridView.Rows.Count - 1; i++)
                    {
                        double dAmount = 0, dMtrs = 0, dPcs = 0;
                        double.TryParse(jobReceiptDataGridView.Rows[i].Cells[amountReceiptTextBox.Index].EditedFormattedValue.ToString(), out dAmount);
                        dTotalAmount = dTotalAmount + dAmount;
                        double.TryParse(jobReceiptDataGridView.Rows[i].Cells[qtyReceiptTextBox.Index].EditedFormattedValue.ToString(), out dMtrs);
                        dTotalMtrs = dTotalMtrs + dMtrs;
                        double.TryParse(jobReceiptDataGridView.Rows[i].Cells[pcsReceiptTextBox.Index].EditedFormattedValue.ToString(), out dPcs);
                        dTotalPcs = dTotalPcs + dPcs;
                    }
                    discPerAmtTextBox.Text = Convert.ToString(dTotalAmount);
                    totalAmountTextBox.Text = Convert.ToString(dTotalAmount);
                    totalPcsTextBox.Text = Convert.ToString(dTotalPcs);
                    totalQtyTextBox.Text = Convert.ToString(dTotalMtrs);
                    
                }
                else if (e.ColumnIndex == rateReceiptTextBox.Index)
                {
                    long lPcs = 0;
                    long.TryParse(jobReceiptDataGridView.Rows[e.RowIndex].Cells[freshTextBox.Index].EditedFormattedValue.ToString(), out lPcs);
                    double dCut = 0;
                    double.TryParse(jobReceiptDataGridView.Rows[e.RowIndex].Cells[cutReceiptTextBox.Index].EditedFormattedValue.ToString(), out dCut);
                    double dQuantity = 0;
                    double.TryParse(jobReceiptDataGridView.Rows[e.RowIndex].Cells[qtyReceiptTextBox.Index].EditedFormattedValue.ToString(), out dQuantity);

                    double dRate = 0;
                    double.TryParse(jobReceiptDataGridView.Rows[e.RowIndex].Cells[rateReceiptTextBox.Index].EditedFormattedValue.ToString(), out dRate);
                    if (jobReceiptDataGridView.Rows[e.RowIndex].Cells[unitReceiptComboBox.Index].EditedFormattedValue.ToString() == "MTRS")
                        jobReceiptDataGridView.Rows[e.RowIndex].Cells[amountReceiptTextBox.Index].Value = dQuantity * dRate;
                    else
                        jobReceiptDataGridView.Rows[e.RowIndex].Cells[amountReceiptTextBox.Index].Value = lPcs * dRate;
                    double dTotalAmount = 0, dTotalPcs = 0, dTotalMtrs = 0;
                    for (int i = 0; i < jobReceiptDataGridView.Rows.Count - 1; i++)
                    {
                        double dAmount = 0, dMtrs = 0, dPcs = 0;
                        double.TryParse(jobReceiptDataGridView.Rows[i].Cells[amountReceiptTextBox.Index].EditedFormattedValue.ToString(), out dAmount);
                        dTotalAmount = dTotalAmount + dAmount;
                        double.TryParse(jobReceiptDataGridView.Rows[i].Cells[qtyReceiptTextBox.Index].EditedFormattedValue.ToString(), out dMtrs);
                        dTotalMtrs = dTotalMtrs + dMtrs;
                        double.TryParse(jobReceiptDataGridView.Rows[i].Cells[pcsReceiptTextBox.Index].EditedFormattedValue.ToString(), out dPcs);
                        dTotalPcs = dTotalPcs + dPcs;
                    }
                    discPerAmtTextBox.Text = Convert.ToString(dTotalAmount);
                    totalAmountTextBox.Text = Convert.ToString(dTotalAmount);
                    totalPcsTextBox.Text = Convert.ToString(dTotalPcs);
                    totalQtyTextBox.Text = Convert.ToString(dTotalMtrs);
                    
                }
                else if(e.ColumnIndex == freshTextBox.Index)
                {
                    bool bValidateItemSum = checkItemDetails(e.RowIndex);
                    if (!bValidateItemSum)
                    {
                        MessageBox.Show("Item details not matching with Total PCS/MTRS", "INPUT ERROR", MessageBoxButtons.OK);
                        jobReceiptDataGridView.CurrentCell.Selected = true;
                        e.Cancel = true;
                        
                    }

                    long lPcs = 0;
                    long.TryParse(jobReceiptDataGridView.Rows[e.RowIndex].Cells[freshTextBox.Index].EditedFormattedValue.ToString(), out lPcs);
                    double dCut = 0;
                    double.TryParse(jobReceiptDataGridView.Rows[e.RowIndex].Cells[cutReceiptTextBox.Index].EditedFormattedValue.ToString(), out dCut);
                    double dQuantity = 0;
                    double.TryParse(jobReceiptDataGridView.Rows[e.RowIndex].Cells[qtyReceiptTextBox.Index].EditedFormattedValue.ToString(), out dQuantity);

                    double dRate = 0;
                    double.TryParse(jobReceiptDataGridView.Rows[e.RowIndex].Cells[rateReceiptTextBox.Index].EditedFormattedValue.ToString(), out dRate);
                    if (jobReceiptDataGridView.Rows[e.RowIndex].Cells[unitReceiptComboBox.Index].EditedFormattedValue.ToString() == "MTRS")
                        jobReceiptDataGridView.Rows[e.RowIndex].Cells[amountReceiptTextBox.Index].Value = dQuantity * dRate;
                    else
                        jobReceiptDataGridView.Rows[e.RowIndex].Cells[amountReceiptTextBox.Index].Value = lPcs * dRate;
                    double dTotalAmount = 0, dTotalPcs = 0, dTotalMtrs = 0;
                    for (int i = 0; i < jobReceiptDataGridView.Rows.Count - 1; i++)
                    {
                        double dAmount = 0, dMtrs = 0, dPcs = 0;
                        double.TryParse(jobReceiptDataGridView.Rows[i].Cells[amountReceiptTextBox.Index].EditedFormattedValue.ToString(), out dAmount);
                        dTotalAmount = dTotalAmount + dAmount;
                        double.TryParse(jobReceiptDataGridView.Rows[i].Cells[qtyReceiptTextBox.Index].EditedFormattedValue.ToString(), out dMtrs);
                        dTotalMtrs = dTotalMtrs + dMtrs;
                        double.TryParse(jobReceiptDataGridView.Rows[i].Cells[pcsReceiptTextBox.Index].EditedFormattedValue.ToString(), out dPcs);
                        dTotalPcs = dTotalPcs + dPcs;
                    }
                    discPerAmtTextBox.Text = Convert.ToString(dTotalAmount);
                    totalAmountTextBox.Text = Convert.ToString(dTotalAmount);
                    totalPcsTextBox.Text = Convert.ToString(dTotalPcs);
                    totalQtyTextBox.Text = Convert.ToString(dTotalMtrs);
                    
                }
                else if (e.ColumnIndex == bundlesReceiptTextBox.Index)
                {
                    string bundlesString = jobReceiptDataGridView.Rows[e.RowIndex].Cells[bundlesReceiptTextBox.Index].EditedFormattedValue.ToString();
                    if (bundlesString != "")
                    {
                        DataTable dt = new DataTable();
                        try
                        {
                            int answer = (int)dt.Compute(bundlesString, "");
                            jobReceiptDataGridView.Rows[e.RowIndex].Cells[pcsReceiptTextBox.Index].Value = answer;
                        }
                        catch (Exception ex)
                        {
                            jobReceiptDataGridView.Rows[e.RowIndex].Cells[pcsReceiptTextBox.Index].Value = 0;
                        }
                    }
                    else
                        jobReceiptDataGridView.Rows[e.RowIndex].Cells[pcsReceiptTextBox.Index].Value = 0;

                }

            }
        }

        private void billNoTextBox_Validating(object sender, CancelEventArgs e)
        {
            if(iJobWorkMode == 3)
            {
                string strBillNo = billNoTextBox.Text;
                if (strBillNo.Length <= 0 || strBillNo == "0")
                {
                    MessageBox.Show("Enter Bill No.", "BILL NO.", MessageBoxButtons.OK);
                    billNoTextBox.Focus();
                    return;
                }
            }
            
        }

        private void discPerTextBox_TextChanged(object sender, EventArgs e)
        {
            double dDisountPercentage = 0;
            bool bFlag = double.TryParse(discPerTextBox.Text,out dDisountPercentage);
            if (!bFlag)
            {
                discPerTextBox.Text = "0";
                discPerTextBox.SelectAll();
                return;
            }
            double dDiscountOnAmount = Convert.ToDouble(discPerAmtTextBox.Text);
            double dDiscountAmount = ((dDisountPercentage / 100) * dDiscountOnAmount);
            discPerAmtFinalTextBox.Text = Convert.ToString(dDiscountAmount);
            double dAddLess = Convert.ToDouble(addLessAmtFinalTextBox.Text);
            double dTdsAmount = Convert.ToDouble(tdsAmountTextBox.Text);
            double dCgstAmount = Convert.ToDouble(cgstAmtTextBox.Text);
            double dSgstAmount = Convert.ToDouble(sgstAmtTextBox.Text);
            double dIgstAmount = Convert.ToDouble(igstAmtTextBox.Text);
            double dGrossAmount = Convert.ToDouble(totalAmountTextBox.Text);
            double dTaxableValue = Convert.ToDouble(dGrossAmount - dDiscountAmount - dAddLess);
            taxValTextBox.Text = Convert.ToString(dTaxableValue);
            double dNetAmount = 0;
            if (partyGstnTextBox.Text != "")
            {
                if ((long)gstTypeComboBox.SelectedValue == 1)
                    dNetAmount = dTaxableValue - dTdsAmount - dCgstAmount - dSgstAmount;
                else if ((long)gstTypeComboBox.SelectedValue == 2)
                    dNetAmount = dTaxableValue - dTdsAmount - dIgstAmount;
            }
            billAmtTextBox.Text = Convert.ToString(dNetAmount);

        }

        private void gstTypeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if((long)gstTypeComboBox.SelectedValue == 1)
            {
                cgstRateTextBox.Enabled = true;
                sgstRateTextBox.Enabled = true;
                cgstAmtTextBox.Enabled = true;
                sgstAmtTextBox.Enabled = true;
                igstAmtTextBox.Enabled = false;
                igstRateTextBox.Enabled = false;
                sgstRateTextBox.Text = "2.5";
                cgstRateTextBox.Text = "2.5";
                igstRateTextBox.Text = "0";

            }
            else if((long)gstTypeComboBox.SelectedValue == 2)
            {
                cgstRateTextBox.Enabled = false;
                sgstRateTextBox.Enabled = false;
                cgstAmtTextBox.Enabled = false;
                sgstAmtTextBox.Enabled = false;
                igstAmtTextBox.Enabled = true;
                igstRateTextBox.Enabled = true;
                sgstRateTextBox.Text = "0";
                cgstRateTextBox.Text = "0";
                igstRateTextBox.Text = "5";
            }

        }

        private void discPerAmtTextBox_TextChanged(object sender, EventArgs e)
        {
            double dDiscountOnAmount = 0;
            bool bFlag = double.TryParse(discPerAmtTextBox.Text, out dDiscountOnAmount);
            if (!bFlag)
            {
                discPerAmtTextBox.Text = "0";
                discPerAmtTextBox.SelectAll();
                return;
            }
            double dDisountPercentage = Convert.ToDouble(discPerTextBox.Text);
            //dDiscountOnAmount = Convert.ToDouble(discPerAmtTextBox.Text);
            double dDiscountAmount = ((dDisountPercentage / 100) * dDiscountOnAmount);
            discPerAmtFinalTextBox.Text = Convert.ToString(dDiscountAmount);
            double dAddLess = Convert.ToDouble(addLessAmtFinalTextBox.Text);
            double dTdsAmount = Convert.ToDouble(tdsAmountTextBox.Text);
            double dCgstAmount = Convert.ToDouble(cgstAmtTextBox.Text);
            double dSgstAmount = Convert.ToDouble(sgstAmtTextBox.Text);
            double dIgstAmount = Convert.ToDouble(igstAmtTextBox.Text);
            double dGrossAmount = Convert.ToDouble(totalAmountTextBox.Text);
            double dTaxableValue = Convert.ToDouble(dGrossAmount - dDiscountAmount + dAddLess);
            taxValTextBox.Text = Convert.ToString(dTaxableValue);
            double dNetAmount = 0;
            if (partyGstnTextBox.Text != "")
            {
                if ((long)gstTypeComboBox.SelectedValue == 1)
                    dNetAmount = dTaxableValue - dTdsAmount - dCgstAmount - dSgstAmount;
                else if ((long)gstTypeComboBox.SelectedValue == 2)
                    dNetAmount = dTaxableValue - dTdsAmount - dIgstAmount;
            }
            billAmtTextBox.Text = Convert.ToString(dNetAmount);
        }

        private void discPerAmtFinalTextBox_TextChanged(object sender, EventArgs e)
        {
            double dDiscountAmount = 0;
            bool bFlag = double.TryParse(discPerAmtFinalTextBox.Text, out dDiscountAmount);
            if (!bFlag)
            {
                discPerAmtFinalTextBox.Text = "0";
                discPerAmtFinalTextBox.SelectAll();
                return;
            }
            double dDisountPercentage = Convert.ToDouble(discPerTextBox.Text);
            double dDiscountOnAmount = Convert.ToDouble(discPerAmtTextBox.Text);
            //dDiscountAmount = ((dDisountPercentage / 100) * dDiscountOnAmount);
            //discPerAmtFinalTextBox.Text = Convert.ToString(dDiscountAmount);
            double dAddLess = Convert.ToDouble(addLessAmtFinalTextBox.Text);
            double dTdsAmount = Convert.ToDouble(tdsAmountTextBox.Text);
            double dCgstAmount = Convert.ToDouble(cgstAmtTextBox.Text);
            double dSgstAmount = Convert.ToDouble(sgstAmtTextBox.Text);
            double dIgstAmount = Convert.ToDouble(igstAmtTextBox.Text);
            double dGrossAmount = Convert.ToDouble(totalAmountTextBox.Text);
            double dTaxableValue = Convert.ToDouble(dGrossAmount - dDiscountAmount + dAddLess);
            taxValTextBox.Text = Convert.ToString(dTaxableValue);
            double dNetAmount = 0;
            if (partyGstnTextBox.Text != "")
            {
                if ((long)gstTypeComboBox.SelectedValue == 1)
                    dNetAmount = dTaxableValue - dTdsAmount - dCgstAmount - dSgstAmount;
                else if ((long)gstTypeComboBox.SelectedValue == 2)
                    dNetAmount = dTaxableValue - dTdsAmount - dIgstAmount;
            }
            billAmtTextBox.Text = Convert.ToString(dNetAmount);
        }

        private void addLessTextBox_TextChanged(object sender, EventArgs e)
        {
            double dAddLess = 0;
            bool bFlag = double.TryParse(addLessTextBox.Text, out dAddLess);
            if (!bFlag)
            {
                addLessTextBox.Text = "0";
                addLessTextBox.SelectAll();
                return;
            }
            double dDiscountAmount = Convert.ToDouble(discPerAmtFinalTextBox.Text);
            //discPerAmtFinalTextBox.Text = Convert.ToString(dDiscountAmount);
            dAddLess = dAddLess * Convert.ToDouble(addLessAmtTextBox.Text);
            addLessAmtFinalTextBox.Text = Convert.ToString(dAddLess);
            double dTdsAmount = Convert.ToDouble(tdsAmountTextBox.Text);
            double dCgstAmount = Convert.ToDouble(cgstAmtTextBox.Text);
            double dSgstAmount = Convert.ToDouble(sgstAmtTextBox.Text);
            double dIgstAmount = Convert.ToDouble(igstAmtTextBox.Text);
            double dGrossAmount = Convert.ToDouble(totalAmountTextBox.Text);
            double dTaxableValue = Convert.ToDouble(dGrossAmount - dDiscountAmount + dAddLess);
            taxValTextBox.Text = Convert.ToString(dTaxableValue);
            double dNetAmount = 0;
            if (partyGstnTextBox.Text != "")
            {
                if ((long)gstTypeComboBox.SelectedValue == 1)
                    dNetAmount = dTaxableValue - dTdsAmount - dCgstAmount - dSgstAmount;
                else if ((long)gstTypeComboBox.SelectedValue == 2)
                    dNetAmount = dTaxableValue - dTdsAmount - dIgstAmount;
            }
            billAmtTextBox.Text = Convert.ToString(dNetAmount);
        }

        private void addLessAmtTextBox_TextChanged(object sender, EventArgs e)
        {
            double dAddLess = 0;
            bool bFlag = double.TryParse(addLessAmtTextBox.Text, out dAddLess);
            if (!bFlag)
            {
                addLessAmtTextBox.Text = "0";
                addLessAmtTextBox.SelectAll();
                return;
            }
            double dDiscountAmount = Convert.ToDouble(discPerAmtFinalTextBox.Text);
            //discPerAmtFinalTextBox.Text = Convert.ToString(dDiscountAmount);
            dAddLess = dAddLess * Convert.ToDouble(addLessTextBox.Text);
            addLessAmtFinalTextBox.Text = Convert.ToString(dAddLess);
            double dTdsAmount = Convert.ToDouble(tdsAmountTextBox.Text);
            double dCgstAmount = Convert.ToDouble(cgstAmtTextBox.Text);
            double dSgstAmount = Convert.ToDouble(sgstAmtTextBox.Text);
            double dIgstAmount = Convert.ToDouble(igstAmtTextBox.Text);
            double dGrossAmount = Convert.ToDouble(totalAmountTextBox.Text);
            double dTaxableValue = Convert.ToDouble(dGrossAmount - dDiscountAmount + dAddLess);
            taxValTextBox.Text = Convert.ToString(dTaxableValue);
            double dNetAmount = 0;
            if (partyGstnTextBox.Text != "")
            {
                if ((long)gstTypeComboBox.SelectedValue == 1)
                    dNetAmount = dTaxableValue - dTdsAmount - dCgstAmount - dSgstAmount;
                else if ((long)gstTypeComboBox.SelectedValue == 2)
                    dNetAmount = dTaxableValue - dTdsAmount - dIgstAmount;
            }
            billAmtTextBox.Text = Convert.ToString(dNetAmount);

        }

        private void addLessAmtFinalTextBox_TextChanged(object sender, EventArgs e)
        {
            double dAddLess = 0;
            bool bFlag = double.TryParse(addLessAmtFinalTextBox.Text, out dAddLess);
            if (!bFlag)
            {
                addLessAmtFinalTextBox.Text = "0";
                addLessAmtFinalTextBox.SelectAll();
                return;
            }
            double dDiscountAmount = Convert.ToDouble(discPerAmtFinalTextBox.Text);

            //dAddLess = dAddLess * Convert.ToDouble(addLessTextBox.Text);
            double dTdsAmount = Convert.ToDouble(tdsAmountTextBox.Text);
            double dCgstAmount = Convert.ToDouble(cgstAmtTextBox.Text);
            double dSgstAmount = Convert.ToDouble(sgstAmtTextBox.Text);
            double dIgstAmount = Convert.ToDouble(igstAmtTextBox.Text);
            double dGrossAmount = Convert.ToDouble(totalAmountTextBox.Text);
            double dTaxableValue = Convert.ToDouble(dGrossAmount - dDiscountAmount + dAddLess);
            taxValTextBox.Text = Convert.ToString(dTaxableValue);
            double dNetAmount = 0;
            if (partyGstnTextBox.Text != "")
            {
                if ((long)gstTypeComboBox.SelectedValue == 1)
                    dNetAmount = dTaxableValue - dTdsAmount - dCgstAmount - dSgstAmount;
                else if ((long)gstTypeComboBox.SelectedValue == 2)
                    dNetAmount = dTaxableValue - dTdsAmount - dIgstAmount;
            }
            billAmtTextBox.Text = Convert.ToString(dNetAmount);
        }

        private void tdsTextBox_TextChanged(object sender, EventArgs e)
        {
            double dTdsPercentage = 0;
            bool bFlag = double.TryParse(tdsTextBox.Text, out dTdsPercentage);
            if (!bFlag)
            {
                tdsTextBox.Text = "0";
                tdsTextBox.SelectAll();
                return;
            }
            double dDiscountAmount = Convert.ToDouble(discPerAmtFinalTextBox.Text);

            double dAddLess = Convert.ToDouble(addLessAmtFinalTextBox.Text);
            
            double dCgstAmount = Convert.ToDouble(cgstAmtTextBox.Text);
            double dSgstAmount = Convert.ToDouble(sgstAmtTextBox.Text);
            double dIgstAmount = Convert.ToDouble(igstAmtTextBox.Text);
            double dGrossAmount = Convert.ToDouble(totalAmountTextBox.Text);
            double dTaxableValue = Convert.ToDouble(dGrossAmount - dDiscountAmount + dAddLess);
            taxValTextBox.Text = Convert.ToString(dTaxableValue);
            double dTdsAmount = (dTdsPercentage / 100) * dTaxableValue;
            tdsAmountTextBox.Text = Convert.ToString(dTdsAmount);
            double dNetAmount = 0;
            if (partyGstnTextBox.Text != "")
            {
                if ((long)gstTypeComboBox.SelectedValue == 1)
                    dNetAmount = dTaxableValue - dTdsAmount - dCgstAmount - dSgstAmount;
                else if ((long)gstTypeComboBox.SelectedValue == 2)
                    dNetAmount = dTaxableValue - dTdsAmount - dIgstAmount;
            }
            billAmtTextBox.Text = Convert.ToString(dNetAmount);
        }

        private void tdsAmountTextBox_TextChanged(object sender, EventArgs e)
        {
            double dTdsAmount = 0;
            bool bFlag = double.TryParse(tdsAmountTextBox.Text, out dTdsAmount);
            if (!bFlag)
            {
                tdsAmountTextBox.Text = "0";
                tdsAmountTextBox.SelectAll();
                return;
            }
            double dDiscountAmount = Convert.ToDouble(discPerAmtFinalTextBox.Text);

            double dAddLess = Convert.ToDouble(addLessAmtFinalTextBox.Text);

            double dCgstAmount = Convert.ToDouble(cgstAmtTextBox.Text);
            double dSgstAmount = Convert.ToDouble(sgstAmtTextBox.Text);
            double dIgstAmount = Convert.ToDouble(igstAmtTextBox.Text);
            double dGrossAmount = Convert.ToDouble(totalAmountTextBox.Text);
            double dTaxableValue = Convert.ToDouble(dGrossAmount - dDiscountAmount + dAddLess);
            
            //double dTdsAmount = (dTdsPercentage / 100) * dTaxableValue;
            //tdsAmountTextBox.Text = Convert.ToString(dTdsAmount);
            double dNetAmount = 0;
            if (partyGstnTextBox.Text != "")
            {
                if ((long)gstTypeComboBox.SelectedValue == 1)
                    dNetAmount = dTaxableValue - dTdsAmount - dCgstAmount - dSgstAmount;
                else if ((long)gstTypeComboBox.SelectedValue == 2)
                    dNetAmount = dTaxableValue - dTdsAmount - dIgstAmount;
            }
            billAmtTextBox.Text = Convert.ToString(dNetAmount);
        }

        private void cgstRateTextBox_TextChanged(object sender, EventArgs e)
        {
            double dCgstRate = 0;
            bool bFlag = double.TryParse(cgstRateTextBox.Text, out dCgstRate);
            if (!bFlag)
            {
                cgstRateTextBox.Text = "0";
                cgstRateTextBox.SelectAll();
                return;
            }
            double dDiscountAmount = Convert.ToDouble(discPerAmtFinalTextBox.Text);

            double dAddLess = Convert.ToDouble(addLessAmtFinalTextBox.Text);

            double dGrossAmount = Convert.ToDouble(totalAmountTextBox.Text);
            double dTaxableValue = Convert.ToDouble(dGrossAmount - dDiscountAmount + dAddLess);
            double dCgstAmount = (dCgstRate / 100) * dTaxableValue;
            cgstAmtTextBox.Text = Convert.ToString(dCgstAmount);
            sgstRateTextBox.Text = cgstRateTextBox.Text;
            
            sgstAmtTextBox.Text = Convert.ToString(dCgstAmount);

            double dSgstAmount = Convert.ToDouble(sgstAmtTextBox.Text);
            double dIgstAmount = Convert.ToDouble(igstAmtTextBox.Text);

            double dTdsAmount = Convert.ToDouble(tdsAmountTextBox.Text);
            //tdsAmountTextBox.Text = Convert.ToString(dTdsAmount);
            double dNetAmount = 0;
            if (partyGstnTextBox.Text != "")
            {
                if ((long)gstTypeComboBox.SelectedValue == 1)
                    dNetAmount = dTaxableValue - dTdsAmount - dCgstAmount - dSgstAmount;
                else if ((long)gstTypeComboBox.SelectedValue == 2)
                    dNetAmount = dTaxableValue - dTdsAmount - dIgstAmount;
            }
            billAmtTextBox.Text = Convert.ToString(dNetAmount);
        }

        private void cgstAmtTextBox_TextChanged(object sender, EventArgs e)
        {
            double dCgstAmount = 0;
            bool bFlag = double.TryParse(cgstAmtTextBox.Text, out dCgstAmount);
            if (!bFlag)
            {
                cgstAmtTextBox.Text = "0";
                cgstAmtTextBox.SelectAll();
                return;
            }
            double dDiscountAmount = Convert.ToDouble(discPerAmtFinalTextBox.Text);

            double dAddLess = Convert.ToDouble(addLessAmtFinalTextBox.Text);

            double dGrossAmount = Convert.ToDouble(totalAmountTextBox.Text);
            double dTaxableValue = Convert.ToDouble(dGrossAmount - dDiscountAmount + dAddLess);
            
            sgstAmtTextBox.Text = Convert.ToString(dCgstAmount);
            
            double dSgstAmount = Convert.ToDouble(sgstAmtTextBox.Text);
            double dIgstAmount = Convert.ToDouble(igstAmtTextBox.Text);

            double dTdsAmount = Convert.ToDouble(tdsAmountTextBox.Text);
            //tdsAmountTextBox.Text = Convert.ToString(dTdsAmount);
            double dNetAmount = 0;
            if (partyGstnTextBox.Text != "")
            {
                if ((long)gstTypeComboBox.SelectedValue == 1)
                    dNetAmount = dTaxableValue - dTdsAmount - dCgstAmount - dSgstAmount;
                else if ((long)gstTypeComboBox.SelectedValue == 2)
                    dNetAmount = dTaxableValue - dTdsAmount - dIgstAmount;
            }
            billAmtTextBox.Text = Convert.ToString(dNetAmount);
        }

        private void sgstRateTextBox_TextChanged(object sender, EventArgs e)
        {
            double dSgstRate = 0;
            bool bFlag = double.TryParse(sgstRateTextBox.Text, out dSgstRate);
            if (!bFlag)
            {
                sgstRateTextBox.Text = "0";
                sgstRateTextBox.SelectAll();
                return;
            }
            double dDiscountAmount = Convert.ToDouble(discPerAmtFinalTextBox.Text);

            double dAddLess = Convert.ToDouble(addLessAmtFinalTextBox.Text);

            double dGrossAmount = Convert.ToDouble(totalAmountTextBox.Text);
            double dTaxableValue = Convert.ToDouble(dGrossAmount - dDiscountAmount + dAddLess);
            double dSgstAmount = (dSgstRate / 100) * dTaxableValue;
            sgstAmtTextBox.Text = Convert.ToString(dSgstAmount);
            cgstRateTextBox.Text = sgstRateTextBox.Text;

            cgstAmtTextBox.Text = Convert.ToString(dSgstAmount);

            double dCgstAmount = Convert.ToDouble(cgstAmtTextBox.Text);
            double dIgstAmount = Convert.ToDouble(igstAmtTextBox.Text);

            double dTdsAmount = Convert.ToDouble(tdsAmountTextBox.Text);
            //tdsAmountTextBox.Text = Convert.ToString(dTdsAmount);
            double dNetAmount = 0;
            if (partyGstnTextBox.Text != "")
            {
                if ((long)gstTypeComboBox.SelectedValue == 1)
                    dNetAmount = dTaxableValue - dTdsAmount - dCgstAmount - dSgstAmount;
                else if ((long)gstTypeComboBox.SelectedValue == 2)
                    dNetAmount = dTaxableValue - dTdsAmount - dIgstAmount;
            }
            billAmtTextBox.Text = Convert.ToString(dNetAmount);
        }

        private void sgstAmtTextBox_TextChanged(object sender, EventArgs e)
        {
            double dSgstAmount = 0;
            bool bFlag = double.TryParse(sgstAmtTextBox.Text, out dSgstAmount);
            if (!bFlag)
            {
                sgstAmtTextBox.Text = "0";
                sgstAmtTextBox.SelectAll();
                return;
            }
            double dDiscountAmount = Convert.ToDouble(discPerAmtFinalTextBox.Text);

            double dAddLess = Convert.ToDouble(addLessAmtFinalTextBox.Text);

            double dGrossAmount = Convert.ToDouble(totalAmountTextBox.Text);
            double dTaxableValue = Convert.ToDouble(dGrossAmount - dDiscountAmount + dAddLess);

            cgstAmtTextBox.Text = Convert.ToString(dSgstAmount);

            double dCgstAmount = Convert.ToDouble(cgstAmtTextBox.Text);
            double dIgstAmount = Convert.ToDouble(igstAmtTextBox.Text);

            double dTdsAmount = Convert.ToDouble(tdsAmountTextBox.Text);
            //tdsAmountTextBox.Text = Convert.ToString(dTdsAmount);
            double dNetAmount = 0;
            if (partyGstnTextBox.Text != "")
            {
                if ((long)gstTypeComboBox.SelectedValue == 1)
                    dNetAmount = dTaxableValue - dTdsAmount - dCgstAmount - dSgstAmount;
                else if ((long)gstTypeComboBox.SelectedValue == 2)
                    dNetAmount = dTaxableValue - dTdsAmount - dIgstAmount;
            }
            billAmtTextBox.Text = Convert.ToString(dNetAmount);
        }

        private void igstRateTextBox_TextChanged(object sender, EventArgs e)
        {
            double dIgstRate = 0;
            bool bFlag = double.TryParse(igstRateTextBox.Text, out dIgstRate);
            if (!bFlag)
            {
                igstRateTextBox.Text = "0";
                igstRateTextBox.SelectAll();
                return;
            }
            double dDiscountAmount = Convert.ToDouble(discPerAmtFinalTextBox.Text);

            double dAddLess = Convert.ToDouble(addLessAmtFinalTextBox.Text);

            double dGrossAmount = Convert.ToDouble(totalAmountTextBox.Text);
            double dTaxableValue = Convert.ToDouble(dGrossAmount - dDiscountAmount + dAddLess);

            double dSgstAmount = Convert.ToDouble(sgstAmtTextBox.Text);
            
            double dCgstAmount = Convert.ToDouble(cgstAmtTextBox.Text);
            double dIgstAmount = (dIgstRate / 100) * dTaxableValue;

            double dTdsAmount = Convert.ToDouble(tdsAmountTextBox.Text);
            //tdsAmountTextBox.Text = Convert.ToString(dTdsAmount);
            double dNetAmount = 0;
            if (partyGstnTextBox.Text != "")
            {
                if ((long)gstTypeComboBox.SelectedValue == 1)
                    dNetAmount = dTaxableValue - dTdsAmount - dCgstAmount - dSgstAmount;
                else if ((long)gstTypeComboBox.SelectedValue == 2)
                    dNetAmount = dTaxableValue - dTdsAmount - dIgstAmount;
            }
            billAmtTextBox.Text = Convert.ToString(dNetAmount);
        }

        private void igstAmtTextBox_TextChanged(object sender, EventArgs e)
        {
            double dIgstAmount = 0;
            bool bFlag = double.TryParse(igstAmtTextBox.Text, out dIgstAmount);
            if (!bFlag)
            {
                igstAmtTextBox.Text = "0";
                igstAmtTextBox.SelectAll();
                return;
            }
            double dDiscountAmount = Convert.ToDouble(discPerAmtFinalTextBox.Text);

            double dAddLess = Convert.ToDouble(addLessAmtFinalTextBox.Text);

            double dGrossAmount = Convert.ToDouble(totalAmountTextBox.Text);
            double dTaxableValue = Convert.ToDouble(dGrossAmount - dDiscountAmount + dAddLess);

            double dSgstAmount = Convert.ToDouble(sgstAmtTextBox.Text);

            double dCgstAmount = Convert.ToDouble(cgstAmtTextBox.Text);
            //double dIgstAmount = (dIgstRate / 100) * dTaxableValue;

            double dTdsAmount = Convert.ToDouble(tdsAmountTextBox.Text);
            //tdsAmountTextBox.Text = Convert.ToString(dTdsAmount);
            double dNetAmount = 0;
            if (partyGstnTextBox.Text != "")
            {
                if ((long)gstTypeComboBox.SelectedValue == 1)
                    dNetAmount = dTaxableValue - dTdsAmount - dCgstAmount - dSgstAmount;
                else if ((long)gstTypeComboBox.SelectedValue == 2)
                    dNetAmount = dTaxableValue - dTdsAmount - dIgstAmount;
            }
            billAmtTextBox.Text = Convert.ToString(dNetAmount);
        }

        private void totalAmountTextBox_TextChanged(object sender, EventArgs e)
        {
            
            double dDiscountAmount = Convert.ToDouble(discPerAmtTextBox.Text) * Convert.ToDouble(discPerTextBox.Text)/100;
            double dAddLess = Convert.ToDouble(addLessAmtFinalTextBox.Text);

            double dGrossAmount = Convert.ToDouble(totalAmountTextBox.Text);
            double dTaxableValue = Convert.ToDouble(dGrossAmount - dDiscountAmount + dAddLess);
            taxValTextBox.Text = Convert.ToString(dTaxableValue);
            double dSgstAmount = Convert.ToDouble(sgstRateTextBox.Text)/100 * dTaxableValue;
            sgstAmtTextBox.Text = Convert.ToString(dSgstAmount); 
            double dCgstAmount = Convert.ToDouble(cgstRateTextBox.Text)/100 * dTaxableValue;
            cgstAmtTextBox.Text = Convert.ToString(dCgstAmount);
            double dIgstAmount = Convert.ToDouble(igstRateTextBox.Text)/100 * dTaxableValue;

            double dTdsAmount = Convert.ToDouble(tdsAmountTextBox.Text);
            //tdsAmountTextBox.Text = Convert.ToString(dTdsAmount);
            double dNetAmount = 0;
            if (partyGstnTextBox.Text != "")
            {
                if ((long)gstTypeComboBox.SelectedValue == 1)
                    dNetAmount = dTaxableValue - dTdsAmount + dCgstAmount + dSgstAmount;
                else if ((long)gstTypeComboBox.SelectedValue == 2)
                    dNetAmount = dTaxableValue - dTdsAmount + dIgstAmount;
            }
            billAmtTextBox.Text = Convert.ToString(Math.Round(dNetAmount));
        }

        private void jobChallanReceiveGridView_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (jobChallanReceiveGridView.CurrentCell.ColumnIndex == refNoChallanReceiveComboBox.Index && e.Control is ComboBox)
            {
                ComboBox comboBox = e.Control as ComboBox;
                //comboBox.SelectedIndex = -1;
                comboBox.SelectedIndexChanged -= refNoReceiveComboSelectionChanged;
                comboBox.SelectedIndexChanged += refNoReceiveComboSelectionChanged;

            }
        }

        private void refNoReceiveComboSelectionChanged(object sender, EventArgs e)
        {
            string strChallanNo = (string)jobChallanReceiveGridView.Rows[jobChallanReceiveGridView.CurrentRow.Index].Cells[refNoChallanReceiveComboBox.Index].EditedFormattedValue.ToString();


            if (refNoChallanReceiveComboBox.Items != null)
            {
                if (strChallanNo == "")
                {
                    //MessageBox.Show("Please Select Challan No", "ERROR", MessageBoxButtons.OK);
                    return;
                }
                //jobprocess_details selectedChallan = (jobprocess_details)jobReceiptDataGridView.Rows[jobReceiptDataGridView.CurrentRow.Index].Cells[refReceiptComboBox.Index].Value;
                //List<jobprocess_itemdetails> jobWorkItemList = selectedChallan.jobprocess_itemdetails.ToList();//partyDB.getJobWorkPendingChallanItemList(Convert.ToInt64(strChallanNo));
                List<jobprocess_itemdetails> jobWorkItemList = partyDB.getJobWorkPendingChallanItemList(strChallanNo);
                if (jobWorkItemList == null)
                {
                    MessageBox.Show("No Pending Item in Challan", "ERROR", MessageBoxButtons.OK);
                    return;
                }

                var itemList = DB.getItemList();
                List<item_details> SelectedChallanItemList = new List<item_details>(); ;


                if (itemList != null)
                {
                    for (int i = 0; i < jobWorkItemList.Count; i++)
                    {
                        item_details itemDetail = itemList.Where(x => x.itemid == jobWorkItemList[i].item_id).SingleOrDefault();
                        if (!SelectedChallanItemList.Contains(itemDetail))
                            SelectedChallanItemList.Add(itemDetail);
                    }


                    DataGridViewComboBoxCell cell = (DataGridViewComboBoxCell)jobChallanReceiveGridView.Rows[jobChallanReceiveGridView.CurrentRow.Index].Cells[itemNameReceivveComboBox.Index];
                    cell.DataSource = SelectedChallanItemList;
                    cell.DisplayMember = "itemname";
                    cell.ValueMember = "itemid";

                }
                long lJobProcssDispatchItemId = 0;
                int iJobProcssDispatchItemSelectedIndex = -1;
                try
                {
                    lJobProcssDispatchItemId = Convert.ToInt64(((ComboBox)sender).SelectedValue);
                    iJobProcssDispatchItemSelectedIndex = Convert.ToInt32(((ComboBox)sender).SelectedIndex);
                }
                catch (Exception ex)
                {
                    return;
                }
                if(lSelectedObjects.Count == (jobChallanReceiveGridView.CurrentRow.Index + 1))
                {
                    lSelectedObjects[jobChallanReceiveGridView.CurrentRow.Index] = lJobProcssDispatchItemId;
                    iSelectedObjectsIndex[jobChallanReceiveGridView.CurrentRow.Index] = iJobProcssDispatchItemSelectedIndex;
                }
                else
                {
                    lSelectedObjects.Add(lJobProcssDispatchItemId);
                    iSelectedObjectsIndex.Add(iJobProcssDispatchItemSelectedIndex);
                }
                


                jobprocess_itemdetails itemDetails = partyDB.getJobWorkDispatchChallanItemList(lJobProcssDispatchItemId);
                if (itemDetails != null)
                {
                    jobChallanReceiveGridView.Rows[jobChallanReceiveGridView.CurrentRow.Index].Cells[jobTypeReceiveTextBox.Index].Value = itemDetails.jobTypeDetails;
                    jobChallanReceiveGridView.Rows[jobChallanReceiveGridView.CurrentRow.Index].Cells[itemNameReceivveComboBox.Index].Value = itemDetails.item_id;
                    jobChallanReceiveGridView.Rows[jobChallanReceiveGridView.CurrentRow.Index].Cells[hsnCodeReceiveTextBox.Index].Value = itemDetails.hsnCode;

                    jobChallanReceiveGridView.Rows[jobChallanReceiveGridView.CurrentRow.Index].Cells[unitReceiveComboBox.Index].Value = itemDetails.unit;
                    jobChallanReceiveGridView.Rows[jobChallanReceiveGridView.CurrentRow.Index].Cells[cutReceiveTextBox.Index].Value = itemDetails.cut;
                    jobChallanReceiveGridView.Rows[jobChallanReceiveGridView.CurrentRow.Index].Cells[rateReceiveTextBox.Index].Value = itemDetails.rate;


                    if (multiColumnItemDisplayDataList.Count > 0)
                    {
                        jobChallanReceiveGridView.Rows[jobChallanReceiveGridView.CurrentRow.Index].Cells[pcsReceiveTextBox.Index].Value = multiColumnItemDisplayDataList[iJobProcssDispatchItemSelectedIndex].available_pcs;

                        jobChallanReceiveGridView.Rows[jobChallanReceiveGridView.CurrentRow.Index].Cells[qunatityReceiveTextBox.Index].Value = multiColumnItemDisplayDataList[iJobProcssDispatchItemSelectedIndex].available_mtrsQty;
                        
                    }


                    /*if (multiColumnItemDisplayDataList != null)
                    {
                        JobWorkReceiptItemForMultiColumnDisplay avialableChallanData = multiColumnItemDisplayDataList.Where(x => x.jobProcessItemDetails_id == lJobProcssDispatchItemId).SingleOrDefault();
                        if (avialableChallanData != null)
                        {
                            jobChallanReceiveGridView.Rows[jobChallanReceiveGridView.CurrentRow.Index].Cells[pcsReceiveTextBox.Index].Value = avialableChallanData.available_pcs;
                            jobChallanReceiveGridView.Rows[jobChallanReceiveGridView.CurrentRow.Index].Cells[qunatityReceiveTextBox.Index].Value = avialableChallanData.available_mtrsQty;
                        }

                    }*/
                    if (itemDetails.unit == "PCS")
                    {
                        jobChallanReceiveGridView.Rows[jobChallanReceiveGridView.CurrentRow.Index].Cells[amountReceiveTextBox.Index].Value = Convert.ToInt64(jobChallanReceiveGridView.Rows[jobChallanReceiveGridView.CurrentRow.Index].Cells[pcsReceiveTextBox.Index].Value) * itemDetails.rate;
                        jobChallanReceiveGridView.Rows[jobChallanReceiveGridView.CurrentRow.Index].Cells[freshReceiveTextBox.Index].Value = jobChallanReceiveGridView.Rows[jobChallanReceiveGridView.CurrentRow.Index].Cells[pcsReceiveTextBox.Index].Value;
                    }
                    else
                    {
                        jobChallanReceiveGridView.Rows[jobChallanReceiveGridView.CurrentRow.Index].Cells[amountReceiveTextBox.Index].Value = Convert.ToInt64(jobChallanReceiveGridView.Rows[jobChallanReceiveGridView.CurrentRow.Index].Cells[qunatityReceiveTextBox.Index].Value) * itemDetails.rate;
                        jobChallanReceiveGridView.Rows[jobChallanReceiveGridView.CurrentRow.Index].Cells[freshReceiveTextBox.Index].Value = jobChallanReceiveGridView.Rows[jobChallanReceiveGridView.CurrentRow.Index].Cells[pcsReceiveTextBox.Index].Value;
                    }

                }
            }

        }

        private void jobChallanReceiveGridView_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (jobChallanReceiveGridView.Rows.Count > 0 && (e.RowIndex <= jobChallanReceiveGridView.Rows.Count - 1) && jobChallanReceiveGridView.Rows[e.RowIndex].ReadOnly == false)
            {
                if (e.ColumnIndex == refNoChallanReceiveComboBox.Index)
                {

                    if ((int)jobChallanReceiveGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].EditedFormattedValue.ToString().Length <= 0)
                    {

                        /*if (refReceiptComboBox.Items.Count != 0)
                        {
                            MessageBox.Show("Please select the correct item", "INPUT ERROR", MessageBoxButtons.OK);
                            //saleDataGridView.CurrentCell = saleDataGridView[e.RowIndex, e.ColumnIndex];
                            jobReceiptDataGridView.CurrentCell.Selected = true;
                            e.Cancel = true;
                        }*/

                    }
                    if (jobChallanReceiveGridView.Rows.Count > e.RowIndex + 1)
                        jobChallanReceiveGridView.Rows[e.RowIndex + 1].ReadOnly = true;
                }
                else if (e.ColumnIndex == pcsReceiveTextBox.Index)
                {
                    long lPcs = 0;
                    long.TryParse(jobChallanReceiveGridView.Rows[e.RowIndex].Cells[freshReceiveTextBox.Index].EditedFormattedValue.ToString(), out lPcs);
                    double dCut = 0;
                    double.TryParse(jobChallanReceiveGridView.Rows[e.RowIndex].Cells[cutReceiveTextBox.Index].EditedFormattedValue.ToString(), out dCut);

                    long lActualPcs = 0;
                    long.TryParse(jobChallanReceiveGridView.Rows[e.RowIndex].Cells[pcsReceiveTextBox.Index].EditedFormattedValue.ToString(), out lActualPcs);
                    jobChallanReceiveGridView.Rows[e.RowIndex].Cells[qunatityReceiveTextBox.Index].Value = lActualPcs * dCut;


                    //jobReceiptDataGridView.Rows[e.RowIndex].Cells[qtyReceiptTextBox.Index].Value = dCut * lPcs;
                    double dRate = 0;
                    double.TryParse(jobChallanReceiveGridView.Rows[e.RowIndex].Cells[rateReceiveTextBox.Index].EditedFormattedValue.ToString(), out dRate);
                    if (jobChallanReceiveGridView.Rows[e.RowIndex].Cells[unitReceiveComboBox.Index].EditedFormattedValue.ToString() == "MTRS")
                        jobChallanReceiveGridView.Rows[e.RowIndex].Cells[amountReceiveTextBox.Index].Value = dCut * lPcs * dRate;
                    else
                        jobChallanReceiveGridView.Rows[e.RowIndex].Cells[amountReceiveTextBox.Index].Value = lPcs * dRate;
                    double dTotalAmount = 0, dTotalPcs = 0, dTotalMtrs = 0;
                    for (int i = 0; i < jobChallanReceiveGridView.Rows.Count - 1; i++)
                    {
                        double dAmount = 0, dMtrs = 0, dPcs = 0;
                        double.TryParse(jobChallanReceiveGridView.Rows[i].Cells[amountReceiveTextBox.Index].EditedFormattedValue.ToString(), out dAmount);
                        dTotalAmount = dTotalAmount + dAmount;
                        double.TryParse(jobChallanReceiveGridView.Rows[i].Cells[qunatityReceiveTextBox.Index].EditedFormattedValue.ToString(), out dMtrs);
                        dTotalMtrs = dTotalMtrs + dMtrs;
                        double.TryParse(jobChallanReceiveGridView.Rows[i].Cells[pcsReceiveTextBox.Index].EditedFormattedValue.ToString(), out dPcs);
                        dTotalPcs = dTotalPcs + dPcs;
                    }
                    totalAmountTextBox.Text = Convert.ToString(Math.Round(dTotalAmount));
                    totalPcsTextBox.Text = Convert.ToString(dTotalPcs);
                    totalQtyTextBox.Text = Convert.ToString(dTotalMtrs);
                    billAmtTextBox.Text = totalAmountTextBox.Text;
                    //discPerAmtTextBox.Text = Convert.ToString(dTotalAmount);
                }
                else if (e.ColumnIndex == qunatityReceiveTextBox.Index)
                {
                    long lPcs = 0;
                    long.TryParse(jobChallanReceiveGridView.Rows[e.RowIndex].Cells[freshReceiveTextBox.Index].EditedFormattedValue.ToString(), out lPcs);
                    double dCut = 0;
                    double.TryParse(jobChallanReceiveGridView.Rows[e.RowIndex].Cells[cutReceiveTextBox.Index].EditedFormattedValue.ToString(), out dCut);
                    double dQuantity = 0;

                    double.TryParse(jobChallanReceiveGridView.Rows[e.RowIndex].Cells[qunatityReceiveTextBox.Index].EditedFormattedValue.ToString(), out dQuantity);

                    double dRate = 0;
                    double.TryParse(jobChallanReceiveGridView.Rows[e.RowIndex].Cells[rateReceiveTextBox.Index].EditedFormattedValue.ToString(), out dRate);
                    if (jobChallanReceiveGridView.Rows[e.RowIndex].Cells[unitReceiveComboBox.Index].EditedFormattedValue.ToString() == "MTRS")
                        jobChallanReceiveGridView.Rows[e.RowIndex].Cells[amountReceiveTextBox.Index].Value = dQuantity * dRate;
                    else
                        jobChallanReceiveGridView.Rows[e.RowIndex].Cells[amountReceiveTextBox.Index].Value = lPcs * dRate;
                    double dTotalAmount = 0, dTotalPcs = 0, dTotalMtrs = 0;
                    for (int i = 0; i < jobChallanReceiveGridView.Rows.Count - 1; i++)
                    {
                        double dAmount = 0, dMtrs = 0, dPcs = 0;
                        double.TryParse(jobChallanReceiveGridView.Rows[i].Cells[amountReceiveTextBox.Index].EditedFormattedValue.ToString(), out dAmount);
                        dTotalAmount = dTotalAmount + dAmount;
                        double.TryParse(jobChallanReceiveGridView.Rows[i].Cells[qunatityReceiveTextBox.Index].EditedFormattedValue.ToString(), out dMtrs);
                        dTotalMtrs = dTotalMtrs + dMtrs;
                        double.TryParse(jobChallanReceiveGridView.Rows[i].Cells[pcsReceiveTextBox.Index].EditedFormattedValue.ToString(), out dPcs);
                        dTotalPcs = dTotalPcs + dPcs;
                    }
                    totalAmountTextBox.Text = Convert.ToString(Math.Round(dTotalAmount));
                    totalPcsTextBox.Text = Convert.ToString(dTotalPcs);
                    totalQtyTextBox.Text = Convert.ToString(dTotalMtrs);
                    billAmtTextBox.Text = totalAmountTextBox.Text;
                    //discPerAmtTextBox.Text = Convert.ToString(dTotalAmount);
                }
                else if (e.ColumnIndex == cutReceiveTextBox.Index)
                {
                    long lPcs = 0;
                    long.TryParse(jobChallanReceiveGridView.Rows[e.RowIndex].Cells[freshReceiveTextBox.Index].EditedFormattedValue.ToString(), out lPcs);
                    double dCut = 0;
                    double.TryParse(jobChallanReceiveGridView.Rows[e.RowIndex].Cells[cutReceiveTextBox.Index].EditedFormattedValue.ToString(), out dCut);
                    double dQuantity = 0;
                    double.TryParse(jobChallanReceiveGridView.Rows[e.RowIndex].Cells[qunatityReceiveTextBox.Index].EditedFormattedValue.ToString(), out dQuantity);

                    double dRate = 0;
                    double.TryParse(jobChallanReceiveGridView.Rows[e.RowIndex].Cells[rateReceiveTextBox.Index].EditedFormattedValue.ToString(), out dRate);
                    if (jobChallanReceiveGridView.Rows[e.RowIndex].Cells[unitReceiveComboBox.Index].EditedFormattedValue.ToString() == "MTRS")
                        jobChallanReceiveGridView.Rows[e.RowIndex].Cells[amountReceiveTextBox.Index].Value = dQuantity * dRate;
                    else
                        jobChallanReceiveGridView.Rows[e.RowIndex].Cells[amountReceiveTextBox.Index].Value = lPcs * dRate;
                    double dTotalAmount = 0, dTotalPcs = 0, dTotalMtrs = 0;
                    for (int i = 0; i < jobChallanReceiveGridView.Rows.Count - 1; i++)
                    {
                        double dAmount = 0, dMtrs = 0, dPcs = 0;
                        double.TryParse(jobChallanReceiveGridView.Rows[i].Cells[amountReceiveTextBox.Index].EditedFormattedValue.ToString(), out dAmount);
                        dTotalAmount = dTotalAmount + dAmount;
                        double.TryParse(jobChallanReceiveGridView.Rows[i].Cells[qunatityReceiveTextBox.Index].EditedFormattedValue.ToString(), out dMtrs);
                        dTotalMtrs = dTotalMtrs + dMtrs;
                        double.TryParse(jobChallanReceiveGridView.Rows[i].Cells[pcsReceiveTextBox.Index].EditedFormattedValue.ToString(), out dPcs);
                        dTotalPcs = dTotalPcs + dPcs;
                    }
                    totalAmountTextBox.Text = Convert.ToString(Math.Round(dTotalAmount));
                    totalPcsTextBox.Text = Convert.ToString(dTotalPcs);
                    totalQtyTextBox.Text = Convert.ToString(dTotalMtrs);
                    billAmtTextBox.Text = totalAmountTextBox.Text;
                    //discPerAmtTextBox.Text = Convert.ToString(dTotalAmount);
                }
                else if (e.ColumnIndex == rateReceiveTextBox.Index)
                {
                    long lPcs = 0;
                    long.TryParse(jobChallanReceiveGridView.Rows[e.RowIndex].Cells[freshReceiveTextBox.Index].EditedFormattedValue.ToString(), out lPcs);
                    double dCut = 0;
                    double.TryParse(jobChallanReceiveGridView.Rows[e.RowIndex].Cells[cutReceiveTextBox.Index].EditedFormattedValue.ToString(), out dCut);
                    double dQuantity = 0;
                    double.TryParse(jobChallanReceiveGridView.Rows[e.RowIndex].Cells[qunatityReceiveTextBox.Index].EditedFormattedValue.ToString(), out dQuantity);

                    double dRate = 0;
                    double.TryParse(jobChallanReceiveGridView.Rows[e.RowIndex].Cells[rateReceiveTextBox.Index].EditedFormattedValue.ToString(), out dRate);
                    if (jobChallanReceiveGridView.Rows[e.RowIndex].Cells[unitReceiveComboBox.Index].EditedFormattedValue.ToString() == "MTRS")
                        jobChallanReceiveGridView.Rows[e.RowIndex].Cells[amountReceiveTextBox.Index].Value = dQuantity * dRate;
                    else
                        jobChallanReceiveGridView.Rows[e.RowIndex].Cells[amountReceiveTextBox.Index].Value = lPcs * dRate;
                    double dTotalAmount = 0, dTotalPcs = 0, dTotalMtrs = 0;
                    for (int i = 0; i < jobChallanReceiveGridView.Rows.Count - 1; i++)
                    {
                        double dAmount = 0, dMtrs = 0, dPcs = 0;
                        double.TryParse(jobChallanReceiveGridView.Rows[i].Cells[amountReceiveTextBox.Index].EditedFormattedValue.ToString(), out dAmount);
                        dTotalAmount = dTotalAmount + dAmount;
                        double.TryParse(jobChallanReceiveGridView.Rows[i].Cells[qunatityReceiveTextBox.Index].EditedFormattedValue.ToString(), out dMtrs);
                        dTotalMtrs = dTotalMtrs + dMtrs;
                        double.TryParse(jobChallanReceiveGridView.Rows[i].Cells[pcsReceiveTextBox.Index].EditedFormattedValue.ToString(), out dPcs);
                        dTotalPcs = dTotalPcs + dPcs;
                    }
                    totalAmountTextBox.Text = Convert.ToString(Math.Round(dTotalAmount));
                    totalPcsTextBox.Text = Convert.ToString(dTotalPcs);
                    totalQtyTextBox.Text = Convert.ToString(dTotalMtrs);
                    billAmtTextBox.Text = totalAmountTextBox.Text;
                    //discPerAmtTextBox.Text = Convert.ToString(dTotalAmount);
                }
                else if (e.ColumnIndex == freshReceiveTextBox.Index)
                {
                    bool bValidateItemSum = checkReceiveItemDetails(e.RowIndex);
                    if (!bValidateItemSum)
                    {
                        MessageBox.Show("Item details not matching with Total PCS/MTRS", "INPUT ERROR", MessageBoxButtons.OK);
                        jobChallanReceiveGridView.CurrentCell.Selected = true;
                        e.Cancel = true;

                    }

                    long lPcs = 0;
                    long.TryParse(jobChallanReceiveGridView.Rows[e.RowIndex].Cells[freshReceiveTextBox.Index].EditedFormattedValue.ToString(), out lPcs);
                    double dCut = 0;
                    double.TryParse(jobChallanReceiveGridView.Rows[e.RowIndex].Cells[cutReceiveTextBox.Index].EditedFormattedValue.ToString(), out dCut);
                    double dQuantity = 0;
                    double.TryParse(jobChallanReceiveGridView.Rows[e.RowIndex].Cells[qunatityReceiveTextBox.Index].EditedFormattedValue.ToString(), out dQuantity);

                    double dRate = 0;
                    double.TryParse(jobChallanReceiveGridView.Rows[e.RowIndex].Cells[rateReceiveTextBox.Index].EditedFormattedValue.ToString(), out dRate);
                    if (jobChallanReceiveGridView.Rows[e.RowIndex].Cells[unitReceiveComboBox.Index].EditedFormattedValue.ToString() == "MTRS")
                        jobChallanReceiveGridView.Rows[e.RowIndex].Cells[amountReceiveTextBox.Index].Value = dQuantity * dRate;
                    else
                        jobChallanReceiveGridView.Rows[e.RowIndex].Cells[amountReceiveTextBox.Index].Value = lPcs * dRate;
                    double dTotalAmount = 0, dTotalPcs = 0, dTotalMtrs = 0;
                    for (int i = 0; i < jobChallanReceiveGridView.Rows.Count - 1; i++)
                    {
                        double dAmount = 0, dMtrs = 0, dPcs = 0;
                        double.TryParse(jobChallanReceiveGridView.Rows[i].Cells[amountReceiveTextBox.Index].EditedFormattedValue.ToString(), out dAmount);
                        dTotalAmount = dTotalAmount + dAmount;
                        double.TryParse(jobChallanReceiveGridView.Rows[i].Cells[qunatityReceiveTextBox.Index].EditedFormattedValue.ToString(), out dMtrs);
                        dTotalMtrs = dTotalMtrs + dMtrs;
                        double.TryParse(jobChallanReceiveGridView.Rows[i].Cells[pcsReceiveTextBox.Index].EditedFormattedValue.ToString(), out dPcs);
                        dTotalPcs = dTotalPcs + dPcs;
                    }
                    totalAmountTextBox.Text = Convert.ToString(Math.Round(dTotalAmount));
                    totalPcsTextBox.Text = Convert.ToString(dTotalPcs);
                    totalQtyTextBox.Text = Convert.ToString(dTotalMtrs);
                    billAmtTextBox.Text = totalAmountTextBox.Text;
                    //discPerAmtTextBox.Text = Convert.ToString(dTotalAmount);
                }
                else if (e.ColumnIndex == bundlesReceiveTextBox.Index)
                {
                    string bundlesString = jobChallanReceiveGridView.Rows[e.RowIndex].Cells[bundlesReceiveTextBox.Index].EditedFormattedValue.ToString();
                    if (bundlesString != "")
                    {
                        DataTable dt = new DataTable();
                        try
                        {
                            int answer = (int)dt.Compute(bundlesString, "");
                            jobChallanReceiveGridView.Rows[e.RowIndex].Cells[pcsReceiveTextBox.Index].Value = answer;
                        }
                        catch (Exception ex)
                        {
                            jobChallanReceiveGridView.Rows[e.RowIndex].Cells[pcsReceiveTextBox.Index].Value = 0;
                        }
                    }
                    else
                        jobChallanReceiveGridView.Rows[e.RowIndex].Cells[pcsReceiveTextBox.Index].Value = 0;

                }

            }
        }

        private void jobChallanReceiveGridView_CellValidated(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == amountReceiveTextBox.Index)
            {
                if (e.RowIndex == (jobChallanReceiveGridView.Rows.Count - 2))
                {
                    DialogResult result = MessageBox.Show("Do You want to add new Item?", "Job Work Receive", MessageBoxButtons.YesNo);
                    if (result == DialogResult.Yes)
                    {
                        jobChallanReceiveGridView.CausesValidation = false;
                        jobChallanReceiveGridView.Rows[e.RowIndex + 1].ReadOnly = false;
                        if (iJobWorkMode == 2)
                            updateReceivedChallanPendingList(e.RowIndex + 1);
                        
                        //DataGridViewComboBoxCell cell = (DataGridViewComboBoxCell)jobDispatchDataGridView.Rows[e.RowIndex + 1].Cells[refNoComboBox.Index];
                        //updateLatestMillChallanList((long)partyNameComboBox.SelectedValue, e.RowIndex + 1);
                        return;
                    }
                    else
                    {
                        jobChallanReceiveGridView.CausesValidation = false;
                        remarkTextBox.Focus();
                    }
                }

            }
            
        }

        private void additionalJobChallanCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            jobDispatchDataGridView.Rows.Clear();
            lSelectedObjects.Clear();
            iSelectedObjectsIndex.Clear();
            updateChallanListForAdditionalJobWork(0);

        }
    }
}
