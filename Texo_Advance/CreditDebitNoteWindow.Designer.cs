﻿namespace Texo_Advance
{
    partial class CreditDebitNoteWindow
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CreditDebitNoteWindow));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.compantNameTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.typeComboBox = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.voucherNoTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.dateTextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.stateCodeComboBox = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.gstTypeComboBox = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.noteNoTextBox = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.accountComboBox = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.refBillNoComboBox = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.refBillDateTextBox = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.grossAmountTextBox = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.cgstPerTextBox = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.sgstPerTextBox = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.cgstAmtTextBox = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.sgstAmtTextBox = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.remarkTextBox = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.netAmountTextBox = new System.Windows.Forms.TextBox();
            this.addNewButton = new System.Windows.Forms.Button();
            this.viewButton = new System.Windows.Forms.Button();
            this.updateButton = new System.Windows.Forms.Button();
            this.deleteButton = new System.Windows.Forms.Button();
            this.closeButton = new System.Windows.Forms.Button();
            this.partyNameComboBox = new JTG.ColumnComboBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Fuchsia;
            this.label1.Location = new System.Drawing.Point(339, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(278, 22);
            this.label1.TabIndex = 0;
            this.label1.Text = "DEBIT / CREDIT NOTE WINDOW";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label2.Location = new System.Drawing.Point(38, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "COMPANY:";
            // 
            // compantNameTextBox
            // 
            this.compantNameTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.compantNameTextBox.Location = new System.Drawing.Point(148, 61);
            this.compantNameTextBox.Name = "compantNameTextBox";
            this.compantNameTextBox.ReadOnly = true;
            this.compantNameTextBox.Size = new System.Drawing.Size(220, 22);
            this.compantNameTextBox.TabIndex = 2;
            this.compantNameTextBox.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label3.Location = new System.Drawing.Point(379, 61);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 16);
            this.label3.TabIndex = 1;
            this.label3.Text = "TYPE:";
            // 
            // typeComboBox
            // 
            this.typeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.typeComboBox.DropDownWidth = 225;
            this.typeComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.typeComboBox.FormattingEnabled = true;
            this.typeComboBox.Location = new System.Drawing.Point(440, 58);
            this.typeComboBox.Name = "typeComboBox";
            this.typeComboBox.Size = new System.Drawing.Size(197, 24);
            this.typeComboBox.TabIndex = 0;
            this.typeComboBox.SelectedIndexChanged += new System.EventHandler(this.typeComboBox_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label4.Location = new System.Drawing.Point(640, 60);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(116, 16);
            this.label4.TabIndex = 1;
            this.label4.Text = "VOUCHER NO.:";
            // 
            // voucherNoTextBox
            // 
            this.voucherNoTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.voucherNoTextBox.Location = new System.Drawing.Point(756, 56);
            this.voucherNoTextBox.Name = "voucherNoTextBox";
            this.voucherNoTextBox.Size = new System.Drawing.Size(81, 22);
            this.voucherNoTextBox.TabIndex = 1;
            this.voucherNoTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.voucherNoTextBox_KeyDown);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label5.Location = new System.Drawing.Point(840, 58);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 16);
            this.label5.TabIndex = 1;
            this.label5.Text = "DATE:";
            // 
            // dateTextBox
            // 
            this.dateTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTextBox.Location = new System.Drawing.Point(899, 55);
            this.dateTextBox.Name = "dateTextBox";
            this.dateTextBox.Size = new System.Drawing.Size(103, 22);
            this.dateTextBox.TabIndex = 2;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label6.Location = new System.Drawing.Point(38, 104);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 16);
            this.label6.TabIndex = 1;
            this.label6.Text = "PARTY:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label7.Location = new System.Drawing.Point(380, 106);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(108, 16);
            this.label7.TabIndex = 1;
            this.label7.Text = "STATE CODE:";
            // 
            // stateCodeComboBox
            // 
            this.stateCodeComboBox.DropDownHeight = 125;
            this.stateCodeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.stateCodeComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stateCodeComboBox.FormattingEnabled = true;
            this.stateCodeComboBox.IntegralHeight = false;
            this.stateCodeComboBox.Location = new System.Drawing.Point(490, 103);
            this.stateCodeComboBox.MaxDropDownItems = 100;
            this.stateCodeComboBox.Name = "stateCodeComboBox";
            this.stateCodeComboBox.Size = new System.Drawing.Size(69, 24);
            this.stateCodeComboBox.TabIndex = 4;
            this.stateCodeComboBox.SelectedIndexChanged += new System.EventHandler(this.stateCodeComboBox_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label8.Location = new System.Drawing.Point(570, 104);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(87, 16);
            this.label8.TabIndex = 1;
            this.label8.Text = "GST TYPE:";
            // 
            // gstTypeComboBox
            // 
            this.gstTypeComboBox.DropDownHeight = 125;
            this.gstTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.gstTypeComboBox.DropDownWidth = 225;
            this.gstTypeComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gstTypeComboBox.FormattingEnabled = true;
            this.gstTypeComboBox.IntegralHeight = false;
            this.gstTypeComboBox.Location = new System.Drawing.Point(660, 101);
            this.gstTypeComboBox.Name = "gstTypeComboBox";
            this.gstTypeComboBox.Size = new System.Drawing.Size(197, 24);
            this.gstTypeComboBox.TabIndex = 5;
            this.gstTypeComboBox.SelectedIndexChanged += new System.EventHandler(this.gstTypeComboBox_SelectedIndexChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label9.Location = new System.Drawing.Point(860, 102);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(84, 16);
            this.label9.TabIndex = 1;
            this.label9.Text = "NOTE NO.:";
            // 
            // noteNoTextBox
            // 
            this.noteNoTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.noteNoTextBox.Location = new System.Drawing.Point(948, 98);
            this.noteNoTextBox.Name = "noteNoTextBox";
            this.noteNoTextBox.Size = new System.Drawing.Size(81, 22);
            this.noteNoTextBox.TabIndex = 6;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label10.Location = new System.Drawing.Point(38, 148);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(85, 16);
            this.label10.TabIndex = 1;
            this.label10.Text = "ACCOUNT:";
            // 
            // accountComboBox
            // 
            this.accountComboBox.DropDownHeight = 125;
            this.accountComboBox.DropDownWidth = 275;
            this.accountComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.accountComboBox.FormattingEnabled = true;
            this.accountComboBox.IntegralHeight = false;
            this.accountComboBox.Location = new System.Drawing.Point(148, 145);
            this.accountComboBox.Name = "accountComboBox";
            this.accountComboBox.Size = new System.Drawing.Size(220, 24);
            this.accountComboBox.TabIndex = 7;
            this.accountComboBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.accountComboBox_KeyDown);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label11.Location = new System.Drawing.Point(380, 146);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(106, 16);
            this.label11.TabIndex = 1;
            this.label11.Text = "REF BILL NO.:";
            // 
            // refBillNoComboBox
            // 
            this.refBillNoComboBox.DropDownHeight = 125;
            this.refBillNoComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.refBillNoComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.refBillNoComboBox.FormattingEnabled = true;
            this.refBillNoComboBox.IntegralHeight = false;
            this.refBillNoComboBox.Location = new System.Drawing.Point(490, 143);
            this.refBillNoComboBox.MaxDropDownItems = 100;
            this.refBillNoComboBox.Name = "refBillNoComboBox";
            this.refBillNoComboBox.Size = new System.Drawing.Size(82, 24);
            this.refBillNoComboBox.TabIndex = 8;
            this.refBillNoComboBox.SelectedIndexChanged += new System.EventHandler(this.refBillNoComboBox_SelectedIndexChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label12.Location = new System.Drawing.Point(594, 144);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(53, 16);
            this.label12.TabIndex = 1;
            this.label12.Text = "DATE:";
            // 
            // refBillDateTextBox
            // 
            this.refBillDateTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.refBillDateTextBox.Location = new System.Drawing.Point(653, 141);
            this.refBillDateTextBox.Name = "refBillDateTextBox";
            this.refBillDateTextBox.ReadOnly = true;
            this.refBillDateTextBox.Size = new System.Drawing.Size(103, 22);
            this.refBillDateTextBox.TabIndex = 9;
            this.refBillDateTextBox.TabStop = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label13.Location = new System.Drawing.Point(38, 192);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(134, 16);
            this.label13.TabIndex = 1;
            this.label13.Text = "GROSS AMOUNT:";
            // 
            // grossAmountTextBox
            // 
            this.grossAmountTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grossAmountTextBox.Location = new System.Drawing.Point(175, 189);
            this.grossAmountTextBox.Name = "grossAmountTextBox";
            this.grossAmountTextBox.Size = new System.Drawing.Size(103, 22);
            this.grossAmountTextBox.TabIndex = 10;
            this.grossAmountTextBox.TextChanged += new System.EventHandler(this.grossAmountTextBox_TextChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label14.Location = new System.Drawing.Point(287, 189);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(70, 16);
            this.label14.TabIndex = 1;
            this.label14.Text = "CGST %:";
            // 
            // cgstPerTextBox
            // 
            this.cgstPerTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cgstPerTextBox.Location = new System.Drawing.Point(356, 186);
            this.cgstPerTextBox.Name = "cgstPerTextBox";
            this.cgstPerTextBox.Size = new System.Drawing.Size(47, 22);
            this.cgstPerTextBox.TabIndex = 11;
            this.cgstPerTextBox.Text = "2.5";
            this.cgstPerTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.cgstPerTextBox.TextChanged += new System.EventHandler(this.cgstPerTextBox_TextChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label15.Location = new System.Drawing.Point(602, 186);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(70, 16);
            this.label15.TabIndex = 1;
            this.label15.Text = "SGST %:";
            // 
            // sgstPerTextBox
            // 
            this.sgstPerTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sgstPerTextBox.Location = new System.Drawing.Point(671, 183);
            this.sgstPerTextBox.Name = "sgstPerTextBox";
            this.sgstPerTextBox.Size = new System.Drawing.Size(47, 22);
            this.sgstPerTextBox.TabIndex = 13;
            this.sgstPerTextBox.Text = "2.5";
            this.sgstPerTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.sgstPerTextBox.TextChanged += new System.EventHandler(this.sgstPerTextBox_TextChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label16.Location = new System.Drawing.Point(413, 188);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(77, 16);
            this.label16.TabIndex = 1;
            this.label16.Text = "AMOUNT:";
            // 
            // cgstAmtTextBox
            // 
            this.cgstAmtTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cgstAmtTextBox.Location = new System.Drawing.Point(495, 185);
            this.cgstAmtTextBox.Name = "cgstAmtTextBox";
            this.cgstAmtTextBox.Size = new System.Drawing.Size(103, 22);
            this.cgstAmtTextBox.TabIndex = 12;
            this.cgstAmtTextBox.Text = "0";
            this.cgstAmtTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.cgstAmtTextBox.TextChanged += new System.EventHandler(this.cgstAmtTextBox_TextChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label17.Location = new System.Drawing.Point(730, 184);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(77, 16);
            this.label17.TabIndex = 1;
            this.label17.Text = "AMOUNT:";
            // 
            // sgstAmtTextBox
            // 
            this.sgstAmtTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sgstAmtTextBox.Location = new System.Drawing.Point(812, 181);
            this.sgstAmtTextBox.Name = "sgstAmtTextBox";
            this.sgstAmtTextBox.Size = new System.Drawing.Size(103, 22);
            this.sgstAmtTextBox.TabIndex = 14;
            this.sgstAmtTextBox.Text = "0";
            this.sgstAmtTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.sgstAmtTextBox.TextChanged += new System.EventHandler(this.sgstAmtTextBox_TextChanged);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label18.Location = new System.Drawing.Point(40, 232);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(75, 16);
            this.label18.TabIndex = 1;
            this.label18.Text = "REMARK:";
            // 
            // remarkTextBox
            // 
            this.remarkTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.remarkTextBox.Location = new System.Drawing.Point(119, 229);
            this.remarkTextBox.Name = "remarkTextBox";
            this.remarkTextBox.Size = new System.Drawing.Size(498, 22);
            this.remarkTextBox.TabIndex = 15;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Red;
            this.label19.Location = new System.Drawing.Point(762, 229);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(112, 16);
            this.label19.TabIndex = 1;
            this.label19.Text = "NET AMOUNT:";
            // 
            // netAmountTextBox
            // 
            this.netAmountTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.netAmountTextBox.Location = new System.Drawing.Point(881, 226);
            this.netAmountTextBox.Name = "netAmountTextBox";
            this.netAmountTextBox.ReadOnly = true;
            this.netAmountTextBox.Size = new System.Drawing.Size(109, 22);
            this.netAmountTextBox.TabIndex = 16;
            this.netAmountTextBox.Text = "0";
            this.netAmountTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // addNewButton
            // 
            this.addNewButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addNewButton.ForeColor = System.Drawing.Color.Red;
            this.addNewButton.Location = new System.Drawing.Point(175, 298);
            this.addNewButton.Name = "addNewButton";
            this.addNewButton.Size = new System.Drawing.Size(103, 41);
            this.addNewButton.TabIndex = 17;
            this.addNewButton.Text = "ADD NEW";
            this.addNewButton.UseVisualStyleBackColor = true;
            this.addNewButton.Click += new System.EventHandler(this.addNewButton_Click);
            // 
            // viewButton
            // 
            this.viewButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.viewButton.ForeColor = System.Drawing.Color.Red;
            this.viewButton.Location = new System.Drawing.Point(306, 298);
            this.viewButton.Name = "viewButton";
            this.viewButton.Size = new System.Drawing.Size(103, 41);
            this.viewButton.TabIndex = 18;
            this.viewButton.Text = "VIEW";
            this.viewButton.UseVisualStyleBackColor = true;
            this.viewButton.Click += new System.EventHandler(this.viewButton_Click);
            // 
            // updateButton
            // 
            this.updateButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.updateButton.ForeColor = System.Drawing.Color.Red;
            this.updateButton.Location = new System.Drawing.Point(431, 298);
            this.updateButton.Name = "updateButton";
            this.updateButton.Size = new System.Drawing.Size(103, 41);
            this.updateButton.TabIndex = 19;
            this.updateButton.Text = "UPDATE";
            this.updateButton.UseVisualStyleBackColor = true;
            this.updateButton.Click += new System.EventHandler(this.updateButton_Click);
            // 
            // deleteButton
            // 
            this.deleteButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deleteButton.ForeColor = System.Drawing.Color.Red;
            this.deleteButton.Location = new System.Drawing.Point(553, 298);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(103, 41);
            this.deleteButton.TabIndex = 20;
            this.deleteButton.Text = "DELETE";
            this.deleteButton.UseVisualStyleBackColor = true;
            this.deleteButton.Click += new System.EventHandler(this.deleteButton_Click);
            // 
            // closeButton
            // 
            this.closeButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.closeButton.ForeColor = System.Drawing.Color.Red;
            this.closeButton.Location = new System.Drawing.Point(679, 298);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(103, 41);
            this.closeButton.TabIndex = 21;
            this.closeButton.Text = "CLOSE";
            this.closeButton.UseVisualStyleBackColor = true;
            this.closeButton.Click += new System.EventHandler(this.closeButton_Click);
            // 
            // partyNameComboBox
            // 
            this.partyNameComboBox.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.partyNameComboBox.DropDownHeight = 125;
            this.partyNameComboBox.DropDownWidth = 17;
            this.partyNameComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.partyNameComboBox.FormattingEnabled = true;
            this.partyNameComboBox.IntegralHeight = false;
            this.partyNameComboBox.Location = new System.Drawing.Point(148, 104);
            this.partyNameComboBox.Name = "partyNameComboBox";
            this.partyNameComboBox.Size = new System.Drawing.Size(220, 23);
            this.partyNameComboBox.TabIndex = 3;
            this.partyNameComboBox.ViewColumn = 0;
            this.partyNameComboBox.SelectedIndexChanged += new System.EventHandler(this.partyNameComboBox_SelectedIndexChanged);
            // 
            // CreditDebitNoteWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.closeButton);
            this.Controls.Add(this.deleteButton);
            this.Controls.Add(this.updateButton);
            this.Controls.Add(this.viewButton);
            this.Controls.Add(this.addNewButton);
            this.Controls.Add(this.partyNameComboBox);
            this.Controls.Add(this.accountComboBox);
            this.Controls.Add(this.refBillNoComboBox);
            this.Controls.Add(this.stateCodeComboBox);
            this.Controls.Add(this.gstTypeComboBox);
            this.Controls.Add(this.typeComboBox);
            this.Controls.Add(this.refBillDateTextBox);
            this.Controls.Add(this.sgstPerTextBox);
            this.Controls.Add(this.cgstPerTextBox);
            this.Controls.Add(this.sgstAmtTextBox);
            this.Controls.Add(this.cgstAmtTextBox);
            this.Controls.Add(this.netAmountTextBox);
            this.Controls.Add(this.remarkTextBox);
            this.Controls.Add(this.grossAmountTextBox);
            this.Controls.Add(this.dateTextBox);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.noteNoTextBox);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.voucherNoTextBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.compantNameTextBox);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.Name = "CreditDebitNoteWindow";
            this.Size = new System.Drawing.Size(1049, 493);
            this.Load += new System.EventHandler(this.CreditDebitNoteWindow_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox compantNameTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox typeComboBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox voucherNoTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox dateTextBox;
        private JTG.ColumnComboBox partyNameComboBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox stateCodeComboBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox gstTypeComboBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox noteNoTextBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox accountComboBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox refBillNoComboBox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox refBillDateTextBox;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox grossAmountTextBox;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox cgstPerTextBox;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox sgstPerTextBox;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox cgstAmtTextBox;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox sgstAmtTextBox;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox remarkTextBox;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox netAmountTextBox;
        private System.Windows.Forms.Button addNewButton;
        private System.Windows.Forms.Button viewButton;
        private System.Windows.Forms.Button updateButton;
        private System.Windows.Forms.Button deleteButton;
        private System.Windows.Forms.Button closeButton;
    }
}
