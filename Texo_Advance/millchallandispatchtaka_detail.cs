//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Texo_Advance
{
    using System;
    using System.Collections.Generic;
    
    public partial class millchallandispatchtaka_detail
    {
        public string marka { get; set; }
        public string lotNo { get; set; }
        public long millTaka_id { get; set; }
        public string greyMtrs { get; set; }
        public Nullable<System.DateTime> version { get; set; }
        public string finMtrs { get; set; }
        public Nullable<bool> isReceived { get; set; }
        public Nullable<long> challan_id { get; set; }
        public bool isDeleted { get; set; }
        public Nullable<double> rate { get; set; }
        public Nullable<long> millvoucher_id { get; set; }
        public Nullable<double> jobAmount { get; set; }
        public Nullable<long> reProcessTaka { get; set; }
    
        public virtual milldispatchchallan_detail milldispatchchallan_detail { get; set; }
        public virtual millreceipt_detail millreceipt_detail { get; set; }
    }
}
