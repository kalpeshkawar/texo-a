﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Mail;
using System.IO;
using System.CodeDom;
using System.ComponentModel.DataAnnotations;
using System.Net.NetworkInformation;
using Texo_Advance.Printer;
using System.Drawing.Printing;
using Aspose.Pdf;

namespace Texo_Advance.Email
{
    class Email
    {
        MailMessage message;
        SmtpClient smtp;
        DGVPrinter print;
        string receiverMail;
        string senderMail;
        string password;
        string body;
        string fileName;
        string type;
        string mailProvider;
        public Email(DGVPrinter print, string receiverMail, string senderMail, string password, string body, string fileName, string type,string mailProvider)
        {

            this.print = print;
            this.receiverMail = receiverMail;
            this.senderMail = senderMail;
            this.password = password;
            this.body = body;
            this.fileName = fileName;
            this.type = type;
            this.mailProvider = mailProvider;

        }           

        
        public string sendEmail(string attachment, String Subject, String body, string receiverEmail, string senderEmail, string senderPassword, string smtpProvider)
        {
            try

            {
                message = new MailMessage();

                if (IsValidEmail(receiverEmail))

                {

                    message.To.Add(receiverEmail);

                }
                else
                    return "Not a valid Receiver Email id";

                message.Subject = Subject;

                if (IsValidEmail(senderEmail))

                {

                    message.To.Add(receiverEmail);

                }
                else
                    return "Not a valid sender Email id";

                message.From = new MailAddress(senderEmail);

                message.Body = body;

                if (attachment.Length > 0)

                {

                    if (System.IO.File.Exists(attachment))

                    {

                        message.Attachments.Add(new Attachment(attachment));

                    }

                }



                // set smtp details

                smtp = new SmtpClient(smtpProvider);

                smtp.Port = 587;

                smtp.EnableSsl = true;

                smtp.Credentials = new NetworkCredential(senderEmail, senderPassword);

                //smtp.Send(message, message.Subject);
                smtp.Send(message);

                //smtp.SendCompleted += new SendCompletedEventHandler(smtp_SendCompleted);

            }

            catch (Exception ex)

            {
                return ex.Message;

            }
            return "Success";
        }

        public static bool IsValidEmail(string text)
        {
            return new EmailAddressAttribute().IsValid(text);
        }

        public static  bool IsConnectedToInternet()
        {
            string host = "www.google.com";  
            bool result = false;
            Ping p = new Ping();
            try
            {
                PingReply reply = p.Send(host, 3000);
                if (reply.Status == IPStatus.Success)
                    return true;
            }
            catch(Exception ex) {
                string test=ex.Message;
            }
            return result;
        }

        public void threadMail()
        {
            PrintDocument doc = print.PrintDocu(print.dgv2);
           
            //doc.DocumentName = "Sending Email.....";
            string file = (DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds.ToString();
            Aspose.Pdf.PrintController pc = new Aspose.Pdf.PrintController();
            pc.FileName = fileName;
            doc.PrintController = pc;
            doc.Print();
            //PdfWriter writer = new PdfWriter(fileName);
            //PdfDocument pdf = new PdfDocument(writer);
            //Document document = new Document(pdf);
            //string text = doc.ToString();
            //Paragraph header = new Paragraph(doc.ToString());
            //document.Add(header);
            //document.Close();
            //string directory = Directory.GetCurrentDirectory();
            //string fileName = directory + "\\" + partyComboBox.Text + "_" + billNoTextBox.Text + ".pdf";
            //PrinterSettings printerSettings = new PrinterSettings();
            //printerSettings.PrinterName = "Microsoft Print to PDF";
            //printerSettings.PrintToFile = true;
            //printerSettings.PrintFileName = fileName;
            //doc.PrinterSettings = printerSettings;
            //doc.Print();   

            string message = sendEmail(fileName, type, body, receiverMail, senderMail, password, "smtp.gmail.com");
            if (message == "Success")
                Console.WriteLine(message);
            //MessageBox.Show("Email Sent successFully!!", "Success", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            else
                Console.WriteLine(message);
            //MessageBox.Show("Email Sent Failed!!", "Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //doc.Dispose();
        }
    }
}