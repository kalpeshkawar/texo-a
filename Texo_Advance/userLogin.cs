﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography.Pkcs;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Texo_Advance.DBItems;

namespace Texo_Advance
{
    public partial class userLogin : Form
    {
        public bool bCloseWindow = false;
        private string companyName = "";
        private string financialYear = "";
        private string companyGstn = "";
        private Form1 mainForm = null;
        MasterDBData DB = new MasterDBData();
        public userLogin(Form1 frm)
        {
            mainForm = frm;
            
            InitializeComponent();
            userdetail userdata = new userdetail();
        }

        private void userLogin_Load(object sender, EventArgs e)
        {
            companyNameComboBox.DataSource = DB.getCompanies();
            companyNameComboBox.DisplayMember = "companyname";
            companyNameComboBox.ValueMember = "companyId";
        }

        private void userLogin_FormClosing(object sender, FormClosingEventArgs e)
        {
           // DialogResult result = DialogResult.No;
            if (!bCloseWindow)
            {
                Application.Exit(e);
                //result = MessageBox.Show("Do you want to close the Application?", "Login", MessageBoxButtons.YesNo);
                if (e.Cancel == true)
                    this.Show();
                /*if (result == DialogResult.Yes)
                {
                    bCloseWindow = true;
                    Application.Exit();
                }
                else
                    e.Cancel = true;
*/
            }
        }


        private void loginButton_Click(object sender, EventArgs e)
        {
            string username = userNameTextBox.Text;
            string password = passwordTextBox.Text;

            if (password.Length != 0 && username.Length != 0)
            {
                using (var db = DBHelper2.GetDbContext(Properties.Settings.Default.MasterDatabse))
                {
                    //userdetail user1 = db.userdetails.Where(x => x.username == username).FirstOrDefault();
                    userdetail user1 = DB.getUserDetailByUserName(username);

                    if (user1 == null)
                    {
                        MessageBox.Show("User Name provided is not available", "Authentication fail", MessageBoxButtons.OK,MessageBoxIcon.Error);
                        userNameTextBox.Focus();
                        return;
                    }
                    if (password == user1.password)
                    {
                        this.bCloseWindow = true;
                        this.Close();
                        companyName = companyNameComboBox.Text;
                        financialYear = financialYearComboBox.Text;
                        Properties.Settings.Default.CompanyName = companyNameComboBox.Text;
                        Properties.Settings.Default.CompanyId = Convert.ToInt64(companyNameComboBox.SelectedValue);
                        Properties.Settings.Default.FinancialYearDbName = financialYearComboBox.SelectedValue.ToString();
                        Properties.Settings.Default.FinancialYear = financialYearComboBox.Text;
                        Properties.Settings.Default.FinancialYearId = DB.getFinancialYearID(Properties.Settings.Default.CompanyId, Properties.Settings.Default.FinancialYear);
                        company_details company = DB.getCompanyGstn(Properties.Settings.Default.CompanyId);
                        companyGstn = company.gstno;
                        Properties.Settings.Default.Address = company.address;
                        Properties.Settings.Default.bankdetails = company.bankdetails;
                        Properties.Settings.Default.bankno = company.bankno;
                        Properties.Settings.Default.city = company.city;
                        Properties.Settings.Default.mobile = company.mobile.ToString();
                        Properties.Settings.Default.phone = company.phoneno.ToString();
                        Properties.Settings.Default.pincode = (int)company.pincode;

                        mainForm.statusUpdate(companyName, financialYear, companyGstn);
                    }
                    else
                    {
                        MessageBox.Show("Please enter the correct password?", "Authentication fail", MessageBoxButtons.OK);
                        passwordTextBox.Focus();
                    }
                        
                }
                
            }
        }

        private void UpdateLabelStatusForMainWindow(Form1 frm,string strCompName,string strFinYear)
        {
            frm.statusUpdate(strCompName, strFinYear, companyGstn);
        }
        public string GetCompanyName()
        {
            return companyName;
        }
        public string GetFinancialYear()
        {
            return financialYear;
        }

        private void userLogin_KeyDown(object sender, KeyEventArgs e)
        {
                if (e.KeyCode == Keys.Enter)
                SendKeys.Send("{TAB}");
        }

        private void companyNameComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (companyNameComboBox.SelectedIndex != -1)
            {
                company_details companydetails = (company_details)companyNameComboBox.SelectedItem;
                List<financial_year> years= DB.getFinancialYearByCompany(companydetails.companyId);
                financialYearComboBox.DataSource = years;
                financialYearComboBox.DisplayMember = "financial_year1";
                financialYearComboBox.ValueMember = "db_name";
            }
        }
    }
}