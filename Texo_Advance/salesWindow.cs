﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;
using Texo_Advance.DBItems;
using System.Security.Policy;
using Texo_Advance.CustomControl;
using Texo_Advance.Printer;
using Texo_Advance.Properties;
using System.Drawing.Printing;
using System.IO;
using Texo_Advance.Email;
using System.Threading;

namespace Texo_Advance
{
    public partial class salesWindow : UserControl
    {
        private string companyName = "";
        private string companyGstn = "";
        MasterDBData DB = new MasterDBData();
        PartyDBData partyDB = new PartyDBData();
        sales_details saleBillDetail;
        List<party_details> partyDetailsList = new List<party_details>();
        List<broker_details> brokerList;
        List<transport_details> transportList;
        List<city_list> cityList;
        List<statecode_details> stateCodeList;
        List<gsttype_details> gstTypeList;
        List<item_details> itemList;
        public salesWindow(string name, string gstn)
        {
            companyName = name;
            companyGstn = gstn;
            //companyGstn = "08AKPPO7958A1Z5";
            InitializeComponent();
            //this.saleDataGridView.Columns["itemNameGridCombo"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.saleDataGridView.Columns["bundlesEditBoxGrid"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.saleDataGridView.Columns["hsncEditBoxGrid"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            //this.saleDataGridView.Columns["packingComboGrid"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            //this.saleDataGridView.Columns["unitComboGrid"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.saleDataGridView.Columns["pcsTextBoxGrid"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.saleDataGridView.Columns["cutEditBoxGrid"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.saleDataGridView.Columns["qtyEditBoxGid"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.saleDataGridView.Columns["rateTextBoxGrid"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.saleDataGridView.Columns["amountTextBoxGrid"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            //saleDataGridView.Rows.Add();
            label5.Show();
            orderNoTextBox.Show();
            refBillNoComboBox.Hide();
            label35.Hide();
        }
        private bool bFlagNewRow = false;

        private void saleDataGridView_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            
        }


        private void saleDataGridView_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void saleDataGridView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void addNewButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (billNoTextBox.Text.Length <= 0)
                {
                    MessageBox.Show("Please enter a valid Bill No.", "Bill Error", MessageBoxButtons.OK);
                    billNoTextBox.Focus();
                    billNoTextBox.SelectAll();
                    return;
                }

                bool bFlag = partyDB.checkBillDuplicacy(billNoTextBox.Text, Convert.ToInt64(salesTypeComboBox.SelectedValue));
                if (!bFlag)
                {
                    MessageBox.Show("Bill No. already Exists", "Bill Error", MessageBoxButtons.OK);
                    billNoTextBox.Focus();
                    billNoTextBox.SelectAll();
                    return;
                }
                sales_details salesDetails = new sales_details();
                salesDetails = getSalesDataFromControl(salesDetails);
                salesDetails.salesitem_details = getSalesItemDetails();
                bool bSalesBillAdded = partyDB.addSalesBill(salesDetails);
                if (bSalesBillAdded)
                {
                    MessageBox.Show("Sales Bill Added Successfully", "SALES", MessageBoxButtons.OK);
                    resetAllData();
                    setDafaultData();
                    billNoTextBox.SelectAll();
                }
                else
                    MessageBox.Show("Sales Bill NOT Added", "ERROR", MessageBoxButtons.OK);
            }
            catch(Exception ex)
            {
                MessageBox.Show("Sales Bill NOT Added", "ERROR", MessageBoxButtons.OK);
                return;
            }
            
        }

        private void saleDataGridView_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            try
            {
                if (e.RowIndex != (saleDataGridView.RowCount - 1))
                {
                    if (e.ColumnIndex == itemNameGridCombo.Index)
                    {
                        if (e.FormattedValue.ToString().Length == 0)
                        {
                            MessageBox.Show("Please select the correct item", "Item Name", MessageBoxButtons.OK);
                            //saleDataGridView.CurrentCell = saleDataGridView[e.RowIndex, e.ColumnIndex];
                            saleDataGridView.CurrentCell.Selected = true;
                            e.Cancel = true;
                        }

                    }
                    else if (e.ColumnIndex == hsncEditBoxGrid.Index)
                    {
                        if (e.FormattedValue.ToString().Length == 0)
                        {
                            DialogResult res = MessageBox.Show("Please enter the HSNCode. Leaving it empty may affect GST Reports.Do You want to continue?", "HSN CODE", MessageBoxButtons.YesNo);
                            if (res == DialogResult.No)
                            {
                                //saleDataGridView.CurrentCell = saleDataGridView[e.RowIndex, e.ColumnIndex];
                                saleDataGridView.CurrentCell.Selected = true;
                                e.Cancel = true;
                            }
                        }
                    }
                    else if (e.ColumnIndex == pcsTextBoxGrid.Index)
                    {
                        double dPcs = Convert.ToDouble(e.FormattedValue.ToString());
                        if (dPcs <= 0)
                        {
                            MessageBox.Show("Please enter correct PCS.", "PCS", MessageBoxButtons.OK);
                            //saleDataGridView.CurrentCell = saleDataGridView[e.RowIndex, e.ColumnIndex];
                            saleDataGridView.CurrentCell.Selected = true;
                            e.Cancel = true;
                        }
                        else
                        {
                            string str = saleDataGridView.Rows[e.RowIndex].Cells[unitComboGrid.Index].FormattedValue.ToString();
                            if (str == "PCS")
                            {
                                string strRate = saleDataGridView.Rows[e.RowIndex].Cells[rateTextBoxGrid.Index].FormattedValue.ToString();
                                if (strRate != "")
                                {
                                    double dRate = Convert.ToDouble(strRate);
                                    string strAmount = Convert.ToString(dPcs * dRate);
                                    saleDataGridView.Rows[e.RowIndex].Cells[amountTextBoxGrid.Index].Value = strAmount;
                                }
                            }
                        }
                    }
                    else if (e.ColumnIndex == cutEditBoxGrid.Index)
                    {
                        double dPcs = Convert.ToDouble(saleDataGridView.Rows[e.RowIndex].Cells[pcsTextBoxGrid.Index].FormattedValue.ToString());
                        if (dPcs <= 0)
                        {
                            MessageBox.Show("Please enter correct Cut", "CUT", MessageBoxButtons.OK);
                            //saleDataGridView.CurrentCell = saleDataGridView[e.RowIndex, e.ColumnIndex];
                            saleDataGridView.CurrentCell.Selected = true;
                            e.Cancel = true;
                        }
                        else
                        {
                            string str = saleDataGridView.Rows[e.RowIndex].Cells[unitComboGrid.Index].FormattedValue.ToString();
                            string strCut = e.FormattedValue.ToString();
                            double dCut;
                            if (strCut != "")
                                dCut = Convert.ToDouble(strCut);
                            else
                                dCut = 0;
                            double dMtrs = Convert.ToInt32(dPcs * dCut);
                            double dRate;
                            string strRate = saleDataGridView.Rows[e.RowIndex].Cells[rateTextBoxGrid.Index].FormattedValue.ToString();

                            if (strRate != "")
                                dRate = Convert.ToDouble(strRate);
                            else
                                dRate = 0;

                            saleDataGridView.Rows[e.RowIndex].Cells[qtyEditBoxGid.Index].Value = Convert.ToString(dMtrs);
                            if (str == "MTS")
                            {

                                //string strMtrs = saleDataGridView.Rows[e.RowIndex].Cells[qtyEditBoxGid.Index].FormattedValue.ToString();
                                if (strRate != "")
                                {
                                    string strAmount = Convert.ToString(dMtrs * dRate);
                                    saleDataGridView.Rows[e.RowIndex].Cells[amountTextBoxGrid.Index].Value = strAmount;
                                }
                                else
                                    saleDataGridView.Rows[e.RowIndex].Cells[amountTextBoxGrid.Index].Value = "0";
                            }
                            else
                                saleDataGridView.Rows[e.RowIndex].Cells[amountTextBoxGrid.Index].Value = Convert.ToString(dPcs * dRate);
                        }
                    }
                    else if (e.ColumnIndex == rateTextBoxGrid.Index)
                    {
                        double dRate = Convert.ToDouble(e.FormattedValue.ToString());
                        if (dRate <= 0)
                        {
                            MessageBox.Show("Please enter correct RATE", "RATE", MessageBoxButtons.OK);
                            //saleDataGridView.CurrentCell = saleDataGridView[e.RowIndex, e.ColumnIndex];
                            saleDataGridView.CurrentCell.Selected = true;
                            e.Cancel = true;
                        }
                        else
                        {
                            double dPcs = Convert.ToDouble(saleDataGridView.Rows[e.RowIndex].Cells[pcsTextBoxGrid.Index].FormattedValue.ToString());
                            string str = saleDataGridView.Rows[e.RowIndex].Cells[unitComboGrid.Index].FormattedValue.ToString();
                            string strCut = saleDataGridView.Rows[e.RowIndex].Cells[cutEditBoxGrid.Index].FormattedValue.ToString();
                            double dCut;
                            if (strCut != "")
                                dCut = Convert.ToDouble(strCut);
                            else
                                dCut = 0;
                            double dMtrs;
                            string strMtrs = saleDataGridView.Rows[e.RowIndex].Cells[qtyEditBoxGid.Index].FormattedValue.ToString();
                            if (strMtrs != "")
                                dMtrs = Convert.ToDouble(strMtrs);
                            else
                                dMtrs = Convert.ToDouble(dPcs * dCut);

                            if (str == "MTS")
                            {
                                //string strMtrs = saleDataGridView.Rows[e.RowIndex].Cells[qtyEditBoxGid.Index].FormattedValue.ToString();
                                string strAmount = Convert.ToString(dMtrs * dRate);
                                saleDataGridView.Rows[e.RowIndex].Cells[amountTextBoxGrid.Index].Value = strAmount;

                            }
                            else
                                saleDataGridView.Rows[e.RowIndex].Cells[amountTextBoxGrid.Index].Value = Convert.ToString(dPcs * dRate);
                        }
                    }
                    else if (e.ColumnIndex == unitComboGrid.Index)
                    {
                        string strPcs = saleDataGridView.Rows[e.RowIndex].Cells[pcsTextBoxGrid.Index].FormattedValue.ToString();
                        double dPcs;
                        if (strPcs != "")
                            dPcs = Convert.ToDouble(strPcs);
                        else
                            dPcs = 0;
                        string str = e.FormattedValue.ToString();
                        string strCut = saleDataGridView.Rows[e.RowIndex].Cells[cutEditBoxGrid.Index].FormattedValue.ToString();
                        double dCut;
                        if (strCut != "")
                            dCut = Convert.ToDouble(strCut);
                        else
                            dCut = 0;
                        double dMtrs = Convert.ToInt32(dPcs * dCut);
                        double dRate;
                        string strRate = saleDataGridView.Rows[e.RowIndex].Cells[rateTextBoxGrid.Index].FormattedValue.ToString();

                        if (strRate != "")
                            dRate = Convert.ToDouble(strRate);
                        else
                            dRate = 0;

                        saleDataGridView.Rows[e.RowIndex].Cells[qtyEditBoxGid.Index].Value = Convert.ToString(dMtrs);
                        if (str == "MTS")
                        {

                            //string strMtrs = saleDataGridView.Rows[e.RowIndex].Cells[qtyEditBoxGid.Index].FormattedValue.ToString();
                            if (strRate != "")
                            {
                                string strAmount = Convert.ToString(dMtrs * dRate);
                                saleDataGridView.Rows[e.RowIndex].Cells[amountTextBoxGrid.Index].Value = strAmount;
                            }
                            else
                                saleDataGridView.Rows[e.RowIndex].Cells[amountTextBoxGrid.Index].Value = "0";
                        }
                        else
                            saleDataGridView.Rows[e.RowIndex].Cells[amountTextBoxGrid.Index].Value = Convert.ToString(dPcs * dRate);
                    }
                    else if(e.ColumnIndex == bundlesEditBoxGrid.Index)
                    {
                        string bundlesString = saleDataGridView.Rows[e.RowIndex].Cells[bundlesEditBoxGrid.Index].EditedFormattedValue.ToString();
                        if(bundlesString != "")
                        {
                            DataTable dt = new DataTable();
                            try
                            {
                                int answer = (int)dt.Compute(bundlesString, "");
                                saleDataGridView.Rows[e.RowIndex].Cells[pcsTextBoxGrid.Index].Value = answer;
                            }
                            catch (Exception ex)
                            {
                                saleDataGridView.Rows[e.RowIndex].Cells[pcsTextBoxGrid.Index].Value = 0;
                            }
                        }
                        else
                            saleDataGridView.Rows[e.RowIndex].Cells[pcsTextBoxGrid.Index].Value = 0;

                    }
                }
            }
            catch(Exception ex)
            {
                return;
            }
            
        }

        private void openBillButton_Click(object sender, EventArgs e)
        {
            if (billNoTextBox.Text.Length <= 0)
            {
                MessageBox.Show("Please enter a valid Bill No.", "Bill Error", MessageBoxButtons.OK);
                billNoTextBox.Focus();
                return;
            }
            string billNo = billNoTextBox.Text;
            List<salesitem_details> saleItemDetails;
            try
            {
                var db = DBHelper2.GetDbContext(Properties.Settings.Default.FinancialYearDbName);
                long lAccountTypeID = Convert.ToInt64(salesTypeComboBox.SelectedValue);
                saleBillDetail = db.sales_details.Where(x => x.billno == billNo).Where(x => x.salesTypeid == lAccountTypeID).SingleOrDefault();
                //saleBillDetail = db.sales_details.Where(x => x.billno == lBillNo).SingleOrDefault();
                //salesDetails = getSalesDataFromControl(salesDetails);
                if (saleBillDetail == null)
                {
                    DialogResult result = MessageBox.Show("Bill No. does not Exists.Do you want to enter a new bill?", "Bill Error", MessageBoxButtons.YesNo);
                    if (result == DialogResult.Yes)
                    {
                        resetAllData();
                        setDafaultData();
                        billNoTextBox.SelectAll();
                        return;
                    }
                    billNoTextBox.Focus();
                    billNoTextBox.SelectAll();
                    return;
                }
                saleItemDetails = saleBillDetail.salesitem_details.ToList();

                db.Dispose();

            }
            catch (Exception ex)
            {
                return;
            }
            //saleBillDetail = partyDB.getSalesBillDetail(Convert.ToInt64(billNoTextBox.Text));

            vNoTextBox.Text = Convert.ToString(saleBillDetail.voucherNo);
            
            DateTime dt, dt1;
            bool bFlag = DateTime.TryParse(saleBillDetail.date.ToString(), out dt);
            if (bFlag)
            {
                dt1 = dt.Date;
                //DateTime.Parse(dt1.ToString()).ToString("dd-MM-yyyy");
                dateTextBox.Text = dt1.ToString("dd-MM-yyyy");
            }


            //dateTextBox.Text = saleBillDetail.date.ToString();
            //partyComboBox.SelectedValue = saleBillDetail.partyid;
            setPartyComboBoxSelectedValue(saleBillDetail.partyid);
            if ((long)salesTypeComboBox.SelectedValue == 5 || (long)salesTypeComboBox.SelectedValue == 6 || (long)salesTypeComboBox.SelectedValue == 7 || (long)salesTypeComboBox.SelectedValue == 8)
                refBillNoComboBox.Text = saleBillDetail.orderno;
            else
                orderNoTextBox.Text = saleBillDetail.orderno;

            stateComboBox.SelectedValue = saleBillDetail.statecode;
            gstTypeComboBox.SelectedValue = saleBillDetail.gstTypeid;
            stateComboBox.SelectedValue = saleBillDetail.station;
            brokerComboBox.SelectedValue = saleBillDetail.brokerid;
            transportComboBox.SelectedValue = saleBillDetail.transportid;
            lrnoTextBox.Text = saleBillDetail.lrno;
            remarkTextBox.Text = saleBillDetail.remark;
            discPerTextBox.Text = Convert.ToString(saleBillDetail.discountPercentage);
            discPerAmtTextBox.Text = Convert.ToString(saleBillDetail.discountOnAmount);
            discPerAmtFinalTextBox.Text = Convert.ToString(saleBillDetail.discount);
            addLessTextBox.Text = Convert.ToString(saleBillDetail.addLessDetail1);
            addLessAmtTextBox.Text = Convert.ToString(saleBillDetail.addLessDetail2);
            addLessAmtFinalTextBox.Text = Convert.ToString(saleBillDetail.additional);
            cgstRateTextBox.Text = Convert.ToString(saleBillDetail.cgstRate);
            cgstAmtTextBox.Text = Convert.ToString(saleBillDetail.cgst);
            sgstRateTextBox.Text = Convert.ToString(saleBillDetail.sgstRate);
            sgstAmtTextBox.Text = Convert.ToString(saleBillDetail.sgst);
            igstRateTextBox.Text = Convert.ToString(saleBillDetail.igstRate);
            igstAmtTextBox.Text = Convert.ToString(saleBillDetail.igst);
            caseNoTextBox.Text = saleBillDetail.caseno;
            caseNoTextBox2.Text = saleBillDetail.caseNoDetail2;
            taxValTextBox.Text = Convert.ToString(saleBillDetail.taxablevalue);
            billAmtTextBox.Text = Convert.ToString(saleBillDetail.billamount);
            totalAmountTextBox.Text = Convert.ToString(saleBillDetail.grossAmount);
            dueDaysTextBox.Text = Convert.ToString(saleBillDetail.duedays);

            //List<salesitem_details> saleItemDetails = partyDB.getItemDetailsOfSaleBill(Convert.ToInt64(billNoTextBox.Text));
            if (saleBillDetail.salesitem_details == null)
                return;
            else
                saleDataGridView.Rows.Clear();
            for (int i = 0; i < saleItemDetails.Count; i++)
            {
                saleDataGridView.Rows.Add();
                saleDataGridView.Rows[i].Cells[itemNameGridCombo.Index].Value = saleItemDetails[i].salesitem_id;
                saleDataGridView.Rows[i].Cells[bundlesEditBoxGrid.Index].Value = saleItemDetails[i].bundles;
                saleDataGridView.Rows[i].Cells[hsncEditBoxGrid.Index].Value = saleItemDetails[i].hsncode;
                saleDataGridView.Rows[i].Cells[packingComboGrid.Index].Value = saleItemDetails[i].packing;
                //saleDataGridView.Rows[i].Cells[packingComboGrid.Index].Value = saleItemDetails[i].packing;
                saleDataGridView.Rows[i].Cells[unitComboGrid.Index].Value = saleItemDetails[i].unit;
                saleDataGridView.Rows[i].Cells[pcsTextBoxGrid.Index].Value = Convert.ToString(saleItemDetails[i].pcs);
                saleDataGridView.Rows[i].Cells[cutEditBoxGrid.Index].Value = Convert.ToString(saleItemDetails[i].cut);
                saleDataGridView.Rows[i].Cells[qtyEditBoxGid.Index].Value = Convert.ToString(saleItemDetails[i].quantity);
                saleDataGridView.Rows[i].Cells[rateTextBoxGrid.Index].Value = Convert.ToString(saleItemDetails[i].rate);
                saleDataGridView.Rows[i].Cells[amountTextBoxGrid.Index].Value = Convert.ToString(saleItemDetails[i].amount);

            }
            button1.Enabled = true;
            deleteButton.Enabled = true;
            addNewButton.Enabled = false;
            billNoTextBox.SelectAll();
        }

        private void salesWindow_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (e.KeyChar == (char)Keys.Escape)
            {
                MessageBox.Show("Enter key pressed");
            }
        }



        private void salesWindow_Load(object sender, EventArgs e)
        {
            comNameTextBox.Text = companyName;
            fillSalesTypeList();
            fillBrokerDetails();
            fillTransportDetails();
            fillStateCodeList();
            fillGstTypeList();
            fillCityList();
            
            //UpdatePartyDetails();

            //List<party_details> partyList = db.party_details.Where(x => x.account_typeid == 0).ToList();
            /*List<party_details> partyList = DB.getPartyDetails(1);
            if(partyList != null)
            {
                partyComboBox.DisplayMember = "party_name";
                partyComboBox.ValueMember = "partyid";
                partyComboBox.DataSource = partyList;
                //partyComboBox.DataBindings.Add()
                    
            }*/
            UpdatePartyDetails();

        }

        private void saleDataGridView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == (int)Keys.Tab)
            {
                MessageBox.Show("Enter key pressed");
            }
        }

        private void saleDataGridView_CellValidated(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == amountTextBoxGrid.Index && e.RowIndex == (saleDataGridView.RowCount - 2))
            {
                DialogResult result = MessageBox.Show("Do You want to add new item?", "Sales", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    //saleDataGridView.CurrentCell = saleDataGridView.Rows[e.RowIndex + 1].Cells[0];
                    //saleDataGridView.Rows.Add();
                    //saleDataGridView.Rows[e.RowIndex + 1].Cells[0].Selected = true;
                    saleDataGridView.CausesValidation = false;
                    //bFlagNewRow = true;
                    return;
                }
                else
                {
                    bFlagNewRow = true;
                    //saleDataGridView.Rows.Remove(saleDataGridView.Rows[e.RowIndex+1]);
                    saleDataGridView.CausesValidation = false;
                    //ClearSelection();
                    remarkTextBox.Focus();
                    //return;
                }

                //   saleDataGridView.Rows.Add();
            }
        }

        private void saleDataGridView_Paint(object sender, PaintEventArgs e)
        {
            if (bFlagNewRow)
            {

                bFlagNewRow = false;
                //int index = saleDataGridView.Rows.Add();

                //saleDataGridView.Rows.Remove(saleDataGridView.Rows[saleDataGridView.RowCount-1]);
            }
        }
        public void setDafaultData()
        {

            using (var db = new Entities())
            {
                long maxVoucherNo = partyDB.getSalesVoucherNo(Convert.ToInt64(salesTypeComboBox.SelectedValue));
                long maxBillNo = Convert.ToInt64(partyDB.getSalesBillNo());
                if (maxVoucherNo != 0 && maxBillNo != 0)
                {
                    vNoTextBox.Text = Convert.ToString(maxVoucherNo + 1);
                    billNoTextBox.Text = Convert.ToString(maxVoucherNo + 1);
                }
                else
                {
                    vNoTextBox.Text = "1";
                    billNoTextBox.Text = "1";
                }
            }

            dateTextBox.Text = DateTime.Today.ToString("dd-MM-yyyy");
            caseNoTextBox.Text = billNoTextBox.Text;
            updateItemList();
            button1.Enabled = false;
            deleteButton.Enabled = false;
            addNewButton.Enabled = true;
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            AccountManagerForm acc = new AccountManagerForm();
            acc.setDefaultAccountType(1);
            acc.ShowDialog();
            fillBrokerDetails();
            fillCityList();
            fillTransportDetails();
            UpdatePartyDetails();

        }

        private void UpdatePartyDetails()
        {
            partyDetailsList = DB.getPartyDetails(1);
            /*if (partyList != null)
            {
                partyComboBox.DisplayMember = "party_name";
                partyComboBox.ValueMember = "partyid";
                partyComboBox.DataSource = partyList;
            }*/

            int iColumnCount = 5;


            DataTable myDataTable = new DataTable("ParentTable");
            // Declare variables for DataColumn and DataRow objects.
            DataColumn myDataColumn;
            DataRow myDataRow;

            //Add some coulumns
            for (int index = 0; index < iColumnCount; index++)
            {
                myDataColumn = new DataColumn();
                myDataColumn.DataType = typeof(string);
                if (index == 0)
                    myDataColumn.ColumnName = "NAME";
                else if (index == 1)
                    myDataColumn.ColumnName = "BROKER";
                else if (index == 2)
                    myDataColumn.ColumnName = "ADDRESS";
                else if (index == 3)
                    myDataColumn.ColumnName = "CITY";
                else if (index == 4)
                    myDataColumn.ColumnName = "PARTY_ID";
                //myDataColumn.Caption = "ParentItem";
                myDataColumn.AutoIncrement = false;
                myDataColumn.ReadOnly = false;
                myDataColumn.Unique = false;
                // Add the Column to the DataColumnCollection.
                myDataTable.Columns.Add(myDataColumn);
            }


            //Add some more rows
            for (int index = 0; index < partyDetailsList.Count; index++)
            {
                myDataRow = myDataTable.NewRow();
                myDataRow[0] = partyDetailsList[index].party_name;
                myDataRow[1] = partyDetailsList[index].transport_details.name; //DB.getBrokerNameFromID(Convert.ToInt64(partyDetailsList[index].brokerid));
                myDataRow[2] = partyDetailsList[index].address + "#" + partyDetailsList[index].address2;
                myDataRow[3] = partyDetailsList[index].city_list.name; // DB.getCityName((long)partyDetailsList[index].city_id);
                myDataRow[4] = partyDetailsList[index].partyid;
                myDataTable.Rows.Add(myDataRow);
            }

            //Now set the Data of the ColumnComboBox
            partyComboBox.DisplayMember = "NAME";
            partyComboBox.ValueMember = "PARTY_ID";
            partyComboBox.Data = myDataTable;
            //Set which row will be displayed in the text box
            //If you set this to a column that isn't displayed then the suggesting functionality won't work.
            partyComboBox.ColumnSpacing = 50;
            partyComboBox.ViewColumn = 0;

            partyComboBox.Columns[0].Width = 200;
            partyComboBox.Columns[1].Width = 150;
            partyComboBox.Columns[2].Width = 250;
            partyComboBox.Columns[3].Width = 50;
            //Set a few columns to not be shown
            partyComboBox.Columns[4].Display = false;
            partyComboBox.SelectedIndex = 0;
        }

        private void partyComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            fetchPartyDataFromPartyName();
            
            if ((long)salesTypeComboBox.SelectedValue == 5 || (long)salesTypeComboBox.SelectedValue == 6 || (long)salesTypeComboBox.SelectedValue == 7 || (long)salesTypeComboBox.SelectedValue == 8)
            {
                if(partyComboBox.SelectedIndex != -1)
                {
                    string partyID = partyComboBox["PARTY_ID"].ToString();
                    fillReferenceBillDetailsForReturn(Convert.ToInt64(partyID));
                }
                
            }
                
        }
        private void fetchPartyDataFromPartyName()
        {
            if (partyComboBox.SelectedIndex != -1)
            {
                
                //party_details partyDetail2 = (party_details)partyComboBox.SelectedItem;
                if (partyComboBox.Items.Count == 0)
                    return;
                string partyID = partyComboBox["PARTY_ID"].ToString();
                long lPartyID = Convert.ToInt64(partyID);
                //var partyDetail = DB.getPartyDetailFromID(1, Convert.ToInt64(partyID));
                var partyDetail = partyDetailsList.Where(x => x.partyid == lPartyID).SingleOrDefault();
                if (partyDetail == null)
                    return;
                transportComboBox.SelectedValue = partyDetail.transportid;
                brokerComboBox.SelectedValue = partyDetail.brokerid;
                stationComboBox.SelectedValue = partyDetail.city_id;
                gstnTextBox.Text = partyDetail.gst;
                string gstn = gstnTextBox.Text;
                if (gstn.Length > 0)
                {
                    string stateCode = gstn.Substring(0, 2);
                    int indexForStateCode = stateComboBox.FindString(stateCode);
                    if (indexForStateCode != -1)
                    {
                        stateComboBox.SelectedIndex = indexForStateCode;
                        stateComboBox.Enabled = false;
                        gstTypeComboBox.Enabled = false;

                    }

                }
                else
                {
                    stateComboBox.Enabled = true;
                    gstTypeComboBox.Enabled = true;
                }



            }
        }
        private void fillBrokerDetails()
        {

            brokerList = DB.getBrokerDetails();
            if (brokerList != null)
            {
                brokerComboBox.DisplayMember = "party_name";
                brokerComboBox.ValueMember = "brokerid";
                brokerComboBox.DataSource = brokerList;

            }

        }
        private void fillTransportDetails()
        {

            transportList = DB.getTransportDetails();
            if (transportList != null)
            {
                transportComboBox.DisplayMember = "name";
                transportComboBox.ValueMember = "transportid";
                transportComboBox.DataSource = transportList;
            }

        }
        private void fillStateCodeList()
        {
            stateCodeList = DB.getStateCodeList();
            if (stateCodeList != null)
            {
                stateComboBox.DisplayMember = "code";
                stateComboBox.ValueMember = "statecode_id";
                stateComboBox.DataSource = stateCodeList;
            }

        }
        private void fillGstTypeList()
        {
            gstTypeComboBox.SelectedIndex = -1;
            gstTypeList = DB.getGstTypeList();
            if (gstTypeList != null)
            {
                gstTypeComboBox.DisplayMember = "type";
                gstTypeComboBox.ValueMember = "gsttype_id";
                gstTypeComboBox.DataSource = gstTypeList;
            }

        }
        private void fillCityList()
        {

            cityList = DB.getCityList();
            if (cityList != null)
            {
                stationComboBox.DisplayMember = "name";
                stationComboBox.ValueMember = "city_id";
                stationComboBox.DataSource = cityList;
            }

        }

        private void itemLinkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            QualityInformationManagerForm qmf = new QualityInformationManagerForm();
            qmf.setMainScreenAddMode(false);
            qmf.ShowDialog();
            updateItemList();

        }
        private void updateItemList()
        {
            itemList = DB.getItemList();
            if (itemList != null)
            {
                itemNameGridCombo.DisplayMember = "itemname";
                itemNameGridCombo.ValueMember = "itemid";
                itemNameGridCombo.DataSource = itemList;
            }

        }

        private void saleDataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == amountTextBoxGrid.Index)
            {
                if (saleDataGridView.RowCount > 0)
                {
                    double dTotalAmount = 0;
                    for (int i = 0; i < (saleDataGridView.RowCount - 1); i++)
                    {
                        string strAmount = saleDataGridView.Rows[i].Cells[amountTextBoxGrid.Index].EditedFormattedValue.ToString();
                        if (strAmount != "")
                            dTotalAmount = dTotalAmount + Convert.ToDouble(strAmount);
                        else
                            dTotalAmount = dTotalAmount + 0;
                    }
                    totalAmountTextBox.Text = Convert.ToString(dTotalAmount);
                }
            }
            else if (e.ColumnIndex == pcsTextBoxGrid.Index)
            {
                if (saleDataGridView.RowCount > 0)
                {
                    double dTotalPcs = 0;
                    for (int i = 0; i < (saleDataGridView.RowCount - 1); i++)
                    {
                        string strPcs = saleDataGridView.Rows[i].Cells[pcsTextBoxGrid.Index].EditedFormattedValue.ToString();
                        if (strPcs != "")
                            dTotalPcs = dTotalPcs + Convert.ToDouble(strPcs);
                        else
                            dTotalPcs = dTotalPcs + 0;
                    }
                    totalPcsTextBox.Text = Convert.ToString(dTotalPcs);
                }
            }
            else if (e.ColumnIndex == qtyEditBoxGid.Index)
            {
                if (saleDataGridView.RowCount > 0)
                {
                    double dTotalMtrs = 0;
                    for (int i = 0; i < (saleDataGridView.RowCount - 1); i++)
                    {
                        string strMtrs = saleDataGridView.Rows[i].Cells[qtyEditBoxGid.Index].EditedFormattedValue.ToString();
                        if (strMtrs != "")
                            dTotalMtrs = dTotalMtrs + Convert.ToDouble(strMtrs);
                        else
                            dTotalMtrs = dTotalMtrs + 0;
                    }
                    totalQtyTextBox.Text = Convert.ToString(dTotalMtrs);
                }
            }
        }

        private void gstTypeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (gstTypeComboBox.SelectedIndex == 0)
            {
                cgstRateTextBox.Text = "0";
                cgstAmtTextBox.Enabled = false;
                sgstRateTextBox.Text = "0";
                sgstAmtTextBox.Enabled = false;
                cgstRateTextBox.Enabled = false;
                sgstRateTextBox.Enabled = false;
                igstRateTextBox.Text = "5";
                igstRateTextBox.Enabled = true;
                igstAmtTextBox.Enabled = true;

                UpdateIgstData();
            }
            else if (gstTypeComboBox.SelectedIndex == 1)
            {
                cgstRateTextBox.Text = "2.5";
                cgstAmtTextBox.Enabled = true;
                sgstRateTextBox.Text = "2.5";
                sgstAmtTextBox.Enabled = true;
                cgstRateTextBox.Enabled = true;
                sgstRateTextBox.Enabled = true;
                igstRateTextBox.Text = "0";
                igstRateTextBox.Enabled = false;
                igstAmtTextBox.Enabled = false;

                UpdateCgstSgstData();
            }

        }

        private void cgstRateTextBox_TextChanged(object sender, EventArgs e)
        {
            string strCgstRate = cgstRateTextBox.Text;
            double dCgstRate = 0;
            bool bFlag = double.TryParse(strCgstRate, out dCgstRate);
            if (bFlag)
                dCgstRate = Convert.ToDouble(strCgstRate);
            double dCgstAmount = 0;
            bFlag = double.TryParse(taxValTextBox.Text, out dCgstAmount);
            if (bFlag)
                dCgstAmount = (Convert.ToDouble(taxValTextBox.Text) * dCgstRate) / 100;
            cgstAmtTextBox.Text = Convert.ToString(dCgstAmount);
            sgstRateTextBox.Text = cgstRateTextBox.Text;
            sgstAmtTextBox.Text = cgstAmtTextBox.Text;

            double dSgstAmount = Convert.ToDouble(sgstAmtTextBox.Text);
            double dTaxableValue = Convert.ToDouble(taxValTextBox.Text);

            billAmtTextBox.Text = Convert.ToString(Math.Round(dCgstAmount + dSgstAmount + dTaxableValue));

        }

        private void sgstRateTextBox_TextChanged(object sender, EventArgs e)
        {
            string strSgstRate = sgstRateTextBox.Text;
            double dSgstRate = 0;
            bool bFlag = double.TryParse(strSgstRate, out dSgstRate);
            if (bFlag)
                dSgstRate = Convert.ToDouble(strSgstRate);
            double dSgstAmount = 0;
            bFlag = double.TryParse(taxValTextBox.Text, out dSgstAmount);
            if (bFlag)
                dSgstAmount = (Convert.ToDouble(taxValTextBox.Text) * dSgstRate) / 100;
            sgstAmtTextBox.Text = Convert.ToString(dSgstAmount);
            cgstRateTextBox.Text = sgstRateTextBox.Text;
            cgstAmtTextBox.Text = sgstAmtTextBox.Text;

            double dCgstAmount = Convert.ToDouble(cgstAmtTextBox.Text);
            double dTaxableValue = Convert.ToDouble(taxValTextBox.Text);

            billAmtTextBox.Text = Convert.ToString(Math.Round(dCgstAmount + dSgstAmount + dTaxableValue));
        }

        private void totalAmountTextBox_TextChanged(object sender, EventArgs e)
        {
            double dTaxableValue = Convert.ToDouble(Convert.ToDouble(totalAmountTextBox.Text) - Convert.ToDouble(discPerAmtFinalTextBox.Text) + Convert.ToDouble(addLessAmtFinalTextBox.Text));
            taxValTextBox.Text = Convert.ToString(dTaxableValue);
        }

        private void taxValTextBox_TextChanged(object sender, EventArgs e)
        {
            if (igstRateTextBox.Enabled)
            {
                UpdateIgstData();
            }
            else
            {
                UpdateCgstSgstData();
            }

        }

        private void UpdateCgstSgstData()
        {
            string strSgstRate = sgstRateTextBox.Text;
            double dSgstRate = Convert.ToDouble(strSgstRate);
            double dSgstAmount = (Convert.ToDouble(taxValTextBox.Text) * dSgstRate) / 100;
            sgstAmtTextBox.Text = Convert.ToString(dSgstAmount);
            cgstRateTextBox.Text = sgstRateTextBox.Text;
            cgstAmtTextBox.Text = sgstAmtTextBox.Text;

            double dCgstAmount = Convert.ToDouble(cgstAmtTextBox.Text);
            double dTaxableValue = Convert.ToDouble(taxValTextBox.Text);

            billAmtTextBox.Text = Convert.ToString(Math.Round(dCgstAmount + dSgstAmount + dTaxableValue));
        }
        private void UpdateIgstData()
        {
            string strIgstRate = igstRateTextBox.Text;
            double dIgstRate = Convert.ToDouble(strIgstRate);
            double dIgstAmount = (Convert.ToDouble(taxValTextBox.Text) * dIgstRate) / 100;
            double dTaxableValue = Convert.ToDouble(taxValTextBox.Text);

            igstAmtTextBox.Text = Convert.ToString(dIgstAmount);
            billAmtTextBox.Text = Convert.ToString(Math.Round(dIgstAmount + dTaxableValue));
        }

        private void stateComboBox_SelectedValueChanged(object sender, EventArgs e)
        {
            string strCompanyStationCode = companyGstn.Substring(0, 2);
            if (stateComboBox.Text == strCompanyStationCode)
                gstTypeComboBox.SelectedValue = Convert.ToInt64(2);
            else
                gstTypeComboBox.SelectedValue = Convert.ToInt64(1);
        }

        private void discPerTextBox_TextChanged(object sender, EventArgs e)
        {
            double dDiscountPercentage = 0;
            double dDiscountPercentageOnAmount = 0;
            double dDiscountAmount = 0;
            bool bFlag = double.TryParse(discPerTextBox.Text, out dDiscountPercentage);
            if (bFlag)
                dDiscountPercentage = Convert.ToDouble(discPerTextBox.Text);
            bool bFlag1 = double.TryParse(discPerAmtTextBox.Text, out dDiscountPercentageOnAmount);
            if (bFlag1)
                dDiscountPercentageOnAmount = Convert.ToDouble(discPerAmtTextBox.Text);
            dDiscountAmount = (dDiscountPercentage * dDiscountPercentageOnAmount) / 100;
            discPerAmtFinalTextBox.Text = Convert.ToString(dDiscountAmount);
        }

        private void discPerAmtTextBox_TextChanged(object sender, EventArgs e)
        {
            double dDiscountPercentage = 0;
            double dDiscountPercentageOnAmount = 0;
            double dDiscountAmount = 0;
            bool bFlag = double.TryParse(discPerTextBox.Text, out dDiscountPercentage);
            if (bFlag)
                dDiscountPercentage = Convert.ToDouble(discPerTextBox.Text);
            bool bFlag1 = double.TryParse(discPerAmtTextBox.Text, out dDiscountPercentageOnAmount);
            if (bFlag1)
                dDiscountPercentageOnAmount = Convert.ToDouble(discPerAmtTextBox.Text);
            dDiscountAmount = (dDiscountPercentage * dDiscountPercentageOnAmount) / 100;
            discPerAmtFinalTextBox.Text = Convert.ToString(dDiscountAmount);
        }

        private void addLessTextBox_TextChanged(object sender, EventArgs e)
        {
            double dAddLess = 0;
            double dAddLessAmount = 0;
            bool bFlag = double.TryParse(addLessAmtTextBox.Text, out dAddLessAmount);
            bool bFlag1 = double.TryParse(addLessTextBox.Text, out dAddLess);

            if (bFlag && bFlag1)
                dAddLessAmount = Convert.ToDouble(addLessTextBox.Text) * Convert.ToDouble(addLessAmtTextBox.Text);
            addLessAmtFinalTextBox.Text = Convert.ToString(dAddLessAmount);
        }

        private void addLessAmtTextBox_TextChanged(object sender, EventArgs e)
        {
            double dAddLess = 0;
            double dAddLessAmount = 0;
            bool bFlag = double.TryParse(addLessAmtTextBox.Text, out dAddLessAmount);
            bool bFlag1 = double.TryParse(addLessTextBox.Text, out dAddLess);

            if (bFlag && bFlag1)
                dAddLessAmount = Convert.ToDouble(addLessTextBox.Text) * Convert.ToDouble(addLessAmtTextBox.Text);
            addLessAmtFinalTextBox.Text = Convert.ToString(dAddLessAmount);
        }

        private void discPerAmtFinalTextBox_TextChanged(object sender, EventArgs e)
        {
            double dDiscPerAmountFinal = 0;
            double dTaxableValue = 0;
            bool bFlag = double.TryParse(discPerAmtFinalTextBox.Text, out dDiscPerAmountFinal);
            if (bFlag)
                dTaxableValue = Convert.ToDouble(totalAmountTextBox.Text) - Convert.ToDouble(discPerAmtFinalTextBox.Text) + Convert.ToDouble(addLessAmtFinalTextBox.Text);
            else
                dTaxableValue = Convert.ToDouble(totalAmountTextBox.Text) - dDiscPerAmountFinal + Convert.ToDouble(addLessAmtFinalTextBox.Text);
            taxValTextBox.Text = Convert.ToString(dTaxableValue);
        }

        private void addLessAmtFinalTextBox_TextChanged(object sender, EventArgs e)
        {
            double dTaxableValue = 0;
            double dAddLessFinalAmount = 0;
            bool bFlag = double.TryParse(addLessAmtFinalTextBox.Text, out dAddLessFinalAmount);
            if (bFlag)
                dTaxableValue = Convert.ToDouble(totalAmountTextBox.Text) - Convert.ToDouble(discPerAmtFinalTextBox.Text) + Convert.ToDouble(addLessAmtFinalTextBox.Text);
            else
                dTaxableValue = Convert.ToDouble(totalAmountTextBox.Text) - Convert.ToDouble(discPerAmtFinalTextBox.Text) + dAddLessFinalAmount;
            taxValTextBox.Text = Convert.ToString(dTaxableValue);
        }



        private void dateTextBox_Validating(object sender, CancelEventArgs e)
        {
            DateTime dt;
            bool isValid = DateTime.TryParseExact(dateTextBox.Text, "dd-MM-yyyy", null, DateTimeStyles.None, out dt);
            if (!isValid)
            {
                MessageBox.Show("Please enter the valid date", "DATE", MessageBoxButtons.OK);
                dateTextBox.Focus();
            }
            }

        private void button1_Click(object sender, EventArgs e)
        {
            string billNo = billNoTextBox.Text;
            bool bFlag = partyDB.checkBillExists(billNoTextBox.Text);
            if (!bFlag)
            {
                DialogResult result = MessageBox.Show("Bill No. does not Exists.Do you want to enter a new bill?", "Bill Error", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    resetAllData();
                    setDafaultData();
                    billNoTextBox.SelectAll();
                    return;
                }
                billNoTextBox.Focus();
                billNoTextBox.SelectAll();
                return;
            }
            if (billNo != saleBillDetail.billno)
            {
                MessageBox.Show("You are not allowd to change Bill No.", "Bill Error", MessageBoxButtons.OK);
                billNoTextBox.Text = Convert.ToString(saleBillDetail.billno);
                billNoTextBox.Focus();
                return;
            }
            bool bSalesBillAdded = false;
            //lBillNo = Convert.ToInt64(vNoTextBox.Text);
            try
            {
                //var db = DBHelper2.GetDbContext(Properties.Settings.Default.FinancialYearDbName);
                //sales_details salesDetails = db.sales_details.Where(x => x.billno == lBillNo).SingleOrDefault();

                saleBillDetail = getSalesDataFromControl(saleBillDetail);

                saleBillDetail.salesitem_details = getSalesItemDetailsForSaleBillUpdate(saleBillDetail.salesitem_details.ToList());

                //db.Dispose();
                bSalesBillAdded = partyDB.updateSalesBill(saleBillDetail);
            }
            catch (System.Data.Entity.Infrastructure.DbUpdateConcurrencyException ex)
            {

                DialogResult res = MessageBox.Show("Sales Bill has been updated by Another User while you were editing,due want to reload", "SALES", MessageBoxButtons.YesNo);
                if (res == DialogResult.Yes)
                {
                    openBillButton_Click(sender, e);
                }
                return;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "SALES", MessageBoxButtons.OK);
                return;
            }

            /*sales_details salesDetails = partyDB.getSalesBillDetail(Convert.ToInt64(vNoTextBox.Text));
            salesDetails = getSalesDataFromControl(salesDetails);
            salesDetails.salesitem_details = getSalesItemDetailsForSaleBillUpdate(salesDetails.salesitem_details.ToList());
            sales_details salesDetails = new sales_details();
            salesDetails = getSalesDataFromControl(salesDetails);
            salesDetails.salesbillid = Convert.ToInt64(vNoTextBox.Text);
            salesDetails.salesitem_details = getSalesItemDetails();
            bool bSalesBillAdded = partyDB.updateSalesBill(salesDetails);*/
            if (bSalesBillAdded)
            {
                MessageBox.Show("Sales Bill updated Successfully", "SALES", MessageBoxButtons.OK);
                resetAllData();
                setDafaultData();
                billNoTextBox.SelectAll();

            }
            else
            {
                List<salesitem_details> items = saleBillDetail.salesitem_details.Where(p => p.item_id == 0).ToList();
                foreach (salesitem_details item in items)
                {
                    saleBillDetail.salesitem_details.Remove(item);
                }

                MessageBox.Show("Sales Bill NOT updated", "ERROR", MessageBoxButtons.OK);
            }
        }

        private sales_details getSalesDataFromControl(sales_details salesDetails)
        {
            salesDetails.billno = billNoTextBox.Text;
            salesDetails.brokerid = Convert.ToInt64(brokerComboBox.SelectedValue);
            salesDetails.caseno = caseNoTextBox.Text;
            salesDetails.cgst = Convert.ToDouble(cgstAmtTextBox.Text);
            salesDetails.date = DateTime.ParseExact(dateTextBox.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            //salesDetails.date = Convert.ToDateTime(dateTextBox.Text);
            salesDetails.discount = discPerAmtFinalTextBox.Text;
            salesDetails.gstTypeid = Convert.ToInt64(gstTypeComboBox.SelectedValue);
            salesDetails.igst = Convert.ToDouble(igstAmtTextBox.Text);
            salesDetails.lrno = lrnoTextBox.Text;
            if((long)salesTypeComboBox.SelectedValue == 5 || (long)salesTypeComboBox.SelectedValue == 6 || (long)salesTypeComboBox.SelectedValue == 7 || (long)salesTypeComboBox.SelectedValue == 8)
                salesDetails.orderno = refBillNoComboBox["BILLNO"].ToString();
            else
                salesDetails.orderno = orderNoTextBox.Text;
            salesDetails.partygst = gstnTextBox.Text;
            salesDetails.partyid = Convert.ToInt64(partyComboBox["PARTY_ID"].ToString());
            //salesDetails.partyid = Convert.ToInt64(partyComboBox.SelectedValue);
            salesDetails.remark = remarkTextBox.Text;
            salesDetails.voucherNo = Convert.ToInt64(vNoTextBox.Text);
            salesDetails.salesTypeid = Convert.ToInt64(salesTypeComboBox.SelectedValue);
            salesDetails.sgst = Convert.ToDouble(sgstAmtTextBox.Text);
            salesDetails.statecode = Convert.ToInt64(stateComboBox.SelectedValue);
            salesDetails.station = Convert.ToInt64(stateComboBox.SelectedValue);
            salesDetails.taxablevalue = Convert.ToDouble(taxValTextBox.Text);
            salesDetails.transportid = Convert.ToInt64(transportComboBox.SelectedValue);
            salesDetails.additional = addLessAmtFinalTextBox.Text;
            salesDetails.billamount = Convert.ToDouble(billAmtTextBox.Text);
            salesDetails.addLessDetail1 = Convert.ToDouble(addLessTextBox.Text);
            salesDetails.addLessDetail2 = Convert.ToDouble(addLessAmtTextBox.Text);
            salesDetails.caseNoDetail2 = caseNoTextBox2.Text;
            salesDetails.cgstRate = Convert.ToDouble(cgstRateTextBox.Text);
            salesDetails.discountOnAmount = Convert.ToDouble(discPerAmtTextBox.Text);
            salesDetails.discountPercentage = Convert.ToDouble(discPerTextBox.Text);
            salesDetails.grossAmount = Convert.ToDouble(totalAmountTextBox.Text);
            salesDetails.igstRate = Convert.ToDouble(igstRateTextBox.Text);
            salesDetails.sgstRate = Convert.ToDouble(sgstRateTextBox.Text);
            salesDetails.duedays = Convert.ToInt64(dueDaysTextBox.Text);
            return salesDetails;

        }

        private List<salesitem_details> getSalesItemDetails()
        {

            if (saleDataGridView.RowCount > 0)
            {
                List<salesitem_details> itemDetails = new List<salesitem_details>();
                for (int i = 0; i < saleDataGridView.RowCount - 1; i++)
                {
                    salesitem_details salesItem = new salesitem_details();
                    salesItem.salesitem_id = (long)(saleDataGridView.Rows[i].Cells[itemNameGridCombo.Index].Value);
                    if (saleDataGridView.Rows[i].Cells[hsncEditBoxGrid.Index].Value == null)
                        salesItem.hsncode = "";
                    else
                        salesItem.hsncode = saleDataGridView.Rows[i].Cells[hsncEditBoxGrid.Index].Value.ToString();
                    if (saleDataGridView.Rows[i].Cells[bundlesEditBoxGrid.Index].Value == null)
                        salesItem.bundles = "";
                    else
                        salesItem.bundles = saleDataGridView.Rows[i].Cells[bundlesEditBoxGrid.Index].Value.ToString();
                    salesItem.amount = Convert.ToDouble(saleDataGridView.Rows[i].Cells[amountTextBoxGrid.Index].Value.ToString());
                    if (saleDataGridView.Rows[i].Cells[cutEditBoxGrid.Index].Value == null)
                        salesItem.cut = 0.0;
                    else
                        salesItem.cut = Convert.ToDouble(saleDataGridView.Rows[i].Cells[cutEditBoxGrid.Index].Value.ToString());
                    salesItem.packing = saleDataGridView.Rows[i].Cells[packingComboGrid.Index].EditedFormattedValue.ToString();
                    salesItem.pcs = Convert.ToInt64(saleDataGridView.Rows[i].Cells[pcsTextBoxGrid.Index].Value.ToString());
                    salesItem.quantity = Convert.ToDouble(saleDataGridView.Rows[i].Cells[qtyEditBoxGid.Index].Value.ToString());
                    salesItem.rate = Convert.ToDouble(saleDataGridView.Rows[i].Cells[rateTextBoxGrid.Index].Value.ToString());
                    //salesItem.salesbillid = Convert.ToInt64(vNoTextBox.Text);
                    salesItem.unit = saleDataGridView.Rows[i].Cells[unitComboGrid.Index].EditedFormattedValue.ToString();
                    itemDetails.Add(salesItem);
                    //itemDetails[i].
                }
                return itemDetails;
            }
            else
                return null;
        }

        private void igstRateTextBox_TextChanged(object sender, EventArgs e)
        {
            double dIgstRate = 0;
            double dIgstAmount = 0;
            bool bFlag = double.TryParse(igstRateTextBox.Text, out dIgstRate);
            if (bFlag)
                dIgstAmount = (dIgstRate * Convert.ToDouble(taxValTextBox.Text)) / 100;

            igstAmtTextBox.Text = Convert.ToString(dIgstAmount);
            billAmtTextBox.Text = Convert.ToString(Math.Round(dIgstAmount + Convert.ToDouble(taxValTextBox.Text)));
        }

        private void igstAmtTextBox_TextChanged(object sender, EventArgs e)
        {
            double dBillAmout = 0;
            double dIgstAmount = 0;
            bool bFlag = double.TryParse(igstAmtTextBox.Text, out dIgstAmount);
            if (bFlag)
                dBillAmout = dIgstAmount + Convert.ToDouble(taxValTextBox.Text);
            else
                dBillAmout = Convert.ToDouble(taxValTextBox.Text);
            billAmtTextBox.Text = Convert.ToString(Math.Round(dBillAmout));
        }

        private void resetAllData()
        {
            saleDataGridView.Rows.Clear();
            remarkTextBox.Text = "";
            totalPcsTextBox.Text = "0";
            totalQtyTextBox.Text = "0";
            totalAmountTextBox.Text = "0";
            discPerTextBox.Text = "0";
            discPerAmtTextBox.Text = "0";
            addLessTextBox.Text = "0";
            addLessAmtTextBox.Text = "0";
            caseNoTextBox.Text = "";
            caseNoTextBox2.Text = "1";
            cgstRateTextBox.Text = "2.5";
            sgstRateTextBox.Text = "2.5";
            igstRateTextBox.Text = "5";
            lrnoTextBox.Text = "";
            orderNoTextBox.Text = "0";
            billNoTextBox.Focus();
        }

        private void billNoTextBox_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Enter)
            {
                if (billNoTextBox.Text.Length <= 0)
                {
                    MessageBox.Show("Please enter a valid Bill No.", "Bill Error", MessageBoxButtons.OK);
                    billNoTextBox.Focus();
                    billNoTextBox.SelectAll();
                    return;
                }
                List<salesitem_details> saleItemDetails;
                string billNo = billNoTextBox.Text;
                try
                {
                    var db = DBHelper2.GetDbContext(Properties.Settings.Default.FinancialYearDbName);
                    long lAccountTypeID = Convert.ToInt64(salesTypeComboBox.SelectedValue);
                    saleBillDetail = db.sales_details.Where(x => x.billno == billNo).Where(x => x.salesTypeid == lAccountTypeID).SingleOrDefault();
                    //salesDetails = getSalesDataFromControl(salesDetails);
                    if (saleBillDetail == null)
                    {
                        DialogResult result = MessageBox.Show("Bill No. does not Exists.Do you want to enter a new bill?", "Bill Error", MessageBoxButtons.YesNo);
                        if (result == DialogResult.Yes)
                        {
                            resetAllData();
                            setDafaultData();
                            billNoTextBox.SelectAll();
                            return;
                        }
                        billNoTextBox.Focus();
                        billNoTextBox.SelectAll();
                        return;
                    }
                    saleItemDetails = saleBillDetail.salesitem_details.ToList();

                    db.Dispose();

                }
                catch (Exception ex)
                {
                    return;
                }




                vNoTextBox.Text = Convert.ToString(saleBillDetail.voucherNo);
                
                DateTime dt, dt1;
                bool bFlag = DateTime.TryParse(saleBillDetail.date.ToString(), out dt);
                if (bFlag)
                {
                    dt1 = dt.Date;
                    //DateTime.Parse(dt1.ToString()).ToString("dd-MM-yyyy");
                    dateTextBox.Text = dt1.ToString("dd-MM-yyyy");
                }


                //dateTextBox.Text = saleBillDetail.date.ToString();
                try
                {
                    setPartyComboBoxSelectedValue(saleBillDetail.partyid);
                    //partyComboBox["PARTY_ID"] = Convert.ToString(saleBillDetail.partyid);
                    //partyComboBox.SelectedValue = saleBillDetail.partyid;
                    if ((long)salesTypeComboBox.SelectedValue == 5 || (long)salesTypeComboBox.SelectedValue == 6 || (long)salesTypeComboBox.SelectedValue == 7 || (long)salesTypeComboBox.SelectedValue == 8)
                        refBillNoComboBox.Text = saleBillDetail.orderno;
                    else
                        orderNoTextBox.Text = saleBillDetail.orderno;
                    stateComboBox.SelectedValue = saleBillDetail.statecode;
                    gstTypeComboBox.SelectedValue = saleBillDetail.gstTypeid;
                    stateComboBox.SelectedValue = saleBillDetail.station;
                    brokerComboBox.SelectedValue = saleBillDetail.brokerid;
                    transportComboBox.SelectedValue = saleBillDetail.transportid;
                    lrnoTextBox.Text = saleBillDetail.lrno;
                    remarkTextBox.Text = saleBillDetail.remark;
                    discPerTextBox.Text = Convert.ToString(saleBillDetail.discountPercentage);
                    discPerAmtTextBox.Text = Convert.ToString(saleBillDetail.discountOnAmount);
                    discPerAmtFinalTextBox.Text = Convert.ToString(saleBillDetail.discount);
                    addLessTextBox.Text = Convert.ToString(saleBillDetail.addLessDetail1);
                    addLessAmtTextBox.Text = Convert.ToString(saleBillDetail.addLessDetail2);
                    addLessAmtFinalTextBox.Text = Convert.ToString(saleBillDetail.additional);
                    cgstRateTextBox.Text = Convert.ToString(saleBillDetail.cgstRate);
                    cgstAmtTextBox.Text = Convert.ToString(saleBillDetail.cgst);
                    sgstRateTextBox.Text = Convert.ToString(saleBillDetail.sgstRate);
                    sgstAmtTextBox.Text = Convert.ToString(saleBillDetail.sgst);
                    igstRateTextBox.Text = Convert.ToString(saleBillDetail.igstRate);
                    igstAmtTextBox.Text = Convert.ToString(saleBillDetail.igst);
                    caseNoTextBox.Text = saleBillDetail.caseno;
                    caseNoTextBox2.Text = saleBillDetail.caseNoDetail2;
                    dueDaysTextBox.Text = Convert.ToString(saleBillDetail.duedays);
                    //taxValTextBox.Text = Convert.ToString(saleBillDetail.taxablevalue);
                    //billAmtTextBox.Text = Convert.ToString(saleBillDetail.billamount);
                    totalAmountTextBox.Text = Convert.ToString(saleBillDetail.grossAmount);
                }
                catch (Exception ex)
                {
                    return;
                }

                //List<salesitem_details> saleItemDetails = partyDB.getItemDetailsOfSaleBill(Convert.ToInt64(billNoTextBox.Text));
                //saleItemDetails = saleBillDetail.salesitem_details.ToList();
                if (saleItemDetails == null)
                    return;
                else
                    saleDataGridView.Rows.Clear();
                for (int i = 0; i < saleItemDetails.Count; i++)
                {
                    saleDataGridView.Rows.Add();
                    saleDataGridView.Rows[i].Cells[itemNameGridCombo.Index].Value = saleItemDetails[i].salesitem_id;
                    saleDataGridView.Rows[i].Cells[bundlesEditBoxGrid.Index].Value = saleItemDetails[i].bundles;
                    saleDataGridView.Rows[i].Cells[hsncEditBoxGrid.Index].Value = saleItemDetails[i].hsncode;
                    saleDataGridView.Rows[i].Cells[packingComboGrid.Index].Value = saleItemDetails[i].packing;
                    //saleDataGridView.Rows[i].Cells[packingComboGrid.Index].Value = saleItemDetails[i].packing;
                    saleDataGridView.Rows[i].Cells[unitComboGrid.Index].Value = saleItemDetails[i].unit;
                    saleDataGridView.Rows[i].Cells[pcsTextBoxGrid.Index].Value = Convert.ToString(saleItemDetails[i].pcs);
                    saleDataGridView.Rows[i].Cells[cutEditBoxGrid.Index].Value = Convert.ToString(saleItemDetails[i].cut);
                    saleDataGridView.Rows[i].Cells[qtyEditBoxGid.Index].Value = Convert.ToString(saleItemDetails[i].quantity);
                    saleDataGridView.Rows[i].Cells[rateTextBoxGrid.Index].Value = Convert.ToString(saleItemDetails[i].rate);
                    saleDataGridView.Rows[i].Cells[amountTextBoxGrid.Index].Value = Convert.ToString(saleItemDetails[i].amount);

                }
                button1.Enabled = true;
                deleteButton.Enabled = true;
                addNewButton.Enabled = false;
                billNoTextBox.SelectAll();
            }



        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            if (billNoTextBox.Text.Length <= 0)
            {
                MessageBox.Show("Please enter a valid Bill No.", "Bill Error", MessageBoxButtons.OK);
                billNoTextBox.Focus();
                return;
            }
            DialogResult result = MessageBox.Show("Are you sure you want to delete the bill", "DELETE BILL", MessageBoxButtons.YesNo);
            if (result == DialogResult.No)
                return;

            bool bFlag = partyDB.deleteSaleBill(Convert.ToInt64(billNoTextBox.Text));
            if (bFlag)
            {
                MessageBox.Show("Bill No. " + billNoTextBox.Text + " deleted successfully.", "Delete Bill", MessageBoxButtons.OK);
                resetAllData();
                setDafaultData();
            }
            else
                MessageBox.Show("Bill No. " + billNoTextBox.Text + " not deleted successfully.", "Delete Bill", MessageBoxButtons.OK);

        }

        private void dueDaysTextBox_TextChanged(object sender, EventArgs e)
        {
            long lDueDays = 0;
            bool bFlag = long.TryParse(dueDaysTextBox.Text, out lDueDays);
            if (!bFlag)
                dueDaysTextBox.Text = "0";

        }

        private List<salesitem_details> getSalesItemDetailsForSaleBillUpdate(List<salesitem_details> itemDetails)
        {
            if (saleDataGridView.RowCount > 0)
            {
                //List<salesitem_details> itemDetails = new List<salesitem_details>();


                for (int i = 0; i < saleDataGridView.RowCount - 1; i++)
                {
                    if (i >= itemDetails.Count)
                    {
                        salesitem_details salesItem = new salesitem_details();
                        salesItem.salesitem_id = (long)(saleDataGridView.Rows[i].Cells[itemNameGridCombo.Index].Value);
                        if (saleDataGridView.Rows[i].Cells[hsncEditBoxGrid.Index].Value == null)
                            salesItem.hsncode = "";
                        else
                            salesItem.hsncode = saleDataGridView.Rows[i].Cells[hsncEditBoxGrid.Index].Value.ToString();
                        if (saleDataGridView.Rows[i].Cells[bundlesEditBoxGrid.Index].Value == null)
                            salesItem.bundles = "";
                        else
                            salesItem.bundles = saleDataGridView.Rows[i].Cells[bundlesEditBoxGrid.Index].Value.ToString();
                        salesItem.amount = Convert.ToDouble(saleDataGridView.Rows[i].Cells[amountTextBoxGrid.Index].Value.ToString());
                        if (saleDataGridView.Rows[i].Cells[cutEditBoxGrid.Index].Value == null)
                            salesItem.cut = 0.0;
                        else
                            salesItem.cut = Convert.ToDouble(saleDataGridView.Rows[i].Cells[cutEditBoxGrid.Index].Value.ToString());
                        salesItem.packing = saleDataGridView.Rows[i].Cells[packingComboGrid.Index].EditedFormattedValue.ToString();
                        salesItem.pcs = Convert.ToInt64(saleDataGridView.Rows[i].Cells[pcsTextBoxGrid.Index].Value.ToString());
                        salesItem.quantity = Convert.ToDouble(saleDataGridView.Rows[i].Cells[qtyEditBoxGid.Index].Value.ToString());
                        salesItem.rate = Convert.ToDouble(saleDataGridView.Rows[i].Cells[rateTextBoxGrid.Index].Value.ToString());
                        //salesItem.salesbillid = Convert.ToInt64(vNoTextBox.Text);
                        salesItem.unit = saleDataGridView.Rows[i].Cells[unitComboGrid.Index].EditedFormattedValue.ToString();
                        //if (itemDetails[i].item_id == 0)
                        itemDetails.Add(salesItem);
                    }
                    else
                    {
                        if (itemDetails[i].item_id == 0)
                        {
                            salesitem_details salesItem = new salesitem_details();
                            salesItem.salesitem_id = (long)(saleDataGridView.Rows[i].Cells[itemNameGridCombo.Index].Value);
                            if (saleDataGridView.Rows[i].Cells[hsncEditBoxGrid.Index].Value == null)
                                salesItem.hsncode = "";
                            else
                                salesItem.hsncode = saleDataGridView.Rows[i].Cells[hsncEditBoxGrid.Index].Value.ToString();
                            if (saleDataGridView.Rows[i].Cells[bundlesEditBoxGrid.Index].Value == null)
                                salesItem.bundles = "";
                            else
                                salesItem.bundles = saleDataGridView.Rows[i].Cells[bundlesEditBoxGrid.Index].Value.ToString();
                            salesItem.amount = Convert.ToDouble(saleDataGridView.Rows[i].Cells[amountTextBoxGrid.Index].Value.ToString());
                            if (saleDataGridView.Rows[i].Cells[cutEditBoxGrid.Index].Value == null)
                                salesItem.cut = 0.0;
                            else
                                salesItem.cut = Convert.ToDouble(saleDataGridView.Rows[i].Cells[cutEditBoxGrid.Index].Value.ToString());
                            salesItem.packing = saleDataGridView.Rows[i].Cells[packingComboGrid.Index].EditedFormattedValue.ToString();
                            salesItem.pcs = Convert.ToInt64(saleDataGridView.Rows[i].Cells[pcsTextBoxGrid.Index].Value.ToString());
                            salesItem.quantity = Convert.ToDouble(saleDataGridView.Rows[i].Cells[qtyEditBoxGid.Index].Value.ToString());
                            salesItem.rate = Convert.ToDouble(saleDataGridView.Rows[i].Cells[rateTextBoxGrid.Index].Value.ToString());
                            //salesItem.salesbillid = Convert.ToInt64(vNoTextBox.Text);
                            salesItem.unit = saleDataGridView.Rows[i].Cells[unitComboGrid.Index].EditedFormattedValue.ToString();
                            if (itemDetails[i].item_id == 0)
                                itemDetails.Add(salesItem);
                        }
                        else
                        {

                            itemDetails[i].salesitem_id = (long)(saleDataGridView.Rows[i].Cells[itemNameGridCombo.Index].Value);
                            if (saleDataGridView.Rows[i].Cells[hsncEditBoxGrid.Index].Value == null)
                                itemDetails[i].hsncode = "";
                            else
                                itemDetails[i].hsncode = saleDataGridView.Rows[i].Cells[hsncEditBoxGrid.Index].Value.ToString();
                            if (saleDataGridView.Rows[i].Cells[bundlesEditBoxGrid.Index].Value == null)
                                itemDetails[i].bundles = "";
                            else
                                itemDetails[i].bundles = saleDataGridView.Rows[i].Cells[bundlesEditBoxGrid.Index].Value.ToString();
                            itemDetails[i].amount = Convert.ToDouble(saleDataGridView.Rows[i].Cells[amountTextBoxGrid.Index].Value.ToString());
                            itemDetails[i].cut = Convert.ToDouble(saleDataGridView.Rows[i].Cells[cutEditBoxGrid.Index].Value.ToString());
                            itemDetails[i].packing = saleDataGridView.Rows[i].Cells[packingComboGrid.Index].EditedFormattedValue.ToString();
                            itemDetails[i].pcs = Convert.ToInt64(saleDataGridView.Rows[i].Cells[pcsTextBoxGrid.Index].Value.ToString());
                            itemDetails[i].quantity = Convert.ToDouble(saleDataGridView.Rows[i].Cells[qtyEditBoxGid.Index].Value.ToString());
                            itemDetails[i].rate = Convert.ToDouble(saleDataGridView.Rows[i].Cells[rateTextBoxGrid.Index].Value.ToString());
                            //itemDetails[i].salesbillid = Convert.ToInt64(vNoTextBox.Text);
                            itemDetails[i].unit = saleDataGridView.Rows[i].Cells[unitComboGrid.Index].EditedFormattedValue.ToString();

                        }
                    }


                    //itemDetails[i].
                }
                return itemDetails;
            }
            else
                return null;
        }

        private void billNoTextBox_TextChanged(object sender, EventArgs e)
        {
            long lBillNo = 0;
            bool bFlag = long.TryParse(billNoTextBox.Text, out lBillNo);
            if (!bFlag)
            {
                MessageBox.Show("Bill No. can only be a number.Please enter proper billno.", "Error", MessageBoxButtons.OK);
                resetAllData();
                setDafaultData();
                billNoTextBox.SelectAll();
            }
        }

        private void partyComboBox_Validating(object sender, CancelEventArgs e)
        {
            if (partyComboBox.SelectedIndex == -1)
            {
                MessageBox.Show("Please Select Party", "Error", MessageBoxButtons.OK);
                partyComboBox.Focus();
                return;

            }
        }

        private void salesTypeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            resetAllData();
            using (var db = new Entities())
            {
                long maxVoucherNo = partyDB.getSalesVoucherNo(Convert.ToInt64(salesTypeComboBox.SelectedValue));
                long maxBillNo = Convert.ToInt64(partyDB.getSalesBillNo());
                if (maxVoucherNo != 0 && maxBillNo != 0)
                {
                    vNoTextBox.Text = Convert.ToString(maxVoucherNo + 1);
                    billNoTextBox.Text = Convert.ToString(maxVoucherNo + 1);
                }
                else
                {
                    vNoTextBox.Text = "1";
                    billNoTextBox.Text = "1";
                }
            }
            if ((long)salesTypeComboBox.SelectedValue == 5 || (long)salesTypeComboBox.SelectedValue == 6 || (long)salesTypeComboBox.SelectedValue == 7 || (long)salesTypeComboBox.SelectedValue == 8)
            {
                billNoTextBox.SelectAll();
                label5.Hide();
                orderNoTextBox.Hide();
                refBillNoComboBox.Show();
                label35.Show();
                fillReferenceBillDetailsForReturn(Convert.ToInt64(partyComboBox["PARTY_ID"].ToString()));
            }
            else
            {
                billNoTextBox.SelectAll();
                label5.Show();
                orderNoTextBox.Show();
                refBillNoComboBox.Hide();
                label35.Hide();
            }


        }

        private void setPartyComboBoxSelectedValue(long lPartyID)
        {
            try
            {
                //partyComboBox.SelectedIndex = -1;
                if (lPartyID >= 0)
                {
                    for (int index = 0; index < partyComboBox.Items.Count; index++)
                    {
                        DataTable partyTable = partyComboBox.Data;
                        DataRow Row = partyTable.Rows[index];
                        if (Row != null)
                        {
                            if (Convert.ToInt64(Row["PARTY_ID"].ToString()) == lPartyID)
                            {
                                partyComboBox.SelectedIndex = -1;
                                partyComboBox.SelectedIndex = index;
                                break;
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                return;
            }
        }

        private void fillSalesTypeList()
        {
            var typeOfSaleList = DB.getSalesTypeList();
            if (typeOfSaleList != null)
            {

                salesTypeComboBox.DisplayMember = "type";
                salesTypeComboBox.ValueMember = "salesTypeid";
                salesTypeComboBox.DataSource = typeOfSaleList;
            }
        }

        private void fillReferenceBillDetailsForReturn(long lPartyID)
        {
            List<sales_details> saleBillList = partyDB.getSalesBillDetailFromPartyID(lPartyID);
            List<PassBookBillDataForMultiColumnDisplay> passBookMultiColunDisplayDataList = new List<PassBookBillDataForMultiColumnDisplay>();
            if (saleBillList == null)
                return;

            for (int i = 0; i < saleBillList.Count; i++)
            {
                PassBookBillDataForMultiColumnDisplay multiColumnDisplayBillData = new PassBookBillDataForMultiColumnDisplay();
                multiColumnDisplayBillData.billNo = saleBillList[i].billno;
                //multiColumnDisplayBillData.billType = Convert.ToInt64(saleBillList[i].salesTypeid);
                DateTime dt, dt1;
                string billDate = "";
                bool bFlag = DateTime.TryParse(saleBillList[i].date.ToString(), out dt);
                if (bFlag)
                {
                    dt1 = dt.Date;
                    //DateTime.Parse(dt1.ToString()).ToString("dd-MM-yyyy");
                    billDate = dt1.ToString("dd-MM-yyyy");
                }
                multiColumnDisplayBillData.date = billDate;
                multiColumnDisplayBillData.netAmount = Convert.ToDouble(saleBillList[i].billamount);
                multiColumnDisplayBillData.taxableValue = Convert.ToInt64(saleBillList[i].taxablevalue);
                List<passbook_itemdetails> billReceivedList = partyDB.getReceivedBillAmount(lPartyID, 1, Convert.ToString(saleBillList[i].billno));
                if (billReceivedList == null || billReceivedList.Count == 0)
                    multiColumnDisplayBillData.pendingAmount = multiColumnDisplayBillData.netAmount;
                else
                {
                    double dReceivedAmount = 0;
                    for (int j = 0; j < billReceivedList.Count; j++)
                    {
                        dReceivedAmount = dReceivedAmount + Convert.ToDouble(billReceivedList[j].discountAmount + billReceivedList[j].others + billReceivedList[j].paymentReceivedAmount + billReceivedList[j].rdAmount + billReceivedList[j].rgAmount + billReceivedList[j].tdsAmount + billReceivedList[j].addLess);
                    }
                    multiColumnDisplayBillData.pendingAmount = multiColumnDisplayBillData.netAmount - dReceivedAmount;
                }

                List<creditdebitnote_details> cdNoteForSaleBillDetails = partyDB.getCDNoteDetailForSaleBill(Convert.ToString(saleBillList[i].billno), lPartyID);
                for(int k = 0;k < cdNoteForSaleBillDetails.Count;k++)
                {
                    multiColumnDisplayBillData.pendingAmount = multiColumnDisplayBillData.pendingAmount - Convert.ToDouble(cdNoteForSaleBillDetails[k].netAmount);
                }


                List<sales_details> rgReceivedList = partyDB.getReceivedRGAmount(Convert.ToString(saleBillList[i].billno), lPartyID);
                if(rgReceivedList != null)
                {
                    for (int j = 0; j < rgReceivedList.Count; j++)
                    {
                        multiColumnDisplayBillData.pendingAmount = multiColumnDisplayBillData.pendingAmount - rgReceivedList[j].billamount;
                    }
                }
                
                if (multiColumnDisplayBillData.pendingAmount > 0 && (saleBillList[i].salesTypeid == 1 || saleBillList[i].salesTypeid == 2 || saleBillList[i].salesTypeid == 3 || saleBillList[i].salesTypeid == 4))
                    passBookMultiColunDisplayDataList.Add(multiColumnDisplayBillData);
            }
                
                int iColumnCount = 4;


                DataTable myDataTable = new DataTable("ParentTable");
                // Declare variables for DataColumn and DataRow objects.
                DataColumn myDataColumn;
                DataRow myDataRow;

                //Add some coulumns
                for (int index = 0; index < iColumnCount; index++)
                {
                    myDataColumn = new DataColumn();
                    myDataColumn.DataType = typeof(string);
                    if (index == 0)
                        myDataColumn.ColumnName = "DATE";
                    else if (index == 1)
                        myDataColumn.ColumnName = "BILLNO";
                    else if (index == 2)
                        myDataColumn.ColumnName = "NET AMOUNT";
                    else if (index == 3)
                        myDataColumn.ColumnName = "PENDING AMOUNT";

                    //myDataColumn.Caption = "ParentItem";
                    myDataColumn.AutoIncrement = false;
                    myDataColumn.ReadOnly = false;
                    myDataColumn.Unique = false;
                    // Add the Column to the DataColumnCollection.
                    myDataTable.Columns.Add(myDataColumn);
                }


                //Add some more rows
                for (int index = 0; index < passBookMultiColunDisplayDataList.Count; index++)
                {
                    myDataRow = myDataTable.NewRow();
                    myDataRow[0] = passBookMultiColunDisplayDataList[index].date;
                    myDataRow[1] = passBookMultiColunDisplayDataList[index].billNo;
                    myDataRow[2] = passBookMultiColunDisplayDataList[index].netAmount;
                    myDataRow[3] = passBookMultiColunDisplayDataList[index].pendingAmount;

                    myDataTable.Rows.Add(myDataRow);
                }


                //Now set the Data of the ColumnComboBox
                refBillNoComboBox.DisplayMember = "BILLNO";
                refBillNoComboBox.ValueMember = "BILLNO";
                refBillNoComboBox.Data = myDataTable;
                //Set which row will be displayed in the text box
                //If you set this to a column that isn't displayed then the suggesting functionality won't work.
                refBillNoComboBox.ColumnSpacing = 50;
                refBillNoComboBox.ViewColumn = 1;

                refBillNoComboBox.Columns[0].Width = 50;
                refBillNoComboBox.Columns[1].Width = 50;
                refBillNoComboBox.Columns[2].Width = 80;
                refBillNoComboBox.Columns[3].Width = 80;
                //Set a few columns to not be shown


            
        }

        private void vNoTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                if (billNoTextBox.Text.Length <= 0)
                {
                    MessageBox.Show("Please enter a valid Bill No.", "Bill Error", MessageBoxButtons.OK);
                    billNoTextBox.Focus();
                    billNoTextBox.SelectAll();
                    return;
                }
                List<salesitem_details> saleItemDetails;
                if (vNoTextBox.Text == "")
                    return;
                long lVoucherNo = Convert.ToInt64(vNoTextBox.Text);
                try
                {
                    var db = DBHelper2.GetDbContext(Properties.Settings.Default.FinancialYearDbName);
                    long lAccountTypeID = Convert.ToInt64(salesTypeComboBox.SelectedValue);
                    saleBillDetail = db.sales_details.Where(x => x.voucherNo == lVoucherNo).Where(x => x.salesTypeid == lAccountTypeID).SingleOrDefault();
                    //salesDetails = getSalesDataFromControl(salesDetails);
                    if (saleBillDetail == null)
                    {
                        DialogResult result = MessageBox.Show("Bill No. does not Exists.Do you want to enter a new bill?", "Bill Error", MessageBoxButtons.YesNo);
                        if (result == DialogResult.Yes)
                        {
                            resetAllData();
                            setDafaultData();
                            billNoTextBox.SelectAll();
                            return;
                        }
                        billNoTextBox.Focus();
                        billNoTextBox.SelectAll();
                        return;
                    }
                    saleItemDetails = saleBillDetail.salesitem_details.ToList();

                    db.Dispose();

                }
                catch (Exception ex)
                {
                    return;
                }

                billNoTextBox.Text = Convert.ToString(saleBillDetail.billno);
                DateTime dt, dt1;
                bool bFlag = DateTime.TryParse(saleBillDetail.date.ToString(), out dt);
                if (bFlag)
                {
                    dt1 = dt.Date;
                    //DateTime.Parse(dt1.ToString()).ToString("dd-MM-yyyy");
                    dateTextBox.Text = dt1.ToString("dd-MM-yyyy");
                }


                //dateTextBox.Text = saleBillDetail.date.ToString();
                try
                {
                    setPartyComboBoxSelectedValue(saleBillDetail.partyid);
                    //partyComboBox["PARTY_ID"] = Convert.ToString(saleBillDetail.partyid);
                    //partyComboBox.SelectedValue = saleBillDetail.partyid;
                    if ((long)salesTypeComboBox.SelectedValue == 5 || (long)salesTypeComboBox.SelectedValue == 6 || (long)salesTypeComboBox.SelectedValue == 7 || (long)salesTypeComboBox.SelectedValue == 8)
                        refBillNoComboBox.Text = saleBillDetail.orderno;
                    else
                        orderNoTextBox.Text = saleBillDetail.orderno;
                    stateComboBox.SelectedValue = saleBillDetail.statecode;
                    gstTypeComboBox.SelectedValue = saleBillDetail.gstTypeid;
                    stateComboBox.SelectedValue = saleBillDetail.station;
                    brokerComboBox.SelectedValue = saleBillDetail.brokerid;
                    transportComboBox.SelectedValue = saleBillDetail.transportid;
                    lrnoTextBox.Text = saleBillDetail.lrno;
                    remarkTextBox.Text = saleBillDetail.remark;
                    discPerTextBox.Text = Convert.ToString(saleBillDetail.discountPercentage);
                    discPerAmtTextBox.Text = Convert.ToString(saleBillDetail.discountOnAmount);
                    discPerAmtFinalTextBox.Text = Convert.ToString(saleBillDetail.discount);
                    addLessTextBox.Text = Convert.ToString(saleBillDetail.addLessDetail1);
                    addLessAmtTextBox.Text = Convert.ToString(saleBillDetail.addLessDetail2);
                    addLessAmtFinalTextBox.Text = Convert.ToString(saleBillDetail.additional);
                    cgstRateTextBox.Text = Convert.ToString(saleBillDetail.cgstRate);
                    cgstAmtTextBox.Text = Convert.ToString(saleBillDetail.cgst);
                    sgstRateTextBox.Text = Convert.ToString(saleBillDetail.sgstRate);
                    sgstAmtTextBox.Text = Convert.ToString(saleBillDetail.sgst);
                    igstRateTextBox.Text = Convert.ToString(saleBillDetail.igstRate);
                    igstAmtTextBox.Text = Convert.ToString(saleBillDetail.igst);
                    caseNoTextBox.Text = saleBillDetail.caseno;
                    caseNoTextBox2.Text = saleBillDetail.caseNoDetail2;
                    dueDaysTextBox.Text = Convert.ToString(saleBillDetail.duedays);
                    //taxValTextBox.Text = Convert.ToString(saleBillDetail.taxablevalue);
                    //billAmtTextBox.Text = Convert.ToString(saleBillDetail.billamount);
                    totalAmountTextBox.Text = Convert.ToString(saleBillDetail.grossAmount);
                }
                catch (Exception ex)
                {
                    return;
                }

                //List<salesitem_details> saleItemDetails = partyDB.getItemDetailsOfSaleBill(Convert.ToInt64(billNoTextBox.Text));
                //saleItemDetails = saleBillDetail.salesitem_details.ToList();
                if (saleItemDetails == null)
                    return;
                else
                    saleDataGridView.Rows.Clear();
                for (int i = 0; i < saleItemDetails.Count; i++)
                {
                    saleDataGridView.Rows.Add();
                    saleDataGridView.Rows[i].Cells[itemNameGridCombo.Index].Value = saleItemDetails[i].salesitem_id;
                    saleDataGridView.Rows[i].Cells[bundlesEditBoxGrid.Index].Value = saleItemDetails[i].bundles;
                    saleDataGridView.Rows[i].Cells[hsncEditBoxGrid.Index].Value = saleItemDetails[i].hsncode;
                    saleDataGridView.Rows[i].Cells[packingComboGrid.Index].Value = saleItemDetails[i].packing;
                    //saleDataGridView.Rows[i].Cells[packingComboGrid.Index].Value = saleItemDetails[i].packing;
                    saleDataGridView.Rows[i].Cells[unitComboGrid.Index].Value = saleItemDetails[i].unit;
                    saleDataGridView.Rows[i].Cells[pcsTextBoxGrid.Index].Value = Convert.ToString(saleItemDetails[i].pcs);
                    saleDataGridView.Rows[i].Cells[cutEditBoxGrid.Index].Value = Convert.ToString(saleItemDetails[i].cut);
                    saleDataGridView.Rows[i].Cells[qtyEditBoxGid.Index].Value = Convert.ToString(saleItemDetails[i].quantity);
                    saleDataGridView.Rows[i].Cells[rateTextBoxGrid.Index].Value = Convert.ToString(saleItemDetails[i].rate);
                    saleDataGridView.Rows[i].Cells[amountTextBoxGrid.Index].Value = Convert.ToString(saleItemDetails[i].amount);

                }
                button1.Enabled = true;
                deleteButton.Enabled = true;
                addNewButton.Enabled = false;
                billNoTextBox.SelectAll();
            }
        }

        private void setRefBillNoComboBoxSelectedValue(long lBillNo)
        {
            try
            {
                //partyComboBox.SelectedIndex = -1;
                if (lBillNo > 0)
                {
                    for (int index = 0; index < refBillNoComboBox.Items.Count; index++)
                    {
                        DataTable partyTable = refBillNoComboBox.Data;
                        DataRow Row = partyTable.Rows[index];
                        if (Row != null)
                        {
                            if (Convert.ToInt64(Row["BILLNO"].ToString()) == lBillNo)
                            {
                                refBillNoComboBox.SelectedIndex = index;
                                break;
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                return;
            }
        }

        private void printButton_Click(object sender, EventArgs e)
        {
           DGVPrinter print= printPage();
           print.PrintPreviewDataGridView(print.dgv2);
        }

        public void setSalesTypeMode(long lSalesTypeMode)
        {
            salesTypeComboBox.SelectedValue = lSalesTypeMode;
        }
        private DGVPrinter printPage()
        {
            string tagLine = "";
            string billerheaading = "";
            string termsandconditions = "";

            List<printersettings_details> printersettingsdetails = partyDB.getPrinterSettings();
            if (printersettingsdetails != null)
            {
                foreach (printersettings_details printst in printersettingsdetails)
                {
                    if ("tagline" == printst.type)
                        tagLine = printst.value;
                    else if ("billerheaading" == printst.type)
                        billerheaading = printst.value;
                    else if ("termsandconditions" == printst.type)
                        termsandconditions = printst.value;
                }
            }

            DGVPrinter print = new DGVPrinter();
            print.Title1 = billerheaading;
            print.Title1Spacing = 10.0F;
            print.Title2 = "GSTIN:    " + companyGstn + "                                                                                   PH.   [O]: " + Settings.Default.phone;
            //print.Title2Spacing = 10.0F;
            print.Title2Alignment = StringAlignment.Near;
            print.InvoiceType = "TAX INVOICE";
            print.InvoiceTypefont = new Font("Tahoma", 14, FontStyle.Underline, GraphicsUnit.Point);
            print.InvoiceTypeSpacing = 10.0F;
            print.Title = companyName;
            print.TitleFont = new Font("Tahoma", 16, FontStyle.Bold, GraphicsUnit.Point);
            print.TitleSpacing = 0.0F;
            print.SubTitleSpacing = 0.0F;
            print.SubTitle = tagLine + "\r\n" + Settings.Default.Address + "," + Settings.Default.city + "-" + Settings.Default.pincode + "\r\n" + companyGstn.Substring(0, 2) + "-Gujarat -" + Settings.Default.pincode + "  [M] " + Settings.Default.mobile;
            print.SubTitleFont = new Font("Tahoma", 10, FontStyle.Bold, GraphicsUnit.Point);
            String line = "---------------------------------------------------------------------------------------------------------------------------------------------------------------";
            print.AdhocLinesFont[0] = new Font("Tahoma", 12, FontStyle.Bold, GraphicsUnit.Point);
            print.AdhocLinesFont[1] = new Font("Tahoma", 10, FontStyle.Bold, GraphicsUnit.Point);
            print.AdhocLinesFont[2] = new Font("Tahoma", 10, FontStyle.Regular, GraphicsUnit.Point);
            print.AdhocLinesFont[3] = new Font("Tahoma", 10, FontStyle.Regular, GraphicsUnit.Point);
            print.AdhocLinesFont[4] = new Font("Tahoma", 10, FontStyle.Regular, GraphicsUnit.Point);
            print.AdhocLinesFont[5] = new Font("Tahoma", 10, FontStyle.Regular, GraphicsUnit.Point);
            print.AdhocLinesFont[6] = new Font("Tahoma", 12, FontStyle.Bold, GraphicsUnit.Point);
            print.AdhocLinesFont[7] = new Font("Tahoma", 10, FontStyle.Bold, GraphicsUnit.Point);
            print.AdhocLinesFont[8] = new Font("Tahoma", 10, FontStyle.Regular, GraphicsUnit.Point);
            print.AdhocLinesFont[9] = new Font("Tahoma", 12, FontStyle.Bold, GraphicsUnit.Point);
            print.AdhocLinesFont[10] = new Font("Tahoma", 10, FontStyle.Regular, GraphicsUnit.Point);
            print.AdhocLinesFont[11] = new Font("Tahoma", 10, FontStyle.Regular, GraphicsUnit.Point);
            print.AdhocLinesFont[12] = new Font("Tahoma", 10, FontStyle.Regular, GraphicsUnit.Point);
            print.AdhocLinesFont[13] = new Font("Tahoma", 12, FontStyle.Bold, GraphicsUnit.Point);
            string[] address = partyComboBox["ADDRESS"].ToString().Split('#');

            string line2 = "Buyer:   " + partyComboBox["NAME"].ToString() + "?BILL NO:         " + billNoTextBox.Text;
            string line3 = "             " + address[0] + ",?VOUCHER NO:  " + vNoTextBox.Text;
            string address2 = address.Length == 2 ? address[1] : "";
            string line4 = "             " + address2 + " ?DATE:              " + dateTextBox.Text;
            string line5 = "             " + partyComboBox["CITY"].ToString() + "?ORDER NO:      " + orderNoTextBox.Text;
            string line6 = "GSTIN:   " + gstnTextBox.Text;
            broker_details brokerAdd = (broker_details)brokerList.Where(b => b.brokerid == (long)brokerComboBox.SelectedValue).SingleOrDefault();
            string line7 = "AGENT:   " + brokerComboBox.Text + "?PHONES:" + brokerAdd.phone1 + "," + brokerAdd.phone2;
            string lineBAdd = "ADDRESS:   " + brokerAdd.address + "," + brokerAdd.address2;
            string linelr = "L. R. NO.:   " + lrnoTextBox.Text + "          L. R. DATE:   " + dateTextBox.Text + "?WEIGHT:  0";
            string linelr2 = "TRANSPORT:   " + transportComboBox.Text + "          CASE NO:   " + caseNoTextBox.Text + "x" + caseNoTextBox2.Text + "?FREIGHT: 0";
            string linelr3 = "STATION:   " + stationComboBox.Text + "-" + stateComboBox.Text;
            String[] adhoclines = new String[14];
            adhoclines[0] = line;
            adhoclines[1] = line2;
            adhoclines[2] = line3;
            adhoclines[3] = line4;
            adhoclines[4] = line5;
            adhoclines[5] = line6;
            adhoclines[6] = line;
            adhoclines[7] = line7;
            adhoclines[8] = lineBAdd;
            adhoclines[9] = line;
            adhoclines[10] = linelr;
            adhoclines[11] = linelr2;
            adhoclines[12] = linelr3;
            adhoclines[13] = line;
            print.printfooterHeight = 746.141052f;
            print.AdhocLines = adhoclines;
            String[] adhoclines2 = new String[16];
            print.AdhocLines2Font[0] = new Font("Tahoma", 12, FontStyle.Bold, GraphicsUnit.Point);
            print.AdhocLines2Margin[0] = "0";
            adhoclines2[0] = line;

            adhoclines2[1] = "BANK DETAILS:   " + Settings.Default.bankdetails + " " + Settings.Default.bankno;
            print.AdhocLines2Font[1] = new Font("Tahoma", 10, FontStyle.Bold, GraphicsUnit.Point);
            print.AdhocLines2Margin[1] = "0";
            adhoclines2[2] = "REMARK:   " + remarkTextBox.Text;
            print.AdhocLines2Font[2] = new Font("Tahoma", 10, FontStyle.Bold, GraphicsUnit.Point);
            print.AdhocLines2Margin[2] = "0";
            adhoclines2[3] = line;
            print.AdhocLines2Font[3] = new Font("Tahoma", 10, FontStyle.Bold, GraphicsUnit.Point);
            print.AdhocLines2Margin[3] = "0";
            string gTotal = "Total:?" + totalPcsTextBox.Text + "?" + totalQtyTextBox.Text + "?" + totalAmountTextBox.Text;
            print.AdhocLines2Font[4] = new Font("Tahoma", 10, FontStyle.Bold, GraphicsUnit.Point);
            print.AdhocLines2Margin[4] = "0,350,510,655";
            adhoclines2[4] = gTotal;
            print.AdhocLines2Font[5] = new Font("Tahoma", 10, FontStyle.Bold, GraphicsUnit.Point);
            print.AdhocLines2Margin[5] = "0";
            adhoclines2[5] = line;
            print.AdhocLines2Font[6] = new Font("Tahoma", 9, FontStyle.Regular, GraphicsUnit.Point);
            print.AdhocLines2Margin[6] = "300";
            int gstType = 0;
            int.TryParse(gstTypeComboBox.SelectedValue.ToString(), out gstType);
            if (1 == gstType)
                adhoclines2[6] = "IGST @ " + igstRateTextBox.Text + " on Taxable Value   " + taxValTextBox.Text + " =               " + igstAmtTextBox.Text;
            else
                adhoclines2[6] = "CGST @ " + cgstRateTextBox.Text + " on Taxable Value   " + taxValTextBox.Text + " =              " + cgstAmtTextBox.Text + "\nSGST @ " + sgstRateTextBox.Text + " on Taxable Value   " + taxValTextBox.Text + " =              " + sgstAmtTextBox.Text;
            print.AdhocLines2Font[7] = new Font("Tahoma", 10, FontStyle.Bold, GraphicsUnit.Point);
            print.AdhocLines2Margin[7] = "0";
            adhoclines2[7] = line;
            print.AdhocLines2Font[8] = new Font("Tahoma", 10, FontStyle.Bold, GraphicsUnit.Point);
            print.AdhocLines2Margin[8] = "0,510,655";
            adhoclines2[8] = "DUE DAYS:" + dueDaysTextBox.Text + "?Invoice Value?" + billAmtTextBox.Text;
            print.AdhocLines2Font[9] = new Font("Tahoma", 10, FontStyle.Bold, GraphicsUnit.Point);
            print.AdhocLines2Margin[9] = "0";
            adhoclines2[9] = line;
            print.AdhocLines2Font[10] = new Font("Tahoma", 8, FontStyle.Regular, GraphicsUnit.Point);
            print.AdhocLines2Margin[10] = "0";
            string wordsNum = NumberUtility.convertToWords(billAmtTextBox.Text);
            adhoclines2[10] = wordsNum.ToUpper();
            print.AdhocLines2Font[11] = new Font("Tahoma", 10, FontStyle.Bold, GraphicsUnit.Point);
            print.AdhocLines2Margin[11] = "0";
            adhoclines2[11] = line;
            print.AdhocLines2Font[12] = new Font("Tahoma", 10, FontStyle.Regular, GraphicsUnit.Point);
            print.AdhocLines2Margin[12] = "0,600";
            adhoclines2[12] = "TERMS & CONDITIONS:-?FOR " + companyName;
            string[] tnm = termsandconditions.Split(';');
            string term1 = "";
            int l = 0;
            foreach (string term in tnm)
            {
                l++;
                if (l <= 3)
                {
                    string termt = term.Replace("\n", "");
                    termt = termt.Trim();
                    term1 = term1 + l + ") " + termt + "\n";
                }
            }
            print.AdhocLines2Font[13] = new Font("Tahoma", 8, FontStyle.Regular, GraphicsUnit.Point);
            print.AdhocLines2Margin[13] = "0";
            adhoclines2[13] = term1;
            print.AdhocLines2Font[14] = new Font("Tahoma", 10, FontStyle.Regular, GraphicsUnit.Point);
            print.AdhocLines2Margin[14] = "0";
            adhoclines2[14] = "    ";
            print.AdhocLines2Font[15] = new Font("Tahoma", 10, FontStyle.Regular, GraphicsUnit.Point);
            print.AdhocLines2Margin[15] = "0,300,610";
            adhoclines2[15] = "CHECKED BY?DELIVERED BY?AUTH. SIGNATORY";
            print.Adhoc2Lines = adhoclines2;
            print.TitleSpacing = 5.0F;
            DataGridView dgView = new DataGridView();
            dgView.CellBorderStyle = DataGridViewCellBorderStyle.None;
            dgView.RowTemplate.DefaultCellStyle.Padding = new Padding(0, 6, 0, 0);
            dgView.RowTemplate.Height = 100;
            dgView.Columns.Add("SRN", "SR.");
            dgView.Columns["SRN"].Width = 30;
            dgView.Columns["SRN"].HeaderCell.Style.Font = new Font("Tahoma", 9, FontStyle.Bold);
            dgView.Columns.Add("GoodsDescription", "DESCRIPTION");
            dgView.Columns["GoodsDescription"].Width = 250;
            dgView.Columns["GoodsDescription"].HeaderCell.Style.Font = new Font("Tahoma", 9, FontStyle.Bold);
            dgView.Columns.Add("HSNCode", "HSN Code");
            dgView.Columns["HSNCode"].Width = 80;
            dgView.Columns["HSNCode"].HeaderCell.Style.Font = new Font("Tahoma", 9, FontStyle.Bold);
            dgView.Columns.Add("Pcs", "Pcs");
            dgView.Columns["Pcs"].Width = 75;
            dgView.Columns["Pcs"].HeaderCell.Style.Font = new Font("Tahoma", 9, FontStyle.Bold);
            dgView.Columns.Add("cut", "Cut");
            dgView.Columns["cut"].Width = 75;
            dgView.Columns["cut"].HeaderCell.Style.Font = new Font("Tahoma", 9, FontStyle.Bold);
            dgView.Columns.Add("Meters", "Meters");
            dgView.Columns["Meters"].Width = 75;
            dgView.Columns["Meters"].HeaderCell.Style.Font = new Font("Tahoma", 9, FontStyle.Bold);
            dgView.Columns.Add("Rate", "Rate");
            dgView.Columns["Rate"].Width = 75;
            dgView.Columns["Rate"].HeaderCell.Style.Font = new Font("Tahoma", 9, FontStyle.Bold);
            dgView.Columns.Add("Amount", "Amount");
            dgView.Columns["Amount"].Width = 100;
            dgView.Columns["Amount"].HeaderCell.Style.Font = new Font("Tahoma", 9, FontStyle.Bold);
            int rows = saleDataGridView.Rows.Count - 1;
            for (int i = 0; i < rows; i++)
            {
                int numberOfCells = 8;
                int index = dgView.Rows.Add();
                //int m = 0;
                //while (m < numberOfCells)
                //{
                //    dgView.Rows[index].Cells[m].Style.Alignment = DataGridViewContentAlignment.BottomRight;
                //    m++;
                //}
                dgView.Rows[index].Cells[0].Value = i + 1;
                dgView.Rows[index].Cells[1].Value = saleDataGridView.Rows[i].Cells[itemNameGridCombo.Index].EditedFormattedValue;
                dgView.Rows[index].Cells[2].Value = saleDataGridView.Rows[i].Cells[hsncEditBoxGrid.Index].Value;
                dgView.Rows[index].Cells[3].Value = saleDataGridView.Rows[i].Cells[pcsTextBoxGrid.Index].Value;
                dgView.Rows[index].Cells[4].Value = saleDataGridView.Rows[i].Cells[cutEditBoxGrid.Index].Value;
                dgView.Rows[index].Cells[5].Value = saleDataGridView.Rows[i].Cells[qtyEditBoxGid.Index].Value;
                dgView.Rows[index].Cells[6].Value = saleDataGridView.Rows[i].Cells[rateTextBoxGrid.Index].Value;
                dgView.Rows[index].Cells[7].Value = saleDataGridView.Rows[i].Cells[amountTextBoxGrid.Index].Value;

            }
            print.dgv2 = dgView;


            return print;

          

        }
        private void button2_Click(object sender, EventArgs e)
        {
            var salesBill=partyDB.getSalesBillDetail(billNoTextBox.Text);
            if (salesBill == null)
            {
                MessageBox.Show("Sales Bill Doesn't Exists, Please save it before sending mail", "BILL_NOTEXISTS", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            try
            {
                if (!Email.Email.IsConnectedToInternet())
                {
                    MessageBox.Show("You are not connected to internet", "Network_Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;

                }
                var id = partyComboBox["PARTY_ID"].ToString();
                party_details party = partyDetailsList.Where(p =>
                 {
                     long id1 = long.Parse(id);
                     return p.partyid == id1;
                 }).FirstOrDefault();
                string receiverEmail = party.emailid;

                //if (!Email.Email.IsValidEmail(receiverEmail))
                //{
                //    if (DialogResult.Yes == MessageBox.Show("Party Email Address is not valid,Due you want to update or abort email send", "Validation_Error", MessageBoxButtons.YesNo))
                //    {

                //    }
                //    else
                //        return;
                //}
                DGVPrinter print = printPage();
                string directory = Directory.GetCurrentDirectory();
                string time = (DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds.ToString();
                string fileName = directory + "\\" + partyComboBox.Text + "_" + billNoTextBox.Text+"_"+time + ".pdf";                
                string body = "Hi " + partyComboBox.Text + ",\r\n\r\n" + "Please find attached generated sales bill from " + companyName + "\r\n\r\n\r\n\r\nThanks & regards,\r\n" + companyName;
                Texo_Advance.Email.Email email = new Texo_Advance.Email.Email(print, "kawararchit@gmail.com", "textoadvance@gmail.com", "Texto2620", body, fileName, salesTypeComboBox.Text, "smtp.gmail.com");
                Thread th = new Thread(new ThreadStart(email.threadMail));
                th.Start();
                MessageBox.Show("Email has been queued", "Success", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            catch(ThreadStateException  ex)
                {
                MessageBox.Show(ex.Message, "ThreadStateException", MessageBoxButtons.OK,MessageBoxIcon.Error);
                return;
            }
            catch (OutOfMemoryException ex)
            {
                MessageBox.Show(ex.Message, "OutOfMemoryException", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            catch (Exception ex)
            {
                MessageBox.Show("Email sent Failed", "Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }


        } 
    }
}

                                      