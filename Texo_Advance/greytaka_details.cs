//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Texo_Advance
{
    using System;
    using System.Collections.Generic;
    
    public partial class greytaka_details
    {
        public long takadetails_id { get; set; }
        public Nullable<long> greyPurchaseDetails_id { get; set; }
        public Nullable<long> takaNo { get; set; }
        public string takaMtrs { get; set; }
        public Nullable<double> takaWeight { get; set; }
        public System.DateTime version { get; set; }
    
        public virtual greypurchase_details greypurchase_details { get; set; }
    }
}
