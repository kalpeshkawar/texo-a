/*
SQLyog Community v10.2 
MySQL - 5.7.31-log : Database - majisa sarees_2020-21_08122020
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`majisa sarees_2020-21_08122020` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `majisa sarees_2020-21_08122020`;

/*Table structure for table `account_details` */

DROP TABLE IF EXISTS `account_details`;

CREATE TABLE `account_details` (
  `account_typeid` bigint(20) NOT NULL AUTO_INCREMENT,
  `account_type` varchar(100) NOT NULL,
  `version` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`account_typeid`),
  UNIQUE KEY `account_typeid` (`account_typeid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `account_details` */

/*Table structure for table `account_type` */

DROP TABLE IF EXISTS `account_type`;

CREATE TABLE `account_type` (
  `account_typeid` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) DEFAULT NULL,
  `effect_on` varchar(50) DEFAULT NULL,
  `version` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`account_typeid`),
  UNIQUE KEY `account_typeid` (`account_typeid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `account_type` */

/*Table structure for table `broker_details` */

DROP TABLE IF EXISTS `broker_details`;

CREATE TABLE `broker_details` (
  `brokerid` bigint(20) NOT NULL AUTO_INCREMENT,
  `account_typeid` bigint(20) NOT NULL,
  `party_name` varchar(100) NOT NULL,
  `address` varchar(4000) DEFAULT NULL,
  `address2` varchar(4000) DEFAULT NULL,
  `city_id` bigint(20) DEFAULT NULL,
  `pincode` bigint(20) DEFAULT NULL,
  `distance` bigint(20) DEFAULT NULL,
  `station_name` varchar(100) DEFAULT NULL,
  `transportid` bigint(20) DEFAULT NULL,
  `contactperson` varchar(500) DEFAULT NULL,
  `phone1` bigint(20) DEFAULT NULL,
  `phone2` bigint(20) DEFAULT NULL,
  `emailid` varchar(100) DEFAULT NULL,
  `panno` varchar(20) DEFAULT NULL,
  `gst` varchar(50) DEFAULT NULL,
  `ledger_typeid` bigint(20) DEFAULT NULL,
  `brokerage` double DEFAULT NULL,
  `tds` double DEFAULT NULL,
  `version` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`brokerid`),
  UNIQUE KEY `brokerid` (`brokerid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `broker_details` */

/*Table structure for table `catogery_details` */

DROP TABLE IF EXISTS `catogery_details`;

CREATE TABLE `catogery_details` (
  `catogeryid` bigint(20) NOT NULL AUTO_INCREMENT,
  `catogeryname` varchar(100) NOT NULL,
  `totalPcs` bigint(20) DEFAULT '0',
  `totalMtrs` double DEFAULT '0',
  `version` timestamp NULL DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `financialYear_Id` bigint(20) DEFAULT NULL,
  `rate` double DEFAULT NULL,
  `totalValue` double DEFAULT NULL,
  PRIMARY KEY (`catogeryid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `catogery_details` */

/*Table structure for table `checker_details` */

DROP TABLE IF EXISTS `checker_details`;

CREATE TABLE `checker_details` (
  `checkertype_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `checker_name` varchar(100) DEFAULT NULL,
  `version` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`checkertype_id`),
  UNIQUE KEY `checkertype_id` (`checkertype_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `checker_details` */

/*Table structure for table `city_list` */

DROP TABLE IF EXISTS `city_list`;

CREATE TABLE `city_list` (
  `city_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `vrsion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`city_id`),
  UNIQUE KEY `city_id` (`city_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `city_list` */

/*Table structure for table `company_details` */

DROP TABLE IF EXISTS `company_details`;

CREATE TABLE `company_details` (
  `companyId` bigint(20) NOT NULL AUTO_INCREMENT,
  `companyname` varchar(100) NOT NULL,
  `address` varchar(4000) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `pincode` bigint(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `mobile` bigint(20) DEFAULT NULL,
  `fax` bigint(20) DEFAULT NULL,
  `phoneno` bigint(20) DEFAULT NULL,
  `bankno` varchar(100) DEFAULT NULL,
  `properitor` varchar(100) DEFAULT NULL,
  `bankdetails` varchar(500) DEFAULT NULL,
  `description` varchar(4100) DEFAULT NULL,
  `panno` varchar(20) DEFAULT NULL,
  `gstno` varchar(50) DEFAULT NULL,
  `ledgertype_id` bigint(20) DEFAULT NULL,
  `version` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`companyId`),
  UNIQUE KEY `companyId` (`companyId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `company_details` */

/*Table structure for table `creditdebitnote_details` */

DROP TABLE IF EXISTS `creditdebitnote_details`;

CREATE TABLE `creditdebitnote_details` (
  `creditdebitnote_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `party_id` bigint(20) DEFAULT NULL,
  `cdNoteType_id` bigint(20) DEFAULT NULL,
  `voucherNo` bigint(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `noteNo` varchar(100) DEFAULT NULL,
  `accountType_id` bigint(20) DEFAULT NULL,
  `refBillNo` varchar(100) DEFAULT NULL,
  `refBillDate` date DEFAULT NULL,
  `grossAmount` double DEFAULT NULL,
  `cgstPercentage` double DEFAULT NULL,
  `cgstAmount` double DEFAULT NULL,
  `sgstPercentage` double DEFAULT NULL,
  `sgstAmount` double DEFAULT NULL,
  `remark` varchar(2000) DEFAULT NULL,
  `netAmount` double DEFAULT NULL,
  `igstPercentage` double DEFAULT NULL,
  `igstAmount` double DEFAULT NULL,
  `version` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`creditdebitnote_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `creditdebitnote_details` */

insert  into `creditdebitnote_details`(`creditdebitnote_id`,`party_id`,`cdNoteType_id`,`voucherNo`,`date`,`noteNo`,`accountType_id`,`refBillNo`,`refBillDate`,`grossAmount`,`cgstPercentage`,`cgstAmount`,`sgstPercentage`,`sgstAmount`,`remark`,`netAmount`,`igstPercentage`,`igstAmount`,`version`) values (3,44,1,1,'2020-10-01','53',79,'1','2020-09-28',1506,0,0,0,0,'discount',1581,5,75.3,'2020-10-01 11:44:33'),(4,48,4,1,'2020-10-01','96',79,'525','2020-09-24',1685,2.5,42.125,2.5,42.125,'discount',1769,0,0,'2020-10-01 11:07:28');

/*Table structure for table `emailnotification` */

DROP TABLE IF EXISTS `emailnotification`;

CREATE TABLE `emailnotification` (
  `emailnotification_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `status` varchar(50) DEFAULT NULL,
  `comment` varchar(3000) DEFAULT NULL,
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `version` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `party_name` varchar(1000) DEFAULT NULL,
  `billNo` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`emailnotification_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `emailnotification` */

/*Table structure for table `financial_year` */

DROP TABLE IF EXISTS `financial_year`;

CREATE TABLE `financial_year` (
  `company_id` bigint(20) NOT NULL,
  `financialyear_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `financial_year` varchar(100) DEFAULT NULL,
  `db_name` varchar(500) DEFAULT NULL,
  `version` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`financialyear_id`),
  UNIQUE KEY `financialyear_id` (`financialyear_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `financial_year` */

/*Table structure for table `greypurchase_details` */

DROP TABLE IF EXISTS `greypurchase_details`;

CREATE TABLE `greypurchase_details` (
  `greyPurchaseDetails_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `greypurchase_id` bigint(20) DEFAULT NULL,
  `billNo` varchar(100) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `partyid` bigint(20) DEFAULT NULL,
  `gst_type` bigint(20) DEFAULT NULL,
  `orderNo` varchar(100) DEFAULT NULL,
  `city` bigint(20) DEFAULT NULL,
  `gstn` varchar(20) DEFAULT NULL,
  `dueDays` bigint(20) DEFAULT NULL,
  `interest` double DEFAULT NULL,
  `broker_id` bigint(20) DEFAULT NULL,
  `quality` bigint(20) DEFAULT NULL,
  `weight` double DEFAULT NULL,
  `hsnCode` bigint(20) DEFAULT NULL,
  `checker` bigint(20) DEFAULT NULL,
  `transport_id` bigint(20) DEFAULT NULL,
  `taka` bigint(20) DEFAULT NULL,
  `mtrs` double DEFAULT NULL,
  `rate` double DEFAULT NULL,
  `grossAmount` double DEFAULT NULL,
  `discountPercentage` double DEFAULT NULL,
  `discountAmount` double DEFAULT NULL,
  `sgstRate` double DEFAULT NULL,
  `cgstRate` double DEFAULT NULL,
  `igstRate` double DEFAULT NULL,
  `sgstAmount` double DEFAULT NULL,
  `cgstAmount` double DEFAULT NULL,
  `igstAmount` double DEFAULT NULL,
  `foldLess` double DEFAULT NULL,
  `addAny` double DEFAULT NULL,
  `lessAny` double DEFAULT NULL,
  `taxableValue` double DEFAULT NULL,
  `remarks` varchar(5000) DEFAULT NULL,
  `netAmount` double DEFAULT NULL,
  `version` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `greyPurchaseType_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`greyPurchaseDetails_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `greypurchase_details` */

insert  into `greypurchase_details`(`greyPurchaseDetails_id`,`greypurchase_id`,`billNo`,`date`,`partyid`,`gst_type`,`orderNo`,`city`,`gstn`,`dueDays`,`interest`,`broker_id`,`quality`,`weight`,`hsnCode`,`checker`,`transport_id`,`taka`,`mtrs`,`rate`,`grossAmount`,`discountPercentage`,`discountAmount`,`sgstRate`,`cgstRate`,`igstRate`,`sgstAmount`,`cgstAmount`,`igstAmount`,`foldLess`,`addAny`,`lessAny`,`taxableValue`,`remarks`,`netAmount`,`version`,`greyPurchaseType_id`) values (1,1,'1025','2020-09-24',49,2,'0',16,'24ASDFW5268A1ZD',30,0,37,1,6.4,5407,0,7,24,2800,14.51,40628,0,0,2.5,2.5,5,1015.7,1015.7,0,0,0,0,40628,'30 DAYS PAYMENT',41644,'2020-10-07 01:02:44',1),(2,2,'1685','2020-10-12',48,2,'0',16,'24ASDEW5862A1SD',30,1.5,40,1,6.4,5407,0,7,4,520,16.51,8585,0,0,2.5,2.5,5,214.625,214.625,0,0,0,0,8585,'',8800,'2020-10-12 11:17:14',1),(4,1,'1','2020-10-12',48,2,'1685',16,'24ASDEW5862A1SD',0,0,40,1,6.4,5407,0,7,1,140,16.51,2311,0,0,2.5,2.5,5,57.775,57.775,0,0,0,0,2311,'return',2369,'2020-10-12 11:52:56',2);

/*Table structure for table `greyquality_details` */

DROP TABLE IF EXISTS `greyquality_details`;

CREATE TABLE `greyquality_details` (
  `greyquality_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `quality_name` varchar(100) DEFAULT NULL,
  `version` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`greyquality_id`),
  UNIQUE KEY `greyquality_id` (`greyquality_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `greyquality_details` */

/*Table structure for table `greytaka_details` */

DROP TABLE IF EXISTS `greytaka_details`;

CREATE TABLE `greytaka_details` (
  `takadetails_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `greyPurchaseDetails_id` bigint(20) DEFAULT NULL,
  `takaNo` bigint(20) DEFAULT NULL,
  `takaMtrs` varchar(20) DEFAULT NULL,
  `takaWeight` double DEFAULT NULL,
  `version` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`takadetails_id`),
  KEY `greypurchase_id` (`greyPurchaseDetails_id`),
  CONSTRAINT `greytaka_details_ibfk_1` FOREIGN KEY (`greyPurchaseDetails_id`) REFERENCES `greypurchase_details` (`greyPurchaseDetails_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

/*Data for the table `greytaka_details` */

insert  into `greytaka_details`(`takadetails_id`,`greyPurchaseDetails_id`,`takaNo`,`takaMtrs`,`takaWeight`,`version`) values (1,1,1,'101',0,'2020-09-24 09:30:44'),(2,1,2,'101.5',0,'2020-09-24 09:30:44'),(3,1,3,'101.75',0,'2020-09-24 09:30:44'),(4,1,4,'101.5',0,'2020-09-24 09:30:44'),(5,1,5,'102',0,'2020-09-24 09:30:44'),(6,1,6,'103',0,'2020-09-24 09:30:44'),(7,1,7,'104',0,'2020-09-24 09:30:44'),(8,1,8,'105',0,'2020-09-24 09:30:44'),(9,1,9,'106',0,'2020-09-24 09:30:44'),(10,1,10,'107',0,'2020-09-24 09:30:44'),(11,1,11,'108',0,'2020-09-24 09:30:44'),(12,1,12,'109',0,'2020-09-24 09:30:44'),(13,1,13,'110',0,'2020-09-24 09:30:44'),(14,1,14,'111',0,'2020-09-24 09:30:44'),(15,1,15,'111',0,'2020-09-24 09:30:44'),(16,1,16,'111',0,'2020-09-24 09:30:44'),(17,1,17,'120',0,'2020-09-24 09:30:44'),(18,1,18,'121',0,'2020-09-24 09:30:44'),(19,1,19,'125',0,'2020-09-24 09:30:44'),(20,1,20,'121',0,'2020-09-24 09:30:44'),(21,1,21,'150',0,'2020-09-24 09:30:44'),(22,1,22,'150',0,'2020-09-24 09:30:44'),(23,1,23,'150',0,'2020-09-24 09:30:44'),(24,1,24,'100+70',0,'2020-09-24 09:30:44'),(25,2,1,'130',0,'2020-10-12 11:17:14'),(26,2,2,'130',0,'2020-10-12 11:17:14'),(27,2,3,'130',0,'2020-10-12 11:17:14'),(28,2,4,'130',0,'2020-10-12 11:17:14'),(30,4,1,'140',0,'2020-10-12 11:52:56');

/*Table structure for table `gsttype_details` */

DROP TABLE IF EXISTS `gsttype_details`;

CREATE TABLE `gsttype_details` (
  `gsttype_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` varchar(100) DEFAULT NULL,
  `version` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`gsttype_id`),
  UNIQUE KEY `gsttype_id` (`gsttype_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `gsttype_details` */

/*Table structure for table `item_details` */

DROP TABLE IF EXISTS `item_details`;

CREATE TABLE `item_details` (
  `itemid` bigint(20) NOT NULL AUTO_INCREMENT,
  `itemname` varchar(100) NOT NULL,
  `catogeryid` bigint(20) NOT NULL,
  `cut` varchar(20) DEFAULT NULL,
  `packing_id` bigint(20) DEFAULT NULL,
  `unit_id` bigint(20) DEFAULT NULL,
  `greyquality_id` bigint(20) DEFAULT NULL,
  `type_id` bigint(20) DEFAULT NULL,
  `itemtype_id` bigint(20) DEFAULT NULL,
  `selling_rate` varchar(20) DEFAULT NULL,
  `hsncode` varchar(20) DEFAULT NULL,
  `gst` varchar(20) DEFAULT NULL,
  `version` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`itemid`),
  UNIQUE KEY `itemid` (`itemid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `item_details` */

/*Table structure for table `jobprocess_details` */

DROP TABLE IF EXISTS `jobprocess_details`;

CREATE TABLE `jobprocess_details` (
  `jobprocess_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `voucherNo` bigint(20) DEFAULT NULL,
  `dispatchDate` date DEFAULT NULL,
  `party_id` bigint(20) DEFAULT NULL,
  `gstType_id` bigint(20) DEFAULT NULL,
  `challanNo` varchar(50) DEFAULT NULL,
  `broker_id` bigint(20) DEFAULT NULL,
  `workType_id` bigint(20) DEFAULT NULL,
  `transport_id` bigint(20) DEFAULT NULL,
  `lrno` varchar(1000) DEFAULT NULL,
  `billNo` varchar(1000) DEFAULT NULL,
  `remark` varchar(5000) DEFAULT NULL,
  `totalPcs` bigint(20) DEFAULT NULL,
  `totalMtrs` double DEFAULT NULL,
  `discountPercentage` double DEFAULT NULL,
  `discountPerOnAmount` double DEFAULT NULL,
  `discountAmount` double DEFAULT NULL,
  `addLessDetail1` double DEFAULT NULL,
  `addLessDetail2` double DEFAULT NULL,
  `tdsPercentage` double DEFAULT NULL,
  `tdsAmount` double DEFAULT NULL,
  `cgstPercentage` double DEFAULT NULL,
  `sgstPercentage` double DEFAULT NULL,
  `cgstAmount` double DEFAULT NULL,
  `sgstAmount` double DEFAULT NULL,
  `igstPercentage` double DEFAULT NULL,
  `igstAmount` double DEFAULT NULL,
  `caseNoDetail1` double DEFAULT NULL,
  `caseNoDetail2` double DEFAULT NULL,
  `amountOnDispatch` double DEFAULT NULL,
  `grossAmount` double DEFAULT NULL,
  `taxableValue` double DEFAULT NULL,
  `netAmount` double DEFAULT NULL,
  `version` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`jobprocess_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `jobprocess_details` */

/*Table structure for table `jobprocess_itemdetails` */

DROP TABLE IF EXISTS `jobprocess_itemdetails`;

CREATE TABLE `jobprocess_itemdetails` (
  `jobProcessItemDetails_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `challanNo` varchar(50) DEFAULT NULL,
  `dispatchReference_id` bigint(20) DEFAULT NULL,
  `receiptReference_id` bigint(20) DEFAULT NULL,
  `item_id` bigint(20) DEFAULT NULL,
  `dipatchBundles` varchar(5000) DEFAULT NULL,
  `receiptBundles` varchar(5000) DEFAULT NULL,
  `hsnCode` varchar(100) DEFAULT NULL,
  `jobTypeDetails` varchar(1000) DEFAULT NULL,
  `unit` varchar(100) DEFAULT NULL,
  `dispatch_pcs` bigint(20) DEFAULT NULL,
  `dispatch_mtrsQty` double DEFAULT NULL,
  `cut` double DEFAULT NULL,
  `rate` double DEFAULT NULL,
  `dispatchItemAmount` double DEFAULT NULL,
  `plainItem` double DEFAULT NULL,
  `secondItem` double DEFAULT NULL,
  `shortItem` double DEFAULT NULL,
  `freshItem` double DEFAULT NULL,
  `voucherNo` bigint(20) DEFAULT NULL,
  `version` timestamp NULL DEFAULT NULL,
  `jobprocess_id` bigint(20) DEFAULT NULL,
  `party_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`jobProcessItemDetails_id`),
  KEY `jobprocess_itemdetails_ibfk_1` (`jobprocess_id`),
  CONSTRAINT `jobprocess_itemdetails_ibfk_1` FOREIGN KEY (`jobprocess_id`) REFERENCES `jobprocess_details` (`jobprocess_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `jobprocess_itemdetails` */

/*Table structure for table `jobprocessreceipt_itemdetails` */

DROP TABLE IF EXISTS `jobprocessreceipt_itemdetails`;

CREATE TABLE `jobprocessreceipt_itemdetails` (
  `jobWorkReceiptId` bigint(20) NOT NULL AUTO_INCREMENT,
  `jobProcessItemDetails_id` bigint(20) DEFAULT NULL,
  `item_id` bigint(20) DEFAULT NULL,
  `receiptBundles` varchar(100) DEFAULT NULL,
  `hsnCode` varchar(100) DEFAULT NULL,
  `jobTypeDetails` varchar(1000) DEFAULT NULL,
  `unit` varchar(100) DEFAULT NULL,
  `cut` double DEFAULT NULL,
  `rate` double DEFAULT NULL,
  `pcs` double DEFAULT NULL,
  `itemAmount` double DEFAULT NULL,
  `plainItem` double DEFAULT NULL,
  `secondItem` double DEFAULT NULL,
  `shortItem` double DEFAULT NULL,
  `freshItem` double DEFAULT NULL,
  `receiveVoucherNo` bigint(20) DEFAULT NULL,
  `jobprocess_id` bigint(20) DEFAULT NULL,
  `version` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `mtrs` double DEFAULT NULL,
  `recdDate` date DEFAULT NULL,
  `challanNo` varchar(50) DEFAULT NULL,
  `remark` varchar(2000) DEFAULT NULL,
  `lrno` varchar(1000) DEFAULT NULL,
  `transport_id` bigint(20) DEFAULT NULL,
  `isDeleted` tinyint(1) DEFAULT '0',
  `jobReceiveBillDetail_id` bigint(20) DEFAULT NULL,
  `party_id` bigint(20) DEFAULT NULL,
  `multipleJobsOnItem` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`jobWorkReceiptId`),
  KEY `jobProcessItemDetails_id` (`jobProcessItemDetails_id`),
  KEY `jobReceiveBillDetail_id` (`jobReceiveBillDetail_id`),
  CONSTRAINT `jobprocessreceipt_itemdetails_ibfk_1` FOREIGN KEY (`jobProcessItemDetails_id`) REFERENCES `jobprocess_itemdetails` (`jobProcessItemDetails_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `jobprocessreceipt_itemdetails_ibfk_2` FOREIGN KEY (`jobReceiveBillDetail_id`) REFERENCES `jobreceive_billdetail` (`jobReceiveBillDetail_id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `jobprocessreceipt_itemdetails` */

/*Table structure for table `jobreceive_billdetail` */

DROP TABLE IF EXISTS `jobreceive_billdetail`;

CREATE TABLE `jobreceive_billdetail` (
  `jobReceiveBillDetail_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `billVoucherNo` bigint(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `party_id` bigint(20) DEFAULT NULL,
  `gstType_id` bigint(20) DEFAULT NULL,
  `billNo` varchar(1000) DEFAULT NULL,
  `broker_id` bigint(20) DEFAULT NULL,
  `workType_id` bigint(20) DEFAULT NULL,
  `transport_id` bigint(20) DEFAULT NULL,
  `lrno` varchar(1000) DEFAULT NULL,
  `discountPercentage` double DEFAULT NULL,
  `discountOnAmount` double DEFAULT NULL,
  `discountAmount` double DEFAULT NULL,
  `addLessDetail1` double DEFAULT NULL,
  `addLessDetail2` double DEFAULT NULL,
  `tdsPercentage` double DEFAULT NULL,
  `tdsAmount` double DEFAULT NULL,
  `cgstPercentage` double DEFAULT NULL,
  `cgstAmount` double DEFAULT NULL,
  `sgstPercentage` double DEFAULT NULL,
  `sgstAmount` double DEFAULT NULL,
  `igstPercentage` double DEFAULT NULL,
  `igstAmount` double DEFAULT NULL,
  `caseNoDetail1` varchar(1000) DEFAULT NULL,
  `caseNoDetail2` varchar(1000) DEFAULT NULL,
  `grossAmount` double DEFAULT NULL,
  `taxableValue` double DEFAULT NULL,
  `netAmount` double DEFAULT NULL,
  `totalPcs` double DEFAULT NULL,
  `totalMtrs` double DEFAULT NULL,
  `remark` varchar(5000) DEFAULT NULL,
  `version` timestamp NULL DEFAULT NULL,
  `addLessDetail3` double DEFAULT NULL,
  PRIMARY KEY (`jobReceiveBillDetail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `jobreceive_billdetail` */

/*Table structure for table `journalentry_detail` */

DROP TABLE IF EXISTS `journalentry_detail`;

CREATE TABLE `journalentry_detail` (
  `journalEntryDetail_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `voucherNo` bigint(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `creditAccount_id` bigint(20) DEFAULT NULL,
  `debitAccount_id` bigint(20) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `chqRefNo` varchar(100) DEFAULT NULL,
  `creditRemarks` varchar(5000) DEFAULT NULL,
  `debitRemarks` varchar(5000) DEFAULT NULL,
  `version` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`journalEntryDetail_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `journalentry_detail` */

insert  into `journalentry_detail`(`journalEntryDetail_id`,`voucherNo`,`date`,`creditAccount_id`,`debitAccount_id`,`amount`,`chqRefNo`,`creditRemarks`,`debitRemarks`,`version`) values (2,1,'2020-09-25',51,78,11000,'15366oo','SALARY SEPT','SALARY SEPT','2020-09-27 11:13:45');

/*Table structure for table `ledger_type` */

DROP TABLE IF EXISTS `ledger_type`;

CREATE TABLE `ledger_type` (
  `ledger_typeid` bigint(20) NOT NULL,
  `type` varchar(50) DEFAULT NULL,
  `version` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ledger_typeid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `ledger_type` */

/*Table structure for table `millchallandispatchtaka_detail` */

DROP TABLE IF EXISTS `millchallandispatchtaka_detail`;

CREATE TABLE `millchallandispatchtaka_detail` (
  `marka` varchar(500) DEFAULT NULL,
  `lotNo` varchar(500) DEFAULT NULL,
  `millTaka_id` bigint(50) NOT NULL AUTO_INCREMENT,
  `greyMtrs` varchar(30) DEFAULT NULL,
  `version` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `finMtrs` varchar(30) DEFAULT NULL,
  `isReceived` tinyint(1) DEFAULT NULL,
  `challan_id` bigint(20) DEFAULT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `rate` double DEFAULT NULL,
  `millvoucher_id` bigint(20) DEFAULT NULL,
  `jobAmount` double DEFAULT NULL,
  `reProcessTaka` bigint(1) DEFAULT '0',
  PRIMARY KEY (`millTaka_id`),
  KEY `challan_id` (`challan_id`),
  KEY `millvoucher_id` (`millvoucher_id`),
  CONSTRAINT `millchallandispatchtaka_detail_ibfk_1` FOREIGN KEY (`challan_id`) REFERENCES `milldispatchchallan_detail` (`challan_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `millchallandispatchtaka_detail_ibfk_2` FOREIGN KEY (`millvoucher_id`) REFERENCES `millreceipt_detail` (`millvoucher_id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

/*Data for the table `millchallandispatchtaka_detail` */

insert  into `millchallandispatchtaka_detail`(`marka`,`lotNo`,`millTaka_id`,`greyMtrs`,`version`,`finMtrs`,`isReceived`,`challan_id`,`isDeleted`,`rate`,`millvoucher_id`,`jobAmount`,`reProcessTaka`) values ('','56953',1,'101','2020-10-08 10:15:21','70+25',1,1,0,13.75,1,5170,1),('','56953',2,'102','2020-10-08 10:15:21','92',1,1,0,13.75,1,5170,1),('','56953',3,'103','2020-10-08 10:15:21','95',1,1,0,13.75,1,5170,1),('','56953',4,'104','2020-10-08 10:15:21','94',1,1,0,13.75,1,5170,1),('','56953',5,'105','2020-10-08 09:04:16','0',0,1,0,13.75,1,5170,0),('','56953',6,'106','2020-10-08 09:04:16','0',0,1,0,13.75,1,5170,0),('','56953',7,'107','2020-10-08 09:04:16','0',0,1,0,13.75,1,5170,0),('','56953',8,'108','2020-10-08 09:04:16','0',0,1,0,13.75,1,5170,0),('','56953',9,'109','2020-10-08 09:04:16','0',0,1,0,13.75,1,5170,0),('','56953',10,'110','2020-10-08 09:04:16','0',0,1,0,13.75,1,5170,0),('','56953',11,'111','2020-10-08 09:04:16','0',0,1,0,13.75,1,5170,0),('','56953',12,'112','2020-10-08 09:04:16','0',0,1,0,13.75,1,5170,0);

/*Table structure for table `milldispatch_detail` */

DROP TABLE IF EXISTS `milldispatch_detail`;

CREATE TABLE `milldispatch_detail` (
  `voucherNo` bigint(20) NOT NULL,
  `date` date DEFAULT NULL,
  `greypurchase_id` bigint(20) DEFAULT NULL,
  `totalTaka` bigint(20) DEFAULT NULL,
  `totalMtrs` double DEFAULT NULL,
  `version` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`voucherNo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `milldispatch_detail` */

insert  into `milldispatch_detail`(`voucherNo`,`date`,`greypurchase_id`,`totalTaka`,`totalMtrs`,`version`) values (1,'2020-09-24',NULL,12,1278,'2020-09-24 09:32:52');

/*Table structure for table `milldispatchchallan_detail` */

DROP TABLE IF EXISTS `milldispatchchallan_detail`;

CREATE TABLE `milldispatchchallan_detail` (
  `challanNo` varchar(50) NOT NULL,
  `voucherNo` bigint(20) DEFAULT NULL,
  `mill_id` bigint(20) DEFAULT NULL,
  `station_id` bigint(20) DEFAULT NULL,
  `mill_gstn` varchar(50) DEFAULT NULL,
  `processType` bigint(20) DEFAULT NULL,
  `totalTakas` bigint(20) DEFAULT NULL,
  `totalMtrs` double DEFAULT NULL,
  `master` varchar(100) DEFAULT NULL,
  `rate` double DEFAULT NULL,
  `cardNo` varchar(50) DEFAULT NULL,
  `version` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `remark` varchar(2000) DEFAULT NULL,
  `quality_id` bigint(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `reference_billNo` varchar(20) DEFAULT NULL,
  `reference_challanNo` varchar(50) DEFAULT NULL,
  `challan_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `receiptVoucherNo` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`challan_id`),
  KEY `voucherNo` (`voucherNo`),
  CONSTRAINT `milldispatchchallan_detail_ibfk_1` FOREIGN KEY (`voucherNo`) REFERENCES `milldispatch_detail` (`voucherNo`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `milldispatchchallan_detail` */

insert  into `milldispatchchallan_detail`(`challanNo`,`voucherNo`,`mill_id`,`station_id`,`mill_gstn`,`processType`,`totalTakas`,`totalMtrs`,`master`,`rate`,`cardNo`,`version`,`remark`,`quality_id`,`date`,`reference_billNo`,`reference_challanNo`,`challan_id`,`isDeleted`,`receiptVoucherNo`) values ('1',1,50,16,'24ASZAA4521S1ZD',1,12,1278,'',13.75,'1','2020-09-24 09:32:53','DNO 5025',1,'2020-09-24','1',NULL,1,0,NULL);

/*Table structure for table `millprocess_type` */

DROP TABLE IF EXISTS `millprocess_type`;

CREATE TABLE `millprocess_type` (
  `millProcess_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` varchar(100) DEFAULT NULL,
  `version` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`millProcess_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `millprocess_type` */

insert  into `millprocess_type`(`millProcess_id`,`type`,`version`) values (1,'PROCESS(GREY DISPATCH)','2020-08-16 08:03:50'),(2,'REPROCESS(BLEACH/SEMI FINISH)','2020-08-16 08:04:38'),(3,'R/F(FINISH GOODS)','2020-08-16 08:04:59');

/*Table structure for table `millreceipt_detail` */

DROP TABLE IF EXISTS `millreceipt_detail`;

CREATE TABLE `millreceipt_detail` (
  `millvoucher_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `receiptVoucherNo` bigint(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `mill_id` bigint(20) DEFAULT NULL,
  `hsnCode` varchar(100) DEFAULT NULL,
  `remark` varchar(5000) DEFAULT NULL,
  `recdTaka` bigint(20) DEFAULT NULL,
  `recdMtrs` double DEFAULT NULL,
  `greyMtrs` double DEFAULT NULL,
  `disountPercentage` double DEFAULT NULL,
  `discountAmount` double DEFAULT NULL,
  `rdPerMtr` double DEFAULT NULL,
  `rdAmount` double DEFAULT NULL,
  `tdsPercentage` double DEFAULT NULL,
  `tdsAmount` double DEFAULT NULL,
  `cgstPercentage` double DEFAULT NULL,
  `sgstPercentage` double DEFAULT NULL,
  `igstPercentage` double DEFAULT NULL,
  `cgstAmount` double DEFAULT NULL,
  `sgstAmount` double DEFAULT NULL,
  `igstAmount` double DEFAULT NULL,
  `grossAmount` double DEFAULT NULL,
  `addLessAny` double DEFAULT NULL,
  `taxableValue` double DEFAULT NULL,
  `invoiceValue` double DEFAULT NULL,
  `netAmountAfterTds` double DEFAULT NULL,
  `billNo` varchar(100) DEFAULT NULL,
  `version` timestamp NULL DEFAULT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `challanNo` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`millvoucher_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `millreceipt_detail` */

insert  into `millreceipt_detail`(`millvoucher_id`,`receiptVoucherNo`,`date`,`mill_id`,`hsnCode`,`remark`,`recdTaka`,`recdMtrs`,`greyMtrs`,`disountPercentage`,`discountAmount`,`rdPerMtr`,`rdAmount`,`tdsPercentage`,`tdsAmount`,`cgstPercentage`,`sgstPercentage`,`igstPercentage`,`cgstAmount`,`sgstAmount`,`igstAmount`,`grossAmount`,`addLessAny`,`taxableValue`,`invoiceValue`,`netAmountAfterTds`,`billNo`,`version`,`isDeleted`,`challanNo`) values (1,1,'2020-09-24',50,'14223552','DNO 5025',5,376,410,3,155.1,0,0,1.5,75.22,2.5,2.5,0,125.37,125.37,0,5170,0,5014.9,5265.64,5190,'G/15333',NULL,0,NULL);

/*Table structure for table `party_details` */

DROP TABLE IF EXISTS `party_details`;

CREATE TABLE `party_details` (
  `partyid` bigint(20) NOT NULL AUTO_INCREMENT,
  `account_typeid` bigint(20) NOT NULL,
  `party_name` varchar(100) NOT NULL,
  `address` varchar(4000) DEFAULT NULL,
  `address2` varchar(4000) DEFAULT NULL,
  `city_id` bigint(20) DEFAULT NULL,
  `pincode` bigint(20) DEFAULT NULL,
  `distance` varchar(500) DEFAULT NULL,
  `brokerid` bigint(20) DEFAULT NULL,
  `station_name` varchar(100) DEFAULT NULL,
  `transportid` bigint(20) DEFAULT NULL,
  `contactperson` varchar(500) DEFAULT NULL,
  `phone1` bigint(20) DEFAULT NULL,
  `phone2` bigint(20) DEFAULT NULL,
  `emailid` varchar(100) DEFAULT NULL,
  `panno` varchar(20) DEFAULT NULL,
  `gst` varchar(50) DEFAULT NULL,
  `ledger_typeid` bigint(20) DEFAULT NULL,
  `tds` double DEFAULT NULL,
  `version` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`partyid`),
  KEY `transportid` (`transportid`),
  KEY `city_id` (`city_id`),
  KEY `brokerid` (`brokerid`),
  CONSTRAINT `party_details_ibfk_1` FOREIGN KEY (`transportid`) REFERENCES `transport_details` (`transportid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `party_details_ibfk_2` FOREIGN KEY (`city_id`) REFERENCES `city_list` (`city_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `party_details_ibfk_3` FOREIGN KEY (`brokerid`) REFERENCES `broker_details` (`brokerid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `party_details` */

/*Table structure for table `passbook_details` */

DROP TABLE IF EXISTS `passbook_details`;

CREATE TABLE `passbook_details` (
  `passBookDetails_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `voucherNo` bigint(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `bankCashAccount_id` bigint(20) DEFAULT NULL,
  `chqNo` varchar(500) DEFAULT NULL,
  `chqDate` date DEFAULT NULL,
  `draweeBank_id` bigint(20) DEFAULT NULL,
  `remark` varchar(5000) DEFAULT NULL,
  `party_id` bigint(20) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `accountType_id` bigint(20) DEFAULT NULL,
  `passbooktype_id` bigint(20) DEFAULT NULL,
  `paymentMode` bigint(20) DEFAULT '0',
  `version` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`passBookDetails_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `passbook_details` */

insert  into `passbook_details`(`passBookDetails_id`,`voucherNo`,`date`,`bankCashAccount_id`,`chqNo`,`chqDate`,`draweeBank_id`,`remark`,`party_id`,`amount`,`accountType_id`,`passbooktype_id`,`paymentMode`,`version`) values (1,1,'2020-09-27',47,'','2020-09-27',0,'SALARY',51,10000,40,2,1,'2020-09-27 12:17:38');

/*Table structure for table `passbook_itemdetails` */

DROP TABLE IF EXISTS `passbook_itemdetails`;

CREATE TABLE `passbook_itemdetails` (
  `passbooItemDetails_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `passBookDetails_id` bigint(20) DEFAULT NULL,
  `tdsAmount` double DEFAULT NULL,
  `days` double DEFAULT NULL,
  `rdAmount` double DEFAULT NULL,
  `discountPercentage` double DEFAULT NULL,
  `discountAmount` double DEFAULT NULL,
  `commPercentage` double DEFAULT NULL,
  `commAmount` double DEFAULT NULL,
  `rgAmount` double DEFAULT NULL,
  `addLess` double DEFAULT NULL,
  `others` double DEFAULT NULL,
  `paymentReceivedAmount` double DEFAULT NULL,
  `pendingAmount` double DEFAULT NULL,
  `billPaid` tinyint(1) DEFAULT '0',
  `version` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `billNo` varchar(100) DEFAULT NULL,
  `totalReceivedAmount` double DEFAULT NULL,
  PRIMARY KEY (`passbooItemDetails_id`),
  KEY `passbook_itemdetails_ibfk_1` (`passBookDetails_id`),
  CONSTRAINT `passbook_itemdetails_ibfk_1` FOREIGN KEY (`passBookDetails_id`) REFERENCES `passbook_details` (`passBookDetails_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `passbook_itemdetails` */

/*Table structure for table `printersettings_details` */

DROP TABLE IF EXISTS `printersettings_details`;

CREATE TABLE `printersettings_details` (
  `printerSettingsDetails_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` varchar(1000) DEFAULT NULL,
  `value` varchar(5000) DEFAULT NULL,
  `version` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`printerSettingsDetails_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `printersettings_details` */

insert  into `printersettings_details`(`printerSettingsDetails_id`,`type`,`value`,`version`) values (1,'tagline','MFG. EXCLUSIV FANCY SAREES',NULL),(2,'billerheaading','!! Shri Ganeshaya Namah !!',NULL),(3,'termsandconditions','SUBJECT TO SURAT JURISDICTION;GOODS HAVE BEEN SOLD & DESPATCHED AT THE ENTIRE RISK OF PURCHASER.;\n COMPLAINTS, IF ANY REGARDING THIS INVOICE MUST BE INFORMED IN WRITING WITHIN 48 HOURS',NULL),(4,'formattedprint','true',NULL);

/*Table structure for table `purchase_details` */

DROP TABLE IF EXISTS `purchase_details`;

CREATE TABLE `purchase_details` (
  `purchasebillid` bigint(20) NOT NULL AUTO_INCREMENT,
  `purchase_typeid` bigint(20) DEFAULT NULL,
  `billno` varchar(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `partyid` bigint(20) DEFAULT NULL,
  `stationcode` bigint(20) DEFAULT NULL,
  `purchase_gsttypeid` bigint(20) DEFAULT NULL,
  `partygst` varchar(50) DEFAULT NULL,
  `brokerid` bigint(20) DEFAULT NULL,
  `transportid` bigint(20) DEFAULT NULL,
  `lrno` varchar(100) DEFAULT NULL,
  `remark` varchar(5000) DEFAULT NULL,
  `discount` varchar(20) DEFAULT '0',
  `additional` varchar(20) DEFAULT '0',
  `caseno` varchar(100) DEFAULT NULL,
  `cgst` double DEFAULT '0',
  `sgst` double DEFAULT '0',
  `igst` double DEFAULT '0',
  `taxablevalue` double DEFAULT NULL,
  `billamount` double DEFAULT NULL,
  `gsttypeid` bigint(20) DEFAULT NULL,
  `discountPercentage` double DEFAULT NULL,
  `discountOnAmount` double DEFAULT NULL,
  `addLessDetail1` double DEFAULT NULL,
  `addLessDetail2` double DEFAULT NULL,
  `cgstRate` double DEFAULT NULL,
  `sgstRate` double DEFAULT NULL,
  `igstRate` double DEFAULT NULL,
  `grossAmount` double DEFAULT NULL,
  `caseNoDetail2` varchar(50) DEFAULT NULL,
  `orderno` varchar(20) DEFAULT NULL,
  `duedays` bigint(20) DEFAULT NULL,
  `station` bigint(20) DEFAULT NULL,
  `version` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `voucherNo` bigint(20) DEFAULT NULL,
  `refBillNo` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`purchasebillid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `purchase_details` */

insert  into `purchase_details`(`purchasebillid`,`purchase_typeid`,`billno`,`date`,`partyid`,`stationcode`,`purchase_gsttypeid`,`partygst`,`brokerid`,`transportid`,`lrno`,`remark`,`discount`,`additional`,`caseno`,`cgst`,`sgst`,`igst`,`taxablevalue`,`billamount`,`gsttypeid`,`discountPercentage`,`discountOnAmount`,`addLessDetail1`,`addLessDetail2`,`cgstRate`,`sgstRate`,`igstRate`,`grossAmount`,`caseNoDetail2`,`orderno`,`duedays`,`station`,`version`,`voucherNo`,`refBillNo`) values (1,1,'525','2020-09-24',48,24,2,'24ASDEW5862A1SD',40,7,'','30 DAYS PAYMENT','576','0','',465.6,465.6,0,18624,19555,NULL,3,19200,0,0,2.5,2.5,0,19200,'1',NULL,30,16,'2020-09-24 02:43:41',1,NULL),(2,6,'526','2020-10-01',48,24,2,'24ASDEW5862A1SD',40,7,'','','0','0','',15,15,0,600,630,NULL,0,0,0,0,2.5,2.5,5,600,'1',NULL,0,16,'2020-10-01 11:46:46',1,'525');

/*Table structure for table `purchaseitem_details` */

DROP TABLE IF EXISTS `purchaseitem_details`;

CREATE TABLE `purchaseitem_details` (
  `item_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `purchasebillid` bigint(20) DEFAULT NULL,
  `bundles` varchar(100) DEFAULT NULL,
  `hsncode` varchar(100) DEFAULT NULL,
  `unit` varchar(20) DEFAULT NULL,
  `cut` double DEFAULT NULL,
  `quantity` double DEFAULT NULL,
  `rate` double DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `purchaseitem_id` bigint(20) DEFAULT NULL,
  `version` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `pcs` bigint(20) DEFAULT NULL,
  `packing` varchar(10) DEFAULT NULL,
  `cgstRate` double DEFAULT '0',
  `cgstAmount` double DEFAULT '0',
  `sgstRate` double DEFAULT '0',
  `sgstAmount` double DEFAULT '0',
  `igstRate` double DEFAULT '0',
  `igstAmount` double DEFAULT '0',
  `rdAmount` double DEFAULT '0',
  `discount` double DEFAULT '0',
  `taxableValue` double DEFAULT '0',
  `totalAmount` double DEFAULT '0',
  `addLess` double DEFAULT '0',
  PRIMARY KEY (`item_id`),
  KEY `purchaseitem_details_ibfk_1` (`purchasebillid`),
  CONSTRAINT `purchaseitem_details_ibfk_1` FOREIGN KEY (`purchasebillid`) REFERENCES `purchase_details` (`purchasebillid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `purchaseitem_details` */

insert  into `purchaseitem_details`(`item_id`,`purchasebillid`,`bundles`,`hsncode`,`unit`,`cut`,`quantity`,`rate`,`amount`,`purchaseitem_id`,`version`,`pcs`,`packing`,`cgstRate`,`cgstAmount`,`sgstRate`,`sgstAmount`,`igstRate`,`igstAmount`,`rdAmount`,`discount`,`taxableValue`,`totalAmount`,`addLess`) values (1,1,'12*4','5407','PCS',6.3,302,400,19200,22,'2020-09-24 02:43:41',48,'BEG',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2,2,'2*1','5407','PCS',6.3,13,300,600,22,'2020-10-01 11:46:46',2,'BEG',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `purchasetype_details` */

DROP TABLE IF EXISTS `purchasetype_details`;

CREATE TABLE `purchasetype_details` (
  `purchasetype_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` varchar(100) DEFAULT NULL,
  `version` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`purchasetype_id`),
  UNIQUE KEY `purchasetype_id` (`purchasetype_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `purchasetype_details` */

/*Table structure for table `sales_details` */

DROP TABLE IF EXISTS `sales_details`;

CREATE TABLE `sales_details` (
  `discountPercentage` double DEFAULT NULL,
  `discountOnAmount` double DEFAULT NULL,
  `addLessDetail1` double DEFAULT NULL,
  `addLessDetail2` double DEFAULT NULL,
  `cgstRate` double DEFAULT NULL,
  `sgstRate` double DEFAULT NULL,
  `igstRate` double DEFAULT NULL,
  `grossAmount` double DEFAULT NULL,
  `caseNoDetail2` varchar(50) DEFAULT NULL,
  `partyid` bigint(20) NOT NULL,
  `salesTypeid` bigint(20) NOT NULL,
  `billno` varchar(20) DEFAULT NULL,
  `orderno` varchar(100) DEFAULT NULL,
  `statecode` bigint(20) DEFAULT NULL,
  `gstTypeid` bigint(20) DEFAULT NULL,
  `station` bigint(20) DEFAULT NULL,
  `partygst` varchar(50) DEFAULT NULL,
  `brokerid` bigint(20) DEFAULT NULL,
  `transportid` bigint(20) DEFAULT NULL,
  `lrno` varchar(100) DEFAULT NULL,
  `remark` varchar(4000) DEFAULT NULL,
  `discount` varchar(20) DEFAULT NULL,
  `additional` varchar(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `caseno` varchar(20) DEFAULT NULL,
  `cgst` double DEFAULT NULL,
  `sgst` double DEFAULT NULL,
  `igst` double DEFAULT NULL,
  `taxablevalue` double NOT NULL,
  `billamount` double NOT NULL,
  `salesbillid` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `duedays` bigint(20) DEFAULT NULL,
  `voucherNo` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`salesbillid`),
  KEY `brokerid` (`brokerid`),
  KEY `transportid` (`transportid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `sales_details` */

insert  into `sales_details`(`discountPercentage`,`discountOnAmount`,`addLessDetail1`,`addLessDetail2`,`cgstRate`,`sgstRate`,`igstRate`,`grossAmount`,`caseNoDetail2`,`partyid`,`salesTypeid`,`billno`,`orderno`,`statecode`,`gstTypeid`,`station`,`partygst`,`brokerid`,`transportid`,`lrno`,`remark`,`discount`,`additional`,`date`,`caseno`,`cgst`,`sgst`,`igst`,`taxablevalue`,`billamount`,`salesbillid`,`version`,`duedays`,`voucherNo`) values (3,23952,0,0,0,0,5,1402446,'1',44,1,'1','0',32,1,32,'33ASGUH5945A1ZD',39,8,'1526652','','718.56','0','2020-09-28','1',0,0,70086.372,1401727.44,1471814,1,'2020-10-10 22:22:04',30,1),(0,0,0,0,0,0,5,2970,'1',44,5,'1','1',32,1,32,'33ASGUH5945A1ZD',39,8,'','','0','0','2020-10-01','',0,0,148.5,2970,3118,2,'2020-10-01 09:54:12',0,1);

/*Table structure for table `sales_row_details` */

DROP TABLE IF EXISTS `sales_row_details`;

CREATE TABLE `sales_row_details` (
  `salesrowid` bigint(20) NOT NULL AUTO_INCREMENT,
  `itemid` bigint(20) DEFAULT NULL,
  `bundles` varchar(100) DEFAULT NULL,
  `hsncode` varchar(100) DEFAULT NULL,
  `packing` varchar(100) DEFAULT NULL,
  `unit` varchar(100) DEFAULT NULL,
  `pcs` bigint(20) DEFAULT NULL,
  `cut` varchar(100) DEFAULT NULL,
  `quantity` double DEFAULT NULL,
  `rate` double DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `version` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`salesrowid`),
  UNIQUE KEY `salesrowid` (`salesrowid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sales_row_details` */

/*Table structure for table `sales_row_mapping` */

DROP TABLE IF EXISTS `sales_row_mapping`;

CREATE TABLE `sales_row_mapping` (
  `salesrowid` bigint(20) NOT NULL,
  `salesid` bigint(20) NOT NULL,
  `mappingid` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`mappingid`),
  UNIQUE KEY `mappingid` (`mappingid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sales_row_mapping` */

/*Table structure for table `sales_type` */

DROP TABLE IF EXISTS `sales_type`;

CREATE TABLE `sales_type` (
  `salesTypeid` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) DEFAULT NULL,
  `version` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`salesTypeid`),
  UNIQUE KEY `salesTypeid` (`salesTypeid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sales_type` */

/*Table structure for table `salesitem_details` */

DROP TABLE IF EXISTS `salesitem_details`;

CREATE TABLE `salesitem_details` (
  `item_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `salesbillid` bigint(20) DEFAULT NULL,
  `bundles` varchar(100) DEFAULT NULL,
  `hsncode` varchar(100) DEFAULT NULL,
  `packing` varchar(10) DEFAULT NULL,
  `unit` varchar(50) DEFAULT NULL,
  `cut` double DEFAULT NULL,
  `pcs` bigint(20) DEFAULT NULL,
  `quantity` double DEFAULT NULL,
  `rate` double DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `version` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `salesitem_id` bigint(20) NOT NULL,
  PRIMARY KEY (`item_id`),
  UNIQUE KEY `item_id` (`item_id`),
  KEY `salesbillid` (`salesbillid`),
  CONSTRAINT `salesitem_details_ibfk_1` FOREIGN KEY (`salesbillid`) REFERENCES `sales_details` (`salesbillid`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `salesitem_details` */

insert  into `salesitem_details`(`item_id`,`salesbillid`,`bundles`,`hsncode`,`packing`,`unit`,`cut`,`pcs`,`quantity`,`rate`,`amount`,`version`,`salesitem_id`) values (1,1,'12*4','5407','BOX','PCS',6.3,48,302,499,23952,'2020-10-10 22:22:04',21),(2,2,'6*1','5407','BEG','PCS',6.3,6,38,495,2970,'2020-10-01 09:54:12',22),(3,1,'12','555','BEG','PCS',500,12,6000,555,6660,'2020-10-10 22:22:04',22),(4,1,'12*9','555','BEG','PCS',55,108,5940,55,5940,'2020-10-10 22:22:04',21),(5,1,'1222','222','BEG','PCS',222,1222,271284,22,26884,'2020-10-10 22:22:04',22),(6,1,'458','555','BEG','PCS',555,458,254190,555,254190,'2020-10-10 22:22:04',21),(7,1,'58*9','552','BEG','PCS',55,55,3025,55,3025,'2020-10-10 22:22:04',22),(8,1,'5555','5555','BEG','PCS',555,5555,3083025,55,305525,'2020-10-10 22:22:04',21),(9,1,'5555','5555','BEG','PCS',55,5555,305525,55,305525,'2020-10-10 22:22:04',22),(10,1,'8559','555','BEG','PCS',55,8559,470745,55,470745,'2020-10-10 22:22:04',21);

/*Table structure for table `statecode_details` */

DROP TABLE IF EXISTS `statecode_details`;

CREATE TABLE `statecode_details` (
  `statecode_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `code` varchar(20) DEFAULT NULL,
  `version` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`statecode_id`),
  UNIQUE KEY `statecode_id` (`statecode_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `statecode_details` */

/*Table structure for table `transport_details` */

DROP TABLE IF EXISTS `transport_details`;

CREATE TABLE `transport_details` (
  `transportid` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `gstn` varchar(100) DEFAULT NULL,
  `version` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`transportid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `transport_details` */

/*Table structure for table `userdetails` */

DROP TABLE IF EXISTS `userdetails`;

CREATE TABLE `userdetails` (
  `username` varchar(100) NOT NULL,
  `password` varchar(100) DEFAULT NULL,
  `usergroup` varchar(100) DEFAULT NULL,
  `version` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `userdetails` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
