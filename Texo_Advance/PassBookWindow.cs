﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Texo_Advance.DBItems;
using System.Globalization;
using Texo_Advance.CustomControl;
using System.Reflection;
using System.Threading;

namespace Texo_Advance
{
    public partial class PassBookWindow : UserControl
    {
        string strCompanyName = "";
        string strCompanyGstn = "";
        MasterDBData DB = new MasterDBData();
        PartyDBData partyDB = new PartyDBData();
        int iPassBookMode = 0;//0-Bank Receipt 1-Bank Payment 2-Cash Receipt 3-Cash Payment
        List<PassBookBillDataForMultiColumnDisplay> passBookMultiColunDisplayDataList = new List<PassBookBillDataForMultiColumnDisplay>();
        List<double> dPendingAmountList = new List<double>();
        passbook_details passBookDetailData = new passbook_details();
        public PassBookWindow(string companyName,string companyGstn)
        {
            InitializeComponent();
            strCompanyName = companyName;
            strCompanyGstn = companyGstn;
        }

        private void PassBookWindow_Load(object sender, EventArgs e)
        {
            companyNameTextBox.Text = strCompanyName;
            fillPassBookTypeDetails();
            fillPartyAccountList();
            dateTextBox.Text = DateTime.Today.ToString("dd-MM-yyyy");
            chqDateTextBox.Text = DateTime.Today.ToString("dd-MM-yyyy");
            fillBankCashAccounts();
            fillDraweeBankDetails();
            bankCashComboBox.SelectedIndex = -1;
            //accountNameComboBox.SelectedIndex = -1;
            draweeBankComboBox.SelectedIndex = -1;
            chqRadioButton.Checked = true;
            addNewButton.Enabled = true;
            viewButton.Enabled = true;
            updateButton.Enabled = false;
            deleteButton.Enabled = false;
        }

        private void fillPassBookTypeDetails()
        {
            List<passbooktype_details> passBookTypeList = DB.getPassBookTypeDetails();
            if(passBookTypeList != null)
            {
                typeComboBox.DisplayMember = "type";
                typeComboBox.ValueMember = "passbooktype_id";
                typeComboBox.DataSource = passBookTypeList;
            }
        }

        private void dateTextBox_Validating(object sender, CancelEventArgs e)
        {
            DateTime dt;
            bool isValid = DateTime.TryParseExact(dateTextBox.Text, "dd-MM-yyyy", null, DateTimeStyles.None, out dt);
            if (!isValid)
            {
                MessageBox.Show("Please enter the valid date", "DATE", MessageBoxButtons.OK);
                dateTextBox.Focus();
            }
        }

        private void chqDateTextBox_Validating(object sender, CancelEventArgs e)
        {
            DateTime dt;
            bool isValid = DateTime.TryParseExact(chqDateTextBox.Text, "dd-MM-yyyy", null, DateTimeStyles.None, out dt);
            if (!isValid)
            {
                MessageBox.Show("Please enter the valid date", "CHQ DATE", MessageBoxButtons.OK);
                dateTextBox.Focus();
            }
            else
            {
                for(int i = 0;i < passbookDataGridView.RowCount-1;i++)
                {
                    DateTime billDate = DateTime.ParseExact(passbookDataGridView.Rows[i].Cells[dateGridTextBox.Index].EditedFormattedValue.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    if (billDate != null)
                    {
                        TimeSpan differenceDays = DateTime.ParseExact(chqDateTextBox.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture) - billDate;
                        passbookDataGridView.Rows[i].Cells[daysGridTextBox.Index].Value = differenceDays.Days;
                    }
                    
                }
                
            }
        }

        private void bankCashComboBox_KeyDown(object sender, KeyEventArgs e)
        {
            int iSelectIndx = -1;
            if (e.KeyCode == Keys.Enter)
            {
                iSelectIndx = bankCashComboBox.FindString(bankCashComboBox.Text);
                if (iSelectIndx == -1)
                {
                    if (iPassBookMode == 0 || iPassBookMode == 1)
                    {
                        if (MessageBox.Show("Do You want to Bank Account?", "ADD ACCOUNT", MessageBoxButtons.YesNo) == DialogResult.Yes)
                        {
                            AccountManagerForm acc = new AccountManagerForm();
                            acc.setDefaultAccountType(6);
                            acc.ShowDialog();
                            fillBankCashAccounts();
                            //DB.addCity(stationComboBox.Text);
                            //GetCityData();
                        }
                    }
                    else if (iPassBookMode == 2 || iPassBookMode == 3)
                    {
                        if (MessageBox.Show("Do You want to Cash Account?", "ADD ACCOUNT", MessageBoxButtons.YesNo) == DialogResult.Yes)
                        {
                            AccountManagerForm acc = new AccountManagerForm();
                            acc.setDefaultAccountType(18);
                            acc.ShowDialog();
                            fillBankCashAccounts();
                            //DB.addCity(stationComboBox.Text);
                            //GetCityData();
                        }
                    }

                }
                else
                {
                    bankCashComboBox.SelectedIndex = iSelectIndx;
                }
            }
        }

        private void typeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (typeComboBox.SelectedIndex == 0)
            {
                iPassBookMode = 0;
                updateControlsForPassBookMode(0);
                resetAllData();
                setDefaultVoucherNo();
            }
            else if (typeComboBox.SelectedIndex == 1)
            {
                iPassBookMode = 1;
                updateControlsForPassBookMode(1);
                resetAllData();
                setDefaultVoucherNo();
            }
            else if (typeComboBox.SelectedIndex == 2)
            {
                iPassBookMode = 2;
                updateControlsForPassBookMode(2);
                resetAllData();
                setDefaultVoucherNo();
            }
            else if (typeComboBox.SelectedIndex == 3)
            {
                iPassBookMode = 3;
                updateControlsForPassBookMode(3);
                resetAllData();
                setDefaultVoucherNo();
            }


        }

        private void updateControlsForPassBookMode(int iMode)
        {
            if(iMode == 0)
            {
                billNoTextBox.Show();
                label6.Show();
                label5.Text = "BANK:";
                chqNoTextBox.Show();
                label10.Show();
                label14.Show();
                label11.Show();
                chqDateTextBox.Show();
                paymentModeGroupBox.Show();
                draweeBankComboBox.Show();

            }
            else if (iMode == 1)
            {
                billNoTextBox.Hide();
                label6.Hide();
                label5.Text = "BANK:";
                chqNoTextBox.Show();
                chqDateTextBox.Show();
                label10.Show();
                label14.Show();
                label11.Hide();
                draweeBankComboBox.Hide();
                paymentModeGroupBox.Show();
            }
            else if (iMode == 2)
            {
                billNoTextBox.Show();
                label5.Text = "CASH:";
                chqNoTextBox.Hide();
                chqDateTextBox.Hide();
                draweeBankComboBox.Hide();
                label10.Hide();
                label14.Hide();
                label11.Hide();
                label6.Show();
                paymentModeGroupBox.Hide();
            }
            else if (iMode == 3)
            {
                billNoTextBox.Hide();
                label5.Text = "CASH:";
                chqNoTextBox.Hide();
                chqDateTextBox.Hide();
                draweeBankComboBox.Hide();
                label10.Hide();
                label14.Hide();
                label11.Hide();
                label6.Hide();
                paymentModeGroupBox.Hide();
            }
        }

        private void fillBankCashAccounts()
        {
            if(iPassBookMode == 0 || iPassBookMode == 1)
            {
                var bankCashAccountList = DB.getPartyDetails(6);
                if (bankCashAccountList != null)
                {
                    bankCashComboBox.DisplayMember = "party_name";
                    bankCashComboBox.ValueMember = "partyid";
                    bankCashComboBox.DataSource = bankCashAccountList;
                }
            }
            else if (iPassBookMode == 2 || iPassBookMode == 3)
            {
                var bankCashAccountList = DB.getPartyDetails(18);
                if (bankCashAccountList != null)
                {
                    bankCashComboBox.DisplayMember = "party_name";
                    bankCashComboBox.ValueMember = "partyid";
                    bankCashComboBox.DataSource = bankCashAccountList;
                }
            }
        }

        private void fillPartyAccountList()
        {
            int iColumnCount = 5;
            var partyAccountList = DB.getPartyAccountListForPassBook();
            /*if (partyAccountList != null)
            {
                accountNameComboBox.DisplayMember = "party_name";
                accountNameComboBox.ValueMember = "partyid";
                accountNameComboBox.DataSource = partyAccountList;
            }*/
            DataTable myDataTable = new DataTable("ParentTable");
            // Declare variables for DataColumn and DataRow objects.
            DataColumn myDataColumn;
            DataRow myDataRow;

            //Add some coulumns
            for (int index = 0; index < iColumnCount; index++)
            {
                myDataColumn = new DataColumn();
                myDataColumn.DataType = typeof(string);
                if(index == 0)
                    myDataColumn.ColumnName = "NAME";
                else if (index == 1)
                    myDataColumn.ColumnName = "ACCOUNT_TYPE";
                else if (index == 2)
                    myDataColumn.ColumnName = "ADDRESS";
                else if (index == 3)
                    myDataColumn.ColumnName = "CITY";
                else if (index == 4)
                    myDataColumn.ColumnName = "PARTY_ID";
                //myDataColumn.Caption = "ParentItem";
                myDataColumn.AutoIncrement = false;
                myDataColumn.ReadOnly = false;
                myDataColumn.Unique = false;
                // Add the Column to the DataColumnCollection.
                myDataTable.Columns.Add(myDataColumn);
            }

            //Add a row
            
            int iCol = 0;
            //Add some more rows
            for (int index = 0; index < partyAccountList.Count; index++)
            {
                myDataRow = myDataTable.NewRow();
                myDataRow[0] = partyAccountList[index].party_name;
                myDataRow[1] = DB.getAccountTypeName(partyAccountList[index].account_typeid);
                myDataRow[2] = partyAccountList[index].address + partyAccountList[index].address2;
                myDataRow[3] = DB.getCityName((long)partyAccountList[index].city_id);
                myDataRow[4] = partyAccountList[index].partyid;
                myDataTable.Rows.Add(myDataRow);
            }


            //Now set the Data of the ColumnComboBox
            accountNameComboBox.ValueMember = "PARTY_ID";
            accountNameComboBox.Data = myDataTable;
            //Set which row will be displayed in the text box
            //If you set this to a column that isn't displayed then the suggesting functionality won't work.
            accountNameComboBox.ColumnSpacing = 10;
            accountNameComboBox.ViewColumn = 0;
            
            accountNameComboBox.Columns[0].Width = 150;
            accountNameComboBox.Columns[1].Width = 100;
            accountNameComboBox.Columns[2].Width = 100;
            accountNameComboBox.Columns[3].Width = 50;
            //Set a few columns to not be shown
            accountNameComboBox.Columns[4].Display = false;
            

        }
        private void setDefaultVoucherNo()
        {
            long lSrNo = partyDB.getPassBookDefaultVoucherNo(iPassBookMode+1);
            if (lSrNo == -1)
            {
                MessageBox.Show("Unable to retrieve Voucher Data", "Error", MessageBoxButtons.OK);
                return;
            }
            else if (lSrNo == 0)
            {
                voucherNoTextBox.Text = "1";
            }
            else
                voucherNoTextBox.Text = Convert.ToString(lSrNo + 1);
        }

        private void fillDraweeBankDetails()
        {
            var bankList = DB.getBankList();
            if(bankList != null)
            {
                draweeBankComboBox.DisplayMember = "bankName";
                draweeBankComboBox.ValueMember = "bankDetails_id";
                draweeBankComboBox.DataSource = bankList;
            }
        }

        private void draweeBankComboBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                int iSelectIndx = draweeBankComboBox.FindString(draweeBankComboBox.Text);
                if (iSelectIndx == -1)
                {
                    if (MessageBox.Show("Do You want to add new BANK?", "ADD BANK", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        DB.addBank(draweeBankComboBox.Text);
                        fillDraweeBankDetails();

                    }
                }
                else
                {
                    draweeBankComboBox.SelectedIndex = iSelectIndx;
                }
            }
        }

        private void billNoTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                string strBillNo = billNoTextBox.Text;
                if (strBillNo == "")
                {
                    MessageBox.Show("Invalid Bill No.", "INVALID", MessageBoxButtons.OK);
                    return;
                }
                sales_details billDetail = partyDB.getSalesBillDetail(strBillNo);
                if (billDetail == null)
                {
                    MessageBox.Show("Bill not exist", "INVALID", MessageBoxButtons.OK);
                    return;
                }
                var partyDetails = DB.getPartyNameFromId(billDetail.partyid);
                accountNameComboBox.Text = partyDetails;
                string partyID = accountNameComboBox["PARTY_ID"].ToString();
                string accountTypeId = accountNameComboBox["ACCOUNT_TYPE"].ToString();
                updatedBillDetails(Convert.ToInt64(partyID), DB.getAccountTypeID(accountTypeId),0);
            }
            
        }

        private void updatedBillDetails(long lPartyID,long lAccountTypeId,int iRowIndex)
        {
            
            if (lAccountTypeId == 1)//sales
            {
                List<sales_details> saleBillList = partyDB.getSalesBillDetailFromPartyID(lPartyID);
                if (saleBillList == null)
                    return;
                passBookMultiColunDisplayDataList.Clear();
                for (int i = 0;i < saleBillList.Count;i++)
                {
                    //If return goods then reduce the pending amount from bill
                    if(saleBillList[i].salesTypeid == 5 || saleBillList[i].salesTypeid == 6 || saleBillList[i].salesTypeid == 7 || saleBillList[i].salesTypeid == 8)
                    {
                        for(int k = 0;k < passBookMultiColunDisplayDataList.Count;k++)
                        {
                            if (passBookMultiColunDisplayDataList[k].billNo == saleBillList[i].orderno)
                                passBookMultiColunDisplayDataList[k].pendingAmount = passBookMultiColunDisplayDataList[k].pendingAmount - saleBillList[i].billamount;
                        }
                    }
                    else
                    {
                        PassBookBillDataForMultiColumnDisplay multiColumnDisplayBillData = new PassBookBillDataForMultiColumnDisplay();
                        multiColumnDisplayBillData.billNo = saleBillList[i].billno;
                        multiColumnDisplayBillData.billType = Convert.ToInt64(saleBillList[i].salesTypeid);
                        DateTime dt, dt1;
                        string billDate = "";
                        bool bFlag = DateTime.TryParse(saleBillList[i].date.ToString(), out dt);
                        if (bFlag)
                        {
                            dt1 = dt.Date;
                            //DateTime.Parse(dt1.ToString()).ToString("dd-MM-yyyy");
                            billDate = dt1.ToString("dd-MM-yyyy");
                        }
                        multiColumnDisplayBillData.date = billDate;
                        multiColumnDisplayBillData.netAmount = Convert.ToDouble(saleBillList[i].billamount);
                        multiColumnDisplayBillData.taxableValue = Convert.ToInt64(saleBillList[i].taxablevalue);
                        List<passbook_itemdetails> billReceivedList = partyDB.getReceivedBillAmount(lPartyID, lAccountTypeId, Convert.ToString(saleBillList[i].billno));
                        if (billReceivedList == null || billReceivedList.Count == 0)
                            multiColumnDisplayBillData.pendingAmount = multiColumnDisplayBillData.netAmount;
                        else
                        {
                            double dReceivedAmount = 0;
                            for (int j = 0; j < billReceivedList.Count; j++)
                            {
                                dReceivedAmount = dReceivedAmount + Convert.ToDouble(billReceivedList[j].discountAmount + billReceivedList[j].others + billReceivedList[j].paymentReceivedAmount + billReceivedList[j].rdAmount + billReceivedList[j].rgAmount + billReceivedList[j].tdsAmount + billReceivedList[j].addLess);
                            }
                            multiColumnDisplayBillData.pendingAmount = multiColumnDisplayBillData.netAmount - dReceivedAmount;
                        }
                        bool bBillFound = false;
                        for (int j = 0; j < passbookDataGridView.RowCount - 1; j++)
                        {
                            string strBillNo = "";
                            strBillNo = passbookDataGridView.Rows[j].Cells[billNoGridTextBox.Index].EditedFormattedValue.ToString();
                            if (strBillNo == multiColumnDisplayBillData.billNo)
                            {
                                bBillFound = true;
                                break;
                            }
                        }
                        if (!bBillFound && multiColumnDisplayBillData.pendingAmount > 0)
                            passBookMultiColunDisplayDataList.Add(multiColumnDisplayBillData);
                    }
                    
                }
                DataTable db = ToDataTable(passBookMultiColunDisplayDataList);
                
                DataGridViewComboBoxCell cell = new DataGridViewMultiColumnComboCell();
                cell = (DataGridViewComboBoxCell)passbookDataGridView.Rows[iRowIndex].Cells[billNoGridTextBox.Index];
                cell.DataSource = db;

                //refReceiptComboBox.HeaderText = "RefNo";

                //cell.DataPropertyName = "challanNo";
                cell.DisplayMember = "billNo";
                cell.ValueMember = "billNo";
                //cell.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
                //cell.Width = 100;
                cell.DropDownWidth = 410;
            }
            else if (lAccountTypeId == 2)//purchase
            {
                passBookMultiColunDisplayDataList.Clear();
                List<purchase_details> purchaseBillList = partyDB.getPurchaseBillDetailFromPartyID(lPartyID);
                if (purchaseBillList != null)
                {
                    
                    for (int i = 0; i < purchaseBillList.Count; i++)
                    {
                        if(purchaseBillList[i].purchase_typeid == 6 || purchaseBillList[i].purchase_typeid == 7)
                        {
                            for(int k = 0; k < passBookMultiColunDisplayDataList.Count;k++)
                            {
                                if (passBookMultiColunDisplayDataList[k].billNo == purchaseBillList[i].refBillNo)
                                    passBookMultiColunDisplayDataList[k].pendingAmount = passBookMultiColunDisplayDataList[k].pendingAmount - Convert.ToDouble(purchaseBillList[i].billamount);

                            }
                        }
                        else
                        {
                            PassBookBillDataForMultiColumnDisplay multiColumnDisplayBillData = new PassBookBillDataForMultiColumnDisplay();
                            multiColumnDisplayBillData.billNo = purchaseBillList[i].billno;
                            multiColumnDisplayBillData.billType = Convert.ToInt64(purchaseBillList[i].purchase_typeid);
                            DateTime dt, dt1;
                            string billDate = "";
                            bool bFlag = DateTime.TryParse(purchaseBillList[i].date.ToString(), out dt);
                            if (bFlag)
                            {
                                dt1 = dt.Date;
                                //DateTime.Parse(dt1.ToString()).ToString("dd-MM-yyyy");
                                billDate = dt1.ToString("dd-MM-yyyy");
                            }
                            multiColumnDisplayBillData.date = billDate;
                            multiColumnDisplayBillData.netAmount = Convert.ToDouble(purchaseBillList[i].billamount);
                            multiColumnDisplayBillData.taxableValue = Convert.ToInt64(purchaseBillList[i].taxablevalue);
                            List<passbook_itemdetails> billReceivedList = partyDB.getReceivedBillAmount(lPartyID, lAccountTypeId, Convert.ToString(purchaseBillList[i].billno));
                            if (billReceivedList == null || billReceivedList.Count == 0)
                                multiColumnDisplayBillData.pendingAmount = multiColumnDisplayBillData.netAmount;
                            else
                            {
                                double dReceivedAmount = 0;
                                for (int j = 0; j < billReceivedList.Count; j++)
                                {
                                    dReceivedAmount = dReceivedAmount + Convert.ToDouble(billReceivedList[j].discountAmount + billReceivedList[j].others + billReceivedList[j].paymentReceivedAmount + billReceivedList[j].rdAmount + billReceivedList[j].rgAmount + billReceivedList[j].tdsAmount + billReceivedList[j].addLess);
                                }
                                multiColumnDisplayBillData.pendingAmount = multiColumnDisplayBillData.netAmount - dReceivedAmount;
                            }
                            bool bBillFound = false;
                            for (int j = 0; j < passbookDataGridView.RowCount - 1; j++)
                            {
                                string strBillNo = "";
                                strBillNo = passbookDataGridView.Rows[j].Cells[billNoGridTextBox.Index].EditedFormattedValue.ToString();
                                if (strBillNo == multiColumnDisplayBillData.billNo)
                                {
                                    bBillFound = true;
                                    break;
                                }
                            }
                            if (!bBillFound && multiColumnDisplayBillData.pendingAmount > 0)
                                passBookMultiColunDisplayDataList.Add(multiColumnDisplayBillData);
                        }
                        
                    }
                }
                
                List<greypurchase_details> greyPurchaseBillList = partyDB.getGreyPurchaseBillDetailFromPartyID(lPartyID);
                if (greyPurchaseBillList != null)
                {
                    for (int i = 0; i < greyPurchaseBillList.Count; i++)
                    {
                        PassBookBillDataForMultiColumnDisplay multiColumnDisplayBillData = new PassBookBillDataForMultiColumnDisplay();
                        multiColumnDisplayBillData.billNo = greyPurchaseBillList[i].billNo;
                        multiColumnDisplayBillData.billType = Convert.ToInt64(0);
                        DateTime dt, dt1;
                        string billDate = "";
                        bool bFlag = DateTime.TryParse(greyPurchaseBillList[i].date.ToString(), out dt);
                        if (bFlag)
                        {
                            dt1 = dt.Date;
                            //DateTime.Parse(dt1.ToString()).ToString("dd-MM-yyyy");
                            billDate = dt1.ToString("dd-MM-yyyy");
                        }
                        multiColumnDisplayBillData.date = billDate;
                        multiColumnDisplayBillData.netAmount = Convert.ToDouble(greyPurchaseBillList[i].netAmount);
                        multiColumnDisplayBillData.taxableValue = Convert.ToInt64(greyPurchaseBillList[i].taxableValue);
                        List<passbook_itemdetails> billReceivedList = partyDB.getReceivedBillAmount(lPartyID, lAccountTypeId, Convert.ToString(greyPurchaseBillList[i].billNo));
                        if (billReceivedList == null || billReceivedList.Count == 0)
                            multiColumnDisplayBillData.pendingAmount = multiColumnDisplayBillData.netAmount;
                        else
                        {
                            double dReceivedAmount = 0;
                            for (int j = 0; j < billReceivedList.Count; j++)
                            {
                                dReceivedAmount = dReceivedAmount + Convert.ToDouble(billReceivedList[j].discountAmount + billReceivedList[j].others + billReceivedList[j].paymentReceivedAmount + billReceivedList[j].rdAmount + billReceivedList[j].rgAmount + billReceivedList[j].tdsAmount + billReceivedList[j].addLess);
                            }
                            multiColumnDisplayBillData.pendingAmount = multiColumnDisplayBillData.netAmount - dReceivedAmount;
                        }
                        List<greypurchase_details> rgReceivedList = partyDB.getGreyPurchasePurchaseRGAmount(Convert.ToString(greyPurchaseBillList[i].billNo), lPartyID);
                        if (rgReceivedList != null)
                        {
                            for (int j = 0; j < rgReceivedList.Count; j++)
                            {
                                multiColumnDisplayBillData.pendingAmount = multiColumnDisplayBillData.pendingAmount - Convert.ToDouble(rgReceivedList[j].netAmount);
                            }
                        }

                        bool bBillFound = false;
                        for (int j = 0; j < passbookDataGridView.RowCount - 1; j++)
                        {
                            string strBillNo = "";
                            strBillNo = passbookDataGridView.Rows[j].Cells[billNoGridTextBox.Index].EditedFormattedValue.ToString();
                            if (strBillNo == multiColumnDisplayBillData.billNo)
                            {
                                bBillFound = true;
                                break;
                            }
                        }
                        if (!bBillFound && multiColumnDisplayBillData.pendingAmount > 0)
                            passBookMultiColunDisplayDataList.Add(multiColumnDisplayBillData);
                    }
                }
                
                
                DataTable db = ToDataTable(passBookMultiColunDisplayDataList);

                DataGridViewComboBoxCell cell = new DataGridViewMultiColumnComboCell();
                cell = (DataGridViewComboBoxCell)passbookDataGridView.Rows[iRowIndex].Cells[billNoGridTextBox.Index];
                cell.DataSource = db;

                //refReceiptComboBox.HeaderText = "RefNo";

                //cell.DataPropertyName = "challanNo";
                cell.DisplayMember = "billNo";
                cell.ValueMember = "billNo";
                //cell.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
                //cell.Width = 100;
                cell.DropDownWidth = 410;
            }
            else if (lAccountTypeId == 10)//mill
            {
                passBookMultiColunDisplayDataList.Clear();
                List<millreceipt_detail> millBillList = partyDB.getMillBillDetails(lPartyID);
                if (millBillList != null)
                {

                    for (int i = 0; i < millBillList.Count; i++)
                    {
                        PassBookBillDataForMultiColumnDisplay multiColumnDisplayBillData = new PassBookBillDataForMultiColumnDisplay();
                        multiColumnDisplayBillData.billNo = millBillList[i].billNo;
                        multiColumnDisplayBillData.billType = -2;// Convert.ToInt64(millBillList[i].purchase_typeid);
                        DateTime dt, dt1;
                        string billDate = "";
                        bool bFlag = DateTime.TryParse(millBillList[i].date.ToString(), out dt);
                        if (bFlag)
                        {
                            dt1 = dt.Date;
                            //DateTime.Parse(dt1.ToString()).ToString("dd-MM-yyyy");
                            billDate = dt1.ToString("dd-MM-yyyy");
                        }
                        multiColumnDisplayBillData.date = billDate;
                        multiColumnDisplayBillData.netAmount = Convert.ToDouble(millBillList[i].invoiceValue);
                        multiColumnDisplayBillData.taxableValue = Convert.ToInt64(millBillList[i].taxableValue);
                        List<passbook_itemdetails> billReceivedList = partyDB.getReceivedBillAmount(lPartyID, lAccountTypeId, Convert.ToString(millBillList[i].billNo));
                        if (billReceivedList == null || billReceivedList.Count == 0)
                            multiColumnDisplayBillData.pendingAmount = multiColumnDisplayBillData.netAmount;
                        else
                        {
                            double dReceivedAmount = 0;
                            for (int j = 0; j < billReceivedList.Count; j++)
                            {
                                dReceivedAmount = dReceivedAmount + Convert.ToDouble(billReceivedList[j].discountAmount + billReceivedList[j].others + billReceivedList[j].paymentReceivedAmount + billReceivedList[j].rdAmount + billReceivedList[j].rgAmount + billReceivedList[j].tdsAmount + billReceivedList[j].addLess);
                            }
                            multiColumnDisplayBillData.pendingAmount = multiColumnDisplayBillData.netAmount - dReceivedAmount;
                        }
                        bool bBillFound = false;
                        for (int j = 0; j < passbookDataGridView.RowCount - 1; j++)
                        {
                            string strBillNo = "";
                            strBillNo = passbookDataGridView.Rows[j].Cells[billNoGridTextBox.Index].EditedFormattedValue.ToString();
                            if (strBillNo == multiColumnDisplayBillData.billNo)
                            {
                                bBillFound = true;
                                break;
                            }
                        }
                        if (!bBillFound && multiColumnDisplayBillData.pendingAmount > 0)
                            passBookMultiColunDisplayDataList.Add(multiColumnDisplayBillData);
                    }
                }

                


                DataTable db = ToDataTable(passBookMultiColunDisplayDataList);

                DataGridViewComboBoxCell cell = new DataGridViewMultiColumnComboCell();
                cell = (DataGridViewComboBoxCell)passbookDataGridView.Rows[iRowIndex].Cells[billNoGridTextBox.Index];
                cell.DataSource = db;

                //refReceiptComboBox.HeaderText = "RefNo";

                //cell.DataPropertyName = "challanNo";
                cell.DisplayMember = "billNo";
                cell.ValueMember = "billNo";
                //cell.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
                //cell.Width = 100;
                cell.DropDownWidth = 410;
            }
            else if (lAccountTypeId == 11)//job work
            {
                passBookMultiColunDisplayDataList.Clear();
                List<jobreceive_billdetail> jobWorkBillList = partyDB.getJobWorkReceiptBillDetails(lPartyID);
                if (jobWorkBillList != null)
                {

                    for (int i = 0; i < jobWorkBillList.Count; i++)
                    {
                        PassBookBillDataForMultiColumnDisplay multiColumnDisplayBillData = new PassBookBillDataForMultiColumnDisplay();
                        multiColumnDisplayBillData.billNo = jobWorkBillList[i].billNo;
                        multiColumnDisplayBillData.billType = Convert.ToInt64(jobWorkBillList[i].workType_id);
                        DateTime dt, dt1;
                        string billDate = "";
                        bool bFlag = DateTime.TryParse(jobWorkBillList[i].date.ToString(), out dt);
                        if (bFlag)
                        {
                            dt1 = dt.Date;
                            //DateTime.Parse(dt1.ToString()).ToString("dd-MM-yyyy");
                            billDate = dt1.ToString("dd-MM-yyyy");
                        }
                        multiColumnDisplayBillData.date = billDate;
                        multiColumnDisplayBillData.netAmount = Convert.ToDouble(jobWorkBillList[i].netAmount);
                        multiColumnDisplayBillData.taxableValue = Convert.ToInt64(jobWorkBillList[i].taxableValue);
                        List<passbook_itemdetails> billReceivedList = partyDB.getReceivedBillAmount(lPartyID, lAccountTypeId, Convert.ToString(jobWorkBillList[i].billNo));
                        if (billReceivedList == null || billReceivedList.Count == 0)
                            multiColumnDisplayBillData.pendingAmount = multiColumnDisplayBillData.netAmount;
                        else
                        {
                            double dReceivedAmount = 0;
                            for (int j = 0; j < billReceivedList.Count; j++)
                            {
                                dReceivedAmount = dReceivedAmount + Convert.ToDouble(billReceivedList[j].discountAmount + billReceivedList[j].others + billReceivedList[j].paymentReceivedAmount + billReceivedList[j].rdAmount + billReceivedList[j].rgAmount + billReceivedList[j].tdsAmount + billReceivedList[j].addLess);
                            }
                            multiColumnDisplayBillData.pendingAmount = multiColumnDisplayBillData.netAmount - dReceivedAmount;
                        }
                        bool bBillFound = false;
                        for (int j = 0; j < passbookDataGridView.RowCount - 1; j++)
                        {
                            string strBillNo = "";
                            strBillNo = passbookDataGridView.Rows[j].Cells[billNoGridTextBox.Index].EditedFormattedValue.ToString();
                            if (strBillNo == multiColumnDisplayBillData.billNo)
                            {
                                bBillFound = true;
                                break;
                            }
                        }
                        if (!bBillFound && multiColumnDisplayBillData.pendingAmount > 0)
                            passBookMultiColunDisplayDataList.Add(multiColumnDisplayBillData);
                    }
                }




                DataTable db = ToDataTable(passBookMultiColunDisplayDataList);

                DataGridViewComboBoxCell cell = new DataGridViewMultiColumnComboCell();
                cell = (DataGridViewComboBoxCell)passbookDataGridView.Rows[iRowIndex].Cells[billNoGridTextBox.Index];
                cell.DataSource = db;

                //refReceiptComboBox.HeaderText = "RefNo";

                //cell.DataPropertyName = "challanNo";
                cell.DisplayMember = "billNo";
                cell.ValueMember = "billNo";
                //cell.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
                //cell.Width = 100;
                cell.DropDownWidth = 410;
            }

        }

        public DataTable ToDataTable<T>(List<T> items)

        {

            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties

            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);

            foreach (PropertyInfo prop in Props)

            {

                //Setting column names as Property names

                dataTable.Columns.Add(prop.Name);

            }

            foreach (T item in items)

            {

                var values = new object[Props.Length];

                for (int i = 0; i < Props.Length; i++)

                {

                    //inserting property values to datatable rows

                    values[i] = Props[i].GetValue(item, null);

                }

                dataTable.Rows.Add(values);

            }

            //put a breakpoint here and check datatable

            return dataTable;

        }

        private void passbookDataGridView_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (passbookDataGridView.CurrentCell.ColumnIndex == billNoGridTextBox.Index && e.Control is ComboBox)
            {
                ComboBox comboBox = e.Control as ComboBox;
                //comboBox.SelectedIndex = -1;
                comboBox.SelectedIndexChanged -= billNoGridComboBoxChanged;
                comboBox.SelectedIndexChanged += billNoGridComboBoxChanged;

            }
        }

        private void billNoGridComboBoxChanged(object sender, EventArgs e)
        {

            try
            {
                string accountTypeId = accountNameComboBox["ACCOUNT_TYPE"].ToString();
                long lAccountTypeId = DB.getAccountTypeID(accountTypeId);
                int iBillNoSelectedIndex = Convert.ToInt32(((ComboBox)sender).SelectedIndex);
                int iCurrentRowIndex = passbookDataGridView.CurrentRow.Index;
                passbookDataGridView.Rows[iCurrentRowIndex].Cells[dateGridTextBox.Index].Value = passBookMultiColunDisplayDataList[iBillNoSelectedIndex].date;
                passbookDataGridView.Rows[iCurrentRowIndex].Cells[taxableValueGridTextBox.Index].Value = passBookMultiColunDisplayDataList[iBillNoSelectedIndex].taxableValue;
                passbookDataGridView.Rows[iCurrentRowIndex].Cells[netAmountGridTextBox.Index].Value = passBookMultiColunDisplayDataList[iBillNoSelectedIndex].netAmount;
                if(iPassBookMode == 0 || iPassBookMode == 2)//Sales
                    passbookDataGridView.Rows[iCurrentRowIndex].Cells[typeGridTextBox.Index].Value = DB.getSalesTypeName(passBookMultiColunDisplayDataList[iBillNoSelectedIndex].billType);
                else if ((iPassBookMode == 1 || iPassBookMode == 3) && lAccountTypeId == 2 && passBookMultiColunDisplayDataList[iBillNoSelectedIndex].billType == 0)//Grey Purchase
                    passbookDataGridView.Rows[iCurrentRowIndex].Cells[typeGridTextBox.Index].Value = "GREY PURCHASE";
                else if ((iPassBookMode == 1 || iPassBookMode == 3) && lAccountTypeId == 2)//Purchase
                    passbookDataGridView.Rows[iCurrentRowIndex].Cells[typeGridTextBox.Index].Value = DB.getPurchaseTypeName(passBookMultiColunDisplayDataList[iBillNoSelectedIndex].billType);
                else if ((iPassBookMode == 1 || iPassBookMode == 3) && lAccountTypeId == 10)//Mill
                    passbookDataGridView.Rows[iCurrentRowIndex].Cells[typeGridTextBox.Index].Value = "MILL RECEIPT";
                else if ((iPassBookMode == 1 || iPassBookMode == 3) && lAccountTypeId == 11)//JobWork
                    passbookDataGridView.Rows[iCurrentRowIndex].Cells[typeGridTextBox.Index].Value = DB.getWorkTypeName(passBookMultiColunDisplayDataList[iBillNoSelectedIndex].billType);
                //passbookDataGridView.Rows[iCurrentRowIndex].Cells[pendingAmtGridTextBox.Index].Value = passBookMultiColunDisplayDataList[iCurrentRowIndex].pendingAmount;
                if (iCurrentRowIndex < dPendingAmountList.Count)
                    dPendingAmountList[iCurrentRowIndex] = passBookMultiColunDisplayDataList[iBillNoSelectedIndex].pendingAmount;
                else
                    dPendingAmountList.Add(passBookMultiColunDisplayDataList[iBillNoSelectedIndex].pendingAmount);
                TimeSpan differenceDays = DateTime.ParseExact(dateTextBox.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture) - DateTime.ParseExact(passBookMultiColunDisplayDataList[iBillNoSelectedIndex].date, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                if(iPassBookMode != 1 || iPassBookMode != 3)
                    passbookDataGridView.Rows[iCurrentRowIndex].Cells[daysGridTextBox.Index].Value = differenceDays.Days;
                else
                {
                    DateTime chqDate = DateTime.ParseExact(chqDateTextBox.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    if(chqDate != null)
                    {
                        differenceDays = chqDate - DateTime.ParseExact(passBookMultiColunDisplayDataList[iBillNoSelectedIndex].date, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        passbookDataGridView.Rows[iCurrentRowIndex].Cells[daysGridTextBox.Index].Value = differenceDays.Days;
                    }
                    else
                        passbookDataGridView.Rows[iCurrentRowIndex].Cells[daysGridTextBox.Index].Value = differenceDays.Days;

                }

                double dReceivedAmount = 0;
                for(int i = 0;i < passbookDataGridView.RowCount-1;i++)
                {
                    double dReceivedAmountInVoucher = 0;
                    double.TryParse(passbookDataGridView.Rows[i].Cells[paymentRecdGridTextBoxColumn.Index].EditedFormattedValue.ToString(),out dReceivedAmountInVoucher);
                    dReceivedAmount = dReceivedAmount + dReceivedAmountInVoucher;
                }
                double dAvailablePaymentAmount = Convert.ToDouble(recAmountTextBox.Text) - dReceivedAmount;
                if(dAvailablePaymentAmount > passBookMultiColunDisplayDataList[iBillNoSelectedIndex].pendingAmount)
                {
                    passbookDataGridView.Rows[iCurrentRowIndex].Cells[paymentRecdGridTextBoxColumn.Index].Value = passBookMultiColunDisplayDataList[iBillNoSelectedIndex].pendingAmount;
                    passbookDataGridView.Rows[iCurrentRowIndex].Cells[pendingAmtGridTextBox.Index].Value = 0;
                }
                else
                {
                    passbookDataGridView.Rows[iCurrentRowIndex].Cells[paymentRecdGridTextBoxColumn.Index].Value = dAvailablePaymentAmount;
                    passbookDataGridView.Rows[iCurrentRowIndex].Cells[pendingAmtGridTextBox.Index].Value = passBookMultiColunDisplayDataList[iBillNoSelectedIndex].pendingAmount - dAvailablePaymentAmount;
                }
                    

            }
            catch(Exception ex)
            {
                return;
            }
        }

        private void passbookDataGridView_Enter(object sender, EventArgs e)
        {
            if(bankCashComboBox.SelectedIndex == -1)
            {
                MessageBox.Show("Please Fill the Bank Details","INPUT",MessageBoxButtons.OK);
                passbookDataGridView.Focus();
                bankCashComboBox.Focus();
                return;
            }
            else if (accountNameComboBox.SelectedIndex == -1)
            {
                MessageBox.Show("Please Fill the Party Details", "INPUT", MessageBoxButtons.OK);
                passbookDataGridView.Focus();
                bankCashComboBox.Focus();
                return;
            }
            else if(Convert.ToDouble(recAmountTextBox.Text) <= 0)
            {
                MessageBox.Show("Please enter valid received Amount", "INPUT", MessageBoxButtons.OK);
                passbookDataGridView.Focus();
                recAmountTextBox.Focus();
                return;
            }
        }

        private void onlineRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if(onlineRadioButton.Checked)
            {
                chqNoTextBox.Hide();
                label14.Text = "TRANSFER DATE:";
                label10.Hide();
            }
        }

        private void chqRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (chqRadioButton.Checked)
            {
                chqNoTextBox.Show();
                label14.Text = "CHQ DATE:";
                label10.Show();
            }
        }

        private void bankCashComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (bankCashComboBox.SelectedIndex != -1)
            {
                double dOutstandingOpeningBalanceAmount = DB.getOutstandingOpeningBalance(Convert.ToInt64(bankCashComboBox.SelectedValue));
                long lAccountTypeId = 0;
                if (iPassBookMode == 0 || iPassBookMode == 1)
                    lAccountTypeId = 6;
                else
                    lAccountTypeId = 18;
                double dOutstandingTransactionAmount = partyDB.getOutstandingAmount(Convert.ToInt64(bankCashComboBox.SelectedValue), lAccountTypeId);
                double dJournalEntryOutstandingAmount = partyDB.getOutstandingFromJournalEntryData(Convert.ToInt64(bankCashComboBox.SelectedValue));
                double dOutstandingAmount = dOutstandingOpeningBalanceAmount + dOutstandingTransactionAmount + dJournalEntryOutstandingAmount;

                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-IN");
                
                string str = CultureInfo.CurrentCulture.NumberFormat.CurrencySymbol;
                if (dOutstandingAmount > 0)
                    bankCashCurBalTextBox.Text = str + string.Format(System.Globalization.CultureInfo.CreateSpecificCulture("hi-IN"), "{0:#,0.00}", dOutstandingAmount) + " CR";//Convert.ToString(Math.Round(dOutstandingAmount,2)) + " CR";
                else if (dOutstandingAmount < 0)
                    bankCashCurBalTextBox.Text = str + string.Format(System.Globalization.CultureInfo.CreateSpecificCulture("hi-IN"), "{0:#,0.00}", -dOutstandingAmount) + " DR";
                else
                    bankCashCurBalTextBox.Text = str + string.Format(System.Globalization.CultureInfo.CreateSpecificCulture("hi-IN"), "{0:#,0.00}", dOutstandingAmount);

            }
            else if (bankCashComboBox.SelectedIndex == -1)
                bankCashCurBalTextBox.Text = "0.0";
        }

        private void accountNameComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (accountNameComboBox.SelectedIndex != -1)
            {
                passbookDataGridView.Rows.Clear();
                dPendingAmountList.Clear();
                accountCurBalTextBox.Text = "0";
                chqNoTextBox.Text = "";
                remarkTextBox.Text = "";
                recAmountTextBox.Text = "0";
                chqDateTextBox.Text = DateTime.Today.ToString("dd-MM-yyyy");
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-IN");
                string str = CultureInfo.CurrentCulture.NumberFormat.CurrencySymbol;
                string partyID = accountNameComboBox["PARTY_ID"].ToString();
                string accountTypeId = accountNameComboBox["ACCOUNT_TYPE"].ToString();
                long lAccountTypeId = DB.getAccountTypeID(accountTypeId);
                double dOutstandingOpeningBalance = DB.getOutstandingOpeningBalance(Convert.ToInt64(partyID));
                double dBillOutstandingAmount = partyDB.getOutstandingAmount(Convert.ToInt64(partyID), lAccountTypeId);
                double dJournalEntryOutstandingAmount = partyDB.getOutstandingFromJournalEntryData(Convert.ToInt64(partyID));
                double dOutstandingAmount = dOutstandingOpeningBalance + dBillOutstandingAmount + dJournalEntryOutstandingAmount;
                if (dOutstandingAmount > 0)
                    accountCurBalTextBox.Text = str + string.Format(System.Globalization.CultureInfo.CreateSpecificCulture("hi-IN"), "{0:#,0.00}", dOutstandingAmount) + " CR";
                else if (dOutstandingAmount < 0)
                    accountCurBalTextBox.Text = str + string.Format(System.Globalization.CultureInfo.CreateSpecificCulture("hi-IN"), "{0:#,0.00}", -dOutstandingAmount) + " DR";
                else
                    accountCurBalTextBox.Text = str + string.Format(System.Globalization.CultureInfo.CreateSpecificCulture("hi-IN"), "{0:#,0.00}", dOutstandingAmount);

                
                updatedBillDetails(Convert.ToInt64(partyID), lAccountTypeId, 0);
            }
            else if (accountNameComboBox.SelectedIndex == -1)
                accountCurBalTextBox.Text = "0.0";
        }

        private void passbookDataGridView_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if(e.ColumnIndex == billNoGridTextBox.Index)
            {
                if(passbookDataGridView.Rows[e.RowIndex].Cells[billNoGridTextBox.Index].EditedFormattedValue.ToString().Length<=0)
                {
                    if(billNoGridTextBox.Items.Count>0)
                    {
                        MessageBox.Show("Please select the correct bill no", "INPUT ERROR", MessageBoxButtons.OK);
                        passbookDataGridView.CurrentCell.Selected = true;
                        e.Cancel = true;
                    }
                }
            }
            else if (e.ColumnIndex == tdsAmountGridTextBox.Index)
            {
                double dDiscountPercentage = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[discountPerGridTextBox.Index].EditedFormattedValue.ToString(), out dDiscountPercentage);
                double dTaxableValue = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[taxableValueGridTextBox.Index].EditedFormattedValue.ToString(), out dTaxableValue);
                double dTdsAmount = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[tdsAmountGridTextBox.Index].EditedFormattedValue.ToString(), out dTdsAmount);
                double dRDAmount = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[rdAmountGridTextBox.Index].EditedFormattedValue.ToString(), out dRDAmount);
                double dDiscountAmount = 0;
                //double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[discAmountGridTextBox.Index].EditedFormattedValue.ToString(), out dDiscountAmount);
                double dCommissionPercentage = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[comissionPerGridTextBox.Index].EditedFormattedValue.ToString(), out dCommissionPercentage);
                double dComissionAmount = 0;
                //double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[commAmtGridTexBox.Index].EditedFormattedValue.ToString(), out dComissionAmount);
                double dRGAmount = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[returnGoodsGridTextBox.Index].EditedFormattedValue.ToString(), out dRGAmount);
                double dAddLessAmount = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[addLessGridTextBox.Index].EditedFormattedValue.ToString(), out dAddLessAmount);
                double dOtherAmount = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[othersGridTextBox.Index].EditedFormattedValue.ToString(), out dOtherAmount);

                double dDiscountOnAmount = 0;
                dDiscountOnAmount = dTaxableValue - dRDAmount - dRGAmount - dAddLessAmount - dOtherAmount;
                dDiscountAmount = Math.Round(dDiscountOnAmount * dDiscountPercentage / 100);
                passbookDataGridView.Rows[e.RowIndex].Cells[discAmountGridTextBox.Index].Value = Convert.ToString(dDiscountAmount);
                double dCommissionOnAmount = 0;
                dCommissionOnAmount = dTaxableValue - dRDAmount - dRGAmount - dAddLessAmount - dOtherAmount - dDiscountAmount;
                dComissionAmount = Math.Round(dCommissionOnAmount * dCommissionPercentage / 100);
                passbookDataGridView.Rows[e.RowIndex].Cells[commAmtGridTexBox.Index].Value = Convert.ToString(dComissionAmount);


                double dPaymentReceivedAmount = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[paymentRecdGridTextBoxColumn.Index].EditedFormattedValue.ToString(), out dPaymentReceivedAmount);
                double dPendingAmount = 0;
                if (dPendingAmountList.Count > e.RowIndex)
                    dPendingAmount = dPendingAmountList[e.RowIndex] - dRDAmount - dDiscountAmount - dComissionAmount - dRGAmount - dAddLessAmount - dOtherAmount - dPaymentReceivedAmount;
                passbookDataGridView.Rows[e.RowIndex].Cells[pendingAmtGridTextBox.Index].Value = Convert.ToString(dPendingAmount - dTdsAmount);
            }
            else if (e.ColumnIndex == rdAmountGridTextBox.Index)
            {
                double dDiscountPercentage = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[discountPerGridTextBox.Index].EditedFormattedValue.ToString(), out dDiscountPercentage);
                double dTaxableValue = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[taxableValueGridTextBox.Index].EditedFormattedValue.ToString(), out dTaxableValue);
                double dTdsAmount = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[tdsAmountGridTextBox.Index].EditedFormattedValue.ToString(), out dTdsAmount);
                double dRDAmount = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[rdAmountGridTextBox.Index].EditedFormattedValue.ToString(), out dRDAmount);
                double dDiscountAmount = 0;
                //double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[discAmountGridTextBox.Index].EditedFormattedValue.ToString(), out dDiscountAmount);
                double dCommissionPercentage = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[comissionPerGridTextBox.Index].EditedFormattedValue.ToString(), out dCommissionPercentage);
                double dComissionAmount = 0;
                //double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[commAmtGridTexBox.Index].EditedFormattedValue.ToString(), out dComissionAmount);
                double dRGAmount = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[returnGoodsGridTextBox.Index].EditedFormattedValue.ToString(), out dRGAmount);
                double dAddLessAmount = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[addLessGridTextBox.Index].EditedFormattedValue.ToString(), out dAddLessAmount);
                double dOtherAmount = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[othersGridTextBox.Index].EditedFormattedValue.ToString(), out dOtherAmount);

                double dDiscountOnAmount = 0;
                dDiscountOnAmount = dTaxableValue - dRDAmount - dRGAmount - dAddLessAmount - dOtherAmount;
                dDiscountAmount = Math.Round(dDiscountOnAmount * dDiscountPercentage / 100);
                passbookDataGridView.Rows[e.RowIndex].Cells[discAmountGridTextBox.Index].Value = Convert.ToString(dDiscountAmount);
                double dCommissionOnAmount = 0;
                dCommissionOnAmount = dTaxableValue - dRDAmount - dRGAmount - dAddLessAmount - dOtherAmount - dDiscountAmount;
                dComissionAmount = Math.Round(dCommissionOnAmount * dCommissionPercentage / 100);
                passbookDataGridView.Rows[e.RowIndex].Cells[commAmtGridTexBox.Index].Value = Convert.ToString(dComissionAmount);


                double dPaymentReceivedAmount = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[paymentRecdGridTextBoxColumn.Index].EditedFormattedValue.ToString(), out dPaymentReceivedAmount);
                double dPendingAmount = 0;
                if (dPendingAmountList.Count > e.RowIndex)
                    dPendingAmount = dPendingAmountList[e.RowIndex] - dRDAmount - dDiscountAmount - dComissionAmount - dRGAmount - dAddLessAmount - dOtherAmount - dPaymentReceivedAmount;
                passbookDataGridView.Rows[e.RowIndex].Cells[pendingAmtGridTextBox.Index].Value = Convert.ToString(dPendingAmount - dTdsAmount);
            }
            else if (e.ColumnIndex == discAmountGridTextBox.Index)
            {
                double dDiscountPercentage = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[discountPerGridTextBox.Index].EditedFormattedValue.ToString(), out dDiscountPercentage);
                double dTaxableValue = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[taxableValueGridTextBox.Index].EditedFormattedValue.ToString(), out dTaxableValue);
                double dTdsAmount = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[tdsAmountGridTextBox.Index].EditedFormattedValue.ToString(), out dTdsAmount);
                double dRDAmount = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[rdAmountGridTextBox.Index].EditedFormattedValue.ToString(), out dRDAmount);
                double dDiscountAmount = 0;
                //double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[discAmountGridTextBox.Index].EditedFormattedValue.ToString(), out dDiscountAmount);
                double dCommissionPercentage = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[comissionPerGridTextBox.Index].EditedFormattedValue.ToString(), out dCommissionPercentage);
                double dComissionAmount = 0;
                //double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[commAmtGridTexBox.Index].EditedFormattedValue.ToString(), out dComissionAmount);
                double dRGAmount = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[returnGoodsGridTextBox.Index].EditedFormattedValue.ToString(), out dRGAmount);
                double dAddLessAmount = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[addLessGridTextBox.Index].EditedFormattedValue.ToString(), out dAddLessAmount);
                double dOtherAmount = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[othersGridTextBox.Index].EditedFormattedValue.ToString(), out dOtherAmount);

                double dDiscountOnAmount = 0;
                dDiscountOnAmount = dTaxableValue - dRDAmount - dRGAmount - dAddLessAmount - dOtherAmount;
                dDiscountAmount = Math.Round(dDiscountOnAmount * dDiscountPercentage / 100);
                passbookDataGridView.Rows[e.RowIndex].Cells[discAmountGridTextBox.Index].Value = Convert.ToString(dDiscountAmount);
                double dCommissionOnAmount = 0;
                dCommissionOnAmount = dTaxableValue - dRDAmount - dRGAmount - dAddLessAmount - dOtherAmount - dDiscountAmount;
                dComissionAmount = Math.Round(dCommissionOnAmount * dCommissionPercentage / 100);
                passbookDataGridView.Rows[e.RowIndex].Cells[commAmtGridTexBox.Index].Value = Convert.ToString(dComissionAmount);


                double dPaymentReceivedAmount = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[paymentRecdGridTextBoxColumn.Index].EditedFormattedValue.ToString(), out dPaymentReceivedAmount);
                double dPendingAmount = 0;
                if (dPendingAmountList.Count > e.RowIndex)
                    dPendingAmount = dPendingAmountList[e.RowIndex] - dRDAmount - dDiscountAmount - dComissionAmount - dRGAmount - dAddLessAmount - dOtherAmount - dPaymentReceivedAmount;
                passbookDataGridView.Rows[e.RowIndex].Cells[pendingAmtGridTextBox.Index].Value = Convert.ToString(dPendingAmount - dTdsAmount);
            }
            else if (e.ColumnIndex == commAmtGridTexBox.Index)
            {
                double dDiscountPercentage = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[discountPerGridTextBox.Index].EditedFormattedValue.ToString(), out dDiscountPercentage);
                double dTaxableValue = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[taxableValueGridTextBox.Index].EditedFormattedValue.ToString(), out dTaxableValue);
                double dTdsAmount = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[tdsAmountGridTextBox.Index].EditedFormattedValue.ToString(), out dTdsAmount);
                double dRDAmount = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[rdAmountGridTextBox.Index].EditedFormattedValue.ToString(), out dRDAmount);
                double dDiscountAmount = 0;
                //double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[discAmountGridTextBox.Index].EditedFormattedValue.ToString(), out dDiscountAmount);
                double dCommissionPercentage = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[comissionPerGridTextBox.Index].EditedFormattedValue.ToString(), out dCommissionPercentage);
                double dComissionAmount = 0;
                //double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[commAmtGridTexBox.Index].EditedFormattedValue.ToString(), out dComissionAmount);
                double dRGAmount = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[returnGoodsGridTextBox.Index].EditedFormattedValue.ToString(), out dRGAmount);
                double dAddLessAmount = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[addLessGridTextBox.Index].EditedFormattedValue.ToString(), out dAddLessAmount);
                double dOtherAmount = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[othersGridTextBox.Index].EditedFormattedValue.ToString(), out dOtherAmount);

                double dDiscountOnAmount = 0;
                dDiscountOnAmount = dTaxableValue - dRDAmount - dRGAmount - dAddLessAmount - dOtherAmount;
                dDiscountAmount = Math.Round(dDiscountOnAmount * dDiscountPercentage / 100);
                passbookDataGridView.Rows[e.RowIndex].Cells[discAmountGridTextBox.Index].Value = Convert.ToString(dDiscountAmount);
                double dCommissionOnAmount = 0;
                dCommissionOnAmount = dTaxableValue - dRDAmount - dRGAmount - dAddLessAmount - dOtherAmount - dDiscountAmount;
                dComissionAmount = Math.Round(dCommissionOnAmount * dCommissionPercentage / 100);
                passbookDataGridView.Rows[e.RowIndex].Cells[commAmtGridTexBox.Index].Value = Convert.ToString(dComissionAmount);


                double dPaymentReceivedAmount = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[paymentRecdGridTextBoxColumn.Index].EditedFormattedValue.ToString(), out dPaymentReceivedAmount);
                double dPendingAmount = 0;
                if (dPendingAmountList.Count > e.RowIndex)
                    dPendingAmount = dPendingAmountList[e.RowIndex] - dRDAmount - dDiscountAmount - dComissionAmount - dRGAmount - dAddLessAmount - dOtherAmount - dPaymentReceivedAmount;
                passbookDataGridView.Rows[e.RowIndex].Cells[pendingAmtGridTextBox.Index].Value = Convert.ToString(dPendingAmount - dTdsAmount);
            }
            else if (e.ColumnIndex == returnGoodsGridTextBox.Index)
            {
                double dDiscountPercentage = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[discountPerGridTextBox.Index].EditedFormattedValue.ToString(), out dDiscountPercentage);
                double dTaxableValue = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[taxableValueGridTextBox.Index].EditedFormattedValue.ToString(), out dTaxableValue);
                double dTdsAmount = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[tdsAmountGridTextBox.Index].EditedFormattedValue.ToString(), out dTdsAmount);
                double dRDAmount = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[rdAmountGridTextBox.Index].EditedFormattedValue.ToString(), out dRDAmount);
                double dDiscountAmount = 0;
                //double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[discAmountGridTextBox.Index].EditedFormattedValue.ToString(), out dDiscountAmount);
                double dCommissionPercentage = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[comissionPerGridTextBox.Index].EditedFormattedValue.ToString(), out dCommissionPercentage);
                double dComissionAmount = 0;
                //double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[commAmtGridTexBox.Index].EditedFormattedValue.ToString(), out dComissionAmount);
                double dRGAmount = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[returnGoodsGridTextBox.Index].EditedFormattedValue.ToString(), out dRGAmount);
                double dAddLessAmount = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[addLessGridTextBox.Index].EditedFormattedValue.ToString(), out dAddLessAmount);
                double dOtherAmount = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[othersGridTextBox.Index].EditedFormattedValue.ToString(), out dOtherAmount);

                double dDiscountOnAmount = 0;
                dDiscountOnAmount = dTaxableValue - dRDAmount - dRGAmount - dAddLessAmount - dOtherAmount;
                dDiscountAmount = Math.Round(dDiscountOnAmount * dDiscountPercentage / 100);
                passbookDataGridView.Rows[e.RowIndex].Cells[discAmountGridTextBox.Index].Value = Convert.ToString(dDiscountAmount);
                double dCommissionOnAmount = 0;
                dCommissionOnAmount = dTaxableValue - dRDAmount - dRGAmount - dAddLessAmount - dOtherAmount - dDiscountAmount;
                dComissionAmount = Math.Round(dCommissionOnAmount * dCommissionPercentage / 100);
                passbookDataGridView.Rows[e.RowIndex].Cells[commAmtGridTexBox.Index].Value = Convert.ToString(dComissionAmount);


                double dPaymentReceivedAmount = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[paymentRecdGridTextBoxColumn.Index].EditedFormattedValue.ToString(), out dPaymentReceivedAmount);
                double dPendingAmount = 0;
                if (dPendingAmountList.Count > e.RowIndex)
                    dPendingAmount = dPendingAmountList[e.RowIndex] - dRDAmount - dDiscountAmount - dComissionAmount - dRGAmount - dAddLessAmount - dOtherAmount - dPaymentReceivedAmount;
                passbookDataGridView.Rows[e.RowIndex].Cells[pendingAmtGridTextBox.Index].Value = Convert.ToString(dPendingAmount - dTdsAmount);
            }
            else if (e.ColumnIndex == addLessGridTextBox.Index)
            {
                double dDiscountPercentage = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[discountPerGridTextBox.Index].EditedFormattedValue.ToString(), out dDiscountPercentage);
                double dTaxableValue = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[taxableValueGridTextBox.Index].EditedFormattedValue.ToString(), out dTaxableValue);
                double dTdsAmount = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[tdsAmountGridTextBox.Index].EditedFormattedValue.ToString(), out dTdsAmount);
                double dRDAmount = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[rdAmountGridTextBox.Index].EditedFormattedValue.ToString(), out dRDAmount);
                double dDiscountAmount = 0;
                //double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[discAmountGridTextBox.Index].EditedFormattedValue.ToString(), out dDiscountAmount);
                double dCommissionPercentage = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[comissionPerGridTextBox.Index].EditedFormattedValue.ToString(), out dCommissionPercentage);
                double dComissionAmount = 0;
                //double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[commAmtGridTexBox.Index].EditedFormattedValue.ToString(), out dComissionAmount);
                double dRGAmount = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[returnGoodsGridTextBox.Index].EditedFormattedValue.ToString(), out dRGAmount);
                double dAddLessAmount = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[addLessGridTextBox.Index].EditedFormattedValue.ToString(), out dAddLessAmount);
                double dOtherAmount = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[othersGridTextBox.Index].EditedFormattedValue.ToString(), out dOtherAmount);

                double dDiscountOnAmount = 0;
                dDiscountOnAmount = dTaxableValue - dRDAmount - dRGAmount - dAddLessAmount - dOtherAmount;
                dDiscountAmount = Math.Round(dDiscountOnAmount * dDiscountPercentage / 100);
                passbookDataGridView.Rows[e.RowIndex].Cells[discAmountGridTextBox.Index].Value = Convert.ToString(dDiscountAmount);
                double dCommissionOnAmount = 0;
                dCommissionOnAmount = dTaxableValue - dRDAmount - dRGAmount - dAddLessAmount - dOtherAmount - dDiscountAmount;
                dComissionAmount = Math.Round(dCommissionOnAmount * dCommissionPercentage / 100);
                passbookDataGridView.Rows[e.RowIndex].Cells[commAmtGridTexBox.Index].Value = Convert.ToString(dComissionAmount);


                double dPaymentReceivedAmount = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[paymentRecdGridTextBoxColumn.Index].EditedFormattedValue.ToString(), out dPaymentReceivedAmount);
                double dPendingAmount = 0;
                if (dPendingAmountList.Count > e.RowIndex)
                    dPendingAmount = dPendingAmountList[e.RowIndex] - dRDAmount - dDiscountAmount - dComissionAmount - dRGAmount - dAddLessAmount - dOtherAmount - dPaymentReceivedAmount;
                passbookDataGridView.Rows[e.RowIndex].Cells[pendingAmtGridTextBox.Index].Value = Convert.ToString(dPendingAmount - dTdsAmount);
            }
            else if (e.ColumnIndex == othersGridTextBox.Index)
            {
                double dDiscountPercentage = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[discountPerGridTextBox.Index].EditedFormattedValue.ToString(), out dDiscountPercentage);
                double dTaxableValue = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[taxableValueGridTextBox.Index].EditedFormattedValue.ToString(), out dTaxableValue);
                double dTdsAmount = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[tdsAmountGridTextBox.Index].EditedFormattedValue.ToString(), out dTdsAmount);
                double dRDAmount = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[rdAmountGridTextBox.Index].EditedFormattedValue.ToString(), out dRDAmount);
                double dDiscountAmount = 0;
                //double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[discAmountGridTextBox.Index].EditedFormattedValue.ToString(), out dDiscountAmount);
                double dCommissionPercentage = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[comissionPerGridTextBox.Index].EditedFormattedValue.ToString(), out dCommissionPercentage);
                double dComissionAmount = 0;
                //double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[commAmtGridTexBox.Index].EditedFormattedValue.ToString(), out dComissionAmount);
                double dRGAmount = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[returnGoodsGridTextBox.Index].EditedFormattedValue.ToString(), out dRGAmount);
                double dAddLessAmount = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[addLessGridTextBox.Index].EditedFormattedValue.ToString(), out dAddLessAmount);
                double dOtherAmount = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[othersGridTextBox.Index].EditedFormattedValue.ToString(), out dOtherAmount);

                double dDiscountOnAmount = 0;
                dDiscountOnAmount = dTaxableValue - dRDAmount - dRGAmount - dAddLessAmount - dOtherAmount;
                dDiscountAmount = Math.Round(dDiscountOnAmount * dDiscountPercentage / 100);
                passbookDataGridView.Rows[e.RowIndex].Cells[discAmountGridTextBox.Index].Value = Convert.ToString(dDiscountAmount);
                double dCommissionOnAmount = 0;
                dCommissionOnAmount = dTaxableValue - dRDAmount - dRGAmount - dAddLessAmount - dOtherAmount - dDiscountAmount;
                dComissionAmount = Math.Round(dCommissionOnAmount * dCommissionPercentage / 100);
                passbookDataGridView.Rows[e.RowIndex].Cells[commAmtGridTexBox.Index].Value = Convert.ToString(dComissionAmount);


                double dPaymentReceivedAmount = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[paymentRecdGridTextBoxColumn.Index].EditedFormattedValue.ToString(), out dPaymentReceivedAmount);
                double dPendingAmount = 0;
                if (dPendingAmountList.Count > e.RowIndex)
                    dPendingAmount = dPendingAmountList[e.RowIndex] - dRDAmount - dDiscountAmount - dComissionAmount - dRGAmount - dAddLessAmount - dOtherAmount - dPaymentReceivedAmount;
                passbookDataGridView.Rows[e.RowIndex].Cells[pendingAmtGridTextBox.Index].Value = Convert.ToString(dPendingAmount - dTdsAmount);
            }
            else if (e.ColumnIndex == paymentRecdGridTextBoxColumn.Index)
            {
                double dDiscountPercentage = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[discountPerGridTextBox.Index].EditedFormattedValue.ToString(), out dDiscountPercentage);
                double dTaxableValue = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[taxableValueGridTextBox.Index].EditedFormattedValue.ToString(), out dTaxableValue);
                double dTdsAmount = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[tdsAmountGridTextBox.Index].EditedFormattedValue.ToString(), out dTdsAmount);
                double dRDAmount = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[rdAmountGridTextBox.Index].EditedFormattedValue.ToString(), out dRDAmount);
                double dDiscountAmount = 0;
                //double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[discAmountGridTextBox.Index].EditedFormattedValue.ToString(), out dDiscountAmount);
                double dCommissionPercentage = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[comissionPerGridTextBox.Index].EditedFormattedValue.ToString(), out dCommissionPercentage);
                double dComissionAmount = 0;
                //double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[commAmtGridTexBox.Index].EditedFormattedValue.ToString(), out dComissionAmount);
                double dRGAmount = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[returnGoodsGridTextBox.Index].EditedFormattedValue.ToString(), out dRGAmount);
                double dAddLessAmount = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[addLessGridTextBox.Index].EditedFormattedValue.ToString(), out dAddLessAmount);
                double dOtherAmount = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[othersGridTextBox.Index].EditedFormattedValue.ToString(), out dOtherAmount);

                double dDiscountOnAmount = 0;
                dDiscountOnAmount = dTaxableValue - dRDAmount - dRGAmount - dAddLessAmount - dOtherAmount;
                dDiscountAmount = Math.Round(dDiscountOnAmount * dDiscountPercentage / 100);
                passbookDataGridView.Rows[e.RowIndex].Cells[discAmountGridTextBox.Index].Value = Convert.ToString(dDiscountAmount);
                double dCommissionOnAmount = 0;
                dCommissionOnAmount = dTaxableValue - dRDAmount - dRGAmount - dAddLessAmount - dOtherAmount - dDiscountAmount;
                dComissionAmount = Math.Round(dCommissionOnAmount * dCommissionPercentage / 100);
                passbookDataGridView.Rows[e.RowIndex].Cells[commAmtGridTexBox.Index].Value = Convert.ToString(dComissionAmount);


                double dPaymentReceivedAmount = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[paymentRecdGridTextBoxColumn.Index].EditedFormattedValue.ToString(), out dPaymentReceivedAmount);
                double dPendingAmount = 0;
                if (dPendingAmountList.Count > e.RowIndex)
                    dPendingAmount = dPendingAmountList[e.RowIndex] - dRDAmount - dDiscountAmount - dComissionAmount - dRGAmount - dAddLessAmount - dOtherAmount - dPaymentReceivedAmount;
                passbookDataGridView.Rows[e.RowIndex].Cells[pendingAmtGridTextBox.Index].Value = Convert.ToString(dPendingAmount - dTdsAmount);
                double dTotalReceiveAmount = 0;
                for (int i = 0; i < passbookDataGridView.RowCount - 1; i++)
                {
                    double dReceiveAmount = 0;
                    double.TryParse(passbookDataGridView.Rows[i].Cells[paymentRecdGridTextBoxColumn.Index].EditedFormattedValue.ToString(), out dReceiveAmount);
                    dTotalReceiveAmount = dTotalReceiveAmount + dReceiveAmount;
                }
                double dReceivedAmount = Convert.ToDouble(recAmountTextBox.Text);
                if (dTotalReceiveAmount > dReceivedAmount)
                {
                    MessageBox.Show("Payment Received Amount cannot be greater than Received Amount", "ERROR", MessageBoxButtons.OK);
                    passbookDataGridView.CurrentCell.Selected = true;
                    e.Cancel = true;

                }
            }
            else if (e.ColumnIndex == discountPerGridTextBox.Index)
            {
                double dDiscountPercentage = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[discountPerGridTextBox.Index].EditedFormattedValue.ToString(), out dDiscountPercentage);
                double dTaxableValue = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[taxableValueGridTextBox.Index].EditedFormattedValue.ToString(), out dTaxableValue);
                double dTdsAmount = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[tdsAmountGridTextBox.Index].EditedFormattedValue.ToString(), out dTdsAmount);
                double dRDAmount = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[rdAmountGridTextBox.Index].EditedFormattedValue.ToString(), out dRDAmount);
                double dDiscountAmount = 0;
                //double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[discAmountGridTextBox.Index].EditedFormattedValue.ToString(), out dDiscountAmount);
                double dCommissionPercentage = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[comissionPerGridTextBox.Index].EditedFormattedValue.ToString(), out dCommissionPercentage);
                double dComissionAmount = 0;
                //double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[commAmtGridTexBox.Index].EditedFormattedValue.ToString(), out dComissionAmount);
                double dRGAmount = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[returnGoodsGridTextBox.Index].EditedFormattedValue.ToString(), out dRGAmount);
                double dAddLessAmount = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[addLessGridTextBox.Index].EditedFormattedValue.ToString(), out dAddLessAmount);
                double dOtherAmount = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[othersGridTextBox.Index].EditedFormattedValue.ToString(), out dOtherAmount);

                double dDiscountOnAmount = 0;
                dDiscountOnAmount = dTaxableValue - dRDAmount - dRGAmount - dAddLessAmount - dOtherAmount;
                dDiscountAmount = Math.Round(dDiscountOnAmount * dDiscountPercentage / 100);
                passbookDataGridView.Rows[e.RowIndex].Cells[discAmountGridTextBox.Index].Value = Convert.ToString(dDiscountAmount);
                double dCommissionOnAmount = 0;
                dCommissionOnAmount = dTaxableValue - dRDAmount - dRGAmount - dAddLessAmount - dOtherAmount - dDiscountAmount;
                dComissionAmount = Math.Round(dCommissionOnAmount * dCommissionPercentage / 100);
                passbookDataGridView.Rows[e.RowIndex].Cells[commAmtGridTexBox.Index].Value = Convert.ToString(dComissionAmount);

                
                double dPaymentReceivedAmount = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[paymentRecdGridTextBoxColumn.Index].EditedFormattedValue.ToString(), out dPaymentReceivedAmount);
                double dPendingAmount = 0;
                if (dPendingAmountList.Count > e.RowIndex)
                    dPendingAmount = dPendingAmountList[e.RowIndex] - dRDAmount - dDiscountAmount - dComissionAmount - dRGAmount - dAddLessAmount - dOtherAmount - dPaymentReceivedAmount;
                
                
                passbookDataGridView.Rows[e.RowIndex].Cells[pendingAmtGridTextBox.Index].Value = Convert.ToString(dPendingAmount - dTdsAmount);
            }
            else if (e.ColumnIndex == comissionPerGridTextBox.Index)
            {
                double dCommissionPercentage = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[comissionPerGridTextBox.Index].EditedFormattedValue.ToString(), out dCommissionPercentage);
                double dTaxableValue = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[taxableValueGridTextBox.Index].EditedFormattedValue.ToString(), out dTaxableValue);
                double dTdsAmount = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[tdsAmountGridTextBox.Index].EditedFormattedValue.ToString(), out dTdsAmount);
                double dRDAmount = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[rdAmountGridTextBox.Index].EditedFormattedValue.ToString(), out dRDAmount);
                double dDiscountAmount = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[discAmountGridTextBox.Index].EditedFormattedValue.ToString(), out dDiscountAmount);
                double dComissionAmount = 0;
                //double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[commAmtGridTexBox.Index].EditedFormattedValue.ToString(), out dComissionAmount);
                double dRGAmount = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[returnGoodsGridTextBox.Index].EditedFormattedValue.ToString(), out dRGAmount);
                double dAddLessAmount = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[addLessGridTextBox.Index].EditedFormattedValue.ToString(), out dAddLessAmount);
                double dOtherAmount = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[othersGridTextBox.Index].EditedFormattedValue.ToString(), out dOtherAmount);

                double dCommissionOnAmount = 0;
                dCommissionOnAmount = dTaxableValue - dRDAmount - dRGAmount - dAddLessAmount - dOtherAmount - dDiscountAmount;
                dComissionAmount = Math.Round(dCommissionOnAmount * dCommissionPercentage / 100);
                passbookDataGridView.Rows[e.RowIndex].Cells[commAmtGridTexBox.Index].Value = Convert.ToString(dComissionAmount);
                double dPaymentReceivedAmount = 0;
                double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[paymentRecdGridTextBoxColumn.Index].EditedFormattedValue.ToString(), out dPaymentReceivedAmount);
                double dPendingAmount = 0;
                if(dPendingAmountList.Count > e.RowIndex)
                    dPendingAmount = dPendingAmountList[e.RowIndex] - dRDAmount - dDiscountAmount - dComissionAmount - dRGAmount - dAddLessAmount - dOtherAmount - dPaymentReceivedAmount;
                passbookDataGridView.Rows[e.RowIndex].Cells[pendingAmtGridTextBox.Index].Value = Convert.ToString(dPendingAmount - dTdsAmount);
            }
        }

        private void passbookDataGridView_CellValidated(object sender, DataGridViewCellEventArgs e)
        {
            if(e.ColumnIndex == pendingAmtGridTextBox.Index)
            {
                double dTotalReceiveAmount = 0;
                for(int i = 0;i < passbookDataGridView.RowCount-1;i++)
                {
                    double dReceiveAmount = 0;
                    double.TryParse(passbookDataGridView.Rows[e.RowIndex].Cells[paymentRecdGridTextBoxColumn.Index].EditedFormattedValue.ToString(),out dReceiveAmount);
                    dTotalReceiveAmount = dTotalReceiveAmount + dReceiveAmount;
                }
                double dReceivedAmount = Convert.ToDouble(recAmountTextBox.Text);
                if (dTotalReceiveAmount < dReceivedAmount)
                {
                    passbookDataGridView.CausesValidation = false;
                    string partyID = accountNameComboBox["PARTY_ID"].ToString();
                    string accountTypeId = accountNameComboBox["ACCOUNT_TYPE"].ToString();
                    updatedBillDetails(Convert.ToInt64(partyID), DB.getAccountTypeID(accountTypeId), e.RowIndex+1);
                }
                else
                    addNewButton.Focus();

            }
        }

        private void bankCashComboBox_Validating(object sender, CancelEventArgs e)
        {
            if(bankCashComboBox.SelectedIndex == -1)
            {
                bankCashComboBox.Focus();
                MessageBox.Show("Please Select a Bank/Cash A/C","ERROR",MessageBoxButtons.OK);
                return;
            }
        }

        private void accountNameComboBox_Validating(object sender, CancelEventArgs e)
        {
            if (accountNameComboBox.SelectedIndex == -1)
            {
                accountNameComboBox.Focus();
                MessageBox.Show("Please Select a A/C", "ERROR", MessageBoxButtons.OK);
                return;
            }
        }

        private void draweeBankComboBox_Validating(object sender, CancelEventArgs e)
        {
            if (draweeBankComboBox.SelectedIndex == -1)
            {
                draweeBankComboBox.Focus();
                MessageBox.Show("Please Select a Bank from List", "ERROR", MessageBoxButtons.OK);
                return;
            }
        }

        private void addNewButton_Click(object sender, EventArgs e)
        {
            passbook_details passBookDetail = new passbook_details();
            string accountTypeId = accountNameComboBox["ACCOUNT_TYPE"].ToString();
            passBookDetail.accountType_id = DB.getAccountTypeID(accountTypeId);
            string partyID = accountNameComboBox["PARTY_ID"].ToString();
            passBookDetail.party_id = Convert.ToInt64(partyID);
            passBookDetail.amount = Convert.ToDouble(recAmountTextBox.Text);
            passBookDetail.bankCashAccount_id = Convert.ToInt64(bankCashComboBox.SelectedValue);
            passBookDetail.chqDate = DateTime.ParseExact(chqDateTextBox.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            passBookDetail.chqNo = chqNoTextBox.Text;
            passBookDetail.date = DateTime.ParseExact(dateTextBox.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            passBookDetail.draweeBank_id = Convert.ToInt64(draweeBankComboBox.SelectedValue);
            passBookDetail.passbooktype_id = Convert.ToInt64(typeComboBox.SelectedValue);
            passBookDetail.remark = remarkTextBox.Text;
            passBookDetail.voucherNo = Convert.ToInt64(voucherNoTextBox.Text);
            if (onlineRadioButton.Checked)
                passBookDetail.paymentMode = 1;
            else
                passBookDetail.paymentMode = 0;
            for (int i = 0;i < passbookDataGridView.Rows.Count-1;i++)
            {
                passbook_itemdetails passBookItemDetails = new passbook_itemdetails();

                passBookItemDetails.billNo = passbookDataGridView.Rows[i].Cells[billNoGridTextBox.Index].EditedFormattedValue.ToString();
                passBookItemDetails.addLess = Convert.ToDouble(passbookDataGridView.Rows[i].Cells[addLessGridTextBox.Index].EditedFormattedValue.ToString());
                passBookItemDetails.commAmount = Convert.ToDouble(passbookDataGridView.Rows[i].Cells[commAmtGridTexBox.Index].EditedFormattedValue.ToString());
                passBookItemDetails.commPercentage = Convert.ToDouble(passbookDataGridView.Rows[i].Cells[comissionPerGridTextBox.Index].EditedFormattedValue.ToString());
                passBookItemDetails.days = Convert.ToDouble(passbookDataGridView.Rows[i].Cells[daysGridTextBox.Index].EditedFormattedValue.ToString());
                passBookItemDetails.discountAmount = Convert.ToDouble(passbookDataGridView.Rows[i].Cells[discAmountGridTextBox.Index].EditedFormattedValue.ToString());
                passBookItemDetails.discountPercentage = Convert.ToDouble(passbookDataGridView.Rows[i].Cells[discountPerGridTextBox.Index].EditedFormattedValue.ToString());
                passBookItemDetails.others = Convert.ToDouble(passbookDataGridView.Rows[i].Cells[othersGridTextBox.Index].EditedFormattedValue.ToString());
                passBookItemDetails.paymentReceivedAmount = Convert.ToDouble(passbookDataGridView.Rows[i].Cells[paymentRecdGridTextBoxColumn.Index].EditedFormattedValue.ToString());
                passBookItemDetails.pendingAmount = Convert.ToDouble(passbookDataGridView.Rows[i].Cells[pendingAmtGridTextBox.Index].EditedFormattedValue.ToString());
                if (passBookItemDetails.pendingAmount == 0)
                    passBookItemDetails.billPaid = true;
                else
                    passBookItemDetails.billPaid = false;
                passBookItemDetails.rdAmount = Convert.ToDouble(passbookDataGridView.Rows[i].Cells[rdAmountGridTextBox.Index].EditedFormattedValue.ToString());
                passBookItemDetails.rgAmount = Convert.ToDouble(passbookDataGridView.Rows[i].Cells[returnGoodsGridTextBox.Index].EditedFormattedValue.ToString());
                passBookItemDetails.tdsAmount = Convert.ToDouble(passbookDataGridView.Rows[i].Cells[tdsAmountGridTextBox.Index].EditedFormattedValue.ToString());
                passBookItemDetails.totalReceivedAmount = passBookItemDetails.tdsAmount + passBookItemDetails.rgAmount + passBookItemDetails.rdAmount + passBookItemDetails.others + passBookItemDetails.addLess + passBookItemDetails.discountAmount + passBookItemDetails.commAmount + passBookItemDetails.paymentReceivedAmount;
                passBookDetail.passbook_itemdetails.Add(passBookItemDetails);
            }
            bool bBillAdded = partyDB.addPassBookVoucher(passBookDetail);
            if(bBillAdded)
            {
                MessageBox.Show("Voucher Added","VOUCHER",MessageBoxButtons.OK);
                resetAllData();
                voucherNoTextBox.Focus();

            }
            else
            {
                MessageBox.Show("Voucher not Added", "VOUCHER", MessageBoxButtons.OK);
            }
        }

        private void resetAllData()
        {
            passbookDataGridView.Rows.Clear();
            dPendingAmountList.Clear();
            accountCurBalTextBox.Text = "0";
            chqNoTextBox.Text = "";
            remarkTextBox.Text = "";
            recAmountTextBox.Text = "0";
            chqDateTextBox.Text = DateTime.Today.ToString("dd-MM-yyyy");
            billNoTextBox.Text = "";
            bankCashComboBox.SelectedIndex = -1;
            accountNameComboBox.SelectedIndex = -1;
            draweeBankComboBox.SelectedIndex = -1;
            bankCashCurBalTextBox.Text = "0";
            setDefaultVoucherNo();
            addNewButton.Enabled = true;
            viewButton.Enabled = true;
            updateButton.Enabled = false;
            deleteButton.Enabled = false;
            accountNameComboBox.Text = "";
            chqRadioButton.Checked = true;
        }

        private void viewButton_Click(object sender, EventArgs e)
        {
            if (voucherNoTextBox.Text == "")
            {
                MessageBox.Show("Please enter voucher no.", "VOUCHER", MessageBoxButtons.OK);
                resetAllData();
                setDefaultVoucherNo();
                return;
            }
            passBookDetailData = partyDB.getPassBookDetail(Convert.ToInt64(voucherNoTextBox.Text), Convert.ToInt64(typeComboBox.SelectedValue));
            if(passBookDetailData == null)
            {
                MessageBox.Show("Voucher No. does not exist","VOUCHER",MessageBoxButtons.OK);
                resetAllData();
                setDefaultVoucherNo();
                return;
            }

            bankCashComboBox.SelectedValue = passBookDetailData.bankCashAccount_id;
            typeComboBox.SelectedValue = passBookDetailData.passbooktype_id;
            DateTime dt, dt1;
            bool bFlag = DateTime.TryParse(passBookDetailData.date.ToString(), out dt);
            if (bFlag)
            {
                dt1 = dt.Date;
                //DateTime.Parse(dt1.ToString()).ToString("dd-MM-yyyy");
                dateTextBox.Text = dt1.ToString("dd-MM-yyyy");
            }
            var partyDetails = DB.getPartyNameFromId(Convert.ToInt64(passBookDetailData.party_id));
            accountNameComboBox.SelectedIndex = -1;//just kept to avoid unknown crash while seting text in multicomboboxcolumn
            accountNameComboBox.Text = partyDetails;
            if (passBookDetailData.paymentMode == 1)
                onlineRadioButton.Checked = true;
            else
                chqRadioButton.Checked = true;
            chqNoTextBox.Text = passBookDetailData.chqNo;
            bFlag = DateTime.TryParse(passBookDetailData.chqDate.ToString(), out dt);
            if (bFlag)
            {
                dt1 = dt.Date;
                //DateTime.Parse(dt1.ToString()).ToString("dd-MM-yyyy");
                chqDateTextBox.Text = dt1.ToString("dd-MM-yyyy");
            }
            recAmountTextBox.Text = Convert.ToString(passBookDetailData.amount);
            draweeBankComboBox.SelectedValue = passBookDetailData.draweeBank_id;
            remarkTextBox.Text = passBookDetailData.remark;
            List<passbook_itemdetails> passBookDetailDataItemList = passBookDetailData.passbook_itemdetails.ToList();
            passbookDataGridView.Rows.Clear();
            dPendingAmountList.Clear();
            for(int i = 0;i<passBookDetailDataItemList.Count;i++)
            {
                passbookDataGridView.Rows.Add();
                DataGridViewRow Row = passbookDataGridView.Rows[i];
                DataGridViewComboBoxCell cell = (DataGridViewComboBoxCell)Row.Cells[billNoGridTextBox.Index];
                cell.Items.Add(passBookDetailDataItemList[i].billNo);
                cell.Value = passBookDetailDataItemList[i].billNo;
                cell.ReadOnly = true;

                if(passBookDetailData.passbooktype_id == 1 || passBookDetailData.passbooktype_id == 3)
                {
                    sales_details saleBillDetail = partyDB.getSalesBillDetail(Convert.ToInt64(passBookDetailData.party_id),passBookDetailDataItemList[i].billNo);
                    bFlag = DateTime.TryParse(saleBillDetail.date.ToString(), out dt);
                    if (bFlag)
                    {
                        dt1 = dt.Date;
                        //DateTime.Parse(dt1.ToString()).ToString("dd-MM-yyyy");
                        Row.Cells[dateGridTextBox.Index].Value = dt1.ToString("dd-MM-yyyy");
                    }
                    Row.Cells[taxableValueGridTextBox.Index].Value = saleBillDetail.taxablevalue;
                    Row.Cells[netAmountGridTextBox.Index].Value = saleBillDetail.billamount;
                    Row.Cells[typeGridTextBox.Index].Value = DB.getSalesTypeName(saleBillDetail.salesTypeid);
                }
                else if ((passBookDetailData.passbooktype_id == 2 || passBookDetailData.passbooktype_id == 4) && passBookDetailData.accountType_id == 2)
                {
                    purchase_details purchaseBillDetail = partyDB.getPurchaseBillDetail(Convert.ToInt64(passBookDetailData.party_id), passBookDetailDataItemList[i].billNo);
                    if (purchaseBillDetail != null)
                    {
                        bFlag = DateTime.TryParse(purchaseBillDetail.date.ToString(), out dt);
                        if (bFlag)
                        {
                            dt1 = dt.Date;
                            //DateTime.Parse(dt1.ToString()).ToString("dd-MM-yyyy");
                            Row.Cells[dateGridTextBox.Index].Value = dt1.ToString("dd-MM-yyyy");
                        }
                        Row.Cells[taxableValueGridTextBox.Index].Value = purchaseBillDetail.taxablevalue;
                        Row.Cells[netAmountGridTextBox.Index].Value = purchaseBillDetail.billamount;
                        Row.Cells[typeGridTextBox.Index].Value = DB.getPurchaseTypeName(Convert.ToInt64(purchaseBillDetail.purchase_typeid));
                    }
                    else
                    {
                        greypurchase_details greyPurchaseBillDetail = partyDB.getGreyPurchaseBillDetail(Convert.ToInt64(passBookDetailData.party_id), Convert.ToString(passBookDetailDataItemList[i].billNo));
                        if (greyPurchaseBillDetail != null)
                        {
                            bFlag = DateTime.TryParse(greyPurchaseBillDetail.date.ToString(), out dt);
                            if (bFlag)
                            {
                                dt1 = dt.Date;
                                //DateTime.Parse(dt1.ToString()).ToString("dd-MM-yyyy");
                                Row.Cells[dateGridTextBox.Index].Value = dt1.ToString("dd-MM-yyyy");
                            }
                            Row.Cells[taxableValueGridTextBox.Index].Value = greyPurchaseBillDetail.taxableValue;
                            Row.Cells[netAmountGridTextBox.Index].Value = greyPurchaseBillDetail.netAmount;

                            Row.Cells[typeGridTextBox.Index].Value = "GREY PURCHASE";
                        }

                    }
                }
                else if ((passBookDetailData.passbooktype_id == 2 || passBookDetailData.passbooktype_id == 4) && (passBookDetailData.accountType_id == 10))
                {
                    millreceipt_detail millReceiptBillDetail = partyDB.getMillReceiptBillDetail(Convert.ToInt64(passBookDetailData.party_id), Convert.ToString(passBookDetailDataItemList[i].billNo));
                    bFlag = DateTime.TryParse(millReceiptBillDetail.date.ToString(), out dt);
                    if (bFlag)
                    {
                        dt1 = dt.Date;
                        //DateTime.Parse(dt1.ToString()).ToString("dd-MM-yyyy");
                        Row.Cells[dateGridTextBox.Index].Value = dt1.ToString("dd-MM-yyyy");
                    }
                    Row.Cells[taxableValueGridTextBox.Index].Value = millReceiptBillDetail.taxableValue;
                    Row.Cells[netAmountGridTextBox.Index].Value = millReceiptBillDetail.invoiceValue;
                    Row.Cells[typeGridTextBox.Index].Value = "MILL RECEIPT";
                }
                else if ((passBookDetailData.passbooktype_id == 2 || passBookDetailData.passbooktype_id == 4) && (passBookDetailData.accountType_id == 11))
                {
                    jobreceive_billdetail jobWorkBillDetail = partyDB.getJobWorkReceiptBillDetail(Convert.ToInt64(passBookDetailData.party_id), Convert.ToString(passBookDetailDataItemList[i].billNo));
                    bFlag = DateTime.TryParse(jobWorkBillDetail.date.ToString(), out dt);
                    if (bFlag)
                    {
                        dt1 = dt.Date;
                        //DateTime.Parse(dt1.ToString()).ToString("dd-MM-yyyy");
                        Row.Cells[dateGridTextBox.Index].Value = dt1.ToString("dd-MM-yyyy");
                    }
                    Row.Cells[taxableValueGridTextBox.Index].Value = jobWorkBillDetail.taxableValue;
                    Row.Cells[netAmountGridTextBox.Index].Value = jobWorkBillDetail.netAmount;
                    Row.Cells[typeGridTextBox.Index].Value = DB.getWorkTypeName(Convert.ToInt64(jobWorkBillDetail.workType_id));
                }

                Row.Cells[tdsAmountGridTextBox.Index].Value = passBookDetailDataItemList[i].tdsAmount;
                Row.Cells[daysGridTextBox.Index].Value = passBookDetailDataItemList[i].days;
                Row.Cells[rdAmountGridTextBox.Index].Value = passBookDetailDataItemList[i].rdAmount;
                Row.Cells[discountPerGridTextBox.Index].Value = passBookDetailDataItemList[i].discountPercentage;
                Row.Cells[discAmountGridTextBox.Index].Value = passBookDetailDataItemList[i].discountAmount;
                Row.Cells[comissionPerGridTextBox.Index].Value = passBookDetailDataItemList[i].commPercentage;
                Row.Cells[commAmtGridTexBox.Index].Value = passBookDetailDataItemList[i].commAmount;
                Row.Cells[returnGoodsGridTextBox.Index].Value = passBookDetailDataItemList[i].rgAmount;
                Row.Cells[addLessGridTextBox.Index].Value = passBookDetailDataItemList[i].addLess;
                Row.Cells[othersGridTextBox.Index].Value = passBookDetailDataItemList[i].others;
                Row.Cells[paymentRecdGridTextBoxColumn.Index].Value = passBookDetailDataItemList[i].paymentReceivedAmount;
                Row.Cells[pendingAmtGridTextBox.Index].Value = passBookDetailDataItemList[i].pendingAmount;
                double dPendingAmount = Convert.ToDouble(passBookDetailDataItemList[i].pendingAmount + passBookDetailDataItemList[i].tdsAmount + passBookDetailDataItemList[i].rdAmount + passBookDetailDataItemList[i].discountAmount + passBookDetailDataItemList[i].commAmount + passBookDetailDataItemList[i].rgAmount + passBookDetailDataItemList[i].addLess + passBookDetailDataItemList[i].others + passBookDetailDataItemList[i].paymentReceivedAmount);
                dPendingAmountList.Add(dPendingAmount);
            }
            addNewButton.Enabled = false;
            viewButton.Enabled = true;
            updateButton.Enabled = true;
            deleteButton.Enabled = true;

        }

        private void voucherNoTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                if(voucherNoTextBox.Text == "")
                {
                    MessageBox.Show("Please enter voucher no.", "VOUCHER", MessageBoxButtons.OK);
                    resetAllData();
                    setDefaultVoucherNo();
                    return;
                }
                passBookDetailData = partyDB.getPassBookDetail(Convert.ToInt64(voucherNoTextBox.Text), Convert.ToInt64(typeComboBox.SelectedValue));
                if (passBookDetailData == null)
                {
                    MessageBox.Show("Voucher No. does not exist", "VOUCHER", MessageBoxButtons.OK);
                    resetAllData();
                    setDefaultVoucherNo();
                    return;
                }

                bankCashComboBox.SelectedValue = passBookDetailData.bankCashAccount_id;
                typeComboBox.SelectedValue = passBookDetailData.passbooktype_id;
                DateTime dt, dt1;
                bool bFlag = DateTime.TryParse(passBookDetailData.date.ToString(), out dt);
                if (bFlag)
                {
                    dt1 = dt.Date;
                    //DateTime.Parse(dt1.ToString()).ToString("dd-MM-yyyy");
                    dateTextBox.Text = dt1.ToString("dd-MM-yyyy");
                }
                var partyDetails = DB.getPartyNameFromId(Convert.ToInt64(passBookDetailData.party_id));
                accountNameComboBox.SelectedIndex = -1;//just kept to avoid unknown crash while seting text in multicomboboxcolumn
                accountNameComboBox.Text = partyDetails;
                if (passBookDetailData.paymentMode == 1)
                    onlineRadioButton.Checked = true;
                else
                    chqRadioButton.Checked = true;
                chqNoTextBox.Text = passBookDetailData.chqNo;
                bFlag = DateTime.TryParse(passBookDetailData.chqDate.ToString(), out dt);
                if (bFlag)
                {
                    dt1 = dt.Date;
                    //DateTime.Parse(dt1.ToString()).ToString("dd-MM-yyyy");
                    chqDateTextBox.Text = dt1.ToString("dd-MM-yyyy");
                }
                recAmountTextBox.Text = Convert.ToString(passBookDetailData.amount);
                draweeBankComboBox.SelectedValue = passBookDetailData.draweeBank_id;
                remarkTextBox.Text = passBookDetailData.remark;
                List<passbook_itemdetails> passBookDetailDataItemList = passBookDetailData.passbook_itemdetails.ToList();
                passbookDataGridView.Rows.Clear();
                dPendingAmountList.Clear();
                for (int i = 0; i < passBookDetailDataItemList.Count; i++)
                {
                    passbookDataGridView.Rows.Add();
                    DataGridViewRow Row = passbookDataGridView.Rows[i];
                    DataGridViewComboBoxCell cell = (DataGridViewComboBoxCell)Row.Cells[billNoGridTextBox.Index];
                    cell.Items.Add(passBookDetailDataItemList[i].billNo);
                    cell.Value = passBookDetailDataItemList[i].billNo;
                    cell.ReadOnly = true;

                    if (passBookDetailData.passbooktype_id == 1 || passBookDetailData.passbooktype_id == 3)
                    {
                        sales_details saleBillDetail = partyDB.getSalesBillDetail(Convert.ToInt64(passBookDetailData.party_id),passBookDetailDataItemList[i].billNo);
                        bFlag = DateTime.TryParse(saleBillDetail.date.ToString(), out dt);
                        if (bFlag)
                        {
                            dt1 = dt.Date;
                            //DateTime.Parse(dt1.ToString()).ToString("dd-MM-yyyy");
                            Row.Cells[dateGridTextBox.Index].Value = dt1.ToString("dd-MM-yyyy");
                        }
                        Row.Cells[taxableValueGridTextBox.Index].Value = saleBillDetail.taxablevalue;
                        Row.Cells[netAmountGridTextBox.Index].Value = saleBillDetail.billamount;
                        Row.Cells[typeGridTextBox.Index].Value = DB.getSalesTypeName(saleBillDetail.salesTypeid);
                    }
                    else if ((passBookDetailData.passbooktype_id == 2 || passBookDetailData.passbooktype_id == 4) && passBookDetailData.accountType_id == 2)
                    {
                        purchase_details purchaseBillDetail = partyDB.getPurchaseBillDetail(Convert.ToInt64(passBookDetailData.party_id), passBookDetailDataItemList[i].billNo);
                        if(purchaseBillDetail != null)
                        {
                            bFlag = DateTime.TryParse(purchaseBillDetail.date.ToString(), out dt);
                            if (bFlag)
                            {
                                dt1 = dt.Date;
                                //DateTime.Parse(dt1.ToString()).ToString("dd-MM-yyyy");
                                Row.Cells[dateGridTextBox.Index].Value = dt1.ToString("dd-MM-yyyy");
                            }
                            Row.Cells[taxableValueGridTextBox.Index].Value = purchaseBillDetail.taxablevalue;
                            Row.Cells[netAmountGridTextBox.Index].Value = purchaseBillDetail.billamount;
                            Row.Cells[typeGridTextBox.Index].Value = DB.getPurchaseTypeName(Convert.ToInt64(purchaseBillDetail.purchase_typeid));
                        }
                        else
                        {
                            greypurchase_details greyPurchaseBillDetail = partyDB.getGreyPurchaseBillDetail(Convert.ToInt64(passBookDetailData.party_id), Convert.ToString(passBookDetailDataItemList[i].billNo));
                            if(greyPurchaseBillDetail != null)
                            {
                                bFlag = DateTime.TryParse(greyPurchaseBillDetail.date.ToString(), out dt);
                                if (bFlag)
                                {
                                    dt1 = dt.Date;
                                    //DateTime.Parse(dt1.ToString()).ToString("dd-MM-yyyy");
                                    Row.Cells[dateGridTextBox.Index].Value = dt1.ToString("dd-MM-yyyy");
                                }
                                Row.Cells[taxableValueGridTextBox.Index].Value = greyPurchaseBillDetail.taxableValue;
                                Row.Cells[netAmountGridTextBox.Index].Value = greyPurchaseBillDetail.netAmount;

                                Row.Cells[typeGridTextBox.Index].Value = "GREY PURCHASE";
                            }
                            
                        }
                        
                    }
                    else if ((passBookDetailData.passbooktype_id == 2 || passBookDetailData.passbooktype_id == 4) && (passBookDetailData.accountType_id == 10))
                    {
                        millreceipt_detail millReceiptBillDetail = partyDB.getMillReceiptBillDetail(Convert.ToInt64(passBookDetailData.party_id), Convert.ToString(passBookDetailDataItemList[i].billNo));
                        bFlag = DateTime.TryParse(millReceiptBillDetail.date.ToString(), out dt);
                        if (bFlag)
                        {
                            dt1 = dt.Date;
                            //DateTime.Parse(dt1.ToString()).ToString("dd-MM-yyyy");
                            Row.Cells[dateGridTextBox.Index].Value = dt1.ToString("dd-MM-yyyy");
                        }
                        Row.Cells[taxableValueGridTextBox.Index].Value = millReceiptBillDetail.taxableValue;
                        Row.Cells[netAmountGridTextBox.Index].Value = millReceiptBillDetail.invoiceValue;
                        Row.Cells[typeGridTextBox.Index].Value = "MILL RECEIPT";
                    }
                    else if ((passBookDetailData.passbooktype_id == 2 || passBookDetailData.passbooktype_id == 4) && (passBookDetailData.accountType_id == 11))
                    {
                        jobreceive_billdetail jobWorkBillDetail = partyDB.getJobWorkReceiptBillDetail(Convert.ToInt64(passBookDetailData.party_id), Convert.ToString(passBookDetailDataItemList[i].billNo));
                        bFlag = DateTime.TryParse(jobWorkBillDetail.date.ToString(), out dt);
                        if (bFlag)
                        {
                            dt1 = dt.Date;
                            //DateTime.Parse(dt1.ToString()).ToString("dd-MM-yyyy");
                            Row.Cells[dateGridTextBox.Index].Value = dt1.ToString("dd-MM-yyyy");
                        }
                        Row.Cells[taxableValueGridTextBox.Index].Value = jobWorkBillDetail.taxableValue;
                        Row.Cells[netAmountGridTextBox.Index].Value = jobWorkBillDetail.netAmount;
                        Row.Cells[typeGridTextBox.Index].Value = DB.getWorkTypeName(Convert.ToInt64(jobWorkBillDetail.workType_id));
                    }

                    Row.Cells[tdsAmountGridTextBox.Index].Value = passBookDetailDataItemList[i].tdsAmount;
                    Row.Cells[daysGridTextBox.Index].Value = passBookDetailDataItemList[i].days;
                    Row.Cells[rdAmountGridTextBox.Index].Value = passBookDetailDataItemList[i].rdAmount;
                    Row.Cells[discountPerGridTextBox.Index].Value = passBookDetailDataItemList[i].discountPercentage;
                    Row.Cells[discAmountGridTextBox.Index].Value = passBookDetailDataItemList[i].discountAmount;
                    Row.Cells[comissionPerGridTextBox.Index].Value = passBookDetailDataItemList[i].commPercentage;
                    Row.Cells[commAmtGridTexBox.Index].Value = passBookDetailDataItemList[i].commAmount;
                    Row.Cells[returnGoodsGridTextBox.Index].Value = passBookDetailDataItemList[i].rgAmount;
                    Row.Cells[addLessGridTextBox.Index].Value = passBookDetailDataItemList[i].addLess;
                    Row.Cells[othersGridTextBox.Index].Value = passBookDetailDataItemList[i].others;
                    Row.Cells[paymentRecdGridTextBoxColumn.Index].Value = passBookDetailDataItemList[i].paymentReceivedAmount;
                    Row.Cells[pendingAmtGridTextBox.Index].Value = passBookDetailDataItemList[i].pendingAmount;
                    double dPendingAmount = Convert.ToDouble(passBookDetailDataItemList[i].pendingAmount + passBookDetailDataItemList[i].tdsAmount + passBookDetailDataItemList[i].rdAmount + passBookDetailDataItemList[i].discountAmount + passBookDetailDataItemList[i].commAmount + passBookDetailDataItemList[i].rgAmount + passBookDetailDataItemList[i].addLess + passBookDetailDataItemList[i].others + passBookDetailDataItemList[i].paymentReceivedAmount);
                    dPendingAmountList.Add(dPendingAmount);
                }
                addNewButton.Enabled = false;
                viewButton.Enabled = true;
                updateButton.Enabled = true;
                deleteButton.Enabled = true;
            }
            
        }

        private void updateButton_Click(object sender, EventArgs e)
        {
            if (passBookDetailData == null)
            {
                MessageBox.Show("Please select a valid voucher", "ERROR", MessageBoxButtons.OK);
                return;
            }
            string accountTypeId = accountNameComboBox["ACCOUNT_TYPE"].ToString();
            passBookDetailData.accountType_id = DB.getAccountTypeID(accountTypeId);
            string partyID = accountNameComboBox["PARTY_ID"].ToString();
            passBookDetailData.party_id = Convert.ToInt64(partyID);
            passBookDetailData.amount = Convert.ToDouble(recAmountTextBox.Text);
            passBookDetailData.bankCashAccount_id = Convert.ToInt64(bankCashComboBox.SelectedValue);
            passBookDetailData.chqDate = DateTime.ParseExact(chqDateTextBox.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            passBookDetailData.chqNo = chqNoTextBox.Text;
            passBookDetailData.date = DateTime.ParseExact(dateTextBox.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            passBookDetailData.draweeBank_id = Convert.ToInt64(draweeBankComboBox.SelectedValue);
            passBookDetailData.passbooktype_id = Convert.ToInt64(typeComboBox.SelectedValue);
            passBookDetailData.remark = remarkTextBox.Text;
            passBookDetailData.voucherNo = Convert.ToInt64(voucherNoTextBox.Text);
            if (onlineRadioButton.Checked)
                passBookDetailData.paymentMode = 1;
            else
                passBookDetailData.paymentMode = 0;

            List<passbook_itemdetails> passBookDetailDataItemList = passBookDetailData.passbook_itemdetails.ToList();
            if (passBookDetailDataItemList.Count > 0)
            {
                for (int i = 0; i < passbookDataGridView.Rows.Count - 1; i++)
                {
                    if (i >= passBookDetailDataItemList.Count)
                    {
                        passbook_itemdetails passBookItemDetails = new passbook_itemdetails();

                        passBookItemDetails.billNo = passbookDataGridView.Rows[i].Cells[billNoGridTextBox.Index].EditedFormattedValue.ToString();
                        passBookItemDetails.addLess = Convert.ToDouble(passbookDataGridView.Rows[i].Cells[addLessGridTextBox.Index].EditedFormattedValue.ToString());
                        passBookItemDetails.commAmount = Convert.ToDouble(passbookDataGridView.Rows[i].Cells[commAmtGridTexBox.Index].EditedFormattedValue.ToString());
                        passBookItemDetails.commPercentage = Convert.ToDouble(passbookDataGridView.Rows[i].Cells[comissionPerGridTextBox.Index].EditedFormattedValue.ToString());
                        passBookItemDetails.days = Convert.ToDouble(passbookDataGridView.Rows[i].Cells[daysGridTextBox.Index].EditedFormattedValue.ToString());
                        passBookItemDetails.discountAmount = Convert.ToDouble(passbookDataGridView.Rows[i].Cells[discAmountGridTextBox.Index].EditedFormattedValue.ToString());
                        passBookItemDetails.discountPercentage = Convert.ToDouble(passbookDataGridView.Rows[i].Cells[discountPerGridTextBox.Index].EditedFormattedValue.ToString());
                        passBookItemDetails.others = Convert.ToDouble(passbookDataGridView.Rows[i].Cells[othersGridTextBox.Index].EditedFormattedValue.ToString());
                        passBookItemDetails.paymentReceivedAmount = Convert.ToDouble(passbookDataGridView.Rows[i].Cells[paymentRecdGridTextBoxColumn.Index].EditedFormattedValue.ToString());
                        passBookItemDetails.pendingAmount = Convert.ToDouble(passbookDataGridView.Rows[i].Cells[pendingAmtGridTextBox.Index].EditedFormattedValue.ToString());
                        if (passBookItemDetails.pendingAmount == 0)
                            passBookItemDetails.billPaid = true;
                        else
                            passBookItemDetails.billPaid = false;
                        passBookItemDetails.rdAmount = Convert.ToDouble(passbookDataGridView.Rows[i].Cells[rdAmountGridTextBox.Index].EditedFormattedValue.ToString());
                        passBookItemDetails.rgAmount = Convert.ToDouble(passbookDataGridView.Rows[i].Cells[returnGoodsGridTextBox.Index].EditedFormattedValue.ToString());
                        passBookItemDetails.tdsAmount = Convert.ToDouble(passbookDataGridView.Rows[i].Cells[tdsAmountGridTextBox.Index].EditedFormattedValue.ToString());
                        passBookItemDetails.totalReceivedAmount = passBookItemDetails.tdsAmount + passBookItemDetails.rgAmount + passBookItemDetails.rdAmount + passBookItemDetails.others + passBookItemDetails.addLess + passBookItemDetails.discountAmount + passBookItemDetails.commAmount + passBookItemDetails.paymentReceivedAmount;
                        passBookDetailDataItemList.Add(passBookItemDetails);
                    }
                    else
                    {
                        passbook_itemdetails passBookItemDetails = passBookDetailDataItemList[i];

                        passBookItemDetails.billNo = passbookDataGridView.Rows[i].Cells[billNoGridTextBox.Index].EditedFormattedValue.ToString();
                        passBookItemDetails.addLess = Convert.ToDouble(passbookDataGridView.Rows[i].Cells[addLessGridTextBox.Index].EditedFormattedValue.ToString());
                        passBookItemDetails.commAmount = Convert.ToDouble(passbookDataGridView.Rows[i].Cells[commAmtGridTexBox.Index].EditedFormattedValue.ToString());
                        passBookItemDetails.commPercentage = Convert.ToDouble(passbookDataGridView.Rows[i].Cells[comissionPerGridTextBox.Index].EditedFormattedValue.ToString());
                        passBookItemDetails.days = Convert.ToDouble(passbookDataGridView.Rows[i].Cells[daysGridTextBox.Index].EditedFormattedValue.ToString());
                        passBookItemDetails.discountAmount = Convert.ToDouble(passbookDataGridView.Rows[i].Cells[discAmountGridTextBox.Index].EditedFormattedValue.ToString());
                        passBookItemDetails.discountPercentage = Convert.ToDouble(passbookDataGridView.Rows[i].Cells[discountPerGridTextBox.Index].EditedFormattedValue.ToString());
                        passBookItemDetails.others = Convert.ToDouble(passbookDataGridView.Rows[i].Cells[othersGridTextBox.Index].EditedFormattedValue.ToString());
                        passBookItemDetails.paymentReceivedAmount = Convert.ToDouble(passbookDataGridView.Rows[i].Cells[paymentRecdGridTextBoxColumn.Index].EditedFormattedValue.ToString());
                        passBookItemDetails.pendingAmount = Convert.ToDouble(passbookDataGridView.Rows[i].Cells[pendingAmtGridTextBox.Index].EditedFormattedValue.ToString());
                        if (passBookItemDetails.pendingAmount == 0)
                            passBookItemDetails.billPaid = true;
                        else
                            passBookItemDetails.billPaid = false;
                        passBookItemDetails.rdAmount = Convert.ToDouble(passbookDataGridView.Rows[i].Cells[rdAmountGridTextBox.Index].EditedFormattedValue.ToString());
                        passBookItemDetails.rgAmount = Convert.ToDouble(passbookDataGridView.Rows[i].Cells[returnGoodsGridTextBox.Index].EditedFormattedValue.ToString());
                        passBookItemDetails.tdsAmount = Convert.ToDouble(passbookDataGridView.Rows[i].Cells[tdsAmountGridTextBox.Index].EditedFormattedValue.ToString());
                        passBookItemDetails.totalReceivedAmount = passBookItemDetails.tdsAmount + passBookItemDetails.rgAmount + passBookItemDetails.rdAmount + passBookItemDetails.others + passBookItemDetails.addLess + passBookItemDetails.discountAmount + passBookItemDetails.commAmount + passBookItemDetails.paymentReceivedAmount;
                        passBookDetailDataItemList[i] = passBookItemDetails;
                    }
                    
                }
                passBookDetailData.passbook_itemdetails = passBookDetailDataItemList;
            }
            
            bool bBillAdded = partyDB.updatePassBookVoucher(passBookDetailData);
            if (bBillAdded)
            {
                MessageBox.Show("Voucher Updated", "VOUCHER", MessageBoxButtons.OK);
                resetAllData();
                voucherNoTextBox.Focus();
                
            }
            else
            {
                MessageBox.Show("Voucher not Updated", "VOUCHER", MessageBoxButtons.OK);
            }
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            if(passBookDetailData == null)
            {
                MessageBox.Show("Please select a valid voucher", "ERROR", MessageBoxButtons.OK);
                return;
            }
            bool bVoucherDeleted = partyDB.deletePassBookVoucher(Convert.ToInt64(voucherNoTextBox.Text), Convert.ToInt64(typeComboBox.SelectedValue));
            if(bVoucherDeleted)
            {
                MessageBox.Show("Voucher Deleted", "SUUCESS", MessageBoxButtons.OK);
                resetAllData();
                setDefaultVoucherNo();
                
            }
            else
            {
                MessageBox.Show("Voucher not Deleted", "ERROR", MessageBoxButtons.OK);
            }
        }
    }
}
