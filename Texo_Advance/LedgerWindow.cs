﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Texo_Advance.DBItems;
using System.Globalization;
using Texo_Advance;

namespace Texo_Advance
{
    public partial class LedgerWindow : UserControl
    {
        List<party_details> partyDetailsList;
        MasterDBData DB = new MasterDBData();
        Form1 mainform = null;
        public LedgerWindow(string companyName, Form1 frm)
        {
            InitializeComponent();
            mainform = frm;
            UpdatePartyDetails();
            companyNameTextBox.Text = companyName;
            fromDateTextBox.Text = DateTime.Today.ToString("dd-MM-yyyy");
            toDateTextBox.Text = DateTime.Today.ToString("dd-MM-yyyy");
        }

        private void UpdatePartyDetails()
        {
            partyDetailsList = DB.getPartyDetails();
            /*if (partyList != null)
            {
                partyComboBox.DisplayMember = "party_name";
                partyComboBox.ValueMember = "partyid";
                partyComboBox.DataSource = partyList;
            }*/

            int iColumnCount = 5;


            DataTable myDataTable = new DataTable("ParentTable");
            // Declare variables for DataColumn and DataRow objects.
            DataColumn myDataColumn;
            DataRow myDataRow;

            //Add some coulumns
            for (int index = 0; index < iColumnCount; index++)
            {
                myDataColumn = new DataColumn();
                myDataColumn.DataType = typeof(string);
                if (index == 0)
                    myDataColumn.ColumnName = "NAME";
                else if (index == 1)
                    myDataColumn.ColumnName = "BROKER";
                else if (index == 2)
                    myDataColumn.ColumnName = "ADDRESS";
                else if (index == 3)
                    myDataColumn.ColumnName = "CITY";
                else if (index == 4)
                    myDataColumn.ColumnName = "PARTY_ID";
                //myDataColumn.Caption = "ParentItem";
                myDataColumn.AutoIncrement = false;
                myDataColumn.ReadOnly = false;
                myDataColumn.Unique = false;
                // Add the Column to the DataColumnCollection.
                myDataTable.Columns.Add(myDataColumn);
            }


            //Add some more rows
            for (int index = 0; index < partyDetailsList.Count; index++)
            {
                myDataRow = myDataTable.NewRow();
                myDataRow[0] = partyDetailsList[index].party_name;
                myDataRow[1] = partyDetailsList[index].transport_details.name; //DB.getBrokerNameFromID(Convert.ToInt64(partyDetailsList[index].brokerid));
                myDataRow[2] = partyDetailsList[index].address + "#" + partyDetailsList[index].address2;
                myDataRow[3] = partyDetailsList[index].city_list.name; // DB.getCityName((long)partyDetailsList[index].city_id);
                myDataRow[4] = partyDetailsList[index].partyid;
                myDataTable.Rows.Add(myDataRow);
            }

            //Now set the Data of the ColumnComboBox
            partyNameComboBox.DisplayMember = "NAME";
            partyNameComboBox.ValueMember = "PARTY_ID";
            partyNameComboBox.Data = myDataTable;
            //Set which row will be displayed in the text box
            //If you set this to a column that isn't displayed then the suggesting functionality won't work.
            partyNameComboBox.ColumnSpacing = 50;
            partyNameComboBox.ViewColumn = 0;

            partyNameComboBox.Columns[0].Width = 200;
            partyNameComboBox.Columns[1].Width = 150;
            partyNameComboBox.Columns[2].Width = 250;
            partyNameComboBox.Columns[3].Width = 50;
            //Set a few columns to not be shown
            partyNameComboBox.Columns[4].Display = false;
            partyNameComboBox.SelectedIndex = 0;
        }

        private void fromDateTextBox_Validating(object sender, CancelEventArgs e)
        {
            DateTime dt;
            bool isValid = DateTime.TryParseExact(fromDateTextBox.Text, "dd-MM-yyyy", null, DateTimeStyles.None, out dt);
            if (!isValid)
            {
                MessageBox.Show("Please enter the valid date", "DATE", MessageBoxButtons.OK);
                fromDateTextBox.Focus();
            }
        }

        private void toDateTextBox_Validating(object sender, CancelEventArgs e)
        {
            DateTime dt;
            bool isValid = DateTime.TryParseExact(toDateTextBox.Text, "dd-MM-yyyy", null, DateTimeStyles.None, out dt);
            if (!isValid)
            {
                MessageBox.Show("Please enter the valid date", "DATE", MessageBoxButtons.OK);
                toDateTextBox.Focus();
            }
        }

        private void ledgerButton_Click(object sender, EventArgs e)
        {
            mainform.openLedgerDetailedWindow(Convert.ToInt64(partyNameComboBox["PARTY_ID"].ToString()));
        }
    }
}
