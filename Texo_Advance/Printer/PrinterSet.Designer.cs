﻿namespace Texo_Advance.Printer
{
    partial class PrinterSet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.formattedPrintcheckBox = new System.Windows.Forms.CheckBox();
            this.saveBtn = new System.Windows.Forms.Button();
            this.billerHeadingTextBox = new System.Windows.Forms.RichTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tagLineTextBox = new System.Windows.Forms.RichTextBox();
            this.tnMTextBox = new System.Windows.Forms.RichTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // formattedPrintcheckBox
            // 
            this.formattedPrintcheckBox.AutoSize = true;
            this.formattedPrintcheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.formattedPrintcheckBox.Location = new System.Drawing.Point(329, 107);
            this.formattedPrintcheckBox.Name = "formattedPrintcheckBox";
            this.formattedPrintcheckBox.Size = new System.Drawing.Size(125, 19);
            this.formattedPrintcheckBox.TabIndex = 14;
            this.formattedPrintcheckBox.Text = "Formatted Print";
            this.formattedPrintcheckBox.UseVisualStyleBackColor = true;
            // 
            // saveBtn
            // 
            this.saveBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveBtn.Location = new System.Drawing.Point(179, 227);
            this.saveBtn.Name = "saveBtn";
            this.saveBtn.Size = new System.Drawing.Size(127, 45);
            this.saveBtn.TabIndex = 13;
            this.saveBtn.Text = "Save";
            this.saveBtn.UseVisualStyleBackColor = true;
            this.saveBtn.Click += new System.EventHandler(this.saveBtn_Click);
            // 
            // billerHeadingTextBox
            // 
            this.billerHeadingTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.billerHeadingTextBox.Location = new System.Drawing.Point(111, 64);
            this.billerHeadingTextBox.Name = "billerHeadingTextBox";
            this.billerHeadingTextBox.Size = new System.Drawing.Size(334, 32);
            this.billerHeadingTextBox.TabIndex = 9;
            this.billerHeadingTextBox.Text = "";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(9, 72);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(103, 15);
            this.label3.TabIndex = 12;
            this.label3.Text = "Biller Heading:";
            // 
            // tagLineTextBox
            // 
            this.tagLineTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tagLineTextBox.Location = new System.Drawing.Point(111, 19);
            this.tagLineTextBox.Name = "tagLineTextBox";
            this.tagLineTextBox.Size = new System.Drawing.Size(334, 32);
            this.tagLineTextBox.TabIndex = 7;
            this.tagLineTextBox.Text = "";
            // 
            // tnMTextBox
            // 
            this.tnMTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tnMTextBox.Location = new System.Drawing.Point(36, 125);
            this.tnMTextBox.Name = "tnMTextBox";
            this.tnMTextBox.Size = new System.Drawing.Size(409, 96);
            this.tnMTextBox.TabIndex = 11;
            this.tnMTextBox.Text = "";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(33, 107);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(290, 15);
            this.label2.TabIndex = 10;
            this.label2.Text = "Terms and Conditions (Lines seperated by ;)";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(33, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 15);
            this.label1.TabIndex = 8;
            this.label1.Text = "Tag Line:";
            // 
            // PrinterSet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.PaleGreen;
            this.ClientSize = new System.Drawing.Size(477, 284);
            this.Controls.Add(this.formattedPrintcheckBox);
            this.Controls.Add(this.saveBtn);
            this.Controls.Add(this.billerHeadingTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tagLineTextBox);
            this.Controls.Add(this.tnMTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "PrinterSet";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Printer Settings";
            this.Load += new System.EventHandler(this.PrinterSet_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox formattedPrintcheckBox;
        private System.Windows.Forms.Button saveBtn;
        private System.Windows.Forms.RichTextBox billerHeadingTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RichTextBox tagLineTextBox;
        private System.Windows.Forms.RichTextBox tnMTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}