﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Texo_Advance.DBItems;

namespace Texo_Advance.Printer
{
    public partial class PrinterSet : Form
    {
        PartyDBData db = new PartyDBData();
        List<printersettings_details> printerSettings;
        public PrinterSet()
        {
            InitializeComponent();
        }

        private void saveBtn_Click(object sender, EventArgs e)
        {
            if (printerSettings != null)
            {
                foreach (printersettings_details print in printerSettings)
                {
                    if ("tagline" == print.type)
                        print.value = tagLineTextBox.Text;
                    else if ("billerheaading" == print.type)
                        print.value = billerHeadingTextBox.Text;
                    else if ("termsandconditions" == print.type)
                        print.value = tnMTextBox.Text;
                    else if ("formattedprint" == print.type)
                    {

                        if (formattedPrintcheckBox.Checked)
                            print.value = "true";
                        else
                            print.value = "false";
                    }

                }
                if (db.UpdatePrinterDetails(printerSettings))
                    MessageBox.Show("Successfully Updated the details!!!", "Updated", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                else
                    MessageBox.Show("Details is not being updated", "Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        private void PrinterSet_Load(object sender, EventArgs e)
        {
            printerSettings = db.getPrinterSettings();


            if (printerSettings != null)
            {
                foreach (printersettings_details print in printerSettings)
                {
                    if ("tagline" == print.type)
                        tagLineTextBox.Text = print.value;
                    else if ("billerheaading" == print.type)
                        billerHeadingTextBox.Text = print.value;
                    else if ("termsandconditions" == print.type)
                        tnMTextBox.Text = print.value;
                    else if ("formattedprint" == print.type)
                    {
                        if (print.value == "true")
                            formattedPrintcheckBox.Checked = true;
                        else
                            formattedPrintcheckBox.Checked = false;
                    }

                }
            }
        }
    }
}
