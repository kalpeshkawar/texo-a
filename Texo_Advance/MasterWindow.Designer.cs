﻿namespace Texo_Advance
{
    partial class MasterWindow
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.createCompanyButton = new System.Windows.Forms.Button();
            this.cuttingReportButton = new System.Windows.Forms.Button();
            this.fin_year = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // createCompanyButton
            // 
            this.createCompanyButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.createCompanyButton.Location = new System.Drawing.Point(28, 32);
            this.createCompanyButton.Name = "createCompanyButton";
            this.createCompanyButton.Size = new System.Drawing.Size(104, 52);
            this.createCompanyButton.TabIndex = 0;
            this.createCompanyButton.Text = "CREATE COMPANY";
            this.createCompanyButton.UseVisualStyleBackColor = true;
            this.createCompanyButton.Click += new System.EventHandler(this.createCompanyButton_Click);
            // 
            // cuttingReportButton
            // 
            this.cuttingReportButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cuttingReportButton.Location = new System.Drawing.Point(28, 104);
            this.cuttingReportButton.Name = "cuttingReportButton";
            this.cuttingReportButton.Size = new System.Drawing.Size(104, 52);
            this.cuttingReportButton.TabIndex = 0;
            this.cuttingReportButton.Text = "CUTTING REPORT";
            this.cuttingReportButton.UseVisualStyleBackColor = true;
            this.cuttingReportButton.Click += new System.EventHandler(this.cuttingReportButton_Click);
            // 
            // fin_year
            // 
            this.fin_year.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fin_year.Location = new System.Drawing.Point(28, 179);
            this.fin_year.Name = "fin_year";
            this.fin_year.Size = new System.Drawing.Size(104, 52);
            this.fin_year.TabIndex = 1;
            this.fin_year.Text = "FINANCIAL YEAR";
            this.fin_year.UseMnemonic = false;
            this.fin_year.UseVisualStyleBackColor = true;
            this.fin_year.Click += new System.EventHandler(this.fin_year_Click);
            // 
            // MasterWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.fin_year);
            this.Controls.Add(this.cuttingReportButton);
            this.Controls.Add(this.createCompanyButton);
            this.Name = "MasterWindow";
            this.Size = new System.Drawing.Size(1008, 548);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button createCompanyButton;
        private System.Windows.Forms.Button cuttingReportButton;
        private System.Windows.Forms.Button fin_year;
    }
}
