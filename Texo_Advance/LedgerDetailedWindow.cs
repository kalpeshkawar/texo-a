﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Texo_Advance.DBItems;

namespace Texo_Advance
{
    public partial class LedgerDetailedWindow : UserControl
    {
        MasterDBData DB = new MasterDBData();
        PartyDBData partyDB = new PartyDBData();
        openingbalance_details openingBalance = null;
        List<sales_details> salesBillDetails = null;
        List<purchase_details> purchaseBillDetails = null;
        public LedgerDetailedWindow()
        {
            InitializeComponent();
        }


        private void ledgerDataGridView_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                //MessageBox.Show("Enter Pressed", "DATE", MessageBoxButtons.OK);
            }
        }

        public void setPartyLedgerData(long lPartyID)
        {
            ledgerDataGridView.Rows.Clear();
            int iRowCount = -1;
            openingBalance = DB.getOpeningBalance(lPartyID);
            if(openingBalance == null)
            {
                ledgerDataGridView.Rows.Add();
                iRowCount++;
                ledgerDataGridView.Rows[iRowCount].Cells[referenceAccountTextBoxGrid.Index].Value = "OPENING BALANCE";
                ledgerDataGridView.Rows[iRowCount].Cells[debitTextBoxGrid.Index].Value = "0.00";
                ledgerDataGridView.Rows[iRowCount].Cells[creditTextBoxGrid.Index].Value = "0.00";
                
            }
            else
            {
                if(openingBalance.openingBalance > 0)
                {
                    ledgerDataGridView.Rows.Add();
                    iRowCount++;
                    ledgerDataGridView.Rows[iRowCount].Cells[referenceAccountTextBoxGrid.Index].Value = "OPENING BALANCE";
                    ledgerDataGridView.Rows[iRowCount].Cells[debitTextBoxGrid.Index].Value = "0.00";
                    ledgerDataGridView.Rows[iRowCount].Cells[creditTextBoxGrid.Index].Value = Convert.ToString(decimal.Round((decimal)openingBalance.openingBalance,2));
                }
                else
                {
                    ledgerDataGridView.Rows.Add();
                    iRowCount++;
                    ledgerDataGridView.Rows[iRowCount].Cells[referenceAccountTextBoxGrid.Index].Value = "OPENING BALANCE";
                    ledgerDataGridView.Rows[iRowCount].Cells[creditTextBoxGrid.Index].Value = "0.00";
                    ledgerDataGridView.Rows[iRowCount].Cells[debitTextBoxGrid.Index].Value = Convert.ToString(openingBalance.openingBalance);
                    
                }
            }
            salesBillDetails = partyDB.getSalesBillDetailFromPartyID(lPartyID);
            if(salesBillDetails != null)
            {
                for(int i = 0;i < salesBillDetails.Count;i++)
                {
                    ledgerDataGridView.Rows.Add();
                    iRowCount++;
                    if (salesBillDetails[i].salesTypeid == 1 || salesBillDetails[i].salesTypeid == 2 || salesBillDetails[i].salesTypeid == 3 || salesBillDetails[i].salesTypeid == 4)
                    {
                        ledgerDataGridView.Rows[iRowCount].Cells[referenceAccountTextBoxGrid.Index].Value = "SALES A/C";
                        if(salesBillDetails[i].salesTypeid == 1)
                            ledgerDataGridView.Rows[iRowCount].Cells[transactionTextBoxGrid.Index].Value = "FINISH SALES";
                        else if (salesBillDetails[i].salesTypeid == 2)
                            ledgerDataGridView.Rows[iRowCount].Cells[transactionTextBoxGrid.Index].Value = "GREY SALES";
                        else if (salesBillDetails[i].salesTypeid == 3)
                            ledgerDataGridView.Rows[iRowCount].Cells[transactionTextBoxGrid.Index].Value = "PACKING MATERIAL SALES";
                        else if (salesBillDetails[i].salesTypeid == 4)
                            ledgerDataGridView.Rows[iRowCount].Cells[transactionTextBoxGrid.Index].Value = "FENT SALES";
                        ledgerDataGridView.Rows[iRowCount].Cells[creditTextBoxGrid.Index].Value = "0.00";
                        ledgerDataGridView.Rows[iRowCount].Cells[debitTextBoxGrid.Index].Value = Convert.ToString(salesBillDetails[i].billamount);
                    }
                    else if (salesBillDetails[i].salesTypeid == 5 || salesBillDetails[i].salesTypeid == 6 || salesBillDetails[i].salesTypeid == 7 || salesBillDetails[i].salesTypeid == 8)
                    {
                        ledgerDataGridView.Rows[iRowCount].Cells[referenceAccountTextBoxGrid.Index].Value = "SALES RETURN A/C";
                        if (salesBillDetails[i].salesTypeid == 5)
                            ledgerDataGridView.Rows[iRowCount].Cells[transactionTextBoxGrid.Index].Value = "FINISH SALES RETURN";
                        else if (salesBillDetails[i].salesTypeid == 6)
                            ledgerDataGridView.Rows[iRowCount].Cells[transactionTextBoxGrid.Index].Value = "GREY SALES RETURN";
                        else if (salesBillDetails[i].salesTypeid == 7)
                            ledgerDataGridView.Rows[iRowCount].Cells[transactionTextBoxGrid.Index].Value = "PACKING MATERIAL SALES RETURN";
                        else if (salesBillDetails[i].salesTypeid == 8)
                            ledgerDataGridView.Rows[iRowCount].Cells[transactionTextBoxGrid.Index].Value = "FENT SALES RETURN";
                        ledgerDataGridView.Rows[iRowCount].Cells[creditTextBoxGrid.Index].Value = Convert.ToString(salesBillDetails[i].billamount);
                        ledgerDataGridView.Rows[iRowCount].Cells[debitTextBoxGrid.Index].Value = "0.00";
                    }
                    DateTime dt, dt1;
                    bool bFlag = DateTime.TryParse(salesBillDetails[i].date.ToString(), out dt);
                    if (bFlag)
                    {
                        dt1 = dt.Date;
                        //DateTime.Parse(dt1.ToString()).ToString("dd-MM-yyyy");
                        ledgerDataGridView.Rows[iRowCount].Cells[dateTextBoxGrid.Index].Value = dt1.ToString("dd-MM-yyyy");
                    }
                    ledgerDataGridView.Rows[iRowCount].Cells[chqBillTextBoxGrid.Index].Value = salesBillDetails[i].billno;
                    ledgerDataGridView.Rows[iRowCount].Cells[voucherTextBoxGrid.Index].Value = salesBillDetails[i].voucherNo;
                    
                }
            }
            purchaseBillDetails = partyDB.getPurchaseBillDetailFromPartyID(lPartyID);
            if(purchaseBillDetails != null)
            {
                for(int i = 0;i < purchaseBillDetails.Count;i++)
                {
                    ledgerDataGridView.Rows.Add();
                    iRowCount++;
                    if(purchaseBillDetails[i].purchase_typeid == 1 || purchaseBillDetails[i].purchase_typeid == 2 || purchaseBillDetails[i].purchase_typeid == 3|| purchaseBillDetails[i].purchase_typeid == 4 || purchaseBillDetails[i].purchase_typeid == 5)
                    {
                        ledgerDataGridView.Rows[iRowCount].Cells[referenceAccountTextBoxGrid.Index].Value = "PURCHASE A/C";
                        if (purchaseBillDetails[i].purchase_typeid == 1)
                            ledgerDataGridView.Rows[iRowCount].Cells[transactionTextBoxGrid.Index].Value = "FINISH PURCHASE";
                        else if (purchaseBillDetails[i].purchase_typeid == 2)
                            ledgerDataGridView.Rows[iRowCount].Cells[transactionTextBoxGrid.Index].Value = "PACKING MATERIAL PURCHASE";
                        else if (purchaseBillDetails[i].purchase_typeid == 3)
                            ledgerDataGridView.Rows[iRowCount].Cells[transactionTextBoxGrid.Index].Value = "GST PURCHASE GOODS";
                        else if (purchaseBillDetails[i].purchase_typeid == 4)
                            ledgerDataGridView.Rows[iRowCount].Cells[transactionTextBoxGrid.Index].Value = "GST GENERAL GOODS";
                        else if (purchaseBillDetails[i].purchase_typeid == 5)
                            ledgerDataGridView.Rows[iRowCount].Cells[transactionTextBoxGrid.Index].Value = "GST INPUT SERVICES";
                        ledgerDataGridView.Rows[iRowCount].Cells[debitTextBoxGrid.Index].Value = "0.00";
                        ledgerDataGridView.Rows[iRowCount].Cells[creditTextBoxGrid.Index].Value = Convert.ToString(purchaseBillDetails[i].billamount);
                    }
                    else if (purchaseBillDetails[i].purchase_typeid == 6 || purchaseBillDetails[i].purchase_typeid == 7)
                    {
                        ledgerDataGridView.Rows[iRowCount].Cells[referenceAccountTextBoxGrid.Index].Value = "PURCHASE RETURN A/C";
                        if (purchaseBillDetails[i].purchase_typeid == 6)
                            ledgerDataGridView.Rows[iRowCount].Cells[transactionTextBoxGrid.Index].Value = "FINISH PURCHASE RETURN";
                        else if (purchaseBillDetails[i].purchase_typeid == 7)
                            ledgerDataGridView.Rows[iRowCount].Cells[transactionTextBoxGrid.Index].Value = "PACKING MATERIAL RETURN";
                        ledgerDataGridView.Rows[iRowCount].Cells[creditTextBoxGrid.Index].Value = "0.00";
                        ledgerDataGridView.Rows[iRowCount].Cells[debitTextBoxGrid.Index].Value = Convert.ToString(purchaseBillDetails[i].billamount);
                    }
                    DateTime dt, dt1;
                    bool bFlag = DateTime.TryParse(purchaseBillDetails[i].date.ToString(), out dt);
                    if (bFlag)
                    {
                        dt1 = dt.Date;
                        //DateTime.Parse(dt1.ToString()).ToString("dd-MM-yyyy");
                        ledgerDataGridView.Rows[iRowCount].Cells[dateTextBoxGrid.Index].Value = dt1.ToString("dd-MM-yyyy");
                    }
                    ledgerDataGridView.Rows[iRowCount].Cells[chqBillTextBoxGrid.Index].Value = purchaseBillDetails[i].billno;
                    ledgerDataGridView.Rows[iRowCount].Cells[voucherTextBoxGrid.Index].Value = purchaseBillDetails[i].voucherNo;

                }
            }

        }
    }
}
