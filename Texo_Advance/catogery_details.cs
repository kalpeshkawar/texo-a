//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Texo_Advance
{
    using System;
    using System.Collections.Generic;
    
    public partial class catogery_details
    {
        public long catogeryid { get; set; }
        public string catogeryname { get; set; }
        public Nullable<long> totalPcs { get; set; }
        public Nullable<double> totalMtrs { get; set; }
        public Nullable<System.DateTime> version { get; set; }
        public Nullable<long> companyId { get; set; }
        public Nullable<long> financialYear_Id { get; set; }
        public Nullable<double> rate { get; set; }
        public Nullable<double> totalValue { get; set; }
    }
}
