﻿namespace Texo_Advance
{
    partial class MillReceiptTakaChallan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.millReceivedTakaDataGridView = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.recdTakaTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.recdMtrsTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.avgShortageTextBox = new System.Windows.Forms.TextBox();
            this.saveButton = new System.Windows.Forms.Button();
            this.closeButton = new System.Windows.Forms.Button();
            this.taka_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.greyMtrsGridTextBox = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.finMtrsGridTextBox = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.shortageGridTextBox = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.millReceivedTakaDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // millReceivedTakaDataGridView
            // 
            this.millReceivedTakaDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.millReceivedTakaDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.taka_id,
            this.greyMtrsGridTextBox,
            this.finMtrsGridTextBox,
            this.shortageGridTextBox});
            this.millReceivedTakaDataGridView.Location = new System.Drawing.Point(12, 63);
            this.millReceivedTakaDataGridView.Name = "millReceivedTakaDataGridView";
            this.millReceivedTakaDataGridView.Size = new System.Drawing.Size(314, 336);
            this.millReceivedTakaDataGridView.TabIndex = 0;
            this.millReceivedTakaDataGridView.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.millReceivedTakaDataGridView_CellValidating);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Blue;
            this.label1.Location = new System.Drawing.Point(118, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(260, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "MILL RECEIPT TAKA DETAILS";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Blue;
            this.label2.Location = new System.Drawing.Point(346, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(97, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "RECD. TAKA";
            // 
            // recdTakaTextBox
            // 
            this.recdTakaTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.recdTakaTextBox.Location = new System.Drawing.Point(346, 93);
            this.recdTakaTextBox.Name = "recdTakaTextBox";
            this.recdTakaTextBox.ReadOnly = true;
            this.recdTakaTextBox.Size = new System.Drawing.Size(76, 22);
            this.recdTakaTextBox.TabIndex = 3;
            this.recdTakaTextBox.TabStop = false;
            this.recdTakaTextBox.Text = "0";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Blue;
            this.label3.Location = new System.Drawing.Point(346, 130);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(105, 16);
            this.label3.TabIndex = 2;
            this.label3.Text = "RECD. MTRS.";
            // 
            // recdMtrsTextBox
            // 
            this.recdMtrsTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.recdMtrsTextBox.Location = new System.Drawing.Point(346, 161);
            this.recdMtrsTextBox.Name = "recdMtrsTextBox";
            this.recdMtrsTextBox.ReadOnly = true;
            this.recdMtrsTextBox.Size = new System.Drawing.Size(76, 22);
            this.recdMtrsTextBox.TabIndex = 3;
            this.recdMtrsTextBox.TabStop = false;
            this.recdMtrsTextBox.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Blue;
            this.label4.Location = new System.Drawing.Point(346, 198);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(106, 16);
            this.label4.TabIndex = 2;
            this.label4.Text = "AVG. SHTG.%";
            // 
            // avgShortageTextBox
            // 
            this.avgShortageTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.avgShortageTextBox.ForeColor = System.Drawing.Color.Red;
            this.avgShortageTextBox.Location = new System.Drawing.Point(346, 229);
            this.avgShortageTextBox.Name = "avgShortageTextBox";
            this.avgShortageTextBox.ReadOnly = true;
            this.avgShortageTextBox.Size = new System.Drawing.Size(76, 22);
            this.avgShortageTextBox.TabIndex = 3;
            this.avgShortageTextBox.TabStop = false;
            this.avgShortageTextBox.Text = "0.0";
            // 
            // saveButton
            // 
            this.saveButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveButton.ForeColor = System.Drawing.Color.Maroon;
            this.saveButton.Location = new System.Drawing.Point(346, 277);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(94, 43);
            this.saveButton.TabIndex = 1;
            this.saveButton.Text = "SAVE";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // closeButton
            // 
            this.closeButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.closeButton.ForeColor = System.Drawing.Color.Maroon;
            this.closeButton.Location = new System.Drawing.Point(346, 340);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(94, 43);
            this.closeButton.TabIndex = 2;
            this.closeButton.Text = "CLOSE";
            this.closeButton.UseVisualStyleBackColor = true;
            this.closeButton.Click += new System.EventHandler(this.closeButton_Click);
            // 
            // taka_id
            // 
            this.taka_id.HeaderText = "TAKAID";
            this.taka_id.Name = "taka_id";
            this.taka_id.Visible = false;
            // 
            // greyMtrsGridTextBox
            // 
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.greyMtrsGridTextBox.DefaultCellStyle = dataGridViewCellStyle1;
            this.greyMtrsGridTextBox.HeaderText = "GREY MTRS.";
            this.greyMtrsGridTextBox.Name = "greyMtrsGridTextBox";
            this.greyMtrsGridTextBox.ReadOnly = true;
            // 
            // finMtrsGridTextBox
            // 
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.NullValue = "0.0";
            this.finMtrsGridTextBox.DefaultCellStyle = dataGridViewCellStyle2;
            this.finMtrsGridTextBox.HeaderText = "FIN. MTRS";
            this.finMtrsGridTextBox.Name = "finMtrsGridTextBox";
            // 
            // shortageGridTextBox
            // 
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Red;
            dataGridViewCellStyle3.NullValue = "0.0";
            this.shortageGridTextBox.DefaultCellStyle = dataGridViewCellStyle3;
            this.shortageGridTextBox.HeaderText = "SHORTAGE %";
            this.shortageGridTextBox.Name = "shortageGridTextBox";
            this.shortageGridTextBox.ReadOnly = true;
            this.shortageGridTextBox.Width = 70;
            // 
            // MillReceiptTakaChallan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ClientSize = new System.Drawing.Size(474, 428);
            this.Controls.Add(this.closeButton);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.avgShortageTextBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.recdMtrsTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.recdTakaTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.millReceivedTakaDataGridView);
            this.ForeColor = System.Drawing.Color.Black;
            this.Name = "MillReceiptTakaChallan";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MillReceiptTakaChallan";
            this.Load += new System.EventHandler(this.MillReceiptTakaChallan_Load);
            ((System.ComponentModel.ISupportInitialize)(this.millReceivedTakaDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView millReceivedTakaDataGridView;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox recdTakaTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox recdMtrsTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox avgShortageTextBox;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Button closeButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn taka_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn greyMtrsGridTextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn finMtrsGridTextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn shortageGridTextBox;
    }
}