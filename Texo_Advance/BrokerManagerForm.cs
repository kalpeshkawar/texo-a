﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Texo_Advance.DBItems;

namespace Texo_Advance
{
    public partial class BrokerManagerForm : Form
    {
        private long lDefaultAccountType = 0;
        MasterDBData DB = new MasterDBData();
        public BrokerManagerForm()
        {
            lDefaultAccountType = 12;
            InitializeComponent();
            GetAccountTypeData();
            GetLedgerTypeData();
            GetCityData();
        }
        private void GetLedgerTypeData()
        {
            var typeOfLedgerList = DB.getLedgerTypeList();
            if(typeOfLedgerList != null)
            {
                ledgerTypeComboBox.DisplayMember = "type";
                ledgerTypeComboBox.ValueMember = "ledger_typeid";
                ledgerTypeComboBox.DataSource = typeOfLedgerList;
            }
            
        }
        private void GetCityData()
        {
            var cityList = DB.getCityList();
            var stationList = DB.getCityList();
            if (cityList != null && stationList != null)
            {
                cityComboBox.DisplayMember = "name";
                stationComboBox.DisplayMember = "name";
                stationComboBox.ValueMember = "city_id";
                cityComboBox.ValueMember = "city_id";
                cityComboBox.DataSource = cityList;
                stationComboBox.DataSource = stationList;

            }
        }

        private void GetAccountTypeData()
        {
            var typeOfAccList = DB.getAccountTypeList();
            if (typeOfAccList != null)
            {
                accTypeComboBox.DisplayMember = "type";
                accTypeComboBox.ValueMember = "account_typeid";
                accTypeComboBox.DataSource = typeOfAccList;
                if (lDefaultAccountType == 0)
                    accTypeComboBox.SelectedIndex = 0;
                else
                    accTypeComboBox.SelectedValue = lDefaultAccountType;
            }
        }
        private void fetchDataFromControl()
        {
            if (accTypeComboBox.SelectedIndex != -1)
            {
                if (nameTextBox.TextLength > 0)
                {
                    var brokerNew = new broker_details();
                    brokerNew.account_typeid = Convert.ToInt64(accTypeComboBox.SelectedValue);
                    brokerNew.party_name = nameTextBox.Text;
                    brokerNew.address = addressTextBox.Text;
                    brokerNew.address2 = addLine2TextBox.Text;
                    brokerNew.city_id = Convert.ToInt64(cityComboBox.SelectedValue);
                    brokerNew.contactperson = conPersonTextBox.Text;
                    if (distanceTextBox.TextLength > 0)
                        brokerNew.distance = long.Parse(distanceTextBox.Text);
                    else
                        brokerNew.distance = 0;
                    brokerNew.emailid = emailIdTextBox.Text;
                    brokerNew.gst = gstnTextBox.Text;
                    brokerNew.ledger_typeid = Convert.ToInt64(ledgerTypeComboBox.SelectedValue);
                    brokerNew.panno = panTextBox.Text;
                    if (phone1TextBox.TextLength > 0)
                        brokerNew.phone1 = long.Parse(phone1TextBox.Text);
                    else
                        brokerNew.phone1 = 0;
                    if (phone2TextBox.TextLength > 0)
                        brokerNew.phone2 = long.Parse(phone2TextBox.Text);
                    else
                        brokerNew.phone2 = 0;
                    if (pinTextBox.TextLength > 0)
                        brokerNew.pincode = long.Parse(pinTextBox.Text);
                    else
                        brokerNew.pincode = 0;
                    brokerNew.station_name = stationComboBox.Text;
                    if (brokeragePerTextBox.TextLength > 0 && brokeragePerTextBox.Text != "0.0")
                        brokerNew.brokerage = double.Parse(brokeragePerTextBox.Text);
                    else
                        brokerNew.brokerage = 0;
                    if (tdsTextBox.TextLength > 0 && tdsTextBox.Text != "0.0")
                        brokerNew.tds = double.Parse(tdsTextBox.Text);
                    else
                        brokerNew.tds = 0;
                    DB.addBroker(brokerNew);
                }
                else
                {
                    MessageBox.Show("Please enter the Party Name", "Party Name", MessageBoxButtons.OK);
                    nameTextBox.Focus();
                }
            }
            else
                MessageBox.Show("Please Select proper Account Type", "Account Type", MessageBoxButtons.OK);
        }

        private void addNewButton_Click(object sender, EventArgs e)
        {
            fetchDataFromControl();
            MessageBox.Show("Broker Successfully Added", "New Broker", MessageBoxButtons.OK);
            Close();

        }

        private void cityComboBox_KeyDown(object sender, KeyEventArgs e)
        {
            int iSelectIndx = -1;
            if (e.KeyCode == Keys.Enter)
            {
                iSelectIndx = cityComboBox.FindString(cityComboBox.Text);
                if (iSelectIndx == -1)
                {
                    if (MessageBox.Show("Do You want to add new City?", "ADD CITY", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        DB.addCity(cityComboBox.Text);
                        GetCityData();
                    }
                }
                else
                {
                    cityComboBox.SelectedIndex = iSelectIndx;
                }

            }
        }

        private void stationComboBox_KeyDown(object sender, KeyEventArgs e)
        {
            int iSelectIndx = -1;
            if (e.KeyCode == Keys.Enter)
            {
                iSelectIndx = stationComboBox.FindString(stationComboBox.Text);
                if (iSelectIndx == -1)
                {
                    if (MessageBox.Show("Do You want to add new Station?", "ADD STATION", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        DB.addCity(stationComboBox.Text);
                        GetCityData();
                    }
                }
                else
                {
                    stationComboBox.SelectedIndex = iSelectIndx;
                }

            }
        }

        private void cityComboBox_Validating(object sender, CancelEventArgs e)
        {
            int iSelectedIndex = cityComboBox.SelectedIndex;
            if (iSelectedIndex == -1)
            {
                MessageBox.Show("Please select the correct City", "CITY", MessageBoxButtons.OK);
                cityComboBox.Focus();
            }
        }

        private void stationComboBox_Validating(object sender, CancelEventArgs e)
        {
            int iSelectedIndex = stationComboBox.SelectedIndex;
            if (iSelectedIndex == -1)
            {
                MessageBox.Show("Please select the correct Station", "STATION", MessageBoxButtons.OK);
                stationComboBox.Focus();
            }
        }

        private void cityComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            long lSelectedIndex = Convert.ToInt64(cityComboBox.SelectedValue);
            if (lSelectedIndex != -1)
            {
                stationComboBox.SelectedValue = lSelectedIndex;
            }
        }

        public void setDefaultName(string brokerName)
        {
            nameTextBox.Text = brokerName;
        }
    }

}
