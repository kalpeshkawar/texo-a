/*
SQLyog Community v10.2 
MySQL - 5.7.31-log : Database - texto-advance
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`texto-advance` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `texto-advance`;

/*Table structure for table `challantaka_mapping` */

DROP TABLE IF EXISTS `challantaka_mapping`;

CREATE TABLE `challantaka_mapping` (
  `challanNo` bigint(20) NOT NULL,
  `millTaka_id` bigint(50) NOT NULL,
  `version` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`challanNo`,`millTaka_id`),
  KEY `millTaka_id` (`millTaka_id`),
  CONSTRAINT `challantaka_mapping_ibfk_3` FOREIGN KEY (`challanNo`) REFERENCES `milldispatchchallan_detail` (`challanNo`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `challantaka_mapping_ibfk_4` FOREIGN KEY (`millTaka_id`) REFERENCES `millchallandispatchtaka_detail` (`millTaka_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `challantaka_mapping` */

/*Table structure for table `millchallandispatchtaka_detail` */

DROP TABLE IF EXISTS `millchallandispatchtaka_detail`;

CREATE TABLE `millchallandispatchtaka_detail` (
  `millTaka_id` bigint(50) NOT NULL AUTO_INCREMENT,
  `greyMtrs` double DEFAULT NULL,
  `version` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `finMtrs` double DEFAULT NULL,
  `isReceived` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`millTaka_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `millchallandispatchtaka_detail` */

/*Table structure for table `milldispatch_detail` */

DROP TABLE IF EXISTS `milldispatch_detail`;

CREATE TABLE `milldispatch_detail` (
  `voucherNo` bigint(20) NOT NULL,
  `date` date DEFAULT NULL,
  `greypurchase_id` bigint(20) DEFAULT NULL,
  `totalTaka` bigint(20) DEFAULT NULL,
  `totalMtrs` double DEFAULT NULL,
  `version` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`voucherNo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `milldispatch_detail` */

/*Table structure for table `milldispatchchallan_detail` */

DROP TABLE IF EXISTS `milldispatchchallan_detail`;

CREATE TABLE `milldispatchchallan_detail` (
  `challanNo` bigint(50) NOT NULL,
  `voucherNo` bigint(20) DEFAULT NULL,
  `mill_id` bigint(20) DEFAULT NULL,
  `station_id` bigint(20) DEFAULT NULL,
  `mill_gstn` varchar(50) DEFAULT NULL,
  `processType` bigint(20) DEFAULT NULL,
  `totalTakas` bigint(20) DEFAULT NULL,
  `totalMtrs` double DEFAULT NULL,
  `master` varchar(100) DEFAULT NULL,
  `rate` double DEFAULT NULL,
  `cardNo` bigint(20) DEFAULT NULL,
  `version` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `remark` varchar(2000) DEFAULT NULL,
  `quality_id` bigint(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `reference_billNo` bigint(20) DEFAULT NULL,
  `reference_challanNo` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`challanNo`),
  KEY `voucherNo` (`voucherNo`),
  CONSTRAINT `milldispatchchallan_detail_ibfk_2` FOREIGN KEY (`voucherNo`) REFERENCES `milldispatch_detail` (`voucherNo`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `milldispatchchallan_detail` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
