﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Texo_Advance.DBItems
{
    class PartyDBData
    {
        String dbName = Properties.Settings.Default.FinancialYearDbName;

        public bool addSalesBill(sales_details salesBill)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                db.sales_details.Add(salesBill);
                db.SaveChanges();
                db.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public bool addJournalEntryVoucher(journalentry_detail journalEntry)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                var version = journalEntry.version;
                //passBookData.version = null;
                var existingParent = db.journalentry_detail
                .Where(p => p.journalEntryDetail_id == journalEntry.journalEntryDetail_id)
                .SingleOrDefault();

                if (existingParent != null)
                {
                    // Update parent
                    db.Entry(existingParent).CurrentValues.SetValues(journalEntry);

                    db.Entry(existingParent).OriginalValues["version"] = version;

                }
                else
                {
                    db.journalentry_detail.Add(journalEntry);
                }

                db.SaveChanges();
                db.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }
        public bool addPurchaseBill(purchase_details purchaseBill)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                db.purchase_details.Add(purchaseBill);
                db.SaveChanges();
                db.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public bool addGreyPurchaseBill(greypurchase_details purchaseBill)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                db.greypurchase_details.Add(purchaseBill);
                db.SaveChanges();
                db.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public bool addMillDispatchChallan(milldispatch_detail millDispatchChallan)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                db.milldispatch_detail.Add(millDispatchChallan);
                db.SaveChanges();
                db.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public bool addPassBookVoucher(passbook_details passBookDetail)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                db.passbook_details.Add(passBookDetail);
                db.SaveChanges();
                db.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public bool checkBillDuplicacy(string strBillNo, long lAccountTypeID)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                int iCount = db.sales_details.Where(x => x.billno == strBillNo).Where(x => x.salesTypeid == lAccountTypeID).Count();
                db.Dispose();
                if (iCount <= 0)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                return false;
            }

        }
        public long getSalesVoucherNo(long lSalesTypeID)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                long lVoucherNo = (long)db.sales_details.Where(p => p.salesTypeid == lSalesTypeID).Select(p => p.voucherNo).DefaultIfEmpty(0).Max();
                db.Dispose();
                return lVoucherNo;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }

        public double getOutstandingAmount(long lPartyId, long lAccountTypeId)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                double dOutstandingAmount = 0;
                double dOutstandindTillDateAmount = 0;
                if (lAccountTypeId == 1)//Sales
                {
                    List<sales_details> totalSalesBill = db.sales_details.Where(x => x.partyid == lPartyId).ToList();

                    if (totalSalesBill != null)
                    {
                        for (int i = 0; i < totalSalesBill.Count; i++)
                        {
                            if (totalSalesBill[i].salesTypeid == 1 || totalSalesBill[i].salesTypeid == 2 || totalSalesBill[i].salesTypeid == 3 || totalSalesBill[i].salesTypeid == 4)
                                dOutstandingAmount = dOutstandingAmount - totalSalesBill[i].billamount;
                            else
                                dOutstandingAmount = dOutstandingAmount + totalSalesBill[i].billamount;
                        }

                    }

                    List<creditdebitnote_details> saleBillCreditDebitNoteDetails = db.creditdebitnote_details.Where(x => x.party_id == lPartyId).ToList();
                    for (int j = 0; j < saleBillCreditDebitNoteDetails.Count; j++)//if credit note thn reduce th outstanding amount by credit note amount.
                    {
                        if (saleBillCreditDebitNoteDetails[j].cdNoteType_id == 1)
                            dOutstandingAmount = dOutstandingAmount + Convert.ToDouble(saleBillCreditDebitNoteDetails[j].netAmount);
                        else
                            dOutstandingAmount = dOutstandingAmount - Convert.ToDouble(saleBillCreditDebitNoteDetails[j].netAmount);
                    }
                    double dPaymentReceivedAmount = 0;
                    List<passbook_details> partyPassBookList = db.passbook_details.Where(x => x.party_id == lPartyId).Where(x => x.accountType_id == lAccountTypeId).ToList();
                    if (partyPassBookList != null)
                    {
                        for (int i = 0; i < partyPassBookList.Count; i++)
                        {
                            List<passbook_itemdetails> partyItemPassBookList = partyPassBookList[i].passbook_itemdetails.ToList();

                            if (partyItemPassBookList != null || partyItemPassBookList.Count > 0)
                            {
                                for (int j = 0; j < partyItemPassBookList.Count; j++)
                                {
                                    double dReceiveAmount = (double)(partyItemPassBookList[j].addLess + partyItemPassBookList[j].commAmount + partyItemPassBookList[j].discountAmount + partyItemPassBookList[j].others + partyItemPassBookList[j].paymentReceivedAmount + partyItemPassBookList[j].rdAmount + partyItemPassBookList[j].rgAmount + partyItemPassBookList[j].tdsAmount);
                                    dPaymentReceivedAmount = dPaymentReceivedAmount + dReceiveAmount;
                                }

                            }
                            else
                            {
                                dPaymentReceivedAmount = dPaymentReceivedAmount + (double)partyPassBookList[i].amount;
                            }
                        }
                    }

                    dOutstandindTillDateAmount = dOutstandingAmount + dPaymentReceivedAmount;
                }
                else if (lAccountTypeId == 6 || lAccountTypeId == 18)//Bank and cash
                {
                    List<passbook_details> bankPassBookDetails = db.passbook_details.Where(x => x.bankCashAccount_id == lPartyId).ToList();
                    if (bankPassBookDetails != null || bankPassBookDetails.Count > 0)
                    {

                        for (int i = 0; i < bankPassBookDetails.Count; i++)
                        {
                            //if(bankPassBookDetails[i].accountType_id == 1 || bankPassBookDetails[i].accountType_id == 6 || bankPassBookDetails[i].accountType_id == 10)
                            if (bankPassBookDetails[i].passbooktype_id == 1 || bankPassBookDetails[i].passbooktype_id == 3)
                            {
                                dOutstandindTillDateAmount = (double)(dOutstandindTillDateAmount + bankPassBookDetails[i].amount);
                            }
                            else if (bankPassBookDetails[i].passbooktype_id == 2 || bankPassBookDetails[i].passbooktype_id == 4) //else if(bankPassBookDetails[i].accountType_id == 2 || bankPassBookDetails[i].accountType_id == 10 || bankPassBookDetails[i].accountType_id == 11)
                                dOutstandindTillDateAmount = (double)(dOutstandindTillDateAmount - bankPassBookDetails[i].amount);

                        }
                    }
                    bankPassBookDetails = db.passbook_details.Where(x => x.party_id == lPartyId).ToList();
                    if (bankPassBookDetails != null || bankPassBookDetails.Count > 0)
                    {
                        for (int i = 0; i < bankPassBookDetails.Count; i++)
                        {
                            //if(bankPassBookDetails[i].accountType_id == 1 || bankPassBookDetails[i].accountType_id == 6 || bankPassBookDetails[i].accountType_id == 10)
                            if (bankPassBookDetails[i].passbooktype_id == 1 || bankPassBookDetails[i].passbooktype_id == 3)
                            {
                                dOutstandindTillDateAmount = (double)(dOutstandindTillDateAmount - bankPassBookDetails[i].amount);
                            }
                            else if (bankPassBookDetails[i].passbooktype_id == 2 || bankPassBookDetails[i].passbooktype_id == 4) //else if(bankPassBookDetails[i].accountType_id == 2 || bankPassBookDetails[i].accountType_id == 10 || bankPassBookDetails[i].accountType_id == 11)
                                dOutstandindTillDateAmount = (double)(dOutstandindTillDateAmount + bankPassBookDetails[i].amount);

                        }
                    }

                }
                else if (lAccountTypeId == 2)//Purchase
                {
                    List<purchase_details> totalFinishPurchaseBill = db.purchase_details.Where(x => x.partyid == lPartyId).ToList();
                    if (totalFinishPurchaseBill != null)
                    {
                        for (int i = 0; i < totalFinishPurchaseBill.Count; i++)
                        {
                            if (totalFinishPurchaseBill[i].purchase_typeid == 1 || totalFinishPurchaseBill[i].purchase_typeid == 2 || totalFinishPurchaseBill[i].purchase_typeid == 3 || totalFinishPurchaseBill[i].purchase_typeid == 4 || totalFinishPurchaseBill[i].purchase_typeid == 5)
                                dOutstandingAmount = (double)(dOutstandingAmount + totalFinishPurchaseBill[i].billamount);
                            else
                                dOutstandingAmount = (double)(dOutstandingAmount - totalFinishPurchaseBill[i].billamount);
                        }

                    }
                    List<greypurchase_details> totalGreyPurchaseBill = db.greypurchase_details.Where(x => x.partyid == lPartyId).ToList();
                    if (totalGreyPurchaseBill != null)
                    {
                        for (int i = 0; i < totalGreyPurchaseBill.Count; i++)
                        {
                            dOutstandingAmount = (double)(dOutstandingAmount + totalGreyPurchaseBill[i].netAmount);
                        }

                    }

                    List<creditdebitnote_details> purchaseBillCreditDebitNoteDetails = db.creditdebitnote_details.Where(x => x.party_id == lPartyId).ToList();
                    for (int j = 0; j < purchaseBillCreditDebitNoteDetails.Count; j++)//if debit note given for purchase then add the debit note amunt in outstanding amount
                    {
                        if (purchaseBillCreditDebitNoteDetails[j].cdNoteType_id == 4)
                            dOutstandingAmount = dOutstandingAmount - Convert.ToDouble(purchaseBillCreditDebitNoteDetails[j].netAmount);
                        else
                            dOutstandingAmount = dOutstandingAmount + Convert.ToDouble(purchaseBillCreditDebitNoteDetails[j].netAmount);
                    }
                    double dPaymentReceivedAmount = 0;
                    List<passbook_details> partyPassBookList = db.passbook_details.Where(x => x.party_id == lPartyId).Where(x => x.accountType_id == lAccountTypeId).ToList();
                    if (partyPassBookList != null)
                    {
                        for (int i = 0; i < partyPassBookList.Count; i++)
                        {
                            List<passbook_itemdetails> partyItemPassBookList = partyPassBookList[i].passbook_itemdetails.ToList();

                            if (partyItemPassBookList != null || partyItemPassBookList.Count > 0)
                            {
                                for (int j = 0; j < partyItemPassBookList.Count; j++)
                                {
                                    double dReceiveAmount = (double)(partyItemPassBookList[j].addLess + partyItemPassBookList[j].commAmount + partyItemPassBookList[j].discountAmount + partyItemPassBookList[j].others + partyItemPassBookList[j].paymentReceivedAmount + partyItemPassBookList[j].rdAmount + partyItemPassBookList[j].rgAmount + partyItemPassBookList[j].tdsAmount);
                                    dPaymentReceivedAmount = dPaymentReceivedAmount + dReceiveAmount;
                                }

                            }
                            else
                            {
                                dPaymentReceivedAmount = dPaymentReceivedAmount + (double)partyPassBookList[i].amount;
                            }
                        }
                    }
                    dOutstandindTillDateAmount = dOutstandingAmount - dPaymentReceivedAmount;
                }
                else if (lAccountTypeId == 10)//Mill
                {
                    List<millreceipt_detail> totalMillReceiptBill = db.millreceipt_detail.Where(x => x.mill_id == lPartyId).Where(x => x.isDeleted == false).ToList();
                    if (totalMillReceiptBill != null)
                    {
                        for (int i = 0; i < totalMillReceiptBill.Count; i++)
                            dOutstandingAmount = (double)(dOutstandingAmount + totalMillReceiptBill[i].invoiceValue);
                    }

                    double dPaymentReceivedAmount = 0;
                    List<passbook_details> partyPassBookList = db.passbook_details.Where(x => x.party_id == lPartyId).Where(x => x.accountType_id == lAccountTypeId).ToList();
                    if (partyPassBookList != null)
                    {
                        for (int i = 0; i < partyPassBookList.Count; i++)
                        {
                            List<passbook_itemdetails> partyItemPassBookList = partyPassBookList[i].passbook_itemdetails.ToList();

                            if (partyItemPassBookList != null || partyItemPassBookList.Count > 0)
                            {
                                for (int j = 0; j < partyItemPassBookList.Count; j++)
                                {
                                    double dReceiveAmount = (double)(partyItemPassBookList[j].addLess + partyItemPassBookList[j].commAmount + partyItemPassBookList[j].discountAmount + partyItemPassBookList[j].others + partyItemPassBookList[j].paymentReceivedAmount + partyItemPassBookList[j].rdAmount + partyItemPassBookList[j].rgAmount + partyItemPassBookList[j].tdsAmount);
                                    dPaymentReceivedAmount = dPaymentReceivedAmount + dReceiveAmount;
                                }

                            }
                            else
                            {
                                dPaymentReceivedAmount = dPaymentReceivedAmount + (double)partyPassBookList[i].amount;
                            }
                        }
                    }
                    dOutstandindTillDateAmount = dOutstandingAmount - dPaymentReceivedAmount;
                }
                else if (lAccountTypeId == 11)//JOB WORK
                {
                    List<jobreceive_billdetail> totalJobWorkReceiptBillDetail = db.jobreceive_billdetail.Where(x => x.party_id == lPartyId).ToList();
                    if (totalJobWorkReceiptBillDetail != null)
                    {
                        for (int i = 0; i < totalJobWorkReceiptBillDetail.Count; i++)
                            dOutstandingAmount = (double)(dOutstandingAmount + totalJobWorkReceiptBillDetail[i].netAmount);
                    }

                    double dPaymentReceivedAmount = 0;
                    List<passbook_details> partyPassBookList = db.passbook_details.Where(x => x.party_id == lPartyId).Where(x => x.accountType_id == lAccountTypeId).ToList();
                    if (partyPassBookList != null)
                    {
                        for (int i = 0; i < partyPassBookList.Count; i++)
                        {
                            List<passbook_itemdetails> partyItemPassBookList = partyPassBookList[i].passbook_itemdetails.ToList();

                            if (partyItemPassBookList != null || partyItemPassBookList.Count > 0)
                            {
                                for (int j = 0; j < partyItemPassBookList.Count; j++)
                                {
                                    double dReceiveAmount = (double)(partyItemPassBookList[j].addLess + partyItemPassBookList[j].commAmount + partyItemPassBookList[j].discountAmount + partyItemPassBookList[j].others + partyItemPassBookList[j].paymentReceivedAmount + partyItemPassBookList[j].rdAmount + partyItemPassBookList[j].rgAmount + partyItemPassBookList[j].tdsAmount);
                                    dPaymentReceivedAmount = dPaymentReceivedAmount + dReceiveAmount;
                                }

                            }
                            else
                            {
                                dPaymentReceivedAmount = dPaymentReceivedAmount + (double)partyPassBookList[i].amount;
                            }
                        }
                    }
                    dOutstandindTillDateAmount = dOutstandingAmount - dPaymentReceivedAmount;
                }
                else
                {
                    double dPartyTransactionAmount = 0;
                    List<passbook_details> partyPassBookList = db.passbook_details.Where(x => x.party_id == lPartyId).Where(x => x.accountType_id == lAccountTypeId).ToList();
                    for (int i = 0; i < partyPassBookList.Count; i++)
                    {
                        if (partyPassBookList[i].passbooktype_id == 1 || partyPassBookList[i].passbooktype_id == 3)
                        {
                            dPartyTransactionAmount = dPartyTransactionAmount + (double)partyPassBookList[i].amount;
                        }
                        else
                            dPartyTransactionAmount = dPartyTransactionAmount - (double)partyPassBookList[i].amount;
                    }
                    dOutstandindTillDateAmount = dPartyTransactionAmount;
                }
                db.Dispose();

                return dOutstandindTillDateAmount;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public jobreceive_billdetail getReceiveJobWorkBillDetail(long lVoucherNo)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                jobreceive_billdetail workBillDetail = db.jobreceive_billdetail.Where(p => p.billVoucherNo == lVoucherNo).SingleOrDefault();
                if (workBillDetail == null)
                {
                    return null;
                }
                List<jobprocessreceipt_itemdetails> receiveItemDetails = workBillDetail.jobprocessreceipt_itemdetails.ToList();
                workBillDetail.jobprocessreceipt_itemdetails = receiveItemDetails;
                db.Dispose();
                return workBillDetail;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public double getOutstandingFromJournalEntryData(long lPartyId)
        {
            try
            {
                double dOutstandingAmount = 0;
                var db = DBHelper2.GetDbContext(dbName);
                var creditAmountList = db.journalentry_detail.Where(x => x.creditAccount_id == lPartyId).Select(x => x.amount).ToList();
                if (creditAmountList != null && creditAmountList.Count > 0)
                {
                    for (int i = 0; i < creditAmountList.Count; i++)
                        dOutstandingAmount = dOutstandingAmount + creditAmountList[i].Value;
                }
                var debitAmountList = db.journalentry_detail.Where(x => x.debitAccount_id == lPartyId).Select(x => x.amount).ToList();
                if (debitAmountList != null && debitAmountList.Count > 0)
                {
                    for (int i = 0; i < debitAmountList.Count; i++)
                        dOutstandingAmount = dOutstandingAmount - debitAmountList[i].Value;
                }
                db.Dispose();
                return dOutstandingAmount;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public string getSalesBillNo()
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                var billNoList = db.sales_details.Select(p => p.billno).ToList();
                var result = (from m in billNoList select m).Max();
                db.Dispose();
                return result;
            }
            catch (Exception ex)
            {
                return "-1";
            }
        }

        public long getJournalEntryVoucherNo()
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                var voucherNoList = db.journalentry_detail.Select(p => p.voucherNo).ToList();
                if (voucherNoList.Count == 0)
                    return 0;
                var result = (long)(from m in voucherNoList select m).Max();
                db.Dispose();
                return result;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }

        public long getCreditDebitNoteVoucherNo(long lCDNoteTypeID)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                var voucherNoList = db.creditdebitnote_details.Where(p => p.cdNoteType_id == lCDNoteTypeID).Select(p => p.voucherNo).ToList();
                if (voucherNoList.Count == 0)
                    return 0;
                var result = (long)(from m in voucherNoList select m).Max();
                db.Dispose();
                return result;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }
        public List<double> getTakaDetailsForReferenceBillNo(string refBillNo)
        {
            try
            {
                var outstandingGreyTakaList = new List<double>();
                long lTotalTaka = 0;
                double dTotalMtrs = 0;
                var db = DBHelper2.GetDbContext(dbName);
                List<milldispatchchallan_detail> challanDetailList = db.milldispatchchallan_detail.Where(x => x.reference_billNo == refBillNo).Where(x => x.isDeleted == false).ToList();
                if (challanDetailList != null)
                {
                    for (int j = 0; j < challanDetailList.Count; j++)
                    {
                        lTotalTaka = lTotalTaka + (long)challanDetailList[j].totalTakas;
                        dTotalMtrs = dTotalMtrs + (double)challanDetailList[j].totalMtrs;
                    }

                }
                else
                    return null;

                db.Dispose();
                outstandingGreyTakaList.Add(Convert.ToDouble(lTotalTaka));
                outstandingGreyTakaList.Add(dTotalMtrs);
                return outstandingGreyTakaList;
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public List<milldispatchchallan_detail> getChallanDetailsForReferenceBillNo(string refBillNo, long lVoucherNo)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                List<milldispatchchallan_detail> challanDetailList = db.milldispatchchallan_detail.Where(x => x.reference_billNo == refBillNo).Where(x => x.voucherNo != lVoucherNo).Where(x => x.isDeleted == false).ToList();
                db.Dispose();
                return challanDetailList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public long getGreyPurchaseBillNo(long lGreyPurchaseType)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                long lSrNo = (long)db.greypurchase_details.Where(p => p.greyPurchaseType_id == lGreyPurchaseType).Select(p => p.greypurchase_id).DefaultIfEmpty(0).Max(); ;
                db.Dispose();
                return lSrNo;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }

        public long getMillDispatchVoucherNo()
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                long lSrNo = (long)db.milldispatch_detail.Select(p => p.voucherNo).DefaultIfEmpty(0).Max();
                db.Dispose();
                return lSrNo;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }

        public journalentry_detail getJournalEntryDetail(long lVoucherNo)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                journalentry_detail voucherEntryDetail = db.journalentry_detail.Where(x => x.voucherNo == lVoucherNo).Single();
                db.Dispose();
                return voucherEntryDetail;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public long getMillReceiptVoucherNo()
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                long lSrNo = (long)db.millreceipt_detail.Where(p => p.receiptVoucherNo != null).Select(p => p.receiptVoucherNo).DefaultIfEmpty(0).Max();
                db.Dispose();
                return lSrNo;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }

        public long getJobWorkReceiptDefaultVoucherNo()
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                long lSrNo = (long)db.jobprocessreceipt_itemdetails.Where(p => p.receiveVoucherNo != null).Select(p => p.receiveVoucherNo).DefaultIfEmpty(0).Max();
                db.Dispose();
                return lSrNo;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }
        public long getJobWorkDefaultVoucherNo()
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                long lSrNo = (long)db.jobreceive_billdetail.Where(p => p.billVoucherNo != null).Select(p => p.billVoucherNo).DefaultIfEmpty(0).Max();
                db.Dispose();
                return lSrNo;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }

        public long getPassBookDefaultVoucherNo(long accountTypeId)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                long lSrNo = (long)db.passbook_details.Where(p => p.passbooktype_id == accountTypeId).Select(p => p.voucherNo).DefaultIfEmpty(0).Max();
                db.Dispose();
                return lSrNo;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }

        public passbook_details getPassBookDetail(long lVoucherNo, long lPassBookTypeID)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                passbook_details passBookData = db.passbook_details.Where(x => x.voucherNo == lVoucherNo).Where(x => x.passbooktype_id == lPassBookTypeID).SingleOrDefault();
                if (passBookData == null)
                    return null;
                List<passbook_itemdetails> passBookDataItemList = passBookData.passbook_itemdetails.ToList();
                passBookData.passbook_itemdetails = passBookDataItemList;
                db.Dispose();
                return passBookData;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public string getJobWorkDefaultChallanNo()
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                var srNoList = db.jobprocess_details.Where(p => p.challanNo != null).Select(p => p.challanNo).ToList();
                var result = (from m in srNoList select m).Max();
                db.Dispose();
                return result;
            }
            catch (Exception ex)
            {
                return "-1";
            }
        }

        public jobprocess_details getJobDispatchData(string challanNo)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                jobprocess_details joProcessDetailData = db.jobprocess_details.Where(x => x.challanNo == challanNo).SingleOrDefault();
                if (joProcessDetailData == null)
                    return null;
                List<jobprocess_itemdetails> tempJobItemData = joProcessDetailData.jobprocess_itemdetails.ToList();
                joProcessDetailData.jobprocess_itemdetails = tempJobItemData;
                return joProcessDetailData;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<jobprocessreceipt_itemdetails> getJobReceiveData(long lReceiveVoucherId)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                List<jobprocessreceipt_itemdetails> joProcessDetailDataList = db.jobprocessreceipt_itemdetails.Where(x => x.receiveVoucherNo == lReceiveVoucherId).Where(x => x.jobReceiveBillDetail_id == null).Where(x => x.isDeleted == false).ToList();
                if (joProcessDetailDataList == null)
                    return null;

                return joProcessDetailDataList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public string getMillDispatchChallanNo()
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                var challanNoList = db.milldispatchchallan_detail.Where(x => x.isDeleted == false).Select(p => p.challanNo).ToList();
                var result = (from m in challanNoList select m).Max();
                db.Dispose();
                return result;
            }
            catch (Exception ex)
            {
                return "-1";
            }
        }


        public milldispatchchallan_detail getMillChallanDetails(string challanNo)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                milldispatchchallan_detail challanDetail = db.milldispatchchallan_detail.Where(x => x.isDeleted == false).Where(x => x.challanNo == challanNo).SingleOrDefault();
                db.Dispose();
                return challanDetail;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<millchallandispatchtaka_detail> getMillChallanTakaDetails(long lChallanId)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                List<millchallandispatchtaka_detail> challanTakaDetail = db.millchallandispatchtaka_detail.Where(x => x.isDeleted == false).Where(x => x.challan_id == lChallanId).ToList();
                db.Dispose();
                return challanTakaDetail;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public sales_details getSalesBillDetail(string strBillNo)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                sales_details saleBillDetail = db.sales_details.Where(x => x.billno == strBillNo).Where(x => x.salesTypeid < 5).SingleOrDefault();
                db.Dispose();
                return saleBillDetail;
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public creditdebitnote_details getCDNoteDetail(long lVoucherNo, long lCDNoteTypeID)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                creditdebitnote_details cdNoteDetail = db.creditdebitnote_details.Where(x => x.voucherNo == lVoucherNo).Where(x => x.cdNoteType_id == lCDNoteTypeID).SingleOrDefault();
                db.Dispose();
                return cdNoteDetail;
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public List<creditdebitnote_details> getCDNoteDetailForSaleBill(string billNo, long lPartyID)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                List<creditdebitnote_details> cdNoteDetailsForSaleBill = db.creditdebitnote_details.Where(x => x.refBillNo == billNo).Where(x => x.party_id == lPartyID).ToList();
                db.Dispose();
                return cdNoteDetailsForSaleBill;
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public List<purchase_details> getOutstandingPurchaseBillDetails(long lPartyID)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                List<purchase_details> outstandingPurchaseBill = new List<purchase_details>();
                List<purchase_details> purchaseBillDetails = db.purchase_details.Where(x => x.partyid == lPartyID).ToList();
                for (int i = 0; i < purchaseBillDetails.Count; i++)
                {
                    double dBillOutstandingAmount = 0;
                    bool bUpdateExistingBillForPurchaseReturn = false;
                    string strBillNo = purchaseBillDetails[i].billno;
                    if (purchaseBillDetails[i].purchase_typeid == 1 || purchaseBillDetails[i].purchase_typeid == 2 || purchaseBillDetails[i].purchase_typeid == 3 || purchaseBillDetails[i].purchase_typeid == 4 || purchaseBillDetails[i].purchase_typeid == 5)
                    {
                        dBillOutstandingAmount = Convert.ToDouble(purchaseBillDetails[i].billamount);

                        List<passbook_itemdetails> passBookDetails = db.passbook_itemdetails.Where(x => x.billNo == strBillNo).ToList();
                        for (int j = 0; j < passBookDetails.Count; j++)
                        {
                            dBillOutstandingAmount = dBillOutstandingAmount - Convert.ToDouble(passBookDetails[j].totalReceivedAmount);
                        }

                        List<creditdebitnote_details> cdNoteBillDetails = db.creditdebitnote_details.Where(x => x.refBillNo == strBillNo).Where(x => x.party_id == lPartyID).ToList();
                        for (int k = 0; k < cdNoteBillDetails.Count; k++)
                        {
                            if (cdNoteBillDetails[k].cdNoteType_id == 2)
                                dBillOutstandingAmount = dBillOutstandingAmount - Convert.ToDouble(cdNoteBillDetails[k].netAmount);
                            else
                                dBillOutstandingAmount = dBillOutstandingAmount + Convert.ToDouble(cdNoteBillDetails[k].netAmount);
                        }
                        var saleBillDetail = purchaseBillDetails[i];
                        saleBillDetail.billamount = dBillOutstandingAmount;
                        purchaseBillDetails[i] = saleBillDetail;
                    }
                    else
                    {
                        if (outstandingPurchaseBill.Count > 0)
                        {
                            bUpdateExistingBillForPurchaseReturn = true;

                            purchase_details purchaseBillReturn = outstandingPurchaseBill.Where(x => x.billno == purchaseBillDetails[i].refBillNo).SingleOrDefault();
                            purchaseBillReturn.billamount = purchaseBillReturn.billamount - purchaseBillDetails[i].billamount;
                            int iSaleBillIndex = outstandingPurchaseBill.FindIndex(x => x.billno == purchaseBillDetails[i].refBillNo);
                            outstandingPurchaseBill[iSaleBillIndex] = purchaseBillReturn;
                        }

                    }

                    if (dBillOutstandingAmount > 0 && !bUpdateExistingBillForPurchaseReturn)
                    {
                        outstandingPurchaseBill.Add(purchaseBillDetails[i]);
                    }

                }
                db.Dispose();
                if (outstandingPurchaseBill.Count == 0)
                    return null;
                else
                    return outstandingPurchaseBill;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<sales_details> getOutstandingSalesBillDetails(long lPartyID)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                List<sales_details> outstandingSalesBill = new List<sales_details>();
                List<sales_details> salesBillDetails = db.sales_details.Where(x => x.partyid == lPartyID).ToList();
                for (int i = 0; i < salesBillDetails.Count; i++)
                {
                    double dBillOutstandingAmount = 0;
                    bool bUpdateExistingBillForSalesReturn = false;
                    string strBillNo = salesBillDetails[i].billno;
                    if (salesBillDetails[i].salesTypeid == 1 || salesBillDetails[i].salesTypeid == 2 || salesBillDetails[i].salesTypeid == 3 || salesBillDetails[i].salesTypeid == 4)
                    {
                        dBillOutstandingAmount = salesBillDetails[i].billamount;

                        List<passbook_itemdetails> passBookDetails = db.passbook_itemdetails.Where(x => x.billNo == strBillNo).ToList();
                        for (int j = 0; j < passBookDetails.Count; j++)
                        {
                            dBillOutstandingAmount = dBillOutstandingAmount - Convert.ToDouble(passBookDetails[j].totalReceivedAmount);
                        }

                        List<creditdebitnote_details> cdNoteBillDetails = db.creditdebitnote_details.Where(x => x.refBillNo == strBillNo).Where(x => x.party_id == lPartyID).ToList();
                        for (int k = 0; k < cdNoteBillDetails.Count; k++)
                        {
                            if (cdNoteBillDetails[k].cdNoteType_id == 1)
                                dBillOutstandingAmount = dBillOutstandingAmount - Convert.ToDouble(cdNoteBillDetails[k].netAmount);
                            else
                                dBillOutstandingAmount = dBillOutstandingAmount + Convert.ToDouble(cdNoteBillDetails[k].netAmount);
                        }
                        var saleBillDetail = salesBillDetails[i];
                        saleBillDetail.billamount = dBillOutstandingAmount;
                        salesBillDetails[i] = saleBillDetail;
                    }
                    else
                    {
                        if (outstandingSalesBill.Count > 0)
                        {
                            bUpdateExistingBillForSalesReturn = true;

                            sales_details saleBillReturn = outstandingSalesBill.Where(x => x.billno == salesBillDetails[i].orderno).SingleOrDefault();
                            saleBillReturn.billamount = saleBillReturn.billamount - salesBillDetails[i].billamount;
                            int iSaleBillIndex = outstandingSalesBill.FindIndex(x => x.billno == salesBillDetails[i].orderno);
                            outstandingSalesBill[iSaleBillIndex] = saleBillReturn;
                        }

                    }

                    if (dBillOutstandingAmount > 0 && !bUpdateExistingBillForSalesReturn)
                    {
                        outstandingSalesBill.Add(salesBillDetails[i]);
                    }

                }
                db.Dispose();
                if (outstandingSalesBill.Count == 0)
                    return null;
                else
                    return outstandingSalesBill;
            }
            catch (Exception ex)
            {
                return null;
            }

        }
        public sales_details getSalesBillDetail(long lPartyId, string strBillNo)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                sales_details saleBillDetail = db.sales_details.Where(x => x.billno == strBillNo).Where(x => x.partyid == lPartyId).SingleOrDefault();
                db.Dispose();
                return saleBillDetail;
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public millreceipt_detail getMillReceiptBillDetail(long lPartyID, string lBillNo)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                millreceipt_detail millReceiptBillDetail = db.millreceipt_detail.Where(x => x.billNo == lBillNo).Where(x => x.mill_id == lPartyID).SingleOrDefault();
                db.Dispose();
                return millReceiptBillDetail;
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public jobreceive_billdetail getJobWorkReceiptBillDetail(long lPartyID, string strBillNo)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                jobreceive_billdetail jobWorkReceiptBillDetail = db.jobreceive_billdetail.Where(x => x.billNo == strBillNo).Where(x => x.party_id == lPartyID).SingleOrDefault();
                db.Dispose();
                return jobWorkReceiptBillDetail;
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public List<sales_details> getSalesBillDetailFromPartyID(long lPartyId)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                List<sales_details> saleBillDetail = db.sales_details.Where(x => x.partyid == lPartyId).ToList();
                db.Dispose();
                return saleBillDetail;
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public List<purchase_details> getPurchaseBillDetailFromPartyID(long lPartyId)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                List<purchase_details> purchaseBillDetail = db.purchase_details.Where(x => x.partyid == lPartyId).ToList();
                db.Dispose();
                return purchaseBillDetail;
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public List<millreceipt_detail> getMillBillDetails(long lPartyId)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                List<millreceipt_detail> millReceiptBillDetail = db.millreceipt_detail.Where(x => x.mill_id == lPartyId).Where(x => x.isDeleted == false).ToList();
                db.Dispose();
                return millReceiptBillDetail;
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public List<jobreceive_billdetail> getJobWorkReceiptBillDetails(long lPartyId)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                List<jobreceive_billdetail> jobWorkReceiptBillDetail = db.jobreceive_billdetail.Where(x => x.party_id == lPartyId).ToList();
                db.Dispose();
                return jobWorkReceiptBillDetail;
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public purchase_details getPurchaseBillDetail(long lPartyId, string billNo)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                purchase_details purchaseBillDetail = db.purchase_details.Where(x => x.partyid == lPartyId).Where(x => x.billno == billNo).SingleOrDefault();
                db.Dispose();
                return purchaseBillDetail;
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public greypurchase_details getGreyPurchaseBillDetail(long lPartyId, string strBillNo)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                greypurchase_details purchaseBillDetail = db.greypurchase_details.Where(x => x.partyid == lPartyId).Where(x => x.billNo == strBillNo).SingleOrDefault();
                db.Dispose();
                return purchaseBillDetail;
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public List<greypurchase_details> getGreyPurchaseBillDetailFromPartyID(long lPartyId)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                List<greypurchase_details> greyPurchaseBillDetail = db.greypurchase_details.Where(x => x.partyid == lPartyId).Where(x => x.greyPurchaseType_id == 1).ToList();
                db.Dispose();
                return greyPurchaseBillDetail;
            }
            catch (Exception ex)
            {
                return null;
            }

        }
        public List<passbook_itemdetails> getReceivedBillAmount(long lPartyId, long lAccountTypeId, string lBillNo)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                List<passbook_details> passBookBillDetailList = db.passbook_details.Where(x => x.party_id == lPartyId).Where(x => x.accountType_id == lAccountTypeId).ToList();
                if (passBookBillDetailList == null)
                    return null;
                List<passbook_itemdetails> receivedBillDetailList = new List<passbook_itemdetails>();
                for (int i = 0; i < passBookBillDetailList.Count; i++)
                {
                    List<passbook_itemdetails> passBookBillItemDetailList = passBookBillDetailList[i].passbook_itemdetails.ToList();
                    passbook_itemdetails receivedBillDetail = passBookBillItemDetailList.Where(x => x.billNo == lBillNo).SingleOrDefault();
                    if (receivedBillDetail != null)
                        receivedBillDetailList.Add(receivedBillDetail);
                }


                db.Dispose();
                return receivedBillDetailList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<sales_details> getReceivedRGAmount(string strRefBillNo, long lPartyID)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                List<sales_details> rgReceivedList = db.sales_details.Where(x => x.orderno == strRefBillNo).Where(x => x.partyid == lPartyID).ToList();
                db.Dispose();
                return rgReceivedList;

            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<purchase_details> getPurchaseRGAmount(string strRefBillNo, long lPartyID)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                List<purchase_details> rgReceivedList = db.purchase_details.Where(x => x.refBillNo == strRefBillNo).Where(x => x.partyid == lPartyID).ToList();
                db.Dispose();
                return rgReceivedList;

            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public greypurchase_details getGreyPurchaseBillDetail(long lSrNo)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                greypurchase_details purchaseBillDetail = db.greypurchase_details.Where(x => x.greypurchase_id == lSrNo).SingleOrDefault();
                db.Dispose();
                return purchaseBillDetail;
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public List<millchallandispatchtaka_detail> getMillReceivedaTakaDetailForReprocess(long lChallanID)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                List<millreceipt_detail> receivedMillTakaList = db.millreceipt_detail.Where(x => x.isDeleted == false).ToList();
                if (receivedMillTakaList == null)
                    return null;
                List<millchallandispatchtaka_detail> millReceivedChallanTakaDetailList = new List<millchallandispatchtaka_detail>();
                for (int i = 0; i < receivedMillTakaList.Count; i++)
                {
                    List<millchallandispatchtaka_detail> millReceivedTakaDetailList = receivedMillTakaList[i].millchallandispatchtaka_detail.ToList();
                    List<millchallandispatchtaka_detail> tempMillReceivedChallanTakaDetailList = millReceivedTakaDetailList.Where(x => x.challan_id == lChallanID).Where(x => x.isReceived == true).Where(x => x.reProcessTaka == 1).ToList();
                    for (int j = 0; j < tempMillReceivedChallanTakaDetailList.Count; j++)
                    {
                        tempMillReceivedChallanTakaDetailList[j].milldispatchchallan_detail = tempMillReceivedChallanTakaDetailList[j].milldispatchchallan_detail;
                        millReceivedChallanTakaDetailList.Add(tempMillReceivedChallanTakaDetailList[j]);
                    }

                }
                db.Dispose();
                return millReceivedChallanTakaDetailList;
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public List<milldispatchchallan_detail> getReferenceChallanDetails(string challanNo)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                List<milldispatchchallan_detail> referenceChallanList = db.milldispatchchallan_detail.Where(x => x.reference_challanNo == challanNo).ToList();
                db.Dispose();
                return referenceChallanList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<milldispatchchallan_detail> getReferenceChallanForOtherVoucherDetails(string challanNo, long lVoucherNo)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                List<milldispatchchallan_detail> referenceChallanList = db.milldispatchchallan_detail.Where(x => x.reference_challanNo == challanNo).Where(x => x.voucherNo != lVoucherNo).ToList();
                db.Dispose();
                return referenceChallanList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public greypurchase_details getGreyPartyIDFromRefBillNo(long lBillNo)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                greypurchase_details greyQualityDetails = db.greypurchase_details.Where(p => p.greypurchase_id == lBillNo).SingleOrDefault();

                db.Dispose();
                return greyQualityDetails;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public long getMillDispatchChallanIDFromChallanNo(string challanNo)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                long lChallanID = db.milldispatchchallan_detail.Where(x => x.challanNo == challanNo).Select(x => x.challan_id).SingleOrDefault();
                db.Dispose();
                return lChallanID;
            }
            catch (Exception ex)
            {
                return -1;
            }

        }

        public string getRefBillNoForReprocessChallan(string challanNo)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                string refBillNo = db.milldispatchchallan_detail.Where(x => x.challanNo == challanNo).Select(x => x.reference_billNo).SingleOrDefault();
                db.Dispose();
                return refBillNo;
            }
            catch (Exception ex)
            {
                return "-1";
            }
        }
        public List<purchase_details> getFinishPurchaseDetail()
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                List<purchase_details> purchaseBillDetail = db.purchase_details.Where(x => x.purchase_typeid == 1).ToList();
                if (purchaseBillDetail == null)
                    return null;
                for (int i = 0; i < purchaseBillDetail.Count; i++)
                {
                    List<purchaseitem_details> itemDetailList = purchaseBillDetail[i].purchaseitem_details.ToList();
                    purchaseBillDetail[i].purchaseitem_details = itemDetailList;
                }
                db.Dispose();
                return purchaseBillDetail;
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public List<jobprocess_itemdetails> ckeckIfFinishPurchaseReferenceInJobWorkExists(long lItemID)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                List<jobprocess_itemdetails> finishPurchaseItemList = db.jobprocess_itemdetails.Where(x => x.dispatchReference_id == lItemID).ToList();
                if (finishPurchaseItemList == null)
                    return null;
                return finishPurchaseItemList;
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public List<salesitem_details> getItemDetailsOfSaleBill(long lBillNo)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                List<salesitem_details> saleItemBillDetail = db.salesitem_details.Where(x => x.salesbillid == lBillNo).ToList();
                db.Dispose();
                return saleItemBillDetail;
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public milldispatch_detail getMillDispatchVoucherData(long lVoucherNo)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                milldispatch_detail millDispatchDetail = db.milldispatch_detail.Where(x => x.voucherNo == lVoucherNo).SingleOrDefault();
                db.Dispose();
                return millDispatchDetail;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool deleteSaleBill(long lBillNo)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                sales_details deleteBillDetail = db.sales_details.Where(x => x.salesbillid == lBillNo).SingleOrDefault();
                db.sales_details.Remove(deleteBillDetail);
                db.SaveChanges();
                db.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public bool deleteCDNoteVoucher(long lVoucherNo, long lCDNoteTypeID)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                creditdebitnote_details deleteCNVoucherDetail = db.creditdebitnote_details.Where(x => x.voucherNo == lVoucherNo).Where(x => x.cdNoteType_id == lCDNoteTypeID).SingleOrDefault();
                db.creditdebitnote_details.Remove(deleteCNVoucherDetail);
                db.SaveChanges();
                db.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }
        public bool deletePassBookVoucher(long lVoucherNo, long lPassBookType)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                passbook_details deleteVoucherDetail = db.passbook_details.Where(x => x.voucherNo == lVoucherNo).Where(x => x.passbooktype_id == lPassBookType).SingleOrDefault();
                db.passbook_details.Remove(deleteVoucherDetail);
                db.SaveChanges();
                db.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public bool deletePurchaseBill(long lVoucherNo, long lPurchaseTypeID)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                purchase_details deleteBillDetail = db.purchase_details.Where(x => x.voucherNo == lVoucherNo).Where(x => x.purchase_typeid == lPurchaseTypeID).SingleOrDefault();
                db.purchase_details.Remove(deleteBillDetail);
                db.SaveChanges();
                db.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public bool deleteJobDispatchBill(string challanNo)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                jobprocess_details deleteJobWorkDispatchDetail = db.jobprocess_details.Where(x => x.challanNo == challanNo).SingleOrDefault();
                db.jobprocess_details.Remove(deleteJobWorkDispatchDetail);
                db.SaveChanges();
                db.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public bool checkJobDispatchChallanUsageBeforeDelete(string challanNo)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                List<jobprocessreceipt_itemdetails> challanDetailsList = db.jobprocessreceipt_itemdetails.Where(x => x.challanNo == challanNo).Where(x => x.isDeleted == false).ToList();
                db.Dispose();
                if (challanDetailsList != null && challanDetailsList.Count > 0)
                    return false;
                else
                    return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public bool checkJobReceiveChallanUsageBeforeDelete(long lReceiveVoucherNo)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                List<jobprocessreceipt_itemdetails> challanDetailsList = db.jobprocessreceipt_itemdetails.Where(x => x.receiveVoucherNo == lReceiveVoucherNo).Where(x => x.jobReceiveBillDetail_id != null).Where(x => x.isDeleted == false).ToList();
                db.Dispose();
                if (challanDetailsList != null && challanDetailsList.Count > 0)
                    return false;
                else
                    return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public bool checkJobReceiptBillUsageBeforeDelete(long lVoucherNo)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                jobreceive_billdetail billDetail = db.jobreceive_billdetail.Where(x => x.billVoucherNo == lVoucherNo).SingleOrDefault();
                List<jobprocess_itemdetails> challanDetailsList = null;
                bool bChallanReused = false;
                if (billDetail != null)
                {
                    List<jobprocessreceipt_itemdetails> receiveItemList = billDetail.jobprocessreceipt_itemdetails.ToList();
                    for (int i = 0; i < receiveItemList.Count; i++)
                    {
                        long lJobWorkReceiptId = receiveItemList[i].jobWorkReceiptId;
                        challanDetailsList = db.jobprocess_itemdetails.Where(x => x.receiptReference_id == lJobWorkReceiptId).ToList();
                        if (challanDetailsList != null)
                        {
                            bChallanReused = true;
                            break;
                        }

                    }
                }
                db.Dispose();
                if (bChallanReused)
                    return false;
                else
                    return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }
        public bool deleteGreyPurchaseBill(long lVoucherNo, long lGreyPurchaseType)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                greypurchase_details deleteBillDetail = db.greypurchase_details.Where(x => x.greypurchase_id == lVoucherNo).Where(x => x.greyPurchaseType_id == lGreyPurchaseType).SingleOrDefault();
                db.greypurchase_details.Remove(deleteBillDetail);
                db.SaveChanges();
                db.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }
        public bool deleteJournalEntryVoucher(long lVoucherNo)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                journalentry_detail deleteJournalEntryDetail = db.journalentry_detail.Where(x => x.voucherNo == lVoucherNo).SingleOrDefault();
                db.journalentry_detail.Remove(deleteJournalEntryDetail);
                db.SaveChanges();
                db.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }
        public bool deleteMillDispatchChallan(long lVoucherNo)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                milldispatch_detail deleteMillVoucherDetail = db.milldispatch_detail.Where(x => x.voucherNo == lVoucherNo).SingleOrDefault();
                db.milldispatch_detail.Remove(deleteMillVoucherDetail);
                db.SaveChanges();
                db.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }
        public bool deleteGreyPurchaseTakaDetail(long lTakaId)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                greytaka_details deleteTakaDetail = db.greytaka_details.Where(x => x.takadetails_id == lTakaId).SingleOrDefault();
                db.greytaka_details.Remove(deleteTakaDetail);
                db.SaveChanges();
                db.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public bool checkBillExists(string strBillNo)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                int iCount = db.sales_details.Where(x => x.billno == strBillNo).Count();
                db.Dispose();
                if (iCount > 0)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public bool checkPurchaseBillExists(long lVoucherNo, string billNo, long lPartyId)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                long lCount = db.purchase_details.Where(x => x.billno == billNo).Where(x => x.partyid == lPartyId).Select(x => x.purchasebillid).SingleOrDefault();
                db.Dispose();
                if ((lCount == lVoucherNo) || lCount == 0)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public bool checkGreyPurchaseBillExists(long lSrNo, string strBillNo, long lPartyId)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                long lCount = (long)db.greypurchase_details.Where(x => x.billNo == strBillNo).Where(x => x.partyid == lPartyId).Select(x => x.greypurchase_id).SingleOrDefault();
                db.Dispose();
                if ((lCount == lSrNo) || lCount == 0)
                    return false;
                else
                    return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public bool checkGreyPurchaseSrnoExists(long lSrNo, long lGreyPurchaseType)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                long lCount = db.greypurchase_details.Where(x => x.greypurchase_id == lSrNo).Where(x => x.greyPurchaseType_id == lGreyPurchaseType).Count();
                db.Dispose();
                if (lCount == 0)
                    return false;
                else
                    return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }
        public bool updateSalesBill(sales_details salesBill)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                var version = salesBill.version;
                salesBill.version = null;
                var existingParent = db.sales_details
        .Where(p => p.salesbillid == salesBill.salesbillid)
        .Include(p => p.salesitem_details)
        .SingleOrDefault();

                if (existingParent != null)
                {
                    // Update parent
                    db.Entry(existingParent).CurrentValues.SetValues(salesBill);

                    db.Entry(existingParent).OriginalValues["version"] = version;
                    // Update and Insert children
                    foreach (var childModel in salesBill.salesitem_details)
                    {
                        childModel.version = null;
                        var existingChild = db.salesitem_details
                         .Where(c => c.item_id == childModel.item_id)
                         .SingleOrDefault();

                        if (existingChild != null && childModel.item_id != 0)
                            // Update child
                            db.Entry(existingChild).CurrentValues.SetValues(childModel);
                        else
                        {

                            existingParent.salesitem_details.Add(childModel);
                        }
                    }

                    db.SaveChanges();
                }

                db.Dispose();
                return true;
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public bool updatePassBookVoucher(passbook_details passBookData)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                var version = passBookData.version;
                //passBookData.version = null;
                var existingParent = db.passbook_details
        .Where(p => p.passBookDetails_id == passBookData.passBookDetails_id)
        .Include(p => p.passbook_itemdetails)
        .SingleOrDefault();

                if (existingParent != null)
                {
                    // Update parent
                    db.Entry(existingParent).CurrentValues.SetValues(passBookData);

                    db.Entry(existingParent).OriginalValues["version"] = version;
                    // Update and Insert children
                    foreach (var childModel in passBookData.passbook_itemdetails)
                    {
                        //childModel.version = null;
                        var existingChild = existingParent.passbook_itemdetails
                         .Where(c => c.passbooItemDetails_id == childModel.passbooItemDetails_id)
                         .SingleOrDefault();

                        if (existingChild != null && childModel.passbooItemDetails_id != 0)
                            // Update child
                            db.Entry(existingChild).CurrentValues.SetValues(childModel);
                        else
                        {

                            existingParent.passbook_itemdetails.Add(childModel);
                        }
                    }

                    db.SaveChanges();
                }

                db.Dispose();
                return true;
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                return false;
            }

        }
        public bool updatePurchaseBill(purchase_details purchaseBill)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                var version = purchaseBill.version;
                //purchaseBill.version = null;
                var existingParent = db.purchase_details
        .Where(p => p.purchasebillid == purchaseBill.purchasebillid)
        .Include(p => p.purchaseitem_details)
        .SingleOrDefault();

                if (existingParent != null)
                {
                    // Update parent
                    db.Entry(existingParent).CurrentValues.SetValues(purchaseBill);

                    db.Entry(existingParent).OriginalValues["version"] = version;
                    // Update and Insert children
                    foreach (var childModel in purchaseBill.purchaseitem_details)
                    {
                        //childModel.version = DateTime.Now;
                        var existingChild = existingParent.purchaseitem_details
                         .Where(c => c.item_id == childModel.item_id)
                         .SingleOrDefault();

                        if (existingChild != null && childModel.item_id != 0)
                        {
                            // Update child
                            db.Entry(existingChild).CurrentValues.SetValues(childModel);

                        }
                        else
                        {

                            existingParent.purchaseitem_details.Add(childModel);
                        }
                    }

                    db.SaveChanges();
                }

                db.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool updateMillDispatchVoucher(milldispatch_detail millDispatchBill)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                db.milldispatch_detail.Add(millDispatchBill);
                //db.sales_details.Attach(salesBill);

                db.Entry(millDispatchBill).State = EntityState.Modified;
                foreach (milldispatchchallan_detail sd in millDispatchBill.milldispatchchallan_detail)
                {
                    db.Entry(sd).State = sd.challan_id == 0 ? EntityState.Added : EntityState.Modified;

                    foreach (millchallandispatchtaka_detail td in sd.millchallandispatchtaka_detail)
                    {
                        db.Entry(td).State = td.millTaka_id == 0 ? EntityState.Added : EntityState.Modified;
                    }

                }

                db.SaveChanges();
                db.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }
        public bool updateGreyPurchaseBill(greypurchase_details purchaseBill)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                db.greypurchase_details.Add(purchaseBill);
                //db.greypurchase_details.Attach(purchaseBill);

                db.Entry(purchaseBill).State = EntityState.Modified;
                foreach (greytaka_details sd in purchaseBill.greytaka_details)
                {
                    db.Entry(sd).State = sd.takadetails_id == 0 ? EntityState.Added : EntityState.Modified;
                    //db.Entry(sd).State = EntityState.Modified;
                }
                db.SaveChanges();
                db.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public bool updateJobWorkDispatchBill(jobprocess_details jobWorkDetail)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                var version = jobWorkDetail.version;
                jobWorkDetail.version = null;
                var existingParent = db.jobprocess_details
        .Where(p => p.jobprocess_id == jobWorkDetail.jobprocess_id)
        .Include(p => p.jobprocess_itemdetails)
        .SingleOrDefault();

                if (existingParent != null)
                {
                    // Update parent
                    db.Entry(existingParent).CurrentValues.SetValues(jobWorkDetail);

                    db.Entry(existingParent).OriginalValues["version"] = version;
                    // Update and Insert children
                    foreach (var childModel in jobWorkDetail.jobprocess_itemdetails)
                    {
                        childModel.version = null;
                        var existingChild = existingParent.jobprocess_itemdetails
                         .Where(c => c.item_id == childModel.item_id)
                         .SingleOrDefault();

                        if (existingChild != null && childModel.item_id != 0)
                            // Update child
                            db.Entry(existingChild).CurrentValues.SetValues(childModel);
                        else
                        {

                            existingParent.jobprocess_itemdetails.Add(childModel);
                        }
                    }

                    db.SaveChanges();
                }

                db.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public long getLastPurchaseBillNo(long lPurchaseTypeID)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                long lLastBillNo = (long)db.purchase_details.Where(p => p.purchase_typeid == lPurchaseTypeID).Select(p => p.voucherNo).DefaultIfEmpty(0).Max();
                db.Dispose();
                return lLastBillNo;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }

        public List<milldispatchchallan_detail> getOustandingChallanList(long lMillId)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                List<milldispatchchallan_detail> pendingMillChallanList = new List<milldispatchchallan_detail>();
                List<milldispatchchallan_detail> challanList = db.milldispatchchallan_detail.Where(x => x.mill_id == lMillId).Where(x => x.isDeleted == false).ToList();
                if (challanList != null)
                {
                    for (int i = 0; i < challanList.Count; i++)
                    {
                        List<millchallandispatchtaka_detail> takaList = challanList[i].millchallandispatchtaka_detail.ToList();
                        bool bTakaPending = false;
                        for (int j = 0; j < takaList.Count; j++)
                        {
                            if (!(bool)takaList[j].isReceived)
                            {
                                bTakaPending = true;
                                break;
                            }

                        }
                        if (bTakaPending)
                            pendingMillChallanList.Add(challanList[i]);
                    }
                    db.Dispose();
                    return pendingMillChallanList;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<milldispatchchallan_detail> getChallanDetail(long lChallanId)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                List<milldispatchchallan_detail> challanDetail = db.milldispatchchallan_detail.Where(x => x.challan_id == lChallanId).ToList();
                db.Dispose();
                return challanDetail;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public millreceipt_detail getMillReceiptVoucherDetail(long lVoucherNo)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);

                millreceipt_detail millReceiptTakaList = db.millreceipt_detail.Where(x => x.receiptVoucherNo == lVoucherNo).SingleOrDefault();
                if (millReceiptTakaList == null)
                    return null;
                List<millchallandispatchtaka_detail> tempTakaList = new List<millchallandispatchtaka_detail>();
                tempTakaList = millReceiptTakaList.millchallandispatchtaka_detail.ToList();
                millReceiptTakaList.millchallandispatchtaka_detail = tempTakaList;
                db.Dispose();
                return millReceiptTakaList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public List<greyquality_details> getGreyQualityList()
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                List<greyquality_details> greyQualityList = db.greyquality_details.Where(x => x.greyquality_id != null).ToList();
                db.Dispose();
                return greyQualityList;

            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<greypurchase_details> getGreyPurchasePurchaseRGAmount(string strRefBillNo, long lPartyID)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                List<greypurchase_details> rgReceivedList = db.greypurchase_details.Where(x => x.greyPurchaseType_id == 2).Where(x => x.orderNo == strRefBillNo).Where(x => x.partyid == lPartyID).ToList();
                db.Dispose();
                return rgReceivedList;

            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<jobprocess_itemdetails> getJobWorkPendingChallanList(long lPartyId)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);

                List<jobprocess_itemdetails> pendingChallanList = db.jobprocess_itemdetails.Where(x => x.voucherNo == null).Where(x => x.party_id == lPartyId).ToList();
                for (int i = 0; i < pendingChallanList.Count; i++)
                {
                    List<jobprocessreceipt_itemdetails> tempchallanReceiptList = pendingChallanList[i].jobprocessreceipt_itemdetails.ToList();
                    List<jobprocessreceipt_itemdetails> challanReceiptList = tempchallanReceiptList.Where(x => x.isDeleted == false).ToList();
                    pendingChallanList[i].jobprocessreceipt_itemdetails = challanReceiptList;
                }

                db.Dispose();

                return pendingChallanList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<jobprocess_itemdetails> getJobWorkPendingChallanItemList(string challanNo)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                List<jobprocess_itemdetails> pendingChallanItemList = db.jobprocess_itemdetails.Where(x => x.challanNo == challanNo).ToList();
                for (int i = 0; i < pendingChallanItemList.Count; i++)
                {
                    List<jobprocessreceipt_itemdetails> reciveItemList = pendingChallanItemList[i].jobprocessreceipt_itemdetails.ToList().Where(x => x.isDeleted == false).ToList();
                    pendingChallanItemList[i].jobprocessreceipt_itemdetails = reciveItemList;
                }
                db.Dispose();
                return pendingChallanItemList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<purchaseitem_details> getPurchaseBillItemDetails(long lPurchaseItemId)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                var itemDetails = db.purchaseitem_details.Where(x => x.item_id == lPurchaseItemId).ToList();
                if (itemDetails == null)
                    return null;
                return itemDetails;

            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public purchaseitem_details getPurchaseBillItemData(long lPurchaseItemId)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                var itemDetails = db.purchaseitem_details.Where(x => x.item_id == lPurchaseItemId).SingleOrDefault();
                if (itemDetails == null)
                    return null;
                return itemDetails;

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<jobprocessreceipt_itemdetails> getReceiveChallanItemList(long lpartyId)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                List<jobprocessreceipt_itemdetails> challanDetailsReceiveItemList = db.jobprocessreceipt_itemdetails.Where(x => x.party_id == lpartyId).Where(x => x.jobReceiveBillDetail_id == null).Where(x => x.isDeleted == false).ToList();
                db.Dispose();
                return challanDetailsReceiveItemList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<jobprocessreceipt_itemdetails> getReceiveChallanItemListForBillReceipt(long lpartyId)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                List<jobprocessreceipt_itemdetails> challanDetailsReceiveItemList = db.jobprocessreceipt_itemdetails.Where(x => x.party_id == lpartyId).Where(x => x.jobReceiveBillDetail_id == null).Where(x => x.isDeleted == false).ToList();
                db.Dispose();
                return challanDetailsReceiveItemList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<jobprocessreceipt_itemdetails> getReceiptBillItemList(long lpartyId)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                List<jobprocessreceipt_itemdetails> challanDetailsReceiveItemList = db.jobprocessreceipt_itemdetails.Where(x => x.party_id == lpartyId).Where(x => x.jobReceiveBillDetail_id != null).Where(x => x.isDeleted == false).ToList();
                db.Dispose();
                return challanDetailsReceiveItemList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<jobprocessreceipt_itemdetails> getReceiveChallanItemListForAdditionalJobWork()
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                List<jobprocessreceipt_itemdetails> challanDetailsReceiveItemList = db.jobprocessreceipt_itemdetails.Where(x => x.jobReceiveBillDetail_id == null).Where(x => x.multipleJobsOnItem == true).Where(x => x.isDeleted == false).ToList();
                List<jobprocess_itemdetails> itemDetailsToChkAdditionalJobWork = db.jobprocess_itemdetails.Where(x => x.receiptReference_id != null).ToList();
                List<int> additionalWorkChallanList = new List<int>();
                for (int i = 0; i < challanDetailsReceiveItemList.Count; i++)
                {
                    for (int j = 0; j < itemDetailsToChkAdditionalJobWork.Count; j++)
                    {
                        if (challanDetailsReceiveItemList[i].jobWorkReceiptId == itemDetailsToChkAdditionalJobWork[j].receiptReference_id)
                        {
                            additionalWorkChallanList.Add(i);

                            break;
                        }

                    }
                }
                for (int i = additionalWorkChallanList.Count - 1; i >= 0; i--)
                {
                    challanDetailsReceiveItemList.RemoveAt(additionalWorkChallanList[i]);
                }
                db.Dispose();
                return challanDetailsReceiveItemList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public jobprocessreceipt_itemdetails getReceiveChallanItemDataForAdditionalJobWork(long lJobWorkReceiptId)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                jobprocessreceipt_itemdetails challanDetailsReceiveItemList = db.jobprocessreceipt_itemdetails.Where(x => x.jobWorkReceiptId == lJobWorkReceiptId).Where(x => x.isDeleted == false).SingleOrDefault();
                db.Dispose();
                return challanDetailsReceiveItemList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public jobprocess_itemdetails getJobWorkDispatchChallanItemList(long lJobWorkProcessDispatchId)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                jobprocess_itemdetails pendingChallanItem = db.jobprocess_itemdetails.Where(x => x.jobProcessItemDetails_id == lJobWorkProcessDispatchId).SingleOrDefault();
                List<jobprocessreceipt_itemdetails> receiptChallanItemList = pendingChallanItem.jobprocessreceipt_itemdetails.ToList().Where(x => x.isDeleted == false).ToList();
                pendingChallanItem.jobprocessreceipt_itemdetails = receiptChallanItemList;
                db.Dispose();
                return pendingChallanItem;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<jobprocessreceipt_itemdetails> getJobWorkReceiveChallanItemListForUpdate(long lReceiveVoucherId)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                List<jobprocessreceipt_itemdetails> receiveChallanItemList = db.jobprocessreceipt_itemdetails.Where(x => x.receiveVoucherNo == lReceiveVoucherId).Where(x => x.isDeleted == false).ToList();

                db.Dispose();
                return receiveChallanItemList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public jobprocessreceipt_itemdetails getJobWorkReceiptChallanItemForBill(long lJobWorkReceiptId)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                jobprocessreceipt_itemdetails receiveChallanItem = db.jobprocessreceipt_itemdetails.Where(x => x.jobWorkReceiptId == lJobWorkReceiptId).Where(x => x.isDeleted == false).Single();

                db.Dispose();
                return receiveChallanItem;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<jobprocessreceipt_itemdetails> getJobReceiveChallanDetail(long lJobProcessItemDetailsID)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                List<jobprocessreceipt_itemdetails> receiveItemDataList = db.jobprocessreceipt_itemdetails.Where(x => x.isDeleted == false).Where(x => x.jobProcessItemDetails_id == lJobProcessItemDetailsID).ToList();
                return receiveItemDataList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public jobprocess_details getJobWorkDispatchChallan(string challanNo)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                jobprocess_details pendingChallan = db.jobprocess_details.Where(x => x.challanNo == challanNo).SingleOrDefault();
                List<jobprocess_itemdetails> pendingChallanItemList = pendingChallan.jobprocess_itemdetails.ToList();

                for (int i = 0; i < pendingChallanItemList.Count; i++)
                {
                    List<jobprocessreceipt_itemdetails> receiptChallanItemList = pendingChallanItemList[i].jobprocessreceipt_itemdetails.ToList();
                    pendingChallanItemList[i].jobprocessreceipt_itemdetails = receiptChallanItemList;
                }
                pendingChallan.jobprocess_itemdetails = pendingChallanItemList;
                db.Dispose();
                return pendingChallan;
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public bool AddMillReceiptVoucher(millreceipt_detail millRecpeiptBill)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                db.millreceipt_detail.Add(millRecpeiptBill);
                //db.sales_details.Attach(salesBill);

                //db.Entry(millRecpeiptBill).State = EntityState.Modified;
                foreach (millchallandispatchtaka_detail sd in millRecpeiptBill.millchallandispatchtaka_detail)
                {
                    db.Entry(sd).State = EntityState.Modified;

                }

                db.SaveChanges();
                db.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public bool addJobWorkDispatchChallan(jobprocess_details jobProcessDetails)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                db.jobprocess_details.Add(jobProcessDetails);
                //db.sales_details.Attach(salesBill);

                db.SaveChanges();
                db.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public bool addJobWorkReceiveChallanItem(List<jobprocess_itemdetails> jobProcessItemDetailsList)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                foreach (jobprocess_itemdetails jobProcessItemDetails in jobProcessItemDetailsList)
                {


                    var version = jobProcessItemDetails.version;

                    jobProcessItemDetails.version = null;

                    var existingParent = db.jobprocess_itemdetails
            .Where(p => p.jobProcessItemDetails_id == jobProcessItemDetails.jobProcessItemDetails_id)
            .Include(p => p.jobprocessreceipt_itemdetails)
            .SingleOrDefault();

                    if (existingParent != null)
                    {

                        // Update parent
                        db.Entry(existingParent).CurrentValues.SetValues(jobProcessItemDetails);

                        db.Entry(existingParent).OriginalValues["version"] = version;
                        // Update and Insert children
                        foreach (var childModel in jobProcessItemDetails.jobprocessreceipt_itemdetails)
                        {
                            //childModel.version = null;
                            var existingChild = existingParent.jobprocessreceipt_itemdetails
                             .Where(c => c.jobWorkReceiptId == childModel.jobWorkReceiptId)
                             .SingleOrDefault();

                            if (existingChild != null && childModel.jobWorkReceiptId != 0)
                                // Update child
                                db.Entry(existingChild).CurrentValues.SetValues(childModel);
                            else
                            {

                                existingParent.jobprocessreceipt_itemdetails.Add(childModel);
                            }
                        }


                    }
                }
                db.SaveChanges();
                db.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public bool deleteJobWorkReceiveChallanBill(jobreceive_billdetail jobProcessBillDetail)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);

                var version = jobProcessBillDetail.version;
                jobProcessBillDetail.version = null;
                var existingParent = db.jobreceive_billdetail
        .Where(p => p.jobReceiveBillDetail_id == jobProcessBillDetail.jobReceiveBillDetail_id)
        .Include(p => p.jobprocessreceipt_itemdetails)
        .SingleOrDefault();


                if (existingParent != null)
                {
                    // Update parent
                    db.jobreceive_billdetail.Remove(existingParent);

                    // Update and Insert children
                }
                else
                    return false;
                foreach (var childModel in jobProcessBillDetail.jobprocessreceipt_itemdetails)
                {
                    //childModel.version = null;
                    var existingChild = db.jobprocessreceipt_itemdetails.Where(c => c.jobWorkReceiptId == childModel.jobWorkReceiptId)
                        .SingleOrDefault();

                    if (existingChild != null && childModel.jobWorkReceiptId != 0)
                        // Update child
                        db.Entry(existingChild).CurrentValues.SetValues(childModel);

                }





                //db.jobreceive_billdetail.Add(jobProcessBillDetail);
                //foreach (jobprocessreceipt_itemdetails sd in jobProcessBillDetail.jobprocessreceipt_itemdetails)
                //{
                //    db.Entry(sd).State = EntityState.Modified;

                //}
                db.SaveChanges();
                db.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public bool addCDNoteDetails(creditdebitnote_details cdNoteDetails)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                var version = cdNoteDetails.version;

                var existingParent = db.creditdebitnote_details
                .Where(p => p.creditdebitnote_id == cdNoteDetails.creditdebitnote_id)
                .SingleOrDefault();

                if (existingParent != null)
                {
                    // Update parent
                    db.Entry(existingParent).CurrentValues.SetValues(cdNoteDetails);

                    db.Entry(existingParent).OriginalValues["version"] = version;
                    // Update and Insert children

                }
                else
                    db.creditdebitnote_details.Add(cdNoteDetails);
                db.SaveChanges();
                db.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool addJobWorkReceiveChallanBill(jobreceive_billdetail jobProcessBillDetail)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);

                var version = jobProcessBillDetail.version;
                jobProcessBillDetail.version = null;
                var existingParent = db.jobreceive_billdetail
        .Where(p => p.jobReceiveBillDetail_id == jobProcessBillDetail.jobReceiveBillDetail_id)
        .Include(p => p.jobprocessreceipt_itemdetails)
        .SingleOrDefault();


                if (existingParent != null)
                {
                    // Update parent
                    db.Entry(existingParent).CurrentValues.SetValues(jobProcessBillDetail);

                    db.Entry(existingParent).OriginalValues["version"] = version;
                    // Update and Insert children
                }
                else
                {
                    db.jobreceive_billdetail.Add(jobProcessBillDetail);
                }
                foreach (var childModel in jobProcessBillDetail.jobprocessreceipt_itemdetails)
                {
                    //childModel.version = null;
                    var existingChild = db.jobprocessreceipt_itemdetails.Where(c => c.jobWorkReceiptId == childModel.jobWorkReceiptId)
                        .SingleOrDefault();

                    if (existingChild != null && childModel.jobWorkReceiptId != 0)
                        // Update child
                        db.Entry(existingChild).CurrentValues.SetValues(childModel);

                }





                //db.jobreceive_billdetail.Add(jobProcessBillDetail);
                //foreach (jobprocessreceipt_itemdetails sd in jobProcessBillDetail.jobprocessreceipt_itemdetails)
                //{
                //    db.Entry(sd).State = EntityState.Modified;

                //}
                db.SaveChanges();
                db.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public bool updateJobWorkReceiveChallanItem(List<jobprocessreceipt_itemdetails> jobProcessReceiveItemDetailsList)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                foreach (jobprocessreceipt_itemdetails jobProcessReceiveItem in jobProcessReceiveItemDetailsList)
                {


                    var version = jobProcessReceiveItem.version;

                    //jobProcessReceiveItem.version = null;

                    var existingParent = db.jobprocessreceipt_itemdetails
                                         .Where(p => p.jobWorkReceiptId == jobProcessReceiveItem.jobWorkReceiptId)
                                         .SingleOrDefault();

                    if (existingParent != null)
                    {

                        // Update parent
                        db.Entry(existingParent).CurrentValues.SetValues(jobProcessReceiveItem);

                        db.Entry(existingParent).OriginalValues["version"] = version;
                        // Update and Insert children
                    }
                    else
                    {

                        db.jobprocessreceipt_itemdetails.Add(jobProcessReceiveItem);
                    }
                }
                db.SaveChanges();
                db.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public bool UpdateMillReceiptVoucher(millreceipt_detail millRecpeiptBill)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                var version = millRecpeiptBill.version;
                millRecpeiptBill.version = null;
                var existingParent = db.millreceipt_detail
        .Where(p => p.millvoucher_id == millRecpeiptBill.millvoucher_id)
        .Include(p => p.millchallandispatchtaka_detail)
        .SingleOrDefault();

                if (existingParent != null)
                {
                    // Update parent
                    db.Entry(existingParent).CurrentValues.SetValues(millRecpeiptBill);

                    db.Entry(existingParent).OriginalValues["version"] = version;
                    // Update and Insert children
                    foreach (var childModel in millRecpeiptBill.millchallandispatchtaka_detail)
                    {
                        var childVersion = childModel.version;
                        //childModel.version = null;
                        var existingChild = existingParent.millchallandispatchtaka_detail.Where(p => p.millTaka_id == childModel.millTaka_id).SingleOrDefault();
                        db.Entry(existingChild).OriginalValues["version"] = childVersion;


                        if (existingChild != null && childModel.millTaka_id != 0)
                            // Update child
                            db.Entry(existingChild).CurrentValues.SetValues(childModel);
                    }

                    db.SaveChanges();
                }

                db.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public bool deleteMillRecietVoucher(long lVoucherNo)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                millreceipt_detail deleteBillDetail = db.millreceipt_detail.Where(x => x.receiptVoucherNo == lVoucherNo).SingleOrDefault();
                db.millreceipt_detail.Remove(deleteBillDetail);
                db.SaveChanges();
                db.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }



        public List<printersettings_details> getPrinterSettings()
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                List<printersettings_details> printerSettings = db.printersettings_details.Where(p => p.printerSettingsDetails_id != 0).ToList();
                db.Dispose();
                return printerSettings;
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public bool UpdatePrinterDetails(List<printersettings_details> printersettingsdetails)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);

                // Update and Insert children
                foreach (var printersettings_detail in printersettingsdetails)
                {
                    var childVersion = printersettings_detail.version;
                    printersettings_detail.version = null;
                    var existingChild = db.printersettings_details.Where(p => p.type == printersettings_detail.type).SingleOrDefault();
                    db.Entry(existingChild).OriginalValues["version"] = childVersion;


                    if (existingChild != null)
                        // Update child
                        db.Entry(existingChild).CurrentValues.SetValues(printersettings_detail);
                }

                db.SaveChanges();


                db.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public List<sales_details> getMonthlySalesReport()
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                //var salesReport= db.Database.SqlQuery<SalesReport>("SELECT MONTHNAME(sd.date) AS MONTH,SUM(billamount) AS billamount,SUM(cgst) AS cgst,SUM(igst) AS igst,SUM(cgst) AS cgst,SUM(taxablevalue) AS taxablevalue,SUM(grossamount) AS grossamount,SUM(pcs) AS pcs,SUM(quantity) AS quanity FROM sales_details sd JOIN salesitem_details st ON sd.salesbillid=st.salesbillid GROUP BY YEAR (sd.date),MONTH(sd.date)").ToList();


                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
