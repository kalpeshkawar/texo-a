﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Texo_Advance
{
    class DBHelper2
    {
        public static Entities GetDbContext(string tenantName)
        {
            var connectionStringTemplate =
            @"metadata=res://*/Model1.csdl|res://*/Model1.ssdl|res://*/Model1.msl;" +
            @"provider=MySql.Data.MySqlClient;" +
            @"provider connection string=""server=localhost;" +
            @"initial catalog={0};" +
             @"user id={1};password={2};" +
            @"integrated security=True;"";"; /*+
            @"App=EntityFramework"";";*/

            var TenantUserName = Properties.Settings.Default.DBUser;
            var TenantPassword = Properties.Settings.Default.DBPassword;
            var connectionString = string.Format(connectionStringTemplate, tenantName, TenantUserName, TenantPassword);
            var db = new Entities(connectionString);
            db.Database.CreateIfNotExists();
            return db;
        }

        public static Boolean createDb(string tenantName)
        {
            var connectionStringTemplate =
            @"metadata=res://*/Model1.csdl|res://*/Model1.ssdl|res://*/Model1.msl;" +
            @"provider=MySql.Data.MySqlClient;" +
            @"provider connection string=""server=localhost;" +
            @"initial catalog={0};" +
             @"user id={1};password={2};" +
            @"integrated security=True;"";"; /*+
            @"App=EntityFramework"";";*/

            var TenantUserName = Properties.Settings.Default.DBUser;
            var TenantPassword = Properties.Settings.Default.DBPassword;
            var connectionString = string.Format(connectionStringTemplate, tenantName, TenantUserName, TenantPassword);
            var db = new Entities(connectionString);
            Boolean b= db.Database.CreateIfNotExists();
            db.Dispose();
            return b;
        }
    }
}
