﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Texo_Advance.Properties;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace Texo_Advance.DBItems
{
    class MasterDBData
    {

        String dbName = Properties.Settings.Default.MasterDatabse;
        long lCompanyID = Properties.Settings.Default.CompanyId;
        long lFinancialYearID = Properties.Settings.Default.FinancialYearId;
        public List<company_details> getCompanies()
        {
            var db=DBHelper2.GetDbContext(dbName);
            List<company_details> companies= db.company_details.ToList();
            db.Dispose();
            return companies;

        }

        public Boolean setCompany(company_details company)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                db.company_details.Add(company);
                int i=db.SaveChanges();
                db.Dispose();
                if (i != 0)
                {
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                return false;
            }

        }


        public List<ledger_type> getLegderType()
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                List<ledger_type> ledgers= db.ledger_type.ToList();              
                db.Dispose();
                return ledgers;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public Boolean setFinancialYear(financial_year year)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                db.financial_year.Add(year);
                int i = db.SaveChanges();
                db.Dispose();
                if (i != 0)
                {
                    if (DBHelper2.createDb(year.db_name))
                        return true;
                    else
                        return false;

                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public List<financial_year> getFinancialYearByCompany(long companyId)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                List<financial_year> years = db.financial_year.Where(x => x.company_id == companyId).ToList();
                db.Dispose();
                return years;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public long getFinancialYearID(long lCompanyId,string financialYear)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                long lFinancialYearID = db.financial_year.Where(x => x.company_id == lCompanyId).Where(x=>x.financial_year1 == financialYear).Select(x=>x.financialyear_id).SingleOrDefault();
                db.Dispose();
                return lFinancialYearID;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }

        public userdetail getUserDetailByUserName(string userName)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                userdetail userDetail = db.userdetails.Where(x => x.username == userName).FirstOrDefault();
                db.Dispose();
                return userDetail;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<party_details> getPartyDetails(long lAccountTypeId)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                List<party_details> partyDetail = db.party_details.Where(x => x.account_typeid == lAccountTypeId).ToList();
                if(partyDetail != null)
                {
                    for(int i = 0;i < partyDetail.Count;i++)
                    {
                        transport_details tempTrensportDetails = partyDetail[i].transport_details;
                        city_list tempCityDetails = partyDetail[i].city_list;
                        broker_details tempBrokerDetail = partyDetail[i].broker_details;
                        partyDetail[i].transport_details = tempTrensportDetails;
                        partyDetail[i].city_list = tempCityDetails;
                        partyDetail[i].broker_details = tempBrokerDetail;
                    }
                    
                }
                db.Dispose();
                return partyDetail;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<party_details> getPartyDetails()
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                List<party_details> partyDetail = db.party_details.Where(x => x.partyid != null).ToList();
                if (partyDetail != null)
                {
                    for (int i = 0; i < partyDetail.Count; i++)
                    {
                        transport_details tempTrensportDetails = partyDetail[i].transport_details;
                        city_list tempCityDetails = partyDetail[i].city_list;
                        broker_details tempBrokerDetail = partyDetail[i].broker_details;
                        partyDetail[i].transport_details = tempTrensportDetails;
                        partyDetail[i].city_list = tempCityDetails;
                        partyDetail[i].broker_details = tempBrokerDetail;
                    }

                }
                db.Dispose();
                return partyDetail;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public string getPartyNameFromId(long lPartyId)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                string partyName = db.party_details.Where(x => x.partyid == lPartyId).Select(x=>x.party_name).SingleOrDefault();
                db.Dispose();
                return partyName;
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public List<party_details> getPartyAccountListForPassBook()
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                List<party_details> partyDetail = db.party_details.Where(x => x.partyid != null).ToList();
                db.Dispose();
                return partyDetail;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<bank_details> getBankList()
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                List<bank_details> bankList = db.bank_details.Where(p => p.bankDetails_id != null).ToList();
                db.Dispose();
                return bankList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public string getItemNameFromId(long lItemId)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                string partyName = db.item_details.Where(x => x.itemid == lItemId).Select(x => x.itemname).SingleOrDefault();
                db.Dispose();
                return partyName;
            }
            catch(Exception ex)
            {
                return "";
            }
        }

        public List<passbooktype_details> getPassBookTypeDetails()
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                List<passbooktype_details> passBookDetails = db.passbooktype_details.Where(x => x.passbooktype_id != null).ToList();
                return passBookDetails;
            }
            catch(Exception ex)
            {
                return null;
            }
        }

        public item_details getItemDetails(string itemName)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                item_details itemDetail = db.item_details.Where(x => x.itemname == itemName).SingleOrDefault();
                db.Dispose();
                return itemDetail;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<worktypegst_details> getWorkTypeGstDetails()
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                List<worktypegst_details> gstWorkTypeDetail = db.worktypegst_details.Where(x => x.worktypegst_id != null).ToList();
                db.Dispose();
                return gstWorkTypeDetail;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<worktype_details> getWorkTypeDetails()
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                List<worktype_details> wokTypeDetail = db.worktype_details.Where(x => x.worktype_id != null).ToList();
                db.Dispose();
                return wokTypeDetail;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public party_details getMillDetails(long lMillId)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                
                party_details partyDetail = db.party_details.Where(x => x.partyid == lMillId).Single();
                db.Dispose();
                return partyDetail;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<broker_details> getBrokerDetails()
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                List<broker_details> brokerDetail = db.broker_details.Where(x => x.brokerid != null).ToList();
                db.Dispose();
                return brokerDetail;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public string getBrokerNameFromID(long lBrokerID)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                string brokerName = db.broker_details.Where(x => x.brokerid == lBrokerID).Select(x=>x.party_name).SingleOrDefault();
                db.Dispose();
                return brokerName;
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        public List<transport_details> getTransportDetails()
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                List<transport_details> transportDetail = db.transport_details.Where(x => x.transportid != null).ToList();
                db.Dispose();
                return transportDetail;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<statecode_details> getStateCodeList()
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                List<statecode_details> stateCodeList = db.statecode_details.Where(x => x.statecode_id != null).ToList();
                db.Dispose();
                return stateCodeList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<gsttype_details> getGstTypeList()
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                List<gsttype_details> gstTypeList = db.gsttype_details.Where(x => x.gsttype_id != null).ToList();
                db.Dispose();
                return gstTypeList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<city_list> getCityList()
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                List<city_list> cityList = db.city_list.Where(x => x.city_id != null).ToList();
                db.Dispose();
                return cityList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public string getCityName(long lCity_id)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                string cityName = db.city_list.Where(x => x.city_id == lCity_id).Select(x=>x.name).SingleOrDefault();
                db.Dispose();
                return cityName;
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public List<item_details> getItemList()
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                List<item_details> itemList = db.item_details.Where(x => x.itemid != null).ToList();
                db.Dispose();
                return itemList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<sales_type> getSalesTypeList()
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                List<sales_type> salesTypeList = db.sales_type.Where(x => x.type != null).ToList();
                db.Dispose();
                return salesTypeList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public string getSalesTypeName(long lSalesType)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                string salesTypeName = db.sales_type.Where(x => x.salesTypeid == lSalesType).Select(x=>x.type).SingleOrDefault();
                db.Dispose();
                return salesTypeName;
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public string getWorkTypeName(long lWorkTypeID)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                string workTypeName = db.worktype_details.Where(x => x.worktype_id == lWorkTypeID).Select(x => x.worktype).SingleOrDefault();
                db.Dispose();
                return workTypeName;
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public string getPurchaseTypeName(long lPurchaseType)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                string purchaseTypeName = db.purchasetype_details.Where(x => x.purchasetype_id == lPurchaseType).Select(x => x.type).SingleOrDefault();
                db.Dispose();
                return purchaseTypeName;
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        public double getOutstandingOpeningBalance(long lPartyId)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                double outstandingAmount = (double)db.openingbalance_details.Where(x => x.party_id == lPartyId).Where(x => x.companyId == lCompanyID).Where(x=>x.financialyear_id == lFinancialYearID).Select(x => x.openingBalance).DefaultIfEmpty(0).SingleOrDefault();
                db.Dispose();
                return outstandingAmount;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }


        public List<itemtype_details> getItemTypeList()
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                List<itemtype_details> itemTypeList = db.itemtype_details.Where(x => x.itemTypeId != null).ToList();
                db.Dispose();
                return itemTypeList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<creditdebitnote_type> getCreditDebitNoteTypeList()
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                List<creditdebitnote_type> creditDebitNoteTypeList = db.creditdebitnote_type.Where(x => x.creditDebitNoteType_id != null).ToList();
                db.Dispose();
                return creditDebitNoteTypeList;
            }
            catch(Exception ex)
            {
                return null;
            }
        }
        public List<catogery_details> getItemCatogeryList()
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                List<catogery_details> itemCatogeryList = db.catogery_details.Where(x => x.companyId == lCompanyID).Where(x => x.financialYear_Id == lFinancialYearID).ToList();
                db.Dispose();
                return itemCatogeryList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<account_type> getAccountTypeList()
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                List<account_type> accountTypeList = db.account_type.Where(x => x.account_typeid != null).ToList();
                db.Dispose();
                return accountTypeList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public string getAccountTypeName(long lAccountTypeId)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                string accountTypeName = db.account_type.Where(x => x.account_typeid == lAccountTypeId).Select(x=>x.type).SingleOrDefault();
                db.Dispose();
                return accountTypeName;
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public long getAccountTypeID(string strAccountType)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                long accountTypeID = db.account_type.Where(x => x.type == strAccountType).Select(x => x.account_typeid).SingleOrDefault();
                db.Dispose();
                return accountTypeID;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }


        public List<ledger_type> getLedgerTypeList()
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                List<ledger_type> ledgerTypeList = db.ledger_type.Where(x => x.ledger_typeid != null).ToList();
                db.Dispose();
                return ledgerTypeList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool addCity(string cityName)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                var city_new = new city_list();
                city_new.name = cityName;
                db.city_list.Add(city_new);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool addOpeningStock(openingstock_details openingStockDetail)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                var version = openingStockDetail.version;
                //passBookData.version = null;
                var existingParent = db.openingstock_details
                .Where(p => p.openingStock_id == openingStockDetail.openingStock_id)
                .SingleOrDefault();

                if (existingParent != null)
                {
                    // Update parent
                    db.Entry(existingParent).CurrentValues.SetValues(openingStockDetail);

                    db.Entry(existingParent).OriginalValues["version"] = version;
                    
                }
                else
                {
                    openingStockDetail.companyId = lCompanyID;
                    openingStockDetail.financialyear_id = lFinancialYearID;
                    db.openingstock_details.Add(openingStockDetail);
                }
                    
                db.SaveChanges();
                db.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public openingstock_details getOpeningStockDetails()
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                openingstock_details openingStockDetail = db.openingstock_details.Where(x => x.companyId == lCompanyID).Where(x => x.financialyear_id == lFinancialYearID).SingleOrDefault();
                db.Dispose();
                return openingStockDetail;
            }
            catch(Exception ex)
            {
                return null;
            }
        }
        public bool addBank(string bankName)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                var new_bank = new bank_details();
                new_bank.bankName = bankName;
                db.bank_details.Add(new_bank);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool addTransport(string transportName)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                var transport_new = new transport_details();
                transport_new.name = transportName;
                db.transport_details.Add(transport_new);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public List<purchasetype_details> getPurchaseTypeList()
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                List<purchasetype_details> purchaseTypeList = db.purchasetype_details.Where(x => x.purchasetype_id != null).ToList();
                db.Dispose();
                return purchaseTypeList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public long getLastPurchaseBillNo()
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                long lLastBillNo = db.purchase_details.Select(p => p.purchasebillid).DefaultIfEmpty(0).Max();
                db.Dispose();
                return lLastBillNo;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }

        public party_details getPartyDetailFromID(long lAccountypeId, long lPartyId)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                party_details partyDetail = db.party_details.Where(x => x.account_typeid == lAccountypeId).Where(x => x.partyid == lPartyId).SingleOrDefault();
                db.Dispose();
                return partyDetail;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<greyquality_details> getGreyQualityList()
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                List<greyquality_details> greyQualityList = db.greyquality_details.Where(x => x.greyquality_id != null).ToList();
                db.Dispose();
                return greyQualityList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<checker_details> getCheckerList()
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                List<checker_details> checkerList = db.checker_details.Where(x => x.checkertype_id != null).ToList();
                db.Dispose();
                return checkerList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool addGreyQuality(string greyQualityName)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                var quality_list = new greyquality_details();
                long lCatogeryID = -1;
                catogery_details catogertDetails = db.catogery_details.Where(x => x.catogeryname == greyQualityName).SingleOrDefault();
                if(catogertDetails == null)
                {
                    var catogeryDetails = new catogery_details();
                    catogeryDetails.catogeryname = greyQualityName;
                    catogeryDetails.companyId = lCompanyID;
                    catogeryDetails.financialYear_Id = lFinancialYearID;
                    db.catogery_details.Add(catogeryDetails);
                    db.SaveChanges();
                    
                }
                
                lCatogeryID = db.catogery_details.Where(x => x.catogeryname == greyQualityName).Select(x => x.catogeryid).DefaultIfEmpty(0).SingleOrDefault();
                quality_list.quality_name = greyQualityName;
                quality_list.catogeryid = lCatogeryID;
                db.greyquality_details.Add(quality_list);
                db.SaveChanges();
                db.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool addMainScreenCatogery(string mainScreenName)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                var catogeryDetails = new catogery_details();
                catogeryDetails.catogeryname = mainScreenName;
                catogeryDetails.companyId = lCompanyID;
                catogeryDetails.financialYear_Id = lFinancialYearID;
                db.catogery_details.Add(catogeryDetails);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool addChecker(string checkerName)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                var checker_new = new checker_details();
                checker_new.checker_name = checkerName;
                db.checker_details.Add(checker_new);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool addBroker(broker_details newBroker)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                db.broker_details.Add(newBroker);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public company_details getCompanyGstn(long lCompanyId)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                company_details companyName = db.company_details.Where(x => x.companyId == lCompanyId).SingleOrDefault();
                db.Dispose();
                return companyName;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public long getGreyPartyIDFromRefBillNo(string strBillNo)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                long lGreyPartyID = (long)db.greypurchase_details.Where(p => p.billNo == strBillNo).Select(p => p.partyid).SingleOrDefault();
                
                db.Dispose();
                return lGreyPartyID;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }
        public string getGreyPartyNameInMillDispatch(long lPartyID)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                string strGreyPartyName = db.party_details.Where(p => p.partyid == lPartyID).Select(p => p.party_name).SingleOrDefault();
                db.Dispose();
                return strGreyPartyName;
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public List<catogery_details> getMainScreenItemList()
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                List <catogery_details> catogeryDetailsList = db.catogery_details.Where(p => p.catogeryid != 0).ToList();
                db.Dispose();
                return catogeryDetailsList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public string getGreyQualityNameInMillDispatch(long lQualityID)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                string strGreyQualityName = db.greyquality_details.Where(p => p.greyquality_id == lQualityID).Select(p => p.quality_name).SingleOrDefault();
                db.Dispose();
                return strGreyQualityName;
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public List<millprocess_type> getMillProcessType()
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                List<millprocess_type> processList = db.millprocess_type.ToList();
                db.Dispose();
                return processList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public greyquality_details GetGreyquality(long lGreyId)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                greyquality_details greyQuality = db.greyquality_details.Where(x => x.greyquality_id == lGreyId).SingleOrDefault();
                db.Dispose();
                return greyQuality;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public string getPartyGstn(long lPartyId)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                string gstNumber = db.party_details.Where(x => x.partyid == lPartyId).Select(x => x.gst).SingleOrDefault();
                db.Dispose();
                return gstNumber;
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public bool addOpeningBalance(openingbalance_details openingBalanceData)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                var version = openingBalanceData.version;
                
                var existingParent = db.openingbalance_details
                .Where(p => p.openingBalance_id == openingBalanceData.openingBalance_id)
                .SingleOrDefault();

                if (existingParent != null)
                {
                    // Update parent
                    db.Entry(existingParent).CurrentValues.SetValues(openingBalanceData);

                    db.Entry(existingParent).OriginalValues["version"] = version;
                    // Update and Insert children
                    
                }
                else
                    db.openingbalance_details.Add(openingBalanceData);
                db.SaveChanges();
                db.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public openingbalance_details getOpeningBalance(long lPartyId)
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                openingbalance_details openingBalanceData = db.openingbalance_details.Where(x=>x.party_id == lPartyId).SingleOrDefault();
                db.Dispose();
                return openingBalanceData;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<greypurchase_type> getGreyPurchaseTypeList()
        {
            try
            {
                var db = DBHelper2.GetDbContext(dbName);
                List<greypurchase_type> greyPurchaseTypeList = db.greypurchase_type.Where(x => x.greyPurchaseType_id != null).ToList();
                db.Dispose();
                return greyPurchaseTypeList;
            }
            catch(Exception ex)
            {
                return null;
            }
        }

    }
}
