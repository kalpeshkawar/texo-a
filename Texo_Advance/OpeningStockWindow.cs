﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Texo_Advance.DBItems;

namespace Texo_Advance
{
    public partial class OpeningStockWindow : UserControl
    {
        MasterDBData DB = new MasterDBData();
        string strCompanyName = "";
        openingstock_details openingStockDetail;
        public OpeningStockWindow(string companyName)
        {
            InitializeComponent();
            strCompanyName = companyName;
            companyNameTextBox.Text = strCompanyName;
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            if(openingStockDetail != null)
            {
                openingStockDetail.totalPcs = Convert.ToDouble(pcsTextBox.Text);
                openingStockDetail.totalMtrs = Convert.ToDouble(mtrsTextBox.Text);
                openingStockDetail.totalValue = Convert.ToDouble(valueTextBox.Text);
                bool bFlag = DB.addOpeningStock(openingStockDetail);
                if(bFlag)
                {
                    MessageBox.Show("Opening Stock Details Updated", "Opening Stock", MessageBoxButtons.OK);
                }
                else
                    MessageBox.Show("Opening Stock Details Not Updated", "ERROR", MessageBoxButtons.OK);
            }
            else
            {
                openingStockDetail = new openingstock_details();
                openingStockDetail.totalPcs = Convert.ToDouble(pcsTextBox.Text);
                openingStockDetail.totalMtrs = Convert.ToDouble(mtrsTextBox.Text);
                openingStockDetail.totalValue = Convert.ToDouble(valueTextBox.Text);
                bool bFlag = DB.addOpeningStock(openingStockDetail);
                if (bFlag)
                {
                    MessageBox.Show("Opening Stock Details Added", "Opening Stock", MessageBoxButtons.OK);
                }
                else
                    MessageBox.Show("Opening Stock Details Not Added", "ERROR", MessageBoxButtons.OK);
            }
            
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            this.Parent.Controls.Remove(this);
        }

        private void resetButton_Click(object sender, EventArgs e)
        {
            resetAllData();
        }

        private void resetAllData()
        {
            pcsTextBox.Text = "0";
            mtrsTextBox.Text = "0.0";
            valueTextBox.Text = "0.0";
        }

        private void OpeningStockWindow_Load(object sender, EventArgs e)
        {
            openingStockDetail = DB.getOpeningStockDetails();
            if(openingStockDetail != null)
            {
                pcsTextBox.Text = Convert.ToString(openingStockDetail.totalPcs);
                mtrsTextBox.Text = Convert.ToString(openingStockDetail.totalMtrs);
                valueTextBox.Text = Convert.ToString(openingStockDetail.totalValue);
            }
        }

    }
}
