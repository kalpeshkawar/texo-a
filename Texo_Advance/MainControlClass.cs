﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Texo_Advance
{
    class MainControlClass
    {
       
        public static void ShowControls(System.Windows.Forms.Control control, System.Windows.Forms.Control homePanel)
        {
            homePanel.Controls.Clear();
            control.Dock = DockStyle.Fill;
            control.BringToFront();
            control.Focus();

            homePanel.Controls.Add(control);
        }

        public static void clearControls(System.Windows.Forms.Control homePanel)
        {
            homePanel.Controls.Clear();
        }

    }
}
