﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Texo_Advance.DBItems;

namespace Texo_Advance
{
    public partial class OpeningBalancesWindow : UserControl
    {
        string strCompanyName = "";
        MasterDBData DB = new MasterDBData();
        openingbalance_details openingBalanceDetail = new openingbalance_details();
        public OpeningBalancesWindow(string companyName)
        {
            InitializeComponent();
            strCompanyName = companyName;
        }

        private void OpeningBalancesWindow_Load(object sender, EventArgs e)
        {
            companyNameTextBox.Text = strCompanyName;
            fillAccountTypeList();
            fillPartyList();
        }

        private void fillAccountTypeList()
        {
            List<account_type> accTypeList = DB.getAccountTypeList();
            if(accTypeList != null)
            {
                accountTypeComboBox.DataSource = accTypeList;
                accountTypeComboBox.DisplayMember = "type";
                accountTypeComboBox.ValueMember = "account_typeid";
            }
        }

        private void fillPartyList()
        {
            int iColumnCount = 5;
            var partyAccountList = DB.getPartyAccountListForPassBook();
            
            DataTable myDataTable = new DataTable("ParentTable");
            // Declare variables for DataColumn and DataRow objects.
            DataColumn myDataColumn;
            DataRow myDataRow;

            //Add some coulumns
            for (int index = 0; index < iColumnCount; index++)
            {
                myDataColumn = new DataColumn();
                myDataColumn.DataType = typeof(string);
                if (index == 0)
                    myDataColumn.ColumnName = "NAME";
                else if (index == 1)
                    myDataColumn.ColumnName = "ACCOUNT_TYPE";
                else if (index == 2)
                    myDataColumn.ColumnName = "ADDRESS";
                else if (index == 3)
                    myDataColumn.ColumnName = "CITY";
                else if (index == 4)
                    myDataColumn.ColumnName = "PARTY_ID";
                //myDataColumn.Caption = "ParentItem";
                myDataColumn.AutoIncrement = false;
                myDataColumn.ReadOnly = false;
                myDataColumn.Unique = false;
                // Add the Column to the DataColumnCollection.
                myDataTable.Columns.Add(myDataColumn);
            }

            
            //Add some more rows
            for (int index = 0; index < partyAccountList.Count; index++)
            {
                myDataRow = myDataTable.NewRow();
                myDataRow[0] = partyAccountList[index].party_name;
                myDataRow[1] = DB.getAccountTypeName(partyAccountList[index].account_typeid);
                myDataRow[2] = partyAccountList[index].address + partyAccountList[index].address2;
                myDataRow[3] = DB.getCityName((long)partyAccountList[index].city_id);
                myDataRow[4] = partyAccountList[index].partyid;
                myDataTable.Rows.Add(myDataRow);
            }


            //Now set the Data of the ColumnComboBox
            accountNameComboBox.ValueMember = "PARTY_ID";
            accountNameComboBox.Data = myDataTable;
            //Set which row will be displayed in the text box
            //If you set this to a column that isn't displayed then the suggesting functionality won't work.
            accountNameComboBox.ColumnSpacing = 50;
            accountNameComboBox.ViewColumn = 0;

            accountNameComboBox.Columns[0].Width = 150;
            accountNameComboBox.Columns[1].Width = 150;
            accountNameComboBox.Columns[2].Width = 200;
            accountNameComboBox.Columns[3].Width = 50;
            //Set a few columns to not be shown
            accountNameComboBox.Columns[4].Display = false;

        }

        private void accountNameComboBox_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                string accountNameText = accountNameComboBox.Text;
                
                int iIndex = accountNameComboBox.FindString(accountNameText);
                if (iIndex<0)
                {
                    AccountManagerForm acc = new AccountManagerForm();
                    //acc.setDefaultAccountType(2);
                    acc.setDefaultName(accountNameText);
                    acc.ShowDialog();
                    fillPartyList();
                    accountNameComboBox.SelectedIndex = -1;
                    accountNameComboBox.Text = accountNameText;
                }

                
            }
        }

        private void addNewButton_Click(object sender, EventArgs e)
        {
            if(accountNameComboBox.SelectedIndex == -1)
            {
                MessageBox.Show("Please enter a Account Name","ERROR",MessageBoxButtons.OK);
                return;
                
            }
            double dDebitOpeningBalance = 0;
            double dCreditOpeningBalance = 0;
            double.TryParse(debitOpeningBalanceTextBox.Text, out dDebitOpeningBalance);
            double.TryParse(creditOpeningBalanceTextBox.Text, out dCreditOpeningBalance);
            if(dCreditOpeningBalance > 0 && dDebitOpeningBalance > 0)
            {
                MessageBox.Show("Account cannot have both credit and debit opening balance", "ERROR", MessageBoxButtons.OK);
                return;
            }
            openingbalance_details newOpeningData = new openingbalance_details();
            if (dDebitOpeningBalance > 0)
            {
                
                newOpeningData.companyId = Properties.Settings.Default.CompanyId;
                string partyID = accountNameComboBox["PARTY_ID"].ToString();
                newOpeningData.party_id = Convert.ToInt64(partyID);
                newOpeningData.remark = remarkTextBox.Text;
                newOpeningData.openingBalance = -dDebitOpeningBalance;
                newOpeningData.financialyear_id = Properties.Settings.Default.FinancialYearId;
            }
            else if(dCreditOpeningBalance > 0)
            {
                
                newOpeningData.companyId = Properties.Settings.Default.CompanyId;
                string partyID = accountNameComboBox["PARTY_ID"].ToString();
                newOpeningData.party_id = Convert.ToInt64(partyID);
                newOpeningData.remark = remarkTextBox.Text;
                newOpeningData.openingBalance = dCreditOpeningBalance;
                newOpeningData.financialyear_id = Properties.Settings.Default.FinancialYearId;
            }

            bool bOpeningBalnaceAdded = DB.addOpeningBalance(newOpeningData);
            if(bOpeningBalnaceAdded)
            {
                MessageBox.Show("Opening Balance Saved", "SUCCESS", MessageBoxButtons.OK);
                resetAllData();
            }
            else
            {
                MessageBox.Show("Opening Balance not Saved", "ERROR", MessageBoxButtons.OK);
            }
        }

        private void resetAllData()
        {
            //accountNameComboBox.SelectedIndex = -1;
            debitOpeningBalanceTextBox.Text = "0";
            creditOpeningBalanceTextBox.Text = "0";
            remarkTextBox.Text = "";
            updateButton.Enabled = false;
            
            addNewButton.Enabled = true;
        }

        private void viewButton_Click(object sender, EventArgs e)
        {
            if (accountNameComboBox.SelectedIndex == -1)
            {
                MessageBox.Show("Please enter a Account Name", "ERROR", MessageBoxButtons.OK);
                return;

            }
            string partyID = accountNameComboBox["PARTY_ID"].ToString();
            
            openingBalanceDetail = DB.getOpeningBalance(Convert.ToInt64(partyID));
            if(openingBalanceDetail == null)
            {
                MessageBox.Show("Opening Balance for the Account does no exist", "ERROR", MessageBoxButtons.OK);
                return;
            }
            if (openingBalanceDetail.openingBalance > 0)
                creditOpeningBalanceTextBox.Text = Convert.ToString(openingBalanceDetail.openingBalance);
            else
                debitOpeningBalanceTextBox.Text = Convert.ToString(-openingBalanceDetail.openingBalance);
            remarkTextBox.Text = openingBalanceDetail.remark;
        }

        private void accountNameComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            if(accountNameComboBox.SelectedIndex != -1)
            {
                string accountTypeId = accountNameComboBox["ACCOUNT_TYPE"].ToString();
                accountTypeComboBox.Text = accountTypeId;
                string partyID = accountNameComboBox["PARTY_ID"].ToString();

                openingBalanceDetail = DB.getOpeningBalance(Convert.ToInt64(partyID));
                if (openingBalanceDetail == null)
                {
                    //MessageBox.Show("Opening Balance for the Account does no exist", "ERROR", MessageBoxButtons.OK);
                    //resetAllData();
                    //accountNameComboBox.SelectedValue = Convert.ToInt64(partyID);
                    creditOpeningBalanceTextBox.Text = "0";
                    debitOpeningBalanceTextBox.Text = "0";
                    return;
                }
                if (openingBalanceDetail.openingBalance > 0)
                {
                    creditOpeningBalanceTextBox.Text = Convert.ToString(openingBalanceDetail.openingBalance);
                    debitOpeningBalanceTextBox.Text = "0";
                }
                else
                {
                    debitOpeningBalanceTextBox.Text = Convert.ToString(-openingBalanceDetail.openingBalance);
                    creditOpeningBalanceTextBox.Text = "0";
                }
                    
                remarkTextBox.Text = openingBalanceDetail.remark;
                updateButton.Enabled = true;
                
                addNewButton.Enabled = false;
            }
            
        }

        private void updateButton_Click(object sender, EventArgs e)
        {
            if (accountNameComboBox.SelectedIndex == -1)
            {
                MessageBox.Show("Please enter a Account Name", "ERROR", MessageBoxButtons.OK);
                return;

            }
            double dDebitOpeningBalance = 0;
            double dCreditOpeningBalance = 0;
            double.TryParse(debitOpeningBalanceTextBox.Text, out dDebitOpeningBalance);
            double.TryParse(creditOpeningBalanceTextBox.Text, out dCreditOpeningBalance);
            if (dCreditOpeningBalance > 0 && dDebitOpeningBalance > 0)
            {
                MessageBox.Show("Account cannot have both credit and debit opening balance", "ERROR", MessageBoxButtons.OK);
                return;
            }
            
            if (dDebitOpeningBalance > 0)
            {

                openingBalanceDetail.companyId = Properties.Settings.Default.CompanyId;
                string partyID = accountNameComboBox["PARTY_ID"].ToString();
                openingBalanceDetail.party_id = Convert.ToInt64(partyID);
                openingBalanceDetail.remark = remarkTextBox.Text;
                openingBalanceDetail.openingBalance = -dDebitOpeningBalance;
                openingBalanceDetail.financialyear_id = Properties.Settings.Default.FinancialYearId;
            }
            else if (dCreditOpeningBalance > 0)
            {

                openingBalanceDetail.companyId = Properties.Settings.Default.CompanyId;
                string partyID = accountNameComboBox["PARTY_ID"].ToString();
                openingBalanceDetail.party_id = Convert.ToInt64(partyID);
                openingBalanceDetail.remark = remarkTextBox.Text;
                openingBalanceDetail.openingBalance = dCreditOpeningBalance;
                openingBalanceDetail.financialyear_id = Properties.Settings.Default.FinancialYearId;
            }

            bool bOpeningBalnaceUpdated = DB.addOpeningBalance(openingBalanceDetail);
            if (bOpeningBalnaceUpdated)
            {
                MessageBox.Show("Opening Balance Updated", "SUCCESS", MessageBoxButtons.OK);
                resetAllData();
            }
            else
            {
                MessageBox.Show("Opening Balance not updated", "ERROR", MessageBoxButtons.OK);
            }
        }

        private void accountNameComboBox_SelectedValueChanged(object sender, EventArgs e)
        {

        }
    }
}
