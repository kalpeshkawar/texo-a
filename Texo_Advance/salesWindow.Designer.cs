﻿namespace Texo_Advance
{
    partial class salesWindow
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(salesWindow));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label2 = new System.Windows.Forms.Label();
            this.comNameTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.salesTypeComboBox = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.billNoTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.orderNoTextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.dateTextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.stateComboBox = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.gstTypeComboBox = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.stationComboBox = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.brokerComboBox = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.gstnTextBox = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.transportComboBox = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.lrnoTextBox = new System.Windows.Forms.TextBox();
            this.saleDataGridView = new System.Windows.Forms.DataGridView();
            this.bundlesEditBoxGrid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hsncEditBoxGrid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pcsTextBoxGrid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cutEditBoxGrid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.qtyEditBoxGid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rateTextBoxGrid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.amountTextBoxGrid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label15 = new System.Windows.Forms.Label();
            this.totalPcsTextBox = new System.Windows.Forms.TextBox();
            this.totalQtyTextBox = new System.Windows.Forms.TextBox();
            this.totalAmountTextBox = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.addNewButton = new System.Windows.Forms.Button();
            this.printButton = new System.Windows.Forms.Button();
            this.deleteButton = new System.Windows.Forms.Button();
            this.openBillButton = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.remarkTextBox = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.igstAmtTextBox = new System.Windows.Forms.TextBox();
            this.sgstAmtTextBox = new System.Windows.Forms.TextBox();
            this.cgstAmtTextBox = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.igstRateTextBox = new System.Windows.Forms.TextBox();
            this.sgstRateTextBox = new System.Windows.Forms.TextBox();
            this.cgstRateTextBox = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.caseNoTextBox2 = new System.Windows.Forms.TextBox();
            this.caseNoTextBox = new System.Windows.Forms.TextBox();
            this.dueDaysTextBox = new System.Windows.Forms.TextBox();
            this.addLessAmtFinalTextBox = new System.Windows.Forms.TextBox();
            this.addLessAmtTextBox = new System.Windows.Forms.TextBox();
            this.addLessTextBox = new System.Windows.Forms.TextBox();
            this.discPerAmtFinalTextBox = new System.Windows.Forms.TextBox();
            this.discPerAmtTextBox = new System.Windows.Forms.TextBox();
            this.discPerTextBox = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.taxValTextBox = new System.Windows.Forms.TextBox();
            this.billAmtTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.itemLinkLabel = new System.Windows.Forms.LinkLabel();
            this.label33 = new System.Windows.Forms.Label();
            this.vNoTextBox = new System.Windows.Forms.TextBox();
            this._texto_advanceDataSet = new Texo_Advance._texto_advanceDataSet();
            this.salestypeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sales_typeTableAdapter = new Texo_Advance._texto_advanceDataSetTableAdapters.sales_typeTableAdapter();
            this.label35 = new System.Windows.Forms.Label();
            this.dataGridViewComboBoxColumn1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.refBillNoComboBox = new JTG.ColumnComboBox();
            this.partyComboBox = new JTG.ColumnComboBox();
            this.itemNameGridCombo = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.packingComboGrid = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.unitComboGrid = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.sendEmailbtn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.saleDataGridView)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._texto_advanceDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.salestypeBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(18, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "COMPANY:";
            // 
            // comNameTextBox
            // 
            this.comNameTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comNameTextBox.Location = new System.Drawing.Point(127, 35);
            this.comNameTextBox.Name = "comNameTextBox";
            this.comNameTextBox.ReadOnly = true;
            this.comNameTextBox.Size = new System.Drawing.Size(228, 21);
            this.comNameTextBox.TabIndex = 2;
            this.comNameTextBox.TabStop = false;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(357, 35);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 16);
            this.label3.TabIndex = 3;
            this.label3.Text = "TYPE:";
            // 
            // salesTypeComboBox
            // 
            this.salesTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.salesTypeComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.salesTypeComboBox.FormattingEnabled = true;
            this.salesTypeComboBox.Location = new System.Drawing.Point(416, 34);
            this.salesTypeComboBox.Name = "salesTypeComboBox";
            this.salesTypeComboBox.Size = new System.Drawing.Size(158, 23);
            this.salesTypeComboBox.TabIndex = 0;
            this.salesTypeComboBox.TabStop = false;
            this.salesTypeComboBox.SelectedIndexChanged += new System.EventHandler(this.salesTypeComboBox_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(701, 37);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 16);
            this.label4.TabIndex = 5;
            this.label4.Text = "B. No.";
            // 
            // billNoTextBox
            // 
            this.billNoTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.billNoTextBox.Location = new System.Drawing.Point(752, 35);
            this.billNoTextBox.Name = "billNoTextBox";
            this.billNoTextBox.Size = new System.Drawing.Size(74, 21);
            this.billNoTextBox.TabIndex = 1;
            this.billNoTextBox.TextChanged += new System.EventHandler(this.billNoTextBox_TextChanged);
            this.billNoTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.billNoTextBox_KeyDown);
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(825, 36);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(106, 16);
            this.label5.TabIndex = 7;
            this.label5.Text = "ORD/REF NO.";
            // 
            // orderNoTextBox
            // 
            this.orderNoTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.orderNoTextBox.Location = new System.Drawing.Point(941, 33);
            this.orderNoTextBox.Name = "orderNoTextBox";
            this.orderNoTextBox.Size = new System.Drawing.Size(56, 21);
            this.orderNoTextBox.TabIndex = 2;
            this.orderNoTextBox.Text = "0";
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(1004, 36);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 16);
            this.label6.TabIndex = 9;
            this.label6.Text = "DATE:";
            // 
            // dateTextBox
            // 
            this.dateTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTextBox.Location = new System.Drawing.Point(1063, 33);
            this.dateTextBox.Name = "dateTextBox";
            this.dateTextBox.Size = new System.Drawing.Size(100, 21);
            this.dateTextBox.TabIndex = 3;
            this.dateTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.dateTextBox_Validating);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(18, 74);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(63, 16);
            this.label7.TabIndex = 11;
            this.label7.Text = "PARTY:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(357, 74);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(89, 16);
            this.label8.TabIndex = 13;
            this.label8.Text = "State Code:";
            // 
            // stateComboBox
            // 
            this.stateComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.stateComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stateComboBox.FormattingEnabled = true;
            this.stateComboBox.Location = new System.Drawing.Point(449, 74);
            this.stateComboBox.Name = "stateComboBox";
            this.stateComboBox.Size = new System.Drawing.Size(62, 23);
            this.stateComboBox.TabIndex = 4;
            this.stateComboBox.SelectedValueChanged += new System.EventHandler(this.stateComboBox_SelectedValueChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(516, 76);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(87, 16);
            this.label9.TabIndex = 15;
            this.label9.Text = "GST TYPE:";
            // 
            // gstTypeComboBox
            // 
            this.gstTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.gstTypeComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gstTypeComboBox.FormattingEnabled = true;
            this.gstTypeComboBox.Location = new System.Drawing.Point(603, 73);
            this.gstTypeComboBox.Name = "gstTypeComboBox";
            this.gstTypeComboBox.Size = new System.Drawing.Size(210, 23);
            this.gstTypeComboBox.TabIndex = 5;
            this.gstTypeComboBox.SelectedIndexChanged += new System.EventHandler(this.gstTypeComboBox_SelectedIndexChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(819, 75);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(78, 16);
            this.label10.TabIndex = 17;
            this.label10.Text = "STATION:";
            // 
            // stationComboBox
            // 
            this.stationComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.stationComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stationComboBox.FormattingEnabled = true;
            this.stationComboBox.Location = new System.Drawing.Point(899, 73);
            this.stationComboBox.Name = "stationComboBox";
            this.stationComboBox.Size = new System.Drawing.Size(127, 23);
            this.stationComboBox.TabIndex = 6;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(357, 109);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(74, 16);
            this.label11.TabIndex = 19;
            this.label11.Text = "BROKER:";
            // 
            // brokerComboBox
            // 
            this.brokerComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.brokerComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.brokerComboBox.FormattingEnabled = true;
            this.brokerComboBox.Location = new System.Drawing.Point(437, 108);
            this.brokerComboBox.Name = "brokerComboBox";
            this.brokerComboBox.Size = new System.Drawing.Size(208, 23);
            this.brokerComboBox.TabIndex = 8;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(18, 111);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(109, 16);
            this.label12.TabIndex = 21;
            this.label12.Text = "PARTY GSTN:";
            // 
            // gstnTextBox
            // 
            this.gstnTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gstnTextBox.Location = new System.Drawing.Point(127, 109);
            this.gstnTextBox.Name = "gstnTextBox";
            this.gstnTextBox.ReadOnly = true;
            this.gstnTextBox.Size = new System.Drawing.Size(228, 21);
            this.gstnTextBox.TabIndex = 7;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(654, 113);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(106, 16);
            this.label13.TabIndex = 23;
            this.label13.Text = "TRANSPORT:";
            // 
            // transportComboBox
            // 
            this.transportComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.transportComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.transportComboBox.FormattingEnabled = true;
            this.transportComboBox.Location = new System.Drawing.Point(761, 110);
            this.transportComboBox.Name = "transportComboBox";
            this.transportComboBox.Size = new System.Drawing.Size(174, 23);
            this.transportComboBox.TabIndex = 9;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(940, 111);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(61, 16);
            this.label14.TabIndex = 25;
            this.label14.Text = "LR NO.:";
            // 
            // lrnoTextBox
            // 
            this.lrnoTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lrnoTextBox.Location = new System.Drawing.Point(1011, 110);
            this.lrnoTextBox.Name = "lrnoTextBox";
            this.lrnoTextBox.Size = new System.Drawing.Size(134, 21);
            this.lrnoTextBox.TabIndex = 10;
            // 
            // saleDataGridView
            // 
            this.saleDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.saleDataGridView.BackgroundColor = System.Drawing.SystemColors.ControlDarkDark;
            this.saleDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.saleDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.itemNameGridCombo,
            this.bundlesEditBoxGrid,
            this.hsncEditBoxGrid,
            this.packingComboGrid,
            this.unitComboGrid,
            this.pcsTextBoxGrid,
            this.cutEditBoxGrid,
            this.qtyEditBoxGid,
            this.rateTextBoxGrid,
            this.amountTextBoxGrid});
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.saleDataGridView.DefaultCellStyle = dataGridViewCellStyle9;
            this.saleDataGridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.saleDataGridView.Location = new System.Drawing.Point(22, 151);
            this.saleDataGridView.MultiSelect = false;
            this.saleDataGridView.Name = "saleDataGridView";
            this.saleDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.saleDataGridView.Size = new System.Drawing.Size(1203, 244);
            this.saleDataGridView.TabIndex = 11;
            this.saleDataGridView.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.saleDataGridView_CellEndEdit);
            this.saleDataGridView.CellValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.saleDataGridView_CellValidated);
            this.saleDataGridView.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.saleDataGridView_CellValidating);
            this.saleDataGridView.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.saleDataGridView_CellValueChanged);
            this.saleDataGridView.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.saleDataGridView_EditingControlShowing);
            this.saleDataGridView.Paint += new System.Windows.Forms.PaintEventHandler(this.saleDataGridView_Paint);
            this.saleDataGridView.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.saleDataGridView_KeyPress);
            // 
            // bundlesEditBoxGrid
            // 
            this.bundlesEditBoxGrid.HeaderText = "BUNDLES";
            this.bundlesEditBoxGrid.Name = "bundlesEditBoxGrid";
            // 
            // hsncEditBoxGrid
            // 
            this.hsncEditBoxGrid.HeaderText = "HSN CODE";
            this.hsncEditBoxGrid.Name = "hsncEditBoxGrid";
            // 
            // pcsTextBoxGrid
            // 
            dataGridViewCellStyle4.NullValue = "0";
            this.pcsTextBoxGrid.DefaultCellStyle = dataGridViewCellStyle4;
            this.pcsTextBoxGrid.HeaderText = "PCS.";
            this.pcsTextBoxGrid.Name = "pcsTextBoxGrid";
            // 
            // cutEditBoxGrid
            // 
            dataGridViewCellStyle5.NullValue = "0";
            this.cutEditBoxGrid.DefaultCellStyle = dataGridViewCellStyle5;
            this.cutEditBoxGrid.HeaderText = "CUT";
            this.cutEditBoxGrid.Name = "cutEditBoxGrid";
            this.cutEditBoxGrid.Width = 75;
            // 
            // qtyEditBoxGid
            // 
            dataGridViewCellStyle6.NullValue = "0";
            this.qtyEditBoxGid.DefaultCellStyle = dataGridViewCellStyle6;
            this.qtyEditBoxGid.HeaderText = "MTS/QTY.";
            this.qtyEditBoxGid.Name = "qtyEditBoxGid";
            this.qtyEditBoxGid.Width = 83;
            // 
            // rateTextBoxGrid
            // 
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.NullValue = "0";
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black;
            this.rateTextBoxGrid.DefaultCellStyle = dataGridViewCellStyle7;
            this.rateTextBoxGrid.HeaderText = "RATE";
            this.rateTextBoxGrid.Name = "rateTextBoxGrid";
            // 
            // amountTextBoxGrid
            // 
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.NullValue = "0";
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.Black;
            this.amountTextBoxGrid.DefaultCellStyle = dataGridViewCellStyle8;
            this.amountTextBoxGrid.HeaderText = "AMOUNT";
            this.amountTextBoxGrid.Name = "amountTextBoxGrid";
            this.amountTextBoxGrid.ReadOnly = true;
            this.amountTextBoxGrid.Width = 145;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.label15.Location = new System.Drawing.Point(561, 440);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(117, 21);
            this.label15.TabIndex = 28;
            this.label15.Text = "GRAND TOTAL";
            // 
            // totalPcsTextBox
            // 
            this.totalPcsTextBox.BackColor = System.Drawing.SystemColors.Info;
            this.totalPcsTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalPcsTextBox.Location = new System.Drawing.Point(728, 442);
            this.totalPcsTextBox.Name = "totalPcsTextBox";
            this.totalPcsTextBox.ReadOnly = true;
            this.totalPcsTextBox.Size = new System.Drawing.Size(61, 22);
            this.totalPcsTextBox.TabIndex = 29;
            this.totalPcsTextBox.TabStop = false;
            // 
            // totalQtyTextBox
            // 
            this.totalQtyTextBox.BackColor = System.Drawing.SystemColors.Info;
            this.totalQtyTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalQtyTextBox.Location = new System.Drawing.Point(873, 442);
            this.totalQtyTextBox.Name = "totalQtyTextBox";
            this.totalQtyTextBox.ReadOnly = true;
            this.totalQtyTextBox.Size = new System.Drawing.Size(63, 22);
            this.totalQtyTextBox.TabIndex = 30;
            this.totalQtyTextBox.TabStop = false;
            // 
            // totalAmountTextBox
            // 
            this.totalAmountTextBox.BackColor = System.Drawing.SystemColors.Info;
            this.totalAmountTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalAmountTextBox.Location = new System.Drawing.Point(1054, 443);
            this.totalAmountTextBox.Name = "totalAmountTextBox";
            this.totalAmountTextBox.ReadOnly = true;
            this.totalAmountTextBox.Size = new System.Drawing.Size(100, 22);
            this.totalAmountTextBox.TabIndex = 31;
            this.totalAmountTextBox.TabStop = false;
            this.totalAmountTextBox.Text = "0";
            this.totalAmountTextBox.TextChanged += new System.EventHandler(this.totalAmountTextBox_TextChanged);
            // 
            // button1
            // 
            this.button1.Enabled = false;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(325, 601);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(90, 38);
            this.button1.TabIndex = 29;
            this.button1.Text = "SAVE BILL";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // addNewButton
            // 
            this.addNewButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.addNewButton.Location = new System.Drawing.Point(192, 601);
            this.addNewButton.Name = "addNewButton";
            this.addNewButton.Size = new System.Drawing.Size(90, 38);
            this.addNewButton.TabIndex = 28;
            this.addNewButton.Text = "ADD NEW";
            this.addNewButton.UseVisualStyleBackColor = true;
            this.addNewButton.Click += new System.EventHandler(this.addNewButton_Click);
            // 
            // printButton
            // 
            this.printButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.printButton.Location = new System.Drawing.Point(597, 601);
            this.printButton.Name = "printButton";
            this.printButton.Size = new System.Drawing.Size(90, 38);
            this.printButton.TabIndex = 31;
            this.printButton.Text = "PRINT";
            this.printButton.UseVisualStyleBackColor = true;
            this.printButton.Click += new System.EventHandler(this.printButton_Click);
            // 
            // deleteButton
            // 
            this.deleteButton.Enabled = false;
            this.deleteButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.deleteButton.Location = new System.Drawing.Point(729, 601);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(75, 38);
            this.deleteButton.TabIndex = 32;
            this.deleteButton.Text = "DELETE";
            this.deleteButton.UseVisualStyleBackColor = true;
            this.deleteButton.Click += new System.EventHandler(this.deleteButton_Click);
            // 
            // openBillButton
            // 
            this.openBillButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.openBillButton.Location = new System.Drawing.Point(456, 601);
            this.openBillButton.Name = "openBillButton";
            this.openBillButton.Size = new System.Drawing.Size(99, 38);
            this.openBillButton.TabIndex = 30;
            this.openBillButton.Text = "OPEN BILL";
            this.openBillButton.UseVisualStyleBackColor = true;
            this.openBillButton.Click += new System.EventHandler(this.openBillButton_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(22, 447);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(75, 16);
            this.label16.TabIndex = 37;
            this.label16.Text = "REMARK:";
            // 
            // remarkTextBox
            // 
            this.remarkTextBox.BackColor = System.Drawing.SystemColors.Info;
            this.remarkTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.remarkTextBox.Location = new System.Drawing.Point(103, 446);
            this.remarkTextBox.Name = "remarkTextBox";
            this.remarkTextBox.Size = new System.Drawing.Size(451, 21);
            this.remarkTextBox.TabIndex = 12;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.igstAmtTextBox);
            this.groupBox1.Controls.Add(this.sgstAmtTextBox);
            this.groupBox1.Controls.Add(this.cgstAmtTextBox);
            this.groupBox1.Controls.Add(this.label30);
            this.groupBox1.Controls.Add(this.label29);
            this.groupBox1.Controls.Add(this.label28);
            this.groupBox1.Controls.Add(this.igstRateTextBox);
            this.groupBox1.Controls.Add(this.sgstRateTextBox);
            this.groupBox1.Controls.Add(this.cgstRateTextBox);
            this.groupBox1.Controls.Add(this.label27);
            this.groupBox1.Controls.Add(this.label26);
            this.groupBox1.Controls.Add(this.label25);
            this.groupBox1.Controls.Add(this.label22);
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.caseNoTextBox2);
            this.groupBox1.Controls.Add(this.caseNoTextBox);
            this.groupBox1.Controls.Add(this.dueDaysTextBox);
            this.groupBox1.Controls.Add(this.addLessAmtFinalTextBox);
            this.groupBox1.Controls.Add(this.addLessAmtTextBox);
            this.groupBox1.Controls.Add(this.addLessTextBox);
            this.groupBox1.Controls.Add(this.discPerAmtFinalTextBox);
            this.groupBox1.Controls.Add(this.discPerAmtTextBox);
            this.groupBox1.Controls.Add(this.discPerTextBox);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.label34);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.label24);
            this.groupBox1.Controls.Add(this.label23);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(37, 481);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(767, 100);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "DETAILS";
            // 
            // igstAmtTextBox
            // 
            this.igstAmtTextBox.Location = new System.Drawing.Point(662, 72);
            this.igstAmtTextBox.Name = "igstAmtTextBox";
            this.igstAmtTextBox.Size = new System.Drawing.Size(93, 22);
            this.igstAmtTextBox.TabIndex = 27;
            this.igstAmtTextBox.Text = "0";
            this.igstAmtTextBox.TextChanged += new System.EventHandler(this.igstAmtTextBox_TextChanged);
            // 
            // sgstAmtTextBox
            // 
            this.sgstAmtTextBox.Location = new System.Drawing.Point(662, 46);
            this.sgstAmtTextBox.Name = "sgstAmtTextBox";
            this.sgstAmtTextBox.Size = new System.Drawing.Size(93, 22);
            this.sgstAmtTextBox.TabIndex = 25;
            this.sgstAmtTextBox.Text = "0";
            // 
            // cgstAmtTextBox
            // 
            this.cgstAmtTextBox.Location = new System.Drawing.Point(662, 20);
            this.cgstAmtTextBox.Name = "cgstAmtTextBox";
            this.cgstAmtTextBox.Size = new System.Drawing.Size(93, 22);
            this.cgstAmtTextBox.TabIndex = 23;
            this.cgstAmtTextBox.Text = "0";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(586, 75);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(69, 16);
            this.label30.TabIndex = 8;
            this.label30.Text = "%  AMT.:";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(585, 47);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(69, 16);
            this.label29.TabIndex = 8;
            this.label29.Text = "%  AMT.:";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(585, 20);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(69, 16);
            this.label28.TabIndex = 8;
            this.label28.Text = "%  AMT.:";
            // 
            // igstRateTextBox
            // 
            this.igstRateTextBox.Location = new System.Drawing.Point(535, 74);
            this.igstRateTextBox.Name = "igstRateTextBox";
            this.igstRateTextBox.Size = new System.Drawing.Size(44, 22);
            this.igstRateTextBox.TabIndex = 26;
            this.igstRateTextBox.Text = "5";
            this.igstRateTextBox.TextChanged += new System.EventHandler(this.igstRateTextBox_TextChanged);
            // 
            // sgstRateTextBox
            // 
            this.sgstRateTextBox.Location = new System.Drawing.Point(535, 47);
            this.sgstRateTextBox.Name = "sgstRateTextBox";
            this.sgstRateTextBox.Size = new System.Drawing.Size(44, 22);
            this.sgstRateTextBox.TabIndex = 24;
            this.sgstRateTextBox.Text = "2.5";
            this.sgstRateTextBox.TextChanged += new System.EventHandler(this.sgstRateTextBox_TextChanged);
            // 
            // cgstRateTextBox
            // 
            this.cgstRateTextBox.Location = new System.Drawing.Point(535, 20);
            this.cgstRateTextBox.Name = "cgstRateTextBox";
            this.cgstRateTextBox.Size = new System.Drawing.Size(44, 22);
            this.cgstRateTextBox.TabIndex = 22;
            this.cgstRateTextBox.Text = "2.5";
            this.cgstRateTextBox.TextChanged += new System.EventHandler(this.cgstRateTextBox_TextChanged);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(475, 76);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(43, 16);
            this.label27.TabIndex = 6;
            this.label27.Text = "IGST";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(475, 50);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(49, 16);
            this.label26.TabIndex = 6;
            this.label26.Text = "SGST";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(475, 24);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(49, 16);
            this.label25.TabIndex = 6;
            this.label25.Text = "CGST";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(188, 73);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(17, 16);
            this.label22.TabIndex = 5;
            this.label22.Text = "X";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(188, 50);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(17, 16);
            this.label21.TabIndex = 5;
            this.label21.Text = "X";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(188, 28);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(21, 16);
            this.label20.TabIndex = 5;
            this.label20.Text = "%";
            // 
            // caseNoTextBox2
            // 
            this.caseNoTextBox2.Location = new System.Drawing.Point(226, 72);
            this.caseNoTextBox2.Name = "caseNoTextBox2";
            this.caseNoTextBox2.Size = new System.Drawing.Size(33, 22);
            this.caseNoTextBox2.TabIndex = 20;
            this.caseNoTextBox2.Text = "1";
            // 
            // caseNoTextBox
            // 
            this.caseNoTextBox.Location = new System.Drawing.Point(115, 72);
            this.caseNoTextBox.Name = "caseNoTextBox";
            this.caseNoTextBox.Size = new System.Drawing.Size(67, 22);
            this.caseNoTextBox.TabIndex = 19;
            // 
            // dueDaysTextBox
            // 
            this.dueDaysTextBox.ForeColor = System.Drawing.Color.Red;
            this.dueDaysTextBox.Location = new System.Drawing.Point(357, 72);
            this.dueDaysTextBox.Name = "dueDaysTextBox";
            this.dueDaysTextBox.Size = new System.Drawing.Size(71, 22);
            this.dueDaysTextBox.TabIndex = 21;
            this.dueDaysTextBox.Text = "0";
            this.dueDaysTextBox.TextChanged += new System.EventHandler(this.dueDaysTextBox_TextChanged);
            // 
            // addLessAmtFinalTextBox
            // 
            this.addLessAmtFinalTextBox.ForeColor = System.Drawing.Color.Red;
            this.addLessAmtFinalTextBox.Location = new System.Drawing.Point(357, 46);
            this.addLessAmtFinalTextBox.Name = "addLessAmtFinalTextBox";
            this.addLessAmtFinalTextBox.Size = new System.Drawing.Size(71, 22);
            this.addLessAmtFinalTextBox.TabIndex = 18;
            this.addLessAmtFinalTextBox.Text = "0";
            this.addLessAmtFinalTextBox.TextChanged += new System.EventHandler(this.addLessAmtFinalTextBox_TextChanged);
            // 
            // addLessAmtTextBox
            // 
            this.addLessAmtTextBox.Location = new System.Drawing.Point(226, 46);
            this.addLessAmtTextBox.Name = "addLessAmtTextBox";
            this.addLessAmtTextBox.Size = new System.Drawing.Size(71, 22);
            this.addLessAmtTextBox.TabIndex = 17;
            this.addLessAmtTextBox.Text = "0";
            this.addLessAmtTextBox.TextChanged += new System.EventHandler(this.addLessAmtTextBox_TextChanged);
            // 
            // addLessTextBox
            // 
            this.addLessTextBox.Location = new System.Drawing.Point(139, 46);
            this.addLessTextBox.Name = "addLessTextBox";
            this.addLessTextBox.Size = new System.Drawing.Size(43, 22);
            this.addLessTextBox.TabIndex = 16;
            this.addLessTextBox.Text = "0";
            this.addLessTextBox.TextChanged += new System.EventHandler(this.addLessTextBox_TextChanged);
            // 
            // discPerAmtFinalTextBox
            // 
            this.discPerAmtFinalTextBox.ForeColor = System.Drawing.Color.Red;
            this.discPerAmtFinalTextBox.Location = new System.Drawing.Point(357, 22);
            this.discPerAmtFinalTextBox.Name = "discPerAmtFinalTextBox";
            this.discPerAmtFinalTextBox.Size = new System.Drawing.Size(71, 22);
            this.discPerAmtFinalTextBox.TabIndex = 15;
            this.discPerAmtFinalTextBox.Text = "0";
            this.discPerAmtFinalTextBox.TextChanged += new System.EventHandler(this.discPerAmtFinalTextBox_TextChanged);
            // 
            // discPerAmtTextBox
            // 
            this.discPerAmtTextBox.Location = new System.Drawing.Point(226, 22);
            this.discPerAmtTextBox.Name = "discPerAmtTextBox";
            this.discPerAmtTextBox.Size = new System.Drawing.Size(71, 22);
            this.discPerAmtTextBox.TabIndex = 14;
            this.discPerAmtTextBox.Text = "0";
            this.discPerAmtTextBox.TextChanged += new System.EventHandler(this.discPerAmtTextBox_TextChanged);
            // 
            // discPerTextBox
            // 
            this.discPerTextBox.Location = new System.Drawing.Point(139, 22);
            this.discPerTextBox.Name = "discPerTextBox";
            this.discPerTextBox.Size = new System.Drawing.Size(43, 22);
            this.discPerTextBox.TabIndex = 13;
            this.discPerTextBox.Text = "0";
            this.discPerTextBox.TextChanged += new System.EventHandler(this.discPerTextBox_TextChanged);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(17, 75);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(78, 16);
            this.label19.TabIndex = 2;
            this.label19.Text = "CASE NO.";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(268, 75);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(89, 16);
            this.label34.TabIndex = 0;
            this.label34.Text = "DUE DAYS:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(16, 48);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(83, 16);
            this.label18.TabIndex = 1;
            this.label18.Text = "ADD/LESS";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(303, 46);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(48, 16);
            this.label24.TabIndex = 0;
            this.label24.Text = "AMT.:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(303, 22);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(48, 16);
            this.label23.TabIndex = 0;
            this.label23.Text = "AMT.:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(16, 22);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(86, 16);
            this.label17.TabIndex = 0;
            this.label17.Text = "DISCOUNT";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(872, 501);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(132, 16);
            this.label31.TabIndex = 40;
            this.label31.Text = "TAXABLE VALUE:";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(872, 554);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(111, 16);
            this.label32.TabIndex = 40;
            this.label32.Text = "BILL AMOUNT:";
            // 
            // taxValTextBox
            // 
            this.taxValTextBox.BackColor = System.Drawing.Color.Lime;
            this.taxValTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.taxValTextBox.ForeColor = System.Drawing.SystemColors.WindowText;
            this.taxValTextBox.Location = new System.Drawing.Point(1054, 499);
            this.taxValTextBox.Name = "taxValTextBox";
            this.taxValTextBox.ReadOnly = true;
            this.taxValTextBox.Size = new System.Drawing.Size(100, 26);
            this.taxValTextBox.TabIndex = 41;
            this.taxValTextBox.TabStop = false;
            this.taxValTextBox.Text = "0";
            this.taxValTextBox.TextChanged += new System.EventHandler(this.taxValTextBox_TextChanged);
            // 
            // billAmtTextBox
            // 
            this.billAmtTextBox.BackColor = System.Drawing.Color.Lime;
            this.billAmtTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.billAmtTextBox.ForeColor = System.Drawing.Color.Blue;
            this.billAmtTextBox.Location = new System.Drawing.Point(1054, 550);
            this.billAmtTextBox.Name = "billAmtTextBox";
            this.billAmtTextBox.ReadOnly = true;
            this.billAmtTextBox.Size = new System.Drawing.Size(100, 26);
            this.billAmtTextBox.TabIndex = 41;
            this.billAmtTextBox.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Blue;
            this.label1.Location = new System.Drawing.Point(566, -2);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(123, 30);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tax Invoice";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(17, 96);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(78, 13);
            this.linkLabel1.TabIndex = 42;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Add New Party";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // itemLinkLabel
            // 
            this.itemLinkLabel.AutoSize = true;
            this.itemLinkLabel.Location = new System.Drawing.Point(109, 136);
            this.itemLinkLabel.Name = "itemLinkLabel";
            this.itemLinkLabel.Size = new System.Drawing.Size(74, 13);
            this.itemLinkLabel.TabIndex = 42;
            this.itemLinkLabel.TabStop = true;
            this.itemLinkLabel.Text = "Add New Item";
            this.itemLinkLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.itemLinkLabel_LinkClicked);
            // 
            // label33
            // 
            this.label33.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(575, 38);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(50, 16);
            this.label33.TabIndex = 5;
            this.label33.Text = "V. No.";
            // 
            // vNoTextBox
            // 
            this.vNoTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vNoTextBox.Location = new System.Drawing.Point(627, 36);
            this.vNoTextBox.Name = "vNoTextBox";
            this.vNoTextBox.Size = new System.Drawing.Size(74, 21);
            this.vNoTextBox.TabIndex = 0;
            this.vNoTextBox.TabStop = false;
            this.vNoTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.vNoTextBox_KeyDown);
            // 
            // _texto_advanceDataSet
            // 
            this._texto_advanceDataSet.DataSetName = "_texto_advanceDataSet";
            this._texto_advanceDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // salestypeBindingSource
            // 
            this.salestypeBindingSource.DataMember = "sales_type";
            this.salestypeBindingSource.DataSource = this._texto_advanceDataSet;
            // 
            // sales_typeTableAdapter
            // 
            this.sales_typeTableAdapter.ClearBeforeFill = true;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(828, 36);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(98, 16);
            this.label35.TabIndex = 17;
            this.label35.Text = "REF BILLNO:";
            // 
            // dataGridViewComboBoxColumn1
            // 
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.Black;
            this.dataGridViewComboBoxColumn1.DefaultCellStyle = dataGridViewCellStyle10;
            this.dataGridViewComboBoxColumn1.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn1.HeaderText = "ITEM NAME";
            this.dataGridViewComboBoxColumn1.MaxDropDownItems = 100;
            this.dataGridViewComboBoxColumn1.Name = "dataGridViewComboBoxColumn1";
            this.dataGridViewComboBoxColumn1.Width = 230;
            // 
            // dataGridViewComboBoxColumn2
            // 
            dataGridViewCellStyle11.NullValue = "BEG";
            this.dataGridViewComboBoxColumn2.DefaultCellStyle = dataGridViewCellStyle11;
            this.dataGridViewComboBoxColumn2.HeaderText = "PACKING";
            this.dataGridViewComboBoxColumn2.Items.AddRange(new object[] {
            "BEG",
            "NAKED",
            "BOX"});
            this.dataGridViewComboBoxColumn2.Name = "dataGridViewComboBoxColumn2";
            // 
            // dataGridViewComboBoxColumn3
            // 
            dataGridViewCellStyle12.NullValue = "PCS";
            this.dataGridViewComboBoxColumn3.DefaultCellStyle = dataGridViewCellStyle12;
            this.dataGridViewComboBoxColumn3.HeaderText = "UNIT";
            this.dataGridViewComboBoxColumn3.Items.AddRange(new object[] {
            "PCS",
            "MTS",
            "KGS",
            "OTHERS",
            "SUITS"});
            this.dataGridViewComboBoxColumn3.Name = "dataGridViewComboBoxColumn3";
            // 
            // refBillNoComboBox
            // 
            this.refBillNoComboBox.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.refBillNoComboBox.DropDownWidth = 17;
            this.refBillNoComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.refBillNoComboBox.Location = new System.Drawing.Point(923, 33);
            this.refBillNoComboBox.Name = "refBillNoComboBox";
            this.refBillNoComboBox.Size = new System.Drawing.Size(81, 22);
            this.refBillNoComboBox.TabIndex = 2;
            this.refBillNoComboBox.ViewColumn = 0;
            // 
            // partyComboBox
            // 
            this.partyComboBox.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.partyComboBox.DropDownWidth = 17;
            this.partyComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.partyComboBox.FormattingEnabled = true;
            this.partyComboBox.Location = new System.Drawing.Point(127, 71);
            this.partyComboBox.MaxDropDownItems = 100;
            this.partyComboBox.Name = "partyComboBox";
            this.partyComboBox.Size = new System.Drawing.Size(228, 22);
            this.partyComboBox.TabIndex = 4;
            this.partyComboBox.ViewColumn = 0;
            this.partyComboBox.SelectedIndexChanged += new System.EventHandler(this.partyComboBox_SelectedIndexChanged);
            this.partyComboBox.Validating += new System.ComponentModel.CancelEventHandler(this.partyComboBox_Validating);
            // 
            // itemNameGridCombo
            // 
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            this.itemNameGridCombo.DefaultCellStyle = dataGridViewCellStyle1;
            this.itemNameGridCombo.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.itemNameGridCombo.HeaderText = "ITEM NAME";
            this.itemNameGridCombo.MaxDropDownItems = 100;
            this.itemNameGridCombo.Name = "itemNameGridCombo";
            this.itemNameGridCombo.Width = 230;
            // 
            // packingComboGrid
            // 
            dataGridViewCellStyle2.NullValue = "BEG";
            this.packingComboGrid.DefaultCellStyle = dataGridViewCellStyle2;
            this.packingComboGrid.HeaderText = "PACKING";
            this.packingComboGrid.Items.AddRange(new object[] {
            "BEG",
            "NAKED",
            "BOX"});
            this.packingComboGrid.Name = "packingComboGrid";
            // 
            // unitComboGrid
            // 
            dataGridViewCellStyle3.NullValue = "PCS";
            this.unitComboGrid.DefaultCellStyle = dataGridViewCellStyle3;
            this.unitComboGrid.HeaderText = "UNIT";
            this.unitComboGrid.Items.AddRange(new object[] {
            "PCS",
            "MTS",
            "KGS",
            "OTHERS",
            "SUITS"});
            this.unitComboGrid.Name = "unitComboGrid";
            // 
            // sendEmailbtn
            // 
            this.sendEmailbtn.BackColor = System.Drawing.SystemColors.HotTrack;
            this.sendEmailbtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.sendEmailbtn.Image = ((System.Drawing.Image)(resources.GetObject("sendEmailbtn.Image")));
            this.sendEmailbtn.Location = new System.Drawing.Point(1054, 601);
            this.sendEmailbtn.Name = "sendEmailbtn";
            this.sendEmailbtn.Size = new System.Drawing.Size(91, 38);
            this.sendEmailbtn.TabIndex = 43;
            this.sendEmailbtn.UseVisualStyleBackColor = false;
            this.sendEmailbtn.Click += new System.EventHandler(this.button2_Click);
            // 
            // salesWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.sendEmailbtn);
            this.Controls.Add(this.refBillNoComboBox);
            this.Controls.Add(this.partyComboBox);
            this.Controls.Add(this.saleDataGridView);
            this.Controls.Add(this.itemLinkLabel);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.billAmtTextBox);
            this.Controls.Add(this.taxValTextBox);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.remarkTextBox);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.openBillButton);
            this.Controls.Add(this.deleteButton);
            this.Controls.Add(this.printButton);
            this.Controls.Add(this.addNewButton);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.totalAmountTextBox);
            this.Controls.Add(this.totalQtyTextBox);
            this.Controls.Add(this.totalPcsTextBox);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.lrnoTextBox);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.transportComboBox);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.gstnTextBox);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.brokerComboBox);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label35);
            this.Controls.Add(this.stationComboBox);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.gstTypeComboBox);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.stateComboBox);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.dateTextBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.orderNoTextBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.vNoTextBox);
            this.Controls.Add(this.billNoTextBox);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.salesTypeComboBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.comNameTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "salesWindow";
            this.Size = new System.Drawing.Size(1278, 667);
            this.Load += new System.EventHandler(this.salesWindow_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.salesWindow_KeyPress);
            ((System.ComponentModel.ISupportInitialize)(this.saleDataGridView)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._texto_advanceDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.salestypeBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox comNameTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox salesTypeComboBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox billNoTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox orderNoTextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox dateTextBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox stateComboBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox gstTypeComboBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox stationComboBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox brokerComboBox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox gstnTextBox;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox transportComboBox;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox lrnoTextBox;
        private System.Windows.Forms.DataGridView saleDataGridView;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox totalPcsTextBox;
        private System.Windows.Forms.TextBox totalQtyTextBox;
        private System.Windows.Forms.TextBox totalAmountTextBox;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button addNewButton;
        private System.Windows.Forms.Button printButton;
        private System.Windows.Forms.Button deleteButton;
        private System.Windows.Forms.Button openBillButton;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox remarkTextBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox caseNoTextBox2;
        private System.Windows.Forms.TextBox caseNoTextBox;
        private System.Windows.Forms.TextBox addLessAmtFinalTextBox;
        private System.Windows.Forms.TextBox addLessAmtTextBox;
        private System.Windows.Forms.TextBox addLessTextBox;
        private System.Windows.Forms.TextBox discPerAmtFinalTextBox;
        private System.Windows.Forms.TextBox discPerAmtTextBox;
        private System.Windows.Forms.TextBox discPerTextBox;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox igstAmtTextBox;
        private System.Windows.Forms.TextBox sgstAmtTextBox;
        private System.Windows.Forms.TextBox cgstAmtTextBox;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox igstRateTextBox;
        private System.Windows.Forms.TextBox sgstRateTextBox;
        private System.Windows.Forms.TextBox cgstRateTextBox;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox taxValTextBox;
        private System.Windows.Forms.TextBox billAmtTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.BindingSource salestypeBindingSource;
        private _texto_advanceDataSet _texto_advanceDataSet;
        private _texto_advanceDataSetTableAdapters.sales_typeTableAdapter sales_typeTableAdapter;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.LinkLabel itemLinkLabel;
        private System.Windows.Forms.DataGridViewComboBoxColumn itemNameGridCombo;
        private System.Windows.Forms.DataGridViewTextBoxColumn bundlesEditBoxGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn hsncEditBoxGrid;
        private System.Windows.Forms.DataGridViewComboBoxColumn packingComboGrid;
        private System.Windows.Forms.DataGridViewComboBoxColumn unitComboGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn pcsTextBoxGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn cutEditBoxGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn qtyEditBoxGid;
        private System.Windows.Forms.DataGridViewTextBoxColumn rateTextBoxGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn amountTextBoxGrid;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox vNoTextBox;
        private System.Windows.Forms.TextBox dueDaysTextBox;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn1;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn2;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn3;
        private JTG.ColumnComboBox partyComboBox;
        private System.Windows.Forms.Label label35;
        private JTG.ColumnComboBox refBillNoComboBox;
        private System.Windows.Forms.Button sendEmailbtn;
    }
}
