﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Texo_Advance.DBItems;

namespace Texo_Advance
{
    public partial class QualityInformationManagerForm : Form
    {
        MasterDBData DB = new MasterDBData();
        int iMainScreenItemIndex = -1;
        private bool bMainScreenAddMode = false;//Add Main Screen Item in Purchse mode by default
        public QualityInformationManagerForm()
        {
            InitializeComponent();
        }

        private void QualityInformationManagerForm_Load(object sender, EventArgs e)
        {
            if (packingComboBox.Items.Count > 0)
                packingComboBox.SelectedIndex = 0;
            if (unitComboBox.Items.Count > 0)
                unitComboBox.SelectedIndex = 0;
            if (typeComboBox.Items.Count > 0)
                typeComboBox.SelectedIndex = 0;
            fillItemTypeList();
            if (itemTypeComboBox.Items.Count > 0)
                itemTypeComboBox.SelectedIndex = 0;
            fillMainScreenList();
            mainScreenComboBox.SelectedIndex = -1;
        }

        private void fillItemTypeList()
        {
            var itemTypeList = DB.getItemTypeList();
            if(itemTypeList != null)
            {
                itemTypeComboBox.DisplayMember = "itemTypeName";
                itemTypeComboBox.ValueMember = "itemTypeId";
                itemTypeComboBox.DataSource = itemTypeList;
            }
        }
        private void addButton_Click(object sender, EventArgs e)
        {
            if(itemNameTextBox.TextLength > 0)
            {
                var itemDetails = new item_details();
                if (mainScreenComboBox.SelectedIndex == -1)
                    itemDetails.catogeryid = -1;
                else
                    itemDetails.catogeryid = Convert.ToInt64(mainScreenComboBox.SelectedValue);
                itemDetails.cut = cutTextBox.Text;
                itemDetails.greyquality_id = greyComboBox.SelectedIndex;
                itemDetails.gst =  gstTextBox.Text;
                itemDetails.hsncode = hsncTextBox.Text;
                itemDetails.itemname = itemNameTextBox.Text;
                itemDetails.itemtype_id = itemTypeComboBox.SelectedIndex;
                itemDetails.packing_id = packingComboBox.SelectedIndex;
                itemDetails.selling_rate = sellingRateTextBox.Text;
                itemDetails.type_id = typeComboBox.SelectedIndex;
                itemDetails.unit_id = unitComboBox.SelectedIndex;
                var catogeryDetails = new catogery_details();
                catogeryDetails.catogeryname = mainScreenComboBox.Text;
                catogeryDetails.companyId = Properties.Settings.Default.CompanyId; 
                catogeryDetails.financialYear_Id = Properties.Settings.Default.FinancialYearId;
                try
                {
                    using (Entities db = new Entities())
                    {
                        if(bMainScreenAddMode)
                        {
                            if (iMainScreenItemIndex == -1)
                            {
                                db.catogery_details.Add(catogeryDetails);
                                db.SaveChanges();
                            }
                                
                        }
                        long lCatogeryId = db.catogery_details.Where(x => x.catogeryname == mainScreenComboBox.Text).Select(x => x.catogeryid).DefaultIfEmpty(0).SingleOrDefault();
                        if (lCatogeryId > 0)
                            itemDetails.catogeryid = lCatogeryId;
                        db.item_details.Add(itemDetails);
                        db.SaveChanges();
                    }
                    MessageBox.Show("Item added Successfully", "New Item", MessageBoxButtons.OK);
                    Close();
                }
                catch(Exception ex)
                {
                    MessageBox.Show("Item not added", "ERROR", MessageBoxButtons.OK);
                    itemNameTextBox.Focus();
                }
                
            }
            else
            {
                MessageBox.Show("Please enter the Item Name", "Item Name", MessageBoxButtons.OK);
                itemNameTextBox.Focus();
            }
        }

        private void mainScreenComboBox_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                if(mainScreenComboBox.Text.Length == 0)
                {
                    MessageBox.Show("Please Enter the MainScreen Name", "MainScreen", MessageBoxButtons.OK);
                    mainScreenComboBox.Focus();
                    return;
                }
                DialogResult result = MessageBox.Show("Do you want to add new MainScreen Item", "ADD", MessageBoxButtons.YesNo);
                if(result == DialogResult.Yes)
                {
                    bool bMainScreenAdded = DB.addMainScreenCatogery(mainScreenComboBox.Text);
                    if (bMainScreenAdded)
                    {
                        fillMainScreenList();
                    }
                    else
                    {
                        MessageBox.Show("Main Screen not added", "Main Screen", MessageBoxButtons.OK);
                        mainScreenComboBox.Focus();
                        return;
                    }
                }
                
            }

        }

        private void fillMainScreenList()
        {
            var mainScreenList = DB.getMainScreenItemList();
            if(mainScreenList != null)
            {
                mainScreenComboBox.DisplayMember = "catogeryname";
                mainScreenComboBox.ValueMember = "catogeryid";
                mainScreenComboBox.DataSource = mainScreenList;
            }
        }

        private void itemNameTextBox_Validating(object sender, CancelEventArgs e)
        {
            if(bMainScreenAddMode)
            {
                if (itemNameTextBox.Text.Length > 0)
                {
                    string itemName = itemNameTextBox.Text;
                    iMainScreenItemIndex = mainScreenComboBox.FindString(itemName);
                    if (iMainScreenItemIndex == -1)
                    {
                        mainScreenComboBox.Text = itemName;
                    }
                    else
                        mainScreenComboBox.SelectedIndex = iMainScreenItemIndex;
                }
            }
        }
        
        public void setMainScreenAddMode(bool bFlag)
        {
            bMainScreenAddMode = bFlag;
        }
    }
}
