/*
SQLyog Community v10.2 
MySQL - 5.7.31-log : Database - texto-advance
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`texto-advance` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `texto-advance`;

/*Table structure for table `account_details` */

DROP TABLE IF EXISTS `account_details`;

CREATE TABLE `account_details` (
  `account_typeid` bigint(20) NOT NULL AUTO_INCREMENT,
  `account_type` varchar(100) NOT NULL,
  `version` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`account_typeid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `account_details` */

/*Table structure for table `account_type` */

DROP TABLE IF EXISTS `account_type`;

CREATE TABLE `account_type` (
  `account_typeid` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) DEFAULT NULL,
  `effect_on` varchar(50) DEFAULT NULL,
  `version` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`account_typeid`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;

/*Data for the table `account_type` */

insert  into `account_type`(`account_typeid`,`type`,`effect_on`,`version`) values (1,'Sundry Debtors','BalanceSheet_Asset',NULL),(2,'Sundry Creditors','BalanceSheet_Liability',NULL),(3,'Loan A/C','BalanceSheet_Liability',NULL),(4,'Unsecured Loan','BalanceSheet',NULL),(5,'TDS A/C','BalanceSheet',NULL),(6,'Bank A/C','BalanceSheet',NULL),(7,'Capital A/C','BalanceSheet_Liability',NULL),(8,'Creditors For Packing Material','BalanceSheet_Liability',NULL),(9,'Debtors For Packing Material','BalanceSheet_Asset',NULL),(10,'Creditors For Dyeing & Printing Charges','BalanceSheet_Liability',NULL),(11,'Creditors For Job Work','BalanceSheet_Liability',NULL),(12,'Broker / Agent','BalanceSheet',NULL),(13,'Creditors For Others','BalanceSheet_Liability',NULL),(14,'IGST A/C','BalanceSheet',NULL),(15,'CGST A/C','BalanceSheet',NULL),(16,'SGST A/C','BalanceSheet',NULL),(17,'Closing Stock','Trading A/C',NULL),(18,'Cash A/C','BalanceSheet_Asset',NULL),(19,'Secured Loan','BalanceSheet',NULL),(20,'Creditors For Grey','BalanceSheet_Liability',NULL),(21,'Other Liabilities','BalanceSheet_Liability',NULL),(22,'Plant & Machinery A/C','BalanceSheet_FixedAsset',NULL),(23,'Depreciation A/C','Profit & Loss',NULL),(24,'Investment A/C','BalanceSheet_Asset',NULL),(25,'PPF A/C','BalanceSheet_Asset',NULL),(26,'Opening Stock','Trading A/C',NULL),(27,'Creditors For Expenses','BalanceSheet_Liability',NULL),(28,'Creditors For Service','BalanceSheet_Liability',NULL),(29,'Debtors For Others','BalanceSheet_Asset',NULL),(30,'Fixed Assets','BalanceSheet_Asset',NULL),(31,'Furniture of Office','BalanceSheet_Asset',NULL),(32,'Loans & Advances','BalanceSheet_Asset',NULL),(33,'Others','BalanceSheet_Asset',NULL),(34,'P&L Expenses','Profit & Loss',NULL),(35,'P&L Income','Profit & Loss',NULL),(36,'Prepaid Expenses','BalanceSheet_Liability',NULL),(37,'Provision for Tax Saving','BalanceSheet_Asset',NULL),(38,'Share Application','BalanceSheet_Asset',NULL),(39,'Shree Ganesay Namah','BalanceSheet_Asset',NULL),(40,'Staff A/C','BalanceSheet_Liability',NULL),(41,'Staff Advance','BalanceSheet_Asset',NULL),(42,'Trading Expenses','Trading A/C',NULL),(43,'Trading Income','Trading A/C',NULL),(44,'Vehicle A/C','BalanceSheet_Asset',NULL);

/*Table structure for table `bank_details` */

DROP TABLE IF EXISTS `bank_details`;

CREATE TABLE `bank_details` (
  `bankDetails_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `bankName` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`bankDetails_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `bank_details` */

insert  into `bank_details`(`bankDetails_id`,`bankName`) values (1,'KVB');

/*Table structure for table `broker_details` */

DROP TABLE IF EXISTS `broker_details`;

CREATE TABLE `broker_details` (
  `brokerid` bigint(20) NOT NULL AUTO_INCREMENT,
  `account_typeid` bigint(20) NOT NULL,
  `party_name` varchar(100) NOT NULL,
  `address` varchar(4000) DEFAULT NULL,
  `address2` varchar(4000) DEFAULT NULL,
  `city_id` bigint(20) DEFAULT NULL,
  `pincode` bigint(20) DEFAULT NULL,
  `distance` bigint(10) DEFAULT NULL,
  `station_name` varchar(100) DEFAULT NULL,
  `transportid` bigint(20) DEFAULT NULL,
  `contactperson` varchar(500) DEFAULT NULL,
  `phone1` bigint(20) DEFAULT NULL,
  `phone2` bigint(20) DEFAULT NULL,
  `emailid` varchar(100) DEFAULT NULL,
  `panno` varchar(20) DEFAULT NULL,
  `gst` varchar(50) DEFAULT NULL,
  `ledger_typeid` bigint(20) DEFAULT NULL,
  `brokerage` double DEFAULT NULL,
  `tds` double DEFAULT NULL,
  `version` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`brokerid`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;

/*Data for the table `broker_details` */

insert  into `broker_details`(`brokerid`,`account_typeid`,`party_name`,`address`,`address2`,`city_id`,`pincode`,`distance`,`station_name`,`transportid`,`contactperson`,`phone1`,`phone2`,`emailid`,`panno`,`gst`,`ledger_typeid`,`brokerage`,`tds`,`version`) values (37,12,'DIRECT','','',16,0,0,'SURAT',NULL,'',0,0,'','','',1,0,0,NULL),(38,12,'SUMATINATH TEX. AGENCY','606 MOMAI COMPLEX','NEAR NEW BOMBAY MARKET',16,0,0,'SURAT',NULL,'',0,0,'','','',1,0,3.75,NULL),(39,12,'A.S.CHANDRASEKARAN','','SOUTH MASI STREET',17,0,0,'MADURAI',NULL,'',0,0,'','AKPSN5268A','33AKPSN5268A1SD',2,0,3.75,NULL),(40,12,'NAGARAM CHOUDHARY','','',16,0,0,'SURAT',NULL,'',0,0,'','','',1,0,0,NULL);

/*Table structure for table `catogery_details` */

DROP TABLE IF EXISTS `catogery_details`;

CREATE TABLE `catogery_details` (
  `catogeryid` bigint(20) NOT NULL AUTO_INCREMENT,
  `catogeryname` varchar(100) NOT NULL,
  `totalPcs` bigint(20) DEFAULT '0',
  `totalMtrs` double DEFAULT '0',
  `version` timestamp NULL DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `financialYear_Id` bigint(20) DEFAULT NULL,
  `rate` double DEFAULT NULL,
  `totalValue` double DEFAULT NULL,
  PRIMARY KEY (`catogeryid`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `catogery_details` */

insert  into `catogery_details`(`catogeryid`,`catogeryname`,`totalPcs`,`totalMtrs`,`version`,`companyId`,`financialYear_Id`,`rate`,`totalValue`) values (7,'BAHUBALI',NULL,NULL,NULL,2,2,NULL,NULL);

/*Table structure for table `checker_details` */

DROP TABLE IF EXISTS `checker_details`;

CREATE TABLE `checker_details` (
  `checkertype_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `checker_name` varchar(100) DEFAULT NULL,
  `version` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`checkertype_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `checker_details` */

/*Table structure for table `city_list` */

DROP TABLE IF EXISTS `city_list`;

CREATE TABLE `city_list` (
  `city_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `vrsion` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`city_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

/*Data for the table `city_list` */

insert  into `city_list`(`city_id`,`name`,`vrsion`) values (16,'SURAT',NULL),(17,'MADURAI',NULL);

/*Table structure for table `company_details` */

DROP TABLE IF EXISTS `company_details`;

CREATE TABLE `company_details` (
  `companyId` bigint(20) NOT NULL AUTO_INCREMENT,
  `companyname` varchar(100) NOT NULL,
  `address` varchar(4000) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `pincode` bigint(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `mobile` bigint(20) DEFAULT NULL,
  `fax` bigint(20) DEFAULT NULL,
  `phoneno` bigint(20) DEFAULT NULL,
  `bankno` varchar(100) DEFAULT NULL,
  `properitor` varchar(100) DEFAULT NULL,
  `bankdetails` varchar(500) DEFAULT NULL,
  `description` varchar(4100) DEFAULT NULL,
  `panno` varchar(20) DEFAULT NULL,
  `gstno` varchar(50) DEFAULT NULL,
  `ledgertype_id` bigint(20) DEFAULT NULL,
  `version` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`companyId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `company_details` */

insert  into `company_details`(`companyId`,`companyname`,`address`,`city`,`pincode`,`email`,`mobile`,`fax`,`phoneno`,`bankno`,`properitor`,`bankdetails`,`description`,`panno`,`gstno`,`ledgertype_id`,`version`) values (1,'ABC CORP. PVT.LTD.','4011 SURAT TEXTILE MARKET;RING ROAD','SURAT',395002,'ABCCORP@GMAIL.COM',7820081393,NULL,NULL,'AC.NO. 502000136262633','KALPESH KAWAR','HDFC BANK','MFGS. OF EXCLUSIVE FANCY DESIGNER SAREES','AUIUS7056H','24AUIUS7056H1A5',NULL,NULL),(2,'MAJISA SAREES','4057 GOLWALA TEXTILE MARKET;RING ROAD','SURAT',395002,'KALPESHKAWAR@GMAIL.COM',7820081393,NULL,NULL,'AC. NO. 5020001456328 IFSC CODE : HDFC0001535','KALPESH KAWAR','HDFC BANK','MFGS. OF EXCLUSIVE FANCY SAREES','ALHUK5986Z','24ALHUK5986Z1Z5',NULL,NULL);

/*Table structure for table `creditdebitnote_details` */

DROP TABLE IF EXISTS `creditdebitnote_details`;

CREATE TABLE `creditdebitnote_details` (
  `creditdebitnote_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `party_id` bigint(20) DEFAULT NULL,
  `cdNoteType_id` bigint(20) DEFAULT NULL,
  `voucherNo` bigint(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `noteNo` varchar(100) DEFAULT NULL,
  `accountType_id` bigint(20) DEFAULT NULL,
  `refBillNo` varchar(100) DEFAULT NULL,
  `refBillDate` date DEFAULT NULL,
  `grossAmount` double DEFAULT NULL,
  `cgstPercentage` double DEFAULT NULL,
  `cgstAmount` double DEFAULT NULL,
  `sgstPercentage` double DEFAULT NULL,
  `sgstAmount` double DEFAULT NULL,
  `remark` varchar(2000) DEFAULT NULL,
  `netAmount` double DEFAULT NULL,
  `igstPercentage` double DEFAULT NULL,
  `igstAmount` double DEFAULT NULL,
  `version` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`creditdebitnote_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `creditdebitnote_details` */

/*Table structure for table `creditdebitnote_type` */

DROP TABLE IF EXISTS `creditdebitnote_type`;

CREATE TABLE `creditdebitnote_type` (
  `creditDebitNoteType_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cdNoteTypeName` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`creditDebitNoteType_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `creditdebitnote_type` */

insert  into `creditdebitnote_type`(`creditDebitNoteType_id`,`cdNoteTypeName`) values (1,'CREDIT NOTE(ON SALES)'),(2,'CREDIT NOTE(ON PURCHASE)'),(3,'DEBIT NOTE(ON SALES)'),(4,'DEBIT NOTE(ON PURCHASE)');

/*Table structure for table `emailnotification` */

DROP TABLE IF EXISTS `emailnotification`;

CREATE TABLE `emailnotification` (
  `emailnotification_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `status` varchar(50) DEFAULT NULL,
  `comment` varchar(3000) DEFAULT NULL,
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `version` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `party_name` varchar(1000) DEFAULT NULL,
  `billNo` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`emailnotification_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `emailnotification` */

/*Table structure for table `financial_year` */

DROP TABLE IF EXISTS `financial_year`;

CREATE TABLE `financial_year` (
  `company_id` bigint(20) NOT NULL,
  `financialyear_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `financial_year` varchar(100) DEFAULT NULL,
  `db_name` varchar(500) DEFAULT NULL,
  `version` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`financialyear_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `financial_year` */

insert  into `financial_year`(`company_id`,`financialyear_id`,`financial_year`,`db_name`,`version`) values (1,1,'2020-21','ABC CORP. PVT.LTD._2020-21_08112020',NULL),(2,2,'2020-21','MAJISA SAREES_2020-21_08122020',NULL);

/*Table structure for table `greypurchase_details` */

DROP TABLE IF EXISTS `greypurchase_details`;

CREATE TABLE `greypurchase_details` (
  `greyPurchaseDetails_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `greypurchase_id` bigint(20) DEFAULT NULL,
  `billNo` varchar(100) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `partyid` bigint(20) DEFAULT NULL,
  `gst_type` bigint(20) DEFAULT NULL,
  `orderNo` varchar(100) DEFAULT NULL,
  `city` bigint(20) DEFAULT NULL,
  `gstn` varchar(20) DEFAULT NULL,
  `dueDays` bigint(20) DEFAULT NULL,
  `interest` double DEFAULT NULL,
  `broker_id` bigint(20) DEFAULT NULL,
  `quality` bigint(20) DEFAULT NULL,
  `weight` double DEFAULT NULL,
  `hsnCode` bigint(20) DEFAULT NULL,
  `checker` bigint(20) DEFAULT NULL,
  `transport_id` bigint(20) DEFAULT NULL,
  `taka` bigint(20) DEFAULT NULL,
  `mtrs` double DEFAULT NULL,
  `rate` double DEFAULT NULL,
  `grossAmount` double DEFAULT NULL,
  `discountPercentage` double DEFAULT NULL,
  `discountAmount` double DEFAULT NULL,
  `sgstRate` double DEFAULT NULL,
  `cgstRate` double DEFAULT NULL,
  `igstRate` double DEFAULT NULL,
  `sgstAmount` double DEFAULT NULL,
  `cgstAmount` double DEFAULT NULL,
  `igstAmount` double DEFAULT NULL,
  `foldLess` double DEFAULT NULL,
  `addAny` double DEFAULT NULL,
  `lessAny` double DEFAULT NULL,
  `taxableValue` double DEFAULT NULL,
  `remarks` varchar(5000) DEFAULT NULL,
  `netAmount` double DEFAULT NULL,
  `version` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `greyPurchaseType_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`greyPurchaseDetails_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `greypurchase_details` */

/*Table structure for table `greypurchase_type` */

DROP TABLE IF EXISTS `greypurchase_type`;

CREATE TABLE `greypurchase_type` (
  `greyPurchaseType_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` varchar(500) DEFAULT NULL,
  `version` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`greyPurchaseType_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `greypurchase_type` */

insert  into `greypurchase_type`(`greyPurchaseType_id`,`type`,`version`) values (1,'GREY PURCHASE','2020-10-07 01:00:43'),(2,'GREY PURCHASE RETURN','2020-10-07 01:00:57');

/*Table structure for table `greyquality_details` */

DROP TABLE IF EXISTS `greyquality_details`;

CREATE TABLE `greyquality_details` (
  `greyquality_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `quality_name` varchar(100) DEFAULT NULL,
  `version` timestamp NULL DEFAULT NULL,
  `catogeryid` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`greyquality_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `greyquality_details` */

insert  into `greyquality_details`(`greyquality_id`,`quality_name`,`version`,`catogeryid`) values (1,'BAHUBALI',NULL,7);

/*Table structure for table `greytaka_details` */

DROP TABLE IF EXISTS `greytaka_details`;

CREATE TABLE `greytaka_details` (
  `takadetails_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `greyPurchaseDetails_id` bigint(20) DEFAULT NULL,
  `takaNo` bigint(20) DEFAULT NULL,
  `takaMtrs` varchar(20) DEFAULT NULL,
  `takaWeight` double DEFAULT NULL,
  `version` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`takadetails_id`),
  KEY `greypurchase_id` (`greyPurchaseDetails_id`),
  CONSTRAINT `greytaka_details_ibfk_1` FOREIGN KEY (`greyPurchaseDetails_id`) REFERENCES `greypurchase_details` (`greyPurchaseDetails_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `greytaka_details` */

/*Table structure for table `gsttype_details` */

DROP TABLE IF EXISTS `gsttype_details`;

CREATE TABLE `gsttype_details` (
  `gsttype_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` varchar(100) DEFAULT NULL,
  `version` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`gsttype_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `gsttype_details` */

insert  into `gsttype_details`(`gsttype_id`,`type`,`version`) values (1,'INTER STATE TAX INVOICE',NULL),(2,'LOCAL TAX INVOICE',NULL),(3,'COMPOSITION SCHEME',NULL),(4,'EXPORT',NULL);

/*Table structure for table `item_details` */

DROP TABLE IF EXISTS `item_details`;

CREATE TABLE `item_details` (
  `itemid` bigint(20) NOT NULL AUTO_INCREMENT,
  `itemname` varchar(100) NOT NULL,
  `catogeryid` bigint(20) NOT NULL,
  `cut` varchar(20) DEFAULT NULL,
  `packing_id` bigint(20) DEFAULT NULL,
  `unit_id` bigint(20) DEFAULT NULL,
  `greyquality_id` bigint(20) DEFAULT NULL,
  `type_id` bigint(20) DEFAULT NULL,
  `itemtype_id` bigint(20) DEFAULT NULL,
  `selling_rate` varchar(20) DEFAULT NULL,
  `hsncode` varchar(20) DEFAULT NULL,
  `gst` varchar(20) DEFAULT NULL,
  `version` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`itemid`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

/*Data for the table `item_details` */

insert  into `item_details`(`itemid`,`itemname`,`catogeryid`,`cut`,`packing_id`,`unit_id`,`greyquality_id`,`type_id`,`itemtype_id`,`selling_rate`,`hsncode`,`gst`,`version`) values (21,'BAHUBALI',7,'5.4',0,0,-1,0,0,'0','5407','5',NULL),(22,'BABY DOLL',7,'6.3',0,0,-1,0,0,'0','5407','5',NULL);

/*Table structure for table `itemtype_details` */

DROP TABLE IF EXISTS `itemtype_details`;

CREATE TABLE `itemtype_details` (
  `itemTypeId` bigint(20) NOT NULL AUTO_INCREMENT,
  `itemTypeName` varchar(500) DEFAULT NULL,
  `version` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`itemTypeId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `itemtype_details` */

insert  into `itemtype_details`(`itemTypeId`,`itemTypeName`,`version`) values (1,'SAREE','2020-09-20 05:54:09'),(2,'SUIT','2020-09-20 05:54:15'),(3,'PACKING','2020-09-20 05:54:19');

/*Table structure for table `jobprocess_details` */

DROP TABLE IF EXISTS `jobprocess_details`;

CREATE TABLE `jobprocess_details` (
  `jobprocess_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `voucherNo` bigint(20) DEFAULT NULL,
  `dispatchDate` date DEFAULT NULL,
  `party_id` bigint(20) DEFAULT NULL,
  `gstType_id` bigint(20) DEFAULT NULL,
  `challanNo` varchar(50) DEFAULT NULL,
  `broker_id` bigint(20) DEFAULT NULL,
  `workType_id` bigint(20) DEFAULT NULL,
  `transport_id` bigint(20) DEFAULT NULL,
  `lrno` varchar(1000) DEFAULT NULL,
  `billNo` varchar(1000) DEFAULT NULL,
  `remark` varchar(5000) DEFAULT NULL,
  `totalPcs` bigint(20) DEFAULT NULL,
  `totalMtrs` double DEFAULT NULL,
  `discountPercentage` double DEFAULT NULL,
  `discountPerOnAmount` double DEFAULT NULL,
  `discountAmount` double DEFAULT NULL,
  `addLessDetail1` double DEFAULT NULL,
  `addLessDetail2` double DEFAULT NULL,
  `tdsPercentage` double DEFAULT NULL,
  `tdsAmount` double DEFAULT NULL,
  `cgstPercentage` double DEFAULT NULL,
  `sgstPercentage` double DEFAULT NULL,
  `cgstAmount` double DEFAULT NULL,
  `sgstAmount` double DEFAULT NULL,
  `igstPercentage` double DEFAULT NULL,
  `igstAmount` double DEFAULT NULL,
  `caseNoDetail1` double DEFAULT NULL,
  `caseNoDetail2` double DEFAULT NULL,
  `amountOnDispatch` double DEFAULT NULL,
  `grossAmount` double DEFAULT NULL,
  `taxableValue` double DEFAULT NULL,
  `netAmount` double DEFAULT NULL,
  `version` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`jobprocess_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `jobprocess_details` */

/*Table structure for table `jobprocess_itemdetails` */

DROP TABLE IF EXISTS `jobprocess_itemdetails`;

CREATE TABLE `jobprocess_itemdetails` (
  `jobProcessItemDetails_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `challanNo` varchar(50) DEFAULT NULL,
  `dispatchReference_id` bigint(20) DEFAULT NULL,
  `receiptReference_id` bigint(20) DEFAULT NULL,
  `item_id` bigint(20) DEFAULT NULL,
  `dipatchBundles` varchar(5000) DEFAULT NULL,
  `receiptBundles` varchar(5000) DEFAULT NULL,
  `hsnCode` varchar(100) DEFAULT NULL,
  `jobTypeDetails` varchar(1000) DEFAULT NULL,
  `unit` varchar(100) DEFAULT NULL,
  `dispatch_pcs` bigint(20) DEFAULT NULL,
  `dispatch_mtrsQty` double DEFAULT NULL,
  `cut` double DEFAULT NULL,
  `rate` double DEFAULT NULL,
  `dispatchItemAmount` double DEFAULT NULL,
  `plainItem` double DEFAULT NULL,
  `secondItem` double DEFAULT NULL,
  `shortItem` double DEFAULT NULL,
  `freshItem` double DEFAULT NULL,
  `voucherNo` bigint(20) DEFAULT NULL,
  `version` timestamp NULL DEFAULT NULL,
  `jobprocess_id` bigint(20) DEFAULT NULL,
  `party_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`jobProcessItemDetails_id`),
  KEY `jobprocess_itemdetails_ibfk_1` (`jobprocess_id`),
  CONSTRAINT `jobprocess_itemdetails_ibfk_1` FOREIGN KEY (`jobprocess_id`) REFERENCES `jobprocess_details` (`jobprocess_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `jobprocess_itemdetails` */

/*Table structure for table `jobprocessreceipt_itemdetails` */

DROP TABLE IF EXISTS `jobprocessreceipt_itemdetails`;

CREATE TABLE `jobprocessreceipt_itemdetails` (
  `jobWorkReceiptId` bigint(20) NOT NULL AUTO_INCREMENT,
  `jobProcessItemDetails_id` bigint(20) DEFAULT NULL,
  `item_id` bigint(20) DEFAULT NULL,
  `receiptBundles` varchar(100) DEFAULT NULL,
  `hsnCode` varchar(100) DEFAULT NULL,
  `jobTypeDetails` varchar(1000) DEFAULT NULL,
  `unit` varchar(100) DEFAULT NULL,
  `cut` double DEFAULT NULL,
  `rate` double DEFAULT NULL,
  `pcs` double DEFAULT NULL,
  `itemAmount` double DEFAULT NULL,
  `plainItem` double DEFAULT NULL,
  `secondItem` double DEFAULT NULL,
  `shortItem` double DEFAULT NULL,
  `freshItem` double DEFAULT NULL,
  `receiveVoucherNo` bigint(20) DEFAULT NULL,
  `jobprocess_id` bigint(20) DEFAULT NULL,
  `version` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `mtrs` double DEFAULT NULL,
  `recdDate` date DEFAULT NULL,
  `challanNo` varchar(50) DEFAULT NULL,
  `remark` varchar(2000) DEFAULT NULL,
  `lrno` varchar(1000) DEFAULT NULL,
  `transport_id` bigint(20) DEFAULT NULL,
  `isDeleted` tinyint(1) DEFAULT '0',
  `jobReceiveBillDetail_id` bigint(20) DEFAULT NULL,
  `party_id` bigint(20) DEFAULT NULL,
  `multipleJobsOnItem` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`jobWorkReceiptId`),
  KEY `jobProcessItemDetails_id` (`jobProcessItemDetails_id`),
  KEY `jobReceiveBillDetail_id` (`jobReceiveBillDetail_id`),
  CONSTRAINT `jobprocessreceipt_itemdetails_ibfk_1` FOREIGN KEY (`jobProcessItemDetails_id`) REFERENCES `jobprocess_itemdetails` (`jobProcessItemDetails_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `jobprocessreceipt_itemdetails_ibfk_2` FOREIGN KEY (`jobReceiveBillDetail_id`) REFERENCES `jobreceive_billdetail` (`jobReceiveBillDetail_id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `jobprocessreceipt_itemdetails` */

/*Table structure for table `jobreceive_billdetail` */

DROP TABLE IF EXISTS `jobreceive_billdetail`;

CREATE TABLE `jobreceive_billdetail` (
  `jobReceiveBillDetail_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `billVoucherNo` bigint(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `party_id` bigint(20) DEFAULT NULL,
  `gstType_id` bigint(20) DEFAULT NULL,
  `billNo` varchar(1000) DEFAULT NULL,
  `broker_id` bigint(20) DEFAULT NULL,
  `workType_id` bigint(20) DEFAULT NULL,
  `transport_id` bigint(20) DEFAULT NULL,
  `lrno` varchar(1000) DEFAULT NULL,
  `discountPercentage` double DEFAULT NULL,
  `discountOnAmount` double DEFAULT NULL,
  `discountAmount` double DEFAULT NULL,
  `addLessDetail1` double DEFAULT NULL,
  `addLessDetail2` double DEFAULT NULL,
  `tdsPercentage` double DEFAULT NULL,
  `tdsAmount` double DEFAULT NULL,
  `cgstPercentage` double DEFAULT NULL,
  `cgstAmount` double DEFAULT NULL,
  `sgstPercentage` double DEFAULT NULL,
  `sgstAmount` double DEFAULT NULL,
  `igstPercentage` double DEFAULT NULL,
  `igstAmount` double DEFAULT NULL,
  `caseNoDetail1` varchar(1000) DEFAULT NULL,
  `caseNoDetail2` varchar(1000) DEFAULT NULL,
  `grossAmount` double DEFAULT NULL,
  `taxableValue` double DEFAULT NULL,
  `netAmount` double DEFAULT NULL,
  `totalPcs` double DEFAULT NULL,
  `totalMtrs` double DEFAULT NULL,
  `remark` varchar(5000) DEFAULT NULL,
  `version` timestamp NULL DEFAULT NULL,
  `addLessDetail3` double DEFAULT NULL,
  PRIMARY KEY (`jobReceiveBillDetail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `jobreceive_billdetail` */

/*Table structure for table `journalentry_detail` */

DROP TABLE IF EXISTS `journalentry_detail`;

CREATE TABLE `journalentry_detail` (
  `journalEntryDetail_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `voucherNo` bigint(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `creditAccount_id` bigint(20) DEFAULT NULL,
  `debitAccount_id` bigint(20) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `chqRefNo` varchar(100) DEFAULT NULL,
  `creditRemarks` varchar(5000) DEFAULT NULL,
  `debitRemarks` varchar(5000) DEFAULT NULL,
  `version` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`journalEntryDetail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `journalentry_detail` */

/*Table structure for table `ledger_type` */

DROP TABLE IF EXISTS `ledger_type`;

CREATE TABLE `ledger_type` (
  `ledger_typeid` bigint(20) NOT NULL,
  `type` varchar(50) DEFAULT NULL,
  `version` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ledger_typeid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `ledger_type` */

insert  into `ledger_type`(`ledger_typeid`,`type`,`version`) values (1,'INDIVIDUAL',NULL),(2,'PROPRIETORSHIP',NULL),(3,'PARTNERSHIP',NULL),(4,'PVT.LTD.COMPANY',NULL),(5,'PUBLIC LTD. COMPANY',NULL),(6,'HUF',NULL);

/*Table structure for table `millchallandispatchtaka_detail` */

DROP TABLE IF EXISTS `millchallandispatchtaka_detail`;

CREATE TABLE `millchallandispatchtaka_detail` (
  `marka` varchar(500) DEFAULT NULL,
  `lotNo` varchar(500) DEFAULT NULL,
  `millTaka_id` bigint(50) NOT NULL AUTO_INCREMENT,
  `greyMtrs` varchar(30) DEFAULT NULL,
  `version` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `finMtrs` varchar(30) DEFAULT NULL,
  `isReceived` tinyint(1) DEFAULT NULL,
  `challan_id` bigint(20) DEFAULT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `rate` double DEFAULT NULL,
  `millvoucher_id` bigint(20) DEFAULT NULL,
  `jobAmount` double DEFAULT NULL,
  `reProcessTaka` bigint(1) DEFAULT '0',
  PRIMARY KEY (`millTaka_id`),
  KEY `challan_id` (`challan_id`),
  KEY `millvoucher_id` (`millvoucher_id`),
  CONSTRAINT `millchallandispatchtaka_detail_ibfk_1` FOREIGN KEY (`challan_id`) REFERENCES `milldispatchchallan_detail` (`challan_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `millchallandispatchtaka_detail_ibfk_2` FOREIGN KEY (`millvoucher_id`) REFERENCES `millreceipt_detail` (`millvoucher_id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `millchallandispatchtaka_detail` */

/*Table structure for table `milldispatch_detail` */

DROP TABLE IF EXISTS `milldispatch_detail`;

CREATE TABLE `milldispatch_detail` (
  `voucherNo` bigint(20) NOT NULL,
  `date` date DEFAULT NULL,
  `greypurchase_id` bigint(20) DEFAULT NULL,
  `totalTaka` bigint(20) DEFAULT NULL,
  `totalMtrs` double DEFAULT NULL,
  `version` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`voucherNo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `milldispatch_detail` */

/*Table structure for table `milldispatchchallan_detail` */

DROP TABLE IF EXISTS `milldispatchchallan_detail`;

CREATE TABLE `milldispatchchallan_detail` (
  `challanNo` varchar(50) NOT NULL,
  `voucherNo` bigint(20) DEFAULT NULL,
  `mill_id` bigint(20) DEFAULT NULL,
  `station_id` bigint(20) DEFAULT NULL,
  `mill_gstn` varchar(50) DEFAULT NULL,
  `processType` bigint(20) DEFAULT NULL,
  `totalTakas` bigint(20) DEFAULT NULL,
  `totalMtrs` double DEFAULT NULL,
  `master` varchar(100) DEFAULT NULL,
  `rate` double DEFAULT NULL,
  `cardNo` varchar(50) DEFAULT NULL,
  `version` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `remark` varchar(2000) DEFAULT NULL,
  `quality_id` bigint(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `reference_billNo` varchar(20) DEFAULT NULL,
  `reference_challanNo` varchar(50) DEFAULT NULL,
  `challan_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `receiptVoucherNo` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`challan_id`),
  KEY `voucherNo` (`voucherNo`),
  CONSTRAINT `milldispatchchallan_detail_ibfk_1` FOREIGN KEY (`voucherNo`) REFERENCES `milldispatch_detail` (`voucherNo`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `milldispatchchallan_detail` */

/*Table structure for table `millprocess_type` */

DROP TABLE IF EXISTS `millprocess_type`;

CREATE TABLE `millprocess_type` (
  `millProcess_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` varchar(100) DEFAULT NULL,
  `version` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`millProcess_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `millprocess_type` */

insert  into `millprocess_type`(`millProcess_id`,`type`,`version`) values (1,'PROCESS(GREY DISPATCH)','2020-08-16 08:03:50'),(2,'REPROCESS(BLEACH/SEMI FINISH)','2020-08-16 08:04:38'),(3,'R/F(FINISH GOODS)','2020-08-16 08:04:59');

/*Table structure for table `millreceipt_detail` */

DROP TABLE IF EXISTS `millreceipt_detail`;

CREATE TABLE `millreceipt_detail` (
  `millvoucher_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `receiptVoucherNo` bigint(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `mill_id` bigint(20) DEFAULT NULL,
  `hsnCode` varchar(100) DEFAULT NULL,
  `remark` varchar(5000) DEFAULT NULL,
  `recdTaka` bigint(20) DEFAULT NULL,
  `recdMtrs` double DEFAULT NULL,
  `greyMtrs` double DEFAULT NULL,
  `disountPercentage` double DEFAULT NULL,
  `discountAmount` double DEFAULT NULL,
  `rdPerMtr` double DEFAULT NULL,
  `rdAmount` double DEFAULT NULL,
  `tdsPercentage` double DEFAULT NULL,
  `tdsAmount` double DEFAULT NULL,
  `cgstPercentage` double DEFAULT NULL,
  `sgstPercentage` double DEFAULT NULL,
  `igstPercentage` double DEFAULT NULL,
  `cgstAmount` double DEFAULT NULL,
  `sgstAmount` double DEFAULT NULL,
  `igstAmount` double DEFAULT NULL,
  `grossAmount` double DEFAULT NULL,
  `addLessAny` double DEFAULT NULL,
  `taxableValue` double DEFAULT NULL,
  `invoiceValue` double DEFAULT NULL,
  `netAmountAfterTds` double DEFAULT NULL,
  `billNo` varchar(100) DEFAULT NULL,
  `version` timestamp NULL DEFAULT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `challanNo` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`millvoucher_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `millreceipt_detail` */

/*Table structure for table `openingbalance_details` */

DROP TABLE IF EXISTS `openingbalance_details`;

CREATE TABLE `openingbalance_details` (
  `openingBalance_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `companyId` bigint(20) DEFAULT NULL,
  `financialyear_id` bigint(20) DEFAULT NULL,
  `party_id` bigint(20) DEFAULT NULL,
  `openingBalance` double DEFAULT NULL,
  `version` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `remark` varchar(5000) DEFAULT NULL,
  PRIMARY KEY (`openingBalance_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `openingbalance_details` */

insert  into `openingbalance_details`(`openingBalance_id`,`companyId`,`financialyear_id`,`party_id`,`openingBalance`,`version`,`remark`) values (1,2,2,47,525000,'2020-09-27 12:16:13','OPENING BALANCE');

/*Table structure for table `openingstock_details` */

DROP TABLE IF EXISTS `openingstock_details`;

CREATE TABLE `openingstock_details` (
  `openingStock_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `companyId` bigint(20) DEFAULT NULL,
  `financialyear_id` bigint(20) DEFAULT NULL,
  `totalPcs` double DEFAULT NULL,
  `totalMtrs` double DEFAULT NULL,
  `totalValue` double DEFAULT NULL,
  `version` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`openingStock_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `openingstock_details` */

insert  into `openingstock_details`(`openingStock_id`,`companyId`,`financialyear_id`,`totalPcs`,`totalMtrs`,`totalValue`,`version`) values (2,2,2,0,0,0,'2020-09-23 12:05:52');

/*Table structure for table `outstanding_details` */

DROP TABLE IF EXISTS `outstanding_details`;

CREATE TABLE `outstanding_details` (
  `outsatndingDetails_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `companyId` bigint(20) DEFAULT NULL,
  `partyid` bigint(20) DEFAULT NULL,
  `outstandingAmount` double DEFAULT '0',
  PRIMARY KEY (`outsatndingDetails_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `outstanding_details` */

/*Table structure for table `party_details` */

DROP TABLE IF EXISTS `party_details`;

CREATE TABLE `party_details` (
  `partyid` bigint(20) NOT NULL AUTO_INCREMENT,
  `account_typeid` bigint(20) NOT NULL,
  `party_name` varchar(100) NOT NULL,
  `address` varchar(4000) DEFAULT NULL,
  `address2` varchar(4000) DEFAULT NULL,
  `city_id` bigint(20) DEFAULT NULL,
  `pincode` bigint(20) DEFAULT NULL,
  `distance` varchar(500) DEFAULT NULL,
  `brokerid` bigint(20) DEFAULT NULL,
  `station_name` varchar(100) DEFAULT NULL,
  `transportid` bigint(20) DEFAULT NULL,
  `contactperson` varchar(500) DEFAULT NULL,
  `phone1` bigint(20) DEFAULT NULL,
  `phone2` bigint(20) DEFAULT NULL,
  `emailid` varchar(100) DEFAULT NULL,
  `panno` varchar(20) DEFAULT NULL,
  `gst` varchar(50) DEFAULT NULL,
  `ledger_typeid` bigint(20) DEFAULT NULL,
  `tds` double DEFAULT NULL,
  `version` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`partyid`),
  KEY `transportid` (`transportid`),
  KEY `city_id` (`city_id`),
  KEY `brokerid` (`brokerid`),
  CONSTRAINT `party_details_ibfk_1` FOREIGN KEY (`transportid`) REFERENCES `transport_details` (`transportid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `party_details_ibfk_2` FOREIGN KEY (`city_id`) REFERENCES `city_list` (`city_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `party_details_ibfk_3` FOREIGN KEY (`brokerid`) REFERENCES `broker_details` (`brokerid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=latin1;

/*Data for the table `party_details` */

insert  into `party_details`(`partyid`,`account_typeid`,`party_name`,`address`,`address2`,`city_id`,`pincode`,`distance`,`brokerid`,`station_name`,`transportid`,`contactperson`,`phone1`,`phone2`,`emailid`,`panno`,`gst`,`ledger_typeid`,`tds`,`version`) values (41,1,'MANJUSHREE SYNTEX','4057 GOLWALA TEXTILE MARKET','RING ROAD',16,395002,'',37,'SURAT',7,'',0,0,'','ALBPK8557A','24ALBPK8557A1ZD',2,1,NULL),(42,1,'KHUSHI FASHION','4057 GOLWALA TEXTILE MARKET','RING ROAD',16,395002,'',37,'SURAT',7,'',0,0,'','AKPPK9895C','24AKPPK895C1ZM',2,0,NULL),(43,1,'RAMDEV TRADERS & CO.','EAST PERUMAL STREET','',17,0,'',38,'SURAT',7,'',0,0,'','AKPIP2584A','33AKPIP2584A1ZD',2,0,NULL),(44,1,'KRISHNA FABRICS','EAST PERUMAL STREET','',17,0,'',39,'MADURAI',8,'',0,0,'','ASGUH5945A','33ASGUH5945A1ZD',2,3.75,NULL),(47,6,'HDFC BANK RING ROAD','','RING ROAD',16,0,'',37,'SURAT',7,'',0,0,'','','',1,0,NULL),(48,2,'ARHAM FABRICS','SOMESHWAR TEXTILE MARKET','RING ROAD',16,395002,'',40,'SURAT',7,'',0,0,'','ASDEW5862A','24ASDEW5862A1SD',2,0,NULL),(49,2,'JAI RAMAPIR TEXTILES','','SURAT',16,0,'',37,'SURAT',7,'',0,0,'','ASDFW5268A','24ASDFW5268A1ZD',2,0,NULL),(50,10,'PASHUPATI D&P MILLS PVT. LTD.','','PANDESARA',16,0,'',37,'SURAT',7,'',0,0,'','ASZAA4521S','24ASZAA4521S1ZD',4,1.5,NULL),(51,40,'SHAILESH PUROHIT','','',16,0,'',37,'SURAT',7,'',0,0,'','','',1,0,NULL),(78,34,'SALARY A/C','','',16,0,'',37,'SURAT',7,'',0,0,'','','',1,0,NULL),(79,42,'DISCOUNT A/C','','',16,0,'',37,'SURAT',7,'',0,0,'','','',1,0,NULL);

/*Table structure for table `passbook_details` */

DROP TABLE IF EXISTS `passbook_details`;

CREATE TABLE `passbook_details` (
  `passBookDetails_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `voucherNo` bigint(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `bankCashAccount_id` bigint(20) DEFAULT NULL,
  `chqNo` varchar(500) DEFAULT NULL,
  `chqDate` date DEFAULT NULL,
  `draweeBank_id` bigint(20) DEFAULT NULL,
  `remark` varchar(5000) DEFAULT NULL,
  `party_id` bigint(20) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `accountType_id` bigint(20) DEFAULT NULL,
  `passbooktype_id` bigint(20) DEFAULT NULL,
  `paymentMode` bigint(20) DEFAULT '0',
  `version` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`passBookDetails_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `passbook_details` */

/*Table structure for table `passbook_itemdetails` */

DROP TABLE IF EXISTS `passbook_itemdetails`;

CREATE TABLE `passbook_itemdetails` (
  `passbooItemDetails_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `passBookDetails_id` bigint(20) DEFAULT NULL,
  `tdsAmount` double DEFAULT NULL,
  `days` double DEFAULT NULL,
  `rdAmount` double DEFAULT NULL,
  `discountPercentage` double DEFAULT NULL,
  `discountAmount` double DEFAULT NULL,
  `commPercentage` double DEFAULT NULL,
  `commAmount` double DEFAULT NULL,
  `rgAmount` double DEFAULT NULL,
  `addLess` double DEFAULT NULL,
  `others` double DEFAULT NULL,
  `paymentReceivedAmount` double DEFAULT NULL,
  `pendingAmount` double DEFAULT NULL,
  `billPaid` tinyint(1) DEFAULT '0',
  `version` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `billNo` varchar(100) DEFAULT NULL,
  `totalReceivedAmount` double DEFAULT NULL,
  PRIMARY KEY (`passbooItemDetails_id`),
  KEY `passbook_itemdetails_ibfk_1` (`passBookDetails_id`),
  CONSTRAINT `passbook_itemdetails_ibfk_1` FOREIGN KEY (`passBookDetails_id`) REFERENCES `passbook_details` (`passBookDetails_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `passbook_itemdetails` */

/*Table structure for table `passbooktype_details` */

DROP TABLE IF EXISTS `passbooktype_details`;

CREATE TABLE `passbooktype_details` (
  `passbooktype_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`passbooktype_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `passbooktype_details` */

insert  into `passbooktype_details`(`passbooktype_id`,`type`) values (1,'BANK RECEIPT'),(2,'BANK PAYMENT'),(3,'CASH RECEIPT'),(4,'CASH PAYMENT');

/*Table structure for table `printersettings_details` */

DROP TABLE IF EXISTS `printersettings_details`;

CREATE TABLE `printersettings_details` (
  `printerSettingsDetails_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` varchar(1000) DEFAULT NULL,
  `value` varchar(5000) DEFAULT NULL,
  `version` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`printerSettingsDetails_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `printersettings_details` */

/*Table structure for table `purchase_details` */

DROP TABLE IF EXISTS `purchase_details`;

CREATE TABLE `purchase_details` (
  `purchasebillid` bigint(20) NOT NULL AUTO_INCREMENT,
  `purchase_typeid` bigint(20) DEFAULT NULL,
  `billno` varchar(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `partyid` bigint(20) DEFAULT NULL,
  `stationcode` bigint(20) DEFAULT NULL,
  `purchase_gsttypeid` bigint(20) DEFAULT NULL,
  `partygst` varchar(50) DEFAULT NULL,
  `brokerid` bigint(20) DEFAULT NULL,
  `transportid` bigint(20) DEFAULT NULL,
  `lrno` varchar(100) DEFAULT NULL,
  `remark` varchar(5000) DEFAULT NULL,
  `discount` varchar(20) DEFAULT '0',
  `additional` varchar(20) DEFAULT '0',
  `caseno` varchar(100) DEFAULT NULL,
  `cgst` double DEFAULT '0',
  `sgst` double DEFAULT '0',
  `igst` double DEFAULT '0',
  `taxablevalue` double DEFAULT NULL,
  `billamount` double DEFAULT NULL,
  `gsttypeid` bigint(20) DEFAULT NULL,
  `version` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `discountPercentage` double DEFAULT NULL,
  `discountOnAmount` double DEFAULT NULL,
  `addLessDetail1` double DEFAULT NULL,
  `addLessDetail2` double DEFAULT NULL,
  `cgstRate` double DEFAULT NULL,
  `sgstRate` double DEFAULT NULL,
  `igstRate` double DEFAULT NULL,
  `grossAmount` double DEFAULT NULL,
  `caseNoDetail2` varchar(50) DEFAULT NULL,
  `orderno` varchar(20) DEFAULT NULL,
  `duedays` bigint(20) DEFAULT NULL,
  `station` bigint(20) DEFAULT NULL,
  `voucherNo` bigint(20) DEFAULT NULL,
  `refBillNo` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`purchasebillid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `purchase_details` */

/*Table structure for table `purchaseitem_details` */

DROP TABLE IF EXISTS `purchaseitem_details`;

CREATE TABLE `purchaseitem_details` (
  `item_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `purchasebillid` bigint(20) DEFAULT NULL,
  `bundles` varchar(100) DEFAULT NULL,
  `hsncode` varchar(100) DEFAULT NULL,
  `unit` varchar(20) DEFAULT NULL,
  `cut` double DEFAULT NULL,
  `quantity` double DEFAULT NULL,
  `rate` double DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `purchaseitem_id` bigint(20) DEFAULT NULL,
  `version` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `pcs` bigint(20) DEFAULT NULL,
  `packing` varchar(10) DEFAULT NULL,
  `cgstRate` double DEFAULT '0',
  `cgstAmount` double DEFAULT '0',
  `sgstRate` double DEFAULT '0',
  `sgstAmount` double DEFAULT '0',
  `igstRate` double DEFAULT '0',
  `igstAmount` double DEFAULT '0',
  `rdAmount` double DEFAULT '0',
  `discount` double DEFAULT '0',
  `taxableValue` double DEFAULT '0',
  `totalAmount` double DEFAULT '0',
  `addLess` double DEFAULT '0',
  PRIMARY KEY (`item_id`),
  KEY `purchasebillid` (`purchasebillid`),
  CONSTRAINT `purchaseitem_details_ibfk_1` FOREIGN KEY (`purchasebillid`) REFERENCES `purchase_details` (`purchasebillid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `purchaseitem_details` */

/*Table structure for table `purchasetype_details` */

DROP TABLE IF EXISTS `purchasetype_details`;

CREATE TABLE `purchasetype_details` (
  `purchasetype_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` varchar(100) DEFAULT NULL,
  `version` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`purchasetype_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `purchasetype_details` */

insert  into `purchasetype_details`(`purchasetype_id`,`type`,`version`) values (1,'Finish Purchase',NULL),(2,'Packing Material Purchase',NULL),(3,'GST Purchase Goods',NULL),(4,'GST General Goods',NULL),(5,'GST Input Services',NULL),(6,'Finish Purchase Return',NULL),(7,'Packing Material Return',NULL);

/*Table structure for table `sales_details` */

DROP TABLE IF EXISTS `sales_details`;

CREATE TABLE `sales_details` (
  `caseNoDetail2` varchar(50) DEFAULT NULL,
  `grossAmount` double DEFAULT NULL,
  `igstRate` double DEFAULT NULL,
  `sgstRate` double DEFAULT NULL,
  `cgstRate` double DEFAULT NULL,
  `addLessDetail2` double DEFAULT NULL,
  `addLessDetail1` double DEFAULT NULL,
  `discountOnAmount` double DEFAULT NULL,
  `discountPercentage` double DEFAULT NULL,
  `partyid` bigint(20) NOT NULL,
  `salesTypeid` bigint(20) NOT NULL,
  `billno` varchar(20) DEFAULT NULL,
  `orderno` varchar(100) DEFAULT NULL,
  `statecode` bigint(20) DEFAULT NULL,
  `gstTypeid` bigint(20) DEFAULT NULL,
  `station` bigint(20) DEFAULT NULL,
  `partygst` varchar(50) DEFAULT NULL,
  `brokerid` bigint(20) DEFAULT NULL,
  `transportid` bigint(100) DEFAULT NULL,
  `lrno` varchar(100) DEFAULT NULL,
  `remark` varchar(4000) DEFAULT NULL,
  `discount` varchar(20) DEFAULT NULL,
  `additional` varchar(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `caseno` varchar(20) DEFAULT NULL,
  `cgst` double DEFAULT NULL,
  `sgst` double DEFAULT NULL,
  `igst` double DEFAULT NULL,
  `taxablevalue` double NOT NULL,
  `billamount` double NOT NULL,
  `salesbillid` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` timestamp NULL DEFAULT NULL,
  `duedays` bigint(20) DEFAULT NULL,
  `voucherNo` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`salesbillid`),
  KEY `brokerid` (`brokerid`),
  KEY `transportid` (`transportid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sales_details` */

/*Table structure for table `sales_row_details` */

DROP TABLE IF EXISTS `sales_row_details`;

CREATE TABLE `sales_row_details` (
  `salesrowid` bigint(20) NOT NULL AUTO_INCREMENT,
  `itemid` bigint(20) DEFAULT NULL,
  `bundles` varchar(100) DEFAULT NULL,
  `hsncode` varchar(100) DEFAULT NULL,
  `packing` varchar(100) DEFAULT NULL,
  `unit` varchar(100) DEFAULT NULL,
  `pcs` bigint(20) DEFAULT NULL,
  `cut` varchar(100) DEFAULT NULL,
  `quantity` double DEFAULT NULL,
  `rate` double DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `version` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`salesrowid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sales_row_details` */

/*Table structure for table `sales_row_mapping` */

DROP TABLE IF EXISTS `sales_row_mapping`;

CREATE TABLE `sales_row_mapping` (
  `salesrowid` bigint(20) NOT NULL,
  `salesid` bigint(20) NOT NULL,
  `mappingid` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`mappingid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sales_row_mapping` */

/*Table structure for table `sales_type` */

DROP TABLE IF EXISTS `sales_type`;

CREATE TABLE `sales_type` (
  `salesTypeid` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) DEFAULT NULL,
  `version` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`salesTypeid`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `sales_type` */

insert  into `sales_type`(`salesTypeid`,`type`,`version`) values (1,'Finish Sales',NULL),(2,'Grey Sales',NULL),(3,'Packing Material',NULL),(4,'Fent Sales',NULL),(5,'Finish Sales Return',NULL),(6,'Grey Sales Return',NULL),(7,'Packing Material Return',NULL),(8,'Fent Sales Return',NULL);

/*Table structure for table `salesitem_details` */

DROP TABLE IF EXISTS `salesitem_details`;

CREATE TABLE `salesitem_details` (
  `item_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `salesbillid` bigint(20) DEFAULT NULL,
  `bundles` varchar(100) DEFAULT NULL,
  `hsncode` varchar(100) DEFAULT NULL,
  `packing` varchar(10) DEFAULT NULL,
  `unit` varchar(50) DEFAULT NULL,
  `cut` double DEFAULT NULL,
  `pcs` bigint(20) DEFAULT NULL,
  `quantity` double DEFAULT NULL,
  `rate` double DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `version` timestamp NULL DEFAULT NULL,
  `salesitem_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`item_id`),
  KEY `salesbillid` (`salesbillid`),
  CONSTRAINT `salesitem_details_ibfk_1` FOREIGN KEY (`salesbillid`) REFERENCES `sales_details` (`salesbillid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `salesitem_details` */

/*Table structure for table `statecode_details` */

DROP TABLE IF EXISTS `statecode_details`;

CREATE TABLE `statecode_details` (
  `statecode_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `code` varchar(20) DEFAULT NULL,
  `version` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`statecode_id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;

/*Data for the table `statecode_details` */

insert  into `statecode_details`(`statecode_id`,`name`,`code`,`version`) values (1,'JAMMU & KASHMIR','01',NULL),(2,'HIMACHAL PRADESH','02',NULL),(3,'PUNJAB','03',NULL),(4,'CHANDIGARH','04',NULL),(5,'UTTARAKHAND','05',NULL),(6,'HARYANA','06',NULL),(7,'DELHI','07',NULL),(8,'RAJASTHAN','08',NULL),(9,'UTTAR PRADESH','09',NULL),(10,'BIHAR','10',NULL),(11,'SIKKIM','11',NULL),(12,'ARUNACHAL PRADESH','12',NULL),(13,'NAGALAND','13',NULL),(14,'MANIPUR','14',NULL),(15,'MIZORAM','15',NULL),(16,'TRIPURA','16',NULL),(17,'MEGHALAYA','17',NULL),(18,'ASSAM','18',NULL),(19,'WEST BENGAL','19',NULL),(20,'JHARKHAND','20',NULL),(21,'ORISSA','21',NULL),(22,'CHATTISHGARH','22',NULL),(23,'MADHYA PRADESH','23',NULL),(24,'GUJARAT','24',NULL),(25,'DAMAN & DIU','25',NULL),(26,'DADRA & NAGAR HAVELI','26',NULL),(27,'MAHARASHTRA','27',NULL),(28,'KARNATAKA','29',NULL),(29,'GOA','30',NULL),(30,'LAKSHADEEP','31',NULL),(31,'KERALA','32',NULL),(32,'TAMIL NADU','33',NULL),(33,'PONDICHERRY','34',NULL),(34,'ANDAMAN & NICOBAR','35',NULL),(35,'TELANGANA','36',NULL),(36,'ANDHRA PRADESH','37',NULL),(37,'OTHER TERRITORIES','97',NULL),(38,'OTHER COUNTRY','99',NULL);

/*Table structure for table `transport_details` */

DROP TABLE IF EXISTS `transport_details`;

CREATE TABLE `transport_details` (
  `transportid` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `gstn` varchar(100) DEFAULT NULL,
  `version` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`transportid`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `transport_details` */

insert  into `transport_details`(`transportid`,`name`,`gstn`,`version`) values (7,'LOCAL DELIVERY',NULL,NULL),(8,'R.K.ROADWAYS',NULL,NULL);

/*Table structure for table `unit_details` */

DROP TABLE IF EXISTS `unit_details`;

CREATE TABLE `unit_details` (
  `unit_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`unit_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `unit_details` */

insert  into `unit_details`(`unit_id`,`type`) values (1,'PCS'),(2,'MTRS');

/*Table structure for table `userdetails` */

DROP TABLE IF EXISTS `userdetails`;

CREATE TABLE `userdetails` (
  `username` varchar(100) NOT NULL,
  `password` varchar(100) DEFAULT NULL,
  `usergroup` varchar(100) DEFAULT NULL,
  `version` timestamp NULL DEFAULT NULL,
  `isAdmin` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `userdetails` */

insert  into `userdetails`(`username`,`password`,`usergroup`,`version`,`isAdmin`) values ('virat','virat',NULL,NULL,0);

/*Table structure for table `worktype_details` */

DROP TABLE IF EXISTS `worktype_details`;

CREATE TABLE `worktype_details` (
  `worktype_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `worktype` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`worktype_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

/*Data for the table `worktype_details` */

insert  into `worktype_details`(`worktype_id`,`worktype`) values (1,'EMB WORK'),(2,'HAND /ADD WORK'),(3,'R.F.'),(4,'LACE STITCH'),(5,'POLISH/CHARAK'),(6,'OTHER'),(7,'SMOKE'),(8,'HAND DYEING'),(9,'LASER CUTING'),(10,'STITCHING'),(11,'WASHING'),(12,'STONE'),(13,'DEKA'),(14,'FOIL'),(15,'OTHERS');

/*Table structure for table `worktypegst_details` */

DROP TABLE IF EXISTS `worktypegst_details`;

CREATE TABLE `worktypegst_details` (
  `worktypegst_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `gstType` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`worktypegst_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `worktypegst_details` */

insert  into `worktypegst_details`(`worktypegst_id`,`gstType`) values (1,'LOCAL TAX INVOICE'),(2,'INTER STATE TAX INVOICE'),(3,'COMPOSITION SCHEME'),(4,'EXPORTS'),(5,'NIL RATED'),(6,'EXEMPTED'),(7,'NON GST'),(8,'IMPORTED');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
