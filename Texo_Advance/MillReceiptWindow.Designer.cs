﻿namespace Texo_Advance
{
    partial class MillReceiptWindow
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label2 = new System.Windows.Forms.Label();
            this.voucherNoTextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.dateTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.partyNameComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.gstnTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.jobTypeComboBox = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.billNoTextBox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.totalRecTakaTextBox = new System.Windows.Forms.TextBox();
            this.millReceiptDataGridView = new System.Windows.Forms.DataGridView();
            this.netAmtAftTdsTextBox = new System.Windows.Forms.TextBox();
            this.taxValTextBox = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.detailsGroupBox = new System.Windows.Forms.GroupBox();
            this.igstAmtTextBox = new System.Windows.Forms.TextBox();
            this.sgstAmtTextBox = new System.Windows.Forms.TextBox();
            this.cgstAmtTextBox = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.igstRateTextBox = new System.Windows.Forms.TextBox();
            this.sgstRateTextBox = new System.Windows.Forms.TextBox();
            this.cgstRateTextBox = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.tdsAmtTextBox2 = new System.Windows.Forms.TextBox();
            this.tdsPerTextBox = new System.Windows.Forms.TextBox();
            this.rdAmtFinalTextBox = new System.Windows.Forms.TextBox();
            this.reMtrTextBox = new System.Windows.Forms.TextBox();
            this.discPerAmtFinalTextBox = new System.Windows.Forms.TextBox();
            this.discPerTextBox = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.totalRecMtrsTextBox = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.totalGreyMtrsTextBox = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.grossAmountTextBox = new System.Windows.Forms.TextBox();
            this.addLessTextBox = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.invValueTextBox = new System.Windows.Forms.TextBox();
            this.companyNameTextBox = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.remarkTextBox = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.hsnCodeTextBox = new System.Windows.Forms.TextBox();
            this.createButton = new System.Windows.Forms.Button();
            this.viewButton = new System.Windows.Forms.Button();
            this.updateButton = new System.Windows.Forms.Button();
            this.deleteButton = new System.Windows.Forms.Button();
            this.dataGridViewComboBoxColumn1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.challanNoComboBox = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.lotNoTextBox = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.recdCardNoTextBox = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reprocessTakaCheckBox = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.markaTextBox = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.greyQualityComboBox = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.takaTextBox = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.recMtrsTextBox = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.greyMtrsTextBox = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.jobRateTextBox = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.jobAmountTextBox = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.millReceiptDataGridView)).BeginInit();
            this.detailsGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(10, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(142, 18);
            this.label2.TabIndex = 3;
            this.label2.Text = "COMPANY NAME";
            // 
            // voucherNoTextBox
            // 
            this.voucherNoTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.voucherNoTextBox.Location = new System.Drawing.Point(624, 42);
            this.voucherNoTextBox.Name = "voucherNoTextBox";
            this.voucherNoTextBox.Size = new System.Drawing.Size(83, 22);
            this.voucherNoTextBox.TabIndex = 0;
            this.voucherNoTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.voucherNoTextBox_KeyDown);
            this.voucherNoTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.voucherNoTextBox_Validating);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(476, 43);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(137, 20);
            this.label7.TabIndex = 10;
            this.label7.Text = "VOUCHER NO.:";
            // 
            // dateTextBox
            // 
            this.dateTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTextBox.Location = new System.Drawing.Point(796, 42);
            this.dateTextBox.Name = "dateTextBox";
            this.dateTextBox.Size = new System.Drawing.Size(107, 22);
            this.dateTextBox.TabIndex = 1;
            this.dateTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.dateTextBox_Validating);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(724, 42);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 20);
            this.label4.TabIndex = 8;
            this.label4.Text = "DATE";
            // 
            // partyNameComboBox
            // 
            this.partyNameComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.partyNameComboBox.FormattingEnabled = true;
            this.partyNameComboBox.Location = new System.Drawing.Point(154, 84);
            this.partyNameComboBox.Name = "partyNameComboBox";
            this.partyNameComboBox.Size = new System.Drawing.Size(282, 24);
            this.partyNameComboBox.TabIndex = 2;
            this.partyNameComboBox.SelectedIndexChanged += new System.EventHandler(this.partyNameComboBox_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 85);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 18);
            this.label1.TabIndex = 12;
            this.label1.Text = "MILL NAME";
            // 
            // gstnTextBox
            // 
            this.gstnTextBox.BackColor = System.Drawing.SystemColors.Info;
            this.gstnTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gstnTextBox.ForeColor = System.Drawing.Color.Red;
            this.gstnTextBox.Location = new System.Drawing.Point(74, 124);
            this.gstnTextBox.Name = "gstnTextBox";
            this.gstnTextBox.ReadOnly = true;
            this.gstnTextBox.Size = new System.Drawing.Size(237, 24);
            this.gstnTextBox.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(13, 125);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 18);
            this.label3.TabIndex = 14;
            this.label3.Text = "GSTN:";
            // 
            // jobTypeComboBox
            // 
            this.jobTypeComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.jobTypeComboBox.FormattingEnabled = true;
            this.jobTypeComboBox.Location = new System.Drawing.Point(728, 80);
            this.jobTypeComboBox.Name = "jobTypeComboBox";
            this.jobTypeComboBox.Size = new System.Drawing.Size(175, 24);
            this.jobTypeComboBox.TabIndex = 4;
            this.jobTypeComboBox.Text = "JOB WORK";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(659, 81);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 20);
            this.label5.TabIndex = 16;
            this.label5.Text = "TYPE";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(459, 84);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(87, 20);
            this.label6.TabIndex = 10;
            this.label6.Text = "BILL NO.:";
            // 
            // billNoTextBox
            // 
            this.billNoTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.billNoTextBox.Location = new System.Drawing.Point(559, 83);
            this.billNoTextBox.Name = "billNoTextBox";
            this.billNoTextBox.Size = new System.Drawing.Size(83, 22);
            this.billNoTextBox.TabIndex = 3;
            this.billNoTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.billNoTextBox_Validating_1);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(20, 407);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(101, 20);
            this.label8.TabIndex = 10;
            this.label8.Text = "REC TAKA:";
            // 
            // totalRecTakaTextBox
            // 
            this.totalRecTakaTextBox.BackColor = System.Drawing.SystemColors.Info;
            this.totalRecTakaTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalRecTakaTextBox.Location = new System.Drawing.Point(131, 406);
            this.totalRecTakaTextBox.Name = "totalRecTakaTextBox";
            this.totalRecTakaTextBox.ReadOnly = true;
            this.totalRecTakaTextBox.Size = new System.Drawing.Size(65, 24);
            this.totalRecTakaTextBox.TabIndex = 11;
            this.totalRecTakaTextBox.TabStop = false;
            this.totalRecTakaTextBox.Text = "0";
            // 
            // millReceiptDataGridView
            // 
            this.millReceiptDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.millReceiptDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.challanNoComboBox,
            this.lotNoTextBox,
            this.recdCardNoTextBox,
            this.reprocessTakaCheckBox,
            this.markaTextBox,
            this.greyQualityComboBox,
            this.takaTextBox,
            this.recMtrsTextBox,
            this.greyMtrsTextBox,
            this.jobRateTextBox,
            this.jobAmountTextBox});
            this.millReceiptDataGridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.millReceiptDataGridView.GridColor = System.Drawing.Color.Maroon;
            this.millReceiptDataGridView.Location = new System.Drawing.Point(19, 177);
            this.millReceiptDataGridView.Name = "millReceiptDataGridView";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.Color.Blue;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.millReceiptDataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle12;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.millReceiptDataGridView.RowsDefaultCellStyle = dataGridViewCellStyle13;
            this.millReceiptDataGridView.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.millReceiptDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.millReceiptDataGridView.Size = new System.Drawing.Size(887, 219);
            this.millReceiptDataGridView.TabIndex = 8;
            this.millReceiptDataGridView.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.millReceiptDataGridView_CellEnter);
            this.millReceiptDataGridView.CellValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.millReceiptDataGridView_CellValidated);
            this.millReceiptDataGridView.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.millReceiptDataGridView_CellValidating);
            this.millReceiptDataGridView.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.millReceiptDataGridView_CellValueChanged);
            this.millReceiptDataGridView.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.millReceiptDataGridView_EditingControlShowing);
            // 
            // netAmtAftTdsTextBox
            // 
            this.netAmtAftTdsTextBox.BackColor = System.Drawing.SystemColors.Info;
            this.netAmtAftTdsTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.netAmtAftTdsTextBox.ForeColor = System.Drawing.SystemColors.ControlText;
            this.netAmtAftTdsTextBox.Location = new System.Drawing.Point(804, 567);
            this.netAmtAftTdsTextBox.Name = "netAmtAftTdsTextBox";
            this.netAmtAftTdsTextBox.ReadOnly = true;
            this.netAmtAftTdsTextBox.Size = new System.Drawing.Size(100, 24);
            this.netAmtAftTdsTextBox.TabIndex = 61;
            this.netAmtAftTdsTextBox.TabStop = false;
            // 
            // taxValTextBox
            // 
            this.taxValTextBox.BackColor = System.Drawing.SystemColors.Info;
            this.taxValTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.taxValTextBox.ForeColor = System.Drawing.SystemColors.ControlText;
            this.taxValTextBox.Location = new System.Drawing.Point(804, 485);
            this.taxValTextBox.Name = "taxValTextBox";
            this.taxValTextBox.ReadOnly = true;
            this.taxValTextBox.Size = new System.Drawing.Size(100, 24);
            this.taxValTextBox.TabIndex = 62;
            this.taxValTextBox.TabStop = false;
            this.taxValTextBox.Text = "0.0";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Red;
            this.label32.Location = new System.Drawing.Point(635, 571);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(168, 16);
            this.label32.TabIndex = 59;
            this.label32.Text = "NET AMT AFTER TDS:";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label31.Location = new System.Drawing.Point(660, 489);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(132, 16);
            this.label31.TabIndex = 60;
            this.label31.Text = "TAXABLE VALUE:";
            // 
            // detailsGroupBox
            // 
            this.detailsGroupBox.Controls.Add(this.igstAmtTextBox);
            this.detailsGroupBox.Controls.Add(this.sgstAmtTextBox);
            this.detailsGroupBox.Controls.Add(this.cgstAmtTextBox);
            this.detailsGroupBox.Controls.Add(this.label30);
            this.detailsGroupBox.Controls.Add(this.label29);
            this.detailsGroupBox.Controls.Add(this.label28);
            this.detailsGroupBox.Controls.Add(this.igstRateTextBox);
            this.detailsGroupBox.Controls.Add(this.sgstRateTextBox);
            this.detailsGroupBox.Controls.Add(this.cgstRateTextBox);
            this.detailsGroupBox.Controls.Add(this.label27);
            this.detailsGroupBox.Controls.Add(this.label26);
            this.detailsGroupBox.Controls.Add(this.label25);
            this.detailsGroupBox.Controls.Add(this.label22);
            this.detailsGroupBox.Controls.Add(this.label20);
            this.detailsGroupBox.Controls.Add(this.tdsAmtTextBox2);
            this.detailsGroupBox.Controls.Add(this.tdsPerTextBox);
            this.detailsGroupBox.Controls.Add(this.rdAmtFinalTextBox);
            this.detailsGroupBox.Controls.Add(this.reMtrTextBox);
            this.detailsGroupBox.Controls.Add(this.discPerAmtFinalTextBox);
            this.detailsGroupBox.Controls.Add(this.discPerTextBox);
            this.detailsGroupBox.Controls.Add(this.label19);
            this.detailsGroupBox.Controls.Add(this.label18);
            this.detailsGroupBox.Controls.Add(this.label9);
            this.detailsGroupBox.Controls.Add(this.label24);
            this.detailsGroupBox.Controls.Add(this.label23);
            this.detailsGroupBox.Controls.Add(this.label17);
            this.detailsGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.detailsGroupBox.ForeColor = System.Drawing.Color.Maroon;
            this.detailsGroupBox.Location = new System.Drawing.Point(3, 439);
            this.detailsGroupBox.Name = "detailsGroupBox";
            this.detailsGroupBox.Size = new System.Drawing.Size(631, 127);
            this.detailsGroupBox.TabIndex = 9;
            this.detailsGroupBox.TabStop = false;
            this.detailsGroupBox.Text = "DETAILS";
            // 
            // igstAmtTextBox
            // 
            this.igstAmtTextBox.Location = new System.Drawing.Point(529, 88);
            this.igstAmtTextBox.Name = "igstAmtTextBox";
            this.igstAmtTextBox.Size = new System.Drawing.Size(93, 24);
            this.igstAmtTextBox.TabIndex = 20;
            this.igstAmtTextBox.Text = "0.0";
            this.igstAmtTextBox.TextChanged += new System.EventHandler(this.igstAmtTextBox_TextChanged);
            // 
            // sgstAmtTextBox
            // 
            this.sgstAmtTextBox.Location = new System.Drawing.Point(529, 53);
            this.sgstAmtTextBox.Name = "sgstAmtTextBox";
            this.sgstAmtTextBox.Size = new System.Drawing.Size(93, 24);
            this.sgstAmtTextBox.TabIndex = 18;
            this.sgstAmtTextBox.Text = "0.0";
            this.sgstAmtTextBox.TextChanged += new System.EventHandler(this.sgstAmtTextBox_TextChanged);
            // 
            // cgstAmtTextBox
            // 
            this.cgstAmtTextBox.Location = new System.Drawing.Point(529, 20);
            this.cgstAmtTextBox.Name = "cgstAmtTextBox";
            this.cgstAmtTextBox.Size = new System.Drawing.Size(93, 24);
            this.cgstAmtTextBox.TabIndex = 16;
            this.cgstAmtTextBox.Text = "0.0";
            this.cgstAmtTextBox.TextChanged += new System.EventHandler(this.cgstAmtTextBox_TextChanged);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(453, 91);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(76, 18);
            this.label30.TabIndex = 8;
            this.label30.Text = "%  AMT.:";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(452, 54);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(76, 18);
            this.label29.TabIndex = 8;
            this.label29.Text = "%  AMT.:";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(452, 20);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(76, 18);
            this.label28.TabIndex = 8;
            this.label28.Text = "%  AMT.:";
            // 
            // igstRateTextBox
            // 
            this.igstRateTextBox.Location = new System.Drawing.Point(402, 90);
            this.igstRateTextBox.Name = "igstRateTextBox";
            this.igstRateTextBox.Size = new System.Drawing.Size(44, 24);
            this.igstRateTextBox.TabIndex = 19;
            this.igstRateTextBox.Text = "5";
            this.igstRateTextBox.TextChanged += new System.EventHandler(this.igstRateTextBox_TextChanged);
            // 
            // sgstRateTextBox
            // 
            this.sgstRateTextBox.Location = new System.Drawing.Point(402, 54);
            this.sgstRateTextBox.Name = "sgstRateTextBox";
            this.sgstRateTextBox.Size = new System.Drawing.Size(44, 24);
            this.sgstRateTextBox.TabIndex = 17;
            this.sgstRateTextBox.Text = "2.5";
            this.sgstRateTextBox.TextChanged += new System.EventHandler(this.sgstRateTextBox_TextChanged);
            // 
            // cgstRateTextBox
            // 
            this.cgstRateTextBox.Location = new System.Drawing.Point(402, 20);
            this.cgstRateTextBox.Name = "cgstRateTextBox";
            this.cgstRateTextBox.Size = new System.Drawing.Size(44, 24);
            this.cgstRateTextBox.TabIndex = 15;
            this.cgstRateTextBox.Text = "2.5";
            this.cgstRateTextBox.TextChanged += new System.EventHandler(this.cgstRateTextBox_TextChanged);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(342, 92);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(46, 18);
            this.label27.TabIndex = 6;
            this.label27.Text = "IGST";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(342, 57);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(53, 18);
            this.label26.TabIndex = 6;
            this.label26.Text = "SGST";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(342, 24);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(54, 18);
            this.label25.TabIndex = 6;
            this.label25.Text = "CGST";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(167, 89);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(22, 18);
            this.label22.TabIndex = 5;
            this.label22.Text = "%";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(167, 28);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(22, 18);
            this.label20.TabIndex = 5;
            this.label20.Text = "%";
            // 
            // tdsAmtTextBox2
            // 
            this.tdsAmtTextBox2.Location = new System.Drawing.Point(257, 88);
            this.tdsAmtTextBox2.Name = "tdsAmtTextBox2";
            this.tdsAmtTextBox2.Size = new System.Drawing.Size(70, 24);
            this.tdsAmtTextBox2.TabIndex = 14;
            this.tdsAmtTextBox2.Text = "0.0";
            this.tdsAmtTextBox2.TextChanged += new System.EventHandler(this.tdsAmtTextBox2_TextChanged);
            // 
            // tdsPerTextBox
            // 
            this.tdsPerTextBox.Location = new System.Drawing.Point(118, 88);
            this.tdsPerTextBox.Name = "tdsPerTextBox";
            this.tdsPerTextBox.Size = new System.Drawing.Size(43, 24);
            this.tdsPerTextBox.TabIndex = 13;
            this.tdsPerTextBox.Text = "0.0";
            this.tdsPerTextBox.TextChanged += new System.EventHandler(this.tdsPerTextBox_TextChanged);
            // 
            // rdAmtFinalTextBox
            // 
            this.rdAmtFinalTextBox.Location = new System.Drawing.Point(256, 53);
            this.rdAmtFinalTextBox.Name = "rdAmtFinalTextBox";
            this.rdAmtFinalTextBox.Size = new System.Drawing.Size(71, 24);
            this.rdAmtFinalTextBox.TabIndex = 12;
            this.rdAmtFinalTextBox.Text = "0.0";
            this.rdAmtFinalTextBox.TextChanged += new System.EventHandler(this.rdAmtFinalTextBox_TextChanged);
            // 
            // reMtrTextBox
            // 
            this.reMtrTextBox.Location = new System.Drawing.Point(118, 53);
            this.reMtrTextBox.Name = "reMtrTextBox";
            this.reMtrTextBox.Size = new System.Drawing.Size(43, 24);
            this.reMtrTextBox.TabIndex = 11;
            this.reMtrTextBox.Text = "0.0";
            this.reMtrTextBox.TextChanged += new System.EventHandler(this.reMtrTextBox_TextChanged);
            // 
            // discPerAmtFinalTextBox
            // 
            this.discPerAmtFinalTextBox.Location = new System.Drawing.Point(256, 22);
            this.discPerAmtFinalTextBox.Name = "discPerAmtFinalTextBox";
            this.discPerAmtFinalTextBox.Size = new System.Drawing.Size(71, 24);
            this.discPerAmtFinalTextBox.TabIndex = 10;
            this.discPerAmtFinalTextBox.Text = "0.0";
            this.discPerAmtFinalTextBox.TextChanged += new System.EventHandler(this.discPerAmtFinalTextBox_TextChanged);
            // 
            // discPerTextBox
            // 
            this.discPerTextBox.Location = new System.Drawing.Point(118, 22);
            this.discPerTextBox.Name = "discPerTextBox";
            this.discPerTextBox.Size = new System.Drawing.Size(43, 24);
            this.discPerTextBox.TabIndex = 9;
            this.discPerTextBox.Text = "0";
            this.discPerTextBox.TextChanged += new System.EventHandler(this.discPerTextBox_TextChanged);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(15, 91);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(41, 18);
            this.label19.TabIndex = 2;
            this.label19.Text = "TDS";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(7, 55);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(112, 18);
            this.label18.TabIndex = 1;
            this.label18.Text = "RD PER MTR";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(202, 88);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(52, 18);
            this.label9.TabIndex = 0;
            this.label9.Text = "AMT.:";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(176, 53);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(81, 18);
            this.label24.TabIndex = 0;
            this.label24.Text = "RD AMT.:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(202, 22);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(52, 18);
            this.label23.TabIndex = 0;
            this.label23.Text = "AMT.:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(16, 22);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(94, 18);
            this.label17.TabIndex = 0;
            this.label17.Text = "DISCOUNT";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(209, 406);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(110, 20);
            this.label10.TabIndex = 10;
            this.label10.Text = "REC MTRS.:";
            // 
            // totalRecMtrsTextBox
            // 
            this.totalRecMtrsTextBox.BackColor = System.Drawing.SystemColors.Info;
            this.totalRecMtrsTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalRecMtrsTextBox.Location = new System.Drawing.Point(320, 405);
            this.totalRecMtrsTextBox.Name = "totalRecMtrsTextBox";
            this.totalRecMtrsTextBox.ReadOnly = true;
            this.totalRecMtrsTextBox.Size = new System.Drawing.Size(87, 24);
            this.totalRecMtrsTextBox.TabIndex = 11;
            this.totalRecMtrsTextBox.TabStop = false;
            this.totalRecMtrsTextBox.Text = "0.0";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(415, 406);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(124, 20);
            this.label11.TabIndex = 10;
            this.label11.Text = "GREY MTRS.:";
            // 
            // totalGreyMtrsTextBox
            // 
            this.totalGreyMtrsTextBox.BackColor = System.Drawing.SystemColors.Info;
            this.totalGreyMtrsTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalGreyMtrsTextBox.Location = new System.Drawing.Point(538, 406);
            this.totalGreyMtrsTextBox.Name = "totalGreyMtrsTextBox";
            this.totalGreyMtrsTextBox.ReadOnly = true;
            this.totalGreyMtrsTextBox.Size = new System.Drawing.Size(87, 24);
            this.totalGreyMtrsTextBox.TabIndex = 11;
            this.totalGreyMtrsTextBox.TabStop = false;
            this.totalGreyMtrsTextBox.Text = "0.0";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label12.Location = new System.Drawing.Point(660, 407);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(134, 16);
            this.label12.TabIndex = 60;
            this.label12.Text = "GROSS AMOUNT:";
            // 
            // grossAmountTextBox
            // 
            this.grossAmountTextBox.BackColor = System.Drawing.SystemColors.Info;
            this.grossAmountTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grossAmountTextBox.ForeColor = System.Drawing.SystemColors.InfoText;
            this.grossAmountTextBox.Location = new System.Drawing.Point(803, 405);
            this.grossAmountTextBox.Name = "grossAmountTextBox";
            this.grossAmountTextBox.ReadOnly = true;
            this.grossAmountTextBox.Size = new System.Drawing.Size(100, 24);
            this.grossAmountTextBox.TabIndex = 62;
            this.grossAmountTextBox.TabStop = false;
            this.grossAmountTextBox.Text = "0.0";
            this.grossAmountTextBox.TextChanged += new System.EventHandler(this.grossAmountTextBox_TextChanged);
            // 
            // addLessTextBox
            // 
            this.addLessTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addLessTextBox.Location = new System.Drawing.Point(804, 446);
            this.addLessTextBox.Name = "addLessTextBox";
            this.addLessTextBox.Size = new System.Drawing.Size(99, 24);
            this.addLessTextBox.TabIndex = 21;
            this.addLessTextBox.Text = "0.0";
            this.addLessTextBox.TextChanged += new System.EventHandler(this.addLessTextBox_TextChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label13.Location = new System.Drawing.Point(659, 448);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(144, 16);
            this.label13.TabIndex = 60;
            this.label13.Text = "OTHER LESS/ADD:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label14.Location = new System.Drawing.Point(661, 530);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(125, 16);
            this.label14.TabIndex = 59;
            this.label14.Text = "INVOICE VALUE:";
            // 
            // invValueTextBox
            // 
            this.invValueTextBox.BackColor = System.Drawing.SystemColors.Info;
            this.invValueTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.invValueTextBox.ForeColor = System.Drawing.SystemColors.ControlText;
            this.invValueTextBox.Location = new System.Drawing.Point(804, 526);
            this.invValueTextBox.Name = "invValueTextBox";
            this.invValueTextBox.ReadOnly = true;
            this.invValueTextBox.Size = new System.Drawing.Size(100, 24);
            this.invValueTextBox.TabIndex = 61;
            this.invValueTextBox.TabStop = false;
            this.invValueTextBox.Text = "0";
            // 
            // companyNameTextBox
            // 
            this.companyNameTextBox.BackColor = System.Drawing.SystemColors.Info;
            this.companyNameTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.companyNameTextBox.ForeColor = System.Drawing.Color.Blue;
            this.companyNameTextBox.Location = new System.Drawing.Point(155, 42);
            this.companyNameTextBox.Name = "companyNameTextBox";
            this.companyNameTextBox.ReadOnly = true;
            this.companyNameTextBox.Size = new System.Drawing.Size(308, 26);
            this.companyNameTextBox.TabIndex = 0;
            this.companyNameTextBox.TabStop = false;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(506, 124);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(88, 18);
            this.label15.TabIndex = 14;
            this.label15.Text = "REMARK.:";
            // 
            // remarkTextBox
            // 
            this.remarkTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.remarkTextBox.Location = new System.Drawing.Point(598, 123);
            this.remarkTextBox.Name = "remarkTextBox";
            this.remarkTextBox.Size = new System.Drawing.Size(305, 22);
            this.remarkTextBox.TabIndex = 7;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(318, 125);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(96, 18);
            this.label16.TabIndex = 14;
            this.label16.Text = "HSNCODE:";
            // 
            // hsnCodeTextBox
            // 
            this.hsnCodeTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hsnCodeTextBox.Location = new System.Drawing.Point(419, 124);
            this.hsnCodeTextBox.Name = "hsnCodeTextBox";
            this.hsnCodeTextBox.Size = new System.Drawing.Size(83, 22);
            this.hsnCodeTextBox.TabIndex = 6;
            // 
            // createButton
            // 
            this.createButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.createButton.ForeColor = System.Drawing.Color.Navy;
            this.createButton.Location = new System.Drawing.Point(74, 583);
            this.createButton.Name = "createButton";
            this.createButton.Size = new System.Drawing.Size(105, 41);
            this.createButton.TabIndex = 21;
            this.createButton.Text = "ADD NEW";
            this.createButton.UseVisualStyleBackColor = true;
            this.createButton.Click += new System.EventHandler(this.createButton_Click);
            // 
            // viewButton
            // 
            this.viewButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.viewButton.ForeColor = System.Drawing.Color.Navy;
            this.viewButton.Location = new System.Drawing.Point(222, 583);
            this.viewButton.Name = "viewButton";
            this.viewButton.Size = new System.Drawing.Size(105, 41);
            this.viewButton.TabIndex = 22;
            this.viewButton.Text = "VIEW";
            this.viewButton.UseVisualStyleBackColor = true;
            this.viewButton.Click += new System.EventHandler(this.viewButton_Click);
            // 
            // updateButton
            // 
            this.updateButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.updateButton.ForeColor = System.Drawing.Color.Navy;
            this.updateButton.Location = new System.Drawing.Point(370, 583);
            this.updateButton.Name = "updateButton";
            this.updateButton.Size = new System.Drawing.Size(105, 41);
            this.updateButton.TabIndex = 23;
            this.updateButton.Text = "UPDATE";
            this.updateButton.UseVisualStyleBackColor = true;
            this.updateButton.Click += new System.EventHandler(this.updateButton_Click);
            // 
            // deleteButton
            // 
            this.deleteButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deleteButton.ForeColor = System.Drawing.Color.Navy;
            this.deleteButton.Location = new System.Drawing.Point(518, 583);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(105, 41);
            this.deleteButton.TabIndex = 24;
            this.deleteButton.Text = "DELETE";
            this.deleteButton.UseVisualStyleBackColor = true;
            this.deleteButton.Click += new System.EventHandler(this.deleteButton_Click);
            // 
            // dataGridViewComboBoxColumn1
            // 
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataGridViewComboBoxColumn1.DefaultCellStyle = dataGridViewCellStyle14;
            this.dataGridViewComboBoxColumn1.HeaderText = "CHALLAN NO.";
            this.dataGridViewComboBoxColumn1.Name = "dataGridViewComboBoxColumn1";
            this.dataGridViewComboBoxColumn1.Width = 70;
            // 
            // dataGridViewComboBoxColumn2
            // 
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataGridViewComboBoxColumn2.DefaultCellStyle = dataGridViewCellStyle15;
            this.dataGridViewComboBoxColumn2.HeaderText = "GREY QUALITY";
            this.dataGridViewComboBoxColumn2.Name = "dataGridViewComboBoxColumn2";
            this.dataGridViewComboBoxColumn2.ReadOnly = true;
            this.dataGridViewComboBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn2.Width = 130;
            // 
            // challanNoComboBox
            // 
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.challanNoComboBox.DefaultCellStyle = dataGridViewCellStyle1;
            this.challanNoComboBox.HeaderText = "CHALLAN NO.";
            this.challanNoComboBox.Name = "challanNoComboBox";
            this.challanNoComboBox.Width = 70;
            // 
            // lotNoTextBox
            // 
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lotNoTextBox.DefaultCellStyle = dataGridViewCellStyle2;
            this.lotNoTextBox.HeaderText = "LOT NO";
            this.lotNoTextBox.Name = "lotNoTextBox";
            this.lotNoTextBox.Width = 70;
            // 
            // recdCardNoTextBox
            // 
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.recdCardNoTextBox.DefaultCellStyle = dataGridViewCellStyle3;
            this.recdCardNoTextBox.HeaderText = "REC CARD NO.";
            this.recdCardNoTextBox.Name = "recdCardNoTextBox";
            this.recdCardNoTextBox.ReadOnly = true;
            this.recdCardNoTextBox.Width = 70;
            // 
            // reprocessTakaCheckBox
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.NullValue = false;
            this.reprocessTakaCheckBox.DefaultCellStyle = dataGridViewCellStyle4;
            this.reprocessTakaCheckBox.HeaderText = "REPROCESS";
            this.reprocessTakaCheckBox.Name = "reprocessTakaCheckBox";
            // 
            // markaTextBox
            // 
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.markaTextBox.DefaultCellStyle = dataGridViewCellStyle5;
            this.markaTextBox.HeaderText = "MARKA";
            this.markaTextBox.Name = "markaTextBox";
            // 
            // greyQualityComboBox
            // 
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.greyQualityComboBox.DefaultCellStyle = dataGridViewCellStyle6;
            this.greyQualityComboBox.HeaderText = "GREY QUALITY";
            this.greyQualityComboBox.Name = "greyQualityComboBox";
            this.greyQualityComboBox.ReadOnly = true;
            this.greyQualityComboBox.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.greyQualityComboBox.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.greyQualityComboBox.Width = 130;
            // 
            // takaTextBox
            // 
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.takaTextBox.DefaultCellStyle = dataGridViewCellStyle7;
            this.takaTextBox.HeaderText = "TAKA";
            this.takaTextBox.Name = "takaTextBox";
            this.takaTextBox.Width = 50;
            // 
            // recMtrsTextBox
            // 
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.NullValue = "0";
            this.recMtrsTextBox.DefaultCellStyle = dataGridViewCellStyle8;
            this.recMtrsTextBox.HeaderText = "REC MTRS.";
            this.recMtrsTextBox.Name = "recMtrsTextBox";
            // 
            // greyMtrsTextBox
            // 
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.NullValue = "0.0";
            this.greyMtrsTextBox.DefaultCellStyle = dataGridViewCellStyle9;
            this.greyMtrsTextBox.HeaderText = "GREY MTRS.";
            this.greyMtrsTextBox.Name = "greyMtrsTextBox";
            this.greyMtrsTextBox.ReadOnly = true;
            // 
            // jobRateTextBox
            // 
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.NullValue = "0.0";
            this.jobRateTextBox.DefaultCellStyle = dataGridViewCellStyle10;
            this.jobRateTextBox.HeaderText = "JOB RATE";
            this.jobRateTextBox.Name = "jobRateTextBox";
            this.jobRateTextBox.Width = 50;
            // 
            // jobAmountTextBox
            // 
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.NullValue = "0.0";
            this.jobAmountTextBox.DefaultCellStyle = dataGridViewCellStyle11;
            this.jobAmountTextBox.HeaderText = "JOB AMOUNT";
            this.jobAmountTextBox.Name = "jobAmountTextBox";
            // 
            // MillReceiptWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.deleteButton);
            this.Controls.Add(this.updateButton);
            this.Controls.Add(this.viewButton);
            this.Controls.Add(this.createButton);
            this.Controls.Add(this.invValueTextBox);
            this.Controls.Add(this.netAmtAftTdsTextBox);
            this.Controls.Add(this.grossAmountTextBox);
            this.Controls.Add(this.taxValTextBox);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.detailsGroupBox);
            this.Controls.Add(this.millReceiptDataGridView);
            this.Controls.Add(this.jobTypeComboBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.companyNameTextBox);
            this.Controls.Add(this.remarkTextBox);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.gstnTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.partyNameComboBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.addLessTextBox);
            this.Controls.Add(this.totalGreyMtrsTextBox);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.totalRecMtrsTextBox);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.totalRecTakaTextBox);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.hsnCodeTextBox);
            this.Controls.Add(this.billNoTextBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.voucherNoTextBox);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.dateTextBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Name = "MillReceiptWindow";
            this.Size = new System.Drawing.Size(1003, 629);
            this.Load += new System.EventHandler(this.MillReceiptWindow_Load);
            ((System.ComponentModel.ISupportInitialize)(this.millReceiptDataGridView)).EndInit();
            this.detailsGroupBox.ResumeLayout(false);
            this.detailsGroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox voucherNoTextBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox dateTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox partyNameComboBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox gstnTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox jobTypeComboBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox billNoTextBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox totalRecTakaTextBox;
        private System.Windows.Forms.DataGridView millReceiptDataGridView;
        private System.Windows.Forms.TextBox netAmtAftTdsTextBox;
        private System.Windows.Forms.TextBox taxValTextBox;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.GroupBox detailsGroupBox;
        private System.Windows.Forms.TextBox igstAmtTextBox;
        private System.Windows.Forms.TextBox sgstAmtTextBox;
        private System.Windows.Forms.TextBox cgstAmtTextBox;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox igstRateTextBox;
        private System.Windows.Forms.TextBox sgstRateTextBox;
        private System.Windows.Forms.TextBox cgstRateTextBox;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox tdsAmtTextBox2;
        private System.Windows.Forms.TextBox tdsPerTextBox;
        private System.Windows.Forms.TextBox rdAmtFinalTextBox;
        private System.Windows.Forms.TextBox reMtrTextBox;
        private System.Windows.Forms.TextBox discPerAmtFinalTextBox;
        private System.Windows.Forms.TextBox discPerTextBox;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox totalRecMtrsTextBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox totalGreyMtrsTextBox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox grossAmountTextBox;
        private System.Windows.Forms.TextBox addLessTextBox;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox invValueTextBox;
        private System.Windows.Forms.TextBox companyNameTextBox;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox remarkTextBox;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox hsnCodeTextBox;
        private System.Windows.Forms.Button createButton;
        private System.Windows.Forms.Button viewButton;
        private System.Windows.Forms.Button updateButton;
        private System.Windows.Forms.Button deleteButton;
        private System.Windows.Forms.DataGridViewComboBoxColumn challanNoComboBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn lotNoTextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn recdCardNoTextBox;
        private System.Windows.Forms.DataGridViewCheckBoxColumn reprocessTakaCheckBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn markaTextBox;
        private System.Windows.Forms.DataGridViewComboBoxColumn greyQualityComboBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn takaTextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn recMtrsTextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn greyMtrsTextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn jobRateTextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn jobAmountTextBox;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn1;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn2;
    }
}
