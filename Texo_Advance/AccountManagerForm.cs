﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Texo_Advance.DBItems;

namespace Texo_Advance
{
    public partial class AccountManagerForm : Form
    {
        private long lDefaultAccountType = 0;
        MasterDBData DB = new MasterDBData();
        string strDefaultName = "";
        public AccountManagerForm()
        {
            InitializeComponent();
        }

        private void AccountManagerForm_Load(object sender, EventArgs e)
        {
            GetAccountTypeData();
            GetLedgerTypeData();
            GetCityData();
            UpdateBrokerList();
            updateTransportList();
            nameTextBox.Text = strDefaultName;
        }
        public void setDefaultAccountType(long strAccType)
        {
            lDefaultAccountType = strAccType;
        }
        private void GetAccountTypeData()
        {

            var typeOfAccList = DB.getAccountTypeList();
            if (typeOfAccList != null)
            {
                accTypeComboBox.DisplayMember = "type";
                accTypeComboBox.ValueMember = "account_typeid";
                accTypeComboBox.DataSource = typeOfAccList;
                if (lDefaultAccountType == 0)
                    accTypeComboBox.SelectedIndex = 0;
                else
                    accTypeComboBox.SelectedValue = lDefaultAccountType;
            }
         }
        private void GetLedgerTypeData()
        {
            
            var typeOfLedgerList = DB.getLedgerTypeList();
            if (typeOfLedgerList != null)
            {
                ledgerTypeComboBox.DisplayMember = "type";
                ledgerTypeComboBox.ValueMember = "ledger_typeid";
                ledgerTypeComboBox.DataSource = typeOfLedgerList;
            }
            
        }
        private void GetCityData()
        {
            var cityList = DB.getCityList();
            var stationList = DB.getCityList();
            if (cityList != null && stationList != null)
            {
                cityComboBox.DisplayMember = "name";
                stationComboBox.DisplayMember = "name";
                stationComboBox.ValueMember = "city_id";
                cityComboBox.ValueMember = "city_id";
                cityComboBox.DataSource = cityList;
                stationComboBox.DataSource = stationList;

            }

            
        }

        private void cityComboBox_KeyDown(object sender, KeyEventArgs e)
        {
            int iSelectIndx = -1;
            if(e.KeyCode == Keys.Enter)
            {
                iSelectIndx = cityComboBox.FindString(cityComboBox.Text);
                if (iSelectIndx == -1)
                {
                    if (MessageBox.Show("Do You want to add new City?", "ADD CITY", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        DB.addCity(cityComboBox.Text);
                        GetCityData();
                        
                    }
                }
                else
                {
                    cityComboBox.SelectedIndex = iSelectIndx;
                }

            }
        }

        private void brokerComboBox_KeyDown(object sender, KeyEventArgs e)
        {
            int iSelectIndx = -1;
            if (e.KeyCode == Keys.Enter)
            {
                iSelectIndx = brokerComboBox.FindString(brokerComboBox.Text);
                if (iSelectIndx == -1)
                {
                    if (MessageBox.Show("Do You want to add new Broker?", "ADD BROKER", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        BrokerManagerForm newBrokerFrorm = new BrokerManagerForm();
                        string brokerName = brokerComboBox.Text;
                        newBrokerFrorm.setDefaultName(brokerComboBox.Text);
                        newBrokerFrorm.ShowDialog();
                        UpdateBrokerList();
                        brokerComboBox.Text = brokerName;
                    }
                }
                else
                {
                    brokerComboBox.SelectedIndex = iSelectIndx;
                }

            }
        }

        private void addNewButton_Click(object sender, EventArgs e)
        {
            if(nameTextBox.Text.Length <= 0)
            {
                MessageBox.Show("Please enter the Party Name", "Party Name", MessageBoxButtons.OK);
                nameTextBox.Focus();
                return;
            }
            if (accTypeComboBox.SelectedIndex == -1)
            {
                MessageBox.Show("Please Select proper Account Type", "Account Type", MessageBoxButtons.OK);
                return;
            }
            if (transportComboBox.SelectedIndex == -1)
            {
                MessageBox.Show("Please Select proper Transport Type", "Transport", MessageBoxButtons.OK);
                return;
            }
            bool bFlag = fetchDataFromControl();
            if(bFlag)
            {
                MessageBox.Show("Party Successfully Added", "New Party", MessageBoxButtons.OK);
                Close();
            }                
            else
                MessageBox.Show("Party not Added", "New Party", MessageBoxButtons.OK);
             
        }
        private void UpdateBrokerList()
        {
            var brokerList = DB.getBrokerDetails();
            if(brokerList != null)
            {
                brokerComboBox.DisplayMember = "party_name";
                brokerComboBox.ValueMember = "brokerid";
                brokerComboBox.DataSource = brokerList;
            }
            
        }

        private void updateTransportList()
        {
            
            var transportList = DB.getTransportDetails();
            if(transportList != null)
            {
                transportComboBox.DisplayMember = "name";
                transportComboBox.ValueMember = "transportid";
                transportComboBox.DataSource = transportList;
            }
            
            
        }

        private bool fetchDataFromControl()
        {
            
                
            var partyNew = new party_details();
            partyNew.account_typeid = Convert.ToInt64(accTypeComboBox.SelectedValue);
            partyNew.party_name = nameTextBox.Text;
            partyNew.address = addressTextBox.Text;
            partyNew.address2 = addLine2TextBox.Text;
            partyNew.city_id = Convert.ToInt64(cityComboBox.SelectedValue);
            partyNew.contactperson = conPersonTextBox.Text;
            partyNew.distance = distanceTextBox.Text;
            partyNew.emailid = emailIdTextBox.Text;
            partyNew.gst = gstnTextBox.Text;
            partyNew.ledger_typeid = Convert.ToInt64(ledgerTypeComboBox.SelectedValue);
            partyNew.panno = panTextBox.Text;
            partyNew.brokerid = Convert.ToInt64(brokerComboBox.SelectedValue);
            partyNew.transportid = Convert.ToInt64(transportComboBox.SelectedValue);
            if (phone1TextBox.TextLength > 0)
                partyNew.phone1 = long.Parse(phone1TextBox.Text);
            else
                partyNew.phone1 = 0;
            if (phone2TextBox.TextLength > 0)
                partyNew.phone2 = long.Parse(phone2TextBox.Text);
            else
                partyNew.phone2 = 0;
            if (pinTextBox.TextLength > 0)
                partyNew.pincode = long.Parse(pinTextBox.Text);
            else
                partyNew.pincode = 0;
            partyNew.station_name = stationComboBox.Text;

            if (tdsTextBox.TextLength > 0 && tdsTextBox.Text != "0.0")
                partyNew.tds = double.Parse(tdsTextBox.Text);
            else
                partyNew.tds = 0;
            try
            {
                using (var db = new Entities())
                {
                    db.party_details.Add(partyNew);
                    db.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
    
            
        }

        private void stationComboBox_KeyDown(object sender, KeyEventArgs e)
        {
            int iSelectIndx = -1;
            if (e.KeyCode == Keys.Enter)
            {
                if (stationComboBox.Text == "")
                    return;
                iSelectIndx = stationComboBox.FindString(stationComboBox.Text);
                if (iSelectIndx == -1)
                {
                    if (MessageBox.Show("Do You want to add new Station?", "ADD STATION", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        string cityName = stationComboBox.Text;
                        DB.addCity(stationComboBox.Text);
                        GetCityData();
                        stationComboBox.Text = cityName;
                    }
                }
                else
                {
                    stationComboBox.SelectedIndex = iSelectIndx;
                }

            }
        }

        private void transportComboBox_KeyDown(object sender, KeyEventArgs e)
        {
            int iSelectIndx = -1;
            if (e.KeyCode == Keys.Enter)
            {
                if (transportComboBox.Text == "")
                    return;
                iSelectIndx = transportComboBox.FindString(transportComboBox.Text);
                if (iSelectIndx == -1)
                {
                    if (MessageBox.Show("Do You want to add new Transport?", "ADD TRANSPORT", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        string transportName = transportComboBox.Text;
                        DB.addTransport(transportComboBox.Text);
                        updateTransportList();
                        transportComboBox.Text = transportName;
                    }
                }
                else
                {
                    transportComboBox.SelectedIndex = iSelectIndx;
                }

            }
        }

        private void brokerComboBox_Validating(object sender, CancelEventArgs e)
        {
            int iSelectedIndex = brokerComboBox.SelectedIndex;
            if(iSelectedIndex == -1)
            {
                MessageBox.Show("Please select the correct Broker", "Broker", MessageBoxButtons.OK);
                brokerComboBox.Focus();
            }
           
        }

        private void cityComboBox_Validating(object sender, CancelEventArgs e)
        {
            int iSelectedIndex = cityComboBox.SelectedIndex;
            if (iSelectedIndex == -1)
            {
                MessageBox.Show("Please select the correct City", "CITY", MessageBoxButtons.OK);
                cityComboBox.Focus();
            }

        }

        private void stationComboBox_Validating(object sender, CancelEventArgs e)
        {
            int iSelectedIndex = stationComboBox.SelectedIndex;
            if (iSelectedIndex == -1)
            {
                MessageBox.Show("Please select the correct City", "CITY", MessageBoxButtons.OK);
                stationComboBox.Focus();
            }
        }

        private void cityComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            long lSelectedIndex = Convert.ToInt64(cityComboBox.SelectedValue);
            if (lSelectedIndex != -1)
            {
                stationComboBox.SelectedValue = lSelectedIndex;
            }
        }

        public void setDefaultName(string defaultName)
        {
            strDefaultName = defaultName;
        }

        public string getDefaultName()
        {
            return nameTextBox.Text;
        }
    }
}
