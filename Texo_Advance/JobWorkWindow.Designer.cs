﻿namespace Texo_Advance
{
    partial class JobWorkWindow
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle44 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle45 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle35 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle36 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle37 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle38 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle39 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle40 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle41 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle42 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle43 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle46 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle47 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle48 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.jobWorkTypeComboBox = new System.Windows.Forms.ComboBox();
            this.billTypeLabel = new System.Windows.Forms.Label();
            this.voucherNoTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.dateTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.partyNameComboBox = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.partyGstnTextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.billNoTextBox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.gstTypeComboBox = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.brokerComboBox = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.workTypeComboBox = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.transportComboBox = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.lrNoTextBox = new System.Windows.Forms.TextBox();
            this.billAmtTextBox = new System.Windows.Forms.TextBox();
            this.taxValTextBox = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.detailsGroupBox = new System.Windows.Forms.GroupBox();
            this.igstAmtTextBox = new System.Windows.Forms.TextBox();
            this.sgstAmtTextBox = new System.Windows.Forms.TextBox();
            this.cgstAmtTextBox = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.igstRateTextBox = new System.Windows.Forms.TextBox();
            this.sgstRateTextBox = new System.Windows.Forms.TextBox();
            this.cgstRateTextBox = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.caseNoTextBox2 = new System.Windows.Forms.TextBox();
            this.tdsTextBox = new System.Windows.Forms.TextBox();
            this.caseNoTextBox = new System.Windows.Forms.TextBox();
            this.addLessAmtFinalTextBox = new System.Windows.Forms.TextBox();
            this.addLessAmtTextBox = new System.Windows.Forms.TextBox();
            this.addLessTextBox = new System.Windows.Forms.TextBox();
            this.tdsAmountTextBox = new System.Windows.Forms.TextBox();
            this.discPerAmtFinalTextBox = new System.Windows.Forms.TextBox();
            this.discPerAmtTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.discPerTextBox = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.remarkTextBox = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.viewBillButton = new System.Windows.Forms.Button();
            this.deleteButton = new System.Windows.Forms.Button();
            this.printButton = new System.Windows.Forms.Button();
            this.addNewButton = new System.Windows.Forms.Button();
            this.updateBill = new System.Windows.Forms.Button();
            this.totalAmountTextBox = new System.Windows.Forms.TextBox();
            this.totalQtyTextBox = new System.Windows.Forms.TextBox();
            this.totalPcsTextBox = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.companyNameTextBox = new System.Windows.Forms.TextBox();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.additionalJobChallanCheckBox = new System.Windows.Forms.CheckBox();
            this.jobChallanReceiveGridView = new System.Windows.Forms.DataGridView();
            this.jobReceiptDataGridView = new System.Windows.Forms.DataGridView();
            this.refReceiptComboBox = new Texo_Advance.CustomControl.DataGridViewMultiColumnComboColumn();
            this.itemNameReceiptComboBox = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.bundlesReceiptTextBox = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hsncodeReceiptTextBox = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.jobTypeReceiptTextBox = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.unitReceiptComboBox = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.pcsReceiptTextBox = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.qtyReceiptTextBox = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.plainTextBox = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.secTextBox = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.shortTextBox = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.freshTextBox = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cutReceiptTextBox = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rateReceiptTextBox = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.amountReceiptTextBox = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.jobDispatchDataGridView = new System.Windows.Forms.DataGridView();
            this.refNoComboBox = new Texo_Advance.CustomControl.DataGridViewMultiColumnComboColumn();
            this.itemNameComboBox = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.bundlesTextBox = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hsncTextBox = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.jobTypeTextBox = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.unitComboBox = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.pcsTextBox = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cutTextBox = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.qtyTextBox = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rateTextBox = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.amountTextBox = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.refNoChallanReceiveComboBox = new Texo_Advance.CustomControl.DataGridViewMultiColumnComboColumn();
            this.itemNameReceivveComboBox = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.bundlesReceiveTextBox = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hsnCodeReceiveTextBox = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.jobTypeReceiveTextBox = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.multipleJobsOnItem = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.unitReceiveComboBox = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.pcsReceiveTextBox = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.qunatityReceiveTextBox = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.plainReceiveTextBox = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.secondReceiveTextBox = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.shortReceiveTextBox = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.freshReceiveTextBox = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cutReceiveTextBox = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rateReceiveTextBox = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.amountReceiveTextBox = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.detailsGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.jobChallanReceiveGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.jobReceiptDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.jobDispatchDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(19, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "COMPANY:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(358, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 20);
            this.label2.TabIndex = 0;
            this.label2.Text = "TYPE:";
            // 
            // jobWorkTypeComboBox
            // 
            this.jobWorkTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.jobWorkTypeComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.jobWorkTypeComboBox.FormattingEnabled = true;
            this.jobWorkTypeComboBox.ItemHeight = 16;
            this.jobWorkTypeComboBox.Location = new System.Drawing.Point(424, 17);
            this.jobWorkTypeComboBox.Name = "jobWorkTypeComboBox";
            this.jobWorkTypeComboBox.Size = new System.Drawing.Size(262, 24);
            this.jobWorkTypeComboBox.TabIndex = 1;
            this.jobWorkTypeComboBox.SelectedValueChanged += new System.EventHandler(this.jobWorkTypeComboBox_SelectedValueChanged);
            // 
            // billTypeLabel
            // 
            this.billTypeLabel.AutoSize = true;
            this.billTypeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.billTypeLabel.Location = new System.Drawing.Point(694, 19);
            this.billTypeLabel.Name = "billTypeLabel";
            this.billTypeLabel.Size = new System.Drawing.Size(130, 18);
            this.billTypeLabel.TabIndex = 0;
            this.billTypeLabel.Text = "VOUCHER NO.:";
            // 
            // voucherNoTextBox
            // 
            this.voucherNoTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.voucherNoTextBox.Location = new System.Drawing.Point(827, 18);
            this.voucherNoTextBox.Name = "voucherNoTextBox";
            this.voucherNoTextBox.Size = new System.Drawing.Size(55, 22);
            this.voucherNoTextBox.TabIndex = 2;
            this.voucherNoTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.voucherNoTextBox_KeyDown);
            this.voucherNoTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.voucherNoTextBox_Validating);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(886, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 20);
            this.label4.TabIndex = 0;
            this.label4.Text = "DATE:";
            // 
            // dateTextBox
            // 
            this.dateTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTextBox.Location = new System.Drawing.Point(953, 19);
            this.dateTextBox.Name = "dateTextBox";
            this.dateTextBox.Size = new System.Drawing.Size(88, 22);
            this.dateTextBox.TabIndex = 3;
            this.dateTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.dateTextBox_Validating);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(19, 60);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 20);
            this.label5.TabIndex = 0;
            this.label5.Text = "PARTY:";
            // 
            // partyNameComboBox
            // 
            this.partyNameComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.partyNameComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.partyNameComboBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.partyNameComboBox.FormattingEnabled = true;
            this.partyNameComboBox.Location = new System.Drawing.Point(126, 58);
            this.partyNameComboBox.Name = "partyNameComboBox";
            this.partyNameComboBox.Size = new System.Drawing.Size(223, 28);
            this.partyNameComboBox.TabIndex = 4;
            this.partyNameComboBox.SelectedIndexChanged += new System.EventHandler(this.partyNameComboBox_SelectedIndexChanged_1);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(19, 99);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(62, 20);
            this.label6.TabIndex = 0;
            this.label6.Text = "GSTN:";
            // 
            // partyGstnTextBox
            // 
            this.partyGstnTextBox.BackColor = System.Drawing.SystemColors.Info;
            this.partyGstnTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.partyGstnTextBox.ForeColor = System.Drawing.Color.Blue;
            this.partyGstnTextBox.Location = new System.Drawing.Point(126, 100);
            this.partyGstnTextBox.Name = "partyGstnTextBox";
            this.partyGstnTextBox.ReadOnly = true;
            this.partyGstnTextBox.Size = new System.Drawing.Size(224, 22);
            this.partyGstnTextBox.TabIndex = 7;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(694, 57);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(87, 20);
            this.label7.TabIndex = 0;
            this.label7.Text = "BILL NO.:";
            // 
            // billNoTextBox
            // 
            this.billNoTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.billNoTextBox.Location = new System.Drawing.Point(830, 58);
            this.billNoTextBox.Name = "billNoTextBox";
            this.billNoTextBox.Size = new System.Drawing.Size(60, 22);
            this.billNoTextBox.TabIndex = 6;
            this.billNoTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.billNoTextBox_Validating);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(357, 58);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(100, 20);
            this.label8.TabIndex = 0;
            this.label8.Text = "GST TYPE:";
            // 
            // gstTypeComboBox
            // 
            this.gstTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.gstTypeComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gstTypeComboBox.FormattingEnabled = true;
            this.gstTypeComboBox.Location = new System.Drawing.Point(464, 56);
            this.gstTypeComboBox.Name = "gstTypeComboBox";
            this.gstTypeComboBox.Size = new System.Drawing.Size(214, 24);
            this.gstTypeComboBox.TabIndex = 5;
            this.gstTypeComboBox.SelectedIndexChanged += new System.EventHandler(this.gstTypeComboBox_SelectedIndexChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(354, 100);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(88, 20);
            this.label9.TabIndex = 0;
            this.label9.Text = "BROKER:";
            // 
            // brokerComboBox
            // 
            this.brokerComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.brokerComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.brokerComboBox.FormattingEnabled = true;
            this.brokerComboBox.Location = new System.Drawing.Point(442, 98);
            this.brokerComboBox.Name = "brokerComboBox";
            this.brokerComboBox.Size = new System.Drawing.Size(214, 24);
            this.brokerComboBox.TabIndex = 8;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(670, 99);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(117, 20);
            this.label10.TabIndex = 0;
            this.label10.Text = "WORK TYPE:";
            // 
            // workTypeComboBox
            // 
            this.workTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.workTypeComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.workTypeComboBox.FormattingEnabled = true;
            this.workTypeComboBox.Location = new System.Drawing.Point(794, 97);
            this.workTypeComboBox.Name = "workTypeComboBox";
            this.workTypeComboBox.Size = new System.Drawing.Size(214, 24);
            this.workTypeComboBox.TabIndex = 9;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(17, 137);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(120, 20);
            this.label11.TabIndex = 0;
            this.label11.Text = "TRANSPORT:";
            // 
            // transportComboBox
            // 
            this.transportComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.transportComboBox.FormattingEnabled = true;
            this.transportComboBox.Location = new System.Drawing.Point(127, 135);
            this.transportComboBox.Name = "transportComboBox";
            this.transportComboBox.Size = new System.Drawing.Size(223, 24);
            this.transportComboBox.TabIndex = 10;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(357, 136);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(72, 20);
            this.label12.TabIndex = 0;
            this.label12.Text = "LR NO.:";
            // 
            // lrNoTextBox
            // 
            this.lrNoTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lrNoTextBox.Location = new System.Drawing.Point(446, 136);
            this.lrNoTextBox.Name = "lrNoTextBox";
            this.lrNoTextBox.Size = new System.Drawing.Size(188, 22);
            this.lrNoTextBox.TabIndex = 11;
            // 
            // billAmtTextBox
            // 
            this.billAmtTextBox.BackColor = System.Drawing.SystemColors.Info;
            this.billAmtTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.billAmtTextBox.ForeColor = System.Drawing.Color.Blue;
            this.billAmtTextBox.Location = new System.Drawing.Point(925, 477);
            this.billAmtTextBox.Name = "billAmtTextBox";
            this.billAmtTextBox.ReadOnly = true;
            this.billAmtTextBox.Size = new System.Drawing.Size(100, 22);
            this.billAmtTextBox.TabIndex = 56;
            this.billAmtTextBox.TabStop = false;
            this.billAmtTextBox.Text = "0.0";
            // 
            // taxValTextBox
            // 
            this.taxValTextBox.BackColor = System.Drawing.SystemColors.Info;
            this.taxValTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.taxValTextBox.ForeColor = System.Drawing.Color.Blue;
            this.taxValTextBox.Location = new System.Drawing.Point(925, 426);
            this.taxValTextBox.Name = "taxValTextBox";
            this.taxValTextBox.ReadOnly = true;
            this.taxValTextBox.Size = new System.Drawing.Size(100, 22);
            this.taxValTextBox.TabIndex = 57;
            this.taxValTextBox.TabStop = false;
            this.taxValTextBox.Text = "0.0";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(782, 481);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(111, 16);
            this.label32.TabIndex = 54;
            this.label32.Text = "BILL AMOUNT:";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(782, 428);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(132, 16);
            this.label31.TabIndex = 55;
            this.label31.Text = "TAXABLE VALUE:";
            // 
            // detailsGroupBox
            // 
            this.detailsGroupBox.Controls.Add(this.igstAmtTextBox);
            this.detailsGroupBox.Controls.Add(this.sgstAmtTextBox);
            this.detailsGroupBox.Controls.Add(this.cgstAmtTextBox);
            this.detailsGroupBox.Controls.Add(this.label30);
            this.detailsGroupBox.Controls.Add(this.label29);
            this.detailsGroupBox.Controls.Add(this.label28);
            this.detailsGroupBox.Controls.Add(this.igstRateTextBox);
            this.detailsGroupBox.Controls.Add(this.sgstRateTextBox);
            this.detailsGroupBox.Controls.Add(this.cgstRateTextBox);
            this.detailsGroupBox.Controls.Add(this.label27);
            this.detailsGroupBox.Controls.Add(this.label26);
            this.detailsGroupBox.Controls.Add(this.label25);
            this.detailsGroupBox.Controls.Add(this.label22);
            this.detailsGroupBox.Controls.Add(this.label21);
            this.detailsGroupBox.Controls.Add(this.label20);
            this.detailsGroupBox.Controls.Add(this.caseNoTextBox2);
            this.detailsGroupBox.Controls.Add(this.tdsTextBox);
            this.detailsGroupBox.Controls.Add(this.caseNoTextBox);
            this.detailsGroupBox.Controls.Add(this.addLessAmtFinalTextBox);
            this.detailsGroupBox.Controls.Add(this.addLessAmtTextBox);
            this.detailsGroupBox.Controls.Add(this.addLessTextBox);
            this.detailsGroupBox.Controls.Add(this.tdsAmountTextBox);
            this.detailsGroupBox.Controls.Add(this.discPerAmtFinalTextBox);
            this.detailsGroupBox.Controls.Add(this.discPerAmtTextBox);
            this.detailsGroupBox.Controls.Add(this.label3);
            this.detailsGroupBox.Controls.Add(this.discPerTextBox);
            this.detailsGroupBox.Controls.Add(this.label19);
            this.detailsGroupBox.Controls.Add(this.label18);
            this.detailsGroupBox.Controls.Add(this.label13);
            this.detailsGroupBox.Controls.Add(this.label24);
            this.detailsGroupBox.Controls.Add(this.label23);
            this.detailsGroupBox.Controls.Add(this.label17);
            this.detailsGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.detailsGroupBox.Location = new System.Drawing.Point(40, 408);
            this.detailsGroupBox.Name = "detailsGroupBox";
            this.detailsGroupBox.Size = new System.Drawing.Size(735, 100);
            this.detailsGroupBox.TabIndex = 15;
            this.detailsGroupBox.TabStop = false;
            this.detailsGroupBox.Text = "DETAILS";
            // 
            // igstAmtTextBox
            // 
            this.igstAmtTextBox.Location = new System.Drawing.Point(631, 72);
            this.igstAmtTextBox.Name = "igstAmtTextBox";
            this.igstAmtTextBox.Size = new System.Drawing.Size(93, 22);
            this.igstAmtTextBox.TabIndex = 30;
            this.igstAmtTextBox.Text = "0.0";
            this.igstAmtTextBox.TextChanged += new System.EventHandler(this.igstAmtTextBox_TextChanged);
            // 
            // sgstAmtTextBox
            // 
            this.sgstAmtTextBox.Location = new System.Drawing.Point(631, 46);
            this.sgstAmtTextBox.Name = "sgstAmtTextBox";
            this.sgstAmtTextBox.Size = new System.Drawing.Size(93, 22);
            this.sgstAmtTextBox.TabIndex = 28;
            this.sgstAmtTextBox.Text = "0.0";
            this.sgstAmtTextBox.TextChanged += new System.EventHandler(this.sgstAmtTextBox_TextChanged);
            // 
            // cgstAmtTextBox
            // 
            this.cgstAmtTextBox.Location = new System.Drawing.Point(631, 20);
            this.cgstAmtTextBox.Name = "cgstAmtTextBox";
            this.cgstAmtTextBox.Size = new System.Drawing.Size(93, 22);
            this.cgstAmtTextBox.TabIndex = 26;
            this.cgstAmtTextBox.Text = "0.0";
            this.cgstAmtTextBox.TextChanged += new System.EventHandler(this.cgstAmtTextBox_TextChanged);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(555, 75);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(69, 16);
            this.label30.TabIndex = 8;
            this.label30.Text = "%  AMT.:";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(554, 47);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(69, 16);
            this.label29.TabIndex = 8;
            this.label29.Text = "%  AMT.:";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(554, 20);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(69, 16);
            this.label28.TabIndex = 8;
            this.label28.Text = "%  AMT.:";
            // 
            // igstRateTextBox
            // 
            this.igstRateTextBox.Location = new System.Drawing.Point(504, 75);
            this.igstRateTextBox.Name = "igstRateTextBox";
            this.igstRateTextBox.Size = new System.Drawing.Size(44, 22);
            this.igstRateTextBox.TabIndex = 29;
            this.igstRateTextBox.Text = "5";
            this.igstRateTextBox.TextChanged += new System.EventHandler(this.igstRateTextBox_TextChanged);
            // 
            // sgstRateTextBox
            // 
            this.sgstRateTextBox.Location = new System.Drawing.Point(504, 47);
            this.sgstRateTextBox.Name = "sgstRateTextBox";
            this.sgstRateTextBox.Size = new System.Drawing.Size(44, 22);
            this.sgstRateTextBox.TabIndex = 27;
            this.sgstRateTextBox.Text = "2.5";
            this.sgstRateTextBox.TextChanged += new System.EventHandler(this.sgstRateTextBox_TextChanged);
            // 
            // cgstRateTextBox
            // 
            this.cgstRateTextBox.Location = new System.Drawing.Point(504, 20);
            this.cgstRateTextBox.Name = "cgstRateTextBox";
            this.cgstRateTextBox.Size = new System.Drawing.Size(44, 22);
            this.cgstRateTextBox.TabIndex = 25;
            this.cgstRateTextBox.Text = "2.5";
            this.cgstRateTextBox.TextChanged += new System.EventHandler(this.cgstRateTextBox_TextChanged);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(444, 76);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(43, 16);
            this.label27.TabIndex = 6;
            this.label27.Text = "IGST";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(444, 50);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(49, 16);
            this.label26.TabIndex = 6;
            this.label26.Text = "SGST";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(444, 24);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(49, 16);
            this.label25.TabIndex = 6;
            this.label25.Text = "CGST";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(166, 73);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(17, 16);
            this.label22.TabIndex = 5;
            this.label22.Text = "X";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(188, 50);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(17, 16);
            this.label21.TabIndex = 5;
            this.label21.Text = "X";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(188, 28);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(21, 16);
            this.label20.TabIndex = 5;
            this.label20.Text = "%";
            // 
            // caseNoTextBox2
            // 
            this.caseNoTextBox2.Location = new System.Drawing.Point(190, 72);
            this.caseNoTextBox2.Name = "caseNoTextBox2";
            this.caseNoTextBox2.Size = new System.Drawing.Size(33, 22);
            this.caseNoTextBox2.TabIndex = 22;
            this.caseNoTextBox2.Text = "1";
            // 
            // tdsTextBox
            // 
            this.tdsTextBox.Location = new System.Drawing.Point(283, 72);
            this.tdsTextBox.Name = "tdsTextBox";
            this.tdsTextBox.Size = new System.Drawing.Size(44, 22);
            this.tdsTextBox.TabIndex = 23;
            this.tdsTextBox.Text = "0.0";
            this.tdsTextBox.TextChanged += new System.EventHandler(this.tdsTextBox_TextChanged);
            // 
            // caseNoTextBox
            // 
            this.caseNoTextBox.Location = new System.Drawing.Point(93, 72);
            this.caseNoTextBox.Name = "caseNoTextBox";
            this.caseNoTextBox.Size = new System.Drawing.Size(67, 22);
            this.caseNoTextBox.TabIndex = 21;
            // 
            // addLessAmtFinalTextBox
            // 
            this.addLessAmtFinalTextBox.ForeColor = System.Drawing.Color.Green;
            this.addLessAmtFinalTextBox.Location = new System.Drawing.Point(343, 46);
            this.addLessAmtFinalTextBox.Name = "addLessAmtFinalTextBox";
            this.addLessAmtFinalTextBox.Size = new System.Drawing.Size(71, 22);
            this.addLessAmtFinalTextBox.TabIndex = 20;
            this.addLessAmtFinalTextBox.Text = "0";
            this.addLessAmtFinalTextBox.TextChanged += new System.EventHandler(this.addLessAmtFinalTextBox_TextChanged);
            // 
            // addLessAmtTextBox
            // 
            this.addLessAmtTextBox.Location = new System.Drawing.Point(212, 46);
            this.addLessAmtTextBox.Name = "addLessAmtTextBox";
            this.addLessAmtTextBox.Size = new System.Drawing.Size(71, 22);
            this.addLessAmtTextBox.TabIndex = 19;
            this.addLessAmtTextBox.Text = "0";
            this.addLessAmtTextBox.TextChanged += new System.EventHandler(this.addLessAmtTextBox_TextChanged);
            // 
            // addLessTextBox
            // 
            this.addLessTextBox.Location = new System.Drawing.Point(139, 46);
            this.addLessTextBox.Name = "addLessTextBox";
            this.addLessTextBox.Size = new System.Drawing.Size(43, 22);
            this.addLessTextBox.TabIndex = 18;
            this.addLessTextBox.Text = "0";
            this.addLessTextBox.TextChanged += new System.EventHandler(this.addLessTextBox_TextChanged);
            // 
            // tdsAmountTextBox
            // 
            this.tdsAmountTextBox.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.tdsAmountTextBox.Location = new System.Drawing.Point(378, 71);
            this.tdsAmountTextBox.Name = "tdsAmountTextBox";
            this.tdsAmountTextBox.Size = new System.Drawing.Size(65, 22);
            this.tdsAmountTextBox.TabIndex = 24;
            this.tdsAmountTextBox.Text = "0.0";
            this.tdsAmountTextBox.TextChanged += new System.EventHandler(this.tdsAmountTextBox_TextChanged);
            // 
            // discPerAmtFinalTextBox
            // 
            this.discPerAmtFinalTextBox.ForeColor = System.Drawing.Color.Green;
            this.discPerAmtFinalTextBox.Location = new System.Drawing.Point(343, 22);
            this.discPerAmtFinalTextBox.Name = "discPerAmtFinalTextBox";
            this.discPerAmtFinalTextBox.Size = new System.Drawing.Size(71, 22);
            this.discPerAmtFinalTextBox.TabIndex = 17;
            this.discPerAmtFinalTextBox.Text = "0.0";
            this.discPerAmtFinalTextBox.TextChanged += new System.EventHandler(this.discPerAmtFinalTextBox_TextChanged);
            // 
            // discPerAmtTextBox
            // 
            this.discPerAmtTextBox.Location = new System.Drawing.Point(212, 22);
            this.discPerAmtTextBox.Name = "discPerAmtTextBox";
            this.discPerAmtTextBox.Size = new System.Drawing.Size(71, 22);
            this.discPerAmtTextBox.TabIndex = 16;
            this.discPerAmtTextBox.Text = "0.0";
            this.discPerAmtTextBox.TextChanged += new System.EventHandler(this.discPerAmtTextBox_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(227, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 16);
            this.label3.TabIndex = 2;
            this.label3.Text = "TDS %";
            // 
            // discPerTextBox
            // 
            this.discPerTextBox.Location = new System.Drawing.Point(139, 22);
            this.discPerTextBox.Name = "discPerTextBox";
            this.discPerTextBox.Size = new System.Drawing.Size(43, 22);
            this.discPerTextBox.TabIndex = 15;
            this.discPerTextBox.Text = "0";
            this.discPerTextBox.TextChanged += new System.EventHandler(this.discPerTextBox_TextChanged);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(13, 75);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(78, 16);
            this.label19.TabIndex = 2;
            this.label19.Text = "CASE NO.";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(16, 48);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(83, 16);
            this.label18.TabIndex = 1;
            this.label18.Text = "ADD/LESS";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(328, 74);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(48, 16);
            this.label13.TabIndex = 0;
            this.label13.Text = "AMT.:";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(289, 46);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(48, 16);
            this.label24.TabIndex = 0;
            this.label24.Text = "AMT.:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(289, 22);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(48, 16);
            this.label23.TabIndex = 0;
            this.label23.Text = "AMT.:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(16, 22);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(86, 16);
            this.label17.TabIndex = 0;
            this.label17.Text = "DISCOUNT";
            // 
            // remarkTextBox
            // 
            this.remarkTextBox.BackColor = System.Drawing.SystemColors.Info;
            this.remarkTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.remarkTextBox.Location = new System.Drawing.Point(106, 373);
            this.remarkTextBox.Name = "remarkTextBox";
            this.remarkTextBox.Size = new System.Drawing.Size(386, 22);
            this.remarkTextBox.TabIndex = 14;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(25, 374);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(75, 16);
            this.label16.TabIndex = 52;
            this.label16.Text = "REMARK:";
            // 
            // viewBillButton
            // 
            this.viewBillButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.viewBillButton.ForeColor = System.Drawing.Color.Blue;
            this.viewBillButton.Location = new System.Drawing.Point(464, 528);
            this.viewBillButton.Name = "viewBillButton";
            this.viewBillButton.Size = new System.Drawing.Size(99, 38);
            this.viewBillButton.TabIndex = 33;
            this.viewBillButton.Text = "VIEW";
            this.viewBillButton.UseVisualStyleBackColor = true;
            this.viewBillButton.Click += new System.EventHandler(this.viewBillButton_Click);
            // 
            // deleteButton
            // 
            this.deleteButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deleteButton.ForeColor = System.Drawing.Color.Red;
            this.deleteButton.Location = new System.Drawing.Point(732, 528);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(99, 38);
            this.deleteButton.TabIndex = 35;
            this.deleteButton.Text = "DELETE";
            this.deleteButton.UseVisualStyleBackColor = true;
            this.deleteButton.Click += new System.EventHandler(this.deleteButton_Click);
            // 
            // printButton
            // 
            this.printButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.printButton.ForeColor = System.Drawing.Color.Blue;
            this.printButton.Location = new System.Drawing.Point(598, 528);
            this.printButton.Name = "printButton";
            this.printButton.Size = new System.Drawing.Size(99, 38);
            this.printButton.TabIndex = 34;
            this.printButton.Text = "PRINT";
            this.printButton.UseVisualStyleBackColor = true;
            // 
            // addNewButton
            // 
            this.addNewButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addNewButton.ForeColor = System.Drawing.Color.Blue;
            this.addNewButton.Location = new System.Drawing.Point(196, 528);
            this.addNewButton.Name = "addNewButton";
            this.addNewButton.Size = new System.Drawing.Size(99, 38);
            this.addNewButton.TabIndex = 31;
            this.addNewButton.Text = "ADD NEW";
            this.addNewButton.UseVisualStyleBackColor = true;
            this.addNewButton.Click += new System.EventHandler(this.addNewButton_Click);
            // 
            // updateBill
            // 
            this.updateBill.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.updateBill.ForeColor = System.Drawing.Color.Blue;
            this.updateBill.Location = new System.Drawing.Point(330, 528);
            this.updateBill.Name = "updateBill";
            this.updateBill.Size = new System.Drawing.Size(99, 38);
            this.updateBill.TabIndex = 32;
            this.updateBill.Text = "UPDATE";
            this.updateBill.UseVisualStyleBackColor = true;
            this.updateBill.Click += new System.EventHandler(this.updateBill_Click);
            // 
            // totalAmountTextBox
            // 
            this.totalAmountTextBox.BackColor = System.Drawing.SystemColors.Info;
            this.totalAmountTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalAmountTextBox.Location = new System.Drawing.Point(923, 371);
            this.totalAmountTextBox.Name = "totalAmountTextBox";
            this.totalAmountTextBox.ReadOnly = true;
            this.totalAmountTextBox.Size = new System.Drawing.Size(100, 22);
            this.totalAmountTextBox.TabIndex = 51;
            this.totalAmountTextBox.TabStop = false;
            this.totalAmountTextBox.Text = "0.0";
            this.totalAmountTextBox.TextChanged += new System.EventHandler(this.totalAmountTextBox_TextChanged);
            // 
            // totalQtyTextBox
            // 
            this.totalQtyTextBox.BackColor = System.Drawing.SystemColors.Info;
            this.totalQtyTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalQtyTextBox.Location = new System.Drawing.Point(726, 371);
            this.totalQtyTextBox.Name = "totalQtyTextBox";
            this.totalQtyTextBox.ReadOnly = true;
            this.totalQtyTextBox.Size = new System.Drawing.Size(63, 22);
            this.totalQtyTextBox.TabIndex = 49;
            this.totalQtyTextBox.TabStop = false;
            this.totalQtyTextBox.Text = "0.0";
            // 
            // totalPcsTextBox
            // 
            this.totalPcsTextBox.BackColor = System.Drawing.SystemColors.Info;
            this.totalPcsTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalPcsTextBox.Location = new System.Drawing.Point(625, 371);
            this.totalPcsTextBox.Name = "totalPcsTextBox";
            this.totalPcsTextBox.ReadOnly = true;
            this.totalPcsTextBox.Size = new System.Drawing.Size(61, 22);
            this.totalPcsTextBox.TabIndex = 47;
            this.totalPcsTextBox.TabStop = false;
            this.totalPcsTextBox.Text = "0";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.label15.Location = new System.Drawing.Point(507, 371);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(120, 21);
            this.label15.TabIndex = 45;
            this.label15.Text = "GRAND TOTAL";
            // 
            // companyNameTextBox
            // 
            this.companyNameTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.companyNameTextBox.ForeColor = System.Drawing.Color.Blue;
            this.companyNameTextBox.Location = new System.Drawing.Point(126, 18);
            this.companyNameTextBox.Name = "companyNameTextBox";
            this.companyNameTextBox.Size = new System.Drawing.Size(223, 26);
            this.companyNameTextBox.TabIndex = 0;
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(18, 82);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(98, 13);
            this.linkLabel1.TabIndex = 60;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "ADD NEW PARTY";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // additionalJobChallanCheckBox
            // 
            this.additionalJobChallanCheckBox.AutoSize = true;
            this.additionalJobChallanCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.additionalJobChallanCheckBox.ForeColor = System.Drawing.Color.RoyalBlue;
            this.additionalJobChallanCheckBox.Location = new System.Drawing.Point(674, 139);
            this.additionalJobChallanCheckBox.Name = "additionalJobChallanCheckBox";
            this.additionalJobChallanCheckBox.Size = new System.Drawing.Size(222, 20);
            this.additionalJobChallanCheckBox.TabIndex = 12;
            this.additionalJobChallanCheckBox.Text = "ADDITIONAL JOB CHALLAN";
            this.additionalJobChallanCheckBox.UseVisualStyleBackColor = true;
            this.additionalJobChallanCheckBox.CheckedChanged += new System.EventHandler(this.additionalJobChallanCheckBox_CheckedChanged);
            // 
            // jobChallanReceiveGridView
            // 
            this.jobChallanReceiveGridView.AllowUserToOrderColumns = true;
            this.jobChallanReceiveGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.jobChallanReceiveGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.refNoChallanReceiveComboBox,
            this.itemNameReceivveComboBox,
            this.bundlesReceiveTextBox,
            this.hsnCodeReceiveTextBox,
            this.jobTypeReceiveTextBox,
            this.multipleJobsOnItem,
            this.unitReceiveComboBox,
            this.pcsReceiveTextBox,
            this.qunatityReceiveTextBox,
            this.plainReceiveTextBox,
            this.secondReceiveTextBox,
            this.shortReceiveTextBox,
            this.freshReceiveTextBox,
            this.cutReceiveTextBox,
            this.rateReceiveTextBox,
            this.amountReceiveTextBox});
            this.jobChallanReceiveGridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.jobChallanReceiveGridView.Location = new System.Drawing.Point(21, 180);
            this.jobChallanReceiveGridView.Name = "jobChallanReceiveGridView";
            this.jobChallanReceiveGridView.Size = new System.Drawing.Size(1018, 175);
            this.jobChallanReceiveGridView.TabIndex = 12;
            this.jobChallanReceiveGridView.Visible = false;
            this.jobChallanReceiveGridView.CellValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.jobChallanReceiveGridView_CellValidated);
            this.jobChallanReceiveGridView.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.jobChallanReceiveGridView_CellValidating);
            this.jobChallanReceiveGridView.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.jobChallanReceiveGridView_EditingControlShowing);
            // 
            // jobReceiptDataGridView
            // 
            this.jobReceiptDataGridView.AllowUserToOrderColumns = true;
            this.jobReceiptDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.jobReceiptDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.refReceiptComboBox,
            this.itemNameReceiptComboBox,
            this.bundlesReceiptTextBox,
            this.hsncodeReceiptTextBox,
            this.jobTypeReceiptTextBox,
            this.unitReceiptComboBox,
            this.pcsReceiptTextBox,
            this.qtyReceiptTextBox,
            this.plainTextBox,
            this.secTextBox,
            this.shortTextBox,
            this.freshTextBox,
            this.cutReceiptTextBox,
            this.rateReceiptTextBox,
            this.amountReceiptTextBox});
            this.jobReceiptDataGridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.jobReceiptDataGridView.Location = new System.Drawing.Point(23, 180);
            this.jobReceiptDataGridView.Name = "jobReceiptDataGridView";
            this.jobReceiptDataGridView.Size = new System.Drawing.Size(1018, 175);
            this.jobReceiptDataGridView.TabIndex = 13;
            this.jobReceiptDataGridView.Visible = false;
            this.jobReceiptDataGridView.CellValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.jobReceiptDataGridView_CellValidated);
            this.jobReceiptDataGridView.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.jobReceiptDataGridView_CellValidating);
            this.jobReceiptDataGridView.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.jobReceiptDataGridView_EditingControlShowing);
            // 
            // refReceiptComboBox
            // 
            dataGridViewCellStyle17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.refReceiptComboBox.DefaultCellStyle = dataGridViewCellStyle17;
            this.refReceiptComboBox.HeaderText = "REF.";
            this.refReceiptComboBox.MaxDropDownItems = 100;
            this.refReceiptComboBox.Name = "refReceiptComboBox";
            this.refReceiptComboBox.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.refReceiptComboBox.Width = 50;
            // 
            // itemNameReceiptComboBox
            // 
            dataGridViewCellStyle18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.itemNameReceiptComboBox.DefaultCellStyle = dataGridViewCellStyle18;
            this.itemNameReceiptComboBox.HeaderText = "ITEM NAME";
            this.itemNameReceiptComboBox.MaxDropDownItems = 100;
            this.itemNameReceiptComboBox.Name = "itemNameReceiptComboBox";
            this.itemNameReceiptComboBox.Width = 130;
            // 
            // bundlesReceiptTextBox
            // 
            dataGridViewCellStyle19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bundlesReceiptTextBox.DefaultCellStyle = dataGridViewCellStyle19;
            this.bundlesReceiptTextBox.HeaderText = "BUNDLES";
            this.bundlesReceiptTextBox.Name = "bundlesReceiptTextBox";
            // 
            // hsncodeReceiptTextBox
            // 
            dataGridViewCellStyle20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hsncodeReceiptTextBox.DefaultCellStyle = dataGridViewCellStyle20;
            this.hsncodeReceiptTextBox.HeaderText = "HSN CODE";
            this.hsncodeReceiptTextBox.Name = "hsncodeReceiptTextBox";
            this.hsncodeReceiptTextBox.Width = 50;
            // 
            // jobTypeReceiptTextBox
            // 
            dataGridViewCellStyle21.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.jobTypeReceiptTextBox.DefaultCellStyle = dataGridViewCellStyle21;
            this.jobTypeReceiptTextBox.HeaderText = "JOB TYPE";
            this.jobTypeReceiptTextBox.Name = "jobTypeReceiptTextBox";
            this.jobTypeReceiptTextBox.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.jobTypeReceiptTextBox.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.jobTypeReceiptTextBox.Width = 99;
            // 
            // unitReceiptComboBox
            // 
            dataGridViewCellStyle22.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle22.NullValue = "PCS";
            this.unitReceiptComboBox.DefaultCellStyle = dataGridViewCellStyle22;
            this.unitReceiptComboBox.HeaderText = "UNIT";
            this.unitReceiptComboBox.Items.AddRange(new object[] {
            "PCS",
            "MTRS"});
            this.unitReceiptComboBox.Name = "unitReceiptComboBox";
            this.unitReceiptComboBox.Width = 70;
            // 
            // pcsReceiptTextBox
            // 
            dataGridViewCellStyle23.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pcsReceiptTextBox.DefaultCellStyle = dataGridViewCellStyle23;
            this.pcsReceiptTextBox.HeaderText = "PCS";
            this.pcsReceiptTextBox.Name = "pcsReceiptTextBox";
            this.pcsReceiptTextBox.Width = 50;
            // 
            // qtyReceiptTextBox
            // 
            dataGridViewCellStyle24.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.qtyReceiptTextBox.DefaultCellStyle = dataGridViewCellStyle24;
            this.qtyReceiptTextBox.HeaderText = "MTS/QTY.";
            this.qtyReceiptTextBox.Name = "qtyReceiptTextBox";
            this.qtyReceiptTextBox.Width = 50;
            // 
            // plainTextBox
            // 
            dataGridViewCellStyle25.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle25.NullValue = "0";
            this.plainTextBox.DefaultCellStyle = dataGridViewCellStyle25;
            this.plainTextBox.HeaderText = "PLAIN";
            this.plainTextBox.Name = "plainTextBox";
            this.plainTextBox.Width = 50;
            // 
            // secTextBox
            // 
            dataGridViewCellStyle26.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle26.NullValue = "0";
            this.secTextBox.DefaultCellStyle = dataGridViewCellStyle26;
            this.secTextBox.HeaderText = "SEC";
            this.secTextBox.Name = "secTextBox";
            this.secTextBox.Width = 50;
            // 
            // shortTextBox
            // 
            dataGridViewCellStyle27.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle27.NullValue = "0";
            this.shortTextBox.DefaultCellStyle = dataGridViewCellStyle27;
            this.shortTextBox.HeaderText = "SHORT";
            this.shortTextBox.Name = "shortTextBox";
            this.shortTextBox.Width = 50;
            // 
            // freshTextBox
            // 
            dataGridViewCellStyle28.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle28.NullValue = "0";
            this.freshTextBox.DefaultCellStyle = dataGridViewCellStyle28;
            this.freshTextBox.HeaderText = "FRESH";
            this.freshTextBox.Name = "freshTextBox";
            this.freshTextBox.Width = 50;
            // 
            // cutReceiptTextBox
            // 
            dataGridViewCellStyle29.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cutReceiptTextBox.DefaultCellStyle = dataGridViewCellStyle29;
            this.cutReceiptTextBox.HeaderText = "CUT";
            this.cutReceiptTextBox.Name = "cutReceiptTextBox";
            this.cutReceiptTextBox.ReadOnly = true;
            this.cutReceiptTextBox.Width = 50;
            // 
            // rateReceiptTextBox
            // 
            dataGridViewCellStyle30.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rateReceiptTextBox.DefaultCellStyle = dataGridViewCellStyle30;
            this.rateReceiptTextBox.HeaderText = "RATE";
            this.rateReceiptTextBox.Name = "rateReceiptTextBox";
            this.rateReceiptTextBox.Width = 50;
            // 
            // amountReceiptTextBox
            // 
            dataGridViewCellStyle31.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.amountReceiptTextBox.DefaultCellStyle = dataGridViewCellStyle31;
            this.amountReceiptTextBox.HeaderText = "AMOUNT";
            this.amountReceiptTextBox.Name = "amountReceiptTextBox";
            // 
            // jobDispatchDataGridView
            // 
            dataGridViewCellStyle32.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle32.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle32.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle32.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle32.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle32.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.jobDispatchDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle32;
            this.jobDispatchDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.jobDispatchDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.refNoComboBox,
            this.itemNameComboBox,
            this.bundlesTextBox,
            this.hsncTextBox,
            this.jobTypeTextBox,
            this.unitComboBox,
            this.pcsTextBox,
            this.cutTextBox,
            this.qtyTextBox,
            this.rateTextBox,
            this.amountTextBox});
            dataGridViewCellStyle44.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle44.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle44.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle44.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle44.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle44.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle44.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.jobDispatchDataGridView.DefaultCellStyle = dataGridViewCellStyle44;
            this.jobDispatchDataGridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.jobDispatchDataGridView.Location = new System.Drawing.Point(23, 180);
            this.jobDispatchDataGridView.Name = "jobDispatchDataGridView";
            dataGridViewCellStyle45.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle45.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle45.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle45.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle45.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle45.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle45.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.jobDispatchDataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle45;
            this.jobDispatchDataGridView.Size = new System.Drawing.Size(1018, 175);
            this.jobDispatchDataGridView.TabIndex = 12;
            this.jobDispatchDataGridView.CellValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.jobDispatchDataGridView_CellValidated);
            this.jobDispatchDataGridView.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.jobDispatchDataGridView_CellValidating);
            this.jobDispatchDataGridView.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.jobDispatchDataGridView_EditingControlShowing);
            // 
            // refNoComboBox
            // 
            dataGridViewCellStyle33.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.refNoComboBox.DefaultCellStyle = dataGridViewCellStyle33;
            this.refNoComboBox.HeaderText = "REF.";
            this.refNoComboBox.Name = "refNoComboBox";
            this.refNoComboBox.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.refNoComboBox.Width = 80;
            // 
            // itemNameComboBox
            // 
            dataGridViewCellStyle34.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.itemNameComboBox.DefaultCellStyle = dataGridViewCellStyle34;
            this.itemNameComboBox.HeaderText = "ITEM NAME";
            this.itemNameComboBox.MaxDropDownItems = 100;
            this.itemNameComboBox.Name = "itemNameComboBox";
            // 
            // bundlesTextBox
            // 
            dataGridViewCellStyle35.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bundlesTextBox.DefaultCellStyle = dataGridViewCellStyle35;
            this.bundlesTextBox.HeaderText = "BUNDLES";
            this.bundlesTextBox.Name = "bundlesTextBox";
            // 
            // hsncTextBox
            // 
            dataGridViewCellStyle36.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hsncTextBox.DefaultCellStyle = dataGridViewCellStyle36;
            this.hsncTextBox.HeaderText = "HSN CODE";
            this.hsncTextBox.Name = "hsncTextBox";
            // 
            // jobTypeTextBox
            // 
            dataGridViewCellStyle37.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.jobTypeTextBox.DefaultCellStyle = dataGridViewCellStyle37;
            this.jobTypeTextBox.HeaderText = "JOB TYPE";
            this.jobTypeTextBox.Name = "jobTypeTextBox";
            this.jobTypeTextBox.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.jobTypeTextBox.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // unitComboBox
            // 
            dataGridViewCellStyle38.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle38.NullValue = "PCS";
            this.unitComboBox.DefaultCellStyle = dataGridViewCellStyle38;
            this.unitComboBox.HeaderText = "UNIT";
            this.unitComboBox.Items.AddRange(new object[] {
            "PCS",
            "MTRS"});
            this.unitComboBox.Name = "unitComboBox";
            // 
            // pcsTextBox
            // 
            dataGridViewCellStyle39.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pcsTextBox.DefaultCellStyle = dataGridViewCellStyle39;
            this.pcsTextBox.HeaderText = "PCS";
            this.pcsTextBox.Name = "pcsTextBox";
            this.pcsTextBox.Width = 50;
            // 
            // cutTextBox
            // 
            dataGridViewCellStyle40.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle40.NullValue = "0";
            this.cutTextBox.DefaultCellStyle = dataGridViewCellStyle40;
            this.cutTextBox.HeaderText = "CUT";
            this.cutTextBox.Name = "cutTextBox";
            this.cutTextBox.Width = 50;
            // 
            // qtyTextBox
            // 
            dataGridViewCellStyle41.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle41.NullValue = "0.0";
            this.qtyTextBox.DefaultCellStyle = dataGridViewCellStyle41;
            this.qtyTextBox.HeaderText = "MTS/QTY.";
            this.qtyTextBox.Name = "qtyTextBox";
            // 
            // rateTextBox
            // 
            dataGridViewCellStyle42.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle42.NullValue = "0.0";
            this.rateTextBox.DefaultCellStyle = dataGridViewCellStyle42;
            this.rateTextBox.HeaderText = "RATE";
            this.rateTextBox.Name = "rateTextBox";
            // 
            // amountTextBox
            // 
            dataGridViewCellStyle43.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle43.NullValue = "0.0";
            this.amountTextBox.DefaultCellStyle = dataGridViewCellStyle43;
            this.amountTextBox.HeaderText = "AMOUNT";
            this.amountTextBox.Name = "amountTextBox";
            // 
            // dataGridViewComboBoxColumn1
            // 
            dataGridViewCellStyle46.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataGridViewComboBoxColumn1.DefaultCellStyle = dataGridViewCellStyle46;
            this.dataGridViewComboBoxColumn1.HeaderText = "REF.";
            this.dataGridViewComboBoxColumn1.Name = "dataGridViewComboBoxColumn1";
            this.dataGridViewComboBoxColumn1.Width = 50;
            // 
            // dataGridViewComboBoxColumn2
            // 
            dataGridViewCellStyle47.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataGridViewComboBoxColumn2.DefaultCellStyle = dataGridViewCellStyle47;
            this.dataGridViewComboBoxColumn2.HeaderText = "ITEM NAME";
            this.dataGridViewComboBoxColumn2.Name = "dataGridViewComboBoxColumn2";
            // 
            // dataGridViewComboBoxColumn3
            // 
            dataGridViewCellStyle48.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle48.NullValue = "PCS";
            this.dataGridViewComboBoxColumn3.DefaultCellStyle = dataGridViewCellStyle48;
            this.dataGridViewComboBoxColumn3.HeaderText = "UNIT";
            this.dataGridViewComboBoxColumn3.Items.AddRange(new object[] {
            "PCS",
            "MTRS"});
            this.dataGridViewComboBoxColumn3.Name = "dataGridViewComboBoxColumn3";
            // 
            // refNoChallanReceiveComboBox
            // 
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.refNoChallanReceiveComboBox.DefaultCellStyle = dataGridViewCellStyle1;
            this.refNoChallanReceiveComboBox.HeaderText = "REF.";
            this.refNoChallanReceiveComboBox.MaxDropDownItems = 100;
            this.refNoChallanReceiveComboBox.Name = "refNoChallanReceiveComboBox";
            this.refNoChallanReceiveComboBox.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.refNoChallanReceiveComboBox.Width = 50;
            // 
            // itemNameReceivveComboBox
            // 
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.itemNameReceivveComboBox.DefaultCellStyle = dataGridViewCellStyle2;
            this.itemNameReceivveComboBox.HeaderText = "ITEM NAME";
            this.itemNameReceivveComboBox.MaxDropDownItems = 100;
            this.itemNameReceivveComboBox.Name = "itemNameReceivveComboBox";
            this.itemNameReceivveComboBox.Width = 130;
            // 
            // bundlesReceiveTextBox
            // 
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bundlesReceiveTextBox.DefaultCellStyle = dataGridViewCellStyle3;
            this.bundlesReceiveTextBox.HeaderText = "BUNDLES";
            this.bundlesReceiveTextBox.Name = "bundlesReceiveTextBox";
            // 
            // hsnCodeReceiveTextBox
            // 
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hsnCodeReceiveTextBox.DefaultCellStyle = dataGridViewCellStyle4;
            this.hsnCodeReceiveTextBox.HeaderText = "HSN CODE";
            this.hsnCodeReceiveTextBox.Name = "hsnCodeReceiveTextBox";
            this.hsnCodeReceiveTextBox.Width = 50;
            // 
            // jobTypeReceiveTextBox
            // 
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.jobTypeReceiveTextBox.DefaultCellStyle = dataGridViewCellStyle5;
            this.jobTypeReceiveTextBox.HeaderText = "JOB TYPE";
            this.jobTypeReceiveTextBox.Name = "jobTypeReceiveTextBox";
            this.jobTypeReceiveTextBox.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.jobTypeReceiveTextBox.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.jobTypeReceiveTextBox.Width = 99;
            // 
            // multipleJobsOnItem
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.NullValue = false;
            this.multipleJobsOnItem.DefaultCellStyle = dataGridViewCellStyle6;
            this.multipleJobsOnItem.HeaderText = "MULTIPLE JOBS";
            this.multipleJobsOnItem.Name = "multipleJobsOnItem";
            this.multipleJobsOnItem.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.multipleJobsOnItem.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // unitReceiveComboBox
            // 
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.NullValue = "PCS";
            this.unitReceiveComboBox.DefaultCellStyle = dataGridViewCellStyle7;
            this.unitReceiveComboBox.HeaderText = "UNIT";
            this.unitReceiveComboBox.Items.AddRange(new object[] {
            "PCS",
            "MTRS"});
            this.unitReceiveComboBox.Name = "unitReceiveComboBox";
            this.unitReceiveComboBox.Width = 70;
            // 
            // pcsReceiveTextBox
            // 
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pcsReceiveTextBox.DefaultCellStyle = dataGridViewCellStyle8;
            this.pcsReceiveTextBox.HeaderText = "PCS";
            this.pcsReceiveTextBox.Name = "pcsReceiveTextBox";
            this.pcsReceiveTextBox.Width = 50;
            // 
            // qunatityReceiveTextBox
            // 
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.qunatityReceiveTextBox.DefaultCellStyle = dataGridViewCellStyle9;
            this.qunatityReceiveTextBox.HeaderText = "MTS/QTY.";
            this.qunatityReceiveTextBox.Name = "qunatityReceiveTextBox";
            this.qunatityReceiveTextBox.Width = 50;
            // 
            // plainReceiveTextBox
            // 
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.NullValue = "0";
            this.plainReceiveTextBox.DefaultCellStyle = dataGridViewCellStyle10;
            this.plainReceiveTextBox.HeaderText = "PLAIN";
            this.plainReceiveTextBox.Name = "plainReceiveTextBox";
            this.plainReceiveTextBox.Width = 50;
            // 
            // secondReceiveTextBox
            // 
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.NullValue = "0";
            this.secondReceiveTextBox.DefaultCellStyle = dataGridViewCellStyle11;
            this.secondReceiveTextBox.HeaderText = "SEC";
            this.secondReceiveTextBox.Name = "secondReceiveTextBox";
            this.secondReceiveTextBox.Width = 50;
            // 
            // shortReceiveTextBox
            // 
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.NullValue = "0";
            this.shortReceiveTextBox.DefaultCellStyle = dataGridViewCellStyle12;
            this.shortReceiveTextBox.HeaderText = "SHORT";
            this.shortReceiveTextBox.Name = "shortReceiveTextBox";
            this.shortReceiveTextBox.Width = 50;
            // 
            // freshReceiveTextBox
            // 
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle13.NullValue = "0";
            this.freshReceiveTextBox.DefaultCellStyle = dataGridViewCellStyle13;
            this.freshReceiveTextBox.HeaderText = "FRESH";
            this.freshReceiveTextBox.Name = "freshReceiveTextBox";
            this.freshReceiveTextBox.Width = 50;
            // 
            // cutReceiveTextBox
            // 
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cutReceiveTextBox.DefaultCellStyle = dataGridViewCellStyle14;
            this.cutReceiveTextBox.HeaderText = "CUT";
            this.cutReceiveTextBox.Name = "cutReceiveTextBox";
            this.cutReceiveTextBox.ReadOnly = true;
            this.cutReceiveTextBox.Width = 50;
            // 
            // rateReceiveTextBox
            // 
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rateReceiveTextBox.DefaultCellStyle = dataGridViewCellStyle15;
            this.rateReceiveTextBox.HeaderText = "RATE";
            this.rateReceiveTextBox.Name = "rateReceiveTextBox";
            this.rateReceiveTextBox.Width = 50;
            // 
            // amountReceiveTextBox
            // 
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.amountReceiveTextBox.DefaultCellStyle = dataGridViewCellStyle16;
            this.amountReceiveTextBox.HeaderText = "AMOUNT";
            this.amountReceiveTextBox.Name = "amountReceiveTextBox";
            // 
            // JobWorkWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.additionalJobChallanCheckBox);
            this.Controls.Add(this.jobChallanReceiveGridView);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.companyNameTextBox);
            this.Controls.Add(this.jobReceiptDataGridView);
            this.Controls.Add(this.billAmtTextBox);
            this.Controls.Add(this.taxValTextBox);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.detailsGroupBox);
            this.Controls.Add(this.remarkTextBox);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.viewBillButton);
            this.Controls.Add(this.deleteButton);
            this.Controls.Add(this.printButton);
            this.Controls.Add(this.addNewButton);
            this.Controls.Add(this.updateBill);
            this.Controls.Add(this.totalAmountTextBox);
            this.Controls.Add(this.totalQtyTextBox);
            this.Controls.Add(this.totalPcsTextBox);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.jobDispatchDataGridView);
            this.Controls.Add(this.lrNoTextBox);
            this.Controls.Add(this.dateTextBox);
            this.Controls.Add(this.partyGstnTextBox);
            this.Controls.Add(this.billNoTextBox);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.voucherNoTextBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.jobWorkTypeComboBox);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.billTypeLabel);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.gstTypeComboBox);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.workTypeComboBox);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.transportComboBox);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.brokerComboBox);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.partyNameComboBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label1);
            this.Name = "JobWorkWindow";
            this.Size = new System.Drawing.Size(1057, 580);
            this.Load += new System.EventHandler(this.JobWorkWindow_Load);
            this.detailsGroupBox.ResumeLayout(false);
            this.detailsGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.jobChallanReceiveGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.jobReceiptDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.jobDispatchDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox jobWorkTypeComboBox;
        private System.Windows.Forms.Label billTypeLabel;
        private System.Windows.Forms.TextBox voucherNoTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox dateTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox partyNameComboBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox partyGstnTextBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox billNoTextBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox gstTypeComboBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox brokerComboBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox workTypeComboBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox transportComboBox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox lrNoTextBox;
        private System.Windows.Forms.DataGridView jobDispatchDataGridView;
        private System.Windows.Forms.TextBox billAmtTextBox;
        private System.Windows.Forms.TextBox taxValTextBox;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.GroupBox detailsGroupBox;
        private System.Windows.Forms.TextBox igstAmtTextBox;
        private System.Windows.Forms.TextBox sgstAmtTextBox;
        private System.Windows.Forms.TextBox cgstAmtTextBox;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox igstRateTextBox;
        private System.Windows.Forms.TextBox sgstRateTextBox;
        private System.Windows.Forms.TextBox cgstRateTextBox;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox caseNoTextBox2;
        private System.Windows.Forms.TextBox caseNoTextBox;
        private System.Windows.Forms.TextBox addLessAmtFinalTextBox;
        private System.Windows.Forms.TextBox addLessAmtTextBox;
        private System.Windows.Forms.TextBox addLessTextBox;
        private System.Windows.Forms.TextBox discPerAmtFinalTextBox;
        private System.Windows.Forms.TextBox discPerAmtTextBox;
        private System.Windows.Forms.TextBox discPerTextBox;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox remarkTextBox;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button viewBillButton;
        private System.Windows.Forms.Button deleteButton;
        private System.Windows.Forms.Button printButton;
        private System.Windows.Forms.Button addNewButton;
        private System.Windows.Forms.Button updateBill;
        private System.Windows.Forms.TextBox totalAmountTextBox;
        private System.Windows.Forms.TextBox totalQtyTextBox;
        private System.Windows.Forms.TextBox totalPcsTextBox;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.DataGridView jobReceiptDataGridView;
        private System.Windows.Forms.TextBox companyNameTextBox;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn1;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn2;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn3;
        private System.Windows.Forms.TextBox tdsTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tdsAmountTextBox;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.DataGridView jobChallanReceiveGridView;
        private System.Windows.Forms.CheckBox additionalJobChallanCheckBox;
        private CustomControl.DataGridViewMultiColumnComboColumn refNoComboBox;
        private System.Windows.Forms.DataGridViewComboBoxColumn itemNameComboBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn bundlesTextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn hsncTextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn jobTypeTextBox;
        private System.Windows.Forms.DataGridViewComboBoxColumn unitComboBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn pcsTextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn cutTextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn qtyTextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn rateTextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn amountTextBox;
        private CustomControl.DataGridViewMultiColumnComboColumn refReceiptComboBox;
        private System.Windows.Forms.DataGridViewComboBoxColumn itemNameReceiptComboBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn bundlesReceiptTextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn hsncodeReceiptTextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn jobTypeReceiptTextBox;
        private System.Windows.Forms.DataGridViewComboBoxColumn unitReceiptComboBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn pcsReceiptTextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn qtyReceiptTextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn plainTextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn secTextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn shortTextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn freshTextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn cutReceiptTextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn rateReceiptTextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn amountReceiptTextBox;
        private CustomControl.DataGridViewMultiColumnComboColumn refNoChallanReceiveComboBox;
        private System.Windows.Forms.DataGridViewComboBoxColumn itemNameReceivveComboBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn bundlesReceiveTextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn hsnCodeReceiveTextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn jobTypeReceiveTextBox;
        private System.Windows.Forms.DataGridViewCheckBoxColumn multipleJobsOnItem;
        private System.Windows.Forms.DataGridViewComboBoxColumn unitReceiveComboBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn pcsReceiveTextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn qunatityReceiveTextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn plainReceiveTextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn secondReceiveTextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn shortReceiveTextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn freshReceiveTextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn cutReceiveTextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn rateReceiveTextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn amountReceiveTextBox;
    }
}
