﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Texo_Advance.DBItems;
using System.Globalization;

namespace Texo_Advance
{
    public partial class MillChallanWindow : Form
    {
        public List<string> takaList = new List<string>();
        
        string strQualityName = "";
        string strChallanNo = "0";
        long lAvailableTaka = 0;
        double dAvailableMtrs = 0.0;
        string strMasterName = "";
        MasterDBData DB = new MasterDBData();
        PartyDBData partyDB = new PartyDBData();
        List <millchallandispatchtaka_detail> challanTakaDetailList = new List<millchallandispatchtaka_detail>();
        public MillChallanWindow()
        {
            InitializeComponent();
            updateMillList();
            
        }
        public MillChallanWindow(string strQuaName,long lAvlTaka, double dAvlMtrs,long lChallanNo,long lJobProcessType)
        {
            InitializeComponent();
            fillProcessTypeList();
            strQualityName = strQuaName;
            lAvailableTaka = lAvlTaka;
            dAvailableMtrs = dAvlMtrs;
            challanNoTextBox.Text = Convert.ToString(lChallanNo);
            totalMtrsTextBox.Text = "0.0";
            totalTakaTextBox.Text = "0";
            rateTextBox.Text = "0.0";
            masterNameTextBox.Text = "";
            jobTypeComboBox.SelectedValue = lJobProcessType;
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            AccountManagerForm acc = new AccountManagerForm();
            acc.setDefaultAccountType(10);
            acc.ShowDialog();
            updateMillList();
            dateTextBox.Text = DateTime.Today.ToString("dd-MM-yyyy");
        }

        private void updateMillList()
        {
            var partyList = DB.getPartyDetails(10);
            if (partyList != null)
            {
                millNameComboBox.DisplayMember = "party_name";
                millNameComboBox.ValueMember = "partyid";
                millNameComboBox.DataSource = partyList;
            }
        }

        private void dateTextBox_TextChanged(object sender, EventArgs e)
        {
            DateTime dt;
            bool isValid = DateTime.TryParseExact(dateTextBox.Text, "dd-MM-yyyy", null, DateTimeStyles.None, out dt);
            if (!isValid)
            {
                MessageBox.Show("Please enter the valid date", "DATE", MessageBoxButtons.OK);
                dateTextBox.Text = DateTime.Today.ToString("dd-MM-yyyy");
                dateTextBox.Focus();
            }
        }

        private void setDefaultData()
        {/*
            long lSrNo = partyDB.getMillDispatchChallanNo();
            if (lSrNo == -1)
            {
                MessageBox.Show("Unable to retrieve Mill Challan Data", "Error", MessageBoxButtons.OK);
                return;
            }
            else if (lSrNo == 0)
            {
                challanNoTextBox.Text = "1";
            }
            else
                challanNoTextBox.Text = Convert.ToString(lSrNo + 1);
            */
        }

        private void MillChallanWindow_Load(object sender, EventArgs e)
        {
            updateMillList();
            //fillProcessTypeList();
            setDefaultData();
            dateTextBox.Text = DateTime.Today.ToString("dd-MM-yyyy");
            qualityTextBox.Text = strQualityName;
        }

        

        private void takaDetailsDataGridView_CellValidating_1(object sender, DataGridViewCellValidatingEventArgs e)
        {
            //double dChkMtrs = Convert.ToDouble(totalMtrsTextBox.Text) + Convert.ToDouble(takaDetailsDataGridView.Rows[takaDetailsDataGridView.CurrentRow.Index].Cells[0].EditedFormattedValue.ToString());
            double dChkMtrs = 0;
            for (int i = 0; i < takaDetailsDataGridView.RowCount - 1; i++)
            {
                double dMtrs = 0;
                bool bFlag = double.TryParse(takaDetailsDataGridView.Rows[i].Cells[0].EditedFormattedValue.ToString(), out dMtrs);
                //double dMtrs = Convert.ToDouble(takaDetailsDataGridView.Rows[i].Cells[0].EditedFormattedValue.ToString());
                dChkMtrs = dChkMtrs + dMtrs;
            }
            if (dChkMtrs > dAvailableMtrs)
            {
                MessageBox.Show("Taka Mtrs ecxeeding", "Error", MessageBoxButtons.OK);
                takaDetailsDataGridView.CurrentCell.Selected = true;
                e.Cancel = true;
                return;
            }
            
            if ((takaDetailsDataGridView.RowCount - 1) > lAvailableTaka)
            {
                MessageBox.Show("Number of taka exceeding", "Error", MessageBoxButtons.OK);
                takaDetailsDataGridView.CurrentCell.Selected = true;
                e.Cancel = true;
                return;
            }
            
            double dTakaMtrs = 0;
            for (int i = 0; i < takaDetailsDataGridView.RowCount-1; i++)
            {
                double dMtrs = 0;
                bool bFlag = double.TryParse(takaDetailsDataGridView.Rows[i].Cells[0].EditedFormattedValue.ToString(), out dMtrs);
                //double dMtrs = Convert.ToDouble(takaDetailsDataGridView.Rows[i].Cells[0].EditedFormattedValue.ToString());
                dTakaMtrs = dTakaMtrs + dMtrs;
            }
            totalMtrsTextBox.Text = Convert.ToString(dTakaMtrs);
            totalTakaTextBox.Text = Convert.ToString(takaDetailsDataGridView.RowCount - 1);
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            double dRate = 0;
            bool bValidRate = double.TryParse(rateTextBox.Text, out dRate);
            if (!bValidRate)
            {
                MessageBox.Show("INVALID RATE", "ERROR", MessageBoxButtons.OK);
                rateTextBox.Focus();
                return;
            }
            if (dRate <= 0)
            {
                MessageBox.Show("INVALID RATE", "ERROR", MessageBoxButtons.OK);
                rateTextBox.Focus();
                return;
            }
            if ((takaDetailsDataGridView.RowCount - 1) == 0)
            {
                MessageBox.Show("Please enter taka details", "TAKA WINDOW", MessageBoxButtons.OK);
                return;
            }
            takaList.Clear();
            strChallanNo = challanNoTextBox.Text;
            strMasterName = masterNameTextBox.Text;
            
            for(int i = 0;i < takaDetailsDataGridView.RowCount - 1;i++)
            {
                string strMtrs = takaDetailsDataGridView.Rows[i].Cells[0].EditedFormattedValue.ToString();
                
                //dMtrs = Convert.ToDouble(takaDetailsDataGridView.Rows[i].Cells[0].EditedFormattedValue.ToString());
                takaList.Add(strMtrs);

            }
            MessageBox.Show("Taka Details Saved", "Taka Details", MessageBoxButtons.OK);
        }
        public string getMasterName()
        {
            return strMasterName;
        }
        public string getChallanNo()
        {
            return strChallanNo;
        }
        public long getMillID()
        {
            return (long) millNameComboBox.SelectedValue;
        }

        private void rateTextBox_Validating(object sender, CancelEventArgs e)
        {
            double dRate = 0;
            bool bValidRate = double.TryParse(rateTextBox.Text,out dRate);
            if(!bValidRate)
            {
                MessageBox.Show("INVALID RATE", "ERROR", MessageBoxButtons.OK);
                rateTextBox.Focus();
                return;
            }
            if(dRate<=0)
            {
                MessageBox.Show("INVALID RATE", "ERROR", MessageBoxButtons.OK);
                rateTextBox.Focus();
                return;
            }

        }
        private void fillProcessTypeList()
        {
            var millProcessList = DB.getMillProcessType();
            if (millProcessList == null)
                return;
            jobTypeComboBox.DisplayMember = "type";
            jobTypeComboBox.ValueMember = "millProcess_id";
            jobTypeComboBox.DataSource = millProcessList;

        }

        public string getRemarkData()
        {
            return remarkTextBox.Text;
        }
        public double getJobRate()
        {
            return Convert.ToDouble(rateTextBox.Text);
        }
        public string getDate()
        {
            return dateTextBox.Text;
        }
        public void setChallanDataInControl(milldispatchchallan_detail challanDetail,List<millchallandispatchtaka_detail> takaDetail)
        {
            takaDetailsDataGridView.Rows.Clear();
            millNameComboBox.SelectedValue = challanDetail.mill_id;
            challanNoTextBox.Text = Convert.ToString(challanDetail.challanNo);
            greyquality_details greyQuality = DB.GetGreyquality((long)challanDetail.quality_id);
            if(greyQuality == null)
            {
                MessageBox.Show("Unable to fetch grey quality","ERROR",MessageBoxButtons.OK);
                return;
            }
            strQualityName = greyQuality.quality_name;
            remarkTextBox.Text = challanDetail.remark;
            rateTextBox.Text = Convert.ToString(challanDetail.rate);
            jobTypeComboBox.SelectedValue = Convert.ToInt64(challanDetail.processType);
            masterNameTextBox.Text = challanDetail.master;
            for(int i = 0;i < takaDetail.Count;i++)
            {
                takaDetailsDataGridView.Rows.Add();
                takaDetailsDataGridView.Rows[i].Cells[0].Value = Convert.ToString(takaDetail[i].greyMtrs);
            }
            dAvailableMtrs = (double)challanDetail.totalMtrs;
            lAvailableTaka = (long)challanDetail.totalTakas;
            totalMtrsTextBox.Text = Convert.ToString(dAvailableMtrs);
            totalTakaTextBox.Text = Convert.ToString(lAvailableTaka);
            DateTime dt, dt1;
            bool bFlag = DateTime.TryParse(challanDetail.date.ToString(), out dt);
            if (bFlag)
            {
                dt1 = dt.Date;
                //DateTime.Parse(dt1.ToString()).ToString("dd-MM-yyyy");
                dateTextBox.Text = dt1.ToString("dd-MM-yyyy");
            }
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
