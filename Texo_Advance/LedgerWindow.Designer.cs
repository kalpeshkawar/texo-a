﻿namespace Texo_Advance
{
    partial class LedgerWindow
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LedgerWindow));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.companyNameTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.fromDateTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.toDateTextBox = new System.Windows.Forms.TextBox();
            this.ledgerButton = new System.Windows.Forms.Button();
            this.closeButton = new System.Windows.Forms.Button();
            this.partyNameComboBox = new JTG.ColumnComboBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Blue;
            this.label1.Location = new System.Drawing.Point(236, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(255, 22);
            this.label1.TabIndex = 0;
            this.label1.Text = "ACCOUNT LEDGER WINDOW";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.label2.Location = new System.Drawing.Point(41, 95);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "COMPANY:";
            // 
            // companyNameTextBox
            // 
            this.companyNameTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.companyNameTextBox.Location = new System.Drawing.Point(139, 95);
            this.companyNameTextBox.Name = "companyNameTextBox";
            this.companyNameTextBox.ReadOnly = true;
            this.companyNameTextBox.Size = new System.Drawing.Size(283, 22);
            this.companyNameTextBox.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.label3.Location = new System.Drawing.Point(41, 144);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 16);
            this.label3.TabIndex = 1;
            this.label3.Text = "PARTY:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.label4.Location = new System.Drawing.Point(41, 195);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(100, 16);
            this.label4.TabIndex = 1;
            this.label4.Text = "FROM DATE:";
            // 
            // fromDateTextBox
            // 
            this.fromDateTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fromDateTextBox.Location = new System.Drawing.Point(140, 194);
            this.fromDateTextBox.Name = "fromDateTextBox";
            this.fromDateTextBox.Size = new System.Drawing.Size(114, 22);
            this.fromDateTextBox.TabIndex = 2;
            this.fromDateTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.fromDateTextBox_Validating);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.MediumVioletRed;
            this.label5.Location = new System.Drawing.Point(262, 195);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(78, 16);
            this.label5.TabIndex = 1;
            this.label5.Text = "TO DATE:";
            // 
            // toDateTextBox
            // 
            this.toDateTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toDateTextBox.Location = new System.Drawing.Point(343, 192);
            this.toDateTextBox.Name = "toDateTextBox";
            this.toDateTextBox.Size = new System.Drawing.Size(112, 22);
            this.toDateTextBox.TabIndex = 2;
            this.toDateTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.toDateTextBox_Validating);
            // 
            // ledgerButton
            // 
            this.ledgerButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ledgerButton.ForeColor = System.Drawing.Color.Blue;
            this.ledgerButton.Location = new System.Drawing.Point(109, 342);
            this.ledgerButton.Name = "ledgerButton";
            this.ledgerButton.Size = new System.Drawing.Size(89, 36);
            this.ledgerButton.TabIndex = 4;
            this.ledgerButton.Text = "LEDGER";
            this.ledgerButton.UseVisualStyleBackColor = true;
            this.ledgerButton.Click += new System.EventHandler(this.ledgerButton_Click);
            // 
            // closeButton
            // 
            this.closeButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.closeButton.ForeColor = System.Drawing.Color.Blue;
            this.closeButton.Location = new System.Drawing.Point(235, 342);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(89, 36);
            this.closeButton.TabIndex = 4;
            this.closeButton.Text = "CLOSE";
            this.closeButton.UseVisualStyleBackColor = true;
            // 
            // partyNameComboBox
            // 
            this.partyNameComboBox.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.partyNameComboBox.DropDownWidth = 17;
            this.partyNameComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.partyNameComboBox.FormattingEnabled = true;
            this.partyNameComboBox.Location = new System.Drawing.Point(139, 144);
            this.partyNameComboBox.Name = "partyNameComboBox";
            this.partyNameComboBox.Size = new System.Drawing.Size(283, 23);
            this.partyNameComboBox.TabIndex = 3;
            this.partyNameComboBox.ViewColumn = 0;
            // 
            // LedgerWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.closeButton);
            this.Controls.Add(this.ledgerButton);
            this.Controls.Add(this.partyNameComboBox);
            this.Controls.Add(this.toDateTextBox);
            this.Controls.Add(this.fromDateTextBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.companyNameTextBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "LedgerWindow";
            this.Size = new System.Drawing.Size(915, 484);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox companyNameTextBox;
        private System.Windows.Forms.Label label3;
        private JTG.ColumnComboBox partyNameComboBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox fromDateTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox toDateTextBox;
        private System.Windows.Forms.Button ledgerButton;
        private System.Windows.Forms.Button closeButton;
    }
}
