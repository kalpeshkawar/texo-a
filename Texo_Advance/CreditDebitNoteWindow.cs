﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using System.Windows.Forms;
using Texo_Advance.DBItems;

namespace Texo_Advance
{
    public partial class CreditDebitNoteWindow : UserControl
    {
        string strCompanyGstn = "";
        MasterDBData DB = new MasterDBData();
        PartyDBData partyDB = new PartyDBData();
        List<party_details> partyAccountList;
        List<sales_details> outstandingSalesBills;
        List<purchase_details> outstandingPurchaseBills;
        creditdebitnote_details cdNoteDetail;
        public CreditDebitNoteWindow(string companyName,string companyGstn)
        {
            InitializeComponent();
            compantNameTextBox.Text = companyName;
            strCompanyGstn = companyGstn; 
        }

        private void CreditDebitNoteWindow_Load(object sender, EventArgs e)
        {
            dateTextBox.Text = DateTime.Today.ToString("dd-MM-yyyy");
            fillStateCodeList();
            fillCreditDebitNoteTypeList();
            fillGstTypeList();
            fillAccountList();
            setDefaultData();
            addNewButton.Enabled = true;
            updateButton.Enabled = false;
            deleteButton.Enabled = false;
        }

        private void fillCreditDebitNoteTypeList()
        {
            var cdNoteTypeList = DB.getCreditDebitNoteTypeList();
            if(cdNoteTypeList != null)
            {
                typeComboBox.DisplayMember = "cdNoteTypeName";
                typeComboBox.ValueMember = "creditDebitNoteType_id";
                typeComboBox.DataSource = cdNoteTypeList;
            }
        }

        private void fillPartyList()
        {
            partyNameComboBox.Text = "";
            int iColumnCount = 5;
            if ((long)typeComboBox.SelectedValue == 1 || (long)typeComboBox.SelectedValue == 3)
                partyAccountList = DB.getPartyDetails(1);
            else
                partyAccountList = DB.getPartyDetails(2);

            DataTable myDataTable = new DataTable("ParentTable");
            // Declare variables for DataColumn and DataRow objects.
            DataColumn myDataColumn;
            DataRow myDataRow;

            //Add some coulumns
            for (int index = 0; index < iColumnCount; index++)
            {
                myDataColumn = new DataColumn();
                myDataColumn.DataType = typeof(string);
                if (index == 0)
                    myDataColumn.ColumnName = "NAME";
                else if (index == 1)
                    myDataColumn.ColumnName = "ACCOUNT_TYPE";
                else if (index == 2)
                    myDataColumn.ColumnName = "ADDRESS";
                else if (index == 3)
                    myDataColumn.ColumnName = "CITY";
                else if (index == 4)
                    myDataColumn.ColumnName = "PARTY_ID";
                //myDataColumn.Caption = "ParentItem";
                myDataColumn.AutoIncrement = false;
                myDataColumn.ReadOnly = false;
                myDataColumn.Unique = false;
                // Add the Column to the DataColumnCollection.
                myDataTable.Columns.Add(myDataColumn);
            }

            //Add some more rows
            for (int index = 0; index < partyAccountList.Count; index++)
            {
                myDataRow = myDataTable.NewRow();
                myDataRow[0] = partyAccountList[index].party_name;
                myDataRow[1] = DB.getAccountTypeName(partyAccountList[index].account_typeid);
                myDataRow[2] = partyAccountList[index].address + partyAccountList[index].address2;
                myDataRow[3] = DB.getCityName((long)partyAccountList[index].city_id);
                myDataRow[4] = partyAccountList[index].partyid;
                myDataTable.Rows.Add(myDataRow);
            }


            //Now set the Data of the ColumnComboBox
            partyNameComboBox.ValueMember = "PARTY_ID";
            partyNameComboBox.Data = myDataTable;
            //Set which row will be displayed in the text box
            //If you set this to a column that isn't displayed then the suggesting functionality won't work.
            partyNameComboBox.ColumnSpacing = 30;
            partyNameComboBox.ViewColumn = 0;

            partyNameComboBox.Columns[0].Width = 175;
            partyNameComboBox.Columns[1].Width = 125;
            partyNameComboBox.Columns[2].Width = 125;
            partyNameComboBox.Columns[3].Width = 75;
            //Set a few columns to not be shown
            partyNameComboBox.Columns[4].Display = false;
            partyNameComboBox.SelectedIndex = -1;


        }

        private void typeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            setDefaultData();
            resetAllData();
            fillPartyList();
            if ((long)typeComboBox.SelectedValue == 1 || (long)typeComboBox.SelectedValue == 3)
                updateOutstandingSalesBill();
            else
                updateOutstandingPurchaseBill();
        }

        private void fillStateCodeList()
        {
            var stateCodeList = DB.getStateCodeList();
            if (stateCodeList != null)
            {
                stateCodeComboBox.DisplayMember = "code";
                stateCodeComboBox.ValueMember = "statecode_id";
                stateCodeComboBox.DataSource = stateCodeList;
            }
        }

        private void fillGstTypeList()
        {
            gstTypeComboBox.SelectedIndex = -1;
            var gstTypeList = DB.getGstTypeList();
            if (gstTypeList != null)
            {
                gstTypeComboBox.DisplayMember = "type";
                gstTypeComboBox.ValueMember = "gsttype_id";
                gstTypeComboBox.DataSource = gstTypeList;
            }

        }

        private void fillAccountList()
        {
            var partyAccountList = DB.getPartyAccountListForPassBook();
            if(partyAccountList != null)
            {
                accountComboBox.DisplayMember = "party_name";
                accountComboBox.ValueMember = "partyid";
                accountComboBox.DataSource = partyAccountList;
            }
        }

        private void setDefaultData()
        {
            long lMaxVoucherNo = partyDB.getCreditDebitNoteVoucherNo(Convert.ToInt64(typeComboBox.SelectedValue));
            if (lMaxVoucherNo != 0)
            {
                voucherNoTextBox.Text = Convert.ToString(lMaxVoucherNo + 1);
            }
            else
            {
                voucherNoTextBox.Text = "1";
            }
        }

        private void partyNameComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (partyNameComboBox.Items.Count <= 0 || partyNameComboBox.SelectedIndex == -1)
                return;
            string partyID = partyNameComboBox["PARTY_ID"].ToString();
            long lPartyID = Convert.ToInt64(partyID);
            var partyDetail = partyAccountList.Where(x => x.partyid == lPartyID).SingleOrDefault();
            if (partyDetail == null)
                return;
            string gstn = partyDetail.gst;
            if (gstn.Length > 0)
            {
                string stateCode = gstn.Substring(0, 2);
                int indexForStateCode = stateCodeComboBox.FindString(stateCode);
                if (indexForStateCode != -1)
                {
                    stateCodeComboBox.SelectedIndex = indexForStateCode;
                    stateCodeComboBox.Enabled = false;
                    gstTypeComboBox.Enabled = false;

                }

            }
            else
            {
                stateCodeComboBox.Enabled = true;
                gstTypeComboBox.Enabled = true;
            }
            if ((long)typeComboBox.SelectedValue == 1 || (long)typeComboBox.SelectedValue == 3)
                updateOutstandingSalesBill();
            else
                updateOutstandingPurchaseBill();
        }

        private void gstTypeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (gstTypeComboBox.SelectedIndex == 0)
            {
                cgstPerTextBox.Text = "5";
                sgstPerTextBox.Text = "0";
                sgstAmtTextBox.Enabled = false;
                sgstPerTextBox.Enabled = false;
                label14.Text = "IGST %";
                //UpdateIgstData();
            }
            else if (gstTypeComboBox.SelectedIndex == 1)
            {
                cgstPerTextBox.Text = "2.5";
                sgstPerTextBox.Text = "2.5";
                sgstAmtTextBox.Enabled = true;
                sgstPerTextBox.Enabled = true;
                label14.Text = "CGST %";
                //UpdateCgstSgstData();
            }
        }

        private void stateCodeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strCompanyStationCode = strCompanyGstn.Substring(0, 2);
            if (stateCodeComboBox.Text == strCompanyStationCode)
                gstTypeComboBox.SelectedValue = Convert.ToInt64(2);
            else
                gstTypeComboBox.SelectedValue = Convert.ToInt64(1);
        }

        private void accountComboBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && accountComboBox.SelectedIndex == -1)
            {
                AccountManagerForm acc = new AccountManagerForm();
                acc.setDefaultAccountType(1);
                acc.setDefaultName(accountComboBox.Text);
                acc.ShowDialog();
                fillAccountList();
            }
        }

        private void addNewButton_Click(object sender, EventArgs e)
        {
            if(partyNameComboBox.SelectedIndex == -1)
            {
                MessageBox.Show("Please Select the Party A/C", "ERROR", MessageBoxButtons.OK);
                partyNameComboBox.Focus();
                return;
            }
            else if(accountComboBox.SelectedIndex == -1)
            {
                MessageBox.Show("Please Select the Account", "ERROR", MessageBoxButtons.OK);
                accountComboBox.Focus();
                return;
            }
            else if(noteNoTextBox.Text.Length <=0 || noteNoTextBox.Text == "0")
            {
                MessageBox.Show("Please Enter the NoteNo.", "ERROR", MessageBoxButtons.OK);
                noteNoTextBox.Focus();
                return;
            }
            else if (refBillNoComboBox.SelectedIndex == -1)
            {
                MessageBox.Show("Please Enter the NoteNo.", "ERROR", MessageBoxButtons.OK);
                refBillNoComboBox.Focus();
                return;
            }
            else if(netAmountTextBox.Text == "0" || netAmountTextBox.Text.Length <= 0)
            {
                MessageBox.Show("Net Amount is Invalid", "ERROR", MessageBoxButtons.OK);
                //refBillNoComboBox.Focus();
                return;
            }
            creditdebitnote_details cdNoteDetails = new creditdebitnote_details();
            cdNoteDetails.accountType_id = Convert.ToInt64(accountComboBox.SelectedValue);
            //cdNoteDetails.cgstAmount = Convert.ToDouble(cgstAmtTextBox.Text);
            //cdNoteDetails.cgstPercentage = Convert.ToDouble(cgstPerTextBox.Text);
            cdNoteDetails.cdNoteType_id = Convert.ToInt64(typeComboBox.SelectedValue);
            cdNoteDetails.date = DateTime.ParseExact(dateTextBox.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            cdNoteDetails.grossAmount = Convert.ToDouble(grossAmountTextBox.Text);
            cdNoteDetails.remark = remarkTextBox.Text;
            cdNoteDetails.refBillDate = DateTime.ParseExact(refBillDateTextBox.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            cdNoteDetails.refBillNo = refBillNoComboBox.Text;
            cdNoteDetails.noteNo = noteNoTextBox.Text;
            cdNoteDetails.netAmount = Convert.ToDouble(netAmountTextBox.Text);
            cdNoteDetails.party_id = Convert.ToInt64(partyNameComboBox["PARTY_ID"].ToString());
            
            if ((long)gstTypeComboBox.SelectedValue == 1)
            {
                cdNoteDetails.igstAmount = Convert.ToDouble(cgstAmtTextBox.Text);
                cdNoteDetails.igstPercentage = Convert.ToDouble(cgstPerTextBox.Text);
                cdNoteDetails.cgstAmount = 0;
                cdNoteDetails.cgstPercentage = 0;
                cdNoteDetails.sgstAmount = 0;
                cdNoteDetails.sgstPercentage = 0;
            }
            else if((long)gstTypeComboBox.SelectedValue == 2)
            {
                cdNoteDetails.igstAmount = 0;
                cdNoteDetails.igstPercentage = 0;
                cdNoteDetails.cgstAmount = Convert.ToDouble(cgstAmtTextBox.Text);
                cdNoteDetails.cgstPercentage = Convert.ToDouble(cgstPerTextBox.Text);
                cdNoteDetails.sgstAmount = Convert.ToDouble(sgstAmtTextBox.Text);
                cdNoteDetails.sgstPercentage = Convert.ToDouble(sgstPerTextBox.Text);
            }
            cdNoteDetails.voucherNo = Convert.ToInt64(voucherNoTextBox.Text);
            bool bFlag = partyDB.addCDNoteDetails(cdNoteDetails);
            if(bFlag)
            {
                MessageBox.Show("Voucher Added Successflly", "SUCCESS", MessageBoxButtons.OK);
                setDefaultData();
                resetAllData();
            }
            else
            {
                MessageBox.Show("Voucher Not Added Successflly", "ERROR", MessageBoxButtons.OK);
            }
        }

        private void viewButton_Click(object sender, EventArgs e)
        {
            long lVoucherNo = 0;
            long.TryParse(voucherNoTextBox.Text, out lVoucherNo);
            cdNoteDetail = partyDB.getCDNoteDetail(lVoucherNo,Convert.ToInt64(typeComboBox.SelectedValue));
            if (cdNoteDetail == null)
                return;
            setPartySelectedValue(Convert.ToInt64(cdNoteDetail.party_id));
            remarkTextBox.Text = cdNoteDetail.remark;
            DateTime dt, dt1;
            bool bFlag = DateTime.TryParse(cdNoteDetail.date.ToString(), out dt);
            if (bFlag)
            {
                dt1 = dt.Date;
                //DateTime.Parse(dt1.ToString()).ToString("dd-MM-yyyy");
                dateTextBox.Text = dt1.ToString("dd-MM-yyyy");
            }
            noteNoTextBox.Text = cdNoteDetail.noteNo;
            grossAmountTextBox.Text = Convert.ToString(cdNoteDetail.grossAmount);
            if((long)gstTypeComboBox.SelectedValue == 1)
            {
                cgstPerTextBox.Text = Convert.ToString(cdNoteDetail.igstPercentage);
                cgstAmtTextBox.Text = Convert.ToString(cdNoteDetail.igstAmount);
            }
            else
            {
                cgstPerTextBox.Text = Convert.ToString(cdNoteDetail.cgstPercentage);
                cgstAmtTextBox.Text = Convert.ToString(cdNoteDetail.cgstAmount);
                sgstPerTextBox.Text = Convert.ToString(cdNoteDetail.sgstPercentage);
                sgstAmtTextBox.Text = Convert.ToString(cdNoteDetail.sgstAmount);
            }
            
            netAmountTextBox.Text = Convert.ToString(cdNoteDetail.netAmount);
            
            refBillNoComboBox.Items.Add(cdNoteDetail.refBillNo);
            refBillNoComboBox.SelectedIndex = 0;
            bFlag = DateTime.TryParse(cdNoteDetail.refBillDate.ToString(), out dt);
            if (bFlag)
            {
                dt1 = dt.Date;
                //DateTime.Parse(dt1.ToString()).ToString("dd-MM-yyyy");
                refBillDateTextBox.Text = dt1.ToString("dd-MM-yyyy");
            }
            accountComboBox.SelectedValue = cdNoteDetail.accountType_id;
            addNewButton.Enabled = false;
            updateButton.Enabled = true;
            deleteButton.Enabled = true;
        }

        private void updateButton_Click(object sender, EventArgs e)
        {
            if (partyNameComboBox.SelectedIndex == -1)
            {
                MessageBox.Show("Please Select the Party A/C", "ERROR", MessageBoxButtons.OK);
                partyNameComboBox.Focus();
                return;
            }
            else if (accountComboBox.SelectedIndex == -1)
            {
                MessageBox.Show("Please Select the Account", "ERROR", MessageBoxButtons.OK);
                accountComboBox.Focus();
                return;
            }
            else if (noteNoTextBox.Text.Length <= 0 || noteNoTextBox.Text == "0")
            {
                MessageBox.Show("Please Enter the NoteNo.", "ERROR", MessageBoxButtons.OK);
                noteNoTextBox.Focus();
                return;
            }
            else if (refBillNoComboBox.SelectedIndex == -1)
            {
                MessageBox.Show("Please Enter the NoteNo.", "ERROR", MessageBoxButtons.OK);
                refBillNoComboBox.Focus();
                return;
            }
            else if (netAmountTextBox.Text == "0" || netAmountTextBox.Text.Length <= 0)
            {
                MessageBox.Show("Net Amount is Invalid", "ERROR", MessageBoxButtons.OK);
                //refBillNoComboBox.Focus();
                return;
            }

            cdNoteDetail.accountType_id = Convert.ToInt64(accountComboBox.SelectedValue);
            //cdNoteDetails.cgstAmount = Convert.ToDouble(cgstAmtTextBox.Text);
            //cdNoteDetails.cgstPercentage = Convert.ToDouble(cgstPerTextBox.Text);
            cdNoteDetail.cdNoteType_id = Convert.ToInt64(typeComboBox.SelectedValue);
            cdNoteDetail.date = DateTime.ParseExact(dateTextBox.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            cdNoteDetail.grossAmount = Convert.ToDouble(grossAmountTextBox.Text);
            cdNoteDetail.remark = remarkTextBox.Text;
            cdNoteDetail.refBillDate = DateTime.ParseExact(refBillDateTextBox.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            cdNoteDetail.refBillNo = refBillNoComboBox.Text;
            cdNoteDetail.noteNo = noteNoTextBox.Text;
            cdNoteDetail.netAmount = Convert.ToDouble(netAmountTextBox.Text);
            cdNoteDetail.party_id = Convert.ToInt64(partyNameComboBox["PARTY_ID"].ToString());

            if ((long)gstTypeComboBox.SelectedValue == 1)
            {
                cdNoteDetail.igstAmount = Convert.ToDouble(cgstAmtTextBox.Text);
                cdNoteDetail.igstPercentage = Convert.ToDouble(cgstPerTextBox.Text);
                cdNoteDetail.cgstAmount = 0;
                cdNoteDetail.cgstPercentage = 0;
                cdNoteDetail.sgstAmount = 0;
                cdNoteDetail.sgstPercentage = 0;
            }
            else if ((long)gstTypeComboBox.SelectedValue == 2)
            {
                cdNoteDetail.igstAmount = 0;
                cdNoteDetail.igstPercentage = 0;
                cdNoteDetail.cgstAmount = Convert.ToDouble(cgstAmtTextBox.Text);
                cdNoteDetail.cgstPercentage = Convert.ToDouble(cgstPerTextBox.Text);
                cdNoteDetail.sgstAmount = Convert.ToDouble(sgstAmtTextBox.Text);
                cdNoteDetail.sgstPercentage = Convert.ToDouble(sgstPerTextBox.Text);
            }
            //cdNoteDetail.voucherNo = Convert.ToInt64(voucherNoTextBox.Text);
            bool bFlag = partyDB.addCDNoteDetails(cdNoteDetail);
            if (bFlag)
            {
                MessageBox.Show("Voucher Updated Successflly", "SUCCESS", MessageBoxButtons.OK);
                setDefaultData();
                resetAllData();
            }
            else
            {
                MessageBox.Show("Voucher Not Updated Successflly", "ERROR", MessageBoxButtons.OK);
            }
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            long lVoucherNo = 0;
            long.TryParse(voucherNoTextBox.Text, out lVoucherNo);
            if (lVoucherNo == 0)
            {
                MessageBox.Show("Please Enter Valid Voucher No", "VOUCHER", MessageBoxButtons.OK);
                voucherNoTextBox.Focus();
                return;
            }
            bool bVoucherDeleted = partyDB.deleteCDNoteVoucher(lVoucherNo,Convert.ToInt64(typeComboBox.SelectedValue));
            if (bVoucherDeleted)
            {
                MessageBox.Show("Voucher Deleted", "Credit/Debit Note", MessageBoxButtons.OK);
                resetAllData();
                setDefaultData();
            }
            else
                MessageBox.Show("Voucher Not Deleted", "Credit/Debit Note", MessageBoxButtons.OK);
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            this.Parent.Controls.Remove(this);
        }

        private void resetAllData()
        {
            sgstAmtTextBox.Text = "0";
            sgstPerTextBox.Text = "2.5";
            cgstAmtTextBox.Text = "0";
            cgstPerTextBox.Text = "2.5";
            remarkTextBox.Clear();
            grossAmountTextBox.Text = "0";
            netAmountTextBox.Text = "0";
            refBillNoComboBox.Items.Clear();
            refBillNoComboBox.SelectedIndex = -1;
            noteNoTextBox.Clear();
            dateTextBox.Text = DateTime.Today.ToString("dd-MM-yyyy");
            addNewButton.Enabled = true;
            updateButton.Enabled = false;
            deleteButton.Enabled = false;
        }

        private void updateOutstandingSalesBill()
        {
            if (partyNameComboBox.SelectedIndex != -1)
            {
                refBillNoComboBox.Items.Clear();
                string partyID = partyNameComboBox["PARTY_ID"].ToString();
                long lPartyID = Convert.ToInt64(partyID);
                outstandingSalesBills = partyDB.getOutstandingSalesBillDetails(lPartyID);
                if (outstandingSalesBills == null)
                    return;
                
                for(int i = 0;i < outstandingSalesBills.Count;i++)
                {
                    refBillNoComboBox.Items.Add(outstandingSalesBills[i].billno);
                }
            }
        }

        private void updateOutstandingPurchaseBill()
        {
            if (partyNameComboBox.SelectedIndex != -1)
            {
                refBillNoComboBox.Items.Clear();
                string partyID = partyNameComboBox["PARTY_ID"].ToString();
                long lPartyID = Convert.ToInt64(partyID);
                outstandingPurchaseBills = partyDB.getOutstandingPurchaseBillDetails(lPartyID);
                if (outstandingPurchaseBills == null)
                    return;

                for (int i = 0; i < outstandingPurchaseBills.Count; i++)
                {
                    refBillNoComboBox.Items.Add(outstandingPurchaseBills[i].billno);
                }
            }
        }
        private void refBillNoComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(refBillNoComboBox.Items.Count>0)
            {

                DateTime billDate;
                if ((long)typeComboBox.SelectedValue == 1 || (long)typeComboBox.SelectedValue == 3)
                    billDate = (DateTime)outstandingSalesBills.Where(x => x.billno == refBillNoComboBox.Text).Select(x=>x.date).Single();
                else
                    billDate = (DateTime)outstandingPurchaseBills.Where(x => x.billno == refBillNoComboBox.Text).Select(x => x.date).Single();
                DateTime dt, dt1;
                bool bFlag = DateTime.TryParse(billDate.ToString(), out dt);
                if (bFlag)
                {
                    dt1 = dt.Date;
                    //DateTime.Parse(dt1.ToString()).ToString("dd-MM-yyyy");
                    refBillDateTextBox.Text = dt1.ToString("dd-MM-yyyy");
                }
            }
        }

        private void grossAmountTextBox_TextChanged(object sender, EventArgs e)
        {
            double dGrossAmount = 0;
            double.TryParse(grossAmountTextBox.Text, out dGrossAmount);
            double dCgstPercentage = 0;
            double.TryParse(cgstPerTextBox.Text, out dCgstPercentage);
            double dCgstAmount = dCgstPercentage * dGrossAmount / 100;
            cgstAmtTextBox.Text = Convert.ToString(dCgstAmount);
            double dSgstPercentage = 0;
            double.TryParse(sgstPerTextBox.Text,out dSgstPercentage);
            double dSgstAmount = dSgstPercentage * dGrossAmount / 100;
            sgstAmtTextBox.Text = Convert.ToString(dSgstAmount);
            double dNetAmount = dGrossAmount + dCgstAmount + dSgstAmount;
            netAmountTextBox.Text = Convert.ToString(Math.Round(dNetAmount));
        }

        private void cgstPerTextBox_TextChanged(object sender, EventArgs e)
        {
            double dGrossAmount = 0;
            double.TryParse(grossAmountTextBox.Text, out dGrossAmount);
            double dCgstPercentage = 0;
            double.TryParse(cgstPerTextBox.Text, out dCgstPercentage);
            double dCgstAmount = dCgstPercentage * dGrossAmount / 100;
            cgstAmtTextBox.Text = Convert.ToString(dCgstAmount);
            double dSgstPercentage = 0;
            double.TryParse(sgstPerTextBox.Text, out dSgstPercentage);
            double dSgstAmount = dSgstPercentage * dGrossAmount / 100;
            sgstAmtTextBox.Text = Convert.ToString(dSgstAmount);
            double dNetAmount = dGrossAmount + dCgstAmount + dSgstAmount;
            netAmountTextBox.Text = Convert.ToString(Math.Round(dNetAmount));
        }

        private void cgstAmtTextBox_TextChanged(object sender, EventArgs e)
        {
            double dGrossAmount = 0;
            double.TryParse(grossAmountTextBox.Text, out dGrossAmount);
            double dCgstPercentage = 0;
            double.TryParse(cgstPerTextBox.Text, out dCgstPercentage);
            double dCgstAmount = dCgstPercentage * dGrossAmount / 100;
            cgstAmtTextBox.Text = Convert.ToString(dCgstAmount);
            double dSgstPercentage = 0;
            double.TryParse(sgstPerTextBox.Text, out dSgstPercentage);
            double dSgstAmount = dSgstPercentage * dGrossAmount / 100;
            sgstAmtTextBox.Text = Convert.ToString(dSgstAmount);
            double dNetAmount = dGrossAmount + dCgstAmount + dSgstAmount;
            netAmountTextBox.Text = Convert.ToString(Math.Round(dNetAmount));
        }

        private void sgstPerTextBox_TextChanged(object sender, EventArgs e)
        {
            double dGrossAmount = 0;
            double.TryParse(grossAmountTextBox.Text, out dGrossAmount);
            double dCgstPercentage = 0;
            double.TryParse(cgstPerTextBox.Text, out dCgstPercentage);
            double dCgstAmount = dCgstPercentage * dGrossAmount / 100;
            cgstAmtTextBox.Text = Convert.ToString(dCgstAmount);
            double dSgstPercentage = 0;
            double.TryParse(sgstPerTextBox.Text, out dSgstPercentage);
            double dSgstAmount = dSgstPercentage * dGrossAmount / 100;
            sgstAmtTextBox.Text = Convert.ToString(dSgstAmount);
            double dNetAmount = dGrossAmount + dCgstAmount + dSgstAmount;
            netAmountTextBox.Text = Convert.ToString(Math.Round(dNetAmount));
        }

        private void sgstAmtTextBox_TextChanged(object sender, EventArgs e)
        {
            double dGrossAmount = 0;
            double.TryParse(grossAmountTextBox.Text, out dGrossAmount);
            double dCgstPercentage = 0;
            double.TryParse(cgstPerTextBox.Text, out dCgstPercentage);
            double dCgstAmount = dCgstPercentage * dGrossAmount / 100;
            cgstAmtTextBox.Text = Convert.ToString(dCgstAmount);
            double dSgstPercentage = 0;
            double.TryParse(sgstPerTextBox.Text, out dSgstPercentage);
            double dSgstAmount = dSgstPercentage * dGrossAmount / 100;
            sgstAmtTextBox.Text = Convert.ToString(dSgstAmount);
            double dNetAmount = dGrossAmount + dCgstAmount + dSgstAmount;
            netAmountTextBox.Text = Convert.ToString(Math.Round(dNetAmount));
        }

        private void voucherNoTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                long lVoucherNo = 0;
                long.TryParse(voucherNoTextBox.Text, out lVoucherNo);
                cdNoteDetail = partyDB.getCDNoteDetail(lVoucherNo, Convert.ToInt64(typeComboBox.SelectedValue));
                if (cdNoteDetail == null)
                    return;
                setPartySelectedValue(Convert.ToInt64(cdNoteDetail.party_id));
                remarkTextBox.Text = cdNoteDetail.remark;
                DateTime dt, dt1;
                bool bFlag = DateTime.TryParse(cdNoteDetail.date.ToString(), out dt);
                if (bFlag)
                {
                    dt1 = dt.Date;
                    //DateTime.Parse(dt1.ToString()).ToString("dd-MM-yyyy");
                    dateTextBox.Text = dt1.ToString("dd-MM-yyyy");
                }
                noteNoTextBox.Text = cdNoteDetail.noteNo;
                grossAmountTextBox.Text = Convert.ToString(cdNoteDetail.grossAmount);
                if ((long)gstTypeComboBox.SelectedValue == 1)
                {
                    cgstPerTextBox.Text = Convert.ToString(cdNoteDetail.igstPercentage);
                    cgstAmtTextBox.Text = Convert.ToString(cdNoteDetail.igstAmount);
                }
                else
                {
                    cgstPerTextBox.Text = Convert.ToString(cdNoteDetail.cgstPercentage);
                    cgstAmtTextBox.Text = Convert.ToString(cdNoteDetail.cgstAmount);
                    sgstPerTextBox.Text = Convert.ToString(cdNoteDetail.sgstPercentage);
                    sgstAmtTextBox.Text = Convert.ToString(cdNoteDetail.sgstAmount);
                }

                netAmountTextBox.Text = Convert.ToString(cdNoteDetail.netAmount);

                refBillNoComboBox.Items.Add(cdNoteDetail.refBillNo);
                refBillNoComboBox.SelectedIndex = 0;
                bFlag = DateTime.TryParse(cdNoteDetail.refBillDate.ToString(), out dt);
                if (bFlag)
                {
                    dt1 = dt.Date;
                    //DateTime.Parse(dt1.ToString()).ToString("dd-MM-yyyy");
                    refBillDateTextBox.Text = dt1.ToString("dd-MM-yyyy");
                }
                accountComboBox.SelectedValue = cdNoteDetail.accountType_id;
                addNewButton.Enabled = false;
                updateButton.Enabled = true;
                deleteButton.Enabled = true;
            }
        }

        private void setPartySelectedValue(long lPartyID)
        {
            try
            {
                //partyComboBox.SelectedIndex = -1;
                if (lPartyID > 0)
                {
                    for (int index = 0; index < partyNameComboBox.Items.Count; index++)
                    {
                        DataTable partyTable = partyNameComboBox.Data;
                        DataRow Row = partyTable.Rows[index];
                        if (Row != null)
                        {
                            if (Convert.ToInt64(Row["PARTY_ID"].ToString()) == lPartyID)
                            {
                                partyNameComboBox.SelectedIndex = -1;
                                partyNameComboBox.SelectedIndex = index;
                                break;
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                return;
            }
        }
    }
}
