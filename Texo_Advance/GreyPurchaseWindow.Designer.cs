﻿namespace Texo_Advance
{
    partial class GreyPurchaseWindow
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GreyPurchaseWindow));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.srnoTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.billNoTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.dateTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.orderTextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.brokerComboBox = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.qualityComboBox = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.wtTextBox = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.hsncTextBox = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.checkerComboBox = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.recdTakaTextBox = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.recdMtrsTextBox = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.purRateTextBox = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.grossAmtTextBox = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.gstnTextBox = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.lrnoTextBox = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.transportComboBox = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.disPerTextBox = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.discAmtTextBox = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.sgstAmtTextBox = new System.Windows.Forms.TextBox();
            this.sgstPerTextBox = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.cgstAmtTextBox = new System.Windows.Forms.TextBox();
            this.cgstPerTextBox = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.igstAmtTextBox = new System.Windows.Forms.TextBox();
            this.igstPerTextBox = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.foldLessTextBox = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.addAnyTextBox = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.lessAnyTextBox = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.netAmtTextBox = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.remarksTextBox = new System.Windows.Forms.TextBox();
            this.addButton = new System.Windows.Forms.Button();
            this.deleteButton = new System.Windows.Forms.Button();
            this.viewButton = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.companyNameTextBox = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.label33 = new System.Windows.Forms.Label();
            this.cityComboBox = new System.Windows.Forms.ComboBox();
            this.label34 = new System.Windows.Forms.Label();
            this.gstTypeComboBox = new System.Windows.Forms.ComboBox();
            this.label35 = new System.Windows.Forms.Label();
            this.taxableValueTextBox = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.dueDaysTextBox = new System.Windows.Forms.TextBox();
            this.interestTextBox = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.updateButton = new System.Windows.Forms.Button();
            this.label38 = new System.Windows.Forms.Label();
            this.greyPurchaseTypeComboBox = new System.Windows.Forms.ComboBox();
            this.partyNameComboBox = new JTG.ColumnComboBox();
            this.refBillNoComboBox = new JTG.ColumnComboBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(21, 78);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "COMPANY:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(380, 79);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 20);
            this.label2.TabIndex = 0;
            this.label2.Text = "SR. NO.:";
            // 
            // srnoTextBox
            // 
            this.srnoTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.srnoTextBox.Location = new System.Drawing.Point(468, 78);
            this.srnoTextBox.Name = "srnoTextBox";
            this.srnoTextBox.Size = new System.Drawing.Size(83, 22);
            this.srnoTextBox.TabIndex = 1;
            this.srnoTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.srnoTextBox_KeyDown);
            this.srnoTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.srnoTextBox_Validating);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(571, 77);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 20);
            this.label3.TabIndex = 0;
            this.label3.Text = "BILL NO.:";
            // 
            // billNoTextBox
            // 
            this.billNoTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.billNoTextBox.Location = new System.Drawing.Point(659, 77);
            this.billNoTextBox.Name = "billNoTextBox";
            this.billNoTextBox.Size = new System.Drawing.Size(83, 22);
            this.billNoTextBox.TabIndex = 2;
            this.billNoTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.billNoTextBox_Validating);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(759, 76);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 20);
            this.label4.TabIndex = 0;
            this.label4.Text = "DATE:";
            // 
            // dateTextBox
            // 
            this.dateTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTextBox.Location = new System.Drawing.Point(826, 76);
            this.dateTextBox.Name = "dateTextBox";
            this.dateTextBox.Size = new System.Drawing.Size(104, 22);
            this.dateTextBox.TabIndex = 3;
            this.dateTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.dateTextBox_Validating);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(21, 121);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(111, 20);
            this.label5.TabIndex = 0;
            this.label5.Text = "PARTY A/C.:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(821, 158);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(113, 20);
            this.label6.TabIndex = 0;
            this.label6.Text = "ORDER NO.:";
            // 
            // orderTextBox
            // 
            this.orderTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.orderTextBox.Location = new System.Drawing.Point(937, 158);
            this.orderTextBox.Name = "orderTextBox";
            this.orderTextBox.Size = new System.Drawing.Size(61, 22);
            this.orderTextBox.TabIndex = 9;
            this.orderTextBox.Text = "0";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(20, 201);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(88, 20);
            this.label7.TabIndex = 0;
            this.label7.Text = "BROKER:";
            // 
            // brokerComboBox
            // 
            this.brokerComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.brokerComboBox.FormattingEnabled = true;
            this.brokerComboBox.Location = new System.Drawing.Point(144, 199);
            this.brokerComboBox.Name = "brokerComboBox";
            this.brokerComboBox.Size = new System.Drawing.Size(231, 24);
            this.brokerComboBox.TabIndex = 14;
            this.brokerComboBox.Validating += new System.ComponentModel.CancelEventHandler(this.brokerComboBox_Validating);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(385, 199);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(90, 20);
            this.label8.TabIndex = 0;
            this.label8.Text = "QUALITY:";
            // 
            // qualityComboBox
            // 
            this.qualityComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.qualityComboBox.FormattingEnabled = true;
            this.qualityComboBox.Location = new System.Drawing.Point(492, 198);
            this.qualityComboBox.Name = "qualityComboBox";
            this.qualityComboBox.Size = new System.Drawing.Size(205, 24);
            this.qualityComboBox.TabIndex = 15;
            this.qualityComboBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.qualityComboBox_KeyDown);
            this.qualityComboBox.Validating += new System.ComponentModel.CancelEventHandler(this.qualityComboBox_Validating);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(729, 199);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(75, 20);
            this.label9.TabIndex = 0;
            this.label9.Text = "WT KG.:";
            // 
            // wtTextBox
            // 
            this.wtTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.wtTextBox.Location = new System.Drawing.Point(804, 198);
            this.wtTextBox.Name = "wtTextBox";
            this.wtTextBox.Size = new System.Drawing.Size(83, 22);
            this.wtTextBox.TabIndex = 16;
            this.wtTextBox.Text = "0.0";
            this.wtTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.wtTextBox_Validating);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(20, 242);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(106, 20);
            this.label10.TabIndex = 0;
            this.label10.Text = "HSN CODE:";
            // 
            // hsncTextBox
            // 
            this.hsncTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hsncTextBox.Location = new System.Drawing.Point(144, 242);
            this.hsncTextBox.Name = "hsncTextBox";
            this.hsncTextBox.Size = new System.Drawing.Size(83, 22);
            this.hsncTextBox.TabIndex = 17;
            this.hsncTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.hsncTextBox_Validating);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(227, 241);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(99, 20);
            this.label11.TabIndex = 0;
            this.label11.Text = "CHECKER:";
            // 
            // checkerComboBox
            // 
            this.checkerComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkerComboBox.FormattingEnabled = true;
            this.checkerComboBox.Location = new System.Drawing.Point(327, 240);
            this.checkerComboBox.Name = "checkerComboBox";
            this.checkerComboBox.Size = new System.Drawing.Size(155, 24);
            this.checkerComboBox.TabIndex = 18;
            this.checkerComboBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.checkerComboBox_KeyDown);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(20, 286);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(106, 20);
            this.label12.TabIndex = 0;
            this.label12.Text = "REC. TAKA:";
            // 
            // recdTakaTextBox
            // 
            this.recdTakaTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.recdTakaTextBox.Location = new System.Drawing.Point(143, 285);
            this.recdTakaTextBox.Name = "recdTakaTextBox";
            this.recdTakaTextBox.Size = new System.Drawing.Size(88, 26);
            this.recdTakaTextBox.TabIndex = 21;
            this.recdTakaTextBox.Text = "0";
            this.recdTakaTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.recdTakaTextBox_Validating);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(21, 323);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(115, 20);
            this.label13.TabIndex = 0;
            this.label13.Text = "REC. MTRS.:";
            // 
            // recdMtrsTextBox
            // 
            this.recdMtrsTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.recdMtrsTextBox.Location = new System.Drawing.Point(143, 322);
            this.recdMtrsTextBox.Name = "recdMtrsTextBox";
            this.recdMtrsTextBox.Size = new System.Drawing.Size(89, 26);
            this.recdMtrsTextBox.TabIndex = 22;
            this.recdMtrsTextBox.Text = "0.0";
            this.recdMtrsTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.recdMtrsTextBox_Validating);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(21, 361);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(103, 20);
            this.label14.TabIndex = 0;
            this.label14.Text = "PUR RATE:";
            // 
            // purRateTextBox
            // 
            this.purRateTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.purRateTextBox.Location = new System.Drawing.Point(143, 360);
            this.purRateTextBox.Name = "purRateTextBox";
            this.purRateTextBox.Size = new System.Drawing.Size(89, 26);
            this.purRateTextBox.TabIndex = 23;
            this.purRateTextBox.Text = "0.0";
            this.purRateTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.purRateTextBox_Validating);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(21, 401);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(124, 20);
            this.label15.TabIndex = 0;
            this.label15.Text = "GROSS AMT.:";
            // 
            // grossAmtTextBox
            // 
            this.grossAmtTextBox.BackColor = System.Drawing.SystemColors.Info;
            this.grossAmtTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grossAmtTextBox.Location = new System.Drawing.Point(143, 400);
            this.grossAmtTextBox.Name = "grossAmtTextBox";
            this.grossAmtTextBox.ReadOnly = true;
            this.grossAmtTextBox.Size = new System.Drawing.Size(112, 26);
            this.grossAmtTextBox.TabIndex = 24;
            this.grossAmtTextBox.Text = "0";
            this.grossAmtTextBox.TextChanged += new System.EventHandler(this.grossAmtTextBox_TextChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(354, 160);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(62, 20);
            this.label16.TabIndex = 0;
            this.label16.Text = "GSTN:";
            // 
            // gstnTextBox
            // 
            this.gstnTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gstnTextBox.Location = new System.Drawing.Point(419, 160);
            this.gstnTextBox.Name = "gstnTextBox";
            this.gstnTextBox.ReadOnly = true;
            this.gstnTextBox.Size = new System.Drawing.Size(194, 22);
            this.gstnTextBox.TabIndex = 11;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(485, 240);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(72, 20);
            this.label17.TabIndex = 0;
            this.label17.Text = "LR NO.:";
            // 
            // lrnoTextBox
            // 
            this.lrnoTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lrnoTextBox.Location = new System.Drawing.Point(562, 240);
            this.lrnoTextBox.Name = "lrnoTextBox";
            this.lrnoTextBox.Size = new System.Drawing.Size(104, 22);
            this.lrnoTextBox.TabIndex = 19;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(671, 239);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(120, 20);
            this.label18.TabIndex = 0;
            this.label18.Text = "TRANSPORT:";
            // 
            // transportComboBox
            // 
            this.transportComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.transportComboBox.FormattingEnabled = true;
            this.transportComboBox.Location = new System.Drawing.Point(787, 239);
            this.transportComboBox.Name = "transportComboBox";
            this.transportComboBox.Size = new System.Drawing.Size(155, 24);
            this.transportComboBox.TabIndex = 20;
            this.transportComboBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.transportComboBox_KeyDown);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(261, 286);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(92, 20);
            this.label19.TabIndex = 0;
            this.label19.Text = "DISC.   %:";
            // 
            // disPerTextBox
            // 
            this.disPerTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.disPerTextBox.Location = new System.Drawing.Point(359, 285);
            this.disPerTextBox.Name = "disPerTextBox";
            this.disPerTextBox.Size = new System.Drawing.Size(62, 26);
            this.disPerTextBox.TabIndex = 25;
            this.disPerTextBox.Text = "0";
            this.disPerTextBox.TextChanged += new System.EventHandler(this.disPerTextBox_TextChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(439, 283);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(108, 20);
            this.label20.TabIndex = 0;
            this.label20.Text = "DISC. AMT.:";
            // 
            // discAmtTextBox
            // 
            this.discAmtTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.discAmtTextBox.Location = new System.Drawing.Point(562, 282);
            this.discAmtTextBox.Name = "discAmtTextBox";
            this.discAmtTextBox.Size = new System.Drawing.Size(88, 26);
            this.discAmtTextBox.TabIndex = 26;
            this.discAmtTextBox.Text = "0";
            this.discAmtTextBox.TextChanged += new System.EventHandler(this.discAmtTextBox_TextChanged);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(439, 321);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(113, 20);
            this.label21.TabIndex = 0;
            this.label21.Text = "SGST  AMT.:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(261, 323);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(97, 20);
            this.label22.TabIndex = 0;
            this.label22.Text = "SGST    %:";
            // 
            // sgstAmtTextBox
            // 
            this.sgstAmtTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sgstAmtTextBox.Location = new System.Drawing.Point(562, 320);
            this.sgstAmtTextBox.Name = "sgstAmtTextBox";
            this.sgstAmtTextBox.Size = new System.Drawing.Size(88, 26);
            this.sgstAmtTextBox.TabIndex = 28;
            this.sgstAmtTextBox.Text = "0";
            this.sgstAmtTextBox.TextChanged += new System.EventHandler(this.sgstAmtTextBox_TextChanged);
            // 
            // sgstPerTextBox
            // 
            this.sgstPerTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sgstPerTextBox.Location = new System.Drawing.Point(359, 322);
            this.sgstPerTextBox.Name = "sgstPerTextBox";
            this.sgstPerTextBox.Size = new System.Drawing.Size(62, 26);
            this.sgstPerTextBox.TabIndex = 27;
            this.sgstPerTextBox.Text = "2.5";
            this.sgstPerTextBox.TextChanged += new System.EventHandler(this.sgstPerTextBox_TextChanged);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(438, 358);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(113, 20);
            this.label23.TabIndex = 0;
            this.label23.Text = "CGST  AMT.:";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(260, 360);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(97, 20);
            this.label24.TabIndex = 0;
            this.label24.Text = "CGST    %:";
            // 
            // cgstAmtTextBox
            // 
            this.cgstAmtTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cgstAmtTextBox.Location = new System.Drawing.Point(561, 357);
            this.cgstAmtTextBox.Name = "cgstAmtTextBox";
            this.cgstAmtTextBox.Size = new System.Drawing.Size(88, 26);
            this.cgstAmtTextBox.TabIndex = 30;
            this.cgstAmtTextBox.Text = "0";
            this.cgstAmtTextBox.TextChanged += new System.EventHandler(this.cgstAmtTextBox_TextChanged);
            // 
            // cgstPerTextBox
            // 
            this.cgstPerTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cgstPerTextBox.Location = new System.Drawing.Point(358, 359);
            this.cgstPerTextBox.Name = "cgstPerTextBox";
            this.cgstPerTextBox.Size = new System.Drawing.Size(62, 26);
            this.cgstPerTextBox.TabIndex = 29;
            this.cgstPerTextBox.Text = "2.5";
            this.cgstPerTextBox.TextChanged += new System.EventHandler(this.cgstPerTextBox_TextChanged);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(439, 396);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(112, 20);
            this.label25.TabIndex = 0;
            this.label25.Text = "IGST   AMT.:";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(261, 398);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(91, 20);
            this.label26.TabIndex = 0;
            this.label26.Text = "IGST    %:";
            // 
            // igstAmtTextBox
            // 
            this.igstAmtTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.igstAmtTextBox.Location = new System.Drawing.Point(562, 395);
            this.igstAmtTextBox.Name = "igstAmtTextBox";
            this.igstAmtTextBox.Size = new System.Drawing.Size(88, 26);
            this.igstAmtTextBox.TabIndex = 32;
            this.igstAmtTextBox.Text = "0";
            this.igstAmtTextBox.TextChanged += new System.EventHandler(this.igstAmtTextBox_TextChanged);
            // 
            // igstPerTextBox
            // 
            this.igstPerTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.igstPerTextBox.Location = new System.Drawing.Point(359, 397);
            this.igstPerTextBox.Name = "igstPerTextBox";
            this.igstPerTextBox.Size = new System.Drawing.Size(62, 26);
            this.igstPerTextBox.TabIndex = 31;
            this.igstPerTextBox.Text = "5";
            this.igstPerTextBox.TextChanged += new System.EventHandler(this.igstPerTextBox_TextChanged);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(703, 281);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(112, 20);
            this.label27.TabIndex = 0;
            this.label27.Text = "FOLD LESS:";
            // 
            // foldLessTextBox
            // 
            this.foldLessTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.foldLessTextBox.Location = new System.Drawing.Point(826, 280);
            this.foldLessTextBox.Name = "foldLessTextBox";
            this.foldLessTextBox.Size = new System.Drawing.Size(88, 26);
            this.foldLessTextBox.TabIndex = 33;
            this.foldLessTextBox.Text = "0";
            this.foldLessTextBox.TextChanged += new System.EventHandler(this.foldLessTextBox_TextChanged);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(703, 321);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(93, 20);
            this.label28.TabIndex = 0;
            this.label28.Text = "ADD ANY:";
            // 
            // addAnyTextBox
            // 
            this.addAnyTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addAnyTextBox.Location = new System.Drawing.Point(826, 320);
            this.addAnyTextBox.Name = "addAnyTextBox";
            this.addAnyTextBox.Size = new System.Drawing.Size(88, 26);
            this.addAnyTextBox.TabIndex = 34;
            this.addAnyTextBox.Text = "0";
            this.addAnyTextBox.TextChanged += new System.EventHandler(this.addAnyTextBox_TextChanged);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(703, 358);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(101, 20);
            this.label29.TabIndex = 0;
            this.label29.Text = "LESS ANY:";
            // 
            // lessAnyTextBox
            // 
            this.lessAnyTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lessAnyTextBox.Location = new System.Drawing.Point(826, 357);
            this.lessAnyTextBox.Name = "lessAnyTextBox";
            this.lessAnyTextBox.Size = new System.Drawing.Size(88, 26);
            this.lessAnyTextBox.TabIndex = 35;
            this.lessAnyTextBox.Text = "0";
            this.lessAnyTextBox.TextChanged += new System.EventHandler(this.lessAnyTextBox_TextChanged);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(707, 443);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(94, 20);
            this.label30.TabIndex = 0;
            this.label30.Text = "NET AMT.:";
            // 
            // netAmtTextBox
            // 
            this.netAmtTextBox.BackColor = System.Drawing.SystemColors.Info;
            this.netAmtTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.netAmtTextBox.Location = new System.Drawing.Point(808, 442);
            this.netAmtTextBox.Name = "netAmtTextBox";
            this.netAmtTextBox.ReadOnly = true;
            this.netAmtTextBox.Size = new System.Drawing.Size(134, 26);
            this.netAmtTextBox.TabIndex = 50;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(20, 445);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(101, 20);
            this.label31.TabIndex = 0;
            this.label31.Text = "REMARKS:";
            // 
            // remarksTextBox
            // 
            this.remarksTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.remarksTextBox.Location = new System.Drawing.Point(142, 444);
            this.remarksTextBox.Name = "remarksTextBox";
            this.remarksTextBox.Size = new System.Drawing.Size(554, 26);
            this.remarksTextBox.TabIndex = 36;
            // 
            // addButton
            // 
            this.addButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addButton.Location = new System.Drawing.Point(315, 505);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(108, 43);
            this.addButton.TabIndex = 38;
            this.addButton.Text = "ADD";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // deleteButton
            // 
            this.deleteButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deleteButton.Location = new System.Drawing.Point(729, 505);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(108, 43);
            this.deleteButton.TabIndex = 41;
            this.deleteButton.Text = "DELETE";
            this.deleteButton.UseVisualStyleBackColor = true;
            this.deleteButton.Click += new System.EventHandler(this.deleteButton_Click);
            // 
            // viewButton
            // 
            this.viewButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.viewButton.Location = new System.Drawing.Point(589, 505);
            this.viewButton.Name = "viewButton";
            this.viewButton.Size = new System.Drawing.Size(108, 43);
            this.viewButton.TabIndex = 40;
            this.viewButton.Text = "VIEW";
            this.viewButton.UseVisualStyleBackColor = true;
            this.viewButton.Click += new System.EventHandler(this.viewButton_Click);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(165, 505);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(108, 43);
            this.button1.TabIndex = 37;
            this.button1.Text = "TAKA DETAILS";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // companyNameTextBox
            // 
            this.companyNameTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.companyNameTextBox.ForeColor = System.Drawing.Color.Blue;
            this.companyNameTextBox.Location = new System.Drawing.Point(143, 79);
            this.companyNameTextBox.Name = "companyNameTextBox";
            this.companyNameTextBox.ReadOnly = true;
            this.companyNameTextBox.Size = new System.Drawing.Size(229, 26);
            this.companyNameTextBox.TabIndex = 0;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Maroon;
            this.label32.Location = new System.Drawing.Point(370, 24);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(258, 24);
            this.label32.TabIndex = 0;
            this.label32.Text = "GREY PURCHASE ENTRY";
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(23, 141);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(78, 13);
            this.linkLabel1.TabIndex = 87;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Add New Party";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(20, 162);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(54, 20);
            this.label33.TabIndex = 0;
            this.label33.Text = "CITY:";
            // 
            // cityComboBox
            // 
            this.cityComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cityComboBox.FormattingEnabled = true;
            this.cityComboBox.Location = new System.Drawing.Point(144, 158);
            this.cityComboBox.Name = "cityComboBox";
            this.cityComboBox.Size = new System.Drawing.Size(205, 24);
            this.cityComboBox.TabIndex = 10;
            this.cityComboBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cityComboBox_KeyDown);
            this.cityComboBox.Validating += new System.ComponentModel.CancelEventHandler(this.cityComboBox_Validating);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(640, 122);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(100, 20);
            this.label34.TabIndex = 0;
            this.label34.Text = "GST TYPE:";
            // 
            // gstTypeComboBox
            // 
            this.gstTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.gstTypeComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gstTypeComboBox.FormattingEnabled = true;
            this.gstTypeComboBox.Location = new System.Drawing.Point(742, 120);
            this.gstTypeComboBox.Name = "gstTypeComboBox";
            this.gstTypeComboBox.Size = new System.Drawing.Size(215, 24);
            this.gstTypeComboBox.TabIndex = 5;
            this.gstTypeComboBox.SelectedIndexChanged += new System.EventHandler(this.gstTypeComboBox_SelectedIndexChanged);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(651, 395);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(158, 20);
            this.label35.TabIndex = 0;
            this.label35.Text = "TAXABLE VALUE:";
            // 
            // taxableValueTextBox
            // 
            this.taxableValueTextBox.BackColor = System.Drawing.SystemColors.Info;
            this.taxableValueTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.taxableValueTextBox.Location = new System.Drawing.Point(804, 393);
            this.taxableValueTextBox.Name = "taxableValueTextBox";
            this.taxableValueTextBox.ReadOnly = true;
            this.taxableValueTextBox.Size = new System.Drawing.Size(134, 26);
            this.taxableValueTextBox.TabIndex = 85;
            this.taxableValueTextBox.Text = "0";
            this.taxableValueTextBox.TextChanged += new System.EventHandler(this.taxableValueTextBox_TextChanged);
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(616, 160);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(106, 20);
            this.label36.TabIndex = 0;
            this.label36.Text = "DUE DAYS:";
            // 
            // dueDaysTextBox
            // 
            this.dueDaysTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dueDaysTextBox.Location = new System.Drawing.Point(721, 159);
            this.dueDaysTextBox.Name = "dueDaysTextBox";
            this.dueDaysTextBox.Size = new System.Drawing.Size(38, 22);
            this.dueDaysTextBox.TabIndex = 12;
            this.dueDaysTextBox.Text = "0";
            this.dueDaysTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.dueDaysTextBox_Validating);
            // 
            // interestTextBox
            // 
            this.interestTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.interestTextBox.Location = new System.Drawing.Point(765, 159);
            this.interestTextBox.Name = "interestTextBox";
            this.interestTextBox.Size = new System.Drawing.Size(31, 22);
            this.interestTextBox.TabIndex = 13;
            this.interestTextBox.Text = "0.0";
            this.interestTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.interestTextBox_Validating);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(799, 159);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(24, 20);
            this.label37.TabIndex = 0;
            this.label37.Text = "%";
            // 
            // updateButton
            // 
            this.updateButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.updateButton.Location = new System.Drawing.Point(449, 505);
            this.updateButton.Name = "updateButton";
            this.updateButton.Size = new System.Drawing.Size(108, 43);
            this.updateButton.TabIndex = 39;
            this.updateButton.Text = "UPDATE";
            this.updateButton.UseVisualStyleBackColor = true;
            this.updateButton.Click += new System.EventHandler(this.updateButton_Click);
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(378, 121);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(59, 20);
            this.label38.TabIndex = 0;
            this.label38.Text = "TYPE:";
            // 
            // greyPurchaseTypeComboBox
            // 
            this.greyPurchaseTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.greyPurchaseTypeComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.greyPurchaseTypeComboBox.FormattingEnabled = true;
            this.greyPurchaseTypeComboBox.Location = new System.Drawing.Point(435, 119);
            this.greyPurchaseTypeComboBox.Name = "greyPurchaseTypeComboBox";
            this.greyPurchaseTypeComboBox.Size = new System.Drawing.Size(199, 24);
            this.greyPurchaseTypeComboBox.TabIndex = 5;
            this.greyPurchaseTypeComboBox.SelectedIndexChanged += new System.EventHandler(this.greyPurchaseTypeComboBox_SelectedIndexChanged);
            // 
            // partyNameComboBox
            // 
            this.partyNameComboBox.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.partyNameComboBox.DropDownWidth = 17;
            this.partyNameComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.partyNameComboBox.Location = new System.Drawing.Point(142, 121);
            this.partyNameComboBox.Name = "partyNameComboBox";
            this.partyNameComboBox.Size = new System.Drawing.Size(230, 23);
            this.partyNameComboBox.TabIndex = 88;
            this.partyNameComboBox.ViewColumn = 0;
            this.partyNameComboBox.SelectedIndexChanged += new System.EventHandler(this.partyNameComboBox_SelectedIndexChanged);
            // 
            // refBillNoComboBox
            // 
            this.refBillNoComboBox.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.refBillNoComboBox.DropDownWidth = 17;
            this.refBillNoComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.refBillNoComboBox.FormattingEnabled = true;
            this.refBillNoComboBox.Location = new System.Drawing.Point(753, 157);
            this.refBillNoComboBox.Name = "refBillNoComboBox";
            this.refBillNoComboBox.Size = new System.Drawing.Size(84, 22);
            this.refBillNoComboBox.TabIndex = 89;
            this.refBillNoComboBox.ViewColumn = 0;
            // 
            // GreyPurchaseWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.MintCream;
            this.Controls.Add(this.refBillNoComboBox);
            this.Controls.Add(this.partyNameComboBox);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.updateButton);
            this.Controls.Add(this.viewButton);
            this.Controls.Add(this.deleteButton);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.addButton);
            this.Controls.Add(this.dateTextBox);
            this.Controls.Add(this.billNoTextBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.gstnTextBox);
            this.Controls.Add(this.lrnoTextBox);
            this.Controls.Add(this.hsncTextBox);
            this.Controls.Add(this.taxableValueTextBox);
            this.Controls.Add(this.netAmtTextBox);
            this.Controls.Add(this.remarksTextBox);
            this.Controls.Add(this.grossAmtTextBox);
            this.Controls.Add(this.purRateTextBox);
            this.Controls.Add(this.recdMtrsTextBox);
            this.Controls.Add(this.igstPerTextBox);
            this.Controls.Add(this.cgstPerTextBox);
            this.Controls.Add(this.sgstPerTextBox);
            this.Controls.Add(this.igstAmtTextBox);
            this.Controls.Add(this.disPerTextBox);
            this.Controls.Add(this.cgstAmtTextBox);
            this.Controls.Add(this.sgstAmtTextBox);
            this.Controls.Add(this.lessAnyTextBox);
            this.Controls.Add(this.addAnyTextBox);
            this.Controls.Add(this.foldLessTextBox);
            this.Controls.Add(this.discAmtTextBox);
            this.Controls.Add(this.recdTakaTextBox);
            this.Controls.Add(this.wtTextBox);
            this.Controls.Add(this.orderTextBox);
            this.Controls.Add(this.companyNameTextBox);
            this.Controls.Add(this.interestTextBox);
            this.Controls.Add(this.dueDaysTextBox);
            this.Controls.Add(this.srnoTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.greyPurchaseTypeComboBox);
            this.Controls.Add(this.gstTypeComboBox);
            this.Controls.Add(this.brokerComboBox);
            this.Controls.Add(this.transportComboBox);
            this.Controls.Add(this.checkerComboBox);
            this.Controls.Add(this.cityComboBox);
            this.Controls.Add(this.label35);
            this.Controls.Add(this.qualityComboBox);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label38);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.label36);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.label1);
            this.Name = "GreyPurchaseWindow";
            this.Size = new System.Drawing.Size(1022, 576);
            this.Load += new System.EventHandler(this.GreyPurchaseWindow_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox srnoTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox billNoTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox dateTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox orderTextBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox brokerComboBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox qualityComboBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox wtTextBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox hsncTextBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox checkerComboBox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox recdTakaTextBox;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox recdMtrsTextBox;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox purRateTextBox;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox grossAmtTextBox;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox gstnTextBox;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox lrnoTextBox;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox transportComboBox;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox disPerTextBox;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox discAmtTextBox;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox sgstAmtTextBox;
        private System.Windows.Forms.TextBox sgstPerTextBox;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox cgstAmtTextBox;
        private System.Windows.Forms.TextBox cgstPerTextBox;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox igstAmtTextBox;
        private System.Windows.Forms.TextBox igstPerTextBox;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox foldLessTextBox;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox addAnyTextBox;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox lessAnyTextBox;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox netAmtTextBox;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox remarksTextBox;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.Button deleteButton;
        private System.Windows.Forms.Button viewButton;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox companyNameTextBox;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.ComboBox cityComboBox;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.ComboBox gstTypeComboBox;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox taxableValueTextBox;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox dueDaysTextBox;
        private System.Windows.Forms.TextBox interestTextBox;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Button updateButton;
        private JTG.ColumnComboBox partyNameComboBox;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.ComboBox greyPurchaseTypeComboBox;
        private JTG.ColumnComboBox refBillNoComboBox;
    }
}
