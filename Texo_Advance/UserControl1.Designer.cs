﻿namespace Texo_Advance
{
    partial class PurchaseWindow
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PurchaseWindow));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            this.Purchase = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dueDaysTextBox = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.igstAmtTextBox = new System.Windows.Forms.TextBox();
            this.sgstAmtTextBox = new System.Windows.Forms.TextBox();
            this.cgstAmtTextBox = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.igstRateTextBox = new System.Windows.Forms.TextBox();
            this.sgstRateTextBox = new System.Windows.Forms.TextBox();
            this.cgstRateTextBox = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.caseNoTextBox2 = new System.Windows.Forms.TextBox();
            this.caseNoTextBox = new System.Windows.Forms.TextBox();
            this.addLessAmtFinalTextBox = new System.Windows.Forms.TextBox();
            this.addLessAmtTextBox = new System.Windows.Forms.TextBox();
            this.addLessTextBox = new System.Windows.Forms.TextBox();
            this.discPerAmtFinalTextBox = new System.Windows.Forms.TextBox();
            this.discPerAmtTextBox = new System.Windows.Forms.TextBox();
            this.discPerTextBox = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.remarkTextBox = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.openBillButton = new System.Windows.Forms.Button();
            this.totalAmountTextBox = new System.Windows.Forms.TextBox();
            this.printButton = new System.Windows.Forms.Button();
            this.addNewButton = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.taxValTextBox = new System.Windows.Forms.TextBox();
            this.deleteButton = new System.Windows.Forms.Button();
            this.billAmtTextBox = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.totalQtyTextBox = new System.Windows.Forms.TextBox();
            this.totalPcsTextBox = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.purchaseDataGridView = new System.Windows.Forms.DataGridView();
            this.lrnoTextBox = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.transportComboBox = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.gstnTextBox = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.brokerComboBox = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.stationComboBox = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.gstTypeComboBox = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.stateComboBox = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.dateTextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.billNoTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.vouvherNoTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.purchaseTypeComboBox = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.comNameTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.itemLinkLabel = new System.Windows.Forms.LinkLabel();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridViewComboBoxColumn1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.refBillNoComboBox = new JTG.ColumnComboBox();
            this.partyComboBox = new JTG.ColumnComboBox();
            this.itemNameGridCombo = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.bundlesEditBoxGrid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hsncEditBoxGrid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.packingComboGrid = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.unitComboGrid = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.pcsTextBoxGrid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cutEditBoxGrid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.qtyEditBoxGid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rateTextBoxGrid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.amountTextBoxGrid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rdAmountTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.discountAmountTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.addLessTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cgstPerTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cgstAmountTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sgstPerTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sgstAmountTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.igstPerTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.igstAmountTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.totalAmountTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.purchaseDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // Purchase
            // 
            this.Purchase.AutoSize = true;
            this.Purchase.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Purchase.Location = new System.Drawing.Point(546, 1);
            this.Purchase.Name = "Purchase";
            this.Purchase.Size = new System.Drawing.Size(118, 32);
            this.Purchase.TabIndex = 0;
            this.Purchase.Text = "Purchase";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dueDaysTextBox);
            this.groupBox1.Controls.Add(this.label34);
            this.groupBox1.Controls.Add(this.igstAmtTextBox);
            this.groupBox1.Controls.Add(this.sgstAmtTextBox);
            this.groupBox1.Controls.Add(this.cgstAmtTextBox);
            this.groupBox1.Controls.Add(this.label30);
            this.groupBox1.Controls.Add(this.label29);
            this.groupBox1.Controls.Add(this.label28);
            this.groupBox1.Controls.Add(this.igstRateTextBox);
            this.groupBox1.Controls.Add(this.sgstRateTextBox);
            this.groupBox1.Controls.Add(this.cgstRateTextBox);
            this.groupBox1.Controls.Add(this.label27);
            this.groupBox1.Controls.Add(this.label26);
            this.groupBox1.Controls.Add(this.label25);
            this.groupBox1.Controls.Add(this.label22);
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.caseNoTextBox2);
            this.groupBox1.Controls.Add(this.caseNoTextBox);
            this.groupBox1.Controls.Add(this.addLessAmtFinalTextBox);
            this.groupBox1.Controls.Add(this.addLessAmtTextBox);
            this.groupBox1.Controls.Add(this.addLessTextBox);
            this.groupBox1.Controls.Add(this.discPerAmtFinalTextBox);
            this.groupBox1.Controls.Add(this.discPerAmtTextBox);
            this.groupBox1.Controls.Add(this.discPerTextBox);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.label24);
            this.groupBox1.Controls.Add(this.label23);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(46, 479);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(767, 100);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "DETAILS";
            // 
            // dueDaysTextBox
            // 
            this.dueDaysTextBox.ForeColor = System.Drawing.Color.Red;
            this.dueDaysTextBox.Location = new System.Drawing.Point(349, 69);
            this.dueDaysTextBox.Name = "dueDaysTextBox";
            this.dueDaysTextBox.Size = new System.Drawing.Size(71, 22);
            this.dueDaysTextBox.TabIndex = 23;
            this.dueDaysTextBox.Text = "0";
            this.dueDaysTextBox.TextChanged += new System.EventHandler(this.dueDaysTextBox_TextChanged);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(260, 72);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(89, 16);
            this.label34.TabIndex = 88;
            this.label34.Text = "DUE DAYS:";
            // 
            // igstAmtTextBox
            // 
            this.igstAmtTextBox.Location = new System.Drawing.Point(654, 69);
            this.igstAmtTextBox.Name = "igstAmtTextBox";
            this.igstAmtTextBox.Size = new System.Drawing.Size(93, 22);
            this.igstAmtTextBox.TabIndex = 29;
            this.igstAmtTextBox.TextChanged += new System.EventHandler(this.igstAmtTextBox_TextChanged);
            // 
            // sgstAmtTextBox
            // 
            this.sgstAmtTextBox.Location = new System.Drawing.Point(654, 43);
            this.sgstAmtTextBox.Name = "sgstAmtTextBox";
            this.sgstAmtTextBox.Size = new System.Drawing.Size(93, 22);
            this.sgstAmtTextBox.TabIndex = 27;
            this.sgstAmtTextBox.TextChanged += new System.EventHandler(this.sgstAmtTextBox_TextChanged);
            // 
            // cgstAmtTextBox
            // 
            this.cgstAmtTextBox.Location = new System.Drawing.Point(654, 17);
            this.cgstAmtTextBox.Name = "cgstAmtTextBox";
            this.cgstAmtTextBox.Size = new System.Drawing.Size(93, 22);
            this.cgstAmtTextBox.TabIndex = 25;
            this.cgstAmtTextBox.TextChanged += new System.EventHandler(this.cgstAmtTextBox_TextChanged);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(578, 72);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(69, 16);
            this.label30.TabIndex = 8;
            this.label30.Text = "%  AMT.:";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(577, 44);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(69, 16);
            this.label29.TabIndex = 8;
            this.label29.Text = "%  AMT.:";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(577, 17);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(69, 16);
            this.label28.TabIndex = 8;
            this.label28.Text = "%  AMT.:";
            // 
            // igstRateTextBox
            // 
            this.igstRateTextBox.Location = new System.Drawing.Point(527, 72);
            this.igstRateTextBox.Name = "igstRateTextBox";
            this.igstRateTextBox.Size = new System.Drawing.Size(44, 22);
            this.igstRateTextBox.TabIndex = 28;
            this.igstRateTextBox.Text = "5";
            this.igstRateTextBox.TextChanged += new System.EventHandler(this.igstRateTextBox_TextChanged);
            // 
            // sgstRateTextBox
            // 
            this.sgstRateTextBox.Location = new System.Drawing.Point(527, 44);
            this.sgstRateTextBox.Name = "sgstRateTextBox";
            this.sgstRateTextBox.Size = new System.Drawing.Size(44, 22);
            this.sgstRateTextBox.TabIndex = 26;
            this.sgstRateTextBox.Text = "2.5";
            this.sgstRateTextBox.TextChanged += new System.EventHandler(this.sgstRateTextBox_TextChanged);
            // 
            // cgstRateTextBox
            // 
            this.cgstRateTextBox.Location = new System.Drawing.Point(527, 17);
            this.cgstRateTextBox.Name = "cgstRateTextBox";
            this.cgstRateTextBox.Size = new System.Drawing.Size(44, 22);
            this.cgstRateTextBox.TabIndex = 24;
            this.cgstRateTextBox.Text = "2.5";
            this.cgstRateTextBox.TextChanged += new System.EventHandler(this.cgstRateTextBox_TextChanged);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(467, 73);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(43, 16);
            this.label27.TabIndex = 6;
            this.label27.Text = "IGST";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(467, 47);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(49, 16);
            this.label26.TabIndex = 6;
            this.label26.Text = "SGST";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(467, 21);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(49, 16);
            this.label25.TabIndex = 6;
            this.label25.Text = "CGST";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(180, 70);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(17, 16);
            this.label22.TabIndex = 5;
            this.label22.Text = "X";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(180, 47);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(17, 16);
            this.label21.TabIndex = 5;
            this.label21.Text = "X";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(180, 25);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(21, 16);
            this.label20.TabIndex = 5;
            this.label20.Text = "%";
            // 
            // caseNoTextBox2
            // 
            this.caseNoTextBox2.Location = new System.Drawing.Point(218, 69);
            this.caseNoTextBox2.Name = "caseNoTextBox2";
            this.caseNoTextBox2.Size = new System.Drawing.Size(33, 22);
            this.caseNoTextBox2.TabIndex = 22;
            this.caseNoTextBox2.Text = "1";
            // 
            // caseNoTextBox
            // 
            this.caseNoTextBox.Location = new System.Drawing.Point(107, 69);
            this.caseNoTextBox.Name = "caseNoTextBox";
            this.caseNoTextBox.Size = new System.Drawing.Size(67, 22);
            this.caseNoTextBox.TabIndex = 21;
            // 
            // addLessAmtFinalTextBox
            // 
            this.addLessAmtFinalTextBox.ForeColor = System.Drawing.Color.Green;
            this.addLessAmtFinalTextBox.Location = new System.Drawing.Point(349, 43);
            this.addLessAmtFinalTextBox.Name = "addLessAmtFinalTextBox";
            this.addLessAmtFinalTextBox.Size = new System.Drawing.Size(71, 22);
            this.addLessAmtFinalTextBox.TabIndex = 20;
            this.addLessAmtFinalTextBox.Text = "0";
            this.addLessAmtFinalTextBox.TextChanged += new System.EventHandler(this.addLessAmtFinalTextBox_TextChanged);
            // 
            // addLessAmtTextBox
            // 
            this.addLessAmtTextBox.Location = new System.Drawing.Point(218, 43);
            this.addLessAmtTextBox.Name = "addLessAmtTextBox";
            this.addLessAmtTextBox.Size = new System.Drawing.Size(71, 22);
            this.addLessAmtTextBox.TabIndex = 19;
            this.addLessAmtTextBox.Text = "0";
            this.addLessAmtTextBox.TextChanged += new System.EventHandler(this.addLessAmtTextBox_TextChanged);
            // 
            // addLessTextBox
            // 
            this.addLessTextBox.Location = new System.Drawing.Point(131, 43);
            this.addLessTextBox.Name = "addLessTextBox";
            this.addLessTextBox.Size = new System.Drawing.Size(43, 22);
            this.addLessTextBox.TabIndex = 18;
            this.addLessTextBox.Text = "0";
            this.addLessTextBox.TextChanged += new System.EventHandler(this.addLessTextBox_TextChanged);
            // 
            // discPerAmtFinalTextBox
            // 
            this.discPerAmtFinalTextBox.ForeColor = System.Drawing.Color.Green;
            this.discPerAmtFinalTextBox.Location = new System.Drawing.Point(349, 19);
            this.discPerAmtFinalTextBox.Name = "discPerAmtFinalTextBox";
            this.discPerAmtFinalTextBox.Size = new System.Drawing.Size(71, 22);
            this.discPerAmtFinalTextBox.TabIndex = 17;
            this.discPerAmtFinalTextBox.Text = "0";
            this.discPerAmtFinalTextBox.TextChanged += new System.EventHandler(this.discPerAmtFinalTextBox_TextChanged);
            // 
            // discPerAmtTextBox
            // 
            this.discPerAmtTextBox.Location = new System.Drawing.Point(218, 19);
            this.discPerAmtTextBox.Name = "discPerAmtTextBox";
            this.discPerAmtTextBox.Size = new System.Drawing.Size(71, 22);
            this.discPerAmtTextBox.TabIndex = 16;
            this.discPerAmtTextBox.Text = "0";
            this.discPerAmtTextBox.TextChanged += new System.EventHandler(this.discPerAmtTextBox_TextChanged);
            // 
            // discPerTextBox
            // 
            this.discPerTextBox.Location = new System.Drawing.Point(131, 19);
            this.discPerTextBox.Name = "discPerTextBox";
            this.discPerTextBox.Size = new System.Drawing.Size(43, 22);
            this.discPerTextBox.TabIndex = 15;
            this.discPerTextBox.Text = "0";
            this.discPerTextBox.TextChanged += new System.EventHandler(this.discPerTextBox_TextChanged);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(9, 72);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(78, 16);
            this.label19.TabIndex = 2;
            this.label19.Text = "CASE NO.";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(8, 45);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(83, 16);
            this.label18.TabIndex = 1;
            this.label18.Text = "ADD/LESS";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(295, 43);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(48, 16);
            this.label24.TabIndex = 0;
            this.label24.Text = "AMT.:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(295, 19);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(48, 16);
            this.label23.TabIndex = 0;
            this.label23.Text = "AMT.:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(8, 19);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(86, 16);
            this.label17.TabIndex = 0;
            this.label17.Text = "DISCOUNT";
            // 
            // remarkTextBox
            // 
            this.remarkTextBox.BackColor = System.Drawing.SystemColors.Info;
            this.remarkTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.remarkTextBox.Location = new System.Drawing.Point(112, 443);
            this.remarkTextBox.Name = "remarkTextBox";
            this.remarkTextBox.Size = new System.Drawing.Size(451, 22);
            this.remarkTextBox.TabIndex = 13;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(31, 444);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(75, 16);
            this.label16.TabIndex = 80;
            this.label16.Text = "REMARK:";
            // 
            // openBillButton
            // 
            this.openBillButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.openBillButton.Location = new System.Drawing.Point(465, 594);
            this.openBillButton.Name = "openBillButton";
            this.openBillButton.Size = new System.Drawing.Size(99, 38);
            this.openBillButton.TabIndex = 32;
            this.openBillButton.Text = "OPEN BILL";
            this.openBillButton.UseVisualStyleBackColor = true;
            this.openBillButton.Click += new System.EventHandler(this.openBillButton_Click);
            // 
            // totalAmountTextBox
            // 
            this.totalAmountTextBox.BackColor = System.Drawing.SystemColors.Info;
            this.totalAmountTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalAmountTextBox.Location = new System.Drawing.Point(1063, 445);
            this.totalAmountTextBox.Name = "totalAmountTextBox";
            this.totalAmountTextBox.ReadOnly = true;
            this.totalAmountTextBox.Size = new System.Drawing.Size(100, 22);
            this.totalAmountTextBox.TabIndex = 79;
            this.totalAmountTextBox.TabStop = false;
            this.totalAmountTextBox.Text = "0";
            this.totalAmountTextBox.TextChanged += new System.EventHandler(this.totalAmountTextBox_TextChanged);
            // 
            // printButton
            // 
            this.printButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.printButton.Location = new System.Drawing.Point(606, 594);
            this.printButton.Name = "printButton";
            this.printButton.Size = new System.Drawing.Size(90, 38);
            this.printButton.TabIndex = 33;
            this.printButton.Text = "PRINT";
            this.printButton.UseVisualStyleBackColor = true;
            // 
            // addNewButton
            // 
            this.addNewButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.addNewButton.Location = new System.Drawing.Point(193, 593);
            this.addNewButton.Name = "addNewButton";
            this.addNewButton.Size = new System.Drawing.Size(90, 38);
            this.addNewButton.TabIndex = 30;
            this.addNewButton.Text = "ADD NEW";
            this.addNewButton.UseVisualStyleBackColor = true;
            this.addNewButton.Click += new System.EventHandler(this.addNewButton_Click);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(328, 594);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(90, 38);
            this.button1.TabIndex = 31;
            this.button1.Text = "SAVE BILL";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // taxValTextBox
            // 
            this.taxValTextBox.BackColor = System.Drawing.Color.White;
            this.taxValTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.taxValTextBox.ForeColor = System.Drawing.Color.Red;
            this.taxValTextBox.Location = new System.Drawing.Point(1063, 496);
            this.taxValTextBox.Name = "taxValTextBox";
            this.taxValTextBox.ReadOnly = true;
            this.taxValTextBox.Size = new System.Drawing.Size(100, 22);
            this.taxValTextBox.TabIndex = 84;
            this.taxValTextBox.TabStop = false;
            this.taxValTextBox.Text = "0";
            this.taxValTextBox.TextChanged += new System.EventHandler(this.taxValTextBox_TextChanged);
            // 
            // deleteButton
            // 
            this.deleteButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.deleteButton.Location = new System.Drawing.Point(738, 594);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(75, 38);
            this.deleteButton.TabIndex = 34;
            this.deleteButton.Text = "DELETE";
            this.deleteButton.UseVisualStyleBackColor = true;
            this.deleteButton.Click += new System.EventHandler(this.deleteButton_Click);
            // 
            // billAmtTextBox
            // 
            this.billAmtTextBox.BackColor = System.Drawing.Color.White;
            this.billAmtTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.billAmtTextBox.ForeColor = System.Drawing.Color.Red;
            this.billAmtTextBox.Location = new System.Drawing.Point(1063, 547);
            this.billAmtTextBox.Name = "billAmtTextBox";
            this.billAmtTextBox.ReadOnly = true;
            this.billAmtTextBox.Size = new System.Drawing.Size(100, 22);
            this.billAmtTextBox.TabIndex = 85;
            this.billAmtTextBox.TabStop = false;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(881, 551);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(111, 16);
            this.label32.TabIndex = 82;
            this.label32.Text = "BILL AMOUNT:";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(881, 498);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(132, 16);
            this.label31.TabIndex = 83;
            this.label31.Text = "TAXABLE VALUE:";
            // 
            // totalQtyTextBox
            // 
            this.totalQtyTextBox.BackColor = System.Drawing.SystemColors.Info;
            this.totalQtyTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalQtyTextBox.Location = new System.Drawing.Point(882, 445);
            this.totalQtyTextBox.Name = "totalQtyTextBox";
            this.totalQtyTextBox.ReadOnly = true;
            this.totalQtyTextBox.Size = new System.Drawing.Size(63, 26);
            this.totalQtyTextBox.TabIndex = 77;
            this.totalQtyTextBox.TabStop = false;
            // 
            // totalPcsTextBox
            // 
            this.totalPcsTextBox.BackColor = System.Drawing.SystemColors.Info;
            this.totalPcsTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalPcsTextBox.Location = new System.Drawing.Point(737, 445);
            this.totalPcsTextBox.Name = "totalPcsTextBox";
            this.totalPcsTextBox.ReadOnly = true;
            this.totalPcsTextBox.Size = new System.Drawing.Size(61, 26);
            this.totalPcsTextBox.TabIndex = 74;
            this.totalPcsTextBox.TabStop = false;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(570, 445);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(115, 16);
            this.label15.TabIndex = 73;
            this.label15.Text = "GRAND TOTAL";
            // 
            // purchaseDataGridView
            // 
            this.purchaseDataGridView.BackgroundColor = System.Drawing.SystemColors.ControlDarkDark;
            this.purchaseDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.purchaseDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.itemNameGridCombo,
            this.bundlesEditBoxGrid,
            this.hsncEditBoxGrid,
            this.packingComboGrid,
            this.unitComboGrid,
            this.pcsTextBoxGrid,
            this.cutEditBoxGrid,
            this.qtyEditBoxGid,
            this.rateTextBoxGrid,
            this.amountTextBoxGrid,
            this.rdAmountTextBoxColumn,
            this.discountAmountTextBoxColumn,
            this.addLessTextBoxColumn,
            this.cgstPerTextBoxColumn,
            this.cgstAmountTextBoxColumn,
            this.sgstPerTextBoxColumn,
            this.sgstAmountTextBoxColumn,
            this.igstPerTextBoxColumn,
            this.igstAmountTextBoxColumn,
            this.totalAmountTextBoxColumn});
            this.purchaseDataGridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.purchaseDataGridView.Location = new System.Drawing.Point(31, 149);
            this.purchaseDataGridView.Name = "purchaseDataGridView";
            this.purchaseDataGridView.Size = new System.Drawing.Size(1152, 269);
            this.purchaseDataGridView.TabIndex = 12;
            this.purchaseDataGridView.CellValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.purchaseDataGridView_CellValidated);
            this.purchaseDataGridView.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.purchaseDataGridView_CellValidating);
            this.purchaseDataGridView.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.purchaseDataGridView_CellValueChanged);
            // 
            // lrnoTextBox
            // 
            this.lrnoTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lrnoTextBox.Location = new System.Drawing.Point(1047, 111);
            this.lrnoTextBox.Name = "lrnoTextBox";
            this.lrnoTextBox.Size = new System.Drawing.Size(134, 22);
            this.lrnoTextBox.TabIndex = 11;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(976, 114);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(61, 16);
            this.label14.TabIndex = 70;
            this.label14.Text = "LR NO.:";
            // 
            // transportComboBox
            // 
            this.transportComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.transportComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.transportComboBox.FormattingEnabled = true;
            this.transportComboBox.Location = new System.Drawing.Point(793, 111);
            this.transportComboBox.Name = "transportComboBox";
            this.transportComboBox.Size = new System.Drawing.Size(174, 24);
            this.transportComboBox.TabIndex = 10;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(680, 114);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(106, 16);
            this.label13.TabIndex = 69;
            this.label13.Text = "TRANSPORT:";
            // 
            // gstnTextBox
            // 
            this.gstnTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gstnTextBox.Location = new System.Drawing.Point(136, 107);
            this.gstnTextBox.Name = "gstnTextBox";
            this.gstnTextBox.ReadOnly = true;
            this.gstnTextBox.Size = new System.Drawing.Size(233, 22);
            this.gstnTextBox.TabIndex = 8;
            this.gstnTextBox.TabStop = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(27, 108);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(109, 16);
            this.label12.TabIndex = 68;
            this.label12.Text = "PARTY GSTN:";
            // 
            // brokerComboBox
            // 
            this.brokerComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.brokerComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.brokerComboBox.FormattingEnabled = true;
            this.brokerComboBox.Location = new System.Drawing.Point(458, 111);
            this.brokerComboBox.Name = "brokerComboBox";
            this.brokerComboBox.Size = new System.Drawing.Size(208, 24);
            this.brokerComboBox.TabIndex = 9;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(378, 112);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(74, 16);
            this.label11.TabIndex = 67;
            this.label11.Text = "BROKER:";
            // 
            // stationComboBox
            // 
            this.stationComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.stationComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stationComboBox.FormattingEnabled = true;
            this.stationComboBox.Location = new System.Drawing.Point(1047, 72);
            this.stationComboBox.Name = "stationComboBox";
            this.stationComboBox.Size = new System.Drawing.Size(148, 24);
            this.stationComboBox.TabIndex = 8;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(963, 75);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(78, 16);
            this.label10.TabIndex = 66;
            this.label10.Text = "STATION:";
            // 
            // gstTypeComboBox
            // 
            this.gstTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.gstTypeComboBox.DropDownWidth = 200;
            this.gstTypeComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gstTypeComboBox.FormattingEnabled = true;
            this.gstTypeComboBox.Location = new System.Drawing.Point(615, 72);
            this.gstTypeComboBox.Name = "gstTypeComboBox";
            this.gstTypeComboBox.Size = new System.Drawing.Size(178, 24);
            this.gstTypeComboBox.TabIndex = 6;
            this.gstTypeComboBox.SelectedIndexChanged += new System.EventHandler(this.gstTypeComboBox_SelectedIndexChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(527, 75);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(87, 16);
            this.label9.TabIndex = 65;
            this.label9.Text = "GST TYPE:";
            // 
            // stateComboBox
            // 
            this.stateComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.stateComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stateComboBox.FormattingEnabled = true;
            this.stateComboBox.Location = new System.Drawing.Point(464, 72);
            this.stateComboBox.Name = "stateComboBox";
            this.stateComboBox.Size = new System.Drawing.Size(62, 24);
            this.stateComboBox.TabIndex = 5;
            this.stateComboBox.SelectedValueChanged += new System.EventHandler(this.stateComboBox_SelectedValueChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(368, 75);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(89, 16);
            this.label8.TabIndex = 64;
            this.label8.Text = "State Code:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(27, 71);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(63, 16);
            this.label7.TabIndex = 62;
            this.label7.Text = "PARTY:";
            // 
            // dateTextBox
            // 
            this.dateTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTextBox.Location = new System.Drawing.Point(1047, 35);
            this.dateTextBox.Name = "dateTextBox";
            this.dateTextBox.Size = new System.Drawing.Size(116, 22);
            this.dateTextBox.TabIndex = 3;
            this.dateTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.dateTextBox_Validating);
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(984, 38);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 16);
            this.label6.TabIndex = 59;
            this.label6.Text = "DATE:";
            // 
            // billNoTextBox
            // 
            this.billNoTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.billNoTextBox.Location = new System.Drawing.Point(892, 36);
            this.billNoTextBox.Name = "billNoTextBox";
            this.billNoTextBox.Size = new System.Drawing.Size(89, 22);
            this.billNoTextBox.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(825, 38);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 16);
            this.label5.TabIndex = 55;
            this.label5.Text = "BILL NO.";
            // 
            // vouvherNoTextBox
            // 
            this.vouvherNoTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vouvherNoTextBox.Location = new System.Drawing.Point(733, 36);
            this.vouvherNoTextBox.Name = "vouvherNoTextBox";
            this.vouvherNoTextBox.Size = new System.Drawing.Size(83, 22);
            this.vouvherNoTextBox.TabIndex = 1;
            this.vouvherNoTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.vouvherNoTextBox_KeyDown);
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(616, 39);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(112, 16);
            this.label4.TabIndex = 52;
            this.label4.Text = "VOUCHER NO.";
            // 
            // purchaseTypeComboBox
            // 
            this.purchaseTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.purchaseTypeComboBox.DropDownWidth = 200;
            this.purchaseTypeComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.purchaseTypeComboBox.FormattingEnabled = true;
            this.purchaseTypeComboBox.Location = new System.Drawing.Point(432, 35);
            this.purchaseTypeComboBox.MaxDropDownItems = 20;
            this.purchaseTypeComboBox.Name = "purchaseTypeComboBox";
            this.purchaseTypeComboBox.Size = new System.Drawing.Size(178, 24);
            this.purchaseTypeComboBox.TabIndex = 0;
            this.purchaseTypeComboBox.TabStop = false;
            this.purchaseTypeComboBox.SelectedIndexChanged += new System.EventHandler(this.purchaseTypeComboBox_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(373, 37);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 16);
            this.label3.TabIndex = 48;
            this.label3.Text = "TYPE:";
            // 
            // comNameTextBox
            // 
            this.comNameTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comNameTextBox.Location = new System.Drawing.Point(136, 37);
            this.comNameTextBox.Name = "comNameTextBox";
            this.comNameTextBox.ReadOnly = true;
            this.comNameTextBox.Size = new System.Drawing.Size(233, 26);
            this.comNameTextBox.TabIndex = 46;
            this.comNameTextBox.TabStop = false;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(27, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 16);
            this.label2.TabIndex = 45;
            this.label2.Text = "COMPANY:";
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(31, 87);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(78, 13);
            this.linkLabel1.TabIndex = 86;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Add New Party";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // itemLinkLabel
            // 
            this.itemLinkLabel.AutoSize = true;
            this.itemLinkLabel.Location = new System.Drawing.Point(87, 132);
            this.itemLinkLabel.Name = "itemLinkLabel";
            this.itemLinkLabel.Size = new System.Drawing.Size(74, 13);
            this.itemLinkLabel.TabIndex = 87;
            this.itemLinkLabel.TabStop = true;
            this.itemLinkLabel.Text = "Add New Item";
            this.itemLinkLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.itemLinkLabel_LinkClicked);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(800, 76);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 16);
            this.label1.TabIndex = 64;
            this.label1.Text = "Ref BillNo:";
            // 
            // dataGridViewComboBoxColumn1
            // 
            dataGridViewCellStyle21.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataGridViewComboBoxColumn1.DefaultCellStyle = dataGridViewCellStyle21;
            this.dataGridViewComboBoxColumn1.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn1.HeaderText = "ITEM NAME";
            this.dataGridViewComboBoxColumn1.Items.AddRange(new object[] {
            "KALPESH",
            "KAWAR"});
            this.dataGridViewComboBoxColumn1.MaxDropDownItems = 100;
            this.dataGridViewComboBoxColumn1.Name = "dataGridViewComboBoxColumn1";
            this.dataGridViewComboBoxColumn1.Width = 230;
            // 
            // dataGridViewComboBoxColumn2
            // 
            dataGridViewCellStyle22.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle22.NullValue = "BEG";
            dataGridViewCellStyle22.SelectionForeColor = System.Drawing.Color.Black;
            this.dataGridViewComboBoxColumn2.DefaultCellStyle = dataGridViewCellStyle22;
            this.dataGridViewComboBoxColumn2.HeaderText = "PACKING";
            this.dataGridViewComboBoxColumn2.Items.AddRange(new object[] {
            "BEG",
            "NAKED",
            "BOX"});
            this.dataGridViewComboBoxColumn2.Name = "dataGridViewComboBoxColumn2";
            // 
            // dataGridViewComboBoxColumn3
            // 
            dataGridViewCellStyle23.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle23.NullValue = "PCS";
            dataGridViewCellStyle23.SelectionForeColor = System.Drawing.Color.Black;
            this.dataGridViewComboBoxColumn3.DefaultCellStyle = dataGridViewCellStyle23;
            this.dataGridViewComboBoxColumn3.HeaderText = "UNIT";
            this.dataGridViewComboBoxColumn3.Items.AddRange(new object[] {
            "PCS",
            "MTRS",
            "KGS",
            "OTHERS",
            "SUITS"});
            this.dataGridViewComboBoxColumn3.Name = "dataGridViewComboBoxColumn3";
            // 
            // refBillNoComboBox
            // 
            this.refBillNoComboBox.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.refBillNoComboBox.DropDownWidth = 17;
            this.refBillNoComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.refBillNoComboBox.FormattingEnabled = true;
            this.refBillNoComboBox.Location = new System.Drawing.Point(884, 74);
            this.refBillNoComboBox.Name = "refBillNoComboBox";
            this.refBillNoComboBox.Size = new System.Drawing.Size(73, 21);
            this.refBillNoComboBox.TabIndex = 7;
            this.refBillNoComboBox.ViewColumn = 0;
            // 
            // partyComboBox
            // 
            this.partyComboBox.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.partyComboBox.DropDownWidth = 17;
            this.partyComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.partyComboBox.Location = new System.Drawing.Point(136, 72);
            this.partyComboBox.Name = "partyComboBox";
            this.partyComboBox.Size = new System.Drawing.Size(233, 23);
            this.partyComboBox.TabIndex = 4;
            this.partyComboBox.ViewColumn = 0;
            this.partyComboBox.SelectedIndexChanged += new System.EventHandler(this.partyComboBox_SelectedIndexChanged);
            // 
            // itemNameGridCombo
            // 
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.itemNameGridCombo.DefaultCellStyle = dataGridViewCellStyle1;
            this.itemNameGridCombo.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.itemNameGridCombo.HeaderText = "ITEM NAME";
            this.itemNameGridCombo.MaxDropDownItems = 100;
            this.itemNameGridCombo.Name = "itemNameGridCombo";
            this.itemNameGridCombo.Width = 230;
            // 
            // bundlesEditBoxGrid
            // 
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bundlesEditBoxGrid.DefaultCellStyle = dataGridViewCellStyle2;
            this.bundlesEditBoxGrid.HeaderText = "BUNDLES";
            this.bundlesEditBoxGrid.Name = "bundlesEditBoxGrid";
            // 
            // hsncEditBoxGrid
            // 
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hsncEditBoxGrid.DefaultCellStyle = dataGridViewCellStyle3;
            this.hsncEditBoxGrid.HeaderText = "HSN CODE";
            this.hsncEditBoxGrid.Name = "hsncEditBoxGrid";
            // 
            // packingComboGrid
            // 
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.NullValue = "BEG";
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            this.packingComboGrid.DefaultCellStyle = dataGridViewCellStyle4;
            this.packingComboGrid.HeaderText = "PACKING";
            this.packingComboGrid.Items.AddRange(new object[] {
            "BEG",
            "NAKED",
            "BOX"});
            this.packingComboGrid.Name = "packingComboGrid";
            // 
            // unitComboGrid
            // 
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.NullValue = "PCS";
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            this.unitComboGrid.DefaultCellStyle = dataGridViewCellStyle5;
            this.unitComboGrid.HeaderText = "UNIT";
            this.unitComboGrid.Items.AddRange(new object[] {
            "PCS",
            "MTRS",
            "KGS",
            "OTHERS",
            "SUITS"});
            this.unitComboGrid.Name = "unitComboGrid";
            // 
            // pcsTextBoxGrid
            // 
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.NullValue = "0";
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            this.pcsTextBoxGrid.DefaultCellStyle = dataGridViewCellStyle6;
            this.pcsTextBoxGrid.HeaderText = "PCS.";
            this.pcsTextBoxGrid.Name = "pcsTextBoxGrid";
            // 
            // cutEditBoxGrid
            // 
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.NullValue = "0";
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black;
            this.cutEditBoxGrid.DefaultCellStyle = dataGridViewCellStyle7;
            this.cutEditBoxGrid.HeaderText = "CUT";
            this.cutEditBoxGrid.Name = "cutEditBoxGrid";
            this.cutEditBoxGrid.Width = 75;
            // 
            // qtyEditBoxGid
            // 
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.NullValue = "0";
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.Black;
            this.qtyEditBoxGid.DefaultCellStyle = dataGridViewCellStyle8;
            this.qtyEditBoxGid.HeaderText = "MTS/QTY.";
            this.qtyEditBoxGid.Name = "qtyEditBoxGid";
            this.qtyEditBoxGid.Width = 83;
            // 
            // rateTextBoxGrid
            // 
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.NullValue = "0";
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.Black;
            this.rateTextBoxGrid.DefaultCellStyle = dataGridViewCellStyle9;
            this.rateTextBoxGrid.HeaderText = "RATE";
            this.rateTextBoxGrid.Name = "rateTextBoxGrid";
            // 
            // amountTextBoxGrid
            // 
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.NullValue = "0";
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.Black;
            this.amountTextBoxGrid.DefaultCellStyle = dataGridViewCellStyle10;
            this.amountTextBoxGrid.HeaderText = "AMOUNT";
            this.amountTextBoxGrid.Name = "amountTextBoxGrid";
            this.amountTextBoxGrid.Width = 145;
            // 
            // rdAmountTextBoxColumn
            // 
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.NullValue = "0";
            this.rdAmountTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle11;
            this.rdAmountTextBoxColumn.HeaderText = "RD";
            this.rdAmountTextBoxColumn.Name = "rdAmountTextBoxColumn";
            // 
            // discountAmountTextBoxColumn
            // 
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.NullValue = "0";
            this.discountAmountTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle12;
            this.discountAmountTextBoxColumn.HeaderText = "DiscountAmount";
            this.discountAmountTextBoxColumn.Name = "discountAmountTextBoxColumn";
            // 
            // addLessTextBoxColumn
            // 
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle13.NullValue = "0";
            this.addLessTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle13;
            this.addLessTextBoxColumn.HeaderText = "ADD /LESS";
            this.addLessTextBoxColumn.Name = "addLessTextBoxColumn";
            // 
            // cgstPerTextBoxColumn
            // 
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.NullValue = "2.5";
            this.cgstPerTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle14;
            this.cgstPerTextBoxColumn.HeaderText = "CGST %";
            this.cgstPerTextBoxColumn.Name = "cgstPerTextBoxColumn";
            // 
            // cgstAmountTextBoxColumn
            // 
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle15.NullValue = "0";
            this.cgstAmountTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle15;
            this.cgstAmountTextBoxColumn.HeaderText = "CGST AMOUNT";
            this.cgstAmountTextBoxColumn.Name = "cgstAmountTextBoxColumn";
            // 
            // sgstPerTextBoxColumn
            // 
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle16.NullValue = "2.5";
            this.sgstPerTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle16;
            this.sgstPerTextBoxColumn.HeaderText = "SGST%";
            this.sgstPerTextBoxColumn.Name = "sgstPerTextBoxColumn";
            // 
            // sgstAmountTextBoxColumn
            // 
            dataGridViewCellStyle17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle17.NullValue = "0";
            this.sgstAmountTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle17;
            this.sgstAmountTextBoxColumn.HeaderText = "SGSTAMOUNT";
            this.sgstAmountTextBoxColumn.Name = "sgstAmountTextBoxColumn";
            // 
            // igstPerTextBoxColumn
            // 
            dataGridViewCellStyle18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle18.NullValue = "5";
            this.igstPerTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle18;
            this.igstPerTextBoxColumn.HeaderText = "IGST% ";
            this.igstPerTextBoxColumn.Name = "igstPerTextBoxColumn";
            // 
            // igstAmountTextBoxColumn
            // 
            dataGridViewCellStyle19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle19.NullValue = "0";
            this.igstAmountTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle19;
            this.igstAmountTextBoxColumn.HeaderText = "IGST AMOUNT";
            this.igstAmountTextBoxColumn.Name = "igstAmountTextBoxColumn";
            // 
            // totalAmountTextBoxColumn
            // 
            dataGridViewCellStyle20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle20.NullValue = "0";
            this.totalAmountTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle20;
            this.totalAmountTextBoxColumn.HeaderText = "TOTAL AMOUNT";
            this.totalAmountTextBoxColumn.Name = "totalAmountTextBoxColumn";
            // 
            // PurchaseWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.refBillNoComboBox);
            this.Controls.Add(this.partyComboBox);
            this.Controls.Add(this.itemLinkLabel);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.remarkTextBox);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.openBillButton);
            this.Controls.Add(this.totalAmountTextBox);
            this.Controls.Add(this.printButton);
            this.Controls.Add(this.addNewButton);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.taxValTextBox);
            this.Controls.Add(this.deleteButton);
            this.Controls.Add(this.billAmtTextBox);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.totalQtyTextBox);
            this.Controls.Add(this.totalPcsTextBox);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.purchaseDataGridView);
            this.Controls.Add(this.lrnoTextBox);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.transportComboBox);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.gstnTextBox);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.brokerComboBox);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.stationComboBox);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.gstTypeComboBox);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.stateComboBox);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.dateTextBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.billNoTextBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.vouvherNoTextBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.purchaseTypeComboBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.comNameTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Purchase);
            this.Name = "PurchaseWindow";
            this.Size = new System.Drawing.Size(1279, 725);
            this.Load += new System.EventHandler(this.PurchaseWindow_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.purchaseDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Purchase;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox igstAmtTextBox;
        private System.Windows.Forms.TextBox sgstAmtTextBox;
        private System.Windows.Forms.TextBox cgstAmtTextBox;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox igstRateTextBox;
        private System.Windows.Forms.TextBox sgstRateTextBox;
        private System.Windows.Forms.TextBox cgstRateTextBox;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox caseNoTextBox2;
        private System.Windows.Forms.TextBox caseNoTextBox;
        private System.Windows.Forms.TextBox addLessAmtFinalTextBox;
        private System.Windows.Forms.TextBox addLessAmtTextBox;
        private System.Windows.Forms.TextBox addLessTextBox;
        private System.Windows.Forms.TextBox discPerAmtFinalTextBox;
        private System.Windows.Forms.TextBox discPerAmtTextBox;
        private System.Windows.Forms.TextBox discPerTextBox;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox remarkTextBox;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button openBillButton;
        private System.Windows.Forms.TextBox totalAmountTextBox;
        private System.Windows.Forms.Button printButton;
        private System.Windows.Forms.Button addNewButton;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox taxValTextBox;
        private System.Windows.Forms.Button deleteButton;
        private System.Windows.Forms.TextBox billAmtTextBox;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox totalQtyTextBox;
        private System.Windows.Forms.TextBox totalPcsTextBox;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.DataGridView purchaseDataGridView;
        private System.Windows.Forms.TextBox lrnoTextBox;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox transportComboBox;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox gstnTextBox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox brokerComboBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox stationComboBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox gstTypeComboBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox stateComboBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox dateTextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox billNoTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox vouvherNoTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox purchaseTypeComboBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox comNameTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.LinkLabel itemLinkLabel;
        private System.Windows.Forms.TextBox dueDaysTextBox;
        private System.Windows.Forms.Label label34;
        private JTG.ColumnComboBox partyComboBox;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn1;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn2;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn3;
        private System.Windows.Forms.Label label1;
        private JTG.ColumnComboBox refBillNoComboBox;
        private System.Windows.Forms.DataGridViewComboBoxColumn itemNameGridCombo;
        private System.Windows.Forms.DataGridViewTextBoxColumn bundlesEditBoxGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn hsncEditBoxGrid;
        private System.Windows.Forms.DataGridViewComboBoxColumn packingComboGrid;
        private System.Windows.Forms.DataGridViewComboBoxColumn unitComboGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn pcsTextBoxGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn cutEditBoxGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn qtyEditBoxGid;
        private System.Windows.Forms.DataGridViewTextBoxColumn rateTextBoxGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn amountTextBoxGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn rdAmountTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn discountAmountTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn addLessTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cgstPerTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cgstAmountTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sgstPerTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sgstAmountTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn igstPerTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn igstAmountTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn totalAmountTextBoxColumn;
    }
}
