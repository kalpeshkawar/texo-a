﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Texo_Advance.Printer;

namespace Texo_Advance
{
    public partial class Form1 : Form
    {
        public string companyGstn = "";
        long lPurchaseTypeMode = 1;//pruchase type modes        
        long lSalesTypeMode = 1;
        public Form1()
        {
            InitializeComponent();

        }

        private void Form1_Load(object sender, EventArgs e)
        {

            userLogin loginWindow = new userLogin(this);
            //userLogin loginWindow = new userLogin();
            if (loginWindow != null)
            {
                loginWindow.Activate();
                loginWindow.Focus();
                loginWindow.TopMost = true;
                //loginWindow.TopLevel = false;

                loginWindow.ShowDialog();
                
            }
            else
            {
               // MessageBox.Show("Application unable to open . Please restart the appalication", "Login Error", MessageBoxButtons.OK);
                this.Close();
            }
                
        }
        public void statusUpdate(string strComName, string strFinYear, string strCompanyGstn)
        {
            labelName.Text = Properties.Settings.Default.CompanyName;
            labelYear.Text = Properties.Settings.Default.FinancialYear;
            companyGstn = strCompanyGstn;
        }
        
        private void salesButton_Click(object sender, EventArgs e)
        {

            labelStatus.Text = "Sales Window";
            salesWindow sw = new salesWindow(labelName.Text,companyGstn);

            MainControlClass.ShowControls(sw, homePanel);
            sw.setSalesTypeMode(lSalesTypeMode);
            sw.setDafaultData();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult result = MessageBox.Show("Do You want to close the Application?", "Exit", MessageBoxButtons.YesNo);
            if (result == DialogResult.No)
                e.Cancel = true;

        }

        private void purchaseButon_Click(object sender, EventArgs e)
        {
            labelStatus.Text = "Purchase Entry";
            PurchaseWindow pw = new PurchaseWindow(labelName.Text, companyGstn);
            MainControlClass.ShowControls(pw, homePanel);
            pw.setPurchaseTypeMode(lPurchaseTypeMode);
            pw.setDafaultData();
        }

        private void homeButton_Click(object sender, EventArgs e)
        {
            labelStatus.Text = "Home Screen";
            MainControlClass.clearControls(homePanel);
        }

        public void openLedgerDetailedWindow(long lPartyID)
        {
            labelStatus.Text = "Home Screen";
            LedgerDetailedWindow ldw = new LedgerDetailedWindow();
            MainControlClass.ShowControls(ldw, homePanel);
            ldw.setPartyLedgerData(lPartyID);
        }

        private void logOutButton_Click(object sender, EventArgs e)
        {
            userLogin loginWindow = new userLogin(this);
            if (loginWindow != null)
            {
                loginWindow.Activate();
                loginWindow.Focus();
                loginWindow.TopMost = true;
                //loginWindow.TopLevel = false;

                loginWindow.ShowDialog();

            }
        }

        private void Form1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Escape)
            {
                //MessageBox.Show("Escape key pressed");
                DialogResult result;
                if (labelStatus.Text != "Home Screen")
                {
                    result = MessageBox.Show("Do you want to exit the current window", labelStatus.Text, MessageBoxButtons.YesNo);
                    if (result == DialogResult.Yes)
                    {
                        labelStatus.Text = "Home Screen";
                        MainControlClass.clearControls(homePanel);
                    }
                        

                }
                else
                    this.Close();
            }
        }

        private void greyPurchaseButton_Click(object sender, EventArgs e)
        {
            labelStatus.Text = "Grey Purchase Entry";
            GreyPurchaseWindow gpw = new GreyPurchaseWindow(labelName.Text, companyGstn);
            gpw.setDafaultData();
            MainControlClass.ShowControls(gpw, homePanel);
            
        }

        private void millTransactionsButton_Click(object sender, EventArgs e)
        {
            labelStatus.Text = "Mill Dispatch Window";
            MillDispatchWindow mdw = new MillDispatchWindow(labelName.Text, companyGstn);
            MainControlClass.ShowControls(mdw, homePanel);
        }

        private void jobWorkButton_Click(object sender, EventArgs e)
        {
            labelStatus.Text = "Job Work Process";
            JobWorkWindow jww = new JobWorkWindow(labelName.Text, companyGstn);
            MainControlClass.ShowControls(jww, homePanel);
        }

        private void masterButton_Click(object sender, EventArgs e)
        {
            labelStatus.Text = "Master Settings Window";
            MasterWindow mw = new MasterWindow();
            MainControlClass.ShowControls(mw, homePanel);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            labelStatus.Text = "Mill Receipt Window";
            MillReceiptWindow mrw = new MillReceiptWindow(labelName.Text, companyGstn);
            MainControlClass.ShowControls(mrw, homePanel);
        }

        private void passbookButton_Click(object sender, EventArgs e)
        {
            labelStatus.Text = "Bank PassBook";
            PassBookWindow pbw = new PassBookWindow(labelName.Text, companyGstn);
            MainControlClass.ShowControls(pbw, homePanel);
        }

        private void openingBalancesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            labelStatus.Text = "Opening Balance Window";
            OpeningBalancesWindow obw = new OpeningBalancesWindow(labelName.Text);
            MainControlClass.ShowControls(obw, homePanel);
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            /*if(e.Control == true && e.KeyCode == Keys.H)
            {
                homeButton.PerformClick();
            }
            else if (e.Control == true && e.KeyCode == Keys.S)
            {
                salesButton.PerformClick();
            }
            else if (e.Control == true && e.KeyCode == Keys.P)
            {
                purchaseButon.PerformClick();
            }
            else if (e.Control == true && e.KeyCode == Keys.G)
            {
                greyPurchaseButton.PerformClick();
            }
            else if (e.Control == true && e.KeyCode == Keys.D)
            {
                millTransactionsButton.PerformClick();
            }
            else if (e.Control == true && e.KeyCode == Keys.M)
            {
                button1.PerformClick();
            }
            else if (e.Control == true && e.KeyCode == Keys.J)
            {
                jobWorkButton.PerformClick();
            }
            else if (e.Control == true && e.KeyCode == Keys.P)
            {
                passbookButton.PerformClick();
            }
            else if (e.Control == true && e.KeyCode == Keys.R)
            {
                reportButton.PerformClick();
            }
            else if (e.Control == true && e.KeyCode == Keys.K)
            {
                gstReturnsButton.PerformClick();
            }
            else if (e.Control == true && e.KeyCode == Keys.A)
            {
                masterButton.PerformClick();
            }
            else if (e.Control == true && e.KeyCode == Keys.L)
            {
                logOutButton.PerformClick();
            }*/
        }

        private void purchaseBillEntryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            lPurchaseTypeMode = 1;
            purchaseButon.PerformClick();
        }

        private void salesBillEntryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            salesButton.PerformClick();
        }

        private void greyPurchaseBillToolStripMenuItem_Click(object sender, EventArgs e)
        {
            greyPurchaseButton.PerformClick();
        }

        private void packingMaterialToolStripMenuItem_Click(object sender, EventArgs e)
        {
            lPurchaseTypeMode = 2;
            purchaseButon.PerformClick();
            lPurchaseTypeMode = 1;
        }

        private void millReceiptBillToolStripMenuItem_Click(object sender, EventArgs e)
        {
            button1.PerformClick();
        }

        private void jobWorkToolStripMenuItem_Click(object sender, EventArgs e)
        {
            jobWorkButton.PerformClick();
        }

        private void openingStockToolStripMenuItem_Click(object sender, EventArgs e)
        {
            labelStatus.Text = "Opening Stock Window";
            OpeningStockWindow osw = new OpeningStockWindow(labelName.Text);
            MainControlClass.ShowControls(osw, homePanel);
        }

        private void journalEntriesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            labelStatus.Text = "Journal Entries Window";
            JournalEntryWindow jew = new JournalEntryWindow(labelName.Text);
            MainControlClass.ShowControls(jew, homePanel);
        }

        private void salesReturnToolStripMenuItem_Click(object sender, EventArgs e)
        {
            lSalesTypeMode = 5;
            salesButton.PerformClick();
            lSalesTypeMode = 1;
        }

        private void purchaseReturnToolStripMenuItem_Click(object sender, EventArgs e)
        {
            lPurchaseTypeMode = 6;
            purchaseButon.PerformClick();
            lPurchaseTypeMode = 1;
        }

        private void greySalesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            lSalesTypeMode = 2;
            salesButton.PerformClick();
            lSalesTypeMode = 1;
        }

        private void greyReturnToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void debitCreditNotesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            labelStatus.Text = "Credit / Debit Window";
            CreditDebitNoteWindow cdw = new CreditDebitNoteWindow(labelName.Text, companyGstn);
            MainControlClass.ShowControls(cdw, homePanel);
        }

        private void printerSettingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PrinterSet print = new PrinterSet();
            print.ShowDialog();
        }

<<<<<<< HEAD
        private void button2_Click(object sender, EventArgs e)
        {
            labelStatus.Text = "Account Ledger Window";
            LedgerWindow lw = new LedgerWindow(labelName.Text,this);
            
            MainControlClass.ShowControls(lw, homePanel);
=======
        private void reportButton_Click(object sender, EventArgs e)
        {
            labelStatus.Text = "Reports";
            Reports.ReportMain pbw = new Reports.ReportMain();
            MainControlClass.ShowControls(pbw, homePanel);
>>>>>>> c321d30cd5930451449378d6f30036a426314729
        }
    }
}
